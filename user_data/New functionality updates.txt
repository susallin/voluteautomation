1. Batch Review
 Assignment & Status
 - Shows whether User complete or not complete(checkbox)(would this be for the whole completetion of the Program?)
 - Shows users active(ex. 8-8-16)
 - Shows name(fistname, lastname)(limit of characters?)
 - Shows a button to assign/invite the users to a Learning Space(Invite)(are we assigning users directly rather then
   going to Manage Users? or we keeping the same behavior?)

 Review & Coaching
 - Shows a list of assign users and corresponding learning space they are assigned to
   (are we showing all of the nested LS(s) they are assign? maybe a drop down option to view within each user?)
 - The Active Learning Space(nested ls(s)) highlighted 
 - Whether the User completed nested LS(s), would it make sense to include Gates on this options for users
   to know which Nested LS are still open?
 - Will the User be able to Switch View to a User's Copy from here?

2. Journal
 - Create different blog entries, All users may add/edit/update blog entries
 - All users can upload images related to the blog entry
 - All users can comment/reply on the blog entries, they can update/delete their comments/replies
 - Facilitator+ can grade the users with stars(will the users appear on a drop down option on the right side?
   this way all users rated star would show and the comments/reply made) This idea is to bring both tabs into one,
   that way is easier for users to just look it in view
 - will there be any button to view the new entries made or will we show list of users to view their entries made(from newest to oldest)? 
   or last visited/updated entry?
 - Once clicking an entry of a user, entry shows with image(option), rate average star if there is any, entries made
   by the user(this refers to any updates made by the user to this single entry)
 - Once clicking an entry of a user, would it show other comments/replies made from other Users for this particular
   entry(this would be in drop down button to show details about the selected entry?)
 - Would Journal support Users within a group?(it will show the group name rather then the list of Users; Maybe a drop
   down button to show the list of users within that specific group?)
 - Will there be an option for Users to see what entries they previously reviewed?

3. Peer Feedback
 * Adding the tool:
    - View tool(both users)
    - no content(both users)
    - Participant can view their contents if they are assign to the Peer Feedback(ls must be published for them to view it)
    - Participant content not completed(display due date/count down) if due date is passed
      ls would be disable; they won't be able to enter to the ls or they can access it but will be mark
      as late with the time they sent.
    - if due date is not passed, they can enter feedback(this may apply to due date passed if we accept the previous
      logic)
    - they can 

Batch review � see Michael�s email

Journal (this is going to be a refactored Idea Board)

Peer Review package (and flows to setup for other packages) � Specs on Operations, but Melanie will be refining them

LTI � Canvas � Specs in Operations

Discussion updates (Yale, global aggregation) � Initial Specs in Operations

Slack bot channel (to coincide with Discussion updates)
 

Updated assessment design � it will be a Qualtrics tool, not assessments, and the new design needs to be added
Code coverage tools implemented for testing

Batch load of users/SIS integration (TBD)

New USERS tab in slideout (ties with batch load of users)

Templates from Trudy
 
Key automation tasks - TBD

All joyride text reviewed and updated

We can combine all these into 1 clean design, and call it REVIEW:

 

The Assignment & Status is what you see, we can discuss Details further. The Review & Coaching would show a list of assigned users and the corresponding learning spaces they are assigned to, with the active learning space highlighted.

 

I also mentioned that we will need another tab called USERS � that will allow the assignment of users from the design view. Let�s schedule a meeting after this deployment to discuss.