package education.volute.reader_testscripts;

import java.io.IOException;

public class ReaderTest {

	public void ReaderAdd(String Role) throws Exception{
		Reader_TC_01 ts = new Reader_TC_01();	
		ts.testReader_TC_01(Role);
	}
	
	public void ReaderEdit(String Role) throws Exception{
		Reader_TC_02 ts = new Reader_TC_02();	
		ts.testReader_TC_02(Role);
	}
	
	public void ReaderDelete(String Role){
		Reader_TC_03 ts = new Reader_TC_03();	
		ts.testReader_TC_03(Role);
	}
	
	public void ReaderContent(String Role, String str) throws Exception{
		Reader_TC_04 ts = new Reader_TC_04();	
		ts.verifyInfo(Role, str);
	}
}
