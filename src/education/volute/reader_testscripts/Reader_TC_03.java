package education.volute.reader_testscripts;

//import java.io.IOException;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Landing;
import education.volute.pages.LoginPage;
//import education.volute.pages.ManageLearningSpaces;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.SocialHub;
import education.volute.pages.NavigatorTool;
import education.volute.pages.Reader;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class Reader_TC_03 extends SuperTestScript {

	//@Test
	public void testReader_TC_03(String Role)
	
	{
		//LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		//SocialHub mj = new SocialHub();
		MenuItems mi = new MenuItems();
		SoftAssertCheck soft = new SoftAssertCheck();
		//Landing ld = new Landing();
		//ManagePrograms mp = new ManagePrograms();
		//ManageLearningSpaces mls = new ManageLearningSpaces();
		ReadCsvFiles csv = new ReadCsvFiles();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		Reader red = new Reader();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Reader, "DeleteReader3");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		//String uName = ExcelUtils.getCellData(i, 1);
        //String pwd = ExcelUtils.getCellData(i, 2);
		String toolDest = ExcelUtils.getCellData(i, 1);
        String actName = ExcelUtils.getCellData(i, 2);
        String subActName = ExcelUtils.getCellData(i, 3);
	    String pgmNameTxt = ExcelUtils.getCellData(i, 4);
        String redFileName = ExcelUtils.getCellData(i, 6);
        //String pgmNameTxt = ExcelUtils.getCellData(i, 3);
        //String lsNameTxt = ExcelUtils.getCellData(i, 4);
        //String lsNameTxt = ExcelUtils.getCellData(i, 5);
        //String lsDescTxt = ExcelUtils.getCellData(i, 6);
        //String sliDName = ExcelUtils.getCellData(i, 7);
        String uRole = ExcelUtils.getCellData(i, 11);
        String appLocName = ExcelUtils.getCellData(i, 12);
        String toolName = ExcelUtils.getCellData(i, 13);
        String expRes = ExcelUtils.getCellData(i, 8);
		
		/*lp.login(uName, pwd);
		lp.logibutton();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnMainMenu();*/
		toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
		/*mi.clickOnNavigation();
		mi.clickOnProgram();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ld.clickOnLandingMP();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnselectedprogram(pgmNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnManageMashupicon(pgmNameTxt);
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*mls.selectaLearningSpace(lsNameTxt);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mls.clickOnEditButton(lsNameTxt);
		mls.clickOnConfigButton();*/
		/*nt.clickOnJourney();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mj.clickOnLaunchAct(actName);*/
		/*try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		mi.fetchAppTool(appLocName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mi.SelectManageFaceOfTool(toolName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//sli.enterSlideDisplayName(sliDName);
		red.clickOnReaderDeleteButton(redFileName);		
		red.clickOnDeleteYesReader();
		
		String actRes = red.fetchReaderDeleteMsg();
		System.out.println(actRes);
		
		String redShows1 = red.fetchManageFaceName(redFileName);
		System.out.println(redShows1);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(redShows1.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking Content does not show on the manage face of the tool");
			System.out.println("The test case passed on the content not showing on the manage face of the tool.");
		}else{
			soft.assertTrue(false, "The test case failed on the content not showing on the manage face of the tool.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    
	    mi.SelectExitManageFaceOfTool(toolName);
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	    String redShows1a = red.fetchListFaceName(redFileName);
		System.out.println(redShows1a);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(redShows1a.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking Content does not show on the face of the tool");
			System.out.println("The test case passed on the content not showing on the face of the tool.");
		}else{
			soft.assertTrue(false, "The test case failed on the content not showing on the face of the tool.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    
		nt.refreshPage();
		/*try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		mi.fetchAppTool(appLocName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String redShows1ab = red.fetchListFaceName(redFileName);
		System.out.println(redShows1ab);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(redShows1ab.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking Content does not show on the face of the tool");
			System.out.println("The test case passed on the content not showing on the face of the tool.");
		}else{
			soft.assertTrue(false, "The test case failed on the content not showing on the face of the tool.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	    mi.SelectManageFaceOfTool(toolName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// This will determine if Value is still showing or not.
		String redShows = red.fetchManageFaceName(redFileName);
		System.out.println(redShows);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(redShows.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking Content does not show on the manage face of the tool");
			System.out.println("The test case passed on the content not showing on the manage face of the tool.");
		}else{
			soft.assertTrue(false, "The test case failed on the content not showing on the manage face of the tool.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    
		
		ExcelUtils.setCellData(redShows, i, 7, Constant.File_TestData_Reader);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		ExcelUtils.setCellData(actRes, i, 9, Constant.File_TestData_Reader);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 10,Constant.File_TestData_Reader);
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 10,Constant.File_TestData_Reader);
		}
		
		
		}
		soft.assertAll();
		}
}
