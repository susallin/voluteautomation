package education.volute.reader_testscripts;

import java.io.IOException;
import java.util.Iterator;

import org.testng.annotations.Test;
import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Landing;
import education.volute.pages.LoginPage;
//import education.volute.pages.ManageLearningSpaces;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.SocialHub;
import education.volute.pages.NavigatorTool;
import education.volute.pages.Reader;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.ToolDestAndCollections;

public class Reader_TC_01 extends SuperTestScript {

	//@Test
	public void testReader_TC_01(String Role) throws Exception		
	{
			//LoginPage lp = new LoginPage();
			NavigatorTool nt = new NavigatorTool();
			//SocialHub mj = new SocialHub();
			MenuItems mi = new MenuItems();
			ReadCsvFiles csv = new ReadCsvFiles();
			//Landing ld = new Landing();
			//ManagePrograms mp = new ManagePrograms();
			//ManageLearningSpaces mls = new ManageLearningSpaces();
			ToolDestAndCollections toolD = new ToolDestAndCollections();
			Reader red = new Reader();
		
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Reader, "AddReader3");
			
			for(int i=1; i<=ExcelUtils.getRowcount(); i++)
			{
			
			//String uName = ExcelUtils.getCellData(i, 1);
	        //String pwd = ExcelUtils.getCellData(i, 2);
			String toolDest = ExcelUtils.getCellData(i, 1);
	        String actName = ExcelUtils.getCellData(i, 2);
	        String subActName = ExcelUtils.getCellData(i, 3);
	        String pgmNameTxt = ExcelUtils.getCellData(i, 4);
	        String toolName = ExcelUtils.getCellData(i, 5);
	        //String pgmNameTxt = ExcelUtils.getCellData(i, 3);
	        //String lsToolName = ExcelUtils.getCellData(i, 4);
	        //String lsNameTxt = ExcelUtils.getCellData(i, 5);
	        //String lsDescTxt = ExcelUtils.getCellData(i, 6);
	        //String redDName = ExcelUtils.getCellData(i, 4);
	        String redFile = ExcelUtils.getCellData(i, 6);
	        //String docName = ExcelUtils.getCellData(i, 6);
	        //String docAuthor = ExcelUtils.getCellData(i, 7);
	        String uRole = ExcelUtils.getCellData(i, 10);
	        String appLocName = ExcelUtils.getCellData(i, 11);
	        String expRes = ExcelUtils.getCellData(i, 7);
			
			/*lp.login(uName, pwd);
			lp.logibutton();
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			nt.clickOnMainMenu();*/
	        toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
			/*mi.clickOnNavigation();
			mi.clickOnProgram();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			ld.clickOnLandingMP();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mp.clickOnselectedprogram(pgmNameTxt);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mp.clickOnManageMashupicon(pgmNameTxt);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/*mls.clickOnAddLS();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mls.selectaTool(lsToolName);
			mls.enterLSName(lsNameTxt);
			mls.enterLsDesc(lsDescTxt);
			mls.clickOnSaveButton();
			mls.clickOnConfigButton();*/
			/*nt.clickOnJourney();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mj.clickOnLaunchAct(actName);*/
			/*try {
				Thread.sleep(15000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			mi.fetchAppTool(appLocName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mi.SelectManageFaceOfTool(toolName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			Iterator<String[]> FileName = csv.ReadList(redFile);
						
			if(FileName.hasNext())
				FileName.next();
			
			while(FileName.hasNext()){
				int n = 0;
				String[] Name = FileName.next();
				boolean flag = true;
				
				String addRed = Name[n];
				String addBackRed = Name[n+1];
				String nameBackButton = Name[n+2];
				String File = Name[n+3];
				String docName = Name[n + 4];
				String docAuthor = Name[n+5];
				
				if(addRed.equalsIgnoreCase("yes")){
					mi.SelectAdd(toolName);
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				
				red.enterOnReaderDocName(docName);
				red.enterOnReaderAuthor(docAuthor);
				
				red.clickOnReaderSelectFileUploadButton(File);
				/*try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+File);*/
				while(flag){
					String result = nt.fetchProgressFile();
					
					if(result.equalsIgnoreCase("Found")){
						
						if(addBackRed.equalsIgnoreCase("yes")){
							mi.clickBackSaveListButton(nameBackButton);
							try {
								Thread.sleep(1000);
							} catch (InterruptedException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						} else{
							red.clickOnReaderSave();
							try {
								Thread.sleep(1000);
							} catch (InterruptedException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
						flag = false;
					}
					else{
						flag = true;
					}
				}
				//csv.writeCsv(FileName, date, setDate);
			}
			//mi.clearDisplayName();
			//mi.enterDisplayName(redDName);
			
			/*red.enterOnReaderDocName(docName);
			red.enterOnReaderAuthor(docAuthor);
			red.clickOnReaderSelectFileUploadButton();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+redFile);
			try {
				Thread.sleep(70000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}	
			red.clickOnReaderSave();
			
			
			red.enterOnReaderDocName("File2");
			red.enterOnReaderAuthor("Anderson B");
			red.clickOnReaderSelectFileUploadButton();//sliFilName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+"PIP_Fut;ure!_of_High;er_Ed!.pdf");
			//Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+"PIP_Fut;ure!_of_High;er_Ed!.pdf");
			try {
				Thread.sleep(50000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			red.clickOnReaderSave();*/
			
			
			String actRes = red.fetchReaderAddMsg();
			System.out.println(actRes);
			ExcelUtils.setCellData(actRes, i, 8, Constant.File_TestData_Reader);
			
			Boolean status = ValidationLib.verifyMsg(expRes, actRes);
			
			if(status)
			{
				ExcelUtils.setCellData("pass", i, 9,Constant.File_TestData_Reader);
			}
			else 
		{
				ExcelUtils.setCellData("Fail", i, 9,Constant.File_TestData_Reader);
			}
			
			
			}
			
	}
}
