package education.volute.reader_testscripts;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sikuli.script.FindFailed;

import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.ManageUsers;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import education.volute.pages.Reader;
import utilities.Constant;
import utilities.SearchElements;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class Reader_TC_04 {

	public void verifyInfo(String Role, String textShows) throws Exception
	{
		Reader red = new Reader();
		NavigatorTool nt = new NavigatorTool();
		MenuItems mi = new MenuItems();
		SoftAssertCheck soft = new SoftAssertCheck();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		SearchElements sr = new SearchElements();
		//DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy, HH:mm:ss a");
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Reader, textShows);
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
			//Date date = new Date();
			//String date1 = dateFormat.format(date);
			String redName = ExcelUtils.getCellData(i, 1);
			String redAuthor = ExcelUtils.getCellData(i, 2);
			String redModify = ExcelUtils.getCellData(i, 3);
			String redUser = ExcelUtils.getCellData(i, 4);
			String toolName = ExcelUtils.getCellData(i, 5);
			String toolDest = ExcelUtils.getCellData(i, 22);
		    String actName = ExcelUtils.getCellData(i, 23);
		    String subActName = ExcelUtils.getCellData(i, 24);
	        String pgmNameTxt = ExcelUtils.getCellData(i, 25);
	        String uRole = ExcelUtils.getCellData(i, 26);
	        String appLocName = ExcelUtils.getCellData(i, 27);
			
			String redShows = red.fetchManageFaceName(redName);
		    System.out.println(redShows);

			ExcelUtils.setCellData(redShows, i, 6, Constant.File_TestData_Reader);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();	
			}
			
			if(redShows.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking the name shows in Manage Face");
				System.out.println("The test case passed on Checking the name shows in Manage Face.");
			}else{
				soft.assertTrue(false, "The test case failed on Checking the name shows in Manage Face.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		    String redShows1 = red.fetchManageFaceName(redAuthor);
		    System.out.println(redShows1);

			ExcelUtils.setCellData(redShows1, i, 7, Constant.File_TestData_Reader);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();	
			}
			
			if(redShows1.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking the author shows in Manage Face");
				System.out.println("The test case passed on Checking the author shows in Manage Face.");
			}else{
				soft.assertTrue(false, "The test case failed on Checking the author shows in Manage Face.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    
		    String redShows2 = red.fetchManageFaceInsights(redName, redModify);
		    System.out.println(redShows2);

			ExcelUtils.setCellData(redShows2, i, 8, Constant.File_TestData_Reader);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();	
			}
			
			if(redShows2.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking the insight modify shows in Manage Face");
				System.out.println("The test case passed on Checking the insight modify shows in Manage Face.");
			}else{
				soft.assertTrue(false, "The test case failed on Checking the insight modify shows in Manage Face.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    String redShows3 = red.fetchManageFaceInsights(redName, redUser);
		    System.out.println(redShows3);

			ExcelUtils.setCellData(redShows3, i, 9, Constant.File_TestData_Reader);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();	
			}
			
			if(redShows3.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking the insight User shows in Manage Face");
				System.out.println("The test case passed on Checking the insight User shows in Manage Face.");
			}else{
				soft.assertTrue(false, "The test case failed on Checking the insight modify shows in Manage Face.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    String redShows4 = red.fetchManageFaceTime(redName,20);
		    System.out.println(redShows4);

			ExcelUtils.setCellData(redShows4, i, 10, Constant.File_TestData_Reader);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();	
			}
			
			if(redShows4.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking the insight Time shows in Manage Face");
				System.out.println("The test case passed on Checking the insight time shows in Manage Face.");
			}else{
				soft.assertTrue(false, "The test case failed on Checking the insight time shows in Manage Face.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    String redShows5 = red.fetchManageFaceImg(redName);
		    System.out.println(redShows5);

			ExcelUtils.setCellData(redShows5, i, 11, Constant.File_TestData_Reader);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();	
			}
			
			if(redShows5.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking the image shows in Manage Face");
				System.out.println("The test case passed on Checking the image shows in Manage Face.");
			}else{
				soft.assertTrue(false, "The test case failed on Checking the image shows in Manage Face.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    mi.selectBackArrow(toolName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			nt.refreshPage();
			try {
				Thread.sleep(15000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			/*red.clickOnReaderReturnToListButton();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
		    
		    String redShows6 = red.fetchListFaceImg(redName);
		    System.out.println(redShows6);

			ExcelUtils.setCellData(redShows6, i, 12, Constant.File_TestData_Reader);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();	
			}
			
			if(redShows6.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking the image shows in the List of face tool");
				System.out.println("The test case passed on Checking the image shows in the List of face tool.");
			}else{
				soft.assertTrue(false, "The test case failed on Checking the image shows in the List of face tool.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    
		    String redShows7 = red.fetchListFaceName(redName);
		    System.out.println(redShows7);

			ExcelUtils.setCellData(redShows7, i, 13, Constant.File_TestData_Reader);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();	
			}
			
			if(redShows7.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking the name shows in the List of face tool");
				System.out.println("The test case passed on Checking the name shows in the List of face tool.");
			}else{
				soft.assertTrue(false, "The test case failed on Checking the name shows in the List of face tool.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    
		    String redShows8 = red.fetchListFaceName(redAuthor);
		    System.out.println(redShows8);

			ExcelUtils.setCellData(redShows8, i, 14, Constant.File_TestData_Reader);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();	
			}
			
			if(redShows8.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking the author shows in the List of face tool");
				System.out.println("The test case passed on Checking the author shows in the List of face tool.");
			}else{
				soft.assertTrue(false, "The test case failed on Checking the author shows in the List of face tool.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	
		    
		    red.clickOnReaderContent(redName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    String redShows9 = red.fetchFaceImg(redName);
		    System.out.println(redShows9);

			ExcelUtils.setCellData(redShows9, i, 15, Constant.File_TestData_Reader);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();	
			}
			
			if(redShows9.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking the image shows in the face tool");
				System.out.println("The test case passed on Checking the image shows in the face tool.");
			}else{
				soft.assertTrue(false, "The test case failed on Checking the image shows in the face tool.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    String redShows10 = red.fetchFaceName(redName);
		    System.out.println(redShows10);

			ExcelUtils.setCellData(redShows10, i, 16, Constant.File_TestData_Reader);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();	
			}
			
			if(redShows10.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking the name shows in the face tool");
				System.out.println("The test case passed on Checking the name shows in the face tool.");
			}else{
				soft.assertTrue(false, "The test case failed on Checking the name shows in the face tool.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    
		    String redShows11 = red.fetchFaceName(redAuthor);
		    System.out.println(redShows11);

			ExcelUtils.setCellData(redShows11, i, 17, Constant.File_TestData_Reader);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();	
			}
			
			if(redShows11.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking the author shows in the face tool");
				System.out.println("The test case passed on Checking the author shows in the face tool.");
			}else{
				soft.assertTrue(false, "The test case failed on Checking the author shows in the face tool.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    String redShows12 = red.fetchFaceInsights(redModify);
		    System.out.println(redShows12);

			ExcelUtils.setCellData(redShows12, i, 18, Constant.File_TestData_Reader);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();	
			}
			
			if(redShows12.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking the insight modify shows in the face tool");
				System.out.println("The test case passed on Checking the inight modify shows in the face tool.");
			}else{
				soft.assertTrue(false, "The test case failed on Checking the insight modify shows in the face tool.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    String redShows13 = red.fetchFaceInsights(redUser);
		    System.out.println(redShows13);

			ExcelUtils.setCellData(redShows13, i, 19, Constant.File_TestData_Reader);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();	
			}
			
			if(redShows13.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking the insight User shows in the face tool");
				System.out.println("The test case passed on Checking the insight User shows in the face tool.");
			}else{
				soft.assertTrue(false, "The test case failed on Checking the insight User shows in the face tool.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    String redShows14 = red.fetchFaceTime(redName, 20);
		    System.out.println(redShows14);

			ExcelUtils.setCellData(redShows14, i, 20, Constant.File_TestData_Reader);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();	
			}
			
			if(redShows14.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking the insight time shows in the face tool");
				System.out.println("The test case passed on Checking the insight time shows in the face tool.");
			}else{
				soft.assertTrue(false, "The test case failed on Checking the insight time shows in the face tool.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    red.clickOnReaderLaunchButton();
			try {
				Thread.sleep(15000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    String redShows15 = sr.fetchImageShows("ReaderFullView.png");
		    System.out.println(redShows15);

			ExcelUtils.setCellData(redShows15, i, 21, Constant.File_TestData_Reader);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();	
			}
			
			if(redShows15.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking the FullView of a content.");
				System.out.println("The test case passed on Checking the FullView of a content.");
			}else{
				soft.assertTrue(false, "The test case failed on Checking the FullView of a content.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    red.clickOnReaderReturnToViewButton();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
			try {
				Thread.sleep(15000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mi.fetchAppTool(appLocName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			mi.SelectManageFaceOfTool(toolName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			
		}
		soft.assertAll();
	}
}
