package education.volute.reader_testscripts;

import java.io.IOException;
import java.util.Iterator;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Landing;
import education.volute.pages.LoginPage;
//import education.volute.pages.ManageLearningSpaces;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.SocialHub;
import education.volute.pages.NavigatorTool;
import education.volute.pages.Reader;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.ToolDestAndCollections;

public class Reader_TC_02 extends SuperTestScript {

	//@Test
	public void testReader_TC_02(String Role) throws Exception 
	
	{
		//LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		//SocialHub mj = new SocialHub();
		MenuItems mi = new MenuItems();
		//Landing ld = new Landing();
		//ManagePrograms mp = new ManagePrograms();
		//ManageLearningSpaces mls = new ManageLearningSpaces();
		ReadCsvFiles csv = new ReadCsvFiles();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		Reader red = new Reader();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Reader, "EditReader3");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		//String uName = ExcelUtils.getCellData(i, 1);
        //String pwd = ExcelUtils.getCellData(i, 2);
        //String actName = ExcelUtils.getCellData(i, 3);
        //String pgmNameTxt = ExcelUtils.getCellData(i, 3);
        //String lsNameTxt = ExcelUtils.getCellData(i, 4);
        //String lsNameTxt = ExcelUtils.getCellData(i, 5);
        //String lsDescTxt = ExcelUtils.getCellData(i, 6);
        //String sliDName = ExcelUtils.getCellData(i, 7);
		String toolDest = ExcelUtils.getCellData(i, 1);
	    String actName = ExcelUtils.getCellData(i, 2);
	    String subActName = ExcelUtils.getCellData(i, 3);
	    String pgmNameTxt = ExcelUtils.getCellData(i, 4);
	    String toolName = ExcelUtils.getCellData(i, 5);
        String redFileName = ExcelUtils.getCellData(i, 6);
        //String docName = ExcelUtils.getCellData(i, 6);
        //String docAuthor = ExcelUtils.getCellData(i, 7);
        String uRole = ExcelUtils.getCellData(i, 10);
        String appLocName = ExcelUtils.getCellData(i, 11);
        String expRes = ExcelUtils.getCellData(i, 7);
		
		/*lp.login(uName, pwd);
		lp.logibutton();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnMainMenu();*/
        toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
		/*mi.clickOnNavigation();
		mi.clickOnProgram();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ld.clickOnLandingMP();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnselectedprogram(pgmNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnManageMashupicon(pgmNameTxt);
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*mls.selectaLearningSpace(lsNameTxt);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mls.clickOnEditButton(lsNameTxt);
		mls.clickOnConfigButton();*/
		/*nt.clickOnJourney();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mj.clickOnLaunchAct(actName);*/
		/*try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		mi.fetchAppTool(appLocName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mi.SelectManageFaceOfTool(toolName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Iterator<String[]> FileName = csv.ReadList(redFileName);
		
		if(FileName.hasNext())
			FileName.next();
		
		while(FileName.hasNext()){
			int n = 0;
			String[] Name = FileName.next();
			
			String addBackRed = Name[n];
			String nameBackButton = Name[n+1];
			String redName = Name[n+2];
			String docName = Name[n+3];
			String docAuthor = Name[n+4];
			
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			red.clickOnReaderEditButton(redName);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			red.clickOnReaderClear();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			nt.clickOnDeletePgmYesButton();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			//red.ClearReaderDocName();
			red.enterOnReaderDocName(docName);
			//red.ClearReaderAuthor();
			red.enterOnReaderAuthor(docAuthor);
			
			if(addBackRed.equalsIgnoreCase("yes")){
				mi.clickBackSaveListButton(nameBackButton);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} else{
				red.clickOnReaderSave();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		
		
		//sli.enterSlideDisplayName(sliDName);
		/*red.clickOnReaderUploaded(redFileName);
		red.ClearReaderDocName();
		red.enterOnReaderDocName(docName);
		red.ClearReaderAuthor();
		red.enterOnReaderAuthor(docAuthor);
		
		red.clickOnReaderSave();*/
		
		String actRes = red.fetchReaderUpdatedMsg();
		System.out.println(actRes);
		ExcelUtils.setCellData(actRes, i, 8, Constant.File_TestData_Reader);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 9,Constant.File_TestData_Reader);
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 9,Constant.File_TestData_Reader);
		}
		
		
		}
		
		}
}
