package education.volute.calendar_testscripts;

public class CalendarTest {

	public void CalendarAdd(String Role) throws Exception{
		Calendar_TC_01 cal = new Calendar_TC_01();	
		cal.testCalendar_TC_01(Role);
	}
	
	public void CalendarEdit(String Role) throws Exception{
		Calendar_TC_02 cal = new Calendar_TC_02();	
		cal.testCalendar_TC_02(Role);
	}
	
	public void CalendarDelete(String Role){
		Calendar_TC_03 cal = new Calendar_TC_03();	
		cal.testCalendar_TC_03(Role);
	}
}
