package education.volute.calendar_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Landing;
import education.volute.pages.LoginPage;
//import education.volute.pages.ManageLearningSpaces;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import education.volute.pages.Calendar;
import utilities.Constant;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class Calendar_TC_03 extends SuperTestScript {

	//@Test
	public void testCalendar_TC_03(String Role) 
	
	{
		//LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		SoftAssertCheck soft = new SoftAssertCheck();
		MenuItems mi = new MenuItems();
		//Landing ld = new Landing();
		//ManagePrograms mp = new ManagePrograms();
		//ManageLearningSpaces mls = new ManageLearningSpaces();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		Calendar cal = new Calendar();
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Calendar, "DeleteCalendar2");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		//String uName = ExcelUtils.getCellData(i, 1);
        //String pwd = ExcelUtils.getCellData(i, 2);
		String toolDest = ExcelUtils.getCellData(i, 1);
	    String actName = ExcelUtils.getCellData(i, 2);
	    String subActName = ExcelUtils.getCellData(i, 3);
	    String pgmNameTxt = ExcelUtils.getCellData(i, 4);
	    String CalName = ExcelUtils.getCellData(i, 5);
        //String lsNameTxt = ExcelUtils.getCellData(i, 4);
        //String lsNameTxt = ExcelUtils.getCellData(i, 5);
        //String lsDescTxt = ExcelUtils.getCellData(i, 6);
        //String sliDName = ExcelUtils.getCellData(i, 7);
        //String vidName = ExcelUtils.getCellData(i, 5);
        //String vidTitle = ExcelUtils.getCellData(i, 6);
        //String sliSName = ExcelUtils.getCellData(i, 7);
        //String sliFilName = ExcelUtils.getCellData(i, 9);
	    String uRole = ExcelUtils.getCellData(i, 10);
	    String toolName = ExcelUtils.getCellData(i, 11);
	    String appLocName = ExcelUtils.getCellData(i, 12);
        String expRes = ExcelUtils.getCellData(i, 6);
		
		/*lp.login(uName, pwd);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnMainMenu();
		mi.clickOnNavigation();*/
        toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
		/*mi.clickOnProgram();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ld.clickOnLandingMP();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnselectedprogram(pgmNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnManageMashupicon(pgmNameTxt);
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*mls.selectaLearningSpace(lsNameTxt);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mls.clickOnEditButton(lsNameTxt);
		mls.clickOnConfigButton();*/
		/*try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		mi.fetchAppTool(appLocName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mi.SelectManageFaceOfTool(toolName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		cal.clickOnCalendarDeleteButton(CalName);
		//sli.enterSlideDisplayName(sliDName);
		cal.clickOnDeleteYesCal();
		
		String actRes = cal.fetchDeletedMsgCal();
		System.out.println(actRes);
		
		String actRes1 = cal.fetchFacesCalShows(CalName);
		System.out.println(actRes1);
		if(actRes1.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking if content shows after it was deleted");
			System.out.println("The test case passed, content does not show on Manage.");
		}else{
			soft.assertTrue(false, "The test case failed, content shows on Manage.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    
	    mi.SelectExitManageFaceOfTool(toolName);
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    
	    String actRes1a = cal.fetchListFacesCalShows(CalName);
		System.out.println(actRes1a);
		if(actRes1a.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking if content shows after it was deleted");
			System.out.println("The test case passed, content does not show on Face.");
		}else{
			soft.assertTrue(false, "The test case failed, content shows on Face.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		nt.refreshPage();
		/*try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		mi.fetchAppTool(appLocName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String actRes1ab = cal.fetchListFacesCalShows(CalName);
		System.out.println(actRes1ab);
		if(actRes1ab.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking if content shows after it was deleted");
			System.out.println("The test case passed, content does not show on Face.");
		}else{
			soft.assertTrue(false, "The test case failed, content shows on Face.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    
		mi.SelectManageFaceOfTool(toolName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// This will determine if Value is still showing or not.
		String calShows = cal.fetchFacesCalShows(CalName);
		System.out.println(calShows);
		if(calShows.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking if content shows after it was deleted");
			System.out.println("The test case passed, content does not show on Manage.");
		}else{
			soft.assertTrue(false, "The test case failed, content shows on Manage.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    
		ExcelUtils.setCellData(calShows, i, 7, Constant.File_TestData_Calendar);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ExcelUtils.setCellData(actRes, i, 8, Constant.File_TestData_Calendar);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 9,Constant.File_TestData_Calendar);
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 9,Constant.File_TestData_Calendar);
		}
		
		
		}
		
		}
}
