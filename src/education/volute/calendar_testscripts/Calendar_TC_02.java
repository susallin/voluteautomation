package education.volute.calendar_testscripts;


import java.util.Iterator;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Calendar;
import education.volute.pages.Landing;
import education.volute.pages.LoginPage;
//import education.volute.pages.ManageLearningSpaces;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.ToolDestAndCollections;

public class Calendar_TC_02 extends SuperTestScript {

	//@Test
	public void testCalendar_TC_02(String Role) throws Exception
	
	{
		//LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		MenuItems mi = new MenuItems();
		//Landing ld = new Landing();
		//ManagePrograms mp = new ManagePrograms();
		ReadCsvFiles csv = new ReadCsvFiles();
		//ManageLearningSpaces mls = new ManageLearningSpaces();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		Calendar cal = new Calendar();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Calendar, "EditCalendar2");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		//String uName = ExcelUtils.getCellData(i, 1);
        //String pwd = ExcelUtils.getCellData(i, 2);
		String toolDest = ExcelUtils.getCellData(i, 1);
	    String actName = ExcelUtils.getCellData(i, 2);
	    String subActName = ExcelUtils.getCellData(i, 3);
	    String pgmNameTxt = ExcelUtils.getCellData(i, 4);
        String toolName = ExcelUtils.getCellData(i, 5);
        String calEvents = ExcelUtils.getCellData(i, 6);   
        //String eventSaved = ExcelUtils.getCellData(i, 5);
        //String eventName = ExcelUtils.getCellData(i, 5);
        //String eventDesc = ExcelUtils.getCellData(i, 6);
        //String newStart = ExcelUtils.getCellData(i, 7);
        //String newEnd = ExcelUtils.getCellData(i, 8);
        //String sliSName = ExcelUtils.getCellData(i, 7);
        //String sliFilName = ExcelUtils.getCellData(i, 9);
        String uRole = ExcelUtils.getCellData(i, 10);
        String appLocName = ExcelUtils.getCellData(i, 11);
        String expRes = ExcelUtils.getCellData(i, 7);
		
		/*lp.login(uName, pwd);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnMainMenu();
		mi.clickOnNavigation();*/
        toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
		/*mi.clickOnProgram();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ld.clickOnLandingMP();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnselectedprogram(pgmNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnManageMashupicon(pgmNameTxt);
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*mls.selectaLearningSpace(lsNameTxt);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mls.clickOnEditButton(lsNameTxt);
		mls.clickOnConfigButton();*/
		/*try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		mi.fetchAppTool(appLocName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mi.SelectManageFaceOfTool(toolName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Iterator<String[]> FileName = csv.ReadList(calEvents);
		
		if(FileName.hasNext())
			FileName.next();
		
		while(FileName.hasNext()){
			int n = 0;
			String[] Name = FileName.next();
			
			String addBackCal = Name[n];
			String nameBackButton = Name[n+1];
			String EventName = Name[n+2];
			String NewEventName = Name[n+3];
			String EventDesc = Name[n+4];
			String EventStartDate = Name[n + 5];
			String EventEndDate = Name[n+6];
			String EventStartTimeHour = Name[n+7];
			String EventStartTimeMin = Name[n+8];
			String EventStartTimeAmPm = Name[n+9];
			String EventEndTimeHour = Name[n+10];
			String EventEndTimeMin = Name[n+11];
			String EventEndTimeAmPm = Name[n+12];
			
			cal.clickOnCalendarSaved(EventName);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			cal.clickOnCalCancel();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			nt.clickOnDeletePgmYesButton();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			//cal.clearCalEventName();
			cal.enterCalEventName(NewEventName);
			//cal.clearCalDescription();
			cal.enterCalDescription(EventDesc);
			
			cal.selectCalStartDateBox();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			//cal.enterTodayCalStartDate(EventStartDate);
			cal.selectTodayCalStartDate(EventStartDate);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			cal.selectCalEndDateBox();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			//cal.enterCalEndDate(EventEndDate);
			cal.selectCalEndDate(EventEndDate);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			if(!EventStartTimeHour.isEmpty()||!EventEndTimeHour.isEmpty()){
				cal.enterCalHourStartTimeTextBox(EventStartTimeHour);
				
				cal.enterCalMinStartTimeTextBox(EventStartTimeMin);
				
				cal.clickCalDropdownButtonStartTimeAmPm();
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				cal.clickCalStartTimeAMPM(EventStartTimeAmPm);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				cal.enterCalHourEndTimeTextBox(EventEndTimeHour);
				
				cal.enterCalMinEndTimeTextBox(EventEndTimeMin);
				
				cal.clickCalDropdownButtonEndTimeAmPm();
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				cal.clickCalEndTimeAMPM(EventEndTimeAmPm);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} else
			{
				//Do nothing
			}
			
			cal.selectCalClearEndDate();
			
			if(addBackCal.equalsIgnoreCase("yes")){
				mi.clickBackSaveListButton(nameBackButton);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} else{
				cal.clickOnCalSave();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		}
		
		/*cal.clickOnCalendarSaved(eventSaved);
		cal.enterCalEventName(""+eventName);
		cal.enterCalDescription(""+eventDesc);
		//sli.enterSlideDisplayName(sliDName);
		//cal.selectCalStartDateBox();
		//cal.selectTodayCalStartDate(newStart);
		cal.selectCalEndDateBox();
		cal.selectCalEndDate(newEnd);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		cal.clickOnCalSave();*/
		
		String actRes = cal.fetchAddedMsgCal();
		System.out.println(actRes);
		ExcelUtils.setCellData(actRes, i, 8, Constant.File_TestData_Calendar);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 9,Constant.File_TestData_Calendar);
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 9,Constant.File_TestData_Calendar);
		}
		
		
		}
		
		}
}
