package education.volute.login_testscripts;

//import java.util.concurrent.TimeUnit;

//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.support.FindBy;
//import org.testng.annotations.Test;
//import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.SocialHub;
import utilities.Constant;
//import utilities.SoftAssertCheck;
import utilities.SoftAssertCheck;

public class Login_TC_01 //extends SuperTestScript 
{
	//@Test
	public void testLogin_TC_01(String lg)
	{
		LoginPage lp = new LoginPage();
		SoftAssertCheck soft = new SoftAssertCheck();
		SocialHub sh = new SocialHub();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_LoginPage, lg);
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
			String uName = ExcelUtils.getCellData(i, 1);
	        String pwd = ExcelUtils.getCellData(i, 2);
	        String expRes = ExcelUtils.getCellData(i, 3);
	        boolean flag = true;
	        
	        lp.login(uName, pwd); 
	        lp.logibutton();
	        
	        while(flag){
				String result = sh.fetchNoticePage();
				
				if(result.equalsIgnoreCase("Found")){
					String actRes = lp.fetchLogPage();
					System.out.println(actRes);
					ExcelUtils.setCellData(actRes, i, 4, Constant.File_TestData_LoginPage);
					
					Boolean status = ValidationLib.verifyMsg(expRes, actRes);
					
					if(status)
					{
						ExcelUtils.setCellData("pass", i, 5,Constant.File_TestData_LoginPage);
						soft.assertTrue(true, "Login Success");
						System.out.println("The test case passed on verifying Login.");
					}
					else 
				{
						ExcelUtils.setCellData("Fail", i, 5,Constant.File_TestData_LoginPage);
						soft.assertTrue(false, "The test case failed on verifying Login.\n");
					}
					
					flag = false;
				}
				else{
					flag = true;
				}
			}
		}
		soft.assertAll();
		
	}
	
	
	//@Test
	public void LoginJmeter()
	{
		//WebDriver driver;
		System.setProperty("webdriver.chrome.driver", "./exe_files/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		//driver.manage().window().maximize();
		
		//driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		driver.get("https://qa.volute.info/login");
		//LoginPage lp = new LoginPage();
		
		//ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_LoginPage, "Login");
		
		//for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		//{
	        
	        int un_size = driver.findElements(By.id("username")).size();
			driver.findElements(By.id("username")).get(un_size-1).sendKeys("newuser@mailinator.com");
			
			int pwd_size = driver.findElements(By.id("password")).size();
			driver.findElements(By.id("password")).get(pwd_size-1).sendKeys("Apr096075-+");
			
	        //lp.login(uName, pwd); 
	        //lp.logibutton();
			
			//WebDriverWait wait = new WebDriverWait(driver, 100);
			//wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#supportedBrowser > form > div.loginPage > #login-button")));
			driver.findElement(By.cssSelector("#supportedBrowser > form > div.loginPage > #login-button")).click();
	        
	        /*try {
				Thread.sleep(16000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
	        
	        //String actRes = lp.fetchLogPage();
	        try {
				driver.findElement(By.cssSelector("volute-container[id='main-volute-content']"));
				System.out.println("Pass");
			}
			catch(NoSuchElementException e){
				System.out.println("Fail");
			}
	        
	        driver.quit();
		//}
		
		
	}
}
