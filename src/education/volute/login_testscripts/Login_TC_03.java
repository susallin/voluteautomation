package education.volute.login_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import utilities.Constant;
import utilities.SoftAssertCheck;

public class Login_TC_03 extends SuperTestScript{

	//@Test
	public void testLogin_TC_03()
	{
		LoginPage lp = new LoginPage();
		SoftAssertCheck soft = new SoftAssertCheck();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_LoginPage, "InvalidPassword");
		
		String uName = ExcelUtils.getCellData(1, 1);
        String pwd = ExcelUtils.getCellData(1, 2);
        String expRes = ExcelUtils.getCellData(1, 3);
        
        lp.login(uName, pwd); 
        lp.logibutton();
        try {
			Thread.sleep(16000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
        String actRes = lp.fetchWrongPassword();
		System.out.println(actRes);
		ExcelUtils.setCellData(actRes, 1, 4, Constant.File_TestData_LoginPage);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", 1, 5,Constant.File_TestData_LoginPage);
			soft.assertTrue(true, "Invalid Password");
			System.out.println("The test case passed on verifying Invalid Password.");
		}
		else 
	{
			ExcelUtils.setCellData("Fail", 1, 5,Constant.File_TestData_LoginPage);
			soft.assertTrue(false, "The test case failed on verifying Invalid Password.\n");
		}
		soft.assertAll();
		
	}
}
