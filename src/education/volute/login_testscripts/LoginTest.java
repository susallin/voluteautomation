package education.volute.login_testscripts;


public class LoginTest {

	public void Login(String lgName){
		Login_TC_01 lg = new Login_TC_01();	
		lg.testLogin_TC_01(lgName);
	}
	
	public void InvalidEmail(){
		Login_TC_02 lg = new Login_TC_02();	
		lg.testLogin_TC_02();
	}
	
	public void InvalidPassword(){
		Login_TC_03 lg = new Login_TC_03();	
		lg.testLogin_TC_03();
	}
	
	public void EmailConfirmation(){
		Login_TC_04 lg = new Login_TC_04();	
		lg.testLogin_TC_04();
	}
	
	public void EmailInvalid(){
		Login_TC_05 lg = new Login_TC_05();	
		lg.testLogin_TC_05();
	}
	
	public void Logout(){
		Logout lg = new Logout();	
		lg.testLogout();
	}
	
	public void ExpirePw(){
		Login_TC_06 lg = new Login_TC_06();	
		lg.testLogin_TC_06();
	}
}
