package education.volute.login_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.SocialHub;
import utilities.Constant;
import utilities.SoftAssertCheck;

public class Login_TC_06 {

	@Test
		public void testLogin_TC_06()
		{
			LoginPage lp = new LoginPage();
			//SoftAssertCheck soft = new SoftAssertCheck();
			SocialHub sh = new SocialHub();
			
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_LoginPage, "ExpirePw");
			
			for(int i=1; i<=ExcelUtils.getRowcount(); i++)
			{
				String uName = ExcelUtils.getCellData(i, 1);
		        String pwd = ExcelUtils.getCellData(i, 2);
		        	        
		        lp.login(uName, pwd); 
		        lp.logibutton();
		        
		        lp.pwExpire(pwd, pwd);
		        lp.expireButton();
		        
			}
			
		}
}
