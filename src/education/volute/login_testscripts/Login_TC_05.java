package education.volute.login_testscripts;

import java.util.Set;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import utilities.Constant;
import utilities.SoftAssertCheck;

public class Login_TC_05 extends SuperTestScript {

	//@Test
	public void testLogin_TC_05()
	{
		LoginPage lp = new LoginPage();
		
		SoftAssertCheck soft = new SoftAssertCheck();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_LoginPage, "EmailInvalid");
		
		String email = ExcelUtils.getCellData(1, 1);
        String expRes = ExcelUtils.getCellData(1, 2);
        
        String parent = driver.getWindowHandle();
        
        lp.forgotPwButton();
        
        lp.switchWindow(parent);
        
        lp.fetchForgotPw();
        
        lp.enterOnemailTextBox(email);
        try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        lp.submitButton();
        try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
        String actRes = lp.fetchWrongEmailPwReset();
		System.out.println(actRes);
		
		driver.close();
		
		//switch back to main window using this code
		driver.switchTo().window(parent);
		
		ExcelUtils.setCellData(actRes, 1, 3, Constant.File_TestData_LoginPage);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", 1, 4,Constant.File_TestData_LoginPage);
			soft.assertTrue(true, "Email Invalid");
			System.out.println("The test case passed on verifying Email Invalid.");
		}
		else 
	{
			ExcelUtils.setCellData("Fail", 1, 4,Constant.File_TestData_LoginPage);
			soft.assertTrue(false, "The test case failed on verifying Email Invalid.\n");
		}		
		soft.assertAll();
		
	}
}
