package education.volute.login_testscripts;

import org.junit.Test;
//import org.testng.annotations.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Login_Jmeter {

	public static WebDriver driver;
	
	@Test
	public void LoginJmeter()
	{
		//WebDriver driver;
		/*System.setProperty("webdriver.chrome.driver", "./exe_files/chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
        options.addArguments("headless");
        options.addArguments("window-size=1200x600");
		driver = new ChromeDriver(options);*/
		System.setProperty("webdriver.chrome.driver", "./exe_files/chromedriver.exe");
		driver = new ChromeDriver();
		//driver.manage().window().maximize();
		
		//driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		driver.get("https://staging.volute.info/login");
		//LoginPage lp = new LoginPage();
		
		//ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_LoginPage, "Login");
		
		//for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		//{
		WebDriverWait wait = new WebDriverWait(driver, 10);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
	        WebElement email = driver.findElement(By.id("username"));
	        email.sendKeys("newuser@mailinator.com");
	        
	    wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("password")));
	        WebElement password = driver.findElement(By.id("password"));
	        password.sendKeys("Apr096075-+");
	        
//	        int un_size = driver.findElements(By.id("username")).size();
//			driver.findElements(By.id("username")).get(un_size-1).sendKeys("newuser@mailinator.com");
//			
//			int pwd_size = driver.findElements(By.id("password")).size();
//			driver.findElements(By.id("password")).get(pwd_size-1).sendKeys("Apr096075-+");
			
	        //lp.login(uName, pwd); 
	        //lp.logibutton();
			
			//WebDriverWait wait = new WebDriverWait(driver, 100);
			//wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#supportedBrowser > form > div.loginPage > #login-button")));
			driver.findElement(By.cssSelector("#login-button")).click();
	        
			
			
        	wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("alerts-widget div[aria-title='Notice']")));
	        /*try {
				Thread.sleep(16000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
	        
	        //String actRes = lp.fetchLogPage();
	        try {
				driver.findElement(By.cssSelector("alerts-widget div[aria-title='Notice']"));
				System.out.println("Pass");
			}
			catch(NoSuchElementException e){
				System.out.println("Fail");
			}
	        
	        driver.quit();
		//}
		
		
	}
	
	//@Test
	/*public void RefreshPageAfterLogin()
	{
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		driver.navigate().refresh();
		/*try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		/*try {
			driver.findElement(By.cssSelector("volute-container[id='main-volute-content']"));
			System.out.println("Pass");
		}
		catch(NoSuchElementException e){
			System.out.println("Fail");
		}
	}*/
}
