package education.volute.login_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.SoftAssertCheck;

public class Logout extends SuperTestScript {

	//@Test
	public void testLogout()
	{
		LoginPage lp = new LoginPage();
		MenuItems mi = new MenuItems();
		NavigatorTool nt = new NavigatorTool();
		SoftAssertCheck soft = new SoftAssertCheck();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_LoginPage, "Logout");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
			String expRes = ExcelUtils.getCellData(i, 1);
			
			nt.clickOnPortfolioIcon();
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mi.clickOnLogout();
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			/*try {
				Thread.sleep(16000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
	        
	        String actRes = lp.fetchLogOffPage();
			System.out.println(actRes);
			ExcelUtils.setCellData(actRes, 1, 2, Constant.File_TestData_LoginPage);
			
			Boolean status = ValidationLib.verifyMsg(expRes, actRes);
			
			if(status)
			{
				ExcelUtils.setCellData("pass", 1, 3,Constant.File_TestData_LoginPage);
				soft.assertTrue(true, "Logout");
				System.out.println("The test case passed on verifying Logout.");
			}
			else 
		{
				ExcelUtils.setCellData("Fail", 1, 3,Constant.File_TestData_LoginPage);
				soft.assertTrue(false, "The test case failed on verifying Logout.\n");
			}

		
		}
		soft.assertAll();
		
	}
}
