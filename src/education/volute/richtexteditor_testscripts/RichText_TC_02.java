package education.volute.richtexteditor_testscripts;

import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import education.volute.pages.RichTextEditor;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class RichText_TC_02 {

	//@Test
		public void testRichText_TC_02(String Role) 	
		{
				//LoginPage lp = new LoginPage();
				NavigatorTool nt = new NavigatorTool();
				//SocialHub mj = new SocialHub();
				ReadCsvFiles csv = new ReadCsvFiles();
				MenuItems mi = new MenuItems();
				//Landing ld = new Landing();
				//ManagePrograms mp = new ManagePrograms();
				//ManageLearningSpaces mls = new ManageLearningSpaces();
				ToolDestAndCollections toolD = new ToolDestAndCollections();
				RichTextEditor ric = new RichTextEditor();		
				SoftAssertCheck soft = new SoftAssertCheck();
				
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_RichTextEditor, "ClearRichText");
				
				for(int i=1; i<=ExcelUtils.getRowcount(); i++)
				{
				
				//String uName = ExcelUtils.getCellData(i, 1);
		        //String pwd = ExcelUtils.getCellData(i, 2);
		        //String pgmNameTxt = ExcelUtils.getCellData(i, 3);
		        //String lsToolName = ExcelUtils.getCellData(i, 4);
		        //String lsNameTxt = ExcelUtils.getCellData(i, 5);
		        //String lsDescTxt = ExcelUtils.getCellData(i, 6);
				String toolDest = ExcelUtils.getCellData(i, 1);
		        String actName = ExcelUtils.getCellData(i, 2);
		        String subActName = ExcelUtils.getCellData(i, 3);
		        String pgmNameTxt = ExcelUtils.getCellData(i, 4);
		        String toolName = ExcelUtils.getCellData(i, 5);
		        //String ricDName = ExcelUtils.getCellData(i, 6);
		        String richWordTxt = ExcelUtils.getCellData(i, 6);
		        //String vidURL2 = ExcelUtils.getCellData(i, 9);
		        String uRole = ExcelUtils.getCellData(i, 7);
		        String appLocName = ExcelUtils.getCellData(i, 8);
				
				/*lp.login(uName, pwd);
				lp.logibutton();
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				nt.clickOnMainMenu();*/
				toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
				/*mi.clickOnNavigation();
				mi.clickOnProgram();
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				ld.clickOnLandingMP();
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				mp.clickOnselectedprogram(pgmNameTxt);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				mp.clickOnManageMashupicon(pgmNameTxt);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				/*mls.clickOnAddLS();
				mls.selectaTool(lsToolName);
				mls.enterLSName(lsNameTxt);
				mls.enterLsDesc(lsDescTxt);
				mls.clickOnSaveButton();
				mls.clickOnConfigButton();*/
				/*nt.clickOnJourney();
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				mj.clickOnLaunchAct(actName);*/
				/*try {
					Thread.sleep(15000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}*/
				mi.fetchAppTool(appLocName);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				mi.SelectManageFaceOfTool(toolName);
				//ric.clickOnRichManage();
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				//mi.clearDisplayName();
				//mi.enterDisplayName(ricDName);
				
				ric.clickOnClearRich();
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				nt.clickOnDeletePgmYesButton();
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				String actRes = ric.fetchManFaceShows(richWordTxt);
				System.out.println(actRes);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				if(actRes.equalsIgnoreCase("Not Found")){
					soft.assertTrue(true, "Checking Content does not show on the manage face of the tool");
					System.out.println("The test case passed on the content not showing on the manage face of the tool.");
				}else{
					soft.assertTrue(false, "The test case failed on the content not showing on the manage face of the tool.\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    mi.SelectExitManageFaceOfTool(toolName);
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    String actRes1 = ric.fetchFaceShows();
				System.out.println(actRes1);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				if(actRes1.equalsIgnoreCase("Not Found")){
					soft.assertTrue(true, "Checking content does not show");
					System.out.println("The test case passed, the content does not show");
				}else{
					soft.assertTrue(false, "The test case failed, the content shows\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				nt.refreshPage();
				
				mi.fetchAppTool(appLocName);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				String actRes2 = ric.fetchFaceShows();
				System.out.println(actRes2);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				if(actRes2.equalsIgnoreCase("Not Found")){
					soft.assertTrue(true, "Checking content does not show");
					System.out.println("The test case passed, the content does not show");
				}else{
					soft.assertTrue(false, "The test case failed, the content shows\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    mi.SelectManageFaceOfTool(toolName);
				//ric.clickOnRichManage();
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				String actRes3 = ric.fetchManFaceShows(richWordTxt);
				System.out.println(actRes3);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				if(actRes3.equalsIgnoreCase("Not Found")){
					soft.assertTrue(true, "Checking Content does not show on the manage face of the tool");
					System.out.println("The test case passed on the content not showing on the manage face of the tool.");
				}else{
					soft.assertTrue(false, "The test case failed on the content not showing on the manage face of the tool.\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				}
				soft.assertAll();
			}
}
