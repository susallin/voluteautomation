package education.volute.richtexteditor_testscripts;

public class RichTextTest {

	public void RichTextAdd(String Role) {
		RichText_TC_01 ts = new RichText_TC_01();	
		ts.testRichText_TC_01(Role);
	}
	
	public void RichTextClear(String Role) {
		RichText_TC_02 ts = new RichText_TC_02();	
		ts.testRichText_TC_02(Role);
	}
}
