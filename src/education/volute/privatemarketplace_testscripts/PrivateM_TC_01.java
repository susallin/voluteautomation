package education.volute.privatemarketplace_testscripts;


import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import education.volute.pages.PrivateMarketplace;
import utilities.Constant;
import utilities.SoftAssertCheck;

public class PrivateM_TC_01 {

	//@Test
		public void testPrivateM_TC_01(String str) 
		
		{
			NavigatorTool nt = new NavigatorTool();
			MenuItems mi = new MenuItems();
			SoftAssertCheck soft = new SoftAssertCheck();
			PrivateMarketplace pm = new PrivateMarketplace();
			
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_PrivateMarketplace, str);
			
			for(int i=1; i<=ExcelUtils.getRowcount(); i++)
			{
			
			String toolName = ExcelUtils.getCellData(i, 1);
			String roleFetName = ExcelUtils.getCellData(i, 2);
	        String fetName = ExcelUtils.getCellData(i, 3);
	        String n = ExcelUtils.getCellData(i, 4);
	        String exRes = ExcelUtils.getCellData(i, 5); 
			
	        
	        nt.clickOnAdmin();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			mi.clickOnMarketplaceMenu();
			try {
				Thread.sleep(7000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			pm.clickOnPmTab();
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			pm.clickToolMarket(toolName);
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			if(roleFetName.equalsIgnoreCase("Role")){
				pm.clickOnRoleAcTab();
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}else{
				pm.clickOnFetAcTab();
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			//Feature+Role
			pm.clickCheckRoleFetAccess(fetName, n);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String actRes = pm.fetchComfirmationMsg();
			System.out.println(actRes);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			if(actRes.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking the toast 'Change Success' appears");
				System.out.println("The test case passed, the toast(Change Success) appears.");
			}else{
				soft.assertTrue(false, "The test case failed, the toast(Change Success) does not appear.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    String pmShows = pm.fetchCheckbox(fetName, n);
			System.out.println(pmShows);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			if(pmShows.equalsIgnoreCase(exRes)){
				soft.assertTrue(true, "Checking Content does show as expected");
				System.out.println("The test case passed, the content shows expected");
			}else{
				soft.assertTrue(false, "The test case failed, the content does not show as expected.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    pm.clickOnBackToListButton();
		    try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    nt.refreshPage();
			try {
				Thread.sleep(15000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			pm.clickToolMarket(toolName);
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			if(roleFetName.equalsIgnoreCase("Role")){
				pm.clickOnRoleAcTab();
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}else{
				pm.clickOnFetAcTab();
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			String pmShows1 = pm.fetchCheckbox(fetName, n);
			System.out.println(pmShows1);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			if(pmShows1.equalsIgnoreCase(exRes)){
				soft.assertTrue(true, "Checking Content does show as expected");
				System.out.println("The test case passed, the content shows expected");
			}else{
				soft.assertTrue(false, "The test case failed, the content does not show as expected.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			
			}
			soft.assertAll();
		}
}
