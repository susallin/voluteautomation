package education.volute.portal_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Landing;
import education.volute.pages.LoginPage;
//import education.volute.pages.ManageLearningSpaces;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import education.volute.pages.Portal;
import education.volute.pages.SocialHub;
import utilities.Constant;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class Portal_TC_01 extends SuperTestScript {
	
	//@Test
	public void testPortal_TC_01(String Role) 	
	{
		//LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		MenuItems mi = new MenuItems();
		SoftAssertCheck soft = new SoftAssertCheck();
		//Landing ld = new Landing();
		//ManagePrograms mp = new ManagePrograms();
		//ManageLearningSpaces mls = new ManageLearningSpaces();
		//SocialHub mj = new SocialHub();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		Portal por = new Portal();
		
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Portal, "AddPortal3");
			
			for(int i=1; i<=ExcelUtils.getRowcount(); i++)
			{
			
			//String uName = ExcelUtils.getCellData(i, 1);
	        //String pwd = ExcelUtils.getCellData(i, 2);
			String toolDest = ExcelUtils.getCellData(i, 1);
	        String actName = ExcelUtils.getCellData(i, 2);
	        String subActName = ExcelUtils.getCellData(i, 3);
	        String pgmNameTxt = ExcelUtils.getCellData(i, 4);
	        String toolName = ExcelUtils.getCellData(i, 5);
	        //String lsToolName = ExcelUtils.getCellData(i, 4);
	        //String lsNameTxt = ExcelUtils.getCellData(i, 5);
	        //String lsDescTxt = ExcelUtils.getCellData(i, 6);
	        //String porDName = ExcelUtils.getCellData(i, 4);
	        String porURL = ExcelUtils.getCellData(i, 6);
	        //String vidURL2 = ExcelUtils.getCellData(i, 9);
	        String uRole = ExcelUtils.getCellData(i, 11);
	        String appLocName = ExcelUtils.getCellData(i, 12);
	        String expRes = ExcelUtils.getCellData(i, 7);
			
			/*lp.login(uName, pwd);
			lp.logibutton();
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			nt.clickOnMainMenu();*/
	        toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
			/*mi.clickOnNavigation();
			mi.clickOnProgram();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			ld.clickOnLandingMP();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mp.clickOnselectedprogram(pgmNameTxt);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mp.clickOnManageMashupicon(pgmNameTxt);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/*mls.clickOnAddLS();
			mls.selectaTool(lsToolName);
			mls.enterLSName(lsNameTxt);
			mls.enterLsDesc(lsDescTxt);
			mls.clickOnSaveButton();
			mls.clickOnConfigButton();*/
			/*try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mj.clickOnLaunchAct(actName);*/
			/*try {
				Thread.sleep(15000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			mi.fetchAppTool(appLocName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mi.SelectManageFaceOfTool(toolName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			//mi.clearDisplayName();
			//mi.enterDisplayName(porDName);
			por.enterPortalURL(porURL);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			por.clickOnPortalSave();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			por.clickOnPortalSave();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String actRes = por.fetchPortalAddMsg();
			System.out.println(actRes);
			if(actRes.equalsIgnoreCase("Portal saved successfully.")){
				soft.assertTrue(true, "Checking if toast shows/appears");
				System.out.println("The test case passed, toast shows.");
			}else{
				soft.assertTrue(false, "The test case failed, toast does not show.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    mi.SelectExitManageFaceOfTool(toolName);
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    String porShows1 = por.fetchFacesPortalShows(porURL);
			System.out.println(porShows1);
			if(porShows1.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking url shows/appears on the face of the tool");
				System.out.println("The test case passed, url shows on the face of the tool.");
			}else{
				soft.assertTrue(false, "The test case failed, url does not show on the face of the tool.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			nt.refreshPage();
			/*try {
				Thread.sleep(15000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			mi.fetchAppTool(appLocName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			// This will determine if Value is still showing or not.
			String porShows = por.fetchFacesPortalShows(porURL);
			System.out.println(porShows);
			if(porShows.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking url shows/appears on the face of the tool");
				System.out.println("The test case passed, url shows on the face of the tool.");
			}else{
				soft.assertTrue(false, "The test case failed, url does not show on the face of the tool.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			ExcelUtils.setCellData(porShows, i, 8, Constant.File_TestData_Portal);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			ExcelUtils.setCellData(actRes, i, 9, Constant.File_TestData_Portal);
			
			Boolean status = ValidationLib.verifyMsg(expRes, actRes);
			
			if(status)
			{
				ExcelUtils.setCellData("pass", i, 10,Constant.File_TestData_Portal);
			}
			else 
		{
				ExcelUtils.setCellData("Fail", i, 10,Constant.File_TestData_Portal);
			}
			
			
			}
			
		}
}
