package education.volute.activitysubs_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.NavigatorTool;
import education.volute.pages.RoomsTool;
import education.volute.pages.MenuItems;
import utilities.Constant;

public class LaunchActivity_TC_01 extends SuperTestScript

{
	@Test
	public void testLaunchActivity_TC_01(String Actname) 
	{
				
		//ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePrograms, "ActName");
		
		//for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		//{
		
		//String uName = ExcelUtils.getCellData(i, 1);
        //String pwd = ExcelUtils.getCellData(i, 2);
        //String roomNameTxt = ExcelUtils.getCellData(i, 3);
        String expRes = ExcelUtils.getCellData(i, 4);
                
     
	LoginPage lp = new LoginPage();
	NavigatorTool nt = new NavigatorTool();
	RoomsTool rt = new RoomsTool();
	MenuItems mi = new MenuItems();
	
	lp.login(uName, pwd);
	nt.clickOnMainMenu();
	mi.clickOnNavigation();
	mi.clickOnRoomS();
	rt.clickOnLaunchOfaRoom(roomNameTxt);
	
	
	String actRes = rt.fetchroomHeader();
	System.out.println(actRes);

	 ExcelUtils.setCellData(actRes, i, 5, Constant.File_TestData_Rooms);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 6, Constant.File_TestData_Rooms);
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 6, Constant.File_TestData_Rooms);
		}
		
		
		//}
		
		}

}
	
	
	
	

