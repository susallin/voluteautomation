/*package education.volute.rooms_testscripts;

import org.testng.annotations.Test;

//import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.NavigatorTool;
import education.volute.pages.MyJourney;
//import education.volute.pages.ManageLearningSpaces;
import education.volute.pages.Slides;
import education.volute.pages.RoomsTool;
import utilities.Constant;

public class ShareToolRoom_TC_02 {
	
	@Test
	public void testShareToolRoom_TC_02() 
	
	{
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Rooms, "TC_02");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String uName = ExcelUtils.getCellData(i, 1);
        String pwd = ExcelUtils.getCellData(i, 2);
		String toolNamExpRes = ExcelUtils.getCellData(i, 3);
		
		LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		MyJourney mj = new MyJourney();
		//ManageLearningSpaces mls = new ManageLearningSpaces();
		Slides sli = new Slides();
		RoomsTool rt = new RoomsTool();
		
		lp.login(uName, pwd);
		
		nt.clickOnHomeIcon();
		mj.clickOnActivity();
		mj.clickOnLaunch();
		
		sli.clickOnSlidesShare();
		
		rt.clickOnRoom();
		rt.clickOnAssignRoom();
		rt.clickOnRoom();
		rt.launchOnRoom();
		
		String actRes = sli.fetchSlidesHeader();
		System.out.println(actRes);
		ExcelUtils.setCellData(actRes, i, 4, Constant.File_TestData_Rooms);
		
		Boolean status = ValidationLib.verifyMsg(toolNamExpRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 5,Constant.File_TestData_Rooms);
		}
		else 
		{
			ExcelUtils.setCellData("Fail", i, 5,Constant.File_TestData_Rooms);
		}
		
		
		}
	}

}*/
