package education.volute.notes_testscripts;

import utilities.SoftAssertCheck;

public class NotesTest {

	public void AddNotes(){
		AddNotes_TC_01 dis = new AddNotes_TC_01();
		dis.testAddNotes_TC_01();
	}
	
	public void EditNotes(){
		EditNotes_TC_02 dis = new EditNotes_TC_02();
		dis.testEditNotes_TC_02();
	}
	
	public void DeleteNote(){
		DeleteNote_TC_03 dis = new DeleteNote_TC_03();
		dis.testDeleteNotes_TC_03();
	}
	
}
