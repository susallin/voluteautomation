package education.volute.notes_testscripts;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import education.volute.common_lib.Story;
import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.NavigatorTool;
import education.volute.pages.Notes;
import utilities.Constant;
import utilities.SoftAssertCheck;

public class DeleteNote_TC_03 extends SuperTestScript
{
	//@Test
	public void testDeleteNotes_TC_03() 
	{
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Notes, "TC_03");
		
		LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		SoftAssertCheck soft = new SoftAssertCheck();
		Story sto = new Story();
		Notes n1 = new Notes();
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		//sto.Tnum(i);
		String uName = ExcelUtils.getCellData(i, 1);
        String pwd = ExcelUtils.getCellData(i, 2);
        String noteTitleTxt = ExcelUtils.getCellData(i, 3);
        String expRes = ExcelUtils.getCellData(i, 5);
                
	
	/*lp.login(uName, pwd);
	lp.logibutton();
	try {
		Thread.sleep(10000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}*/
	nt.clickOnNotesIcon();
	n1.clickOnXofaNote(noteTitleTxt);
	n1.clickOnDelteOnConfirmationPopup();
	//	n1.clickOnDelteOnConfirmationPopup();
	
//	n1.deletionConfirmation(deletionConfirmationTxt);
//	
	 String actRes = n1.fetchSuccessMsg();
	 System.out.println(actRes);
	 try {
		Thread.sleep(4000);
	 } catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	 }
	 
	 String noteShows = n1.fetchFacesNoteShows(noteTitleTxt);
		System.out.println(noteShows);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(noteShows.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking Content Please does not show on the front face of the tool");
			System.out.println("The test case passed on the content is not showing on the front face of the tool.");
		}else{
			soft.assertTrue(false, "The test case failed on the content is showing on the front face of the tool.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	 
	    nt.refreshPage();
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnNotesIcon();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// This will determine if Value is still showing or not.
		String noteShows1 = n1.fetchFacesNoteShows(noteTitleTxt);
		System.out.println(noteShows1);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(noteShows1.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking Content Please does not show on the front face of the tool");
			System.out.println("The test case passed on the content is not showing on the front face of the tool.");
		}else{
			soft.assertTrue(false, "The test case failed on the content is showing on the front face of the tool.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    
		ExcelUtils.setCellData(noteShows, i, 4, Constant.File_TestData_Notes);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	 
	 ExcelUtils.setCellData(actRes, i, 6, Constant.File_TestData_Notes);
//		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
//		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 7, Constant.File_TestData_Notes);
		}
		else 
    	{
			ExcelUtils.setCellData("Fail", i, 7, Constant.File_TestData_Notes);
		}
		
//		
		}
		soft.assertAll();
		}

}

	
	
		

