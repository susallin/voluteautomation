package education.volute.notes_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.Story;
import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.NavigatorTool;
import education.volute.pages.Notes;
import utilities.Constant;
import utilities.SoftAssertCheck;

public class EditNotes_TC_02 extends SuperTestScript

{
	//@Test
	public void testEditNotes_TC_02() 
	{
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Notes, "TC_02");
		
		LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		SoftAssertCheck soft = new SoftAssertCheck();
		Story sto = new Story();
		Notes n1 = new Notes();
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		//sto.Tnum(i);
		String uName = ExcelUtils.getCellData(i, 1);
        String pwd = ExcelUtils.getCellData(i, 2);
        String oldTitleTxt = ExcelUtils.getCellData(i, 3);
        String newTitleTxt = ExcelUtils.getCellData(i, 4);
        String newtagsTxt = ExcelUtils.getCellData(i, 5);
        String newnotesTxt = ExcelUtils.getCellData(i, 6);
        String expRes = ExcelUtils.getCellData(i, 7);
	
	/*lp.login(uName, pwd);
	lp.logibutton();
	try {
		Thread.sleep(10000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	nt.clickOnNotesIcon();
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}*/

	n1.clickOnOldNote(oldTitleTxt);
	n1.enterNotesTitle(newTitleTxt);
	n1.enterNotesTag(newtagsTxt);
	n1.enterNoteTextHere(newnotesTxt);
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	n1.clickOnBackNotesButton();
	
	String actRes = n1.getNoteTile(oldTitleTxt+newTitleTxt);
	System.out.println(actRes);
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	String noteShows = n1.fetchFacesNoteShows(oldTitleTxt+newTitleTxt);
	System.out.println(noteShows);
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	if(noteShows.equalsIgnoreCase("Found")){
		soft.assertTrue(true, "Checking Content Please does not show on the front face of the tool");
		System.out.println("The test case passed on the content not showing on the front face of the tool.");
	}else{
		soft.assertTrue(false, "The test case failed on the content not showing on the front face of the tool.\n");
	}
    try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	nt.refreshPage();
	try {
		Thread.sleep(15000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	nt.clickOnNotesIcon();
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	String noteShows1 = n1.fetchFacesNoteShows(oldTitleTxt+newTitleTxt);
	System.out.println(noteShows1);
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	if(noteShows1.equalsIgnoreCase("Found")){
		soft.assertTrue(true, "Checking Content Please does not show on the front face of the tool");
		System.out.println("The test case passed on the content not showing on the front face of the tool.");
	}else{
		soft.assertTrue(false, "The test case failed on the content not showing on the front face of the tool.\n");
	}
    try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}

	 ExcelUtils.setCellData(actRes, i, 8, Constant.File_TestData_Notes);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 9, Constant.File_TestData_Notes);
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 9, Constant.File_TestData_Notes);
		}
		
		
		}
		
		}

}
	



