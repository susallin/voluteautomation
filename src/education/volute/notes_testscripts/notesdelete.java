package education.volute.notes_testscripts;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class notesdelete 
{
		
		public static void main(String[] args) throws InterruptedException{
			// TODO Auto-generated method stub
			// Loading the page.
			
			System.setProperty("webdriver.chrome.driver", "./exe_files/chromedriver.exe");
			WebDriver driver = new ChromeDriver();
			//WebDriver driver = new FirefoxDriver();
			driver.manage().window().maximize();
			driver.get("https://sandbox.volute.info/login");
			driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
			
			// Login in the page.
		    driver.findElement(By.name("email")).sendKeys("raultester@mailinator.com");
									
			driver.findElement(By.cssSelector("input[type='password']")).sendKeys("password");
									
			driver.findElement(By.id("login-button")).click();
			
			Thread.sleep(10000);
			
			// click Home
			driver.findElement(By.cssSelector("paper-icon-button[title='Home']")).click();
			Thread.sleep(10000);
			
			// Click Floatable Note Tools
		    driver.findElement(By.cssSelector("paper-icon-button[title='Notes']")).click();
		    Thread.sleep(10000);
		    
		    // Add/Create a note
		    driver.findElement(By.cssSelector("paper-button[class*='save-button style-scope volute-notes-face1 x-scope paper-button-0']")).click();
		    
	             // Delete Notes
		    driver.findElement(By.cssSelector("paper-icon-button[alt='delete this note']")).click();
		
		    // Confirm Deletion Notes is Cancel
		    driver.findElement(By.cssSelector("paper-dialog > div > paper-button:nth-of-type(2)")).click();
		    
		    // Delete Notes
		    driver.findElement(By.cssSelector("paper-icon-button[alt='delete this note']")).click();
		    Thread.sleep(10000);
		    // Confirm Deletion Notes
		    driver.findElement(By.cssSelector("paper-dialog > div > paper-button:nth-of-type(1)")).click();
		    
		    // close notes
		    //driver.findElement(By.cssSelector("paper-icon-button[id='close']")).click();
		    driver.findElement(By.id("close")).click();
		}
	}

