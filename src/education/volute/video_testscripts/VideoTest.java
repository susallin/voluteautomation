package education.volute.video_testscripts;

import java.io.IOException;

public class VideoTest {

	public void VideoAdd(String Role) throws Exception{
		Video_TC_01 ts = new Video_TC_01();	
		ts.testVideo_TC_01(Role);
	}
	
	public void VideoEdit(String Role) throws Exception{
		Video_TC_02 ts = new Video_TC_02();	
		ts.testVideo_TC_02(Role);
	}
	
	public void VideoDelete(String Role){
		Video_TC_03 ts = new Video_TC_03();	
		ts.testVideo_TC_03(Role);
	}
}
