package education.volute.video_testscripts;

//import java.io.IOException;
import java.util.Iterator;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.Video;
import education.volute.pages.ManagePrograms;
import education.volute.pages.Landing;
import education.volute.pages.MenuItems;
import education.volute.pages.SocialHub;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.ToolDestAndCollections;

public class Video_TC_01 extends SuperTestScript{
	//@Test
	public void testVideo_TC_01(String Role) throws Exception 
	{
			//LoginPage lp = new LoginPage();
			NavigatorTool nt = new NavigatorTool();
			//SocialHub mj = new SocialHub();
			ReadCsvFiles csv = new ReadCsvFiles();
			MenuItems mi = new MenuItems();
			//Landing ld = new Landing();
			//ManagePrograms mp = new ManagePrograms();
			//ManageLearningSpaces mls = new ManageLearningSpaces();
			ToolDestAndCollections toolD = new ToolDestAndCollections();
			Video vid = new Video();
		
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Video, "AddVideo3");
			
			for(int i=1; i<=ExcelUtils.getRowcount(); i++)
			{
			
			//String uName = ExcelUtils.getCellData(i, 1);
	        //String pwd = ExcelUtils.getCellData(i, 2);
			String toolDest = ExcelUtils.getCellData(i, 1);
	        String actName = ExcelUtils.getCellData(i, 2);
	        String subActName = ExcelUtils.getCellData(i, 3);
	        String pgmNameTxt = ExcelUtils.getCellData(i, 4);
	        String toolName = ExcelUtils.getCellData(i, 5);
	        //String lsToolName = ExcelUtils.getCellData(i, 4);
	        //String lsNameTxt = ExcelUtils.getCellData(i, 5);
	        //String lsDescTxt = ExcelUtils.getCellData(i, 6);
	        //String sliDName = ExcelUtils.getCellData(i, 7);
	        String vidFilNames = ExcelUtils.getCellData(i, 6);
	        //String vidURL = ExcelUtils.getCellData(i, 5);
	        //String vidURL2 = ExcelUtils.getCellData(i, 6);
	        String uRole = ExcelUtils.getCellData(i, 10);
	        String appLocName = ExcelUtils.getCellData(i, 11);
	        String expRes = ExcelUtils.getCellData(i, 7);
			
			//lp.login(uName, pwd);
			/*lp.logibutton();
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			nt.clickOnMainMenu();*/
	        toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
			//mi.clickOnNavigation();
			//mi.clickOnProgram();
			//ld.clickOnLandingMP();
			/*try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mp.clickOnselectedprogram(pgmNameTxt);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mp.clickOnManageMashupicon(pgmNameTxt);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/*mls.clickOnAddLS();
			mls.selectaTool(lsToolName);
			mls.enterLSName(lsNameTxt);
			mls.enterLsDesc(lsDescTxt);
			mls.clickOnSaveButton();
			mls.clickOnConfigButton();*/
			/*nt.clickOnJourney();
			try {
				Thread.sleep(8000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mj.clickOnLaunchAct(actName);
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
	        
	        // Here Tool will execute 
			/*try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			mi.fetchAppTool(appLocName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mi.SelectManageFaceOfTool(toolName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			Iterator<String[]> FileName = csv.ReadList(vidFilNames);
			
			if(FileName.hasNext())
				FileName.next();
			
			while(FileName.hasNext()){
				int n = 0;
				String[] Name = FileName.next();
				boolean flag = true;
				//System.out.println(" Values are ");

				String addVid = Name[n];
				String addBackVid = Name[n+1];
				String nameBackButton = Name[n+2];
				String TypeFile = Name[n+3];
				String File = Name[n+4];
				String VidTitle = Name[n + 5];
				String VidDesc = Name[n+6];
				
				if(addVid.equalsIgnoreCase("yes")){
					mi.SelectAdd(toolName);
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				
				if(TypeFile.equalsIgnoreCase("MP4")) {
					vid.clickOnvideoManualUpload();
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					vid.enterOnVideoTitle(VidTitle);
					vid.enterOnVideoDescription(VidDesc);
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					vid.clickOnVideoAddFile(File);
					/*try {
						Thread.sleep(5000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+File);*/
					while(flag){
						String result = nt.fetchProgressFile();
						
						if(result.equalsIgnoreCase("Found")){
							
							if(addBackVid.equalsIgnoreCase("yes")){
								mi.clickBackSaveListButton(nameBackButton);
								try {
									Thread.sleep(1000);
								} catch (InterruptedException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
							} else{
								vid.clickOnVideoSave();
								try {
									Thread.sleep(1000);
								} catch (InterruptedException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
							}
							flag = false;
						}
						else{
							flag = true;
						}
					}
				}
				
				if(TypeFile.equalsIgnoreCase("Youtube") || TypeFile.equalsIgnoreCase("Vimeo") || TypeFile.equalsIgnoreCase("Kaltura")){
					vid.clickOnVideoUrlUpload();
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					vid.enterVideoInsertURL(File);
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					vid.enterOnVideoDescription(VidDesc);
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					if(addBackVid.equalsIgnoreCase("yes")){
						mi.clickBackSaveListButton(nameBackButton);
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					} else{
						vid.clickOnVideoSave();
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}
			}
				
				/*if(TypeFile.equalsIgnoreCase("Vimeo")){
					vid.enterVideoInsertURL(File);
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					vid.enterOnVideoDescription(" ");
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					vid.clickOnVideoSave();
				}*/
				
				/*mi.SelectAdd(toolName);
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				sli.enterSlideShowName(SliSName);
				
				sli.clickOnSlideSelectFile();//sliFilName);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+File);
				try {
					Thread.sleep(70000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				sli.clickOnSlidesShowSave();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				
			}
			
			vid.clickOnVideoCheckBoxUpload();
			vid.clickOnVideoAddFile();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Runtime.getRuntime().exec("./user_data/Autoit/FileUploadMp4.exe");
			try {
				Thread.sleep(7000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			vid.enterOnVideoTitle("Quick Mp4 Test");
			vid.enterOnVideoDescription("Higher Education League");
			try {
				Thread.sleep(70000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			vid.clickOnVideoSave();
			//sli.enterSlideDisplayName(sliDName);
			vid.enterVideoInsertURL(vidURL);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			vid.enterOnVideoDescription(" ");
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			vid.clickOnVideoSave();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			vid.enterVideoInsertURL(vidURL2);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			vid.enterOnVideoDescription(" ");
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			vid.clickOnVideoSave();
			//try {
			//	Thread.sleep(5000);
			//} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
			//	e1.printStackTrace();
			//}*/
			
			String actRes = vid.fetchVideoAddMsg();
			System.out.println(actRes);
			ExcelUtils.setCellData(actRes, i, 8, Constant.File_TestData_Video);
			
			Boolean status = ValidationLib.verifyMsg(expRes, actRes);
			
			if(status)
			{
				ExcelUtils.setCellData("pass", i, 9,Constant.File_TestData_Video);
			}
			else 
		{
				ExcelUtils.setCellData("Fail", i, 9,Constant.File_TestData_Video);
			}
			
			
			}
			
		}
	}
