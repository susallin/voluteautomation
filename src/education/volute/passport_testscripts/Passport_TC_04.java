package education.volute.passport_testscripts;

import java.util.Iterator;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import education.volute.pages.Passport;
import education.volute.pages.Video;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class Passport_TC_04 extends SuperTestScript {

	public void testPassport_TC_04(String Role) throws Exception 
	
	{
		//LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		ReadCsvFiles csv = new ReadCsvFiles();
		//SocialHub mj = new SocialHub();
		MenuItems mi = new MenuItems();
		//ReadCsvFiles csv = new ReadCsvFiles();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		//Landing ld = new Landing();
		//ManagePrograms mp = new ManagePrograms();
		//ManageLearningSpaces mls = new ManageLearningSpaces();
		Passport pas = new Passport();
		SoftAssertCheck soft = new SoftAssertCheck();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Passport, "HighlightPassport");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		//String uName = ExcelUtils.getCellData(i, 1);
        //String pwd = ExcelUtils.getCellData(i, 2);
		String toolDest = ExcelUtils.getCellData(i, 1);
	    String actName = ExcelUtils.getCellData(i, 2);
	    String subActName = ExcelUtils.getCellData(i, 3);
	    String pgmNameTxt = ExcelUtils.getCellData(i, 4);
        String pasFileNames = ExcelUtils.getCellData(i, 5);
        //String pgmNameTxt = ExcelUtils.getCellData(i, 3);
        //String lsNameTxt = ExcelUtils.getCellData(i, 4);
        //String lsNameTxt = ExcelUtils.getCellData(i, 5);
        //String lsDescTxt = ExcelUtils.getCellData(i, 6);
        //String sliDName = ExcelUtils.getCellData(i, 7);
        //String vidName = ExcelUtils.getCellData(i, 5);
        //String vidTitle = ExcelUtils.getCellData(i, 6);
        //String sliSName = ExcelUtils.getCellData(i, 7);
        //String sliFilName = ExcelUtils.getCellData(i, 9);
        String uRole = ExcelUtils.getCellData(i, 6);
        String appLocName = ExcelUtils.getCellData(i, 7);
		
		/*lp.login(uName, pwd);
		lp.logibutton();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnMainMenu();*/
        toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
		/*mi.clickOnNavigation();
		mi.clickOnProgram();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ld.clickOnLandingMP();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnselectedprogram(pgmNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnManageMashupicon(pgmNameTxt);
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*mls.selectaLearningSpace(lsNameTxt);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mls.clickOnEditButton(lsNameTxt);
		mls.clickOnConfigButton();*/
		/*nt.clickOnJourney();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mj.clickOnLaunchAct(actName);*/
		/*try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		mi.fetchAppTool(appLocName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Iterator<String[]> FileName = csv.ReadList(pasFileNames);
		
		if(FileName.hasNext())
			FileName.next();
		
		while(FileName.hasNext()){
			int n = 0;
			String[] Name = FileName.next();
			//System.out.println(" Values are ");

			String firstName = Name[n];
			String secondName = Name[n+1];
			String thirdName = Name[n + 2];
			String fourthName = Name[n+3];
			
			
			pas.clickOnPassportTab(firstName);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String pasHighlighted = pas.fetchPasisHighlighted(firstName);
			System.out.println(pasHighlighted);
			if(pasHighlighted.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking tab.");
				System.out.println("The test case passed, the tab user selected highlights.");
			}else{
				soft.assertTrue(false, "The test case failed, the tab user selected does not highlight.\n");
			}
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String pasNotHighlighted = pas.fetchPasisNotHighlighted(secondName);
			System.out.println(pasNotHighlighted);
			if(pasNotHighlighted.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking tab.");
				System.out.println("The test case passed, user did not select this tab; the tab does not highlight.");
			}else{
				soft.assertTrue(false, "The test case failed, user did not select this tab; the tab highlights.\n");
			}
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String pasNotHighlighted2 = pas.fetchPasisNotHighlighted(thirdName);
			System.out.println(pasNotHighlighted2);
			if(pasNotHighlighted2.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking tab.");
				System.out.println("The test case passed, user did not select this tab; the tab does not highlight.");
			}else{
				soft.assertTrue(false, "The test case failed, user did not select this tab; the tab highlights.\n");
			}
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String pasNotHighlighted3 = pas.fetchPasisNotHighlighted(fourthName);
			System.out.println(pasNotHighlighted3);
			if(pasNotHighlighted3.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking tab.");
				System.out.println("The test case passed, user did not select this tab; the tab does not highlight.");
			}else{
				soft.assertTrue(false, "The test case failed, user did not select this tab; the tab highlights.\n");
			}
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
		
		
		}
		soft.assertAll();
		
		}
}
