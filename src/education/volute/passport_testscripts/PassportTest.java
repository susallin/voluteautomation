package education.volute.passport_testscripts;

public class PassportTest {

	public void PassportAdd(String Role) throws Exception {
		Passport_TC_01 pas = new Passport_TC_01();	
		pas.testPassport_TC_01(Role);
	}
	
	public void PassportEdit(String Role) throws Exception {
		Passport_TC_02 pas = new Passport_TC_02();	
		pas.testPassport_TC_02(Role);
	}
	
	public void PassportDelete(String Role) {
		Passport_TC_03 pas = new Passport_TC_03();	
		pas.testPassport_TC_03(Role);
	}
	
	public void PassportTabs(String Role) throws Exception {
		Passport_TC_04 pas = new Passport_TC_04();	
		pas.testPassport_TC_04(Role);
	}
}
