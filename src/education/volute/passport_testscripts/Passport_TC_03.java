package education.volute.passport_testscripts;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import education.volute.pages.Passport;
import education.volute.pages.Video;
import utilities.Constant;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class Passport_TC_03 extends SuperTestScript {

	public void testPassport_TC_03(String Role) 
	
	{
		//LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		//SocialHub mj = new SocialHub();
		MenuItems mi = new MenuItems();
		//ReadCsvFiles csv = new ReadCsvFiles();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		//Landing ld = new Landing();
		//ManagePrograms mp = new ManagePrograms();
		//ManageLearningSpaces mls = new ManageLearningSpaces();
		Video vid = new Video();
		Passport pas = new Passport();
		SoftAssertCheck soft = new SoftAssertCheck();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Passport, "DeletePassport");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		//String uName = ExcelUtils.getCellData(i, 1);
        //String pwd = ExcelUtils.getCellData(i, 2);
		String toolDest = ExcelUtils.getCellData(i, 1);
	    String actName = ExcelUtils.getCellData(i, 2);
	    String subActName = ExcelUtils.getCellData(i, 3);
	    String pgmNameTxt = ExcelUtils.getCellData(i, 4);
	    String toolName = ExcelUtils.getCellData(i, 5);
        String pasFileName = ExcelUtils.getCellData(i, 6);
        String pasFileName1 = ExcelUtils.getCellData(i, 7);
        //String pgmNameTxt = ExcelUtils.getCellData(i, 3);
        //String lsNameTxt = ExcelUtils.getCellData(i, 4);
        //String lsNameTxt = ExcelUtils.getCellData(i, 5);
        //String lsDescTxt = ExcelUtils.getCellData(i, 6);
        //String sliDName = ExcelUtils.getCellData(i, 7);
        //String vidName = ExcelUtils.getCellData(i, 5);
        //String vidTitle = ExcelUtils.getCellData(i, 6);
        //String sliSName = ExcelUtils.getCellData(i, 7);
        //String sliFilName = ExcelUtils.getCellData(i, 9);
        String uRole = ExcelUtils.getCellData(i, 12);
        String appLocName = ExcelUtils.getCellData(i, 13);
        String expRes = ExcelUtils.getCellData(i, 9);
		
		/*lp.login(uName, pwd);
		lp.logibutton();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnMainMenu();*/
        toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
		/*mi.clickOnNavigation();
		mi.clickOnProgram();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ld.clickOnLandingMP();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnselectedprogram(pgmNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnManageMashupicon(pgmNameTxt);
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*mls.selectaLearningSpace(lsNameTxt);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mls.clickOnEditButton(lsNameTxt);
		mls.clickOnConfigButton();*/
		/*nt.clickOnJourney();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mj.clickOnLaunchAct(actName);*/
		/*try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		mi.fetchAppTool(appLocName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		mi.SelectManageFaceOfTool(toolName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		pas.clickOnPassportDeleteButton(pasFileName);
		//sli.enterSlideDisplayName(sliDName);
		vid.clickOnDeleteYesVideo();
		
		String actRes = pas.fetchPassportDeleteMsg();
		System.out.println(actRes);
		if(actRes.equalsIgnoreCase("Topic deleted successfully")){
			soft.assertTrue(true, "Checking if toast shows/appears");
			System.out.println("The test case passed, toast shows.");
		}else{
			soft.assertTrue(false, "The test case failed, toast does not show.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		// This will determine if Value is still showing or not.
		String pasShows = pas.fetchManageFacesPasShows(pasFileName);
		System.out.println(pasShows);
		if(pasShows.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking if content still shows on Manage");
			System.out.println("The test case passed on verifying the content was deleted from Passport tool on Manage.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the content was deleted from Passport tool on Manage.\n");
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		pas.clickOnPassportDeleteButton(pasFileName1);
		//sli.enterSlideDisplayName(sliDName);
		vid.clickOnDeleteYesVideo();
		
		String actRes1 = pas.fetchPassportDeleteMsg();
		System.out.println(actRes1);
		if(actRes1.equalsIgnoreCase("Topic deleted successfully")){
			soft.assertTrue(true, "Checking if toast shows/appears");
			System.out.println("The test case passed, toast shows.");
		}else{
			soft.assertTrue(false, "The test case failed, toast does not show.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		// This will determine if Value is still showing or not.
		String pasShows1 = pas.fetchManageFacesPasShows(pasFileName);
		System.out.println(pasShows1);
		if(pasShows1.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking if content still shows on Manage");
			System.out.println("The test case passed on verifying the content was deleted from Passport tool on Manage.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the content was deleted from Passport tool on Manage.\n");
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		mi.SelectExitManageFaceOfTool(toolName);
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    
	    String pasShows2 = pas.fetchFacesPasShows(pasFileName);
		System.out.println(pasShows2);
		if(pasShows2.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking if content still shows on Face");
			System.out.println("The test case passed on verifying the content was deleted from Passport tool on Face.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the content was deleted from Passport tool on Face.\n");
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		 String pasShows3 = pas.fetchFacesPasShows(pasFileName1);
		 System.out.println(pasShows3);
		 if(pasShows3.equalsIgnoreCase("Not Found")){
		   	soft.assertTrue(true, "Checking if content still shows on Face");
			System.out.println("The test case passed on verifying the content was deleted from Passport tool on Face.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the content was deleted from Passport tool on Face.\n");
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		nt.refreshPage();
		
		mi.fetchAppTool(appLocName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String pasShows1a = pas.fetchFacesPasShows(pasFileName);
		System.out.println(pasShows1a);
		if(pasShows1a.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking if content still shows on Face");
			System.out.println("The test case passed on verifying the content was deleted from Passport tool on Face.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the content was deleted from Passport tool on Face.\n");
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String pasShows1b = pas.fetchFacesPasShows(pasFileName1);
		System.out.println(pasShows1b);
		if(pasShows1b.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking if content still shows on Face");
			System.out.println("The test case passed on verifying the content was deleted from Passport tool on Face.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the content was deleted from Passport tool on Face.\n");
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		mi.SelectManageFaceOfTool(toolName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		// This will determine if Value is still showing or not.
		String pasShows2a = pas.fetchManageFacesPasShows(pasFileName);
		System.out.println(pasShows2a);
		if(pasShows2a.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking if content still shows Manage");
			System.out.println("The test case passed on verifying the content was deleted from Passport tool Manage.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the content was deleted from Passport tool Manage.\n");
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		// This will determine if Value is still showing or not.
		String pasShows2b = pas.fetchManageFacesPasShows(pasFileName1);
		System.out.println(pasShows2b);
		if(pasShows2b.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking if content still shows Manage");
			System.out.println("The test case passed on verifying the content was deleted from Passport tool Manage.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the content was deleted from Passport tool Manage.\n");
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ExcelUtils.setCellData(pasShows+"\n"+pasShows2, i, 8, Constant.File_TestData_Passport);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		ExcelUtils.setCellData(actRes, i, 10, Constant.File_TestData_Passport);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 11,Constant.File_TestData_Passport);
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 11,Constant.File_TestData_Passport);
		}
		
		
		}
		soft.assertAll();
		}
}
