package education.volute.passport_testscripts;

import java.util.Iterator;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import education.volute.pages.Passport;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class Passport_TC_02 extends SuperTestScript{

		//@Test
		public void testPassport_TC_02(String Role) throws Exception 
		{
				//LoginPage lp = new LoginPage();
				NavigatorTool nt = new NavigatorTool();
				//SocialHub mj = new SocialHub();
				ReadCsvFiles csv = new ReadCsvFiles();
				MenuItems mi = new MenuItems();
				SoftAssertCheck soft = new SoftAssertCheck();
				//Landing ld = new Landing();
				//ManagePrograms mp = new ManagePrograms();
				//ManageLearningSpaces mls = new ManageLearningSpaces();
				ToolDestAndCollections toolD = new ToolDestAndCollections();
				Passport pas = new Passport();
			
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Passport, "EditPassport");
				
				for(int i=1; i<=ExcelUtils.getRowcount(); i++)
				{
				
				//String uName = ExcelUtils.getCellData(i, 1);
		        //String pwd = ExcelUtils.getCellData(i, 2);
				String toolDest = ExcelUtils.getCellData(i, 1);
		        String actName = ExcelUtils.getCellData(i, 2);
		        String subActName = ExcelUtils.getCellData(i, 3);
		        String pgmNameTxt = ExcelUtils.getCellData(i, 4);
		        String toolName = ExcelUtils.getCellData(i, 5);
		        //String lsToolName = ExcelUtils.getCellData(i, 4);
		        //String lsNameTxt = ExcelUtils.getCellData(i, 5);
		        //String lsDescTxt = ExcelUtils.getCellData(i, 6);
		        //String sliDName = ExcelUtils.getCellData(i, 7);
		        String pasFilNames = ExcelUtils.getCellData(i, 6);
		        //String vidURL = ExcelUtils.getCellData(i, 5);
		        //String vidURL2 = ExcelUtils.getCellData(i, 6);
		        String uRole = ExcelUtils.getCellData(i, 10);
		        String appLocName = ExcelUtils.getCellData(i, 11);
		        String expRes = ExcelUtils.getCellData(i, 7);
				
				//lp.login(uName, pwd);
				/*lp.logibutton();
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				nt.clickOnMainMenu();*/
		        toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
				//mi.clickOnNavigation();
				//mi.clickOnProgram();
				//ld.clickOnLandingMP();
				/*try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				mp.clickOnselectedprogram(pgmNameTxt);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				mp.clickOnManageMashupicon(pgmNameTxt);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				/*mls.clickOnAddLS();
				mls.selectaTool(lsToolName);
				mls.enterLSName(lsNameTxt);
				mls.enterLsDesc(lsDescTxt);
				mls.clickOnSaveButton();
				mls.clickOnConfigButton();*/
				/*nt.clickOnJourney();
				try {
					Thread.sleep(8000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				mj.clickOnLaunchAct(actName);
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}*/
		        
		        // Here Tool will execute 
				/*try {
					Thread.sleep(15000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}*/
				mi.fetchAppTool(appLocName);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				//pas.clickOnPassportManage();
				mi.SelectManageFaceOfTool(toolName);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				Iterator<String[]> FileName = csv.ReadList(pasFilNames);
				
				if(FileName.hasNext())
					FileName.next();
				
				while(FileName.hasNext()){
					int n = 0;
					String[] Name = FileName.next();
					boolean flag = true;
					//System.out.println(" Values are ");

					String addBackPas = Name[n];
					String nameBackButton = Name[n+1];
					String oldTitle = Name[n+2];
					String TypeIcon = Name[n+3];
					String newTitle = Name[n+4];
					String File = Name[n+5];
					String pasContent = Name[n+6];
					
					pas.clickOnPassportEditButton(oldTitle);
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
//					pas.clickOnSelectIconPassport();
//					try {
//						Thread.sleep(2000);
//					} catch (InterruptedException e1) {
//						// TODO Auto-generated catch block
//						e1.printStackTrace();
//					}
//					
//					pas.selectIconPassport(TypeIcon);
//					
//					pas.clearPassportTitle();
//					try {
//						Thread.sleep(2000);
//					} catch (InterruptedException e1) {
//						// TODO Auto-generated catch block
//						e1.printStackTrace();
//					}
//					pas.enterOnTitlePassport(newTitle);
					
					pas.clickOnRemoveImgPassportButton();
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					String imgShows = pas.fetchPassportImgShows();
					System.out.println(imgShows);
					if(imgShows.equalsIgnoreCase("Not Found")){
						soft.assertTrue(true, "Checking the Image for passport does not show after clicking x");
						System.out.println("The test case passed on Checking the Image for passport, does not show after removing it.");
					}else{
						soft.assertTrue(false, "The test case failed on Checking the Image for passport, does show after removing it.\n");
					}
				    try {
						Thread.sleep(3000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				    
				    pas.clickOnSelectFilePassport(File);
				    
				    while(flag){
						String result = nt.fetchProgressFile();
						
						if(result.equalsIgnoreCase("Found")){
							flag = false;
						}else{
							flag = true;
						}
					}
				    
				    flag = true;
				    
				    pas.clickOnCancelButtonPassport();
				    try {
						Thread.sleep(3000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					nt.clickOnDeletePgmYesButton();
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					String imgShows1 = pas.fetchPassportImgShows();
					System.out.println(imgShows1);
					if(imgShows1.equalsIgnoreCase("Not Found")){
						soft.assertTrue(true, "Checking the Image for passport does not show after clicking x");
						System.out.println("The test case passed on Checking the Image for passport, does not show after removing it.");
					}else{
						soft.assertTrue(false, "The test case failed on Checking the Image for passport, does show after removing it.\n");
					}
				    try {
						Thread.sleep(3000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					pas.clickOnSelectFilePassport(File);
					/*try {
						Thread.sleep(5000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+File);*/			
					while(flag){
						String result = nt.fetchProgressFile();
						
						if(result.equalsIgnoreCase("Found")){
							pas.clickOnSelectIconPassport();
							try {
								Thread.sleep(2000);
							} catch (InterruptedException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							
							pas.selectIconPassport(TypeIcon);
							
							pas.enterOnTitlePassport(newTitle);
							
							pas.enterOnContentPassport(pasContent);
							try {
								Thread.sleep(2000);
							} catch (InterruptedException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							
							if(addBackPas.equalsIgnoreCase("yes")){
								mi.clickBackSaveListButton(nameBackButton);
								try {
									Thread.sleep(1000);
								} catch (InterruptedException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
							} else{
								pas.clickOnSaveButtonPassport();
								try {
									Thread.sleep(1000);
								} catch (InterruptedException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
							}
							flag = false;
						}
						else{
							flag = true;
						}
					}
				}
				
				String actRes = pas.fetchPassportAddMsg();
				System.out.println(actRes);
				if(actRes.equalsIgnoreCase("Topic saved successfully")){
					soft.assertTrue(true, "Checking if toast shows/appears");
					System.out.println("The test case passed, toast shows.");
				}else{
					soft.assertTrue(false, "The test case failed, toast does not show.\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
				
				ExcelUtils.setCellData(actRes, i, 8, Constant.File_TestData_Passport);
				
				Boolean status = ValidationLib.verifyMsg(expRes, actRes);
				
				if(status)
				{
					ExcelUtils.setCellData("pass", i, 9,Constant.File_TestData_Passport);
				}
				else 
			{
					ExcelUtils.setCellData("Fail", i, 9,Constant.File_TestData_Passport);
				}
				
				
				}
				
			}
}
