package education.volute.biography_testscripts;

import java.io.IOException;

import org.testng.annotations.Test;

import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Biography;
import utilities.Constant;
import utilities.NavigateTo;

public class Biography_TC_02 {

	//@Test
	public void testBiography_TC_02()
	
	{
		NavigateTo nav = new NavigateTo();
		Biography bio = new Biography();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Biography, "BiographyContent");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		//String uName = ExcelUtils.getCellData(i, 1);
        //String pwd = ExcelUtils.getCellData(i, 2);
		String navigate = ExcelUtils.getCellData(i, 1);
		String bioName = ExcelUtils.getCellData(i, 2);
		String bioNameShows;
		String bioMidName = ExcelUtils.getCellData(i, 4);
		String bioMidShows;
		String bioLastName = ExcelUtils.getCellData(i, 6);
		String bioLastNameShows;
		String bioCity = ExcelUtils.getCellData(i, 8);
		String bioCityShows;
		String bioState = ExcelUtils.getCellData(i, 10);
		String bioStateShows;
		String bioCountry = ExcelUtils.getCellData(i, 12);
		String bioCountryShows;
		String bioBiography = ExcelUtils.getCellData(i, 14);
		String bioBiographyShows;
		
		/*nav.GoTo(navigate);
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		
		bioNameShows = bio.fetchTopFacesBioShows(bioName);
		System.out.println(bioNameShows);
		ExcelUtils.setCellData(bioNameShows, i, 3, Constant.File_TestData_Biography);
		
		bioMidShows = bio.fetchTopFacesBioShows(bioMidName);
		System.out.println(bioMidShows);
		ExcelUtils.setCellData(bioMidShows, i, 5, Constant.File_TestData_Biography);
		
		bioLastNameShows = bio.fetchTopFacesBioShows(bioLastName);
		System.out.println(bioLastNameShows);
		ExcelUtils.setCellData(bioLastNameShows, i, 7, Constant.File_TestData_Biography);
		
		bioCityShows = bio.fetchTopFacesBioShows(bioCity);
		System.out.println(bioCityShows);
		ExcelUtils.setCellData(bioCityShows, i, 9, Constant.File_TestData_Biography);
		
		bioStateShows = bio.fetchTopFacesBioShows(bioState);
		System.out.println(bioStateShows);
		ExcelUtils.setCellData(bioStateShows, i, 11, Constant.File_TestData_Biography);
		
		bioCountryShows = bio.fetchTopFacesBioShows(bioCountry);
		System.out.println(bioCountryShows);
		ExcelUtils.setCellData(bioCountryShows, i, 13, Constant.File_TestData_Biography);
		
		/*bioOrgShows = bio.fetchTopFacesBioShows(bioOrganization);
		System.out.println(bioOrgShows);
		ExcelUtils.setCellData(bioOrgShows, i, 15, Constant.File_TestData_Biography);*/
		}
	}
}
