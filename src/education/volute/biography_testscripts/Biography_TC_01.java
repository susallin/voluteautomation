package education.volute.biography_testscripts;

import java.io.IOException;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
//import education.volute.pages.ManageLearningSpaces;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
//import education.volute.pages.SubMenu;
import education.volute.pages.Biography;
import education.volute.pages.Landing;
import utilities.Constant;
import utilities.ToolDestAndCollections;

public class Biography_TC_01 extends SuperTestScript {

	//@Test
	public void testBiography_TC_01(String Role) throws Exception 
	
	{
		//LoginPage lp = new LoginPage();
		//NavigatorTool nt = new NavigatorTool();
		MenuItems mi = new MenuItems();
		//Landing ld = new Landing();
		//ManagePrograms mp = new ManagePrograms();
		//ManageLearningSpaces mls = new ManageLearningSpaces();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		Biography bio = new Biography();
		//SubMenu sub = new SubMenu();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Biography, "AddBiography2");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		//String uName = ExcelUtils.getCellData(i, 1);
        //String pwd = ExcelUtils.getCellData(i, 2);
		String toolDest = ExcelUtils.getCellData(i, 1);
		String actName = ExcelUtils.getCellData(i, 2);
		String subActName = ExcelUtils.getCellData(i, 3);
        String pgmNameTxt = ExcelUtils.getCellData(i, 4);
        String toolName = ExcelUtils.getCellData(i, 5);
        //String lsToolName = ExcelUtils.getCellData(i, 4);
        //String lsNameTxt = ExcelUtils.getCellData(i, 5);
        //String lsDescTxt = ExcelUtils.getCellData(i, 6);
        //String bioDNameTxt = ExcelUtils.getCellData(i, 7);
        String bioFile = ExcelUtils.getCellData(i, 6);
        String bioFNameTxt = ExcelUtils.getCellData(i, 7);
        String bioMNameTxt = ExcelUtils.getCellData(i, 8);
        String bioLNameTxt = ExcelUtils.getCellData(i, 9);
        String bioCityTxt = ExcelUtils.getCellData(i, 10);
        String bioStateTxt = ExcelUtils.getCellData(i, 11);
        String bioEmailTxt = ExcelUtils.getCellData(i, 12);
        String bioOrgTxt = ExcelUtils.getCellData(i, 13);
        String bioTitleTxt = ExcelUtils.getCellData(i, 14);
        String bioBiographyTxt = ExcelUtils.getCellData(i, 15);
        String bioLinkedinTxt = ExcelUtils.getCellData(i, 16);
        String bioFacebookTxt = ExcelUtils.getCellData(i, 17);
        String bioTwitterTxt = ExcelUtils.getCellData(i, 18);
        String bioWebTxt = ExcelUtils.getCellData(i, 19);
        String uRole = ExcelUtils.getCellData(i, 23);
        String appLocName = ExcelUtils.getCellData(i, 24);
        String expRes = ExcelUtils.getCellData(i, 20);
		
		/*lp.login(uName, pwd);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnMainMenu();*/
        toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
		/*mi.clickOnNavigation();
		mi.clickOnProgram();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ld.clickOnLandingMP();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnselectedprogram(pgmNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnManageMashupicon(pgmNameTxt);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		/*mls.clickOnAddLS();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mls.selectaTool(lsToolName);
		mls.enterLSName(lsNameTxt);
		mls.enterLsDesc(lsDescTxt);
		mls.clickOnSaveButton();
		mls.clickOnConfigButton();*/
		/*try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		mi.fetchAppTool(appLocName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mi.SelectManageFaceOfTool(toolName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		bio.clickOnBioSelectFile(bioFile);
		/*try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+bioFile);*/
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String imgFilSaved = bio.fetchBioImgSavedMsg(); // Checking Toast
		System.out.println(imgFilSaved);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		bio.clickOnClearImgBioButton();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String imgFilDeleted = bio.fetchBioImgDeletedMsg(); // Checking Toast
		System.out.println(imgFilDeleted);
		
		// Here we start filling the tool
		bio.clickOnBioSelectFile(bioFile);
		/*try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+bioFile);*/
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//sli.enterSlideDisplayName(sliDName);
		bio.enterBioFirstName(bioFNameTxt);
		bio.enterBioMiddleName(bioMNameTxt);
		bio.enterBioLastName(bioLNameTxt);
		bio.clickOnBioCountry();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		bio.clickOnBioSelectCt();
		bio.enterCity(bioCityTxt);
		bio.enterBioState(bioStateTxt);	
		bio.enterBioEmail(bioEmailTxt);
		bio.enterBioOrganization(bioOrgTxt);
		bio.enterBioTitle(bioTitleTxt);
		bio.clickOnBioAddPosition();
		//bio.clickOnAddressProfile();
		//pro.enterProAddress(proAddressTxt);
		//bio.clickOnBioBiographyButton();
		bio.enterBioBiography(bioBiographyTxt);
		bio.enterBioLinks(bioLinkedinTxt);
		bio.enterBioFacebook(bioFacebookTxt);
		bio.enterBioTwitter(bioTwitterTxt);
		bio.enterBioWebsite(bioWebTxt);
		//pro.enterProLinks(proLinksTxt);
		bio.clickOnBioSaveButton();
		
		String actRes = bio.fetchBioSavedMsg();
		System.out.println(actRes);
		ExcelUtils.setCellData(actRes, i, 21, Constant.File_TestData_Biography);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 22,Constant.File_TestData_Biography);
		}
		else 
		{
			ExcelUtils.setCellData("Fail", i, 22,Constant.File_TestData_Biography);
		}
		
		
		}
	}
}
