package education.volute.profile_testscripts;

import java.io.IOException;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Portfolio;
import education.volute.pages.Biography;
//import education.volute.pages.Collaborate;
import education.volute.pages.LoginPage;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;

public class Profile_TC_01 extends SuperTestScript

{
	//@Test
	public void testProfile_TC_01() throws Exception 
	
	{
		LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		MenuItems mi = new MenuItems();
		Biography bio = new Biography();
		Portfolio pro = new Portfolio();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Profile, "AddProfile2");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String uName = ExcelUtils.getCellData(i, 1);
        String pwd = ExcelUtils.getCellData(i, 2);
        String proFile = ExcelUtils.getCellData(i, 3);
        String proFNameTxt = ExcelUtils.getCellData(i, 4);
        String proMNameTxt = ExcelUtils.getCellData(i, 5);
        String proLNameTxt = ExcelUtils.getCellData(i, 6);   
        String proPhoneTxt = ExcelUtils.getCellData(i, 7);
        String proEmailTxt = ExcelUtils.getCellData(i, 8);
        String proOrgTxt = ExcelUtils.getCellData(i, 9);
        String proTitleTxt = ExcelUtils.getCellData(i, 10);
        String proAddressTxt = ExcelUtils.getCellData(i, 11);
        String proStateTxt = ExcelUtils.getCellData(i, 12);
        String proCityTxt = ExcelUtils.getCellData(i, 13);
        String proZipTxt = ExcelUtils.getCellData(i, 14);
        String proBiographyTxt = ExcelUtils.getCellData(i, 15);
        String proLinkedinTxt = ExcelUtils.getCellData(i, 16);
        String proFacebookTxt = ExcelUtils.getCellData(i, 17);
        String proTwitterTxt = ExcelUtils.getCellData(i, 18);
        String proWebTxt = ExcelUtils.getCellData(i, 19);
        //String proLinksTxt = ExcelUtils.getCellData(i, 9);
        String expRes = ExcelUtils.getCellData(i, 20);
		
		lp.login(uName, pwd);
		lp.logibutton();
		try {
			Thread.sleep(20000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		//nt.clickOnPortfolioIcon();
		
		/*mi.goToProfile();
		try {
			Thread.sleep(7000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		
		pro.clickOnManageProfile();		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		bio.clickOnBioSelectFile(proFile);
		/*try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+proFile);*/
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String imgFilSaved = bio.fetchBioImgSavedMsg(); // Checking Toast
		System.out.println(imgFilSaved);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		pro.clickOnClearImgProButton();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String imgFilDeleted = bio.fetchBioImgSavedMsg(); // Checking Toast
		System.out.println(imgFilDeleted);
		
		// Here we start filling the tool
		bio.clickOnBioSelectFile(proFile);
		/*try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+proFile);*/
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		pro.clearProFirstName();
		pro.enterProFirstName(proFNameTxt);
		pro.enterProMiddleName(proMNameTxt);
		pro.clearProLastName();
		pro.enterProLastName(proLNameTxt);
		pro.enterProPhone(proPhoneTxt);
		pro.clearProEmail();
		pro.enterProEmail(proEmailTxt);
		bio.enterBioOrganization(proOrgTxt);
		bio.enterBioTitle(proTitleTxt);
		pro.clickOnAddPosition();
		pro.enterProAddress(proAddressTxt);
		bio.enterBioState(proStateTxt);
		bio.enterCity(proCityTxt);
		pro.enterZipCode(proZipTxt);
		pro.clickOnCountryProfile();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		bio.clickOnBioSelectCt();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//pro.clickOnCountryProfile();
		//pro.countryName();
		pro.enterProBiography(proBiographyTxt);
		bio.enterBioLinks(proLinkedinTxt);
		bio.enterBioFacebook(proFacebookTxt);
		bio.enterBioTwitter(proTwitterTxt);
		bio.enterBioWebsite(proWebTxt);
		pro.clickOnSaveProfile();
		
		String actRes = pro.fetchProfileSuccessMsg();
		System.out.println(actRes);
		ExcelUtils.setCellData(actRes, i, 21, Constant.File_TestData_Profile);
		try {
    		Thread.sleep(3000);
    	} catch (InterruptedException e1) {
    		// TODO Auto-generated catch block
    		e1.printStackTrace();
    	}
		
		nt.clickOnPortfolioIcon();
    	try {
    		Thread.sleep(3000);
    	} catch (InterruptedException e1) {
    		// TODO Auto-generated catch block
    		e1.printStackTrace();
    	}
    	
    	mi.clickOnLogout();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 22,Constant.File_TestData_Profile);
		}
		else 
		{
			ExcelUtils.setCellData("Fail", i, 22,Constant.File_TestData_Profile);
		}
		
		
		}
	}
}