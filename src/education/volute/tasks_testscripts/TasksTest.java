package education.volute.tasks_testscripts;



public class TasksTest {

	
	public void TasksAdd(String Role) throws Exception{
		Tasks_TC_01 ts = new Tasks_TC_01();	
		ts.testTasks_TC_01(Role);
	}
	
	public void TasksEdit(String Role) throws Exception{
		Tasks_TC_04 ts = new Tasks_TC_04();	
		ts.testTasks_TC_04(Role);
	}
	
	public void TaskStrike(String Role){
		Tasks_TC_03 ts = new Tasks_TC_03();	
		ts.testTasks_TC_03(Role);
	}
	
	public void TasksDelete(String Role){
		Tasks_TC_02 ts = new Tasks_TC_02();	
		ts.testTasks_TC_02(Role);
	}
}
