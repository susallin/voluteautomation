package education.volute.tasks_testscripts;

import org.testng.Assert;

//import java.io.IOException;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Landing;
import education.volute.pages.LoginPage;
//import education.volute.pages.ManageLearningSpaces;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.SocialHub;
import education.volute.pages.NavigatorTool;
import education.volute.pages.Tasks;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class Tasks_TC_02 extends SuperTestScript {

	//@Test
	public void testTasks_TC_02(String Role) 
	
	{
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Task, "DeleteTasks3");
		
		 //LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		//SocialHub mj = new SocialHub();
		MenuItems mi = new MenuItems();
		SoftAssertCheck soft = new SoftAssertCheck();
		ReadCsvFiles csv = new ReadCsvFiles();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		//Landing ld = new Landing();
		//ManagePrograms mp = new ManagePrograms();
		//ManageLearningSpaces mls = new ManageLearningSpaces();
		Tasks tas = new Tasks();
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String toolDest = ExcelUtils.getCellData(i, 1);
	    String actName = ExcelUtils.getCellData(i, 2);
	    String subActName = ExcelUtils.getCellData(i, 3);
	    String pgmNameTxt = ExcelUtils.getCellData(i, 4);
        String tasName = ExcelUtils.getCellData(i, 6);
        //String pgmNameTxt = ExcelUtils.getCellData(i, 3);
        //String lsNameTxt = ExcelUtils.getCellData(i, 4);
        //String lsNameTxt = ExcelUtils.getCellData(i, 5);
        //String lsDescTxt = ExcelUtils.getCellData(i, 6);
        //String sliDName = ExcelUtils.getCellData(i, 7);
        //String sliFileName = ExcelUtils.getCellData(i, 5);
        //String sliSName = ExcelUtils.getCellData(i, 6);
        //String sliFilName = ExcelUtils.getCellData(i, 9);
        String uRole = ExcelUtils.getCellData(i, 11);
        String appLocName = ExcelUtils.getCellData(i, 12);
        String expRes = ExcelUtils.getCellData(i, 8);
		
		/*toolD.login(uName, pwd);
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnMainMenu();*/
        toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
		//mi.clickOnNavigation();
		//mi.clickOnProgram();
		//ld.clickOnLandingMP();
		/*try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnselectedprogram(pgmNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnManageMashupicon(pgmNameTxt);
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*mls.selectaLearningSpace(lsNameTxt);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mls.clickOnEditButton(lsNameTxt);
		mls.clickOnConfigButton();*/
		/*nt.clickOnJourney();
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mj.clickOnLaunchAct(actName);*/
		/*try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		mi.fetchAppTool(appLocName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		tas.clickOnDeleteButtonTasks(tasName);
		tas.clickOnDeleteYesTask();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String actRes = tas.fetchTaskDeleteMsg();
		System.out.println(actRes);
		
		String tasShows1 = tas.fetchTasShows(tasName);
		System.out.println(tasShows1);
		if(tasShows1.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking if task is deleted");
			System.out.println("The test case passed, task does not show.");
		}else{
			soft.assertTrue(false, "The test case failed, task show.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		nt.refreshPage();
		/*try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		mi.fetchAppTool(appLocName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// This will determine if Value is still showing or not.
		String tasShows = tas.fetchTasShows(tasName);
		System.out.println(tasShows);
		if(tasShows.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking if task is deleted");
			System.out.println("The test case passed, task does not show.");
		}else{
			soft.assertTrue(false, "The test case failed, task shows.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    
		ExcelUtils.setCellData(tasShows, i, 7, Constant.File_TestData_Task);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		ExcelUtils.setCellData(actRes, i, 9, Constant.File_TestData_Task);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 10,Constant.File_TestData_Task);
			//Assert.assertEquals(ExcelUtils.getCellData(i, 7),"pass");
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 10,Constant.File_TestData_Task);
			//Assert.assertEquals(ExcelUtils.getCellData(i, 7),"pass");
		}
		
		
		}
		soft.assertAll();
		}
}
