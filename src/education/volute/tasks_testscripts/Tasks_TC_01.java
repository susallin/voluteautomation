package education.volute.tasks_testscripts;

import java.util.Iterator;

import org.testng.Assert;
import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.Tasks;
//import education.volute.pages.ManageLearningSpaces;
import education.volute.pages.ManagePrograms;
import education.volute.pages.Landing;
import education.volute.pages.MenuItems;
import education.volute.pages.SocialHub;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.ToolDestAndCollections;

public class Tasks_TC_01 extends SuperTestScript {
	//@Test
	public void testTasks_TC_01(String Role) throws Exception 		
	{
		//LoginPage lp = new LoginPage();
		//NavigatorTool nt = new NavigatorTool();
		//SocialHub mj = new SocialHub();
		ReadCsvFiles csv = new ReadCsvFiles();
		//MenuItems mi = new MenuItems();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		MenuItems mi = new MenuItems();
		//Landing ld = new Landing();
		//ManagePrograms mp = new ManagePrograms();
		//ManageLearningSpaces mls = new ManageLearningSpaces();
		Tasks tas = new Tasks();
					
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Task, "AddTasks3");
			
			for(int i=1; i<=ExcelUtils.getRowcount(); i++)
			{
			
			//String uName = ExcelUtils.getCellData(i, 1);
	        //String pwd = ExcelUtils.getCellData(i, 2);
	        //String pgmNameTxt = ExcelUtils.getCellData(i, 3);
	        //String lsToolName = ExcelUtils.getCellData(i, 4);
	        //String lsNameTxt = ExcelUtils.getCellData(i, 5);
	        //String lsDescTxt = ExcelUtils.getCellData(i, 6);
	        //String taskDName = ExcelUtils.getCellData(i, 7);
			String toolDest = ExcelUtils.getCellData(i, 1);
	        String actName = ExcelUtils.getCellData(i, 2);
	        String subActName = ExcelUtils.getCellData(i, 3);
	        String pgmNameTxt = ExcelUtils.getCellData(i, 4);
	        String tasFilNames = ExcelUtils.getCellData(i, 6);
	        //String typTask = ExcelUtils.getCellData(i, 5);
	        //String typTask2 = ExcelUtils.getCellData(i, 6);
	        String uRole = ExcelUtils.getCellData(i, 10);
	        String appLocName = ExcelUtils.getCellData(i, 11);
	        String expRes = ExcelUtils.getCellData(i, 7);
			
	        /*toolD.login(uName, pwd);
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			//nt.clickOnMainMenu();
	        toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
			//mi.clickOnNavigation();
			//mi.clickOnProgram();
			//ld.clickOnLandingMP();
			/*try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mp.clickOnselectedprogram(pgmNameTxt);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mp.clickOnManageMashupicon(pgmNameTxt);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/*mls.clickOnAddLS();
			mls.selectaTool(lsToolName);
			mls.enterLSName(lsNameTxt);
			mls.enterLsDesc(lsDescTxt);
			mls.clickOnSaveButton();
			mls.clickOnConfigButton();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			//tas.clickOnTasksManage();
			/*try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			/*nt.clickOnJourney();
			try {
				Thread.sleep(8000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mj.clickOnLaunchAct(actName);*/
	        
			/*try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			mi.fetchAppTool(appLocName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Iterator<String[]> FileName = csv.ReadList(tasFilNames);
			
			if(FileName.hasNext())
				FileName.next();
			
			while(FileName.hasNext()){
				int n = 0;
				String[] Name = FileName.next();
				
				String typTask = Name[n];
				try {
					Thread.sleep(4000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				tas.clickOnTasksAddBar();
				tas.enterTypeTask(typTask);
				tas.clickOnTasksAddButton();
			}
			
			String actRes = tas.fetchTaskAddMsg();
			System.out.println(actRes);
			ExcelUtils.setCellData(actRes, i, 8, Constant.File_TestData_Task);
			
			Boolean status = ValidationLib.verifyMsg(expRes, actRes);
			
			if(status)
			{
				ExcelUtils.setCellData("pass", i, 9,Constant.File_TestData_Task);
				//Assert.assertEquals(ExcelUtils.getCellData(i, 9),"pass");
			}
			else 
		{
				ExcelUtils.setCellData("Fail", i, 9,Constant.File_TestData_Task);
				//Assert.assertEquals(ExcelUtils.getCellData(i, 9),"pass");
			}
			
			
			}
			
		}
}

