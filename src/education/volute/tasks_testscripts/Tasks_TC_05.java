package education.volute.tasks_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.NavigatorTool;
import education.volute.pages.SocialHub;
import education.volute.pages.Tasks;
import utilities.Constant;

public class Tasks_TC_05 {

	@Test
	public void testTasks_TC_05() 		
	{
					
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Task, "AddTasks2");
			
			for(int i=1; i<=ExcelUtils.getRowcount(); i++)
			{
			
			String uName = ExcelUtils.getCellData(i, 1);
	        String pwd = ExcelUtils.getCellData(i, 2);
	        //String pgmNameTxt = ExcelUtils.getCellData(i, 3);
	        //String lsToolName = ExcelUtils.getCellData(i, 4);
	        //String lsNameTxt = ExcelUtils.getCellData(i, 5);
	        //String lsDescTxt = ExcelUtils.getCellData(i, 6);
	        //String taskDName = ExcelUtils.getCellData(i, 7);
	        String actName = ExcelUtils.getCellData(i, 3);
	        String typTask = ExcelUtils.getCellData(i, 5);
	        String typTask2 = ExcelUtils.getCellData(i, 6);
	        String expRes = ExcelUtils.getCellData(i, 7);
	        
	        LoginPage lp = new LoginPage();
			NavigatorTool nt = new NavigatorTool();
			SocialHub mj = new SocialHub();
			//MenuItems mi = new MenuItems();
			//Landing ld = new Landing();
			//ManagePrograms mp = new ManagePrograms();
			//ManageLearningSpaces mls = new ManageLearningSpaces();
			Tasks tas = new Tasks();
			
			lp.login(uName, pwd);
			lp.logibutton();
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			nt.clickOnMainMenu();
			//mi.clickOnNavigation();
			//mi.clickOnProgram();
			//ld.clickOnLandingMP();
			/*try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mp.clickOnselectedprogram(pgmNameTxt);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mp.clickOnManageMashupicon(pgmNameTxt);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/*mls.clickOnAddLS();
			mls.selectaTool(lsToolName);
			mls.enterLSName(lsNameTxt);
			mls.enterLsDesc(lsDescTxt);
			mls.clickOnSaveButton();
			mls.clickOnConfigButton();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			//tas.clickOnTasksManage();
			/*try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			nt.clickOnJourney();
			try {
				Thread.sleep(8000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mj.clickOnLaunchAct(actName);
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			tas.clickOnTasksAddBar();
			tas.enterTypeTask(typTask);
			tas.clickOnTasksAddButton();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String actRes = tas.fetchTaskAddMsg();
			System.out.println(actRes);
			ExcelUtils.setCellData(actRes, i, 8, Constant.File_TestData_Task);
			
			Boolean status = ValidationLib.verifyMsg(expRes, actRes);
			
			if(status)
			{
				ExcelUtils.setCellData("pass", i, 9,Constant.File_TestData_Task);
			}
			else 
		{
				ExcelUtils.setCellData("Fail", i, 9,Constant.File_TestData_Task);
			}
			
			
			}
			
		}
}
