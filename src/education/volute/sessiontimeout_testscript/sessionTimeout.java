package education.volute.sessiontimeout_testscript;

import java.io.IOException;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Landing;
import education.volute.pages.LoginPage;
import education.volute.pages.ManageLearningSpaces;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
//import education.volute.pages.sessiontimeout_testscript;
import utilities.Constant;

public class sessionTimeout extends SuperTestScript {
	
	@Test
	public void sessionTimeout () 
	
	{
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_SessionTO, "SessionTimeout");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String uName = ExcelUtils.getCellData(i, 1);
        String pwd = ExcelUtils.getCellData(i, 2);
        String pgmNameTxt = ExcelUtils.getCellData(i, 3);
        String lsToolName = ExcelUtils.getCellData(i, 4);
        String lsNameTxt = ExcelUtils.getCellData(i, 5);
        String lsDescTxt = ExcelUtils.getCellData(i, 6);
        //String sliDName = ExcelUtils.getCellData(i, 7);
        String sliSName = ExcelUtils.getCellData(i, 8);
        //String sliFilName = ExcelUtils.getCellData(i, 9);
        String expRes = ExcelUtils.getCellData(i, 10);
        
        LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		MenuItems mi = new MenuItems();
		Landing ld = new Landing();
		ManagePrograms mp = new ManagePrograms();
		ManageLearningSpaces mls = new ManageLearningSpaces();
		//Slides sli = new Slides();
		
		lp.login(uName, pwd);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		while(true) {
			nt.clickOnHomeIcon();
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			nt.clickOnProfileIcon();
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			nt.clickOnLandingIcon();
			try {
				Thread.sleep(15000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		
		
		}
		
		}

}
