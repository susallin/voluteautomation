package education.volute.collaborate_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Collaborate;
import education.volute.pages.LoginPage;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import education.volute.pages.SocialHub;
import utilities.Constant;
import utilities.SoftAssertCheck;

public class Group_TC_02 extends SuperTestScript
{

	//@Test
	public void testGroup_TC_02() 
	
	{
		LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		MenuItems mi = new MenuItems();
		SoftAssertCheck soft = new SoftAssertCheck();
		//SocialHub mj = new SocialHub();
		Collaborate col = new Collaborate();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Collaborate, "AddGroup2");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String uName = ExcelUtils.getCellData(i, 1);
        String pwd = ExcelUtils.getCellData(i, 2);
        String grpNameTxt = ExcelUtils.getCellData(i, 3);
        String grpDescTxt = ExcelUtils.getCellData(i, 4);
        String expRes = ExcelUtils.getCellData(i, 5);
	
		
		/*lp.login(uName, pwd);
		lp.logibutton();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnMainMenu();
		//mi.clickOnNavigation();
		//mi.clickOnCollaborate();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnMenuTab();
		mi.clickOnGroup();
		*/
		//col.clickOnGroupsTab();
		col.clickOnAddGroupButton();
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		col.enterGropName(grpNameTxt);
		col.enterGropDesc(grpDescTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		col.selectMemebrsforGroup();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		col.clickOnSaveGroup();
		
		String actRes = col.fetchGroupShows(grpNameTxt);
		System.out.println(actRes);
		
		if(actRes.equalsIgnoreCase("Found")){
			soft.assertTrue(true, "Checking Content shows on the tool");
			System.out.println("The test case passed, the content is showing on the tool.");
		}else{
			soft.assertTrue(false, "The test case failed, the content not is showing on the tool.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ExcelUtils.setCellData(actRes, i, 6, Constant.File_TestData_Collaborate );
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 7,Constant.File_TestData_Collaborate);
		}
		else 
		{
			ExcelUtils.setCellData("Fail", i, 7,Constant.File_TestData_Collaborate);
		}
		
		
		}
		
		}
}
