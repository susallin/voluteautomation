package education.volute.collaborate_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Collaborate;
import education.volute.pages.LoginPage;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;

public class AddRoomWithIndividuals_TC_03 extends SuperTestScript
{
	@Test
	public void testAddRoomWithIndividuals_TC_03()
	
	{
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Collaborate, "TC_03");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String uName = ExcelUtils.getCellData(i, 1);
        String pwd = ExcelUtils.getCellData(i, 2);
        String roomNameTxt = ExcelUtils.getCellData(i, 3);
        String roomDescTxt = ExcelUtils.getCellData(i, 4);
        String expRes = ExcelUtils.getCellData(i, 5);
        
        LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		MenuItems mi = new MenuItems();
		Collaborate col = new Collaborate();
	
		
		lp.login(uName, pwd);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnMainMenu();
		mi.clickOnNavigation();
		mi.clickOnCollaborate();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		col.clickOnRoomsTab();
		col.clickOnAddRoomButton();
		col.enterRoomName(roomNameTxt);
		col.enterroomDesc(roomDescTxt);
		col.clickOnAssignIndividualsRadio();
		col.selectIndividualsforRoom();
		col.clickOnSaveRoom();
		
		String actRes = col.fetchRoomSuccessMsg();
		System.out.println(actRes);
		ExcelUtils.setCellData(actRes, i, 6, Constant.File_TestData_Collaborate );
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 7,Constant.File_TestData_Collaborate);
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 7,Constant.File_TestData_Collaborate);
		}
		
		
		}
		
		}

}



