package education.volute.collaborate_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Collaborate;
import education.volute.pages.LoginPage;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.SoftAssertCheck;

public class Group_TC_04 extends SuperTestScript
{

	//@Test
	public void testGroup_TC_04(String str) 
	
	{
		LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		MenuItems mi = new MenuItems();
		Collaborate col = new Collaborate();
		SoftAssertCheck soft = new SoftAssertCheck();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Collaborate, str);
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String uName = ExcelUtils.getCellData(i, 1);
        String pwd = ExcelUtils.getCellData(i, 2);
        String grpName = ExcelUtils.getCellData(i, 3);
        String expRes = ExcelUtils.getCellData(i, 4);
	
		
		/*lp.login(uName, pwd);
		lp.logibutton();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnMainMenu();
		//mi.clickOnNavigation();
		//mi.clickOnCollaborate();
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnMenuTab();
		mi.clickOnGroup();
		//col.clickOnGroupsTab();*/
		col.clickOnDeleteGroupCollab(grpName);
		col.clickOnDeleteYesCollab();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String actRes = col.fetchGroupShows(grpName);
		System.out.println(actRes);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(actRes.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking Content does not shows on the tool");
			System.out.println("The test case passed, the content does not show on the tool.");
		}else{
			soft.assertTrue(false, "The test case failed, the content shows on the tool.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    
	    nt.refreshPage();
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String actRes1 = col.fetchGroupShows(grpName);
		System.out.println(actRes1);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(actRes1.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking Content does not shows on the tool");
			System.out.println("The test case passed, the content does not show on the tool.");
		}else{
			soft.assertTrue(false, "The test case failed, the content shows on the tool.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ExcelUtils.setCellData(actRes, i, 5, Constant.File_TestData_Collaborate );
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 6,Constant.File_TestData_Collaborate);
		}
		else 
		{
			ExcelUtils.setCellData("Fail", i, 6,Constant.File_TestData_Collaborate);
		}
		
		
		}
		
		}
}
