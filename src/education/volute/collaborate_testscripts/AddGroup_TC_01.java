package education.volute.collaborate_testscripts;

import java.util.Iterator;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Collaborate;
import education.volute.pages.LoginPage;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import education.volute.pages.SocialHub;
import utilities.Constant;
import utilities.ReadCsvFiles;

public class AddGroup_TC_01 extends SuperTestScript

{
	//@Test
	public void testAddGroup_TC_01() throws Exception 
	
	{
		LoginPage lp = new LoginPage();
		ReadCsvFiles csv = new ReadCsvFiles();
		NavigatorTool nt = new NavigatorTool();
		MenuItems mi = new MenuItems();
		//SocialHub mj = new SocialHub();
		Collaborate col = new Collaborate();
			
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Collaborate, "TC_01");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String uName = ExcelUtils.getCellData(i, 1);
        String pwd = ExcelUtils.getCellData(i, 2);
        String grpNames = ExcelUtils.getCellData(i, 3);
        //String grpNameTxt = ExcelUtils.getCellData(i, 3);
        //String grpDescTxt = ExcelUtils.getCellData(i, 4);
        String expRes = ExcelUtils.getCellData(i, 5);
	
		
		/*lp.login(uName, pwd);
		lp.logibutton();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		nt.clickOnCollaborateIcon();
		//mi.clickOnNavigation();
		//mi.clickOnCollaborate();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//nt.clickOnMenuTab();
		//mi.clickOnGroup();
		
		Iterator<String[]> FileName = csv.ReadList(grpNames);
		
		if(FileName.hasNext())
			FileName.next();
		
		while(FileName.hasNext()){
			int n = 0;
			String[] Name = FileName.next();
			
			String GroupName = Name[n];
			String GroupDescription = Name[n+1];
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//col.clickOnGroupsTab();
			col.clickOnAddGroupButton();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			col.enterGropName(GroupName);
			col.enterGropDesc(GroupDescription);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			col.selectMemebrsforGroup();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			col.clickOnSaveGroup();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		String actRes = col.fetchGroupSuccessMsg();
		System.out.println(actRes);
		ExcelUtils.setCellData(actRes, i, 6, Constant.File_TestData_Collaborate );
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 7,Constant.File_TestData_Collaborate);
		}
		else 
		{
			ExcelUtils.setCellData("Fail", i, 7,Constant.File_TestData_Collaborate);
		}
		
		
		}
		
		}

}



