package education.volute.collaborate_testscripts;


public class GroupTest {

	public void CollaborateAddGroup() throws Exception{
		AddGroup_TC_01 col = new AddGroup_TC_01();
		col.testAddGroup_TC_01();
	}
	
	public void CollaborateEditGroup(){
		EditGroup_TC_05 col = new EditGroup_TC_05();
		col.testEditGroup_TC_05();
	}
	
	public void CollaborateDeleteGroup(){
		DeleteGroup_TC_07 col = new DeleteGroup_TC_07();
		col.testDeleteGroup_TC_07();
	}
	
	public void CollaborateAddGroup2(){
		Group_TC_02 col = new Group_TC_02();
		col.testGroup_TC_02();
	}
	
	public void CollaborateEditGroupShows(){
		Group_TC_03 col = new Group_TC_03();
		col.testGroup_TC_03();
	}
	
	public void CollaborateDeleteGroupShows(String str){
		Group_TC_04 col = new Group_TC_04();
		col.testGroup_TC_04(str);
	}
}
