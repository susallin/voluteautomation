package education.volute.photoshare_testscripts;

import java.util.Iterator;

import education.volute.common_lib.SuperTestScript;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import education.volute.pages.Photoshare;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.SearchElements;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class Photoshare_TC_04 extends SuperTestScript {

public void testPhotoshare_TC_04(String Role) throws Exception 
	
	{
		//LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		MenuItems mi = new MenuItems();
		//Landing ld = new Landing();
		//ManagePrograms mp = new ManagePrograms();
		//ManageLearningSpaces mls = new ManageLearningSpaces();
		ReadCsvFiles csv = new ReadCsvFiles();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		Photoshare ph = new Photoshare();
		SoftAssertCheck soft = new SoftAssertCheck();
		SearchElements sr = new SearchElements();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Photoshare, "DeletePhoto");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String toolDest = ExcelUtils.getCellData(i, 1);
	    String actName = ExcelUtils.getCellData(i, 2);
	    String subActName = ExcelUtils.getCellData(i, 3);
        String pgmNameTxt = ExcelUtils.getCellData(i, 4);
        String toolName = ExcelUtils.getCellData(i, 5);
        String PhFilNames = ExcelUtils.getCellData(i, 6);
        //String expRes = ExcelUtils.getCellData(i, 6);
        String uRole = ExcelUtils.getCellData(i, 7);
        String appLocName = ExcelUtils.getCellData(i, 8);
		
		
        toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
		
		/*try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		mi.fetchAppTool(appLocName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		// We start here...
		mi.SelectManageFaceOfTool(toolName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		/*id.clickAddIdeaButton();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		
		Iterator<String[]> FileName = csv.ReadList(PhFilNames);
		
		if(FileName.hasNext())
			FileName.next();
		
		while(FileName.hasNext()){
			int n = 0;
			String[] Name = FileName.next();
			
			String PhTitle = Name[n];
			//String newPhTitle = Name[n+1];
			String phFile = Name[n+1];
			
			ph.ClickOnEditAlbum(PhTitle);
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			/*ph.clearAlbumTitle();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			ph.enterAlbumTitlePhotoshare(newPhTitle);*/
			
			ph.selectRemovePhoto();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String confirmPhotoDelMsg = ph.fetchDeletedImgPhotoshareMsg();
			System.out.println(confirmPhotoDelMsg);
			
			if(confirmPhotoDelMsg.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking delete message Photo successfully deleted");
				System.out.println("The test case passed on verifying the toast message (Photo successfully deleted).");
			}else{
				soft.assertTrue(false, "The test case failed on verifying the toast message (Photo successfully deleted).\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    ph.moveToPhotoTag();
		    try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    String confirmPhotoDel = sr.fetchImageShows(phFile);
			System.out.println(confirmPhotoDel);
			
			if(confirmPhotoDel.equalsIgnoreCase("Not Found")){
				soft.assertTrue(true, "Checking Photo is deleted successfully");
				System.out.println("The test case passed, the content is deleted.");
			}else{
				soft.assertTrue(false, "The test case failed, the content still shows.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    ph.clickOnBackToListPhotoshare();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		
	}
	soft.assertAll();
		
 }
}
