package education.volute.photoshare_testscripts;

import java.util.Iterator;

import education.volute.common_lib.SuperTestScript;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.ideaboard_testscripts.IdeaCheck;
import education.volute.pages.IdeaBoard;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import education.volute.pages.Photoshare;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class Photoshare_TC_01 extends SuperTestScript{

public void testPhotoshare_TC_01(String Role) throws Exception 
	
	{
		//LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		MenuItems mi = new MenuItems();
		//Landing ld = new Landing();
		//ManagePrograms mp = new ManagePrograms();
		//ManageLearningSpaces mls = new ManageLearningSpaces();
		ReadCsvFiles csv = new ReadCsvFiles();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		Photoshare ph = new Photoshare();
		SoftAssertCheck soft = new SoftAssertCheck();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Photoshare, "ErrorPhotoshare");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String toolDest = ExcelUtils.getCellData(i, 1);
	    String actName = ExcelUtils.getCellData(i, 2);
	    String subActName = ExcelUtils.getCellData(i, 3);
        String pgmNameTxt = ExcelUtils.getCellData(i, 4);
        String toolName = ExcelUtils.getCellData(i, 5);
        String PhFilNames = ExcelUtils.getCellData(i, 6);
        //String expRes = ExcelUtils.getCellData(i, 6);
        String uRole = ExcelUtils.getCellData(i, 7);
        String appLocName = ExcelUtils.getCellData(i, 8);
		
		
        toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
		
		/*try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		mi.fetchAppTool(appLocName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		// We start here...
		mi.SelectManageFaceOfTool(toolName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		/*id.clickAddIdeaButton();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		
		Iterator<String[]> FileName = csv.ReadList(PhFilNames);
		
		if(FileName.hasNext())
			FileName.next();
		
		while(FileName.hasNext()){
			int n = 0;
			//boolean flag = true;
			String[] Name = FileName.next();
			
			String phTitle = Name[n];
			String phFile = Name[n+1];
			
			mi.SelectAdd(toolName);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			ph.clearAlbumTitle();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			ph.enterAlbumTitlePhotoshare(phTitle);
			
			ph.clickOnSelectFilePhotoshareButton(phFile);
			/*try {
				Thread.sleep(4000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+phFile);*/
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			/*while(flag){
				String result = nt.fetchProgressFile();
				
				if(result.equalsIgnoreCase("Found")){
					ph.clickOnBackToListPhotoshare();
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					flag = false;
				}
				else{
					flag = true;
				}
     		}*/
			
			String errorMsg = ph.fetchDUploadFilePhotoshareErrorMsg();
			System.out.println(errorMsg);
			
			if(errorMsg.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking error message Please populate all required fields");
				System.out.println("The test case passed on verifying the toast message (File is too large or file type is not supported).");
			}else{
				soft.assertTrue(false, "The test case failed on verifying the toast message (File is too large or file type is not supported).\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    ph.clickOnBackToListPhotoshare();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String confirmAlbumMsg = ph.fetchSaveAlbumPhotoshareMsg();
			System.out.println(confirmAlbumMsg);
			
			if(confirmAlbumMsg.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking error message Please populate all required fields");
				System.out.println("The test case passed on verifying the toast message (Album successfully saved).");
			}else{
				soft.assertTrue(false, "The test case failed on verifying the toast message (Album successfully saved).\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		
	}
	soft.assertAll();
		
 }
	
}
