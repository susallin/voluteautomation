package education.volute.photoshare_testscripts;

public class PhotoshareTest {

	public void ErrorPhotoshareCheck(String Role) throws Exception {
		Photoshare_TC_01 ph = new Photoshare_TC_01();	
		ph.testPhotoshare_TC_01(Role);
	}
	
	public void AddPhotoshare(String Role) throws Exception {
		Photoshare_TC_02 ph = new Photoshare_TC_02();	
		ph.testPhotoshare_TC_02(Role);
	}
	
	public void EditPhotoshare(String Role) throws Exception {
		Photoshare_TC_03 ph = new Photoshare_TC_03();	
		ph.testPhotoshare_TC_03(Role);
	}
	
	public void DeletePhoto(String Role) throws Exception {
		Photoshare_TC_04 ph = new Photoshare_TC_04();	
		ph.testPhotoshare_TC_04(Role);
	}
	
	public void DeleteAlbum(String Role) throws Exception {
		Photoshare_TC_05 ph = new Photoshare_TC_05();	
		ph.testPhotoshare_TC_05(Role);
	}
}
