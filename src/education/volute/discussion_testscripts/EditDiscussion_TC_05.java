package education.volute.discussion_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Discussion;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.ToolDestAndCollections;

public class EditDiscussion_TC_05 extends SuperTestScript {

	//@Test
	public void testEditDiscussion_TC_05(String Role) 
	
	{
		//LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		//MenuItems mi = new MenuItems();
		//Landing ld = new Landing();
		//ManagePrograms mp = new ManagePrograms();
		//ManageLearningSpaces mls = new ManageLearningSpaces();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		Discussion dis = new Discussion();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Discussion, "EditDiscussionShows");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String uName = ExcelUtils.getCellData(i, 1);
        String pwd = ExcelUtils.getCellData(i, 2);
        String toolDest = ExcelUtils.getCellData(i, 3);
        String actName = ExcelUtils.getCellData(i, 4);
        String subActName = ExcelUtils.getCellData(i, 5);
        String pgmNameTxt = ExcelUtils.getCellData(i, 6);
        String typeDisc = ExcelUtils.getCellData(i, 7);
        //String lsNameTxt = ExcelUtils.getCellData(i, 5);
        //String lsDescTxt = ExcelUtils.getCellData(i, 6);
        //String sliDName = ExcelUtils.getCellData(i, 7);
        String createdDisc = ExcelUtils.getCellData(i, 8);
        String discName = ExcelUtils.getCellData(i, 9);
        String editDiscName = ExcelUtils.getCellData(i, 10);
        //String sliSName = ExcelUtils.getCellData(i, 7);
        //String sliFilName = ExcelUtils.getCellData(i, 9);
        String uRole = ExcelUtils.getCellData(i, 14);
        String expRes = ExcelUtils.getCellData(i, 11);
		
		/*toolD.login(uName, pwd);
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		nt.clickOnDiscussionIcon(typeDisc);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
	    try {
	    	Thread.sleep(15000);
	    } catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		dis.clickOnEditDiscButton(typeDisc, createdDisc);
		dis.enterDiscNameTextBox(typeDisc, " "+discName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		dis.clickOnSaveDiscButton(typeDisc);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
			
		String actRes = dis.fetchDiscussionShows(typeDisc, editDiscName);
		System.out.println(actRes);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		dis.clickOnDisCloseflButton(typeDisc);
		
		nt.refreshPage();
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ExcelUtils.setCellData(actRes, i, 12, Constant.File_TestData_Discussion);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 13,Constant.File_TestData_Discussion);
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 13,Constant.File_TestData_Discussion);
		}
		
		
		}
		
		}
}
