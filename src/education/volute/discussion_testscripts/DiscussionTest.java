package education.volute.discussion_testscripts;



public class DiscussionTest {

	public void AddDiscussion(String txt, String Role) throws Exception{
		AddDiscussion_TC_01 dis = new AddDiscussion_TC_01();
		dis.testAddDiscussion_TC_01(txt, Role);
	}
	
	public void EditDiscussion(String Role){
		EditDiscussion_TC_02 dis = new EditDiscussion_TC_02();
		dis.testEditDiscussion_TC_02(Role);
	}
	
	public void DeleteDiscussion(String Role){
		DelDiscussion_TC_03 dis = new DelDiscussion_TC_03();
		dis.testDelDiscussion_TC_03(Role);
	}
	
	public void AddDiscussionShows(String Role){
		AddDiscussion_TC_04 dis = new AddDiscussion_TC_04();
		dis.testAddDiscussion_TC_04(Role);
	}
	
	public void EditDiscussionShows(String Role){
		EditDiscussion_TC_05 dis = new EditDiscussion_TC_05();
		dis.testEditDiscussion_TC_05(Role);
	}
	
	public void DeleteDiscussionShows(String str, String Role){
		DelDiscussion_TC_06 dis = new DelDiscussion_TC_06();
		dis.testDelDiscussion_TC_06(str, Role);
	}

}
