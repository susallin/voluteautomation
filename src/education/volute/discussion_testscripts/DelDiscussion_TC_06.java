package education.volute.discussion_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Discussion;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class DelDiscussion_TC_06 extends SuperTestScript 
{

	//@Test
	public void testDelDiscussion_TC_06(String str, String Role) 
	
	{
		//LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		//MenuItems mi = new MenuItems();
		//Landing ld = new Landing();
		//ManagePrograms mp = new ManagePrograms();
		//ManageLearningSpaces mls = new ManageLearningSpaces();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		SoftAssertCheck soft = new SoftAssertCheck();
		Discussion dis = new Discussion();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Discussion, str);
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String uName = ExcelUtils.getCellData(i, 1);
        String pwd = ExcelUtils.getCellData(i, 2);
        String toolDest = ExcelUtils.getCellData(i, 3);
        String actName = ExcelUtils.getCellData(i, 4);
        String subActName = ExcelUtils.getCellData(i, 5);
        String pgmNameTxt = ExcelUtils.getCellData(i, 6);
        String typeDisc = ExcelUtils.getCellData(i, 7);
        String createdDisc = ExcelUtils.getCellData(i, 8);
        String uRole = ExcelUtils.getCellData(i, 13);
        String expRes = ExcelUtils.getCellData(i, 10);
		
		/*toolD.login(uName, pwd);
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		nt.clickOnDiscussionIcon(typeDisc);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		dis.clickOnDeleteDiscButton(typeDisc, createdDisc);
		dis.clickOnDeleteYesDiscussion();
		try {
			Thread.sleep(7000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String actRes = dis.fetchDiscussionShows(typeDisc, createdDisc);
		System.out.println(actRes);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		dis.clickOnDisCloseflButton(typeDisc);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		// Checking to make sure is not showing
		nt.refreshPage();
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		nt.clickOnDiscussionIcon(typeDisc);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String disShows = dis.fetchDiscussionShows(typeDisc, createdDisc);
		System.out.println(disShows);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ExcelUtils.setCellData(disShows, i, 9, Constant.File_TestData_Discussion);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		dis.clickOnDisCloseflButton(typeDisc);
		
		ExcelUtils.setCellData(actRes, i, 11, Constant.File_TestData_Discussion);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 12,Constant.File_TestData_Discussion);
			soft.assertTrue(true, "Content");
			System.out.println("The test case passed on verifying Activity Toast.");
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 12,Constant.File_TestData_Discussion);
			soft.assertTrue(false, "The test case failed on verifying Activity Toast.\n");
		}
		
		
		}
		soft.assertAll();
		}
}
