package education.volute.discussion_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Discussion;
import education.volute.pages.LoginPage;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class DelDiscussion_TC_03 extends SuperTestScript {

	//@Test
	public void testDelDiscussion_TC_03(String Role) 
	
	{
		//LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		SoftAssertCheck soft = new SoftAssertCheck();
		//MenuItems mi = new MenuItems();
		//Landing ld = new Landing();
		//ManagePrograms mp = new ManagePrograms();
		//ManageLearningSpaces mls = new ManageLearningSpaces();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		Discussion dis = new Discussion();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Discussion, "DeleteDiscussion");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String uName = ExcelUtils.getCellData(i, 1);
        String pwd = ExcelUtils.getCellData(i, 2);
        String toolDest = ExcelUtils.getCellData(i, 3);
        String actName = ExcelUtils.getCellData(i, 4);
        String subActName = ExcelUtils.getCellData(i, 5);
        String pgmNameTxt = ExcelUtils.getCellData(i, 6);
        String typeDisc = ExcelUtils.getCellData(i, 7);
        String createdDisc = ExcelUtils.getCellData(i, 8);
        String uRole = ExcelUtils.getCellData(i, 12);
        String expRes = ExcelUtils.getCellData(i, 9);
		
		/*toolD.login(uName, pwd);
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		nt.clickOnDiscussionIcon(typeDisc);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		dis.clickOnDeleteDiscButton(typeDisc, createdDisc);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		dis.clickOnDeleteYesDiscussion();
			
		String actRes = dis.fetchDiscussionDeleteMsg();
		System.out.println(actRes);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String actRes2a = dis.fetchDiscussionShows(typeDisc, createdDisc);
		System.out.println(actRes2a);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(actRes2a.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking Content does not show on the tool");
			System.out.println("The test case passed on the content not showing on the tool.");
		}else{
			soft.assertTrue(false, "The test case failed, the content shows on the tool.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		dis.clickOnDisCloseflButton(typeDisc);
		
		nt.refreshPage();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		nt.clickOnDiscussionIcon(typeDisc);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String actRes2 = dis.fetchDiscussionShows(typeDisc, createdDisc);
		System.out.println(actRes2);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(actRes2.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking Content does not show on the tool");
			System.out.println("The test case passed on the content not showing on the tool.");
		}else{
			soft.assertTrue(false, "The test case failed, the content shows on the tool.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		dis.clickOnDisCloseflButton(typeDisc);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ExcelUtils.setCellData(actRes, i, 10, Constant.File_TestData_Discussion);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 11,Constant.File_TestData_Discussion);
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 11,Constant.File_TestData_Discussion);
		}
		
		
		}
		soft.assertAll();
		}
}
