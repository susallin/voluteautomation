package education.volute.discussion_testscripts;


import java.util.Iterator;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import education.volute.pages.Discussion;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class AddDiscussion_TC_01 extends SuperTestScript {

	//@Test
	public void testAddDiscussion_TC_01(String txt, String Role) throws Exception 
	
	{
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		SoftAssertCheck soft = new SoftAssertCheck();
		ReadCsvFiles csv = new ReadCsvFiles();
		NavigatorTool nt = new NavigatorTool();
		Discussion dis = new Discussion();
		MenuItems mi = new MenuItems();
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Discussion, txt);
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String uName = ExcelUtils.getCellData(i, 1);
        String pwd = ExcelUtils.getCellData(i, 2);
        String toolDest = ExcelUtils.getCellData(i, 3);
        String actName = ExcelUtils.getCellData(i, 4);
        String subActName = ExcelUtils.getCellData(i, 5);
        String pgmNameTxt = ExcelUtils.getCellData(i, 6);
        String typeDisc = ExcelUtils.getCellData(i, 7);
        String disName = ExcelUtils.getCellData(i, 8);
        String indNames = ExcelUtils.getCellData(i, 9);
        //String sliDName = ExcelUtils.getCellData(i, 7);
        //String sliFilName = ExcelUtils.getCellData(i, 9);
        String uRole = ExcelUtils.getCellData(i, 13);
        String expRes = ExcelUtils.getCellData(i, 10);
		
		/*toolD.login(uName, pwd);
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnDiscussionIcon(typeDisc);
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		dis.clickOnAddNewDiscButton(typeDisc);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		dis.enterDiscNameTextBox(typeDisc, disName);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		dis.clickOnAssignGroupsOrIndividualsButton(typeDisc);
		//mi.clickOnAssignUserButton();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		// here we can loop
		Iterator<String[]> FileName = csv.ReadList(indNames);
		
		if(FileName.hasNext())
			FileName.next();
		
		while(FileName.hasNext()){
			int n = 0;
			String[] Name = FileName.next();
			
			String Type = Name[n];
			String indgrpNameF = Name[n+1];
			
			if(Type.equalsIgnoreCase("Individual")) {
				
				mi.clickOnIndividualTab(typeDisc);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				mi.selectIndividual(typeDisc, indgrpNameF);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			} else{
				
				mi.clickOnGroupTab(typeDisc);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				mi.selectGroup(typeDisc, indgrpNameF);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		// here we end
		mi.clickOnDoneButton(typeDisc);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		dis.clickOnSaveDiscButton(typeDisc);
		
		
		String actRes = dis.fetchAddedMsgDiscussion();
		System.out.println(actRes);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String discShows1 = dis.fetchDiscussionShows(typeDisc, disName);
		System.out.println(discShows1);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(discShows1.equalsIgnoreCase("Found")){
			soft.assertTrue(true, "Checking Content shows on the tool");
			System.out.println("The test case passed, the content shows on the tool.");
		}else{
			soft.assertTrue(false, "The test case failed, the content does not show on the tool.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		dis.clickOnDisCloseflButton(typeDisc);
		
		nt.refreshPage();
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ExcelUtils.setCellData(actRes, i, 11, Constant.File_TestData_Discussion);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 12,Constant.File_TestData_Discussion);
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 12,Constant.File_TestData_Discussion);
		}
		
		
		}
		soft.assertAll();
		}
}
