package education.volute.discussion_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
//import education.volute.pages.Landing;
import education.volute.pages.LoginPage;
//import education.volute.pages.ManageLearningSpaces;
//import education.volute.pages.ManagePrograms;
//import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import education.volute.pages.Discussion;
import utilities.Constant;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class EditDiscussion_TC_02 extends SuperTestScript {

	//@Test
	public void testEditDiscussion_TC_02(String Role) 
	
	{
		//LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		SoftAssertCheck soft = new SoftAssertCheck();
		//MenuItems mi = new MenuItems();
		//Landing ld = new Landing();
		//ManagePrograms mp = new ManagePrograms();
		//ManageLearningSpaces mls = new ManageLearningSpaces();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		Discussion dis = new Discussion();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Discussion, "EditDiscussion");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String uName = ExcelUtils.getCellData(i, 1);
        String pwd = ExcelUtils.getCellData(i, 2);
        String toolDest = ExcelUtils.getCellData(i, 3);
        String actName = ExcelUtils.getCellData(i, 4);
        String subActName = ExcelUtils.getCellData(i, 5);
        String pgmNameTxt = ExcelUtils.getCellData(i, 6);
        String typeDisc = ExcelUtils.getCellData(i, 7);
        //String lsNameTxt = ExcelUtils.getCellData(i, 5);
        //String lsDescTxt = ExcelUtils.getCellData(i, 6);
        //String sliDName = ExcelUtils.getCellData(i, 7);
        String createdDisc = ExcelUtils.getCellData(i, 8);
        String discName = ExcelUtils.getCellData(i, 9);
        //String sliSName = ExcelUtils.getCellData(i, 7);
        //String sliFilName = ExcelUtils.getCellData(i, 9);
        String uRole = ExcelUtils.getCellData(i, 13);
        String expRes = ExcelUtils.getCellData(i, 10);
		
		/*toolD.login(uName, pwd);
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		nt.clickOnDiscussionIcon(typeDisc);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
        try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		dis.clickOnEditDiscButton(typeDisc, createdDisc);
		dis.enterDiscNameTextBox(typeDisc, discName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		dis.clickOnSaveDiscButton(typeDisc);
		
		String actRes = dis.fetchUpdatedMsgDiscussion();
		System.out.println(actRes);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String discShows1 = dis.fetchDiscussionShows(typeDisc, discName);
		System.out.println(discShows1);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(discShows1.equalsIgnoreCase("Found")){
			soft.assertTrue(true, "Checking Content shows on the tool");
			System.out.println("The test case passed, the content shows on the tool.");
		}else{
			soft.assertTrue(false, "The test case failed, the content does not show on the tool.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		dis.clickOnDisCloseflButton(typeDisc);
		
		nt.refreshPage();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		nt.clickOnDiscussionIcon(typeDisc);
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String discShows1a = dis.fetchDiscussionShows(typeDisc, discName);
		System.out.println(discShows1a);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(discShows1a.equalsIgnoreCase("Found")){
			soft.assertTrue(true, "Checking Content shows on the tool");
			System.out.println("The test case passed, the content shows on the tool.");
		}else{
			soft.assertTrue(false, "The test case failed, the content does not show on the tool.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		dis.clickOnDisCloseflButton(typeDisc);
		
		ExcelUtils.setCellData(actRes, i, 11, Constant.File_TestData_Discussion);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 12,Constant.File_TestData_Discussion);
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 12,Constant.File_TestData_Discussion);
		}
		
		
		}
		soft.assertAll();
		}
}
