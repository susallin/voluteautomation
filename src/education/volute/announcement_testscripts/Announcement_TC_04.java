package education.volute.announcement_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Announcements;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class Announcement_TC_04 extends SuperTestScript
{

	//@Test
	public void testAnnouncement_TC_04(String anFile, String Role) 
	
	{
		//LoginPage lp = new LoginPage();
		//NavigatorTool nt = new NavigatorTool();
		MenuItems mi = new MenuItems();
		SoftAssertCheck soft = new SoftAssertCheck();
		//Landing ld = new Landing();
		//ManagePrograms mp = new ManagePrograms();
		//ManageLearningSpaces mls = new ManageLearningSpaces();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		Announcements an = new Announcements();
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Announcements, anFile);
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		//String uName = ExcelUtils.getCellData(i, 1);
        //String pwd = ExcelUtils.getCellData(i, 2);
		String anName = ExcelUtils.getCellData(i, 1);
	    String anAuthor = ExcelUtils.getCellData(i, 2);
	    String anDesc = ExcelUtils.getCellData(i, 3);
	    String toolName = ExcelUtils.getCellData(i, 4);
	    String toolDest = ExcelUtils.getCellData(i, 18);
	    String actName = ExcelUtils.getCellData(i, 19);
	    String subActName = ExcelUtils.getCellData(i, 20);
        String pgmNameTxt = ExcelUtils.getCellData(i, 21);
        String uRole = ExcelUtils.getCellData(i, 22);
        String appLocName = ExcelUtils.getCellData(i, 23);
	    
	    String anShows = an.fetchManageFacesAnShows(anName);
	    System.out.println(anShows);

		ExcelUtils.setCellData(anShows, i, 5, Constant.File_TestData_Announcements);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();	
		}
		
		if(anShows.equalsIgnoreCase("Found")){
			soft.assertTrue(true, "Checking a content in Manage Face");
			System.out.println("The test case passed on verifying the Name on the Manage Face of Announcement tool.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the Name on the Manage Face of Announcement tool.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String anShows1 = an.fetchManageFacesImgAnShows(anName);
		System.out.println(anShows1);
		
		ExcelUtils.setCellData(anShows1, i, 6, Constant.File_TestData_Announcements);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();	
		}
		
		if(anShows1.equalsIgnoreCase("Found")){
			soft.assertTrue(true, "Checking a content in Manage Face");
			System.out.println("The test case passed on verifying the image on the Manage Face of Announcement tool.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the image on the Manage Face of Announcement tool.\n");
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		/*String anShows1a = an.fetchManageFacesUserInsight(User);
		System.out.println(anShows1a);
		
		if(anShows1a.equalsIgnoreCase("Found")){
			soft.assertTrue(true, "Checking a content in Manage Face");
			System.out.println("The test case passed on verifying, the Creator/User of content shows Manage Face of Announcement tool.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying, the Creator/User of content shows Manage Face of Announcement tool.\n");
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		
		mi.selectBackArrow(toolName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String anShows2 = an.fetchListFacesNameAnShows(anName);
		System.out.println(anShows2);
		
		ExcelUtils.setCellData(anShows2, i, 7, Constant.File_TestData_Announcements);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();	
		}
		
		if(anShows2.equalsIgnoreCase("Found")){
			soft.assertTrue(true, "Checking a content from list");
			System.out.println("The test case passed on verifying the Name on the list of files on face of Announcement tool.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the Name on the list of files on face of Announcement tool.\n");
		}
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String anShows3 = an.fetchListFacesPostedAnShows(anName);
		System.out.println(anShows3);
		
		ExcelUtils.setCellData(anShows3, i, 8, Constant.File_TestData_Announcements);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();	
		}
		
		if(anShows3.equalsIgnoreCase("Found")){
			soft.assertTrue(true, "Checking a content from list");
			System.out.println("The test case passed on verifying the Posted word on the list of files on face of Announcement tool.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the Posted word on the list of files on face of Announcement tool.\n");
		}
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String anShows4 = an.fetchListFacesUserAnShows(anName, anAuthor);
		System.out.println(anShows4);
		
		ExcelUtils.setCellData(anShows4, i, 9, Constant.File_TestData_Announcements);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();	
		}
		
		if(anShows4.equalsIgnoreCase("Found")){
			soft.assertTrue(true, "Checking a content from list");
			System.out.println("The test case passed on verifying the Name of the User on the list of files on face of Announcement tool.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the Name of the User on the list of files on face of Announcement tool.\n");
		}
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String anShows5 = an.fetchListFacesDescAnShows(anDesc);
		System.out.println(anShows5);
		
		ExcelUtils.setCellData(anShows5, i, 10, Constant.File_TestData_Announcements);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();	
		}
		
		if(anShows5.equalsIgnoreCase("Found")){
			soft.assertTrue(true, "Checking a content from list");
			System.out.println("The test case passed on verifying the Description on the list of files on face of Announcement tool.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the Description on the list of files on face of Announcement tool.\n");
		}
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String anShows6 = an.fetchListFacesImgAnShows(anName);
		System.out.println(anShows6);
		
		ExcelUtils.setCellData(anShows6, i, 11, Constant.File_TestData_Announcements);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();	
		}
		
		if(anShows6.equalsIgnoreCase("Found")){
			soft.assertTrue(true, "Checking a content from list");
			System.out.println("The test case passed on verifying the image on the list of files on face of Announcement tool.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the image on the list of files on face of Announcement tool.\n");
		}
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		an.clickOnListAn(anName);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String anShows7 = an.fetchDetailFacesNameAnShows(anName);
		System.out.println(anShows7);
		
		ExcelUtils.setCellData(anShows7, i, 12, Constant.File_TestData_Announcements);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();	
		}
		
		if(anShows7.equalsIgnoreCase("Found")){
			soft.assertTrue(true, "Checking a content once selected");
			System.out.println("The test case passed on verifying the Name on the list of files on face of Announcement tool.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the Name on the list of files on face of Announcement tool.\n");
		}
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String anShows8 = an.fetchDetailFacesImgAnShows(anName);
		System.out.println(anShows8);
		
		ExcelUtils.setCellData(anShows8, i, 13, Constant.File_TestData_Announcements);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();	
		}
		
		if(anShows8.equalsIgnoreCase("Found")){
			soft.assertTrue(true, "Checking a content once selected");
			System.out.println("The test case passed on verifying the Image of the selected file on face of Announcement tool.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the Image of the selected file on face of Announcement tool.\n");
		}
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String anShows9 = an.fetchDetailFacesPostedAnShows(anName);
		System.out.println(anShows9);
		
		ExcelUtils.setCellData(anShows9, i, 14, Constant.File_TestData_Announcements);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();	
		}
		
		if(anShows9.equalsIgnoreCase("Found")){
			soft.assertTrue(true, "Checking a content once selected");
			System.out.println("The test case passed on verifying the Posted word of the selected file on face of Announcement tool.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the Posted word of the selected file on face of Announcement tool.\n");
		}
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String anShows10 = an.fetchDetailFacesUserAnShows(anName, anAuthor);
		System.out.println(anShows10);
		
		ExcelUtils.setCellData(anShows10, i, 15, Constant.File_TestData_Announcements);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();	
		}
		
		if(anShows10.equalsIgnoreCase("Found")){
			soft.assertTrue(true, "Checking a content once selected");
			System.out.println("The test case passed on verifying the Name of the User of the selected file on face of Announcement tool.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the Name of the User of the selected file on face of Announcement tool.\n");
		}
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String anShows11 = an.fetchDetailFacesFormattedTimeAnShows(anName);
		System.out.println(anShows11);
		
		ExcelUtils.setCellData(anShows11, i, 16, Constant.File_TestData_Announcements);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();	
		}
		
		if(anShows11.equalsIgnoreCase("Found")){
			soft.assertTrue(true, "Checking a content once selected");
			System.out.println("The test case passed on verifying the Formatted Time of the selected file on face of Announcement tool.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the Formatted Time of the selected file on face of Announcement tool.\n");
		}
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String anShows12 = an.fetchDetailFacesDescAnShows(anDesc);
		System.out.println(anShows12);
		
		ExcelUtils.setCellData(anShows12, i, 17, Constant.File_TestData_Announcements);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();	
		}
		
		if(anShows12.equalsIgnoreCase("Found")){
			soft.assertTrue(true, "Checking a content once selected");
			System.out.println("The test case passed on verifying the description of the selected file on face of Announcement tool.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the description of the selected file on face of Announcement tool.\n");
		}
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		an.clickOnBackToListAn(anName);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
		
		mi.fetchAppTool(appLocName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		mi.SelectManageFaceOfTool(toolName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		}
		soft.assertAll();
		
		}
}
