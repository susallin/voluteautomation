package education.volute.announcement_testscripts;

import java.util.Iterator;

//import java.io.IOException;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Landing;
//import education.volute.pages.LoginPage;
//import education.volute.pages.ManageLearningSpaces;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import education.volute.pages.Announcements;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class Announcement_TC_02 extends SuperTestScript {

	//@Test
	public void testAnnouncement_TC_02(String Role) throws Exception 
	
	{
		//LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		MenuItems mi = new MenuItems();
		//Landing ld = new Landing();
		//ManagePrograms mp = new ManagePrograms();
		//ManageLearningSpaces mls = new ManageLearningSpaces();
		ReadCsvFiles csv = new ReadCsvFiles();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		Announcements an = new Announcements();
		SoftAssertCheck soft = new SoftAssertCheck();
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Announcements, "EditAnnouncements2");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		//String uName = ExcelUtils.getCellData(i, 1);
        //String pwd = ExcelUtils.getCellData(i, 2);
		String toolDest = ExcelUtils.getCellData(i, 1);
	    String actName = ExcelUtils.getCellData(i, 2);
	    String subActName = ExcelUtils.getCellData(i, 3);
        String pgmNameTxt = ExcelUtils.getCellData(i, 4);
	    String toolName = ExcelUtils.getCellData(i, 5);
        //String lsNameTxt = ExcelUtils.getCellData(i, 4);
        //String lsNameTxt = ExcelUtils.getCellData(i, 5);
        //String lsDescTxt = ExcelUtils.getCellData(i, 6);
        //String sliDName = ExcelUtils.getCellData(i, 7);
        String AnFileNames = ExcelUtils.getCellData(i, 6);
        //String sliFilName = ExcelUtils.getCellData(i, 9);
        String uRole = ExcelUtils.getCellData(i, 10);
        String appLocName = ExcelUtils.getCellData(i, 11);
        String expRes = ExcelUtils.getCellData(i, 7);
		
		/*lp.login(uName, pwd);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnMainMenu();
		mi.clickOnNavigation();*/
        toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
		/*mi.clickOnProgram();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ld.clickOnLandingMP();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnselectedprogram(pgmNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnManageMashupicon(pgmNameTxt);
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*mls.selectaLearningSpace(lsNameTxt);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mls.clickOnEditButton(lsNameTxt);
		mls.clickOnConfigButton();*/
		/*try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		mi.fetchAppTool(appLocName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mi.SelectManageFaceOfTool(toolName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Iterator<String[]> FileName = csv.ReadList(AnFileNames);
		
		if(FileName.hasNext())
			FileName.next();
		
		while(FileName.hasNext()){ //until filename is empty
			int n = 0;
			String[] Name = FileName.next();
			boolean flag = true;
			
			String addBackAn = Name[n];
			String nameBackButton = Name[n+1];
			String anName = Name[n+2];
			String File = Name[n+3];
			String anTitle = Name[n+4];
			String anDesc = Name[n+5];
			
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			an.clickOnAnEditButton(anName);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			an.clickOnAnClear();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			nt.clickOnDeletePgmYesButton();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			//an.clearAnTitle();
			an.enterAnTitleName(anTitle);
			//an.clearAnDesc();
			an.enterAnDescription(anDesc);
			
			an.clickOnAnSelectFile(File);
			
			while(flag){
				String result = nt.fetchProgressFile();
				
				if(result.equalsIgnoreCase("Found")){
					
					if(addBackAn.equalsIgnoreCase("yes")){
						mi.clickBackSaveListButton(nameBackButton);
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					} else{
						an.clickOnAnSave();
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
					flag = false;
				}
				else{
					flag = true;
				}
			}
			
		}
		
		//sli.enterSlideDisplayName(sliDName);
		//an.clickOnAnnUploaded(anFileName);
		//an.enterAnTitleName(anTitle);
		//an.enterAnDescription(anDesc);//sliFilName);
		
		//an.clickOnAnSave();
		
		String actRes = an.fetchAddedMsgAn();
		System.out.println(actRes);
		ExcelUtils.setCellData(actRes, i, 8, Constant.File_TestData_Announcements);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 9,Constant.File_TestData_Announcements);
			soft.assertTrue(true, "Toast");
			System.out.println("The test case passed on verifying Activity Toast.");
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 9,Constant.File_TestData_Announcements);
			soft.assertTrue(false, "The test case failed on verifying Activity Toast.\n");
		}
		
		
		}
		soft.assertAll();
		
		}
}
