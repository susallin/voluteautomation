package education.volute.announcement_testscripts;

public class AnnouncementTest {

	public void AnAdd(String Role) throws Exception{
		Announcement_TC_01 an = new Announcement_TC_01();	
		an.testAnnouncement_TC_01(Role);
	}
	
	public void AnEdit(String Role) throws Exception{
		Announcement_TC_02 an = new Announcement_TC_02();	
		an.testAnnouncement_TC_02(Role);
	}
	
	public void AnDelete(String Role) {
		Announcement_TC_03 an = new Announcement_TC_03();	
		an.testAnnouncement_TC_03(Role);
	}
	
	public void AnCheckContent(String anFile, String Role) {
		Announcement_TC_04 an = new Announcement_TC_04();	
		an.testAnnouncement_TC_04(anFile, Role);
	}
}
