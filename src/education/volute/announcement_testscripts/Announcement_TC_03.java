package education.volute.announcement_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Announcements;
import education.volute.pages.Landing;
//import education.volute.pages.LoginPage;
//import education.volute.pages.ManageLearningSpaces;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class Announcement_TC_03 extends SuperTestScript {

	//@Test
	public void testAnnouncement_TC_03(String Role) 
	
	{
		//LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		MenuItems mi = new MenuItems();
		//Landing ld = new Landing();
		//ManagePrograms mp = new ManagePrograms();
		//ManageLearningSpaces mls = new ManageLearningSpaces();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		Announcements an = new Announcements();
		SoftAssertCheck soft = new SoftAssertCheck();
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Announcements, "DeleteAnnouncements2");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		//String uName = ExcelUtils.getCellData(i, 1);
        //String pwd = ExcelUtils.getCellData(i, 2);
		String toolDest = ExcelUtils.getCellData(i, 1);
	    String actName = ExcelUtils.getCellData(i, 2);
	    String subActName = ExcelUtils.getCellData(i, 3);
	    String pgmNameTxt = ExcelUtils.getCellData(i, 4);
        String anFileName = ExcelUtils.getCellData(i, 6);
        String anFileName1 = ExcelUtils.getCellData(i, 7);
        //String lsNameTxt = ExcelUtils.getCellData(i, 4);
        //String lsNameTxt = ExcelUtils.getCellData(i, 5);
        //String lsDescTxt = ExcelUtils.getCellData(i, 6);
        //String sliDName = ExcelUtils.getCellData(i, 7);
        String uRole = ExcelUtils.getCellData(i, 12);
        String toolName = ExcelUtils.getCellData(i, 13);
        String appLocName = ExcelUtils.getCellData(i, 14);
        String expRes = ExcelUtils.getCellData(i, 9);
		
		/*lp.login(uName, pwd);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnMainMenu();*/
        toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
		/*mi.clickOnNavigation();
		mi.clickOnProgram();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ld.clickOnLandingMP();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnselectedprogram(pgmNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnManageMashupicon(pgmNameTxt);
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*mls.selectaLearningSpace(lsNameTxt);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mls.clickOnEditButton(lsNameTxt);
		mls.clickOnConfigButton();*/
		/*try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		mi.fetchAppTool(appLocName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		mi.SelectManageFaceOfTool(toolName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//sli.enterSlideDisplayName(sliDName);
		an.clickOnAnDeleteButton(anFileName);
		an.clickOnDeleteYesAn();
		
		String actRes = "Okay";//an.fetchDeletedMsgAn();
		System.out.println(actRes);
		
		// This will determine if Value is still showing or not.
		String actRes2 = an.fetchManageFacesAnShows(anFileName);
		System.out.println(actRes2);
		if(actRes2.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking if content still shows");
			System.out.println("The test case passed on verifying the content was deleted from Announcement tool on Manage.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the content was deleted from Announcement tool on Manage.\n");
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		an.clickOnAnDeleteButton(anFileName1);
		an.clickOnDeleteYesAn();
		
		String actRes1 = "Okay";//an.fetchDeletedMsgAn();
		System.out.println(actRes1);
		
		// This will determine if Value is still showing or not.
		String actRes2a = an.fetchManageFacesAnShows(anFileName1);
		System.out.println(actRes2a);
		if(actRes2a.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking if content still shows");
			System.out.println("The test case passed on verifying the content was deleted from Announcement tool on Manage.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the content was deleted from Announcement tool on Manage.\n");
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		mi.selectBackArrow(toolName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String actRes2b = an.fetchListFacesNameAnShows(anFileName);
		System.out.println(actRes2b);
		if(actRes2b.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking if content still shows");
			System.out.println("The test case passed on verifying the content was deleted from Announcement tool on Face.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the content was deleted from Announcement tool on Face.\n");
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String actRes2c = an.fetchListFacesNameAnShows(anFileName1);
		System.out.println(actRes2c);
		if(actRes2c.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking if content still shows");
			System.out.println("The test case passed on verifying the content was deleted from Announcement tool on Face.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the content was deleted from Announcement tool on Face.\n");
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		nt.refreshPage();
		
		mi.fetchAppTool(appLocName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String actRes2d = an.fetchListFacesNameAnShows(anFileName);
		System.out.println(actRes2d);
		if(actRes2d.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking if content still shows");
			System.out.println("The test case passed on verifying the content was deleted from Announcement tool on Face.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the content was deleted from Announcement tool on Face.\n");
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String actRes2e = an.fetchListFacesNameAnShows(anFileName1);
		System.out.println(actRes2e);
		if(actRes2e.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking if content still shows");
			System.out.println("The test case passed on verifying the content was deleted from Announcement tool on Face.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the content was deleted from Announcement tool on Face.\n");
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		mi.SelectManageFaceOfTool(toolName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// This will determine if Value is still showing or not.
		String anShows = an.fetchManageFacesAnShows(anFileName);
		System.out.println(anShows);
		if(anShows.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking if content still shows");
			System.out.println("The test case passed on verifying the content was deleted from Announcement tool on Manage.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the content was deleted from Announcement tool on Manage.\n");
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		// This will determine if Value is still showing or not.
		String anShowst = an.fetchManageFacesAnShows(anFileName1);
		System.out.println(anShowst);
		if(anShowst.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking if content still shows");
			System.out.println("The test case passed on verifying the content was deleted from Announcement tool on Manage.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the content was deleted from Announcement tool on Manage.\n");
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ExcelUtils.setCellData(anShows, i, 8, Constant.File_TestData_Announcements);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();	
		}
		
		ExcelUtils.setCellData(actRes, i, 10, Constant.File_TestData_Announcements);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 11,Constant.File_TestData_Announcements);
			soft.assertTrue(true, "Confirming");
			System.out.println("The test case passed on verifying Announcement.");
		}
		else 
		{
			ExcelUtils.setCellData("Fail", i, 11,Constant.File_TestData_Announcements);
			soft.assertTrue(false, "The test case failed on verifying Announcement.\n");
		}
		
		
		}
		soft.assertAll();
		
		}
}
