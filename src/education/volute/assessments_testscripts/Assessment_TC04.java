package education.volute.assessments_testscripts;

import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Assessments;
import education.volute.pages.MenuItems;
import utilities.Constant;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class Assessment_TC04 {
	
		//@Test
		public void testAssessment_TC04(String Role) 	
		{
			//LoginPage lp = new LoginPage();
			//NavigatorTool nt = new NavigatorTool();
			SoftAssertCheck soft = new SoftAssertCheck();
			MenuItems mi = new MenuItems();
			//Landing ld = new Landing();
			//ManagePrograms mp = new ManagePrograms();
			//ManageLearningSpaces mls = new ManageLearningSpaces();
			//SocialHub mj = new SocialHub();
			ToolDestAndCollections toolD = new ToolDestAndCollections();
			Assessments am = new Assessments();
			
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Assessments, "EnableAssessmentSurvey");
				
				for(int i=1; i<=ExcelUtils.getRowcount(); i++)
				{
				
				//String uName = ExcelUtils.getCellData(i, 1);
		        //String pwd = ExcelUtils.getCellData(i, 2);
				String toolDest = ExcelUtils.getCellData(i, 1);
		        String actName = ExcelUtils.getCellData(i, 2);
		        String subActName = ExcelUtils.getCellData(i, 3);
		        String pgmNameTxt = ExcelUtils.getCellData(i, 4);
		        //String lsToolName = ExcelUtils.getCellData(i, 4);
		        //String lsNameTxt = ExcelUtils.getCellData(i, 5);
		        //String lsDescTxt = ExcelUtils.getCellData(i, 6);
		        //String porDName = ExcelUtils.getCellData(i, 4);
		        String toolName = ExcelUtils.getCellData(i, 5);
		        String aUsername = ExcelUtils.getCellData(i, 6);
		        String aApi = ExcelUtils.getCellData(i, 7);
		        String aTab = ExcelUtils.getCellData(i, 8);
		        String surName = ExcelUtils.getCellData(i, 9);
		        String uRole = ExcelUtils.getCellData(i, 10);
		        String appLocName = ExcelUtils.getCellData(i, 11);
		        //String expRes = ExcelUtils.getCellData(i, 8);
				
				/*lp.login(uName, pwd);
				lp.logibutton();
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				nt.clickOnMainMenu();*/
		        toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
				/*mi.clickOnNavigation();
				mi.clickOnProgram();
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				ld.clickOnLandingMP();
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				mp.clickOnselectedprogram(pgmNameTxt);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				mp.clickOnManageMashupicon(pgmNameTxt);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				/*mls.clickOnAddLS();
				mls.selectaTool(lsToolName);
				mls.enterLSName(lsNameTxt);
				mls.enterLsDesc(lsDescTxt);
				mls.clickOnSaveButton();
				mls.clickOnConfigButton();*/
				/*try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				mj.clickOnLaunchAct(actName);*/
				/*try {
					Thread.sleep(15000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}*/
				mi.fetchAppTool(appLocName);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				mi.SelectManageFaceOfTool(toolName);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				//mi.clearDisplayName();
				//mi.enterDisplayName(porDName);
				mi.SelectConfigurationButton(toolName);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				am.clickEnableQualIntegrationButton();
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				am.clickSaveAssessmentButton();
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				am.clickEnableQualIntegrationButton();
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				am.clickSaveAssessmentButton();
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				String errorMsg1 = am.fetchErrorMsgA1();
				System.out.println(errorMsg1);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				if(errorMsg1.equalsIgnoreCase("Found")){
					soft.assertTrue(true, "Checking error message Please populate all required fields");
					System.out.println("The test case passed on verifying the toast message (Please populate all required fields).");
				}else{
					soft.assertTrue(false, "The test case failed on verifying the toast message (Please populate all required fields).\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    am.enterUsername(aUsername);
			    am.enterApi(aApi);
			    
			    am.clickSaveAssessmentButton();
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			    String saveMsg = am.fetchSaveMsgAssessment();
				System.out.println(saveMsg);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				if(saveMsg.equalsIgnoreCase("Found")){
					soft.assertTrue(true, "Checking message Configuration saved successfully");
					System.out.println("The test case passed on verifying the toast message (Configuration saved successfully).");
				}else{
					soft.assertTrue(false, "The test case failed on verifying the toast message (Configuration saved successfully).\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			    mi.SelectContentButton(toolName);
			    try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    am.clickOnSelectTab(aTab);
			    try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    /*am.clickOnSelectSurvey("Quiz 1");
			    try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}*/
			    
			    am.clickOnSelectSurvey(surName);
			    try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    am.clickSaveSurveyButton();
			    try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    String saveSurveyMsg = am.fetchSaveMsgSurvey();
				System.out.println(saveSurveyMsg);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				if(saveSurveyMsg.equalsIgnoreCase("Found")){
					soft.assertTrue(true, "Checking save Survey published successfully");
					System.out.println("The test case passed on verifying the toast message (Survey published successfully).");
				}else{
					soft.assertTrue(false, "The test case failed on verifying the toast message (Survey published successfully).\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    mi.selectBackArrow(toolName);
			    try {
					Thread.sleep(4000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    am.clickStartAssessmentButton();
			    try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
//			    am.clickNextSurveyButton();
//			    try {
//					Thread.sleep(10000);
//				} catch (InterruptedException e1) {
//					// TODO Auto-generated catch block
//					e1.printStackTrace();
//				}
				}
				
			}
}
