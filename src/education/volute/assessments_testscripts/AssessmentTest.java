package education.volute.assessments_testscripts;

public class AssessmentTest {

	public void CheckToastError1and3(String Role) {
		Assessment_TC01 an = new Assessment_TC01();	
		an.testAssessment_TC01(Role);
	}
	
	public void CheckToastError1and2(String Role) {
		Assessment_TC02 an = new Assessment_TC02();	
		an.testAssessment_TC02(Role);
	}
	
	public void EnableCheckSurveyToastError(String Role) {
		Assessment_TC03 an = new Assessment_TC03();	
		an.testAssessment_TC03(Role);
	}
	
	public void EnableTakeSurvey(String Role) {
		Assessment_TC04 an = new Assessment_TC04();	
		an.testAssessment_TC04(Role);
	}
}
