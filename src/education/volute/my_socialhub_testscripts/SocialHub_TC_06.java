package education.volute.my_socialhub_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.MenuItems;
import education.volute.pages.SocialHub;
import utilities.Constant;
import utilities.SoftAssertCheck;

public class SocialHub_TC_06 extends SuperTestScript
{
	//@Test
	public void testSocialHub_TC_06() //throws Exception 
	
	{
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_SocialHub, "LabelShows");
		
		// Instantiating method mi/nt to void method.
		// Calling Methods outside of for loop.
		LoginPage lp = new LoginPage();
		MenuItems mi = new MenuItems();
        //NavigatorTool nt = new NavigatorTool();
        SocialHub hb = new SocialHub();
        SoftAssertCheck soft = new SoftAssertCheck();
        
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		//String uName = ExcelUtils.getCellData(i, 1);
        //String pwd = ExcelUtils.getCellData(i, 2);
        String socText = ExcelUtils.getCellData(i, 1);
        //String expRes = ExcelUtils.getCellData(i, 2);
        
        //LoginPage lp = new LoginPage();
        
        /*lp.login(uName, pwd);
		lp.logibutton();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
        
        if(socText.equalsIgnoreCase("Notices")){
        	String actRes = hb.fetchHubLabel(socText);
			System.out.println(actRes);
			if(actRes.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking notice shows");
				System.out.println("The test case passed, notice label shows.");
			}else{
				soft.assertTrue(false, "The test case failed, notice label does not show.\n");
			}
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			//mi.clickOnLogout();
        }
        
        if(socText.equalsIgnoreCase("Notifications")){
        	String actRes = hb.fetchNotificationT();
			System.out.println(actRes);
			if(actRes.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking notification shows");
				System.out.println("The test case passed, notification label shows.");
			}else{
				soft.assertTrue(false, "The test case failed, notification label does not show.\n");
			}
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
        }
        
        if(socText.equalsIgnoreCase("PROFILE")){
        	String actRes = hb.fetchProfileT();
			System.out.println(actRes);
			if(actRes.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking profile shows");
				System.out.println("The test case passed, profile label shows.");
			}else{
				soft.assertTrue(false, "The test case failed, profile label does not show.\n");
			}
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
        }
        
        if(socText.equalsIgnoreCase("TUTORIAL")){
        	String actRes = hb.fetchHubLabel(socText);
			System.out.println(actRes);
			if(actRes.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking tutorial shows");
				System.out.println("The test case passed, tutorial label shows.");
			}else{
				soft.assertTrue(false, "The test case failed, tutorial label does not show.\n");
			}
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
        }
        
        
			
//			ExcelUtils.setCellData(actRes, i, 3, Constant.File_TestData_SocialHub);
//			
//			Boolean status = ValidationLib.verifyMsg(expRes, actRes);
//			
//			if(status)
//			{
//				ExcelUtils.setCellData("pass", i, 4,Constant.File_TestData_SocialHub);
//			}
//			else 
//		    {
//				ExcelUtils.setCellData("Fail", i, 4,Constant.File_TestData_SocialHub);
//			}
		//} else {
	//	System.out.println("Please Enter a valid message to be found in Excell.");
		//}
		}
		soft.assertAll();
	}
}
