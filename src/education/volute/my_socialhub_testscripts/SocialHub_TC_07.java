package education.volute.my_socialhub_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.MenuItems;
import education.volute.pages.SocialHub;
import utilities.Constant;
import utilities.SoftAssertCheck;

public class SocialHub_TC_07 extends SuperTestScript
{
	//@Test
	public void testSocialHub_TC_07() //throws Exception 
	
	{
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_SocialHub, "UserNameShows");
		
		// Instantiating method mi/nt to void method.
		// Calling Methods outside of for loop.
		LoginPage lp = new LoginPage();
		MenuItems mi = new MenuItems();
        //NavigatorTool nt = new NavigatorTool();
		SoftAssertCheck soft = new SoftAssertCheck();
        SocialHub hb = new SocialHub();
        
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		//String uName = ExcelUtils.getCellData(i, 1);
        //String pwd = ExcelUtils.getCellData(i, 2);
        String socText = ExcelUtils.getCellData(i, 1);
        String expRes = ExcelUtils.getCellData(i, 2);
        
        //LoginPage lp = new LoginPage();
        
        /*lp.login(uName, pwd);
		lp.logibutton();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
        try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
			String actRes = hb.fetchHubLabel(socText);
			System.out.println(actRes);
			//mi.clickOnLogout();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			ExcelUtils.setCellData(actRes, i, 3, Constant.File_TestData_SocialHub);
			
			Boolean status = ValidationLib.verifyMsg(expRes, actRes);
			
			if(status)
			{
				soft.assertTrue(true, "Checking user name label");
				ExcelUtils.setCellData("pass", i, 4,Constant.File_TestData_SocialHub);
				System.out.println("The test case passed, user name shows on profile widget.");
			}
			else 
		    {
				soft.assertTrue(false, "The test case failed, user name does not show on profile widget.\n");
				ExcelUtils.setCellData("Fail", i, 4,Constant.File_TestData_SocialHub);
			}
		//} else {
	//	System.out.println("Please Enter a valid message to be found in Excell.");
		//}
		}
		soft.assertAll();
	}
}
