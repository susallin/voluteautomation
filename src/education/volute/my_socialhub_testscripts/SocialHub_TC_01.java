package education.volute.my_socialhub_testscripts;

import java.util.Iterator;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Discussion;
import education.volute.pages.LoginPage;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import education.volute.pages.Notes;
import education.volute.pages.SocialHub;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.SoftAssertCheck;

public class SocialHub_TC_01 extends SuperTestScript {
	MenuItems mi;// = new MenuItems();
	//MenuItems mi;// = new MenuItems();
	NavigatorTool nt;// = new NavigatorTool();
	//@Test
	public void testSocialHub_TC_01() //throws Exception 
	
	{
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_SocialHub, "SocialHubEmpty2");
		
		// Instantiating method mi/nt to void method.
		// Calling Methods outside of for loop.
		LoginPage lp = new LoginPage();
		SoftAssertCheck soft = new SoftAssertCheck();
		mi = new MenuItems();
        nt = new NavigatorTool();
        
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String uName = ExcelUtils.getCellData(i, 1);
        String pwd = ExcelUtils.getCellData(i, 2);
        String socText = ExcelUtils.getCellData(i, 3);
        String user = ExcelUtils.getCellData(i, 4);
        String expRes = ExcelUtils.getCellData(i, 5);
        
        
        //LoginPage lp = new LoginPage();
        
        lp.login(uName, pwd);
		lp.logibutton();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//if(!expRes.isEmpty()){
			String actRes = SocialHub(socText, user);
			System.out.println(actRes);
			mi.clickOnLogout();
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			ExcelUtils.setCellData(actRes, i, 6, Constant.File_TestData_SocialHub);
			
			Boolean status = ValidationLib.verifyMsg(expRes, actRes);
			
			if(status)
			{
				soft.assertTrue(true, "Checking messages shows");
				ExcelUtils.setCellData("pass", i, 7,Constant.File_TestData_SocialHub);
				System.out.println("The test case passed, "+socText+" message shows");
			}
			else 
		    {
				soft.assertTrue(false, "The test case failed, "+socText+" message does not show.\n");
				ExcelUtils.setCellData("Fail", i, 7,Constant.File_TestData_SocialHub);
			}
		//} else {
	//	System.out.println("Please Enter a valid message to be found in Excell.");
		//}
		}
		soft.assertAll();
	}
		
	public String SocialHub(String sText, String user){
		SocialHub hb = new SocialHub();
		mi = new MenuItems();
        nt = new NavigatorTool();
		//MenuItems mi = new MenuItems();
		//NavigatorTool nt = new NavigatorTool();
		
		if(sText.equalsIgnoreCase("Welcome")){
			String txt;
			txt = hb.fetchMyJourneyWelcomeMsg(user);
			sText = txt;
			//return txt;		
		}else if(sText.equalsIgnoreCase("No Program Selected")){
			String txt;
			nt.clickOnJourney();
			hb.clickOnDeselectProg();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			txt = hb.fetchMyJourneyWidgetMsg3();
			sText = txt;
			nt.clickOnJourney();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			//return txt;
		}else if(sText.equalsIgnoreCase("No Learning Space Active Journey Tool")){
			String txt;
			nt.clickOnJourney();
			txt = hb.fetchMyJourneyWidgetMsg2();
			sText = txt;
			nt.clickOnJourney();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			//return txt;
		}/*else if(sText.equalsIgnoreCase("No Program Selected Journey Tool")){
			String txt;
			hb.clickOnDeselectProg();
			mi.clickOnMainMenu();
			nt.clickOnJourney();
			txt = hb.fetchMyJourneyWidgetMsg4();
			sText = txt;
		}*/else if(sText.equalsIgnoreCase("discussions")){
			hb.selectGrpDsc(sText);
			String txt;
			txt = hb.fetchDiscussionWidget();
			sText = txt;
		}else if(sText.equalsIgnoreCase("groups")){
			hb.selectGrpDsc(sText);
			String txt;
			txt = hb.fetchGroupWidget();
			sText = txt;
		}else if(sText.equalsIgnoreCase("events")){
			hb.selectGrpDsc(sText);
			String txt;
			txt = hb.fetchGroupWidget();
			sText = txt;
		}else if(sText.equalsIgnoreCase("No Notice")){
			String txt;
			txt = hb.fetchNoticeWidget();
			sText = txt;
		}else if(sText.equalsIgnoreCase("No Notifications")){
			String txt;
			txt = hb.fetchNotificationWidget();
			sText = txt;
		}
		
		//else if(sText.equalsIgnoreCase("No Program Active Journey Tool"))
		//	txt = hb.fetchMyJourneyWidgetMsg2();
		//} else if(tname.equalsIgnoreCase("Discussion")){
		//return txt;
		nt.clickOnPortfolioIcon();
		return sText;	
	}
	
}
