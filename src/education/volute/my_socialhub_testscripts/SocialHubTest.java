package education.volute.my_socialhub_testscripts;


public class SocialHubTest {

		
		public void SocialHubEmptyMsg() {
			SocialHub_TC_01 hb = new SocialHub_TC_01();
			hb.testSocialHub_TC_01();
		}
		
		public void SocialHubSelectProgramShows() {
			SocialHub_TC_02 hb = new SocialHub_TC_02();
			hb.testSocialHub_TC_02();
		}
		
		/*public void SocialHubDiscussionShows() {
			SocialHub_TC_03 hb = new SocialHub_TC_03();
			hb.testSocialHub_TC_03();
		}
		
		public void SocialHubGroupShows() {
			SocialHub_TC_04 hb = new SocialHub_TC_04();
			hb.testSocialHub_TC_04();
		}
		
		public void SocialHubNoticeShows() {
			SocialHub_TC_05 hb = new SocialHub_TC_05();
			hb.testSocialHub_TC_05();
		}*/
		
		public void SocialLabelShows(){
			SocialHub_TC_06 hb = new SocialHub_TC_06();
			hb.testSocialHub_TC_06();
		}
		
		public void SocialUserNameShows(){
			SocialHub_TC_07 hb = new SocialHub_TC_07();
			hb.testSocialHub_TC_07();
		}
		
		public void SocialVisitedActSub(){
			SocialHub_TC_08 hb = new SocialHub_TC_08();
			hb.testSocialHub_TC_08();
		}
		
		public void SocialGoToActSub(String Act, String Sub, String Role){
			SocialHub_TC_08 hb = new SocialHub_TC_08();
			hb.testSocialHub_TC_08Part2(Act, Sub, Role);
		}
		
		// Verify Page 
		public void SocialHubVerifyPage(){
			SocialHubVerifyPage hb = new SocialHubVerifyPage();
			hb.testSocialHubVerifyPage();
		}
		
		public void SocialHubChangeProgram(String str) throws Exception{
			LaunchNewProgram_TC_01 hb = new LaunchNewProgram_TC_01();
			hb.testLaunchNewProgram_TC_01(str);
		}
}
