package education.volute.my_socialhub_testscripts;

import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.SoftAssertCheck;

public class SocialHubVerifyPage {

	public void testSocialHubVerifyPage() //throws Exception 
	{
		
		MenuItems mi = new MenuItems();
		NavigatorTool nt = new NavigatorTool();
		LoginPage lp = new LoginPage();
		ManagePrograms mp = new ManagePrograms();
		SoftAssertCheck soft = new SoftAssertCheck();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_SocialHub, "SocialValidatePage");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
			String uName = ExcelUtils.getCellData(i, 1);
	        String pwd = ExcelUtils.getCellData(i, 2);
	        String roleText = ExcelUtils.getCellData(i, 3);
	        String lsRole = ExcelUtils.getCellData(i, 4);
	        
	        lp.login(uName, pwd); 
	        lp.logibutton();
	        
	        mi.fetchAppTool("div[class$='profile-module'] paper-icon-button[icon='icons:settings']");
	        try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	        
	        //roleText.equalsIgnoreCase("Participant")||roleText.equalsIgnoreCase("Observer")
	        if(roleText.equalsIgnoreCase("User")){
	        	String supShows = nt.supportShows();
				System.out.println(supShows);
				//ExcelUtils.setCellData(supShows, i, 4, Constant.File_TestData_SocialHub);
				if(supShows.equalsIgnoreCase("Found")){
					soft.assertTrue(true, "Support button shows.");
					System.out.println("The test case passed, Support button shows for the "+roleText+".");
				}else{
					soft.assertTrue(false, "The test case failed, Support button does not show for the "+roleText+".\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    
			    String homeShows = nt.homeShows();
				System.out.println(homeShows);
				//ExcelUtils.setCellData(homeShows, i, 5, Constant.File_TestData_SocialHub);
				if(homeShows.equalsIgnoreCase("Found")){
					soft.assertTrue(true, "Home button shows.");
					System.out.println("The test case passed, Home button shows for the "+roleText+".");
				}else{
					soft.assertTrue(false, "The test case failed, Home button does not show for the "+roleText+".\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    
			    String noteShows = nt.noteShows();
				System.out.println(noteShows);
				//ExcelUtils.setCellData(noteShows, i, 6, Constant.File_TestData_SocialHub);
				if(noteShows.equalsIgnoreCase("Found")){
					soft.assertTrue(true, "Notes button shows.");
					System.out.println("The test case passed, Notes button shows for the "+roleText+".");
				}else{
					soft.assertTrue(false, "The test case failed, Notes button does not show for the "+roleText+".\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    
			    String discussionShows = nt.discussionShows();
				System.out.println(discussionShows);
				//ExcelUtils.setCellData(discussionShows, i, 7, Constant.File_TestData_SocialHub);
				if(discussionShows.equalsIgnoreCase("Found")){
					soft.assertTrue(true, "Discussion button shows.");
					System.out.println("The test case passed, Discussion button shows for the "+roleText+".");
				}else{
					soft.assertTrue(false, "The test case failed, Discussion button does not show for the "+roleText+".\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    
			    String collaboratorShows = nt.collaborateShows();
				System.out.println(collaboratorShows);
				//ExcelUtils.setCellData(collaboratorShows, i, 8, Constant.File_TestData_SocialHub);
				if(collaboratorShows.equalsIgnoreCase("Found")){
					soft.assertTrue(true, "Group button shows.");
					System.out.println("The test case passed, Group button shows for the "+roleText+".");
				}else{
					soft.assertTrue(false, "The test case failed, Group button does not show for the "+roleText+".\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    
			    String portfolioShows = nt.portfolioShows();
				System.out.println(portfolioShows);
				//ExcelUtils.setCellData(portfolioShows, i, 9, Constant.File_TestData_SocialHub);
				if(portfolioShows.equalsIgnoreCase("Found")){
					soft.assertTrue(true, "Portfolio button shows.");
					System.out.println("The test case passed, Portfolio button shows for the "+roleText+".");
				}else{
					soft.assertTrue(false, "The test case failed, Portfolio button does not show for the "+roleText+".\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    if(portfolioShows.equalsIgnoreCase("Found")){
			    	nt.clickOnPortfolioIcon();
			    	try {
			    		Thread.sleep(3000);
			    	} catch (InterruptedException e1) {
			    		// TODO Auto-generated catch block
			    		e1.printStackTrace();
			    	}
			    	
			    	String profileShows = mi.profileShows();
			    	System.out.println(profileShows);
					//ExcelUtils.setCellData(profileShows, i, 10, Constant.File_TestData_SocialHub);
					if(profileShows.equalsIgnoreCase("Found")){
						soft.assertTrue(true, "Profile button shows.");
						System.out.println("The test case passed, Profile button shows for the "+roleText+".");
					}else{
						soft.assertTrue(false, "The test case failed, Profile button does not show for the "+roleText+".\n");
					}
				    try {
						Thread.sleep(3000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				    
				    String logoutShows = mi.logoutShows();
			    	System.out.println(logoutShows);
					//ExcelUtils.setCellData(logoutShows, i, 11, Constant.File_TestData_SocialHub);
					if(logoutShows.equalsIgnoreCase("Found")){
						soft.assertTrue(true, "Logout button shows.");
						System.out.println("The test case passed, Logout button shows for the "+roleText+".");
					}else{
						soft.assertTrue(false, "The test case failed, Logout button does not show for the "+roleText+".\n");
					}
				    try {
						Thread.sleep(3000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				    
				    nt.clickOnPortfolioIcon();
			    	try {
			    		Thread.sleep(3000);
			    	} catch (InterruptedException e1) {
			    		// TODO Auto-generated catch block
			    		e1.printStackTrace();
			    	}
			    }
			    
			    String journeyShows = nt.journeyShows();
				System.out.println(journeyShows);
				//ExcelUtils.setCellData(journeyShows, i, 12, Constant.File_TestData_SocialHub);
				if(journeyShows.equalsIgnoreCase("Found")){
					soft.assertTrue(true, "Journey button shows.");
					System.out.println("The test case passed, Journey button shows for the "+roleText+".");
				}else{
					soft.assertTrue(false, "The test case failed, Journey button does not show for the "+roleText+".\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    
			    if(lsRole.equalsIgnoreCase("Fac")){
			    	String adminShows = nt.adminShows();
					System.out.println(adminShows);
					//ExcelUtils.setCellData(adminShows, i, 13, Constant.File_TestData_SocialHub);
					if(adminShows.equalsIgnoreCase("Found")){
						soft.assertTrue(true, "Admin button shows.");
						System.out.println("The test case passed, Admin button shows for the "+roleText+".");
					}else{
						soft.assertTrue(false, "The test case failed, Admin button does not show for the "+roleText+".\n");
					}
				    try {
						Thread.sleep(3000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				    
				    if(adminShows.equalsIgnoreCase("Found")){
				    	nt.clickOnAdmin();
				    	try {
				    		Thread.sleep(3000);
				    	} catch (InterruptedException e1) {
				    		// TODO Auto-generated catch block
				    		e1.printStackTrace();
				    	}
				    	
				    	String programShows = mi.programShows();
				    	System.out.println(programShows);
						//ExcelUtils.setCellData(programShows, i, 14, Constant.File_TestData_SocialHub);
						if(programShows.equalsIgnoreCase("Found")){
							soft.assertTrue(true, "Program button shows.");
							System.out.println("The test case passed, Program button shows for the "+roleText+".");
						}else{
							soft.assertTrue(false, "The test case failed, Program button does not show for the "+roleText+".\n");
						}
					    try {
							Thread.sleep(3000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					    
					    mi.clickOnProgram();
					    try {
							Thread.sleep(5000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					    
					    String lsAddButtonShows = mp.fetchAddLSBtn();
				    	System.out.println(lsAddButtonShows);
						//ExcelUtils.setCellData(programShows, i, 14, Constant.File_TestData_SocialHub);
						if(lsAddButtonShows.equalsIgnoreCase("Not Found")){
							soft.assertTrue(true, "Learning Space Add button does not show.");
							System.out.println("The test case passed, LS Add button does not show for the "+roleText+".");
						}else{
							soft.assertTrue(false, "The test case failed, LS Add button shows for the "+roleText+".\n");
						}
					    try {
							Thread.sleep(3000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					    
					    nt.clickOnHomeIcon();
					    try {
							Thread.sleep(5000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					  }
			    }else{
			    	String adminShows = nt.adminShows();
					System.out.println(adminShows);
					//ExcelUtils.setCellData(adminShows, i, 13, Constant.File_TestData_SocialHub);
					if(adminShows.equalsIgnoreCase("Not Found")){
						soft.assertTrue(true, "Admin button does not show.");
						System.out.println("The test case passed, Admin button does not show for the "+roleText+".");
					}else{
						soft.assertTrue(false, "The test case failed, Admin button shows for the "+roleText+".\n");
					}
				    try {
						Thread.sleep(3000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			    }
			    
			    //////////////////////////////////////
	        }else{
	        	//////////////////////////////////////
	        	String supShows = nt.supportShows();
				System.out.println(supShows);
				//ExcelUtils.setCellData(supShows, i, 4, Constant.File_TestData_SocialHub);
				if(supShows.equalsIgnoreCase("Found")){
					soft.assertTrue(true, "Support button shows.");
					System.out.println("The test case passed, Support button shows for the "+roleText+".");
				}else{
					soft.assertTrue(false, "The test case failed, Support button does not show for the "+roleText+".\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    
			    String homeShows = nt.homeShows();
				System.out.println(homeShows);
				//ExcelUtils.setCellData(homeShows, i, 5, Constant.File_TestData_SocialHub);
				if(homeShows.equalsIgnoreCase("Found")){
					soft.assertTrue(true, "Home button shows.");
					System.out.println("The test case passed, Home button shows for the "+roleText+".");
				}else{
					soft.assertTrue(false, "The test case failed, Home button does not show for the "+roleText+".\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    
			    String noteShows = nt.noteShows();
				System.out.println(noteShows);
				//ExcelUtils.setCellData(noteShows, i, 6, Constant.File_TestData_SocialHub);
				if(noteShows.equalsIgnoreCase("Found")){
					soft.assertTrue(true, "Notes button shows.");
					System.out.println("The test case passed, Notes button shows for the "+roleText+".");
				}else{
					soft.assertTrue(false, "The test case failed, Notes button does not show for the "+roleText+".\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    
			    String discussionShows = nt.discussionShows();
				System.out.println(discussionShows);
				//ExcelUtils.setCellData(discussionShows, i, 7, Constant.File_TestData_SocialHub);
				if(discussionShows.equalsIgnoreCase("Found")){
					soft.assertTrue(true, "Discussion button shows.");
					System.out.println("The test case passed, Discussion button shows for the "+roleText+".");
				}else{
					soft.assertTrue(false, "The test case failed, Discussion button does not show for the "+roleText+".\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    
			    String collaboratorShows = nt.collaborateShows();
				System.out.println(collaboratorShows);
				//ExcelUtils.setCellData(collaboratorShows, i, 8, Constant.File_TestData_SocialHub);
				if(collaboratorShows.equalsIgnoreCase("Found")){
					soft.assertTrue(true, "Group button shows.");
					System.out.println("The test case passed, Group button shows for the "+roleText+".");
				}else{
					soft.assertTrue(false, "The test case failed, Group button does not show for the "+roleText+".\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    
			    String portfolioShows = nt.portfolioShows();
				System.out.println(portfolioShows);
				//ExcelUtils.setCellData(portfolioShows, i, 9, Constant.File_TestData_SocialHub);
				if(portfolioShows.equalsIgnoreCase("Found")){
					soft.assertTrue(true, "Portfolio button shows.");
					System.out.println("The test case passed, Portfolio button shows for the "+roleText+".");
				}else{
					soft.assertTrue(false, "The test case failed, Portfolio button does not show for the "+roleText+".\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    if(portfolioShows.equalsIgnoreCase("Found")){
			    	nt.clickOnPortfolioIcon();
			    	try {
			    		Thread.sleep(3000);
			    	} catch (InterruptedException e1) {
			    		// TODO Auto-generated catch block
			    		e1.printStackTrace();
			    	}
			    	
			    	String profileShows = mi.profileShows();
			    	System.out.println(profileShows);
					//ExcelUtils.setCellData(profileShows, i, 10, Constant.File_TestData_SocialHub);
					if(profileShows.equalsIgnoreCase("Found")){
						soft.assertTrue(true, "Profile button shows.");
						System.out.println("The test case passed, Profile button shows for the "+roleText+".");
					}else{
						soft.assertTrue(false, "The test case failed, Profile button does not show for the "+roleText+".\n");
					}
				    try {
						Thread.sleep(3000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				    
				    String logoutShows = mi.logoutShows();
			    	System.out.println(logoutShows);
					//ExcelUtils.setCellData(logoutShows, i, 11, Constant.File_TestData_SocialHub);
					if(logoutShows.equalsIgnoreCase("Found")){
						soft.assertTrue(true, "Logout button shows.");
						System.out.println("The test case passed, Logout button shows for the "+roleText+".");
					}else{
						soft.assertTrue(false, "The test case failed, Logout button does not show for the "+roleText+".\n");
					}
				    try {
						Thread.sleep(3000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				    
				    nt.clickOnPortfolioIcon();
			    	try {
			    		Thread.sleep(3000);
			    	} catch (InterruptedException e1) {
			    		// TODO Auto-generated catch block
			    		e1.printStackTrace();
			    	}
			    }
			    
			    String journeyShows = nt.journeyShows();
				System.out.println(journeyShows);
				//ExcelUtils.setCellData(journeyShows, i, 12, Constant.File_TestData_SocialHub);
				if(journeyShows.equalsIgnoreCase("Found")){
					soft.assertTrue(true, "Journey button shows.");
					System.out.println("The test case passed, Journey button shows for the "+roleText+".");
				}else{
					soft.assertTrue(false, "The test case failed, Journey button does not show for the "+roleText+".\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    
			    String adminShows = nt.adminShows();
				System.out.println(adminShows);
				//ExcelUtils.setCellData(adminShows, i, 13, Constant.File_TestData_SocialHub);
				if(adminShows.equalsIgnoreCase("Found")){
					soft.assertTrue(true, "Admin button shows.");
					System.out.println("The test case passed, Admin button shows for the "+roleText+".");
				}else{
					soft.assertTrue(false, "The test case failed, Admin button does not show for the "+roleText+".\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    
			    if(adminShows.equalsIgnoreCase("Found")){
			    	nt.clickOnAdmin();
			    	try {
			    		Thread.sleep(3000);
			    	} catch (InterruptedException e1) {
			    		// TODO Auto-generated catch block
			    		e1.printStackTrace();
			    	}
			    	
			    	String programShows = mi.programShows();
			    	System.out.println(programShows);
					//ExcelUtils.setCellData(programShows, i, 14, Constant.File_TestData_SocialHub);
					if(programShows.equalsIgnoreCase("Found")){
						soft.assertTrue(true, "Program button shows.");
						System.out.println("The test case passed, Program button shows for the "+roleText+".");
					}else{
						soft.assertTrue(false, "The test case failed, Program button does not show for the "+roleText+".\n");
					}
				    try {
						Thread.sleep(3000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			    	
				    
				    String manageuserShows = mi.manageUsersShows();
			    	System.out.println(manageuserShows);
					//ExcelUtils.setCellData(manageuserShows, i, 15, Constant.File_TestData_SocialHub);
					if(manageuserShows.equalsIgnoreCase("Found")){
						soft.assertTrue(true, "Manage User button shows.");
						System.out.println("The test case passed, Manage User button shows for the "+roleText+".");
					}else{
						soft.assertTrue(false, "The test case failed, Manage User button does not show for the "+roleText+".\n");
					}
				    try {
						Thread.sleep(3000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				    
				    
				    String marketplaceShows = mi.marketplaceShows();
			    	System.out.println(marketplaceShows);
					//ExcelUtils.setCellData(marketplaceShows, i, 16, Constant.File_TestData_SocialHub);
					if(marketplaceShows.equalsIgnoreCase("Found")){
						soft.assertTrue(true, "Marketplace button shows.");
						System.out.println("The test case passed, Marketplace button shows for the "+roleText+".");
					}else{
						soft.assertTrue(false, "The test case failed, Marketplace button does not show for the "+roleText+".\n");
					}
				    try {
						Thread.sleep(3000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			    }
	        }
	        
	        nt.clickOnPortfolioIcon();
	    	try {
	    		Thread.sleep(3000);
	    	} catch (InterruptedException e1) {
	    		// TODO Auto-generated catch block
	    		e1.printStackTrace();
	    	}
	    	
	    	mi.clickOnLogout();
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		soft.assertAll();
	}
}
