package education.volute.my_socialhub_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.MenuItems;
import education.volute.pages.SocialHub;
import utilities.Constant;

public class SocialHub_TC_03 extends SuperTestScript {

	@Test
	public void testSocialHub_TC_03() //throws Exception 
	
	{
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_SocialHub, "DiscussionShows");
		
		// Instantiating method mi/nt to void method.
		// Calling Methods outside of for loop.
		LoginPage lp = new LoginPage();
		MenuItems mi = new MenuItems();
        //NavigatorTool nt = new NavigatorTool();
        SocialHub hb = new SocialHub();
        
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String uName = ExcelUtils.getCellData(i, 1);
        String pwd = ExcelUtils.getCellData(i, 2);
        //String socText = ExcelUtils.getCellData(i, 3);
        String expRes = ExcelUtils.getCellData(i, 3);
        
        //LoginPage lp = new LoginPage();
        
        /*lp.login(uName, pwd);
		lp.logibutton();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
        try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
			String actRes = hb.fetchDiscussionT();
			System.out.println(actRes);
			//mi.clickOnLogout();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			ExcelUtils.setCellData(actRes, i, 4, Constant.File_TestData_SocialHub);
			
			Boolean status = ValidationLib.verifyMsg(expRes, actRes);
			
			if(status)
			{
				ExcelUtils.setCellData("pass", i, 5,Constant.File_TestData_SocialHub);
			}
			else 
		    {
				ExcelUtils.setCellData("Fail", i, 5,Constant.File_TestData_SocialHub);
			}
		//} else {
	//	System.out.println("Please Enter a valid message to be found in Excell.");
		//}
		}
	}
}
