package education.volute.my_journey_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.MyJourney;
import education.volute.pages.NavigatorTool;
import utilities.Constant;

public class LaunchLearningspace_TC_02 extends SuperTestScript 
{

	@Test
	public void testLaunchLearningSpace_TC_02() 
	{
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_MyJourney, "Act_SubMenu");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String uName = ExcelUtils.getCellData(i, 1);
        String pwd = ExcelUtils.getCellData(i, 2);
        String pgmNameTxt = ExcelUtils.getCellData(i, 3);
        String pgmNameTxt2 = ExcelUtils.getCellData(i, 4);
        String actNameTxt = ExcelUtils.getCellData(i, 5);
        //String subActNameTxt = ExcelUtils.getCellData(i, 5);
        String expRes = ExcelUtils.getCellData(i, 6);
                
     
	LoginPage lp = new LoginPage();
	NavigatorTool nt = new NavigatorTool();
	MyJourney mj = new MyJourney();
	
	lp.login(uName, pwd);
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	nt.clickOnHomeIcon();
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	mj.clickOnPgmDropDown();
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	mj.selectPgmFromDropDown(pgmNameTxt);
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	mj.clickOnPgmDropDown();
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	mj.selectPgmFromDropDown(pgmNameTxt2);
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	mj.clickOnActivity();
	//mj.clickOnActivity(actNameTxt);
	/*mj.clickOnActivityContainingSubs(actNameTxt);
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	mj.clickOnSubActivity(subActNameTxt);*/
	mj.clickOnLaunch();
	
	String actRes = mj.fetchLSHeader();
	System.out.println(actRes);

	 ExcelUtils.setCellData(actRes, i, 7, Constant.File_TestData_MyJourney);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 8, Constant.File_TestData_MyJourney);
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 8, Constant.File_TestData_MyJourney);
		}
		
		
		}
		
		}
	
}
