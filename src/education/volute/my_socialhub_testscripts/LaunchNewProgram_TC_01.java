package education.volute.my_socialhub_testscripts;

import java.util.Iterator;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.SocialHub;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.SearchElements;
import utilities.SoftAssertCheck;

public class LaunchNewProgram_TC_01 extends SuperTestScript

{
	//@Test
	public void testLaunchNewProgram_TC_01(String str) throws Exception
	{
		NavigatorTool nt = new NavigatorTool();
        SocialHub sh = new SocialHub();
        ReadCsvFiles csv = new ReadCsvFiles();
        SearchElements sr = new SearchElements();
		SoftAssertCheck soft = new SoftAssertCheck();
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_SocialHub, str);
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
			
        String pgmFileNames = ExcelUtils.getCellData(i, 1);
        /*String pgmDescTxt = ExcelUtils.getCellData(i, 2);
        String pgmNameTxt2 = ExcelUtils.getCellData(i, 3);
        String pgmDescTxt2 = ExcelUtils.getCellData(i, 4);
        String actNameTxt = ExcelUtils.getCellData(i, 5);
        String subActNameTxt = ExcelUtils.getCellData(i, 6);
        String expRes = ExcelUtils.getCellData(i, 7);*/
	
        Iterator<String[]> FileName = csv.ReadList(pgmFileNames);
		
		if(FileName.hasNext())
			FileName.next();
		
		while(FileName.hasNext()){
			int n = 0;
			String[] Name = FileName.next();
			
			//String journey = Name[n];
			String pgmNameTxt = Name[n];
			String logo = Name[n+1];
			
			nt.clickOnJourney();
			try {
				Thread.sleep(7000);
			} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			}
			
			sh.clickOnPgmDropDown();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	
			sh.selectPgmFromDropDown(pgmNameTxt);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	
			nt.clickOnDeletePgmYesButton();
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	
			String progName = sh.fetchProgramName(logo, pgmNameTxt);
			System.out.println(progName);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	
			if(progName.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking Program name shows on Social Hub");
				System.out.println("The test case passed, the program name shows(Program Changed Successfully).");
			}else{
				soft.assertTrue(false, "The test case failed, the program name does not show.\n");
			}
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			nt.refreshPage();
			try {
				Thread.sleep(7000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String progName1 = sh.fetchProgramName(logo, pgmNameTxt);
			System.out.println(progName1);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	
			if(progName1.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking Program name shows on Social Hub");
				System.out.println("The test case passed, the program name shows(Program Changed Successfully).");
			}else{
				soft.assertTrue(false, "The test case failed, the program name does not show.\n");
			}
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} 
	
	/*sh.clickOnPgmDropDown();
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	sh.selectPgmFromDropDown(pgmNameTxt2);
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	nt.clickOnDeletePgmYesButton();
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	String progDesc1 = sh.fetchProgramDescription(pgmDescTxt2);
	System.out.println(progDesc1);
	try {
		Thread.sleep(2000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	if(progDesc1.equalsIgnoreCase("Found")){
		soft.assertTrue(true, "Checking Program changed Program description shows on Journey");
		System.out.println("The test case passed on verifying the program description (Program Changed Successfully).");
	}else{
		soft.assertTrue(false, "The test case failed on verifying the Program description (Program did not change).\n");
	}
    try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}*/
	/*sh.clickOnActivityContainingSubs(actNameTxt);
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	sh.clickOnSubActivity(subActNameTxt);
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	sh.clickOnLaunch();
	
	String actRes = mj.fetchLSHeader();
	System.out.println(actRes);

	 ExcelUtils.setCellData(actRes, i, 8, Constant.File_TestData_SocialHub);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 9, Constant.File_TestData_MyJourney);
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 9, Constant.File_TestData_MyJourney);
		}*/
		
		
		}
		
		}

}

