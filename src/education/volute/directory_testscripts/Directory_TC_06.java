package education.volute.directory_testscripts;

import java.util.Iterator;

import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.MenuItems;
import education.volute.pages.ProfileList;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class Directory_TC_06 {

	//@Test
			public void testDirectory_TC_06(String Role) throws Exception 	
			{
					//LoginPage lp = new LoginPage();
					//NavigatorTool nt = new NavigatorTool();
					ReadCsvFiles csv = new ReadCsvFiles();
					MenuItems mi = new MenuItems();
					SoftAssertCheck soft = new SoftAssertCheck();
					//SocialHub mj = new SocialHub();
					//Landing ld = new Landing();
					//ManagePrograms mp = new ManagePrograms();
					//ManageLearningSpaces mls = new ManageLearningSpaces();
					ToolDestAndCollections toolD = new ToolDestAndCollections();
					//Directory dir = new Directory();
					ProfileList pro = new ProfileList();
					
					ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Directory, "SearchIndividual");
					
					for(int i=1; i<=ExcelUtils.getRowcount(); i++)
					{
					
					//String uName = ExcelUtils.getCellData(i, 1);
			        //String pwd = ExcelUtils.getCellData(i, 2);
			        //String pgmNameTxt = ExcelUtils.getCellData(i, 3);
					String toolDest = ExcelUtils.getCellData(i, 1);
				    String actName = ExcelUtils.getCellData(i, 2);
				    String subActName = ExcelUtils.getCellData(i, 3);
				    String pgmNameTxt = ExcelUtils.getCellData(i, 4);
				    String toolName = ExcelUtils.getCellData(i, 5);
				    String searchIndivs = ExcelUtils.getCellData(i, 6);
			        String appLocName = ExcelUtils.getCellData(i, 7);
					
					toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
					
					mi.fetchAppTool(appLocName);
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					
					Iterator<String[]> FileName = csv.ReadList(searchIndivs);
							
					if(FileName.hasNext())
						FileName.next();
							
					while(FileName.hasNext()){
						int n = 0;
						String[] Name = FileName.next();
								
						String typeIndv = Name[n];
						String ind = Name[n+1];
						String determine = Name[n+2];
						String ind2 = Name[n+3];
						String determine2 = Name[n+4];
								
						mi.SelectSearchOfTool(toolName);
						try {
							Thread.sleep(4000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						mi.enterSearch(toolName, typeIndv);
						try {
							Thread.sleep(2000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
									
						String dirShows = pro.fetchFaceIndShows(ind);
						System.out.println(dirShows);
						try {
							Thread.sleep(2000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						if(dirShows.equalsIgnoreCase(determine)){
							soft.assertTrue(true, "Checking User");
							System.out.println("The test case passed, User "+determine+".");
						}else{
							soft.assertTrue(false, "The test case failed, User "+determine+".\n");
						}
					    try {
							Thread.sleep(3000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
					    String dirShows1 = pro.fetchFaceIndShows(ind2);
						System.out.println(dirShows1);
						try {
							Thread.sleep(2000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						if(dirShows1.equalsIgnoreCase(determine2)){
							soft.assertTrue(true, "Checking User");
							System.out.println("The test case passed, User "+determine2+".");
						}else{
							soft.assertTrue(false, "The test case failed, User "+determine2+".\n");
						}
					    try {
							Thread.sleep(3000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						mi.SelectSearchOfTool(toolName);
						try {
							Thread.sleep(4000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				    }
					soft.assertAll();
			}
}
