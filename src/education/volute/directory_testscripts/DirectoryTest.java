package education.volute.directory_testscripts;

public class DirectoryTest {

	public void DirectoryAddUser(String Role) throws Exception{
		Directory_TC_01 dir = new Directory_TC_01();	
		dir.testDirectory_TC_01(Role);
	}
	
	public void DirectoryRemoveUser(String Role) throws Exception{
		Directory_TC_05 dir = new Directory_TC_05();	
		dir.testDirectory_TC_05(Role);
	}
	
	public void DirectorySearchIndiv(String Role) throws Exception{
		Directory_TC_06 dir = new Directory_TC_06();	
		dir.testDirectory_TC_06(Role);
	}
}
