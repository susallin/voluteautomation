package education.volute.directory_testscripts;

import java.util.Iterator;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Directory;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import education.volute.pages.Video;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class Directory_TC_05 extends SuperTestScript {

	//@Test
	public void testDirectory_TC_05(String Role) throws Exception 	
	{
			//LoginPage lp = new LoginPage();
			ReadCsvFiles csv = new ReadCsvFiles();
			NavigatorTool nt = new NavigatorTool();
			MenuItems mi = new MenuItems();
			SoftAssertCheck soft = new SoftAssertCheck();
			//SocialHub mj = new SocialHub();
			//Landing ld = new Landing();
			//ManagePrograms mp = new ManagePrograms();
			//ManageLearningSpaces mls = new ManageLearningSpaces();
			ToolDestAndCollections toolD = new ToolDestAndCollections();
			Video vid = new Video();
			Directory dir = new Directory();
			//SubMenu sub = new SubMenu();
						
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Directory, "DeleteInd");
				
				for(int i=1; i<=ExcelUtils.getRowcount(); i++)
				{
				
				//String uName = ExcelUtils.getCellData(i, 1);
		        //String pwd = ExcelUtils.getCellData(i, 2);
				String toolDest = ExcelUtils.getCellData(i, 1);
		        String actName = ExcelUtils.getCellData(i, 2);
		        String subActName = ExcelUtils.getCellData(i, 3);
		        String pgmNameTxt = ExcelUtils.getCellData(i, 4);
		        String toolName = ExcelUtils.getCellData(i, 5);
		        //String dirDName = ExcelUtils.getCellData(i, 4);
		        String indNames = ExcelUtils.getCellData(i, 6);
		        String indName = ExcelUtils.getCellData(i, 8);
		        String indName2 = ExcelUtils.getCellData(i, 10);
		        String indName3 = ExcelUtils.getCellData(i, 12);
		        //String pgmNameTxt = ExcelUtils.getCellData(i, 3);
		        //String lsToolName = ExcelUtils.getCellData(i, 4);
		        //String lsNameTxt = ExcelUtils.getCellData(i, 5);
		        //String lsDescTxt = ExcelUtils.getCellData(i, 6);
		        //String sliDName = ExcelUtils.getCellData(i, 7);
		        //String dirName = ExcelUtils.getCellData(i, 8);
		        //String docName = ExcelUtils.getCellData(i, 9);
		        //String docAuthor = ExcelUtils.getCellData(i, 10);
		        String uRole = ExcelUtils.getCellData(i, 15);
		        String appLocName = ExcelUtils.getCellData(i, 16);
		        String expRes = ExcelUtils.getCellData(i, 7);
				
				/*lp.login(uName, pwd);
				lp.logibutton();
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				nt.clickOnMainMenu();*/
		        toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
				/*mi.clickOnNavigation();
				mi.clickOnProgram();
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				ld.clickOnLandingMP();
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				mp.clickOnselectedprogram(pgmNameTxt);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				mp.clickOnManageMashupicon(pgmNameTxt);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				/*mls.clickOnAddLS();
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				mls.selectaTool(lsToolName);
				mls.enterLSName(lsNameTxt);
				mls.enterLsDesc(lsDescTxt);
				mls.clickOnSaveButton();
				mls.clickOnConfigButton();*/
				/*try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				nt.clickOnJourney();
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				mj.clickOnLaunchAct(actName);*/
				/*try {
					Thread.sleep(15000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}*/
				mi.fetchAppTool(appLocName);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				mi.SelectManageFaceOfTool(toolName);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				//mi.clearDisplayName();
				//mi.enterDisplayName(dirDName);
				dir.clickOnDirDeleteButton(indNames);
				try {
					Thread.sleep(4000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				vid.clickOnDeleteYesVideo();
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				String toastMsg = dir.fetchDeleteMsgDirectory();
				System.out.println(toastMsg);
				if(toastMsg.equalsIgnoreCase("Found")){
					soft.assertTrue(true, "Checking toast");
					System.out.println("The test case passed toast shows.");
				}else{
					soft.assertTrue(false, "The test case failed toast does not show.\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				String actRes = dir.fetchManageFaceIndShows(indName);
				System.out.println(actRes);
				if(actRes.equalsIgnoreCase("Not Found")){
					soft.assertTrue(true, "Checking if User shows after it was deleted");
					System.out.println("The test case passed User does not show on Manage.");
				}else{
					soft.assertTrue(false, "The test case failed User does show on Manage.\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    mi.SelectExitManageFaceOfTool(toolName);
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    String actRes1a = dir.fetchFaceIndShows(indName);
				System.out.println(actRes1a);
				if(actRes1a.equalsIgnoreCase("Not Found")){
					soft.assertTrue(true, "Checking if User shows after it was deleted");
					System.out.println("The test case passed User does not show on Face.");
				}else{
					soft.assertTrue(false, "The test case failed User does show on Face.\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				nt.refreshPage();
				/*try {
					Thread.sleep(15000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}*/
				mi.fetchAppTool(appLocName);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				String actRes1 = dir.fetchFaceIndShows(indName);
				System.out.println(actRes1);
				if(actRes1.equalsIgnoreCase("Not Found")){
					soft.assertTrue(true, "Checking if User shows after it was deleted");
					System.out.println("The test case passed User does not show on Face.");
				}else{
					soft.assertTrue(false, "The test case failed User does show on Face.\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			    mi.SelectManageFaceOfTool(toolName);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				String actResShows = dir.fetchManageFaceIndShows(indName);
				System.out.println(actResShows);
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if(actResShows.equalsIgnoreCase("Not Found")){
					soft.assertTrue(true, "Checking if User shows after it was deleted");
					System.out.println("The test case passed User does not show on Manage.");
				}else{
					soft.assertTrue(false, "The test case failed User does show on Manage.\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				/*mi.clickOnAssignUserButton();
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				mi.clickOnIndividualTab();
				try {
					Thread.sleep(8000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}		
				mi.selectIndividual(indName);
				mi.selectIndividual(indName2);
				//dir.clickOnDirectoryGroupButton();
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}*/
				/*mi.clickOnSelectedTab();
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				mi.clickOnDoneButton();
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}*/
				/*sub.ClickOnSubMenuMLS();
				try {
					Thread.sleep(8000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}*/
				//dir.clickOnReaderSave();
				
				ExcelUtils.setCellData(actRes, i, 9, Constant.File_TestData_Directory );
				
				/*String actRes2 = mi.fetchIndGrpShows(indName2);
				System.out.println(actRes2);
				ExcelUtils.setCellData(actRes2, i, 11, Constant.File_TestData_ProfileList);
				
				String actRes3 = mi.fetchIndGrpShows(indName3);
				System.out.println(actRes3);
				ExcelUtils.setCellData(actRes3, i, 13, Constant.File_TestData_ProfileList);
				*/
				
				Boolean status = ValidationLib.verifyMsg(expRes, actRes);
				
				if(status)
				{
					ExcelUtils.setCellData("pass", i, 14,Constant.File_TestData_Directory);
				}
				else 
			{
					ExcelUtils.setCellData("Fail", i, 14,Constant.File_TestData_Directory);
				}
				
				
				}
				
				soft.assertAll();
	}
}
