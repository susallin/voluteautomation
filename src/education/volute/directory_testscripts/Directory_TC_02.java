package education.volute.directory_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Directory;
import education.volute.pages.LoginPage;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import education.volute.pages.SocialHub;
import utilities.Constant;

public class Directory_TC_02 extends SuperTestScript {

	@Test
	public void testDirectory_TC_02() 	
	{
					
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Directory, "AssignGroups");
			
			for(int i=1; i<=ExcelUtils.getRowcount(); i++)
			{
			
			String uName = ExcelUtils.getCellData(i, 1);
	        String pwd = ExcelUtils.getCellData(i, 2);
	        String actName = ExcelUtils.getCellData(i, 3);
	        String dirDName = ExcelUtils.getCellData(i, 4);
	        String grpName = ExcelUtils.getCellData(i, 5);
	        //String pgmNameTxt = ExcelUtils.getCellData(i, 3);
	        //String lsToolName = ExcelUtils.getCellData(i, 4);
	        //String lsNameTxt = ExcelUtils.getCellData(i, 5);
	        //String lsDescTxt = ExcelUtils.getCellData(i, 6);
	        //String sliDName = ExcelUtils.getCellData(i, 7);
	        //String dirName = ExcelUtils.getCellData(i, 8);
	        //String docName = ExcelUtils.getCellData(i, 9);
	        //String docAuthor = ExcelUtils.getCellData(i, 10);
	        String uRole = ExcelUtils.getCellData(i, 9);
	        String expRes = ExcelUtils.getCellData(i, 6);
	        
	        LoginPage lp = new LoginPage();
			NavigatorTool nt = new NavigatorTool();
			MenuItems mi = new MenuItems();
			SocialHub mj = new SocialHub();
			//Landing ld = new Landing();
			//ManagePrograms mp = new ManagePrograms();
			//ManageLearningSpaces mls = new ManageLearningSpaces();
			Directory dir = new Directory();
			//SubMenu sub = new SubMenu();
			
			lp.login(uName, pwd);
			lp.logibutton();
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			nt.clickOnMainMenu();
			/*mi.clickOnNavigation();
			mi.clickOnProgram();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			ld.clickOnLandingMP();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mp.clickOnselectedprogram(pgmNameTxt);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mp.clickOnManageMashupicon(pgmNameTxt);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/*mls.clickOnAddLS();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mls.selectaTool(lsToolName);
			mls.enterLSName(lsNameTxt);
			mls.enterLsDesc(lsDescTxt);
			mls.clickOnSaveButton();
			mls.clickOnConfigButton();*/
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			nt.clickOnJourney();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mj.clickOnLaunchAct(actName);
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			dir.clickOnDirectoryManage();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mi.clearDisplayName();
			mi.enterDisplayName(dirDName);
			mi.clickOnAssignUserButton();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mi.clickOnGroupTab();
			try {
				Thread.sleep(8000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}		
			mi.selectGroup(grpName);
			//dir.clickOnDirectoryGroupButton();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mi.clickOnSelectedTab();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			/*sub.ClickOnSubMenuMLS();
			try {
				Thread.sleep(8000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			//dir.clickOnReaderSave();
			
			String actRes = mi.fetchIndivGroupSelected(grpName);
			System.out.println(actRes);
			ExcelUtils.setCellData(actRes, i, 7, Constant.File_TestData_Directory );
			
			Boolean status = ValidationLib.verifyMsg(expRes, actRes);
			
			if(status)
			{
				ExcelUtils.setCellData("pass", i, 8,Constant.File_TestData_Directory);
			}
			else 
		{
				ExcelUtils.setCellData("Fail", i, 8,Constant.File_TestData_Directory);
			}
			
			
			}
	}
}
