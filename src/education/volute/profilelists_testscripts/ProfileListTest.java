package education.volute.profilelists_testscripts;

public class ProfileListTest {

	public void ProfileListAddIndiv(String Role) throws Exception{
		ProfileList_TC_01 ts = new ProfileList_TC_01();	
		ts.testProfileList_TC_01(Role);
	}
	
	public void ProfileListDeletIndv(String Role) throws Exception{
		ProfileList_TC_02 ts = new ProfileList_TC_02();
		ts.testProfileList_TC_02(Role);
	}
	
	public void ProfileListSearchIndv(String Role) throws Exception{
		profileList_TC_03 ts = new profileList_TC_03();
		ts.testProfileList_TC_03(Role);
	}
}
