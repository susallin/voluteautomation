package education.volute.profilelists_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.ProfileList;
import education.volute.pages.SocialHub;
import education.volute.pages.Directory;
import education.volute.pages.Landing;
import education.volute.pages.LoginPage;
//import education.volute.pages.ManageLearningSpaces;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import education.volute.pages.SubMenu;
import education.volute.pages.Video;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class ProfileList_TC_02 extends SuperTestScript{

	//@Test
	public void testProfileList_TC_02(String Role) 	
	{
		//LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		ReadCsvFiles csv = new ReadCsvFiles();
		MenuItems mi = new MenuItems();
		SoftAssertCheck soft = new SoftAssertCheck();
		//SocialHub mj = new SocialHub();
		//Landing ld = new Landing();
		//ManagePrograms mp = new ManagePrograms();
		//ManageLearningSpaces mls = new ManageLearningSpaces();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		Video vid = new Video();
		Directory dir = new Directory();
		ProfileList pro = new ProfileList();
		
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ProfileList, "DeleteInd");
			
			for(int i=1; i<=ExcelUtils.getRowcount(); i++)
			{
			
				//String uName = ExcelUtils.getCellData(i, 1);
		        //String pwd = ExcelUtils.getCellData(i, 2);
		        //String pgmNameTxt = ExcelUtils.getCellData(i, 3);
				String toolDest = ExcelUtils.getCellData(i, 1);
			    String actName = ExcelUtils.getCellData(i, 2);
			    String subActName = ExcelUtils.getCellData(i, 3);
			    String pgmNameTxt = ExcelUtils.getCellData(i, 4);
			    String toolName = ExcelUtils.getCellData(i, 5);
		        //String proDName = ExcelUtils.getCellData(i, 4);
		        String indNames = ExcelUtils.getCellData(i, 6);
		        String indName = ExcelUtils.getCellData(i, 8);
		        String indName2 = ExcelUtils.getCellData(i, 10);
		        String indName3 = ExcelUtils.getCellData(i, 12);
		        //String grpName = ExcelUtils.getCellData(i, 6);
		        //String ProName = ExcelUtils.getCellData(i, 8);
		        //String docName = ExcelUtils.getCellData(i, 9);
		        //String docAuthor = ExcelUtils.getCellData(i, 10);
		        String uRole = ExcelUtils.getCellData(i, 15);
		        String appLocName = ExcelUtils.getCellData(i, 16);
		        String expRes = ExcelUtils.getCellData(i, 7);
			
			/*lp.login(uName, pwd);
			lp.logibutton();
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			nt.clickOnMainMenu();*/
			toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
			/*mi.clickOnNavigation();
			mi.clickOnProgram();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			ld.clickOnLandingMP();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mp.clickOnselectedprogram(pgmNameTxt);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mp.clickOnManageMashupicon(pgmNameTxt);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/*mls.clickOnAddLS();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mls.selectaTool(lsToolName);
			mls.enterLSName(lsNameTxt);
			mls.enterLsDesc(lsDescTxt);
			mls.clickOnSaveButton();
			mls.clickOnConfigButton();*/
			/*try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mj.clickOnLaunchAct(actName);*/
			/*try {
				Thread.sleep(15000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			mi.fetchAppTool(appLocName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mi.SelectManageFaceOfTool(toolName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			//mi.clearDisplayName();
			//mi.enterDisplayName(proDName);
			//mi.clickOnAssignUserButton();
			pro.clickOnPLDeleteButton(indNames);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// This is the same button across tools
			vid.clickOnDeleteYesVideo();
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String toastMsg = dir.fetchDeleteMsgDirectory();
			System.out.println(toastMsg);
			if(toastMsg.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking toast");
				System.out.println("The test case passed toast shows.");
			}else{
				soft.assertTrue(false, "The test case failed toast does not show.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			/*mi.clickOnGroupTab();
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}		
			mi.selectGroup(grpName);
			//pro.clickOnPlGroupButton();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mi.clickOnSelectedTab();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			//dir.clickOnReaderSave();*/
			
			String actRes = pro.fetchManageFaceIndShows(indName);
			System.out.println(actRes);
			if(actRes.equalsIgnoreCase("Not Found")){
				soft.assertTrue(true, "Checking if User email shows after it was deleted");
				System.out.println("The test case passed User email does not show.");
			}else{
				soft.assertTrue(false, "The test case failed User does show.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    mi.SelectExitManageFaceOfTool(toolName);
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    String actRes1a = pro.fetchFaceIndShows(indName);
			System.out.println(actRes1a);
			if(actRes1a.equalsIgnoreCase("Not Found")){
				soft.assertTrue(true, "Checking if User shows after it was deleted");
				System.out.println("The test case passed User does not show on Face.");
			}else{
				soft.assertTrue(false, "The test case failed User does show on Face.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
			nt.refreshPage();
			/*try {
				Thread.sleep(15000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			mi.fetchAppTool(appLocName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String actRes1 = pro.fetchFaceIndShows(indName);
			System.out.println(actRes1);
			if(actRes1.equalsIgnoreCase("Not Found")){
				soft.assertTrue(true, "Checking if User shows after it was deleted");
				System.out.println("The test case passed User does not show on Face.");
			}else{
				soft.assertTrue(false, "The test case failed User does show on Face.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		    mi.SelectManageFaceOfTool(toolName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String actResShows = pro.fetchManageFaceIndShows(indName);
			System.out.println(actResShows);
			if(actResShows.equalsIgnoreCase("Not Found")){
				soft.assertTrue(true, "Checking if User email shows after it was deleted");
				System.out.println("The test case passed User email does not show.");
			}else{
				soft.assertTrue(false, "The test case failed User does show.\n");
			}
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			ExcelUtils.setCellData(actRes, i, 9, Constant.File_TestData_ProfileList);
			
			Boolean status = ValidationLib.verifyMsg(expRes, actRes);
			
			if(status)
			{
				ExcelUtils.setCellData("pass", i, 14,Constant.File_TestData_ProfileList);
			}
			else 
		{
				ExcelUtils.setCellData("Fail", i, 14,Constant.File_TestData_ProfileList);
			}
			
			
			}
			soft.assertAll();
	}
}

