package education.volute.ideaboard_testscripts;

import java.util.Iterator;

import education.volute.common_lib.SuperTestScript;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.IdeaBoard;
import education.volute.pages.MenuItems;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class IdeaBoard_TC_05 extends SuperTestScript{

public void testIdeaBoard_TC_05(String txt, String Role) throws Exception 
	
	{
		//LoginPage lp = new LoginPage();
		//NavigatorTool nt = new NavigatorTool();
		MenuItems mi = new MenuItems();
		//Landing ld = new Landing();
		//ManagePrograms mp = new ManagePrograms();
		//ManageLearningSpaces mls = new ManageLearningSpaces();
		ReadCsvFiles csv = new ReadCsvFiles();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		IdeaBoard id = new IdeaBoard();	
		SoftAssertCheck soft = new SoftAssertCheck();
		IdeaCheck ch = new IdeaCheck();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_IdeaBoard, txt);
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String toolDest = ExcelUtils.getCellData(i, 1);
	    String actName = ExcelUtils.getCellData(i, 2);
	    String subActName = ExcelUtils.getCellData(i, 3);
        String pgmNameTxt = ExcelUtils.getCellData(i, 4);
        String IdFilNames = ExcelUtils.getCellData(i, 5);
        //String expRes = ExcelUtils.getCellData(i, 6);
        String uRole = ExcelUtils.getCellData(i, 6);
        String appLocName = ExcelUtils.getCellData(i, 7);
		
		
        toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
		
		/*try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		mi.fetchAppTool(appLocName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		// We start here...
		
		/*id.clickAddIdeaButton();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		
		Iterator<String[]> FileName = csv.ReadList(IdFilNames);
		
		if(FileName.hasNext())
			FileName.next();
		
		while(FileName.hasNext()){
			int n = 0;
			String[] Name = FileName.next();
			
			String idText = Name[n];
			String idName = Name[n+1];
			String idComment = Name[n + 2];
			String idFile = Name[n+3];
			String idNewContent = Name[n+4];
			
			id.ClickOnSelectedIdeaButton(idName);
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			ch.check(idText, idName, idComment, idFile, idNewContent);
			
			id.ClickBackIdeaTapButton();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		
	}
	soft.assertAll();
		
 }
}
