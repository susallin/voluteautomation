package education.volute.ideaboard_testscripts;

import education.volute.pages.IdeaBoard;
import education.volute.pages.NavigatorTool;
import utilities.SoftAssertCheck;

public class IdeaCheck {

	public void check(String idtext, String name, String content, String File, String newContent) throws Exception {
		
		IdeaBoard id = new IdeaBoard();	
		NavigatorTool nt = new NavigatorTool();
		SoftAssertCheck soft = new SoftAssertCheck();
		boolean flag = true;
		
		if(idtext.equalsIgnoreCase("Empty")) {
			
			id.clickOnSaveIdeaButton();
			
			String errorMsg1 = id.fetchIdeaTitleErrorMsg();
			System.out.println(errorMsg1);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			if(errorMsg1.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking error message Idea title must be at least 2 characters.");
				System.out.println("The test case passed on verifying the toast message (Idea title must be at least 2 characters.).");
			}else{
				soft.assertTrue(false, "The test case failed on verifying the toast message (Idea title must be at least 2 characters.).\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		if(idtext.equalsIgnoreCase("Title")) {
			
			id.clearIdeaTitle();
			id.enterIdeaTitle(name);
			id.clearIdeaContent();
			id.enterIdeaContent(content);
		
			id.clickOnSelectImageButton(File);
			/*try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
			Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+File);*/
			while(flag){
				String result = nt.fetchProgressFile();
				
				if(result.equalsIgnoreCase("Found")){
					id.clickOnSaveIdeaButton();
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					flag = false;
				}
				else{
					flag = true;
				}
			}
			
			String errorMsg2 = id.fetchIdeaTitleErrorMsg();
			System.out.println(errorMsg2);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			if(errorMsg2.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking error message Idea title must be at least 2 characters.");
				System.out.println("The test case passed on verifying the toast message (Idea title must be at least 2 characters.).");
			}else{
				soft.assertTrue(false, "The test case failed on verifying the toast message (Idea title must be at least 2 characters.).\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    id.clickOnDeleteFileButton();
		}
		
		
		if(idtext.equalsIgnoreCase("Content")) {
			
			id.clearIdeaTitle();
			id.enterIdeaTitle(name);
			id.clearIdeaContent();
			id.enterIdeaContent(content);
		
			/*id.clickOnSelectImageButton();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
			Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+idFile);
			*/try {
				Thread.sleep(4000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
			id.clickOnSaveIdeaButton();
			
			String errorMsg3 = id.fetchIdeaContentErrorMsg();
			System.out.println(errorMsg3);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			if(errorMsg3.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking error message Idea content must be at least 2 characters.");
				System.out.println("The test case passed on verifying the toast message (Idea content must be at least 2 characters.).");
			}else{
				soft.assertTrue(false, "The test case failed on verifying the toast message (Idea content must be at least 2 characters.).\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    id.clickOnDeleteFileButton();
		}
		
		
		if(idtext.equalsIgnoreCase("File")) {
		
			id.clearIdeaTitle();
			id.enterIdeaTitle(name);
			id.clearIdeaContent();
			id.enterIdeaContent(content);
			
			id.clickOnSelectImageButton(File);
			/*try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
			Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+File);*/
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String errorMsg4 = id.fetchIdeaFileErrorMsg();
			System.out.println(errorMsg4);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			if(errorMsg4.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking error message File is too large or file type is not supported.");
				System.out.println("The test case passed on verifying the toast message (File is too large or file type is not supported.).");
			}else{
				soft.assertTrue(false, "The test case failed on verifying the toast message (File is too large or file type is not supported.).\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    id.clickOnDeleteFileButton();
		}
		
		if(idtext.equalsIgnoreCase("Add")) {
			
			id.clearIdeaTitle();
			id.enterIdeaTitle(name);
			id.clearIdeaContent();
			id.enterIdeaContent(content);
		
			id.clickOnSelectImageButton(File);
			/*try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
			Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+File);*/
			while(flag){
				String result = nt.fetchProgressFile();
				
				if(result.equalsIgnoreCase("Found")){
					id.clickOnSaveIdeaButton();
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					flag = false;
				}
				else{
					flag = true;
				}
			}
			/*try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
			id.clickOnSaveIdeaButton();*/
			
			String confirmMsg5 = id.fetchIdeaConfirmationMsg();
			System.out.println(confirmMsg5);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			if(confirmMsg5.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking add Idea message Idea created successfully");
				System.out.println("The test case passed on verifying the toast message (Idea created successfully).");
			}else{
				soft.assertTrue(false, "The test case failed on verifying the toast message (Idea created successfully).\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		if(idtext.equalsIgnoreCase("Edit")) {
			
			id.clickOnEditIdeaButton();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			id.clearIdeaTitle();
			id.enterIdeaTitle(name);
			id.clearIdeaContent();
			id.enterIdeaContent(content);
		
			id.clickOnSelectImageButton(File);
			/*try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
			Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+File);*/
			while(flag){
				String result = nt.fetchProgressFile();
				
				if(result.equalsIgnoreCase("Found")){
					id.clickOnSaveIdeaButton();
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					flag = false;
				}
				else{
					flag = true;
				}
			}
			/*try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
			id.clickOnSaveIdeaButton();*/
			
			String upMsg6 = id.fetchIdeaUpdatedMsg();
			System.out.println(upMsg6);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			if(upMsg6.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking update Idea message Idea updated successfully");
				System.out.println("The test case passed on verifying the toast message (Idea updated successfully).");
			}else{
				soft.assertTrue(false, "The test case failed on verifying the toast message (Idea updated successfully).\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		
		if(idtext.equalsIgnoreCase("Delete")) {
			
			id.clickOnDeleteIdeaButton();
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			nt.clickOnDeletePgmYesButton();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String delMsg7 = id.fetchIdeaDeletetedMsg();
			System.out.println(delMsg7);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			if(delMsg7.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking delete Idea message Idea deleted successfully");
				System.out.println("The test case passed on verifying the toast message (Idea deleted successfully).");
			}else{
				soft.assertTrue(false, "The test case failed on verifying the toast message (Idea deleted successfully).\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    
		}
		
		if(idtext.equalsIgnoreCase("Addcm")) {
			
			id.clickAddCommentButton();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			id.enterComment(content);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			id.clickSubmitCommentButton();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String addcmMsg8 = id.fetchCommentConfirmationMsg();
			System.out.println(addcmMsg8);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			if(addcmMsg8.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking add comment message Commented successfully");
				System.out.println("The test case passed on verifying the toast message (Commented successfully).");
			}else{
				soft.assertTrue(false, "The test case failed on verifying the toast message (Commented successfully).\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		
		if(idtext.equalsIgnoreCase("Editcm")||idtext.equalsIgnoreCase("Editreply")) {
			
			id.clickOnEditCommentButton(content);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			id.clearIdeaComment();
			id.enterComment(newContent);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			id.clickSubmitCommentButton();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String editcmMsg9 = id.fetchCommentUpdatedMsg();
			System.out.println(editcmMsg9);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			if(editcmMsg9.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking update comment message Comment updated successfully");
				System.out.println("The test case passed on verifying the toast message (Comment updated successfully).");
			}else{
				soft.assertTrue(false, "The test case failed on verifying the toast message (Comment updated successfully).\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		if(idtext.equalsIgnoreCase("Delcm")||idtext.equalsIgnoreCase("Delreply")) {
			
			id.clickOnDeleteCommentButton(content);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			nt.clickOnDeletePgmYesButton();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String delcmMsg10 = id.fetchCommentDeletedMsg();
			System.out.println(delcmMsg10);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			if(delcmMsg10.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking delete comment message Comment deleted successfully");
				System.out.println("The test case passed on verifying the toast message (Comment deleted successfully).");
			}else{
				soft.assertTrue(false, "The test case failed on verifying the toast message (Comment deleted successfully).\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		if(idtext.equalsIgnoreCase("Reply")) {
			
			id.clickOnReplyButton(content);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			id.enterComment(newContent);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			id.clickSubmitCommentButton();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String addryMsg11 = id.fetchCommentConfirmationMsg();
			System.out.println(addryMsg11);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			if(addryMsg11.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking add comment message Commented successfully");
				System.out.println("The test case passed on verifying the toast message (Commented successfully).");
			}else{
				soft.assertTrue(false, "The test case failed on verifying the toast message (Commented successfully).\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
}
