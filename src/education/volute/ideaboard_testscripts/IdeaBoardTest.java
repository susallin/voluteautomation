package education.volute.ideaboard_testscripts;

public class IdeaBoardTest {

	public void IdeaBoardErrorMessageCheck(String Role) throws Exception{
		IdeaBoard_TC_01 id = new IdeaBoard_TC_01();	
		id.testIdeaBoard_TC_01(Role);
	}
	
	public void AddIdea(String Role) throws Exception{
		IdeaBoard_TC_02 id = new IdeaBoard_TC_02();	
		id.testIdeaBoard_TC_02(Role);
	}
	
	public void EditIdea(String Role) throws Exception{
		IdeaBoard_TC_03 id = new IdeaBoard_TC_03();	
		id.testIdeaBoard_TC_03(Role);
	}
	
	public void DeleteIdea(String Role) throws Exception{
		IdeaBoard_TC_04 id = new IdeaBoard_TC_04();	
		id.testIdeaBoard_TC_04(Role);
	}
	
	public void CommentReplyIdea(String txt, String Role) throws Exception{
		IdeaBoard_TC_05 id = new IdeaBoard_TC_05();	
		id.testIdeaBoard_TC_05(txt, Role);
	}
}
