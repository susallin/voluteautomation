package education.volute.ideaboard_testscripts;

import java.util.Iterator;

import education.volute.common_lib.SuperTestScript;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.IdeaBoard;
import education.volute.pages.MenuItems;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class IdeaBoard_TC_01 extends SuperTestScript{

public void testIdeaBoard_TC_01(String Role) throws Exception 
	
	{
		//LoginPage lp = new LoginPage();
		//NavigatorTool nt = new NavigatorTool();
		MenuItems mi = new MenuItems();
		//Landing ld = new Landing();
		//ManagePrograms mp = new ManagePrograms();
		//ManageLearningSpaces mls = new ManageLearningSpaces();
		ReadCsvFiles csv = new ReadCsvFiles();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		IdeaBoard id = new IdeaBoard();	
		SoftAssertCheck soft = new SoftAssertCheck();
		IdeaCheck ch = new IdeaCheck();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_IdeaBoard, "IdeaBoardErrors");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String toolDest = ExcelUtils.getCellData(i, 1);
	    String actName = ExcelUtils.getCellData(i, 2);
	    String subActName = ExcelUtils.getCellData(i, 3);
        String pgmNameTxt = ExcelUtils.getCellData(i, 4);
        String IdFilNames = ExcelUtils.getCellData(i, 5);
        //String expRes = ExcelUtils.getCellData(i, 6);
        String uRole = ExcelUtils.getCellData(i, 6);
        String appLocName = ExcelUtils.getCellData(i, 7);
		
		
        toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
		
		/*try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		mi.fetchAppTool(appLocName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		// We start here...
		
		/*id.clickAddIdeaButton();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		
		Iterator<String[]> FileName = csv.ReadList(IdFilNames);
		
		if(FileName.hasNext())
			FileName.next();
		
		while(FileName.hasNext()){
			int n = 0;
			String[] Name = FileName.next();
			
			String idText = Name[n];
			String idName = Name[n+1];
			String idContent = Name[n + 2];
			String idFile = Name[n+3];
			String idNewContent = Name[n+4];
			
			id.clickAddIdeaButton();
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			ch.check(idText, idName, idContent, idFile, idNewContent);
			
			id.clickOnCancelIdeaButton();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			/*if(idText.equalsIgnoreCase("Empty")) {
				
				id.clickOnSaveIdeaButton();
				
				String errorMsg1 = id.fetchIdeaTitleErrorMsg();
				System.out.println(errorMsg1);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				if(errorMsg1.equalsIgnoreCase("Found")){
					soft.assertTrue(true, "Checking error message Idea title must be at least 2 characters.");
					System.out.println("The test case passed on verifying the toast message (Idea title must be at least 2 characters.).");
				}else{
					soft.assertTrue(false, "The test case failed on verifying the toast message (Idea title must be at least 2 characters.).\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			if(idText.equalsIgnoreCase("Title")) {
				
				id.clearIdeaTitle();
				id.enterIdeaTitle(idName);
				id.clearIdeaContent();
				id.enterIdeaContent(idContent);
			
				id.clickOnSelectImageButton();
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			
				Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+idFile);
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			
				id.clickOnSaveIdeaButton();
				
				String errorMsg2 = id.fetchIdeaTitleErrorMsg();;
				System.out.println(errorMsg2);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				if(errorMsg2.equalsIgnoreCase("Found")){
					soft.assertTrue(true, "Checking error message Idea title must be at least 2 characters.");
					System.out.println("The test case passed on verifying the toast message (Idea title must be at least 2 characters.).");
				}else{
					soft.assertTrue(false, "The test case failed on verifying the toast message (Idea title must be at least 2 characters.).\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    id.clickOnDeleteFileButton();
			}
			
			
			if(idText.equalsIgnoreCase("Content")) {
				
				id.clearIdeaTitle();
				id.enterIdeaTitle(idName);
				id.clearIdeaContent();
				id.enterIdeaContent(idContent);
			
				/*id.clickOnSelectImageButton();
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			
				Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+idFile);
				*//*try {
					Thread.sleep(4000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			
				id.clickOnSaveIdeaButton();
				
				String errorMsg3 = id.fetchIdeaContentErrorMsg();
				System.out.println(errorMsg3);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				if(errorMsg3.equalsIgnoreCase("Found")){
					soft.assertTrue(true, "Checking error message Idea content must be at least 2 characters.");
					System.out.println("The test case passed on verifying the toast message (Idea content must be at least 2 characters.).");
				}else{
					soft.assertTrue(false, "The test case failed on verifying the toast message (Idea content must be at least 2 characters.).\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    id.clickOnDeleteFileButton();
			}
			
			
			if(idText.equalsIgnoreCase("File")) {
			
				id.clearIdeaTitle();
				id.enterIdeaTitle(idName);
				id.clearIdeaContent();
				id.enterIdeaContent(idContent);
				
				id.clickOnSelectImageButton();
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			
				Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+idFile);
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				String errorMsg4 = id.fetchIdeaFileErrorMsg();
				System.out.println(errorMsg4);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				if(errorMsg4.equalsIgnoreCase("Found")){
					soft.assertTrue(true, "Checking error message File is too large or file type is not supported.");
					System.out.println("The test case passed on verifying the toast message (File is too large or file type is not supported.).");
				}else{
					soft.assertTrue(false, "The test case failed on verifying the toast message (File is too large or file type is not supported.).\n");
				}
			    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			    id.clickOnDeleteFileButton();
			}*/
		}
		
		
	}
	soft.assertAll();
		
 }
}
	

