package education.volute.slides_testscripts;


import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
//import education.volute.pages.Landing;
import education.volute.pages.LoginPage;
//import education.volute.pages.ManageLearningSpaces;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.SocialHub;
import education.volute.pages.NavigatorTool;
import education.volute.pages.Slides;
import utilities.Constant;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class Slides_TC_03 extends SuperTestScript {

	//@Test
	public void testSlides_TC_03(String Role) //throws IOException 
	
	{
		 //LoginPage lp = new LoginPage();
		 NavigatorTool nt = new NavigatorTool();
	     //SocialHub mj = new SocialHub();
		 MenuItems mi = new MenuItems();
		 SoftAssertCheck soft = new SoftAssertCheck();
		 //Landing ld = new Landing();
		 //ManagePrograms mp = new ManagePrograms();
		 //ManageLearningSpaces mls = new ManageLearningSpaces();
		 ToolDestAndCollections toolD = new ToolDestAndCollections();
		 Slides sli = new Slides();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Slides, "DeleteSlides3");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		//String uName = ExcelUtils.getCellData(i, 1);
        //String pwd = ExcelUtils.getCellData(i, 2);
        //String pgmNameTxt = ExcelUtils.getCellData(i, 3);
        //String lsNameTxt = ExcelUtils.getCellData(i, 4);
        //String lsNameTxt = ExcelUtils.getCellData(i, 5);
        //String lsDescTxt = ExcelUtils.getCellData(i, 6);
        //String sliDName = ExcelUtils.getCellData(i, 7);
        String toolDest = ExcelUtils.getCellData(i, 1);
        String actName = ExcelUtils.getCellData(i, 2);
        //String sliDName = ExcelUtils.getCellData(i, 5);
        String subActName = ExcelUtils.getCellData(i, 3);
        String pgmNameTxt = ExcelUtils.getCellData(i, 4);
        String sliFileName = ExcelUtils.getCellData(i, 5);
        //String sliSName = ExcelUtils.getCellData(i, 6);
        //String sliFilName = ExcelUtils.getCellData(i, 9);
        String uRole = ExcelUtils.getCellData(i, 10);
        String appLocName = ExcelUtils.getCellData(i, 11);
        String toolName = ExcelUtils.getCellData(i, 12);
        String expRes = ExcelUtils.getCellData(i, 7);
		
		/*toolD.login(uName, pwd);
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
		/*nt.clickOnMainMenu();
		//mi.clickOnNavigation();
		//mi.clickOnProgram();
		/*try {
			Thread.sleep(8000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnselectedprogram(pgmNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnManageMashupicon(pgmNameTxt);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*mls.selectaLearningSpace(lsNameTxt);
		mls.clickOnEditButton(lsNameTxt);
		mls.clickOnConfigButton();
		nt.clickOnJourney();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mj.clickOnLaunchAct(actName);*/
		/*try {
			Thread.sleep(12000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		mi.fetchAppTool(appLocName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mi.SelectManageFaceOfTool(toolName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//mi.clearDisplayName();
		//mi.enterDisplayName(sliDName);
		//sli.enterSlideDisplayName(sliDName);
		sli.clickOnSlideDeleteButton(sliFileName);
		sli.clickOnDeleteYesSlides();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String actRes = sli.fetchSlidesDeleteMsg();//Deleted**
		System.out.println(actRes);//actRes);
		
		String slidShows1 = sli.fetchManageFacesliShows(sliFileName);
		System.out.println(slidShows1);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(slidShows1.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking Content does not show on the manage face of the tool");
			System.out.println("The test case passed on the content not showing on the manage face of the tool.");
		}else{
			soft.assertTrue(false, "The test case failed on the content not showing on the manage face of the tool.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    
	    mi.SelectExitManageFaceOfTool(toolName);
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    
	    String slidShows1a = sli.fetchFacesliShows(sliFileName);
		System.out.println(slidShows1a);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(slidShows1a.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking Content does not show on the face of the tool");
			System.out.println("The test case passed on the content not showing on the face of the tool.");
		}else{
			soft.assertTrue(false, "The test case failed on the content not showing on the face of the tool.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    
		nt.refreshPage();
		/*try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		mi.fetchAppTool(appLocName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String slidShows1ab = sli.fetchFacesliShows(sliFileName);
		System.out.println(slidShows1ab);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(slidShows1ab.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking Content does not show on the face of the tool");
			System.out.println("The test case passed on the content not showing on the face of the tool.");
		}else{
			soft.assertTrue(false, "The test case failed on the content not showing on the face of the tool.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	    mi.SelectManageFaceOfTool(toolName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// This will determine if Value is still showing or not.
		String slidShows = sli.fetchManageFacesliShows(sliFileName);
		System.out.println(slidShows);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(slidShows.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking Content does not show on the manage face of the tool");
			System.out.println("The test case passed on the content not showing on the manage face of the tool.");
		}else{
			soft.assertTrue(false, "The test case failed on the content not showing on the manage face of the tool.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ExcelUtils.setCellData(slidShows, i, 6, Constant.File_TestData_Slides);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//toolD.logout();
		
		ExcelUtils.setCellData(actRes, i, 8, Constant.File_TestData_Slides);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 9,Constant.File_TestData_Slides);
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 9,Constant.File_TestData_Slides);
		}
		
		
		}
		soft.assertAll();
		}
}
