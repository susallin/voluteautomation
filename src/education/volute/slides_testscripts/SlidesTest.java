package education.volute.slides_testscripts;

import java.io.IOException;

public class SlidesTest {

	public void SlidesAdd(String Role) throws Exception{
		Slides_TC_01 ts = new Slides_TC_01();	
		ts.testSlides_TC_01(Role);
	}
	
	public void SlidesEdit(String Role) throws Exception {
		Slides_TC_02 ts = new Slides_TC_02();	
		ts.testSlides_TC_02(Role);
	}
	
	public void SlidesDelete(String Role){
		Slides_TC_03 ts = new Slides_TC_03();	
		ts.testSlides_TC_03(Role);
	}
	
	public void SlidesAddShowsOnManageFace() throws Exception{
		Slides_TC_04 ts = new Slides_TC_04();	
		ts.testSlides_TC_04();
	}
	
	public void SlidesAddShowsOnFace() throws Exception{
		Slides_TC_05 ts = new Slides_TC_05();	
		ts.testSlides_TC_05();
	}
}
