package education.volute.slides_testscripts;

import java.util.Iterator;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.MenuItems;
import education.volute.pages.Slides;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.ToolDestAndCollections;

public class Slides_TC_05 extends SuperTestScript {

	@Test
	public void testSlides_TC_05() throws Exception 
	
	{
		//LoginPage lp = new LoginPage();
		//NavigatorTool nt = new NavigatorTool();
		ReadCsvFiles csv = new ReadCsvFiles();
		MenuItems mi = new MenuItems();
		//Landing ld = new Landing();
        //ocialHub mj = new SocialHub();
		//ManagePrograms mp = new ManagePrograms();
		//ManageLearningSpaces mls = new ManageLearningSpaces();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		Slides sli = new Slides();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Slides, "AddSlides5");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String uName = ExcelUtils.getCellData(i, 1);
        String pwd = ExcelUtils.getCellData(i, 2);
        //String lsToolName = ExcelUtils.getCellData(i, 4);
        //String lsNameTxt = ExcelUtils.getCellData(i, 5);
        //String lsDescTxt = ExcelUtils.getCellData(i, 6);
        //String sliDName = ExcelUtils.getCellData(i, 7);
        String toolDest = ExcelUtils.getCellData(i, 3);
        String actName = ExcelUtils.getCellData(i, 4);
        String sliDName = ExcelUtils.getCellData(i, 5);
        String subActName = ExcelUtils.getCellData(i, 6);
        String pgmNameTxt = ExcelUtils.getCellData(i, 7);
        //String sliSNames = ExcelUtils.getCellData(i, 7);
        String sliFilNames = ExcelUtils.getCellData(i, 8);
        String sliFImg = ExcelUtils.getCellData(i, 9);
        String expRes = ExcelUtils.getCellData(i, 10);
		
		toolD.login(uName, pwd);
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		toolD.Destination(toolDest, actName, subActName, pgmNameTxt);
		
		// Here Tool will execute 
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		sli.clickOnSlidesManage();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
			mi.clearDisplayName();
			mi.enterDisplayName(sliDName);
				
			Iterator<String[]> FileName = csv.ReadList(sliFilNames);
				
			if(FileName.hasNext())
				FileName.next();
				
			while(FileName.hasNext()){
				int n = 0;
				String[] Name = FileName.next();
				//System.out.println(" Values are ");

				String File = Name[n];
				String SliSName = Name[n + 1];
				
				sli.enterSlideShowName(SliSName);
				
				sli.clickOnSlideSelectFile();//sliFilName);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+File);
				try {
					Thread.sleep(15000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				sli.clickOnSlidesShowSave();
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

					
			}
				sli.clickOnSlidesBackArrow();
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				String actRes = sli.fetchSliShowsFaceImg(sliFImg);
				System.out.println(actRes);

				toolD.logout();
				
				ExcelUtils.setCellData(actRes, i, 11, Constant.File_TestData_Slides);
				
				Boolean status = ValidationLib.verifyMsg(expRes, actRes);
				
				if(status)
				{
					ExcelUtils.setCellData("pass", i, 12,Constant.File_TestData_Slides);
				}
				else 
			{
					ExcelUtils.setCellData("Fail", i, 12,Constant.File_TestData_Slides);
				}
				
				
				}
				
		}
}
