package education.volute.slides_testscripts;

import java.io.IOException;
import java.util.Iterator;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
//import education.volute.pages.Landing;
import education.volute.pages.LoginPage;
//import education.volute.pages.ManageLearningSpaces;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.SocialHub;
import education.volute.pages.NavigatorTool;
import education.volute.pages.Slides;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.ToolDestAndCollections;

public class Slides_TC_02 extends SuperTestScript 
{
	
	//@Test
	public void testSlides_TC_02(String Role) throws Exception 
	
	{
		//LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		//SocialHub mj = new SocialHub();
		ReadCsvFiles csv = new ReadCsvFiles();
		MenuItems mi = new MenuItems();
		//Landing ld = new Landing();
		//ManagePrograms mp = new ManagePrograms();
		//ManageLearningSpaces mls = new ManageLearningSpaces();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		Slides sli = new Slides();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Slides, "EditSlides_02_P3");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		//String uName = ExcelUtils.getCellData(i, 1);
        //String pwd = ExcelUtils.getCellData(i, 2);
        //String pgmNameTxt = ExcelUtils.getCellData(i, 3);
        //String lsNameTxt = ExcelUtils.getCellData(i, 4);
        //String lsNameTxt = ExcelUtils.getCellData(i, 5);
        //String lsDescTxt = ExcelUtils.getCellData(i, 6);
        //String sliDName = ExcelUtils.getCellData(i, 7);
        String toolDest = ExcelUtils.getCellData(i, 1);
        String actName = ExcelUtils.getCellData(i, 2);
        //String sliDName = ExcelUtils.getCellData(i, 5);
        String subActName = ExcelUtils.getCellData(i, 3);
        String pgmNameTxt = ExcelUtils.getCellData(i, 4);
        String sliFileNames = ExcelUtils.getCellData(i, 5);
        //String sliNewFileName = ExcelUtils.getCellData(i, 6);
        //String sliSName = ExcelUtils.getCellData(i, 7);
        //String sliFilName = ExcelUtils.getCellData(i, 9);
        String uRole = ExcelUtils.getCellData(i, 9);
        String appLocName = ExcelUtils.getCellData(i, 10);
        String toolName = ExcelUtils.getCellData(i, 11);
        String expRes = ExcelUtils.getCellData(i, 6);
		
		/*toolD.login(uName, pwd);
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
		/*nt.clickOnMainMenu();
		//mi.clickOnNavigation();
		//mi.clickOnProgram();
		/*try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnselectedprogram(pgmNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnManageMashupicon(pgmNameTxt);
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*mls.selectaLearningSpace(lsNameTxt);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mls.clickOnEditButton(lsNameTxt);
		mls.clickOnConfigButton();
		nt.clickOnJourney();
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mj.clickOnLaunchAct(actName);*/
		
		/*try {
			Thread.sleep(12000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		mi.fetchAppTool(appLocName);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mi.SelectManageFaceOfTool(toolName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Iterator<String[]> FileName = csv.ReadList(sliFileNames);
		
		if(FileName.hasNext())
			FileName.next();
		
		while(FileName.hasNext()){
			int n = 0;
			String[] Name = FileName.next();
			boolean flag = true;
			
			String OldSName = Name[n];
			String OldSNameEdit = Name[n + 1];
			String File = Name[n + 2];
			String SliSName = Name[n + 3];
			
			sli.clickOnselectedSlide(OldSName);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			sli.clickOnSlideEditButton(OldSNameEdit);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			sli.clickOnSlidesShowCancel();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			nt.clickOnDeletePgmYesButton();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			//sli.clearSlideShowName();
			sli.enterSlideShowName(SliSName);
			sli.clickOnSlideSelectFile(File);//sliFilName);
			/*try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			
			//Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+File);
			while(flag){
				String result = nt.fetchProgressFile();
				
				if(result.equalsIgnoreCase("Found")){
					sli.clickOnSlidesShowSave();
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					flag = false;
				}
				else{
					flag = true;
				}
			}
		}
		//mi.clearDisplayName();
		//mi.enterDisplayName(sliDName);
		//sli.enterSlideDisplayName(sliDName);
		
		/*try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		
		String actRes = sli.fetchSlidesAddMsg();//Updating**
		System.out.println(actRes);
		
		//toolD.logout();
		
		ExcelUtils.setCellData(actRes, i, 7, Constant.File_TestData_Slides);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 8,Constant.File_TestData_Slides);
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 8,Constant.File_TestData_Slides);
		}
		
		
		}
		
		}
}
