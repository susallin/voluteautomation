package education.volute.slides_testscripts;

//import java.io.IOException;
import java.util.Iterator;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
//import education.volute.pages.LoginPage;
import education.volute.pages.Slides;
//import education.volute.pages.ManageLearningSpaces;
import education.volute.pages.ManagePrograms;
//import education.volute.pages.Landing;
import education.volute.pages.MenuItems;
import education.volute.pages.SocialHub;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.ToolDestAndCollections;

// Note: Purpose of this Add Learning Space is to add a single tool and test them independently.
// We can get into the tools to execute.
// This one is for Slides Tools

public class Slides_TC_01 extends SuperTestScript {
	//@Test
	public void testSlides_TC_01(String Role) throws Exception 
	
	{
		//LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		ReadCsvFiles csv = new ReadCsvFiles();
		MenuItems mi = new MenuItems();
		//Landing ld = new Landing();
        //SocialHub mj = new SocialHub();
		//ManagePrograms mp = new ManagePrograms();
		//ManageLearningSpaces mls = new ManageLearningSpaces();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		Slides sli = new Slides();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Slides, "AddSlides3");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		//String uName = ExcelUtils.getCellData(i, 1);
        //String pwd = ExcelUtils.getCellData(i, 2);
        //String lsToolName = ExcelUtils.getCellData(i, 4);
        //String lsNameTxt = ExcelUtils.getCellData(i, 5);
        //String lsDescTxt = ExcelUtils.getCellData(i, 6);
        //String sliDName = ExcelUtils.getCellData(i, 7);
        String toolDest = ExcelUtils.getCellData(i, 1);
        String actName = ExcelUtils.getCellData(i, 2);
        String subActName = ExcelUtils.getCellData(i, 3);
        String pgmNameTxt = ExcelUtils.getCellData(i, 4);
        String toolName = ExcelUtils.getCellData(i, 5);
        //String sliSNames = ExcelUtils.getCellData(i, 7);
        String sliFilNames = ExcelUtils.getCellData(i, 6);
        String uRole = ExcelUtils.getCellData(i, 10);
        String appLocName = ExcelUtils.getCellData(i, 11);
        String expRes = ExcelUtils.getCellData(i, 7);
      
        String autoitscriptpath = "\\"+"File_upload_selenium_webdriver.au";
		
		/*toolD.login(uName, pwd);
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		/*toolD.selectProgram(pgmNameTxt);
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
		//nt.clickOnMainMenu();
		
		
		
		//mi.clickOnNavigation();
		//mi.clickOnProgram();
		
		/*try {
			Thread.sleep(8000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnselectedprogram(pgmNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnManageMashupicon(pgmNameTxt);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		/*mls.clickOnAddLS();
		mls.selectaTool(lsToolName);
		mls.enterLSName(lsNameTxt);
		mls.enterLsDesc(lsDescTxt);
		mls.clickOnSaveButton();
		mls.clickOnConfigButton();*/
		//nt.clickOnJourney();
		
		
		/*try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mj.clickOnLaunchAct(actName);*/
		
		// Here Tool will execute 
		/*try {
			Thread.sleep(12000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		mi.fetchAppTool(appLocName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mi.SelectManageFaceOfTool(toolName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//mi.clearDisplayName();
		//mi.enterDisplayName(sliDName);
		//mi.Savedisplay(tool);
		
		/*mi.SelectAdd(toolName);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		// This is where we read and upload our files.
		
		//sli.enterSlideDisplayName(sliDName);
		/*sli.enterSlideShowName(sliSName);
		sli.clickOnSlideSelectFile();//sliFilName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+"codpaste-teaching#pack#.pdf");
		try {
			Thread.sleep(70000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		sli.clickOnSlidesShowSave();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		
		Iterator<String[]> FileName = csv.ReadList(sliFilNames);
		
		if(FileName.hasNext())
			FileName.next();
		
		while(FileName.hasNext()){
			int n = 0;
			String[] Name = FileName.next();
			boolean flag = true;
			//System.out.println(" Values are ");

			String addSlid = Name[n];
			String addBackSli = Name[n+1];
			String nameBackButton = Name[n+2];
			String File = Name[n + 3];
			String SliSName = Name[n + 4];
			
			if(addSlid.equalsIgnoreCase("yes")){
				mi.SelectAdd(toolName);
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
//			mi.SelectAdd(toolName);
//			try {
//				Thread.sleep(3000);
//			} catch (InterruptedException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
			mi.fetchAppTool("paper-input[label='Slideshow Name:'] input");
			sli.enterSlideShowName(SliSName);
			
			sli.clickOnSlideSelectFile();//(File);//sliFilName);
			/*try {
				Thread.sleep(500000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			/*Runtime.getRuntime().exec(
					"cmd.exe /c Start AutoIt3.exe " + autoitscriptpath + " \""
							+ filepath + "\"");*/
			
			Runtime.getRuntime().exec("cmd.exe /c Start AutoIt3.exe " + autoitscriptpath + " \""
					+ "./user_data/Autoit/FileUploadPDFNew.exe"+" "+File + "\"");
			
				while(flag){
					String result = nt.fetchProgressFile();
					
					if(result.equalsIgnoreCase("Found")){
						
						if(addBackSli.equalsIgnoreCase("yes")){
							mi.clickBackSaveListButton(nameBackButton);
							try {
								Thread.sleep(1000);
							} catch (InterruptedException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						} else{
							sli.clickOnSlidesShowSave();
							try {
								Thread.sleep(1000);
							} catch (InterruptedException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
						
						flag = false;
					}
					else{
						flag = true;
					}
				}
		}
		/*
		sli.enterSlideShowName("File2");
		sli.clickOnSlideSelectFile();//sliFilName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+"PIP_Fut;ure!_of_High;er_Ed!.pdf");
		try {
			Thread.sleep(50000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		sli.clickOnSlidesShowSave();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		sli.enterSlideShowName("File3");
		sli.clickOnSlideSelectFile();//sliFilName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+"AlabamaHand,book,'RJ'.pdf");
		try {
			Thread.sleep(70000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		sli.clickOnSlidesShowSave();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		sli.enterSlideShowName("File4");
		sli.clickOnSlideSelectFile();//sliFilName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+"Plant-based&MilkArticle&.pdf");
		try {
			Thread.sleep(50000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		sli.clickOnSlidesShowSave();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		sli.enterSlideShowName("File5");
		sli.clickOnSlideSelectFile();//sliFilName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+"design-thin..king(RJ).pdf");
		try {
			Thread.sleep(70000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		sli.clickOnSlidesShowSave();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		sli.enterSlideShowName("File6");
		sli.clickOnSlideSelectFile();//sliFilName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+"hand=book`One`.pdf");
		try {
			Thread.sleep(50000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		sli.clickOnSlidesShowSave();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		sli.enterSlideShowName("File7");
		sli.clickOnSlideSelectFile();//sliFilName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+"Intercom+on-Jobs+to-be+Done%1%copy.pdf");
		try {
			Thread.sleep(50000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		sli.clickOnSlidesShowSave();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		sli.enterSlideShowName("File8");
		sli.clickOnSlideSelectFile();//sliFilName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+"Map{-of~South~Kensington}-Campus-~pdf~.pdf");
		try {
			Thread.sleep(50000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		sli.clickOnSlidesShowSave();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		sli.enterSlideShowName("File9");
		sli.clickOnSlideSelectFile();//sliFilName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+"encry^ption.[original].pdf");
		try {
			Thread.sleep(50000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		sli.clickOnSlidesShowSave();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		sli.enterSlideShowName("File10");
		sli.clickOnSlideSelectFile();//sliFilName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+"fund_accred_20ques_02.pdf");
		try {
			Thread.sleep(50000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		sli.clickOnSlidesShowSave();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		sli.enterSlideShowName("File11");
		sli.clickOnSlideSelectFile();//sliFilName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+"Plant-Test@Article@.pdf");
		try {
			Thread.sleep(50000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		sli.clickOnSlidesShowSave();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		sli.enterSlideShowName("File12");
		sli.clickOnSlideSelectFile();//sliFilName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+"Story_The_Necklace$pdf$.pdf");
		try {
			Thread.sleep(70000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		sli.clickOnSlidesShowSave();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		/*
		sli.enterSlideShowName("File13");
		sli.clickOnSlideSelectFile();//sliFilName);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+"test_0.pdf");
		try {
			Thread.sleep(300000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		sli.clickOnSlidesShowSave();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		
		String actRes = sli.fetchSlidesAddMsg();
		System.out.println(actRes);
		
		//toolD.logout();
		
		ExcelUtils.setCellData(actRes, i, 8, Constant.File_TestData_Slides);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 9,Constant.File_TestData_Slides);
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 9,Constant.File_TestData_Slides);
		}
		
		
		}
		
		}

}
