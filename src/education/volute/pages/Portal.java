package education.volute.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import education.volute.common_lib.SuperTestScript;

public class Portal extends NavigatorTool{

	/*@FindBy(id = "Help")
	private WebElement portalHelp;
	private By portalHelpBy = By.id("Help");
	
	@FindBy(id = "Share")
	private WebElement portalShare;
	private By portalShareBy = By.id("Share");*/
	
	@FindBy(css = "paper-fab[title='Manage Portal']")
	private WebElement managePortal;
	private By managePortalBy = By.cssSelector("paper-fab[title='Manage Portal']");
	
	@FindBy(id = "Full Screen")
	private WebElement portalFullScreen;
	private By portalFullScreenBy = By.id("Full Screen");
	
	@FindBy(css = "paper-toolbar[id='paperToolbar'] > div > div[title='Portal']")
	private WebElement portalHeader;
	private By portalHeaderBy = By.cssSelector("paper-toolbar[id='paperToolbar'] > div > div[title='Portal']");
	
	@FindBy(css = "paper-input[label='Display Name']")
	private WebElement portalDisplayName;
	private By portalDisplayNameBy = By.cssSelector("paper-input[label='Display Name']");
	
	@FindBy(css = "input[title='Portal URL']")
	private WebElement portalURL;
	private By portalURLBy = By.cssSelector("input[title='Portal URL']");
	
	@FindBy(css = "div[id='tabsContent'] > paper-tab:nth-of-type(1)")
	private WebElement portalContentTab;
	private By portalContentTabBy = By.cssSelector("div[id='tabsContent'] > paper-tab:nth-of-type(1)");
	
	@FindBy(css = "div[id='tabsContent'] > paper-tab:nth-of-type(2)")
	private WebElement portalFeaturesTab;
	private By portalFeaturesTabBy = By.cssSelector("div[id='tabsContent'] > paper-tab:nth-of-type(2)");
	
	@FindBy(css = "paper-dropdown-menu[label='Select Role']")
	private WebElement portalSelectRoleMenu;
	private By portalSelectRoleMenuBy = By.cssSelector("paper-dropdown-menu[label='Select Role']");
	
	@FindBy(css = "paper-button[class='save-button style-scope iframe-manager x-scope paper-button-0']")
	private WebElement portalSave;
	private By portalSaveBy = By.cssSelector("paper-button[class='save-button style-scope iframe-manager x-scope paper-button-0']");
	
	@FindBy(css = "paper-button[class='cancel-button style-scope iframe-manager x-scope paper-button-0']")
	private WebElement portalCancel;
	private By portalCancelBy = By.cssSelector("paper-button[class='cancel-button style-scope iframe-manager x-scope paper-button-0']");
	
	@FindBy(css = "paper-toast[title='Portal saved successfully.']")
	private WebElement addedMsgPortal;
	private By addedMsgPortalBy = By.cssSelector("paper-toast[title='Portal saved successfully.']");
	
	@FindBy(css = "div[class='done-button style-scope app-management']")
	private WebElement portalDoneButton;
	private By portalDoneButtonBy = By.cssSelector("div[class='done-button style-scope app-management']");
	
	public Portal()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	/*public void clickOnPortalHelp()
	{
		explicitWaitvisibilityOfElementLocated(portalHelpBy);
		portalHelp.click();
	}
	
	public void clickOnPortalShare()
	{
		explicitWaitvisibilityOfElementLocated(portalShareBy);
		portalShare.click();
	}*/
	
	public void clickOnPortalManage()
	{
		//explicitWaitvisibilityOfElementLocated(managePortalBy);
		//managePortal.click();
		javaScriptExecutorClick(managePortalBy);
	}
	
	public void clickOnPortalFullScreen()
	{
		explicitWaitvisibilityOfElementLocated(portalFullScreenBy);
		portalFullScreen.click();
	}
	
	public String fetchPortalHeader()
	{
		explicitWaitelementToBeClickable(portalHeaderBy);
		String msg = portalHeader.getText();
		return msg;
	}
	
	public void enterPortalDisplayName(String porDName)
	{
		explicitWaitvisibilityOfElementLocated(portalDisplayNameBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(portalDisplayName).click().sendKeys(porDName).perform();
		
	}
	
	public void enterPortalURL(String porURL)
	{
		explicitWaitvisibilityOfElementLocated(portalURLBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(portalURL).click().sendKeys(porURL).perform();
		
	}
	
	public void clickOnPortalContentTab()
	{
		explicitWaitvisibilityOfElementLocated(portalContentTabBy);
		portalContentTab.click();
	}
	
	public void clickOnPortalFeaturesTab()
	{
		explicitWaitvisibilityOfElementLocated(portalFeaturesTabBy);
		portalFeaturesTab.click();
	}
	
	public void clickOnPortalSelectRoleMenu()
	{
		explicitWaitvisibilityOfElementLocated(portalSelectRoleMenuBy);
		portalSelectRoleMenu.click();
	}
	
	public void selectPortalRole(String porRoleNum)
	{
		WebElement por = SuperTestScript.driver.findElement(By.cssSelector("paper-listbox > paper-item:nth-of-type("+porRoleNum+")"));
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-listbox > paper-item:nth-of-type("+porRoleNum+")"));
		por.click();
		
	}
	
	public void clickOnPortalSave()
	{
		explicitWaitvisibilityOfElementLocated(portalSaveBy);
		portalSave.click();
	}
	
	public void clickOnPortalCancel()
	{
		explicitWaitvisibilityOfElementLocated(portalCancelBy);
		portalCancel.click();
	}
	
	public String fetchPortalAddMsg()
	{	
		explicitWaitvisibilityOfElementLocated(addedMsgPortalBy);
		String msg = addedMsgPortal.getText();
		return msg;
	}
	
	public void clickOnPortalDone()
	{
		explicitWaitvisibilityOfElementLocated(portalDoneButtonBy);
		portalDoneButton.click();
	}
	
	//div[title='There is no portal content']
	
	public String fetchFacesPortalShows(String porStr)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("iframe[title='Portal iFrame'][src='"+porStr+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
}
