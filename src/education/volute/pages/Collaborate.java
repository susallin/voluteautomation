package education.volute.pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import education.volute.common_lib.SuperTestScript;

public class Collaborate extends NavigatorTool

{
	
	@FindBy(id = "groupsView")
	private WebElement groupsTab;
	private By groupsTabBy = By.id("groupsView");
	
	@FindBy(id = "addGroup")
	private WebElement addNewGroupButton;
	private By addNewGroupButtonBy = By.id("addGroup");
	
	/*@FindBy(css = "paper-icon-item[title='Volute Group'] > paper-icon-button[id='editGroup']")
	private WebElement editGroupButton;
	private By editGroupButtonBy = By.cssSelector("editGroup");
	
	@FindBy(css = "paper-icon-item[title='Volute Group'] > paper-icon-button[id='deleteGroup']")
	private WebElement deleteGroupButton;
	private By deleteGroupButtonBy = By.cssSelector("paper-icon-item[title='Volute Group'] > paper-icon-button[id='deleteGroup']");
	*/
	@FindBy(id = "groupName")
	private WebElement groupNameTextBox;
	private By groupNameTextBoxBy = By.id("groupName");
	
	@FindBy(id = "groupDescription")
	private WebElement groupDescriptionTextBox;
	private By groupDescriptionTextBoxBy = By.id("groupDescription");
	
	@FindBy(css = "paper-toast[title = 'Group created successfully']>span[id='label'][class='style-scope paper-toast']")
	private WebElement grpSavedMsg;
	private By grpSavedMsgBy = By.cssSelector("paper-toast[title = 'Group created successfully']>span[id='label'][class='style-scope paper-toast']");
	
	@FindBy(css = "paper-toast[title = 'Group deleted successfully']>span[id='label'][class='style-scope paper-toast']")
	private WebElement grpDeletedMsg;
	private By grpDeletedMsgBy = By.cssSelector("paper-toast[title = 'Group deleted successfully']>span[id='label'][class='style-scope paper-toast']");
	
	@FindBy(css = "paper-toast[title = 'Updated Group successfully']>span[id='label'][class='style-scope paper-toast']")
	private WebElement grpUpdatedMsg;
	private By grpUpdatedMsgBy = By.cssSelector("paper-toast[title = 'Updated Group successfully']>span[id='label'][class='style-scope paper-toast']");
	/* Searches for Users.
	@FindBy(css = "paper-input[label='Search Users']")
	private WebElement searchUsersTextBox;
	private By searchUsersTextBoxBy = By.cssSelector("paper-input[label='Search Users']");
	*/
	
	/* Selecting from a Specific role of users
	@FindBy(css = "paper-dropdown-menu[label='Filter by roles']")
	private WebElement filterByRolesButton;
	private By filterByRolesButtonBy = By.cssSelector("paper-dropdown-menu[label='Filter by roles']");
	
	@FindBy(css = "paper-listbox[attr-for-selected='uuid'] > paper-item:nth-of-type(1)")
	private WebElement selectRoleButton;
	private By selectRoleButtonBy = By.cssSelector("paper-listbox[attr-for-selected='uuid'] > paper-item:nth-of-type(1)");
	*/
	
	/* Cancels the creation of group
	@FindBy(id = "cancelAddGroup")
	private WebElement selectCancelGroupButton;
	private By selectCancelGroupButtonBy = By.id("cancelAddGroup");
	*/
	
	
	@FindBy(id = "saveAddGroup")
	private WebElement saveGroupButton;
	private By saveGroupButtonBy = By.id("saveAddGroup");
	
	@FindBy(id = "cancelAddGroup")
	private WebElement cancelGroupButton;
	private By cancelGroupButtonBy = By.id("cancelAddGroup");
	/*----------------------------------------------------------ROOMS------------------------------------------------------------------------------*/
	
	@FindBy(id = "roomsView")
	private WebElement roomsTab;
	private By roomsTabBy = By.id("roomsView");
	
	@FindBy(id = "addRoom")
	private WebElement addNewRoomButton;
	private By addNewRoomButtonBy = By.id("addRoom");
	
	/*@FindBy(id = "editRoom")
	private WebElement editRoomButton;
	private By editRoomButtonBy = By.id("editRoom");
	
	@FindBy(id = "deleteRoom")
	private WebElement deleteRoomButton;
	private By deleteRoomButtonBy = By.id("deleteRoom");*/
	
	@FindBy(id = "roomName")
	private WebElement roomNameTextBox;
	private By roomNameTextBoxBy = By.id("roomName");
	
	@FindBy(id = "roomDescription")
	private WebElement roomDescriptionTextBox;
	private By roomDescriptionTextBoxBy = By.id("roomDescription");
	
	@FindBy(css = "paper-radio-group > paper-radio-button:nth-of-type(1)")
	private WebElement assignGroupsRadioButton;
	private By assignGroupsRadioButtonBy = By.cssSelector("paper-radio-group > paper-radio-button:nth-of-type(1)");
	
	/* Wrote findElements
	@FindBy(css = "paper-listbox[id='groupsInRoomListBox'] > paper-item")
	private WebElement selectGroupsn;
	*/
	private By selectGroupsBy = By.cssSelector("paper-listbox[id='groupsInRoomListBox'] > paper-item");
	
			
	@FindBy(css = "paper-radio-group > paper-radio-button:nth-of-type(2)")
	private WebElement assignIndividualsButton;
	private By assignIndividualsButtonBy = By.cssSelector("paper-radio-group > paper-radio-button:nth-of-type(1)");
	
	/* Cancels the creation of room
	@FindBy(id = "cancelRoomCreation")
	private WebElement selectCancelButton;
	private By selectCancelButtonBy = By.id("cancelRoomCreation");
	*/
	
	@FindBy(id = "saveRoomCreation")
	private WebElement saveRoomButton;
	private By saveRoomButtonBy = By.id("saveRoomCreation");
	
	@FindBy(css = "paper-toast[title = 'Updated Room successfully']>span[id='label'][class='style-scope paper-toast']")
	private WebElement roomUpdatedMsg;
	private By roomUpdatedMsgBy = By.cssSelector("paper-toast[title = 'Updated Room successfully']>span[id='label'][class='style-scope paper-toast']");
	
	@FindBy(css = "paper-toast[title = 'Room created successfully']>span[id='label'][class='style-scope paper-toast']")
	private WebElement roomSavedMsg;
	private By roomSavedMsgBy = By.cssSelector("paper-toast[title = 'Room created successfully']>span[id='label'][class='style-scope paper-toast']");
	
	@FindBy(css = "paper-toast[title = 'Room deleted successfully']>span[id='label'][class='style-scope paper-toast']")
	private WebElement roomDeletedMsg;
	private By roomDeletedMsgBy = By.cssSelector("paper-toast[title = 'Room deleted successfully']>span[id='label'][class='style-scope paper-toast']");
	
	@FindBy(css = "paper-button:nth-of-type(1)[class='style-scope confirmation-component x-scope paper-button-0']")
	private WebElement deleteYesCollab;
	private By deleteYesCollabBy = By.cssSelector("paper-button:nth-of-type(1)[class='style-scope confirmation-component x-scope paper-button-0']");
	
	@FindBy(css = "paper-button:nth-of-type(2)[class='style-scope confirmation-component x-scope paper-button-0']")
	private WebElement deleteNoCollab;
	private By deleteNoCollabBy = By.cssSelector("paper-button:nth-of-type(2)[class='style-scope confirmation-component x-scope paper-button-0']");
	//@FindBy(css = "paper-listbox[id='groupListbox'] > paper-icon-item[id='0'] > paper-icon-button[id='editGroup']")
	//private WebElement editGroup;
	//private By editGroupBy = By.cssSelector("paper-listbox[id='groupListbox'] > paper-icon-item[id='0'] > paper-icon-button[id='editGroup']");
	
	//@FindBy(css = "paper-listbox[id='groupListbox'] > paper-icon-item[id='0'] > paper-icon-button[id='editGroup']")
	//private WebElement editGroup;
	//private By editGroupBy = By.cssSelector("paper-listbox[id='groupListbox'] > paper-icon-item[id='0'] > paper-icon-button[id='editGroup']");
	
	public Collaborate()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	/*------------------------------------------------------------GROUPS METHODS------------------------------------------------------------------------------*/
	
	public void clickOnGroupsTab()
	{
		explicitWaitvisibilityOfElementLocated(groupsTabBy);
		groupsTab.click();
	}
	
	public void clickOnAddGroupButton()
	{
		explicitWaitvisibilityOfElementLocated(addNewGroupButtonBy);
		addNewGroupButton.click();
	}
	
	/*public void clickOnEditGroupButton()
	{
		explicitWaitvisibilityOfElementLocated(editGroupButtonBy);
		editGroupButton.click();
	}
	
	public void clickOnDeleteGroupButton()
	{
		explicitWaitvisibilityOfElementLocated(deleteGroupButtonBy);
		deleteGroupButton.click();
	}*/
	
	public void enterGropName(String grpName)
	{
		explicitWaitvisibilityOfElementLocated(groupNameTextBoxBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(groupNameTextBox).click().sendKeys(grpName).perform();
		
	}
	
	public void enterGropDesc(String grpDesc)
	{
		explicitWaitvisibilityOfElementLocated(groupDescriptionTextBoxBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(groupDescriptionTextBox).click().sendKeys(grpDesc).perform();
		
	}
	
	public void selectMemebrsforGroup()
	{
		
		String membercss = "neon-animated-pages[id='groupPage'] paper-listbox[attr-for-selected='user-id']>paper-item:nth-of-type(1)";
		String membercss2 = "neon-animated-pages[id='groupPage'] paper-listbox[attr-for-selected='user-id']>paper-item:nth-of-type(2)";
		String membercss3 = "neon-animated-pages[id='groupPage'] paper-listbox[attr-for-selected='user-id']>paper-item:nth-of-type(3)";
		String membercss4 = "neon-animated-pages[id='groupPage'] paper-listbox[attr-for-selected='user-id']>paper-item:nth-of-type(4)";
		By member1By = By.cssSelector(membercss);
		explicitWaitvisibilityOfElementLocated(member1By);
		SuperTestScript.driver.findElement(By.cssSelector(membercss)).click();
		SuperTestScript.driver.findElement(By.cssSelector(membercss2)).click();
		SuperTestScript.driver.findElement(By.cssSelector(membercss3)).click();
		SuperTestScript.driver.findElement(By.cssSelector(membercss4)).click();
		
	}
	
	public void clickOnCollaborateGroups(String colGroup)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//div[contains(text( ),'"+ colGroup +"')]"));
		WebElement selectGroup = SuperTestScript.driver.findElement(By.xpath("//div[contains(text( ),'"+ colGroup +"')]"));
    	selectGroup.click();
	}
	
	public void clickOnSaveGroup()
	{
		explicitWaitvisibilityOfElementLocated(saveGroupButtonBy);
		saveGroupButton.click();
	}
	
	public void clickOnEditGroupCollab(String editColl)
	{
		WebElement buttonEdit = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-item[title='"+ editColl +"'] > paper-icon-button[id='editGroup']"));
		buttonEdit.click();
		//WebElement ls = SuperTestScript.driver.findElement(By.cssSelector("div[title='"+lsName+"']"));
		//explicitWaitvisibilityOfElementLocated(By.cssSelector("div[title='"+lsName+"']"));
		//ls.click();
	}
	
	public void clickOnRemoveUserButtonGroup(String userName)
	{
		WebElement buttonRemove = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-button[title='Remove "+userName+"']"));
		buttonRemove.click();
		//WebElement ls = SuperTestScript.driver.findElement(By.cssSelector("div[title='"+lsName+"']"));
		//explicitWaitvisibilityOfElementLocated(By.cssSelector("div[title='"+lsName+"']"));
		//ls.click();
	}
	
	public void clickOnDeleteGroupCollab(String delColl)
	{
		WebElement buttonDel = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-item[title='"+ delColl +"'] > paper-icon-button[id='deleteGroup']"));
		buttonDel.click();
	}
	
	public String fetchGroupDeletedMsg()
	{
		
		explicitWaitvisibilityOfElementLocated(grpDeletedMsgBy);
		String msg = grpDeletedMsg.getText();
		return msg;
	}
	
	public String fetchGroupSuccessMsg()
	{
		
		explicitWaitvisibilityOfElementLocated(grpSavedMsgBy);
		String msg = grpSavedMsg.getText();
		return msg;
	}
	
	/*public String fetchGroupShows(String gropShows)
	{
		WebElement grp = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-item[title='"+gropShows+"']"));
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-icon-item[title='"+gropShows+"']"));
		String msg = grp.getText();
		return msg;
	}*/
	
	public String fetchGroupShows(String gropShows)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("paper-icon-item[title='"+gropShows+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchGroupUpdatedMsg()
	{
		
		explicitWaitvisibilityOfElementLocated(grpUpdatedMsgBy);
		String msg = grpUpdatedMsg.getText();
		return msg;
	}
	
	/*----------------------------------------------------------------ROOM METHODS-----------------------------------------------------------------------------------------------*/
	
	public void clickOnRoomsTab()
	{
		explicitWaitvisibilityOfElementLocated(roomsTabBy);
		roomsTab.click();
	}
	
	public void clickOnAddRoomButton()
	{
		explicitWaitvisibilityOfElementLocated(addNewRoomButtonBy);
		addNewRoomButton.click();
	}
	
	/*public void clickOnEditRoomButton()
	{
		explicitWaitvisibilityOfElementLocated(editRoomButtonBy);
		editRoomButton.click();
	}
	
	public void clickOnDeleteRoomButton()
	{
		explicitWaitvisibilityOfElementLocated(deleteRoomButtonBy);
		deleteRoomButton.click();
	}*/
	
	public void enterRoomName(String roomName)
	{
		explicitWaitvisibilityOfElementLocated(roomNameTextBoxBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(roomNameTextBox).click().sendKeys(roomName).perform();
		
	}
	
	public void enterroomDesc(String roomDesc)
	{
		explicitWaitvisibilityOfElementLocated(roomDescriptionTextBoxBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(roomDescriptionTextBox).click().sendKeys(roomDesc).perform();
		
	}
	
	public void clickOnAssignGroupsRadio()
	{
		explicitWaitvisibilityOfElementLocated(assignGroupsRadioButtonBy);
		assignGroupsRadioButton.click();
	}
	
	public void clickOnAssignIndividualsRadio()
	{
		explicitWaitvisibilityOfElementLocated(assignIndividualsButtonBy);
		assignIndividualsButton.click();
	}
	
	public void selectGroupsforRoom()
	{
		explicitWaitvisibilityOfElementLocated(selectGroupsBy);
		List<WebElement> groups = SuperTestScript.driver.findElements(selectGroupsBy);
		for(int i=0; i<=groups.size()-1; i++)
		{
			groups.get(i).click();
		}
		
		
	}
	
	public void selectIndividualsforRoom()
	{
		
		String member1Xpath = "/html/body/volute-container/iron-pages/volute-app-fullsize/div[2]/div/volute-app/div/div/iron-pages/neon-animatable[1]/volute-app-face/volute-app-group-face1/neon-animated-pages/div[2]/div/neon-animated-pages/volute-app-group-user-select/div/div/div/paper-listbox/paper-item[1]";
		String member2Xpath = "/html/body/volute-container/iron-pages/volute-app-fullsize/div[2]/div/volute-app/div/div/iron-pages/neon-animatable[1]/volute-app-face/volute-app-group-face1/neon-animated-pages/div[2]/div/neon-animated-pages/volute-app-group-user-select/div/div/div/paper-listbox/paper-item[2]";
		By member1By = By.xpath(member1Xpath);
		explicitWaitvisibilityOfElementLocated(member1By);
		SuperTestScript.driver.findElement(By.xpath(member1Xpath)).click();
		SuperTestScript.driver.findElement(By.xpath(member2Xpath)).click();
		
	}
	
	public void clickOnDeleteYesCollab()
	{
		explicitWaitvisibilityOfElementLocated(deleteYesCollabBy);
		deleteYesCollab.click();
	}
	
	public void clickOnDeleteNoCollab()
	{
		explicitWaitvisibilityOfElementLocated(deleteNoCollabBy);
		deleteNoCollab.click();
	}
	
	public void clickOnEditRoomCollab(String editRoom)
	{
		WebElement buttonEdit = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-item[title='"+ editRoom +"'] > paper-icon-button[id='editRoom']"));
		buttonEdit.click();
		//WebElement ls = SuperTestScript.driver.findElement(By.cssSelector("div[title='"+lsName+"']"));
		//explicitWaitvisibilityOfElementLocated(By.cssSelector("div[title='"+lsName+"']"));
		//ls.click();
	}
	
	public void clickOnDeleteRoomCollab(String delRoom)
	{
		WebElement buttonDel = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-item[title='"+ delRoom +"'] > paper-icon-button[id='deleteRoom']"));
		buttonDel.click();
	}
	
	public void clickOnSaveRoom()
	{
		explicitWaitvisibilityOfElementLocated(saveRoomButtonBy);
		saveRoomButton.click();
	}
	
	public String fetchRoomUpdatedMsg()
	{
		
		explicitWaitvisibilityOfElementLocated(roomUpdatedMsgBy);
		String msg = roomUpdatedMsg.getText();
		return msg;
	}
	
	public String fetchRoomSuccessMsg()
	{
		
		explicitWaitvisibilityOfElementLocated(roomSavedMsgBy);
		String msg = roomSavedMsg.getText();
		return msg;
	}
	
	public String fetchRoomDeletedMsg()
	{
		
		explicitWaitvisibilityOfElementLocated(roomDeletedMsgBy);
		String msg = roomDeletedMsg.getText();
		return msg;
	}
}
