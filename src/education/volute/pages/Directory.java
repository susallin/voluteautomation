package education.volute.pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import education.volute.common_lib.SuperTestScript;

public class Directory extends NavigatorTool {
	
	/*@FindBy(id = "Help")
	private WebElement directoryHelp;
	private By directoryHelpBy = By.id("Help");
	
	@FindBy(id = "Share")
	private WebElement directoryShare;
	private By directoryShareBy = By.id("Share");*/
	
	@FindBy(css = "paper-fab[title='Manage Directory']")
	private WebElement manageDirectory;
	private By manageDirectoryBy = By.cssSelector("paper-fab[title='Manage Directory']");
	
	@FindBy(id = "fullscreen")
	private WebElement directoryFullScreen;
	private By directoryFullScreenBy = By.id("fullscreen");
	
	@FindBy(css = "paper-input[label='Search']")
	private WebElement searchDirectory;
	private By searchDirectoryBy = By.cssSelector("paper-input[label='Search']");
	
	/*@FindBy(css = "paper-dropdown-menu[label='Sort By']")
	private WebElement sortDirectory;
	private By sortDirectoryBy = By.cssSelector("paper-dropdown-menu[label='Sort By']");*/
	
	@FindBy(css = "paper-toolbar[id='paperToolbar'] > div > div[title='Directory']")
	private WebElement directoryHeader;
	private By directoryHeaderBy = By.cssSelector("paper-toolbar[id='paperToolbar'] > div > div[title='Directory']");
	
	/*@FindBy(css = "paper-input[label='Display Name']")
	private WebElement directoryDisplayName;
	private By directoryDisplayNameBy = By.cssSelector("paper-dropdown-menu[label='Sort By']");
	*/
	
	/*@FindBy(css = "iron-icon[title='Assign Users']")
	private WebElement directoryAssignUserButton;
	private By directoryAssignUserButtonBy = By.cssSelector("iron-icon[title='Assign Users']");
	*/
	
	/*@FindBy(css = "div[id='tabsContent'] > paper-tab:nth-of-type(1)")
	private WebElement directoryIndividualButton;
	private By directoryIndividualButtonBy = By.cssSelector("div[id='tabsContent'] > paper-tab:nth-of-type(1)");
	
	@FindBy(css = "div[id='tabsContent'] > paper-tab:nth-of-type(2)")
	private WebElement directoryGroupButton;
	private By directoryGroupButtonBy = By.cssSelector("div[id='tabsContent'] > paper-tab:nth-of-type(2)");
	*/
	@FindBy(css = "paper-input[label='Search Users']")
	private WebElement directorySearchIndividual;
	private By directorySearchIndividualBy = By.cssSelector("paper-input[label='Search Users']");
	
	@FindBy(css = "paper-input[label='Search Groups']")
	private WebElement directorySearchGroups;
	private By directorySearchGroupsBy = By.cssSelector("paper-input[label='Search Groups']");
	
	/*@FindBy(css = "paper-button[class='style-scope volute-app-assignment-face1 x-scope paper-button-0']")
	private WebElement directoryAssignIndivGroups;
	private By directoryAssignIndivGroupsBy = By.cssSelector("paper-button[class='style-scope volute-app-assignment-face1 x-scope paper-button-0']");
	
	@FindBy(css = "paper-button[class='save-button style-scope assign-module x-scope paper-button-0']")
	private WebElement directoryCloseAssignPeople;
	private By directoryCloseAssignPeopleBy = By.cssSelector("paper-button[class='save-button style-scope assign-module x-scope paper-button-0']");
	*/
	/*@FindBy(css = "volute-app[root-folder='../volute-app-directory'] > div > div > neon-animated-pages > neon-animatable:nth-of-type(2) > volute-app-face > app-management > div > div:nth-of-type(1) > div[class='done-button style-scope app-management'] > iron-icon")
	private WebElement directoryDoneButton;
	private By directoryDoneButtonBy = By.cssSelector("volute-app[root-folder='../volute-app-directory'] > div > div > neon-animated-pages > neon-animatable:nth-of-type(2) > volute-app-face > app-management > div > div:nth-of-type(1) > div[class='done-button style-scope app-management'] > iron-icon");
	*/
	@FindBy(css = "div[id='tabsContent'] > paper-tab:nth-of-type(1)")
	private WebElement directoryContentTab;
	private By directoryContentTabBy = By.cssSelector("div[id='tabsContent'] > paper-tab:nth-of-type(1)");
	
	@FindBy(css = "div[id='tabsContent'] > paper-tab:nth-of-type(2)")
	private WebElement directoryFeaturesTab;
	private By directoryFeaturesTabBy = By.cssSelector("div[id='tabsContent'] > paper-tab:nth-of-type(2)");
	
	@FindBy(css = "paper-dropdown-menu[label='Select Role']")
	private WebElement directorySelectRoleMenu;
	private By directorySelectRoleMenuBy = By.cssSelector("paper-dropdown-menu[label='Select Role']");
	
	@FindBy(css = "paper-toast[title='Users updated successfully']")
	private WebElement addMsgDirectory;
	private By addMsgDirectoryBy = By.cssSelector("paper-toast[title='Users updated successfully']");
	
	@FindBy(css = "paper-toast[title='User removed successfully']")
	private WebElement deleteMsgDirectory;
	private By deleteMsgDirectoryBy = By.cssSelector("paper-toast[title='User removed successfully']");
	
	public Directory()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	/*public void clickOnDirectoryHelp()
	{
		explicitWaitvisibilityOfElementLocated(directoryHelpBy);
		directoryHelp.click();
	}
	
	public void clickOnDirectoryShare()
	{
		explicitWaitvisibilityOfElementLocated(directoryShareBy);
		directoryShare.click();
	}*/
	
	public void clickOnDirectoryManage()
	{
		//explicitWaitvisibilityOfElementLocated(manageDirectoryBy);
		//manageDirectory.click();
		javaScriptExecutorClick(manageDirectoryBy);
	}
	
	public void clickOnDirectoryFullScreen()
	{
		explicitWaitvisibilityOfElementLocated(directoryFullScreenBy);
		directoryFullScreen.click();
	}
	
	public void enterSearchBoxDirectory(String serDUser)
	{
		explicitWaitvisibilityOfElementLocated(searchDirectoryBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(searchDirectory).click().sendKeys(serDUser).perform();
		
	}
	
	/*public void clickOnSortDirectory()
	{
		explicitWaitvisibilityOfElementLocated(sortDirectoryBy);
		sortDirectory.click();
	}*/
	
	public void selectSort(String sortDirNumber)
	{
		WebElement ls = SuperTestScript.driver.findElement(By.cssSelector("paper-item[id='"+sortDirNumber+"']"));
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-item[id='"+sortDirNumber+"']"));
		ls.click();
	}
	
	public String fetchDirectoryHeader()
	{
		explicitWaitelementToBeClickable(directoryHeaderBy);
		String msg = directoryHeader.getText();
		return msg;
	}
	
	/*public void enterDirectoryDisplayName(String dirDName)
	{
		explicitWaitvisibilityOfElementLocated(directoryDisplayNameBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(directoryDisplayName).click().sendKeys(dirDName).perform();
		
	}
	
	public void clickOnAssignUserButtonDirectory()
	{
		explicitWaitvisibilityOfElementLocated(directoryAssignUserButtonBy);
		directoryAssignUserButton.click();
	}
	
	public void clickOnDirectoryIndividualButton()
	{
		explicitWaitvisibilityOfElementLocated(directoryIndividualButtonBy);
		directoryIndividualButton.click();
	}
	
	public void clickOnDirectoryGroupButton()
	{
		explicitWaitvisibilityOfElementLocated(directoryGroupButtonBy);
		directoryGroupButton.click();
	}*/
	
	public void enterDirectorySearchIndividual(String serIndiv)
	{
		explicitWaitvisibilityOfElementLocated(directorySearchIndividualBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(directorySearchIndividual).click().sendKeys(serIndiv).perform();
		
	}
	
	public void enterDirectorySearchGroups(String serGroup)
	{
		explicitWaitvisibilityOfElementLocated(directorySearchGroupsBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(directorySearchGroups).click().sendKeys(serGroup).perform();
		
	}
	
	/*public void clickOnDirectoryAssignUsers()
	{
		explicitWaitvisibilityOfElementLocated(directoryAssignIndivGroupsBy);
		directoryAssignIndivGroups.click();
	}
	
	public void clickOnDirectoryCloseAssignUsers()
	{
		explicitWaitvisibilityOfElementLocated(directoryCloseAssignPeopleBy);
		directoryCloseAssignPeople.click();
	}*/
	
	public void clickOnDirectoryContentTab()
	{
		explicitWaitvisibilityOfElementLocated(directoryContentTabBy);
		directoryContentTab.click();
	}
	
	public void clickOnDirectoryFeaturesTab()
	{
		explicitWaitvisibilityOfElementLocated(directoryFeaturesTabBy);
		directoryFeaturesTab.click();
	}
	
	public void clickOnDirectorySelectRoleMenu()
	{
		explicitWaitvisibilityOfElementLocated(directorySelectRoleMenuBy);
		directorySelectRoleMenu.click();
	}
	
	public void selectDirectoryRole(String dirRoleNum)
	{
		WebElement dir = SuperTestScript.driver.findElement(By.cssSelector("paper-listbox > paper-item:nth-of-type("+dirRoleNum+")"));
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-listbox > paper-item:nth-of-type("+dirRoleNum+")"));
		dir.click();
		
	}
	
	public void selectAllIndividuals()
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("assign-module[id='assignment'] > neon-animated-pages > div:nth-of-type(1) > paper-listbox > paper-icon-item"));//paper-icon-item[class='style-scope assign-module x-scope paper-icon-item-0']
		List<WebElement> allIndivs = SuperTestScript.driver.findElements(By.cssSelector("assign-module[id='assignment'] > neon-animated-pages > div:nth-of-type(1) > paper-listbox > paper-icon-item"));//paper-listbox[id='userListbox'] > paper-icon-item
		
		for(int i=0; i<=allIndivs.size()-1; i++)
		{
			allIndivs.get(i).click();
		}
		
	}
	
	public void selectAllGroups()
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-listbox[id='groupListbox'] > paper-icon-item"));
		List<WebElement> allGroups = SuperTestScript.driver.findElements(By.cssSelector("paper-listbox[id='groupListbox'] > paper-icon-item"));
		
		for(int i=0; i<=allGroups.size()-1; i++)
		{
			allGroups.get(i).click();
		}
		
	}
	
	public String fetchdirUserAdded(String ind){
		WebElement userInd = SuperTestScript.driver.findElement(By.cssSelector("paper-item-body[title='User "+ind+"']"));
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-item-body[title='User "+ind+"']"));
		String msg = userInd.getText();
		return msg;
	}
	
	public void clickOnDirDeleteButton(String dirName)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-icon-button[title='Remove User "+dirName+"']"));
		WebElement selectDir = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-button[title='Remove User "+dirName+"']"));
    	selectDir.click();
	}
	
	public String fetchManageFaceIndShows(String ind)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("paper-item-body[title='User "+ind+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchFaceIndShows(String ind)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("div[title='User "+ind+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchAddMsgDirectory()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(addMsgDirectoryBy);
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchDeleteMsgDirectory()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(deleteMsgDirectoryBy);
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	/*public void clickOnDirectoryDoneButton()
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//paper-button[contains(text( ),'Done')]"));
		WebElement selectDone = SuperTestScript.driver.findElement(By.xpath("//paper-button[contains(text( ),'Done')]"));
    	selectDone.click();
	}*/
	
}
