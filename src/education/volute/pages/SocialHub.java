package education.volute.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


import education.volute.common_lib.SuperTestScript;

public class SocialHub extends NavigatorTool
{
	//@FindBy(xpath = "/html/body/volute-container/neon-animated-pages/volute-app-fullsize/div[2]/div/volute-app/div[1]/div/neon-animated-pages/neon-animatable[1]/volute-app-face/div/journey-face1-module/div/div[1]/div[1]/paper-dropdown-menu/paper-menu-button/div/div/paper-input/paper-input-container/div[1]/iron-icon")
	//private WebElement pgmDropDown;
	//private By pgmDropDownBy = By.xpath("/html/body/volute-container/neon-animated-pages/volute-app-fullsize/div[2]/div/volute-app/div[1]/div/neon-animated-pages/neon-animatable[1]/volute-app-face/div/journey-face1-module/div/div[1]/div[1]/paper-dropdown-menu/paper-menu-button/div/div/paper-input/paper-input-container/div[1]/iron-icon");
	
	@FindBy(id = "clearIcon")
	private WebElement deselectProg;
	private By deselectProgBy = By.id("clearIcon");
			
	@FindBy(id = "programList")
	private WebElement pgmDropDown;
	private By pgmDropDownBy = By.id("programList");
	
	@FindBy(css = "div:nth-of-type(2) > paper-listbox > paper-item:nth-of-type(3)")
	private WebElement SelectAct;
	private By SelectActBy = By.cssSelector("div:nth-of-type(2) > paper-listbox > paper-item:nth-of-type(3)");
	
	@FindBy(css = "paper-listbox > paper-item:nth-of-type(6)[class='sub-activity-paper-item lastVisitedSubfalse style-scope journey-face1-module x-scope paper-item-4']")
	private WebElement SelectSubAct;
	private By SelectSubActBy = By.cssSelector("paper-listbox > paper-item:nth-of-type(6)[class='sub-activity-paper-item lastVisitedSubfalse style-scope journey-face1-module x-scope paper-item-4']");
	
	@FindBy(css = "div[id='groups']")
	private WebElement selectGroups;
	private By selectGroupsBy = By.cssSelector("div[id='groups']");
	
	@FindBy(css = "paper-dialog[id='groupsModal'] > paper-fab[aria-label='close recent groups modal']")
	private WebElement closeGroupModal;
	private By closeGroupModalBy = By.cssSelector("paper-dialog[id='groupsModal'] > paper-fab[aria-label='close recent groups modal']");
	
	/*@FindBy(xpath = "//paper-button[contains(text(),'Launch')]")
	private WebElement launchButton;
	private By launchButtonBy = By.xpath("//paper-button[contains(text(),'Launch')]");*/
	
	@FindBy(css = "paper-button[class='save-button style-scope journey-face1-module x-scope paper-button-0']")
	private WebElement launchButton;
	private By launchButtonBy = By.cssSelector("paper-button[class='save-button style-scope journey-face1-module x-scope paper-button-0']");
	
	@FindBy(css = "paper-icon-button[title='Mashup Navigator Toggle Down']")
	private WebElement navToggleDown;
	private By navToggleDownBy = By.cssSelector("paper-icon-button[title='Mashup Navigator Toggle Down']");
	
	@FindBy(css = "paper-icon-button[title='Mashup Navigator Toggle Up']")
	private WebElement navToggleUp;
	private By navToggleUpBy = By.cssSelector("paper-icon-button[title='Mashup Navigator Toggle Up']");
	
	@FindBy(css = "div[class='module-inner style-scope mashup-navigator']")
	private WebElement lsHeader;
	private By lsHeaderBy = By.cssSelector("div[class='module-inner style-scope mashup-navigator']");
	
	@FindBy(css = "div[title='Group participation will show here. Check back soon!']")
	private WebElement groupWidgetMsg;
	private By groupWidgetMsgBy = By.cssSelector("div[title='Group participation will show here. Check back soon!']");
	
	@FindBy(css = "div[title='There are no events in your active program. Check back soon!']")
	private WebElement eventWidgetMsg;
	private By eventWidgetMsgBy = By.cssSelector("div[title='There are no events in your active program. Check back soon!']");
	
	@FindBy(css = "div[title='There is no current notice.']")
	private WebElement noticeWidgetMsg;
	private By noticeWidgetMsgBy = By.cssSelector("div[title='There is no current notice.']");
	
	@FindBy(css = "div[title='There are no new notifications.']")
	private WebElement notificationWidgetMsg;
	private By notificationWidgetMsgBy = By.cssSelector("div[title='There are no new notifications.']");
	
	@FindBy(css = "div[title='Your active discussions will show here. Check back soon!']")
	private WebElement discWidgetMsg;
	private By discWidgetMsgBy = By.cssSelector("div[title='Your active discussions will show here. Check back soon!']");
	
	@FindBy(css = "//div[@id='welcome'] //div[contains(text( ),'Welcome Mr. Hubs!')]")
	private WebElement myJourneyWidgetMsg;
	private By myJourneyWidgetMsgBy = By.cssSelector("//div[@id='welcome'] //div[contains(text( ),'Welcome Mr. Hubs!')]");
	
	@FindBy(css = "div[id='journeySocialHub'] > div[title='You have no available Learning Spaces.']")
	private WebElement myJourneyWidgetMsg2;
	private By myJourneyWidgetMsg2By = By.cssSelector("div[id='journeySocialHub'] > div[title='You have no available Learning Spaces.']");
	
	@FindBy(css = "div[title='You have not selected a Program.']")
	private WebElement myJourneyWidgetMsg3;
	private By myJourneyWidgetMsg3By = By.cssSelector("div[title='You have not selected a Program.']");
	
	@FindBy(css = "section> div> journey-face1-module> div:nth-of-type(1)> div > div[title='You have not selected a Program.']")
	private WebElement myJourneyWidgetMsg4;
	private By myJourneyWidgetMsg4By = By.cssSelector("section> div> journey-face1-module> div:nth-of-type(1)> div > div[title='You have not selected a Program.']");
	
	@FindBy(css = "paper-button[id='notices']")
	private WebElement selectStatusTab;
	private By selectStatusTabBy = By.cssSelector("paper-button[id='notices']");
	
	@FindBy(css = "paper-button[id='profile']")
	private WebElement selectProfileTab;
	private By selectProfileTabBy = By.cssSelector("paper-button[id='profile']");
	
	@FindBy(css = "paper-button[id='notifications']")
	private WebElement selectNotificationTab;
	private By selectNotificationTabBy = By.cssSelector("paper-button[id='notifications']");
	
	public SocialHub()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	public void clickOnDeselectProg()
	{
		explicitWaitvisibilityOfElementLocated(deselectProgBy);
		deselectProg.click();
	}
	
	public void clickOnPgmDropDown()
	{
		explicitWaitvisibilityOfElementLocated(pgmDropDownBy);
		pgmDropDown.click();
	}
	
	public void selectPgmFromDropDown(String pgmName)
	{
		/*WebElement pgm = SuperTestScript.driver.findElement(By.xpath("//vaadin-combo-box-item[contains(text(),'"+pgmName+"')]"));
		explicitWaitelementToBeClickable(By.xpath("//vaadin-combo-box-item[contains(text(),'"+pgmName+"')]"));
		pgm.click();*/
		//vaadin-combo-box-item[text()[contains(.,'"+pgmName+"')]]
		javaScriptExecutorNoClick(By.xpath("//vaadin-combo-box-overlay //vaadin-combo-box-item[text()[contains(.,'"+pgmName+"')]]"));
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		javaScriptExecutorClick(By.xpath("//vaadin-combo-box-overlay //vaadin-combo-box-item[text()[contains(.,'"+pgmName+"')]]"));
	}
	
	public void clickOnGroups()
	{
		explicitWaitvisibilityOfElementLocated(selectGroupsBy);
		selectGroups.click();
	}
	
	public void ClickOncloseGroupModal()
	{
		explicitWaitvisibilityOfElementLocated(closeGroupModalBy);
		closeGroupModal.click();
	}
	
	public String fetchProgramDropDown()
	{
		try {
			SuperTestScript.driver.findElement(By.cssSelector("vaadin-combo-box[label='Select:']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchProgramName(String logo, String progName)
	{
		if(logo.equalsIgnoreCase("Logo")){
			SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			try {
				SuperTestScript.driver.findElement(By.cssSelector("div[class^='text-center'] > div[title='"+progName+"']"));
				return "Found";
			}
			catch(NoSuchElementException e){
				return "Not Found";
			}
		}else{
			SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			try {
				SuperTestScript.driver.findElement(By.cssSelector("div[class^='program-name-no-logo'][title='"+progName+"']"));
				return "Found";
			}
			catch(NoSuchElementException e){
				return "Not Found";
			}
		}
	}
	
	public void selectGrpDsc(String grpDscName)
	{
		WebElement grpDsc = SuperTestScript.driver.findElement(By.cssSelector("div[id='"+grpDscName+"']"));
		explicitWaitelementToBeClickable(By.cssSelector("div[id='"+grpDscName+"']"));
		grpDsc.click();
		
	}
	
	public String fetchDiscussionT()
	{
		try {
			SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'Recent Discussions')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchProfileT()
	{
		try {
			SuperTestScript.driver.findElement(By.xpath("//paper-item[text()[contains(.,'PROFILE')]]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchNotificationT()
	{
		try {
			SuperTestScript.driver.findElement(By.xpath("//notification-widget/div[contains(text(),'Notifications')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchActor(String userName)
	{
		try {
			SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'"+userName+"')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchHubLabel(String labelName)
	{
		try {
			SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'"+labelName+"')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnActivity()
	{
		explicitWaitvisibilityOfElementLocated(SelectActBy);
		SelectAct.click();
	}
	
	public void clickOnActivity(String actName)
	{
      	//WebElement act = SuperTestScript.driver.findElement(By.xpath("//paper-item[3]/span/text()['"+ actName +"']"));
     	//explicitWaitelementToBeClickable(By.xpath("//paper-item[3]/span/text()['"+ actName +"']"));
      	//act.click();
		SuperTestScript.driver.findElement(By.xpath("//paper-item[3]/span/text()['"+ actName +"']")).click();  
		
	}
	
	public void clickOnActivityContainingSubs(String actName)
	{
		SuperTestScript.driver.findElement(By.xpath("//b[contains(text(),'"+ actName +"')]")).click();
	}
	
	//b[contains(text(),'Learning Path')]
	
	public void clickOnSubActivity()
	{
		explicitWaitvisibilityOfElementLocated(SelectSubActBy);
		SelectSubAct.click();
	}
	
	public void clickOnSubActivity(String subActName)
	{
      	
      	WebElement subAct = SuperTestScript.driver.findElement(By.xpath("//span[contains(text(),'"+subActName+"')]"));
      	explicitWaitelementToBeClickable(By.xpath("//span[contains(text(),'"+subActName+"')]"));
      	subAct.click();
	}
	
	public void clickOnLaunchActByWidget(String LuAct)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("volute-app[id='UUID1337'] paper-button[title='Launch Activity "+LuAct+"']"));
      	WebElement LuBAct = SuperTestScript.driver.findElement(By.cssSelector("volute-app[id='UUID1337'] paper-button[title='Launch Activity "+LuAct+"']"));
      	LuBAct.click();
	}
	
	public void clickOnShowSubActsByWidget(String showSubActs)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("volute-app[id='UUID1337'] paper-icon-button[title='More SubActivity "+showSubActs+"']"));
      	WebElement showSubBActs = SuperTestScript.driver.findElement(By.cssSelector("volute-app[id='UUID1337'] paper-icon-button[title='More SubActivity "+showSubActs+"']"));
      	showSubBActs.click();
	}
	
	public void clickOnLaunchSubActByWidget(String LuSubAct)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("volute-app[id='UUID1337'] paper-button[title='Launch SubActivity "+LuSubAct+"']"));
      	WebElement LuSubBAct = SuperTestScript.driver.findElement(By.cssSelector("volute-app[id='UUID1337'] paper-button[title='Launch SubActivity "+LuSubAct+"']"));
      	LuSubBAct.click();
	}
	
	/*public void clickOnLaunchAct(String LuAct)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-icon-button[title='Launch Activity "+LuAct+"']"));
      	WebElement LuBAct = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-button[title='Launch Activity "+LuAct+"']"));
      	LuBAct.click();
	}
	
	public void clickOnEditLaunchAct(String EdLuAct)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-icon-button[title='Edit "+EdLuAct+"']"));
  		WebElement LuBAct = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-button[title='Edit "+EdLuAct+"']"));
  		LuBAct.click();
	}*/
	
	public void clickOnLaunchAct(String LuAct, String uRole)
	{
		
		if(uRole.equalsIgnoreCase("Participant")||uRole.equalsIgnoreCase("Observer")){
			explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-icon-button[title='Launch Learning Space "+LuAct+"']"));
      		WebElement LuBAct = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-button[title='Launch Learning Space "+LuAct+"']"));
      		LuBAct.click();
		}else{
			explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-icon-button[title='Edit "+LuAct+"']"));
      		WebElement LuBAct = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-button[title='Edit "+LuAct+"']"));
      		LuBAct.click();
		}
      	/*explicitWaitvisibilityOfElementLocated(By.xpath("title='Launch Activity [["+LuAct+"]]'"));
      	WebElement LuBAct = SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'"+LuAct+"')]/../../paper-button[contains(text(),'Launch')]"));
      	LuBAct.click();*/
		//explicitWaitelementToBeClickable(launchButtonBy);
		//launchButton.click();
	}
	
	public void clickOnShowSubActs(String showSubActs)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//div[text()[contains(.,'"+showSubActs+"')]]/../.. //paper-icon-button[@title='Show Nested Learning Spaces']"));  //"paper-icon-button[title='More SubActivity "+showSubActs+"']"));
      	WebElement showSubBActs = SuperTestScript.driver.findElement(By.xpath("//div[text()[contains(.,'"+showSubActs+"')]]/../.. //paper-icon-button[@title='Show Nested Learning Spaces']"));
      	showSubBActs.click();
		/*explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-icon-item[title='"+showSubActs+"'] > paper-icon-button[title='Show SubActivities']"));  //"paper-icon-button[title='More SubActivity "+showSubActs+"']"));
      	WebElement showSubBActs = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-item[title='"+showSubActs+"'] > paper-icon-button[title='Show SubActivities']"));
      	showSubBActs.click();*/
	}
	
	/*public void clickOnLaunchSubAct(String LuSubAct)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-icon-button[title='Launch SubActivity "+LuSubAct+"']"));
      	WebElement LuSubBAct = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-button[title='Launch SubActivity "+LuSubAct+"']"));
      	LuSubBAct.click();
	}
	
	public void clickOnEditLaunchSubAct(String EdLuSubAct)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-icon-button[title='Edit SubActivity "+EdLuSubAct+"']"));
  		WebElement LuSubBAct = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-button[title='Edit SubActivity "+EdLuSubAct+"']"));
  		LuSubBAct.click();
	}*/
	
	public void clickOnLaunchSubAct(String LuSubAct, String uRole)
	{
		if(uRole.equalsIgnoreCase("Participant")||uRole.equalsIgnoreCase("Observer")){
			explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-icon-button[title='Launch Learning Space "+LuSubAct+"']"));
      		WebElement LuSubBAct = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-button[title='Launch Learning Space "+LuSubAct+"']"));
      		LuSubBAct.click();
		}else{
			explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-icon-button[title='Edit "+LuSubAct+"']"));
      		WebElement LuSubBAct = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-button[title='Edit "+LuSubAct+"']"));
      		LuSubBAct.click();
		}
	}
	
	public String fetchJourneyActSub(String title)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//paper-icon-button[@title='Launch Learning Space "+title+"']/../../paper-icon-item[@class='parent-activity lastVisitedtrue style-scope activity-item x-scope paper-icon-item-1']"));
		//javaScriptExecutorNoClick(By.cssSelector("div[title='"+title+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnNavToggleDown()
	{
		explicitWaitvisibilityOfElementLocated(navToggleDownBy);
		navToggleDown.click();
	}
	
	public void clickOnNavToggleUp()
	{
		explicitWaitvisibilityOfElementLocated(navToggleUpBy);
		navToggleUp.click();
	}
	
	public String fetchLSHeader()
	{
		explicitWaitelementToBeClickable(lsHeaderBy);
		String msg = lsHeader.getText();
		return msg;
	}
	
	/*public String fetchSocialShows(String socShows)
	{
		try {
			SuperTestScript.driver.findElement(By.cssSelector("paper-icon-item[title='"+gropShows+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}*/
	
	
      	
	public String fetchGroupWidget()
	{
		explicitWaitelementToBeClickable(groupWidgetMsgBy);
		String msg = groupWidgetMsg.getText();
		return msg;
	}
	
	public String fetchEventWidget()
	{
		explicitWaitelementToBeClickable(eventWidgetMsgBy);
		String msg = eventWidgetMsg.getText();
		return msg;
	}
	
	public String fetchGroupWidgetName(String groupName)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("div[title='Group Name "+groupName+"']"));
      	WebElement groupBName = SuperTestScript.driver.findElement(By.cssSelector("div[title='Group Name "+groupName+"']"));
      	String msg = groupBName.getText();
      	return msg;
	}

	public boolean fetchGroupWidgetActorName(String actorName)
	{
      	boolean isDisplayed = SuperTestScript.driver.findElement(By.cssSelector("iron-image[title='"+actorName+"']")).isDisplayed();
		
		if(isDisplayed){
			return true;
		} else {
			return false;
		}
	}
	
	public String fetchDiscussionWidget()
	{
		explicitWaitelementToBeClickable(discWidgetMsgBy);
		String msg = discWidgetMsg.getText();
		return msg;
	}
	
	public String fetchDiscussionWidgetName(String discName)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("div[title='Discussion Name "+discName+"']"));
      	WebElement discBName = SuperTestScript.driver.findElement(By.cssSelector("div[title='Discussion Name "+discName+"']"));
      	String msg = discBName.getText();
      	return msg;
	}
	
	public String fetchDiscussionWidgetActName(String discActName)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//div[contains(text(),'"+discActName+"')]"));
      	WebElement discActBName = SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'"+discActName+"')]"));
      	String msg = discActBName.getText();
      	return msg;
	}
	
	public String fetchProWidgetName(String proName)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("div[title='"+proName+"']"));
      	WebElement proBName = SuperTestScript.driver.findElement(By.cssSelector("div[title='"+proName+"']"));
      	String msg = proBName.getText();
      	return msg;
	}
	
	public String fetchProWidgetRole(String proRole)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("div[title='"+proRole+"']"));
      	WebElement proBRole = SuperTestScript.driver.findElement(By.cssSelector("div[title='"+proRole+"']"));
      	String msg = proBRole.getText();
      	return msg;
	}
	
	public String fetchMyJourneyWelcomeMsg(String user)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//div[@id='welcome'] //div[contains(text( ),'Welcome "+user+"!')]"));
      	WebElement socWelcome = SuperTestScript.driver.findElement(By.xpath("//div[@id='welcome'] //div[contains(text( ),'Welcome "+user+"!')]"));
      	String msg = socWelcome.getText();
      	return msg;
		
		/*explicitWaitvisibilityOfElementLocated(myJourneyWidgetMsgBy);
		String msg = myJourneyWidgetMsg.getText();
		return msg;*/
	}
	
	public String fetchVisitedActSub(String actSub)
	{
		try {
			SuperTestScript.driver.findElement(By.xpath("//social-hub //div[contains(text( ),'"+actSub+"')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchMyJourneyWidgetMsg2()
	{
		explicitWaitelementToBeClickable(myJourneyWidgetMsg2By);
		String msg = myJourneyWidgetMsg2.getText();
		return msg;
	}
	
	public String fetchMyJourneyWidgetMsg3()
	{
		explicitWaitelementToBeClickable(myJourneyWidgetMsg3By);
		String msg = myJourneyWidgetMsg3.getText();
		return msg;
	}
	
	public String fetchMyJourneyWidgetMsg4()
	{
		explicitWaitelementToBeClickable(myJourneyWidgetMsg4By);
		String msg = myJourneyWidgetMsg4.getText();
		return msg;
	}
	
	public String fetchNoticeWidget()
	{
		explicitWaitelementToBeClickable(noticeWidgetMsgBy);
		String msg = noticeWidgetMsg.getText();
		return msg;
	}
	
	public String fetchNotificationWidget()
	{
		explicitWaitelementToBeClickable(notificationWidgetMsgBy);
		String msg = notificationWidgetMsg.getText();
		return msg;
	}
	
	public String fetchNoticePage()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//profile-module //paper-icon-button[@icon='icons:settings']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchJourneyList()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//div[@id='activitiesList']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchAlertWidgetName(String alertName)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("div[title='"+alertName+"']"));
      	WebElement alertBName = SuperTestScript.driver.findElement(By.cssSelector("div[title='"+alertName+"']"));
      	String msg = alertBName.getText();
      	return msg;
	}
	
	public String fetchAlertWidgetDate(String alertDate)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("div[title='"+alertDate+"']"));
      	WebElement alertBDate = SuperTestScript.driver.findElement(By.cssSelector("div[title='"+alertDate+"']"));
      	String msg = alertBDate.getText();
      	return msg;
	}
	
	public String fetchAlertWidgetMessage(String alertMessage)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("div[title='"+alertMessage+"']"));
      	WebElement alertBMessage = SuperTestScript.driver.findElement(By.cssSelector("div[title='"+alertMessage+"']"));
      	String msg = alertBMessage.getText();
      	return msg;
	}
	
	public void clickStatusTab()
	{
		explicitWaitvisibilityOfElementLocated(selectStatusTabBy);
		selectStatusTab.click();
	}
	
	public void clickProfileTab()
	{
		explicitWaitvisibilityOfElementLocated(selectProfileTabBy);
		selectProfileTab.click();
	}
	
	public void clickNotificationTab()
	{
		explicitWaitvisibilityOfElementLocated(selectNotificationTabBy);
		selectNotificationTab.click();
	}
}
