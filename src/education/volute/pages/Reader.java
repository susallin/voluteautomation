package education.volute.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import education.volute.common_lib.SuperTestScript;

public class Reader extends NavigatorTool{

	/*@FindBy(id = "Help")
	private WebElement readerHelp;
	private By readerHelpBy = By.id("Help");
	
	@FindBy(id = "Share")
	private WebElement readerShare;
	private By readerShareBy = By.id("Share");*/
	
	@FindBy(css = "paper-fab[id^='Reader'][title='Manage']")
	private WebElement manageReader;
	private By manageReaderBy = By.cssSelector("paper-fab[id^='Reader'][title='Manage']");
	
	@FindBy(css = "paper-toolbar[id='paperToolbar'] > div > div[title='Reader']")
	private WebElement readerHeader;
	private By readerHeaderBy = By.cssSelector("paper-toolbar[id='paperToolbar'] > div > div[title='Reader']");
	
	// We can select a particular pdf using div:nth-of-type("num")
	@FindBy(css = "reader-face1-module > div:nth-of-type(3) > div")
	private WebElement readerSelectPDF;
	private By readerSelectPDFBy = By.cssSelector("reader-face1-module > div:nth-of-type(3) > div");
	
	@FindBy(css = "paper-button[class='save-button style-scope reader-face1-module x-scope paper-button-0']")
	private WebElement readerOpenPDFButton;
	private By readerOpenPDFButtonBy = By.cssSelector("paper-button[class='save-button style-scope reader-face1-module x-scope paper-button-0']");
	
	@FindBy(css = "paper-input[label='Display Name']")
	private WebElement readerDisplayName;
	private By readerDisplayNameBy = By.cssSelector("paper-input[label='Display Name']");
	
	@FindBy(css = "form[id='saveReaderForm'] vaadin-upload[title='Select Reading'] paper-button[id='addFiles']")
	private WebElement readerSelectFileUploadButton;
	private By readerSelectFileUploadButtonBy = By.cssSelector("form[id='saveReaderForm'] vaadin-upload[title='Select Reading'] paper-button[id='addFiles']");
	
	@FindBy(css = "paper-toast[title='Document added successfully']")
	private WebElement addedMsgReader;
	private By addedMsgReaderBy = By.cssSelector("paper-toast[title='Document added successfully']");
	
	@FindBy(css = "paper-input[label='Document Name:'] input")
	private WebElement readerDocName;
	private By readerDocNameBy = By.cssSelector("paper-input[label='Document Name:'] input");
	
	@FindBy(css = "paper-input[label='Document Author:'] input")
	private WebElement readerDocAuthor;
	private By readerDocAuthorBy = By.cssSelector("paper-input[label='Document Author:'] input");
	
	// We can select a particular file using paper-item:nth-of-type("n")[id='volute-app-reader-face1-readerList']
	@FindBy(css = "paper-menu > div > paper-item[id='volute-app-reader-face1-readerList']")
	private WebElement readerUploadedPDF;
	private By readerUploadedPDFBy = By.cssSelector("paper-menu > div > paper-item[id='volute-app-reader-face1-readerList']");
	
	@FindBy(css = "paper-toast[title='Document updated successfully']")
	private WebElement updateMsgReader;
	private By updateMsgReaderBy = By.cssSelector("paper-toast[title='Document updated successfully']");
	
	@FindBy(css = "div[class='face2-module-reader-container style-scope reader-manager'] > paper-listbox > paper-icon-item > iron-icon")
	private WebElement readerDeleteButton;
	private By readerDeleteButtonBy = By.cssSelector("div[class='face2-module-reader-container style-scope reader-manager'] > paper-listbox > paper-icon-item > iron-icon");
	
	@FindBy(css = "paper-button:nth-of-type(1)[class='style-scope confirmation-component x-scope paper-button-0']")
	private WebElement deleteYesReader;
	private By deleteYesReaderBy = By.cssSelector("paper-button:nth-of-type(1)[class='style-scope confirmation-component x-scope paper-button-0']");
	
	@FindBy(css = "paper-button:nth-of-type(2)[class='style-scope confirmation-component x-scope paper-button-0']")
	private WebElement deleteNoReader;
	private By deleteNoReaderBy = By.cssSelector("paper-button:nth-of-type(2)[class='style-scope confirmation-component x-scope paper-button-0']");
	
	@FindBy(css = "paper-toast[title='Document deleted successfully']")
	private WebElement deleteMsgReader;
	private By deleteMsgReaderBy = By.cssSelector("paper-toast[title='Document deleted successfully']");
	
	@FindBy(css = "paper-toast[title='Please populate all required fields']")
	private WebElement missingInfoMsgReader;
	private By missingInfoMsgReaderBy = By.cssSelector("paper-toast[title='Please populate all required fields']");
	
	@FindBy(css = "form[id='saveReaderForm'] paper-button[title='Clear']")
	private WebElement readerClear;
	private By readerClearBy = By.cssSelector("form[id='saveReaderForm'] paper-button[title='Clear']");
	
	@FindBy(css = "form[id='saveReaderForm'] paper-button[title='Save']")
	private WebElement readerSave;
	private By readerSaveBy = By.cssSelector("form[id='saveReaderForm'] paper-button[title='Save']");
	
	@FindBy(css = "div[id='tabsContent'] > paper-tab:nth-of-type(1)")
	private WebElement readerContentTab;
	private By readerContentTabBy = By.cssSelector("div[id='tabsContent'] > paper-tab:nth-of-type(1)");
	
	@FindBy(css = "div[id='tabsContent'] > paper-tab:nth-of-type(2)")
	private WebElement readerFeaturesTab;
	private By readerFeaturesTabBy = By.cssSelector("div[id='tabsContent'] > paper-tab:nth-of-type(2)");
	
	@FindBy(css = "paper-dropdown-menu[label='Select Role']")
	private WebElement readerSelectRoleMenu;
	private By readerSelectRoleMenuBy = By.cssSelector("paper-dropdown-menu[label='Select Role']");
	
	@FindBy(css = "div[class='done-button style-scope app-management']")
	private WebElement readerDoneButton;
	private By readerDoneButtonBy = By.cssSelector("div[class='done-button style-scope app-management']");
	
	@FindBy(css = "paper-fab[id^='launchReader']")
	private WebElement readerLaunchReaderButton;
	private By readerLaunchReaderButtonBy = By.cssSelector("paper-fab[id^='launchReader']");
	
	@FindBy(css = "paper-fab[id^='returnToListReader']")
	private WebElement readerReturnToListButton;
	private By readerReturnToListButtonBy = By.cssSelector("paper-fab[id^='returnToListReader']");
	
	@FindBy(css = "paper-icon-button[id^='returnToView']")
	private WebElement readerReturnToView;
	private By readerReturnToViewBy = By.cssSelector("paper-icon-button[id^='returnToView']");
	
	public Reader()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	/*public void clickOnReaderHelp()
	{
		explicitWaitvisibilityOfElementLocated(readerHelpBy);
		readerHelp.click();
	}
	
	public void clickOnReaderShare()
	{
		explicitWaitvisibilityOfElementLocated(readerShareBy);
		readerShare.click();
	}*/
	
	public void clickOnReaderManage()
	{
		
		//explicitWaitvisibilityOfElementLocated(manageReaderBy);
		//manageReader.click();
		javaScriptExecutorClick(manageReaderBy);
		//javaScriptExecutor(manageReaderBy);
		//manageReader.click();
		//Actions a1 = new Actions(SuperTestScript.driver);
		//a1.moveToElement(SuperTestScript.driver.findElement(manageReaderBy)).click().build().perform();
	}
	
	public String fetchReaderHeader()
	{
		explicitWaitelementToBeClickable(readerHeaderBy);
		String msg = readerHeader.getText();
		return msg;
	}
	
	public void clickOnReaderSelectPDF()
	{
		explicitWaitvisibilityOfElementLocated(readerSelectPDFBy);
		readerSelectPDF.click();
	}
	
	public void clickOnReaderOpenPDFButton()
	{
		explicitWaitvisibilityOfElementLocated(readerOpenPDFButtonBy);
		readerOpenPDFButton.click();
	}
	
	public void enterReaderDisplayName(String redDName)
	{
		explicitWaitvisibilityOfElementLocated(readerDisplayNameBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(readerDisplayName).click().sendKeys(redDName).perform();
		
	}
	
	public void clickOnReaderSelectFileUploadButton(String fileName) throws Exception
	{
		javaScriptExecutorUpload(readerSelectFileUploadButtonBy, fileName);
		//explicitWaitvisibilityOfElementLocated(readerSelectFileUploadButtonBy);
		//readerSelectFileUploadButton.click();
	}
	
	/*
	public void clickOnReaderAddFile(String redFilName)
	{
		explicitWaitvisibilityOfElementLocated(readerSelectFileUploadButtonBy);
		readerSelectFileUploadButton.sendKeys("C:/Users/Volute/Desktop/"+redFilName+"");
	}*/
	
	public String fetchMissingInfoMsgReader()
	{	
		explicitWaitvisibilityOfElementLocated(missingInfoMsgReaderBy);
		String msg = missingInfoMsgReader.getText();
		return msg;
	}
	
	public String fetchReaderAddMsg()
	{	
		explicitWaitvisibilityOfElementLocated(addedMsgReaderBy);
		String msg = addedMsgReader.getText();
		return msg;
	}
	
	public void ClearReaderDocName()
	{
		explicitWaitvisibilityOfElementLocated(readerDocNameBy);
		readerDocName.clear();
	}
	
	public void enterOnReaderDocName(String reDName)
	{
		explicitWaitvisibilityOfElementLocated(readerDocNameBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(readerDocName).click().sendKeys(reDName).perform();
		
	}
	
	public void ClearReaderAuthor()
	{
		explicitWaitvisibilityOfElementLocated(readerDocAuthorBy);
		readerDocAuthor.clear();
	}
	
	public void enterOnReaderAuthor(String reDAuthor)
	{
		explicitWaitvisibilityOfElementLocated(readerDocAuthorBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(readerDocAuthor).click().sendKeys(reDAuthor).perform();
		
	}
	
	public void clickOnReaderUploadedPDF()
	{
		explicitWaitvisibilityOfElementLocated(readerUploadedPDFBy);
		readerUploadedPDF.click();
	}
	
	public void clickOnReaderEditButton(String redName)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//div[contains(text(),'"+redName+"')]/../../paper-icon-button[@icon='icons:create']"));
		WebElement selectVid = SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'"+redName+"')]/../../paper-icon-button[@icon='icons:create']"));
    	selectVid.click();
	}
	
	public String fetchFacesRedShows(String redStr)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("//reader-manager //div[contains(text(),'"+redStr+"')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnReaderUploaded(String redName)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//paper-listbox/paper-icon-item/paper-item-body/div[contains(text( ),'"+ redName +"')]"));
		WebElement selectAct = SuperTestScript.driver.findElement(By.xpath("//paper-listbox/paper-icon-item/paper-item-body/div[contains(text( ),'"+ redName +"')]"));
    	selectAct.click();
	}
	
	public void clickOnReaderDeleteButton()
	{
		explicitWaitvisibilityOfElementLocated(readerDeleteButtonBy);
		readerDeleteButton.click();
	}
	
	public void clickOnReaderDeleteButton(String redName)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//div[contains(text(),'"+redName+"')]/../../paper-icon-button[@icon='icons:delete']"));
		WebElement selectRed = SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'"+redName+"')]/../../paper-icon-button[@icon='icons:delete']"));
    	selectRed.click();
	}
	
	public void clickOnDeleteYesReader()
	{
		explicitWaitvisibilityOfElementLocated(deleteYesReaderBy);
		deleteYesReader.click();
	}
	
	public void clickOnDeleteNoReader()
	{
		explicitWaitvisibilityOfElementLocated(deleteNoReaderBy);
		deleteNoReader.click();
	}
	
	public String fetchReaderUpdatedMsg()
	{	
		explicitWaitvisibilityOfElementLocated(updateMsgReaderBy);
		String msg = updateMsgReader.getText();
		return msg;
	}
	
	public String fetchReaderDeleteMsg()
	{	
		explicitWaitvisibilityOfElementLocated(deleteMsgReaderBy);
		String msg = deleteMsgReader.getText();
		return msg;
	}
	
	public void clickOnReaderClear()
	{
		explicitWaitvisibilityOfElementLocated(readerClearBy);
		readerClear.click();
	}
	
	public void clickOnReaderSave()
	{
		explicitWaitvisibilityOfElementLocated(readerSaveBy);
		readerSave.click();
	}
	
	public void clickOnReaderContentTab()
	{
		explicitWaitvisibilityOfElementLocated(readerContentTabBy);
		readerContentTab.click();
	}
	
	public void clickOnReaderFeaturesTab()
	{
		explicitWaitvisibilityOfElementLocated(readerFeaturesTabBy);
		readerFeaturesTab.click();
	}
	
	public void clickOnReaderSelectRoleMenu()
	{
		explicitWaitvisibilityOfElementLocated(readerSelectRoleMenuBy);
		readerSelectRoleMenu.click();
	}
	
	public void clickOnReaderDoneButton()
	{
		explicitWaitvisibilityOfElementLocated(readerDoneButtonBy);
		readerDoneButton.click();
	}
	
	public String fetchManageFaceName(String redName) // this was change to void
	{
		/*CheckFound check = new CheckFound();
		String str;
		str = check.determineIfFound("xpath", By.xpath("//div[@id='management'] //paper-item-body[text()[contains(.,'"+imgName+"')]]/.. //img[@src]"));
		return str;*/
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//reader-manager //div[contains(text(),'"+redName+"')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchManageFaceInsights(String redName, String insight) // this was change to void
	{
		/*CheckFound check = new CheckFound();
		String str;
		str = check.determineIfFound("xpath", By.xpath("//div[@id='management'] //paper-item-body[text()[contains(.,'"+imgName+"')]]/.. //img[@src]"));
		return str;*/
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//reader-manager //div[contains(text(),'"+redName+"')]/../.. //span[contains(text(),'"+insight+"')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchManageFaceTime(String redName, int numChar) // this was change to void
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//reader-manager //div[contains(text(),'"+redName+"')]/../.. //span[@class='user-insights-date style-scope reader-manager']"));
		WebElement selectRed = SuperTestScript.driver.findElement(By.xpath("//reader-manager //div[contains(text(),'"+redName+"')]/../.. //span[@class='user-insights-date style-scope reader-manager']"));
		String msg = selectRed.getText();
		//System.out.println(msg);
		int msgNumber = msg.length();
		//System.out.println(msgNumber);
		if(msgNumber==numChar){
			return "Found";
		}else{
			return "Not Found";
		}
	}
	
	public String fetchManageFaceImg(String redName) // this was change to void
	{
		/*CheckFound check = new CheckFound();
		String str;
		str = check.determineIfFound("xpath", By.xpath("//div[@id='management'] //paper-item-body[text()[contains(.,'"+imgName+"')]]/.. //img[@src]"));
		return str;*/
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//reader-manager //div[contains(text(),'"+redName+"')]/../.. //img[@src]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnReaderLaunchButton()
	{
		/*explicitWaitvisibilityOfElementLocated(readerLaunchReaderButtonBy);
		readerLaunchReaderButton.click();*/
		javaScriptExecutorClick(readerLaunchReaderButtonBy);
	}
	
	public void clickOnReaderReturnToListButton()
	{
			/*explicitWaitvisibilityOfElementLocated(readerReturnToListButtonBy);
			readerReturnToListButton.click();*/
			javaScriptExecutorClick(readerReturnToListButtonBy);
	}
	
	public void clickOnReaderContent(String redName)
	{
			/*explicitWaitvisibilityOfElementLocated(By.xpath("//div[@id='content'] //div[contains(text(),'"+redName+"')]"));
			WebElement selectRed = SuperTestScript.driver.findElement(By.xpath("//div[@id='content'] //div[contains(text(),'"+redName+"')]"));
			selectRed.click();*/
			javaScriptExecutorClick(By.xpath("//div[@id='content'] //div[contains(text(),'"+redName+"')]"));
	}
	
	public void clickOnReaderReturnToViewButton()
	{
		/*explicitWaitvisibilityOfElementLocated(readerReturnToViewBy);
		readerReturnToView.click();*/
		javaScriptExecutorClick(readerReturnToViewBy);
	}
	
	public String fetchListFaceImg(String redName) // this was change to void
	{
		/*CheckFound check = new CheckFound();
		String str;
		str = check.determineIfFound("xpath", By.xpath("//div[@id='management'] //paper-item-body[text()[contains(.,'"+imgName+"')]]/.. //img[@src]"));
		return str;*/
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//div[@name='list'] //div[contains(text(),'"+redName+"')]/../.. //img[@src]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchListFaceName(String redName) // this was change to void
	{
		/*CheckFound check = new CheckFound();
		String str;
		str = check.determineIfFound("xpath", By.xpath("//div[@id='management'] //paper-item-body[text()[contains(.,'"+imgName+"')]]/.. //img[@src]"));
		return str;*/
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//div[@name='list'] //div[contains(text(),'"+redName+"')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchFaceImg(String redName) // this was change to void
	{
		/*CheckFound check = new CheckFound();
		String str;
		str = check.determineIfFound("xpath", By.xpath("//div[@id='management'] //paper-item-body[text()[contains(.,'"+imgName+"')]]/.. //img[@src]"));
		return str;*/
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//div[@name='view'] //div[contains(text(),'"+redName+"')]/../.. //img[@src]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchFaceName(String redName) // this was change to void
	{
		/*CheckFound check = new CheckFound();
		String str;
		str = check.determineIfFound("xpath", By.xpath("//div[@id='management'] //paper-item-body[text()[contains(.,'"+imgName+"')]]/.. //img[@src]"));
		return str;*/
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//div[@name='view'] //div[contains(text(),'"+redName+"')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchFaceInsights(String redName) // this was change to void
	{
		/*CheckFound check = new CheckFound();
		String str;
		str = check.determineIfFound("xpath", By.xpath("//div[@id='management'] //paper-item-body[text()[contains(.,'"+imgName+"')]]/.. //img[@src]"));
		return str;*/
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//div[@name='view'] //span[contains(text(),'"+redName+"')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchFaceTime(String redName, int numChar) // this was change to void
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//div[@name='view'] //div[contains(text(),'"+redName+"')]/../.. //span[contains(text(),'2')]"));
		WebElement selectRed = SuperTestScript.driver.findElement(By.xpath("//div[@name='view'] //div[contains(text(),'"+redName+"')]/../.. //span[contains(text(),'2')]"));
		String msg = selectRed.getText();
		int msgNumber = msg.length();
		
		if(msgNumber==numChar){
			return "Found";
		}else{
			return "Not Found";
		}
	}
	
	public String fetchFileFullView(String redName) // this was change to void
	{
		/*CheckFound check = new CheckFound();
		String str;
		str = check.determineIfFound("xpath", By.xpath("//div[@id='management'] //paper-item-body[text()[contains(.,'"+imgName+"')]]/.. //img[@src]"));
		return str;*/
		try {
			SuperTestScript.driver.findElement(By.cssSelector("div[id='viewer'] canvas[id='page1']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
}
