package education.volute.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import education.volute.common_lib.SuperTestScript;

public class ManagePrograms extends NavigatorTool

{
	@FindBy(css = "fab-module[title-id='Add Learning Space'] paper-fab[title='Add Learning Space']") //paper-fab[title='Add Program']
	private WebElement addPgmButton;
	private By addPgmButtonBy = By.cssSelector("fab-module[title-id='Add Learning Space'] paper-fab[title='Add Learning Space']");
	
	@FindBy(css = "fab-module[title-search='Search Learning Space'] paper-fab[title='Search Learning Space']")
	private WebElement searchPgmButton;
	private By searchPgmButtonBy = By.cssSelector("fab-module[title-search='Search Learning Space'] paper-fab[title='Search Learning Space']");
	
	@FindBy(css = "fab-module[title-search='Search Learning Space'] paper-input[id='search'] input")
	private WebElement enterSearchPgm;
	private By enterSearchPgmBy = By.cssSelector("fab-module[title-search='Search Learning Space'] paper-input[id='search'] input");
	
	@FindBy(css = "input[title='Learning Space Name']")
	private WebElement pgmNameTextBox;
	private By pgmNameTextBoxBy = By.cssSelector("input[title='Learning Space Name']");
			
	@FindBy(css = "vaadin-date-picker[title='Learning Space Start Date']")
	private WebElement pgmStartDateTextBox;
	private By pgmStartDateTextBoxBy = By.cssSelector("vaadin-date-picker[title='Learning Space Start Date']");
			
	@FindBy(css = "vaadin-date-picker[title='Learning Space End Date']")
	private WebElement pgmEndDateTextBox;
	private By pgmEndDateTextBoxBy = By.cssSelector("vaadin-date-picker[title='Learning Space End Date']");
	
	@FindBy(css = "paper-textarea[title='Learning Space Description'] textarea[id='textarea']")
	private WebElement pgmDescTextBox;
	private By pgmDescTextBoxBy = By.cssSelector("paper-textarea[title='Learning Space Description'] textarea[id='textarea']");
	
	@FindBy(css = "form[id='saveActivityForm'] paper-swatch-picker[title='Learning Space Theme Select']")
	private WebElement pgmThemeSelect;
	private By pgmThemeSelectBy = By.cssSelector("form[id='saveActivityForm'] paper-swatch-picker[title='Learning Space Theme Select']");
	
	@FindBy(css = "vaadin-upload[title='Upload Logo'] paper-button[id='addFiles']")
	private WebElement pgmSelectFile;
	private By pgmSelectFileBy = By.cssSelector("vaadin-upload[title='Upload Logo'] paper-button[id='addFiles']");
	
	@FindBy(css = "program-upsert paper-button[title='Cancel']")
	private WebElement cancelPgmButton;
	private By cancelPgmButtonBy = By.cssSelector("program-upsert paper-button[title='Cancel']");
	
	@FindBy(css = "paper-button[title='Save']")
	private WebElement savePgmButton;
	private By savePgmButtonBy = By.cssSelector("paper-button[title='Save']");
		
	@FindBy(css = "paper-toast[title = 'Learning Space created successfully']")
	private WebElement pgmSavedMsg;
	private By pgmSavedMsgBy = By.cssSelector("paper-toast[title = 'Learning Space created successfully']");


	@FindBy(css = "paper-toast[title = 'Learning Space updated successfully']") //>span[id='label'][class='style-scope paper-toast']
	private WebElement pgmEditsSavedMsg;
	private By pgmEditsSavedMsgBy = By.cssSelector("paper-toast[title = 'Learning Space updated successfully']");
	
	@FindBy(css = "paper-toast[title = 'Learning Space deleted successfully']")
	private WebElement pgmDeletedMsg;
	private By pgmDeleteddMsgBy = By.cssSelector("paper-toast[title = 'Learning Space deleted successfully']");
	
	@FindBy(xpath = "//paper-button[text()='Yes']")
	private WebElement deletePgmYesButton;
	private By deletePgmYesButtonBy = By.xpath("//paper-button[text()='Yes']");
	
	@FindBy(css= "paper-fab[title='Back to Programs List']")
	private WebElement backButtonPgmList;
	private By backButtonPgmListBy = By.cssSelector("paper-fab[title='Back to Programs List']");
	
	public ManagePrograms()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	public void clickOnBackButtonPgmList()
	{
		explicitWaitvisibilityOfElementLocated(backButtonPgmListBy);
		backButtonPgmList.click();
		
	}
	
	public String fetchAddLSBtn()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//String click;
		try {
			SuperTestScript.driver.findElement(addPgmButtonBy);
			return "Found";
			//clickendTour();
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnAddPgmBtn()
	{
		explicitWaitvisibilityOfElementLocated(addPgmButtonBy);
		addPgmButton.click();
		
	}
	
	public void clickOnSearchPgmBtn()
	{
		explicitWaitvisibilityOfElementLocated(searchPgmButtonBy);
		searchPgmButton.click();
		
	}
	
	public void enterSearchPgm(String text)
	{	
		explicitWaitvisibilityOfElementLocated(enterSearchPgmBy);
      	WebElement enterSearch = SuperTestScript.driver.findElement(enterSearchPgmBy);

		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(enterSearch).click().sendKeys(text).perform();
	}
	
	public void enterPgmName(String pgmName)
	{
		explicitWaitvisibilityOfElementLocated(pgmNameTextBoxBy);
		pgmNameTextBox.sendKeys(pgmName);
		
	}
	
	public void clearPgmName()
	{
		explicitWaitvisibilityOfElementLocated(pgmNameTextBoxBy);
		pgmNameTextBox.clear();
	}
	
	public void clickPgmstartDateBox() 
	{
		explicitWaitvisibilityOfElementLocated(pgmStartDateTextBoxBy);
		pgmStartDateTextBox.click();
	}
	
	public void selectPgmStartDateNum(String dateNum)
	{
		/*WebElement pgm = SuperTestScript.driver.findElement(By.cssSelector("vaadin-date-picker[name='startDate'][label='Start Date'][title='Program Start Date'] > iron-dropdown > div > vaadin-date-picker-overlay > div[id='scrollers'] > vaadin-infinite-scroller[id='scroller'] > div > div:nth-of-type(2) > div:nth-of-type(2) > vaadin-month-calendar > div > div[aria-label='"+dateNum+"']"));
		explicitWaitvisibilityOfElementLocated(By.cssSelector("vaadin-date-picker[name='startDate'][label='Start Date'][title='Program Start Date'] > iron-dropdown > div > vaadin-date-picker-overlay > div[id='scrollers'] > vaadin-infinite-scroller[id='scroller'] > div > div:nth-of-type(2) > div:nth-of-type(2) > vaadin-month-calendar > div > div[aria-label='"+dateNum+"']"));//vaadin-date-picker[title='Program Start Date']"+" "+"paper-item[style='"+dateNum+"']"));
		pgm.click();*/
		javaScriptExecutorClick(By.cssSelector("vaadin-date-picker[title='Learning Space Start Date'] div[aria-label='"+ dateNum +"']"));
	}
	
	public void clickPgmEndDateBox()
	{
		explicitWaitvisibilityOfElementLocated(pgmEndDateTextBoxBy);
		pgmEndDateTextBox.click();
	}	
	
	public void selectPgmEndDateNum(String dateNum)
	{
		/*WebElement pgm = SuperTestScript.driver.findElement(By.cssSelector("vaadin-date-picker[title='Program End Date']"+" "+"div[aria-label='"+dateNum+"']"));
		explicitWaitelementToBeClickable(By.cssSelector("vaadin-date-picker[title='Program End Date']"+" "+"paper-item[style='"+dateNum+"']"));
		pgm.click();*/
		javaScriptExecutorClick(By.cssSelector("vaadin-date-picker[title='Learning Space End Date'] div[aria-label='"+ dateNum +"']"));
	}
	
	public void enterPgmDesc(String pgmDesc)
	{
		//explicitWaitvisibilityOfElementLocated(pgmDescTextBoxBy);
		//int desc_size = SuperTestScript.driver.findElements(pgmDescTextBoxBy).size();
		//SuperTestScript.driver.findElements(pgmDescTextBoxBy).get(desc_size-1).sendKeys(pgmDesc);
		
		explicitWaitvisibilityOfElementLocated(pgmDescTextBoxBy);
		pgmDescTextBox.sendKeys(pgmDesc);
	}
	
	public void clearPgmDesc()
	{
		explicitWaitvisibilityOfElementLocated(pgmDescTextBoxBy);
		pgmDescTextBox.clear();
	}
	
	public void clickPgmThemeSelect()
	{
		explicitWaitvisibilityOfElementLocated(pgmThemeSelectBy);
		pgmThemeSelect.click();
		
	}
	
	public void selectPgmColor(String pgmColor)
	{
		WebElement pgm = SuperTestScript.driver.findElement(By.cssSelector("paper-item[style='"+pgmColor+"']"));
		explicitWaitelementToBeClickable(By.cssSelector("paper-item[style='"+pgmColor+"']"));
		//javaScriptExecutor(By.cssSelector("paper-item[style='"+pgmColor+"']"));
		pgm.click();
		
	}
	
	public void clickPgmSelectFile()
	{
		explicitWaitvisibilityOfElementLocated(pgmSelectFileBy);
		pgmSelectFile.click();
		
	}
	
	public void clickOnSavePgm()
	{
		explicitWaitvisibilityOfElementLocated(savePgmButtonBy);
		savePgmButton.click();
	}
	
	public void clickOncancelPgmButton()
	{
		explicitWaitvisibilityOfElementLocated(cancelPgmButtonBy);
		cancelPgmButton.click();
	}
	
	public String fetchProgShows(String pgmShows)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("div[title='Learning Space "+pgmShows+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchSuccessMsg()
	{
		
		explicitWaitvisibilityOfElementLocated(pgmSavedMsgBy);
		String msg = pgmSavedMsg.getText();
		return msg;
	}
	
	public String fetchPgmEditsSavedSuccessMsg()
	{
		
		explicitWaitvisibilityOfElementLocated(pgmEditsSavedMsgBy);
		String msg = pgmEditsSavedMsg.getText();
		return msg;
	}
	
	public String fetchPgmDeletedMsg()
	{
		
		explicitWaitvisibilityOfElementLocated(pgmDeleteddMsgBy);
		String msg = pgmDeletedMsg.getText();
		return msg;
	}
		
	// To add an Activity
	public void clickOnSelectProgramAct(String pgmName)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-icon-item[title='Focus Learning Space "+pgmName+"'] paper-icon-button[title='Add Nested Learning Space']"));
		WebElement selectProgramAct = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-item[title='Focus Learning Space "+pgmName+"'] paper-icon-button[title='Add Nested Learning Space']"));
    	selectProgramAct.click();
	}
	
	public void clickOnselectedprogram(String pgmName)
	{
		/*explicitWaitvisibilityOfElementLocated(By.cssSelector("div[title='Program "+pgmName+"']"));
		WebElement selectPgm = SuperTestScript.driver.findElement(By.cssSelector("div[title='Program "+pgmName+"']"));
    	selectPgm.click();*/
    	explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-icon-item[title='Focus Learning Space "+pgmName+"']"));
		WebElement selectProgramAct = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-item[title='Focus Learning Space "+pgmName+"']"));
    	selectProgramAct.click();
    	
	}
	
	public String fetchExpandIcon(String pgmName)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("paper-icon-item[title='Focus Learning Space "+pgmName+"'] paper-icon-button[alt='Expand More']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchExpandIcon2(String pgmName)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("paper-icon-item[title='Focus Learning Space "+pgmName+"'] paper-icon-button[alt='Expand More'][hidden]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	// Expand/Hide Learning Space
	public void clickOnExpandProgram(String pgmName)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-icon-item[title='Focus Learning Space "+pgmName+"'] paper-icon-button[alt='Expand More']"));
		WebElement selectProgramAct = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-item[title='Focus Learning Space "+pgmName+"'] paper-icon-button[alt='Expand More']"));
	   	selectProgramAct.click();
	}
	
	public void clickOnHideProgram(String pgmName)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-icon-item[title='Focus Learning Space "+pgmName+"'] paper-icon-button[alt='Collapse']"));
		WebElement selectProgramAct = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-item[title='Focus Learning Space "+pgmName+"'] paper-icon-button[alt='Collapse']"));
	   	selectProgramAct.click();
	}
	
	public void clickOnManageMashupicon(String pgmName)
	{
		
		explicitWaitelementToBeClickable(By.xpath("//div[contains(text(),'" + pgmName + "')]/../..//paper-icon-button[@title='View Program Mashups']"));
		WebElement mngMashup = SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'" + pgmName + "')]/../..//paper-icon-button[@title='View Program Mashups']"));
    	mngMashup.click();
	}
	
	public void clickOnEditPgmicon(String pgmName)
	{
		explicitWaitelementToBeClickable(By.cssSelector("paper-icon-button[title='Edit Learning Space "+pgmName+"']"));
		WebElement editPgmIcon = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-button[title='Edit Learning Space "+pgmName+"']"));
    	editPgmIcon.click();
	}

	public void clickOnDeletePgmicon(String pgmName)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-icon-button[title='Delete Learning Space "+pgmName+"']"));
		WebElement deletePgmIcon = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-button[title='Delete Learning Space "+pgmName+"']"));
    	deletePgmIcon.click();
	}

	public void clickOnDeletePgmYesButton()
	{
		explicitWaitvisibilityOfElementLocated(deletePgmYesButtonBy);
		deletePgmYesButton.click();
		
	}
	
	
	
	}
