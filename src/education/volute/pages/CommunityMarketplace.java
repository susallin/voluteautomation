package education.volute.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import education.volute.common_lib.SuperTestScript;

public class CommunityMarketplace extends NavigatorTool {

	@FindBy(css = "div[id='topBar'] > paper-input[label='Search']")
	private WebElement searchCM;
	private By searchCMBy = By.cssSelector("div[id='topBar'] > paper-input[label='Search']");
	
	@FindBy(css = "div[id='topBar'] > div:nth-of-type(1) > paper-checkbox:nth-of-type(1) > div[id='checkboxContainer']")
	private WebElement toolCheckBoxCM;
	private By toolCheckBoxCMBy = By.cssSelector("div[id='topBar'] > div:nth-of-type(1) > paper-checkbox:nth-of-type(1) > div[id='checkboxContainer']");
	
	@FindBy(css = "div[id='topBar'] > div:nth-of-type(1) > paper-checkbox:nth-of-type(2) > div[id='checkboxContainer']")
	private WebElement bundlesCheckBoxCM;
	private By bundlesCheckBoxCMBy = By.cssSelector("div[id='topBar'] > div:nth-of-type(1) > paper-checkbox:nth-of-type(2) > div[id='checkboxContainer']");
	
	@FindBy(css = "div[id='topBar'] > div:nth-of-type(3) > paper-dropdown-menu > paper-menu-button")
	private WebElement sortCM;
	private By sortCMBy = By.cssSelector("div[id='topBar'] > div:nth-of-type(3) > paper-dropdown-menu > paper-menu-button");
	
	@FindBy(css = "paper-button[class='view-guides style-scope tool-module x-scope paper-button-0']")
	private WebElement viewGuidesCM;
	private By viewGuidesCMBy = By.cssSelector("paper-button[class='view-guides style-scope tool-module x-scope paper-button-0']");
	
	public CommunityMarketplace()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	public void enterSearchCM(String srcName)
	{
		explicitWaitvisibilityOfElementLocated(searchCMBy);
		searchCM.sendKeys(srcName);
		
	}
	
	public void clickOnToolCheckBoxCM()
	{
		explicitWaitvisibilityOfElementLocated(toolCheckBoxCMBy);
		toolCheckBoxCM.click();
	}
	
	public void clickOnBundlesCheckBoxCM()
	{
		explicitWaitvisibilityOfElementLocated(bundlesCheckBoxCMBy);
		bundlesCheckBoxCM.click();
	}
	
	public void clickOnSortCM()
	{
		explicitWaitvisibilityOfElementLocated(sortCMBy);
		sortCM.click();
	}
	
	public void clickOnViewGuidesCM()
	{
		explicitWaitvisibilityOfElementLocated(viewGuidesCMBy);
		viewGuidesCM.click();
	}
}
