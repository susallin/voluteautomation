package education.volute.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import education.volute.common_lib.SuperTestScript;

public class IdeaBoard extends NavigatorTool {

	@FindBy(css = "paper-fab[aria-label='Add Idea'][title='Add Idea']")
	private WebElement addIdeaButton;
	private By addIdeaButtonBy = By.cssSelector("paper-fab[aria-label='Add Idea'][title='Add Idea']");
	
	@FindBy(css = "paper-button[aria-label='my ideas tab button']")
	private WebElement ideaTapButton;
	private By ideaTapButtonBy = By.cssSelector("paper-button[aria-label='my ideas tab button']");
	
	@FindBy(css = "paper-button[aria-label='my stats tab button']")
	private WebElement statTapButton;
	private By statTapButtonBy = By.cssSelector("paper-button[aria-label='my stats tab button']");
	
	@FindBy(css = "paper-toast[title='Rating must be at lease one star.']")
	private WebElement emptyStarErrorMsg;
	private By emptyStarErrorMsgBy = By.cssSelector("paper-toast[title='Rating must be at lease one star.']");
	
	@FindBy(css = "paper-toast[title='Rated successfully']")
	private WebElement ratedConfirmationMsg;
	private By ratedConfirmationMsgBy = By.cssSelector("paper-toast[title='Rated successfully']");
	
	@FindBy(css = "paper-button[aria-label='Add comment to post']")
	private WebElement addCommentButton;
	private By addCommentButtonBy = By.cssSelector("paper-button[aria-label='Add comment to post']");
	
	@FindBy(css = "form[id='commentUpsertForm'] textarea")
	private WebElement enterComment;
	private By enterCommentBy = By.cssSelector("form[id='commentUpsertForm'] textarea");
	
	@FindBy(css = "paper-icon-button[aria-label='Submit comment']")
	private WebElement submitCommentButton;
	private By submitCommentButtonBy = By.cssSelector("paper-icon-button[aria-label='Submit comment']");
	
	@FindBy(css = "paper-toast[title='Comment should be at least 2 characters.']")
	private WebElement emptyCommentErrorMsg;
	private By empyCommentErrorMsgBy = By.cssSelector("paper-toast[title='Comment should be at least 2 characters.']");
	
	@FindBy(css = "paper-toast[title='Commented successfully']")
	private WebElement commentConfirmationMsg;
	private By commentConfirmationMsgBy = By.cssSelector("paper-toast[title='Commented successfully']");
	
	@FindBy(css = "paper-toast[title='Comment updated successfully']")
	private WebElement commentUpdatedMsg;
	private By commentUpdatedMsgBy = By.cssSelector("paper-toast[title='Comment updated successfully']");
	
	@FindBy(css = "paper-toast[title='Comment deleted successfully']")
	private WebElement commentDeletedMsg;
	private By commentDeletedMsgBy = By.cssSelector("paper-toast[title='Comment deleted successfully']");
	
	@FindBy(css = "paper-input[id='ideaTitle'][aria-label='Enter idea title'] input")
	private WebElement idTitleName;
	private By idTitleNameBy = By.cssSelector("paper-input[id='ideaTitle'][aria-label='Enter idea title'] input");
	
	@FindBy(css = "paper-textarea[id='ideaContent'] textarea")
	private WebElement idContentName;
	private By idContentNameBy = By.cssSelector("paper-textarea[id='ideaContent'] textarea");
	
	@FindBy(css = "form[id='postUpsertForm'] paper-button[id='addFiles']")
	private WebElement selectImageButton;
	private By selectImageButtonBy = By.cssSelector("form[id='postUpsertForm'] paper-button[id='addFiles']");
	
	@FindBy(css = "post-upsert paper-button[aria-label='Cancel button']")
	private WebElement cancelIdeaButton;
	private By cancelIdeaButtonBy = By.cssSelector("post-upsert paper-button[aria-label='Cancel button']");
	
	@FindBy(css = "post-upsert paper-button[aria-label='Save idea button']")
	private WebElement saveIdeaButton;
	private By saveIdeaButtonBy = By.cssSelector("post-upsert paper-button[aria-label='Save idea button']");
	
	@FindBy(css = "vaadin-upload-file paper-icon-button[file-event='file-abort']")
	private WebElement deleteFileButton;
	private By deleteFileButtonBy = By.cssSelector("vaadin-upload-file paper-icon-button[file-event='file-abort']");
			
	@FindBy(css = "paper-toast[title='Idea title must be at least 2 characters.']")
	private WebElement ideaTitleErrorMsg;
	private By ideaTitleErrorMsgBy = By.cssSelector("paper-toast[title='Idea title must be at least 2 characters.']");
	
	@FindBy(css = "paper-toast[title='Idea content must be at least 2 characters.']")
	private WebElement ideaContentErrorMsg;
	private By ideaContentErrorMsgBy = By.cssSelector("paper-toast[title='Idea content must be at least 2 characters.']");
	
	@FindBy(css = "paper-toast[title='File is too large or file type is not supported.']")
	private WebElement ideaFileErrorMsg;
	private By ideaFileErrorMsgBy = By.cssSelector("paper-toast[title='File is too large or file type is not supported.']");
	
	@FindBy(css = "paper-toast[title='Idea created successfully']")
	private WebElement ideaCreatedMsg;
	private By ideaCreatedMsgBy = By.cssSelector("paper-toast[title='Idea created successfully']");
	
	@FindBy(css = "paper-toast[title='Idea updated successfully']")
	private WebElement ideaUpdatedMsg;
	private By ideaUpdatedMsgBy = By.cssSelector("paper-toast[title='Idea updated successfully']");
	
	@FindBy(css = "paper-toast[title='Idea deleted successfully']")
	private WebElement ideaDeletedMsg;
	private By ideaDeletedMsgBy = By.cssSelector("paper-toast[title='Idea deleted successfully']");
	
	public IdeaBoard()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	public void clickAddIdeaButton()
	{
		//explicitWaitvisibilityOfElementLocated(addIdeaButtonBy);
		//addIdeaButton.click();
		javaScriptExecutorClick(addIdeaButtonBy);
	}
	
	public void clickOnEditIdeaButton()
	{
		javaScriptExecutorClick(By.cssSelector("forum-controller paper-icon-button[aria-label='Edit Button']"));
		/*explicitWaitvisibilityOfElementLocated(By.cssSelector("forum-controller paper-icon-button[aria-label='Edit Button']"));
		WebElement selectEdit = SuperTestScript.driver.findElement(By.cssSelector("forum-controller paper-icon-button[aria-label='Edit Button']"));
    	selectEdit.click();*/
	}
	
	public void clickOnDeleteIdeaButton()
	{
		javaScriptExecutorClick(By.cssSelector("forum-controller paper-icon-button[aria-label='Delete Button']"));
		/*explicitWaitvisibilityOfElementLocated(By.cssSelector("forum-controller paper-icon-button[aria-label='Delete Button']"));
		WebElement selectDelete = SuperTestScript.driver.findElement(By.cssSelector("forum-controller paper-icon-button[aria-label='Delete Button']"));
    	selectDelete.click();*/
	}
	
	public void enterIdeaTitle(String idTName)
	{
		javaScriptExecutorNoClick(idContentNameBy);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		explicitWaitvisibilityOfElementLocated(idTitleNameBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(idTitleName).click().sendKeys(idTName).perform();
		//javaScriptExecutorWrite(idTitleNameBy, idTName);
	}
	
	public void clearIdeaTitle()
	{
		javaScriptExecutorNoClick(idContentNameBy);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		explicitWaitvisibilityOfElementLocated(idTitleNameBy);
		idTitleName.clear();	
	}
	
	public void enterIdeaContent(String content)
	{
		int desc_size = SuperTestScript.driver.findElements(idContentNameBy).size();
		SuperTestScript.driver.findElements(idContentNameBy).get(desc_size-1).sendKeys(content);
	}
	
	public void clearIdeaContent()
	{
		explicitWaitvisibilityOfElementLocated(idContentNameBy);
		idContentName.clear();	
	}
	
	public void clickOnSelectImageButton(String fileName) throws Exception 
	{
		javaScriptExecutorNoClick(saveIdeaButtonBy);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		javaScriptExecutorUpload(selectImageButtonBy, fileName);
		//explicitWaitvisibilityOfElementLocated(selectImageButtonBy);
		//selectImageButton.click();
	}
	
	public void clickOnCancelIdeaButton() //throws IOException 
	{
		javaScriptExecutorClick(cancelIdeaButtonBy);
		/*explicitWaitvisibilityOfElementLocated(cancelIdeaButtonBy);
		cancelIdeaButton.click();*/
	}
	
	public void clickOnSaveIdeaButton() //throws IOException 
	{
		javaScriptExecutorClick(saveIdeaButtonBy);
		//explicitWaitvisibilityOfElementLocated(saveIdeaButtonBy);
		//saveIdeaButton.click();
	}
	
	public void clickOnDeleteFileButton() //throws IOException 
	{
		javaScriptExecutorNoClick(saveIdeaButtonBy);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(deleteFileButtonBy);
			deleteFileButton.click();
		}
		catch(NoSuchElementException e){
			// Do Nothing
		}
	}
	
	public void clickIdeaTapButton()
	{
		explicitWaitvisibilityOfElementLocated(ideaTapButtonBy);
		ideaTapButton.click();
	}
	
	public void clickStatTapButton()
	{
		explicitWaitvisibilityOfElementLocated(statTapButtonBy);
		statTapButton.click();
	}
	
	public void clickAddCommentButton()
	{
		javaScriptExecutorClick(addCommentButtonBy);
		//explicitWaitvisibilityOfElementLocated(addCommentButtonBy);
		//addCommentButton.click();
	}
	
	public void clearIdeaComment()
	{
		explicitWaitvisibilityOfElementLocated(enterCommentBy);
		enterComment.clear();	
	}
	
	public void enterComment(String comment)
	{
		int desc_size = SuperTestScript.driver.findElements(enterCommentBy).size();
		SuperTestScript.driver.findElements(enterCommentBy).get(desc_size-1).sendKeys(comment);
	}
	
	public void clickSubmitCommentButton()
	{
		javaScriptExecutorClick(submitCommentButtonBy);
		//explicitWaitvisibilityOfElementLocated(submitCommentButtonBy);
		//submitCommentButton.click();
	}
	
	public void clickOnReplyButton(String commentName)
	{
		javaScriptExecutorClick(By.xpath("//span[text()[contains(.,'"+commentName+"')]]/.. //paper-button[@aria-label='Reply to comment button']"));
		/*explicitWaitvisibilityOfElementLocated(By.xpath("//span[text()[contains(.,'"+commentName+"')]]/.. //paper-button[@aria-label='Reply to comment button']"));
		WebElement selectReply = SuperTestScript.driver.findElement(By.xpath("//span[text()[contains(.,'"+commentName+"')]]/.. //paper-button[@aria-label='Reply to comment button']"));
    	selectReply.click();*/
	}
	
	public void clickOnEditCommentButton(String commentName)
	{
		javaScriptExecutorClick(By.xpath("//span[text()[contains(.,'"+commentName+"')]]/.. //paper-button[@aria-label='Edit your comment']"));
		/*explicitWaitvisibilityOfElementLocated(By.xpath("//span[text()[contains(.,'"+commentName+"')]]/.. //paper-button[@aria-label='Edit your comment']"));
		WebElement selectEdit = SuperTestScript.driver.findElement(By.xpath("//span[text()[contains(.,'"+commentName+"')]]/.. //paper-button[@aria-label='Edit your comment']"));
    	selectEdit.click();*/
	}
	
	public void clickOnDeleteCommentButton(String commentName)
	{
		javaScriptExecutorClick(By.xpath("//span[text()[contains(.,'"+commentName+"')]]/.. //paper-button[@aria-label='Delete your comment']"));
		/*explicitWaitvisibilityOfElementLocated(By.xpath("//span[text()[contains(.,'"+commentName+"')]]/.. //paper-button[@aria-label='Delete your comment']"));
		WebElement selectDelete = SuperTestScript.driver.findElement(By.xpath("//span[text()[contains(.,'"+commentName+"')]]/.. //paper-button[@aria-label='Delete your comment']"));
    	selectDelete.click();*/
	}
	
	public void RateIdea(String ideaNum)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("span[class^='stars']>"+ideaNum+""));
		WebElement selectNum = SuperTestScript.driver.findElement(By.cssSelector("span[class^='stars']>"+ideaNum+""));
    	selectNum.click();
	}
	
	public void ClickSubmitRateIdeaButton()
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("div[class^='rate-section desktop-rating'] paper-button[class^='submit']"));
		WebElement selectSubmit = SuperTestScript.driver.findElement(By.cssSelector("div[class^='rate-section desktop-rating'] paper-button[class^='submit']"));
    	selectSubmit.click();
	}
	
	public void ClickOnSelectedIdeaButton(String ideaName)
	{
		javaScriptExecutorClick(By.xpath("//div[@title='"+ideaName+"']/../..//iron-icon[@icon='icons:lightbulb-outline']"));
		/*explicitWaitvisibilityOfElementLocated(By.cssSelector("//div[@title='"+ideaName+"']/../..//iron-icon[@icon='icons:lightbulb-outline']"));
		WebElement selectIdea = SuperTestScript.driver.findElement(By.cssSelector("//div[@title='"+ideaName+"']/../..//iron-icon[@icon='icons:lightbulb-outline']"));
    	selectIdea.click();*/
	}
	
	public void ClickBackIdeaTapButton()
	{
		javaScriptExecutorClick(By.cssSelector("paper-fab[aria-label='back to idea list button']"));
		/*explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-fab[aria-label='back to idea list button']"));
		WebElement selectBack = SuperTestScript.driver.findElement(By.cssSelector("paper-fab[aria-label='back to idea list button']"));
    	selectBack.click();*/
	}
	
	public String fetchFacesAddedIdeaBulbShows(String ideaName)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//div[@title='"+ideaName+"']/../..//iron-icon[@icon='icons:lightbulb-outline']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchFacesDeletedIdeaBulbShows()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//i[contains(text(),'This idea has been deleted.')]/../..//iron-icon[@icon='icons:lightbulb-outline']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchFacesIdeaCommentShows(String numComment)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("div[title='"+numComment+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchFacesIdeaStarsShows(String numStars)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("div[title='"+numStars+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchErrorMsgStarIdea()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(emptyStarErrorMsgBy);
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchRatedConfirmationMsg()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(ratedConfirmationMsgBy);
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchEmpyCommentErrorMsg()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(empyCommentErrorMsgBy);
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchIdeaTitleErrorMsg()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(ideaTitleErrorMsgBy);
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchIdeaContentErrorMsg()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(ideaContentErrorMsgBy);
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchIdeaFileErrorMsg()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(ideaFileErrorMsgBy);
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchIdeaConfirmationMsg()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(ideaCreatedMsgBy);
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchIdeaUpdatedMsg()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(ideaUpdatedMsgBy);
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchIdeaDeletetedMsg()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(ideaDeletedMsgBy);
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchCommentConfirmationMsg()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(commentConfirmationMsgBy);
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchCommentUpdatedMsg()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(commentUpdatedMsgBy);
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchCommentDeletedMsg()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(commentDeletedMsgBy);
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
}
