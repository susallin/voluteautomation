package education.volute.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import education.volute.common_lib.SuperTestScript;

public class SubMenu extends NavigatorTool {

	@FindBy(css = "sub-menu[id='subMenu'] > div > paper-tabs > div > div > paper-tab:nth-of-type(1)")
	private WebElement subMenuPM;
	private By subMenuPMBy = By.cssSelector("sub-menu[id='subMenu'] > div > paper-tabs > div > div > paper-tab:nth-of-type(1)");

	@FindBy(css = "sub-menu[id='subMenu'] > div > paper-tabs > div > div > paper-tab:nth-of-type(2)")
	private WebElement subMenuCM;
	private By subMenuCMBy = By.cssSelector("sub-menu[id='subMenu'] > div > paper-tabs > div > div > paper-tab:nth-of-type(2)");
	
	@FindBy(css = "sub-menu[id='subMenu'] > div > paper-tabs > div > div > paper-tab:nth-of-type(3)")
	private WebElement subMenuCT;
	private By subMenuCTBy = By.cssSelector("sub-menu[id='subMenu'] > div > paper-tabs > div > div > paper-tab:nth-of-type(3)");
	
	@FindBy(css = "sub-menu[id='subMenu'] > div > paper-tabs > div > div > paper-tab:nth-of-type(1)")
	private WebElement subMenuMP;
	private By subMenuMPBy = By.cssSelector("sub-menu[id='subMenu'] > div > paper-tabs > div > div > paper-tab:nth-of-type(1)");
	
	@FindBy(css = "sub-menu[id='subMenu'] > div > paper-tabs > div > div > paper-tab:nth-of-type(2)")
	private WebElement subMenuMU;
	private By subMenuMUBy = By.cssSelector("sub-menu[id='subMenu'] > div > paper-tabs > div > div > paper-tab:nth-of-type(2)");
	
	@FindBy(css = "sub-menu[id='subMenu'] > div > paper-tabs > div > div > paper-tab:nth-of-type(3)")
	private WebElement subMenuMLS;
	private By subMenuMLSBy = By.cssSelector("sub-menu[id='subMenu'] > div > paper-tabs > div > div > paper-tab:nth-of-type(3)");
	
	@FindBy(css = "paper-icon-button[icon='icons:arrow-back']")
	private WebElement arrowBackPMCM;
	private By arrowBackPMCMBy = By.cssSelector("paper-icon-button[icon='icons:arrow-back']");
	
	@FindBy(css = "app-access-management > div > div > paper-tabs > div > div > paper-tab:nth-of-type(1)")
	private WebElement roleAccessPM;
	private By roleAccessPMBy = By.cssSelector("app-access-management > div > div > paper-tabs > div > div > paper-tab:nth-of-type(1)");
	
	@FindBy(css = "app-access-management > div > div > paper-tabs > div > div > paper-tab:nth-of-type(2)")
	private WebElement featureAccessPM;
	private By featureAccessPMBy = By.cssSelector("app-access-management > div > div > paper-tabs > div > div > paper-tab:nth-of-type(2)");
	
	@FindBy(css = "td:nth-of-type(2)[class='text-center style-scope app-access-management'] > put-delete-check > paper-checkbox")
	private WebElement adminAccessPM;
	private By adminAccessPMBy = By.cssSelector("td:nth-of-type(2)[class='text-center style-scope app-access-management'] > put-delete-check > paper-checkbox");

	@FindBy(css = "td:nth-of-type(3)[class='text-center style-scope app-access-management'] > put-delete-check > paper-checkbox")
	private WebElement facilitatorAccessPM;
	private By facilitatorAccessPMBy = By.cssSelector("td:nth-of-type(3)[class='text-center style-scope app-access-management'] > put-delete-check > paper-checkbox");
	
	@FindBy(css = "td:nth-of-type(4)[class='text-center style-scope app-access-management'] > put-delete-check > paper-checkbox")
	private WebElement participantAccessPM;
	private By participantAccessPMBy = By.cssSelector("td:nth-of-type(4)[class='text-center style-scope app-access-management'] > put-delete-check > paper-checkbox");
	
	@FindBy(css = "td:nth-of-type(5)[class='text-center style-scope app-access-management'] > put-delete-check > paper-checkbox")
	private WebElement observerAccessPM;
	private By observerAccessPMBy = By.cssSelector("td:nth-of-type(5)[class='text-center style-scope app-access-management'] > put-delete-check > paper-checkbox");
	
	@FindBy(css = "paper-toast[text='Change success']")
	private WebElement successMsgPM;
	private By successMsgPMBy = By.cssSelector("paper-toast[text='Change success']");
	
	public SubMenu()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	public void ClickOnSubMenuPM()
	{
		explicitWaitvisibilityOfElementLocated(subMenuPMBy);
		subMenuPM.click();
	}
	
	public void ClickOnSubMenuCM()
	{
		explicitWaitvisibilityOfElementLocated(subMenuCMBy);
		subMenuCM.click();
	}
	
	public void ClickOnSubMenuCT()
	{
		explicitWaitvisibilityOfElementLocated(subMenuCTBy);
		subMenuCT.click();
	}
	
	public void ClickOnSubMenuMP()
	{
		explicitWaitvisibilityOfElementLocated(subMenuMPBy);
		subMenuMP.click();
	}
	
	public void ClickOnSubMenuMU()
	{
		explicitWaitvisibilityOfElementLocated(subMenuMUBy);
		subMenuMU.click();
	}
	
	public void ClickOnSubMenuMLS()
	{
		explicitWaitvisibilityOfElementLocated(subMenuMLSBy);
		subMenuMLS.click();
	}
	
	public void ClickOnArrowBackPMCM()
	{
		explicitWaitvisibilityOfElementLocated(arrowBackPMCMBy);
		arrowBackPMCM.click();
	}
	
	public void ClickOnRoleAccessPM()
	{
		explicitWaitvisibilityOfElementLocated(roleAccessPMBy);
		roleAccessPM.click();
	}
	
	public void ClickOnFeatureAccessPM()
	{
		explicitWaitvisibilityOfElementLocated(featureAccessPMBy);
		featureAccessPM.click();
	}
	
	public void ClickOnAdminAccessPM()
	{
		explicitWaitvisibilityOfElementLocated(adminAccessPMBy);
		adminAccessPM.click();
	}
	
	public void ClickOnFacilitatorAccessPM()
	{
		explicitWaitvisibilityOfElementLocated(facilitatorAccessPMBy);
		facilitatorAccessPM.click();
	}
	
	public void ClickOnParticipantAccessPM()
	{
		explicitWaitvisibilityOfElementLocated(participantAccessPMBy);
		participantAccessPM.click();
	}
	
	public void ClickOnObserverAccessPM()
	{
		explicitWaitvisibilityOfElementLocated(observerAccessPMBy);
		observerAccessPM.click();
	}
	
	public String fetchSuccessMsgPM()
	{	
		explicitWaitvisibilityOfElementLocated(successMsgPMBy);
		String msg = successMsgPM.getText();
		return msg;
	}
}
