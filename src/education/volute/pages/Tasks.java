package education.volute.pages;

import java.util.concurrent.TimeUnit;

//import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import education.volute.common_lib.SuperTestScript;

public class Tasks extends NavigatorTool {
	
	/*@FindBy(id = "Help")
	private WebElement tasksHelp;
	private By tasksHelpBy = By.id("Help");
	
	@FindBy(id = "Share")
	private WebElement tasksShare;
	private By tasksShareBy = By.id("Share");*/
	
	@FindBy(css = "paper-fab[title='Manage Task']")
	private WebElement manageTasks;
	private By manageTasksBy = By.cssSelector("paper-fab[title='Manage Task']");
	
	@FindBy(id = "fullscreen")
	private WebElement tasksFullScreen;
	private By tasksFullScreenBy = By.id("fullscreen");
	
	@FindBy(css = "paper-input[label='Display Name']")
	private WebElement tasksDisplayName;
	private By tasksDisplayNameBy = By.cssSelector("paper-input[label='Display Name']");
	
	@FindBy(css = "div[class='done-button style-scope app-management']")
	private WebElement tasksDone;
	private By tasksDoneBy = By.cssSelector("div[class='done-button style-scope app-management']");
	
	@FindBy(css = "paper-fab[id^='addTasks']")
	private WebElement tasksAddBar;
	private By tasksAddBarBy = By.cssSelector("paper-fab[id^='addTasks']");
	
	@FindBy(css = "paper-input[label='Type Task...']")
	private WebElement typeTask;
	private By typeTaskBy = By.cssSelector("paper-input[label='Type Task...']");
	
	/*@FindBy(id = "textarea")
	private WebElement typeEditedTask;
	private By typeEditedTaskBy = By.id("textarea");*/
	
	@FindBy(css = "paper-button[title='Add New Task']")//"paper-button[class*='add-task-paper-button style-scope volute-add-task x-scope paper-button-0']")
	private WebElement tasksAddButton;
	private By tasksAddButtonBy = By.cssSelector("paper-button[title='Add New Task']");
	
	/*@FindBy(css = "li:nth-of-type(2) > volute-task > div > div:nth-of-type(1) > paper-checkbox > div")
	private WebElement checkTask;
	private By checkTaskBy = By.cssSelector("li:nth-of-type(2) > volute-task > div > div:nth-of-type(1) > paper-checkbox > div");
	*/
	
	@FindBy(css = "paper-checkbox[aria-checked='false']")
	private WebElement StrikeTask;
	private By StrikeTaskBy = By.cssSelector("paper-checkbox[aria-checked='false']");
	
	@FindBy(css = "li:nth-of-type(1) > volute-task > div > div > div:nth-of-type(2) > iron-icon")
	private WebElement deleteTaskButton;
	private By deleteTaskButtonBy = By.cssSelector("li:nth-of-type(1) > volute-task > div > div > div:nth-of-type(2) > iron-icon");
	
	@FindBy(css = "paper-button:nth-of-type(1)[class='style-scope confirmation-component x-scope paper-button-0']")
	private WebElement deleteYesTask;
	private By deleteYesTaskBy = By.cssSelector("paper-button:nth-of-type(1)[class='style-scope confirmation-component x-scope paper-button-0']");
	
	@FindBy(css = "paper-button:nth-of-type(1)[class='style-scope confirmation-component x-scope paper-button-0']")
	private WebElement deleteNoTask;
	private By deleteNoTaskBy = By.cssSelector("paper-button:nth-of-type(2)[class='style-scope confirmation-component x-scope paper-button-0']");
	
	@FindBy(css = "paper-toast[title='Task successfully deleted']")
	private WebElement deleteMsgTask;
	private By deleteMsgTaskBy = By.cssSelector("paper-toast[title='Task successfully deleted']");
	
	@FindBy(css = "paper-toast[title='Task successfully saved']")
	private WebElement updatedMsgTask;
	private By updatedMsgTaskBy = By.cssSelector("paper-toast[title='Task successfully saved']");
	
	@FindBy(css = "paper-toast[title='Task successfully added']")
	private WebElement addedMsgTask;
	private By addedMsgTaskBy = By.cssSelector("paper-toast[title='Task successfully added']");
	
	public Tasks()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	/*public void clickOnTasksHelp()
	{
		explicitWaitvisibilityOfElementLocated(tasksHelpBy);
		tasksHelp.click();
	}
	
	public void clickOnTasksShare()
	{
		explicitWaitvisibilityOfElementLocated(tasksShareBy);
		tasksShare.click();
	}*/
	
	public void clickOnTasksManage()
	{
		explicitWaitvisibilityOfElementLocated(manageTasksBy);
		manageTasks.click();
	}
	
	public void clickOnTasksFullScreen()
	{
		explicitWaitvisibilityOfElementLocated(tasksFullScreenBy);
		tasksFullScreen.click();
	}
	
	public void enterTaskDisplayName(String tasDName)
	{
		explicitWaitvisibilityOfElementLocated(tasksDisplayNameBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(tasksDisplayName).click().sendKeys(tasDName).perform();
		
	}
	
	public void clickOnTasksDone()
	{
		explicitWaitvisibilityOfElementLocated(tasksDoneBy);
		tasksDone.click();
	}
	
	public void clickOnTasksAddBar()
	{
		/*explicitWaitvisibilityOfElementLocated(tasksAddBarBy);
		tasksAddBar.click();*/
		javaScriptExecutorClick(tasksAddBarBy);
	}
	
	public void enterTypeTask(String typTask)
	{
		explicitWaitvisibilityOfElementLocated(typeTaskBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(typeTask).click().sendKeys(typTask).perform();
		//javaScriptExecutorWrite(typeTaskBy, typTask);
	}
	
	public void clickOnTasksAddButton()
	{
		//explicitWaitvisibilityOfElementLocated(tasksAddButtonBy);
		//tasksAddButton.click();
		javaScriptExecutorClick(tasksAddButtonBy);
	}
	
	public void clickOnCheckBoxTasks(String TasC)
	{
		//explicitWaitvisibilityOfElementLocated(checkTaskBy);
		//checkTask.click();
		
		/*explicitWaitvisibilityOfElementLocated(By.xpath("//div[contains(text(),'"+TasC+"')]/../../div/paper-checkbox"));
      	WebElement TasCheck = SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'"+TasC+"')]/../../div/paper-checkbox"));
      	TasCheck.click();*/
      	javaScriptExecutorClick(By.xpath("//div[contains(text(),'"+TasC+"')]/../../div/paper-checkbox"));
      	
	}
	
	public void clickOnEditButtonTasks(String TasE)
	{
		//explicitWaitvisibilityOfElementLocated(checkTaskBy);
		//checkTask.click();
		javaScriptExecutor(By.cssSelector("volute-app[id='UUID1395']"));
		
		/*explicitWaitvisibilityOfElementLocated(By.cssSelector("li[title='Task "+TasE+"'] paper-icon-button[icon='icons:create']"));
      	WebElement TasEdit = SuperTestScript.driver.findElement(By.cssSelector("li[title='Task "+TasE+"'] paper-icon-button[icon='icons:create']"));
      	TasEdit.click();*/
      	javaScriptExecutorClick(By.cssSelector("li[title='Task "+TasE+"'] paper-icon-button[icon='icons:create']"));
	}
	
	public void enterEditTask(String oldTask, String newTask)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("li[title='Task "+oldTask+"'] textarea[id='textarea']"));
      	WebElement TypeEdit = SuperTestScript.driver.findElement(By.cssSelector("li[title='Task "+oldTask+"'] textarea[id='textarea']"));
		
		//explicitWaitvisibilityOfElementLocated(typeEditedTaskBy);
      	TypeEdit.clear();
		//li[title='Task second task of result'] textarea[id='textarea']
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(TypeEdit).click().sendKeys(newTask).perform();
		//javaScriptExecutorWrite(By.cssSelector("li[title='Task "+oldTask+"'] textarea[id='textarea']"), newTask);
	}
	
	public void clickOnSaveButtonTasks(String TasS)
	{
		//explicitWaitvisibilityOfElementLocated(checkTaskBy);
		//checkTask.click();		
		/*explicitWaitvisibilityOfElementLocated(By.cssSelector("li[title='Task "+TasS+"'] paper-button"));
      	WebElement TasSave = SuperTestScript.driver.findElement(By.cssSelector("li[title='Task "+TasS+"'] paper-button"));
      	TasSave.click();*/  
      	javaScriptExecutorClick(By.cssSelector("li[title='Task "+TasS+"'] paper-button"));
	}
	
	public void clickOnDeleteButtonTasks(String TasD)
	{
		//explicitWaitvisibilityOfElementLocated(checkTaskBy);
		//checkTask.click();
		javaScriptExecutor(By.cssSelector("volute-app[id='UUID1395']"));
		
		explicitWaitvisibilityOfElementLocated(By.xpath("//div[contains(text(),'"+TasD+"')]/../../volute-app-feature-control[@feature-name='Delete Task']"));
      	WebElement TasDelete = SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'"+TasD+"')]/../../volute-app-feature-control[@feature-name='Delete Task']"));
      	TasDelete.click();
      	//javaScriptExecutorClick(By.xpath("//div[contains(text(),'"+TasD+"')]/../../volute-app-feature-control[@feature-name='Delete Task']"));
	}
	
	public void clickOnDeleteTasksButton()
	{
		explicitWaitvisibilityOfElementLocated(deleteTaskButtonBy);
		deleteTaskButton.click();
	}
	
	public void clickOnDeleteYesTask()
	{
		explicitWaitvisibilityOfElementLocated(deleteYesTaskBy);
		deleteYesTask.click();
	}
	
	public void clickOnDeleteNoTask()
	{
		explicitWaitvisibilityOfElementLocated(deleteNoTaskBy);
		deleteNoTask.click();
	}
	
	public boolean fetchStrikeTask(String TasStr)
	{
		//explicitWaitvisibilityOfElementLocated(StrikeTaskBy);
		//String msg = StrikeTask.getText();
		//return msg;
		boolean isDisplayed = SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'"+TasStr+"')]/../../div/paper-checkbox/div[1]/div/div")).isDisplayed();
		
		if(isDisplayed){
			return true;
		} else {
			return false;
		}
	}
	
	public String fetchTaskDeleteMsg()
	{
		explicitWaitvisibilityOfElementLocated(deleteMsgTaskBy);
		String msg = deleteMsgTask.getText();
		return msg;
	}
	
	public String fetchTasShows(String tasStr)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("//div[contains(text(),'"+tasStr+"')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchTaskAddMsg()
	{	
		explicitWaitvisibilityOfElementLocated(addedMsgTaskBy);
		String msg = addedMsgTask.getText();
		return msg;
	}
	
	public String fetchTaskUpdatedMsg()
	{	
		explicitWaitvisibilityOfElementLocated(updatedMsgTaskBy);
		String msg = updatedMsgTask.getText();
		return msg;
	}
}
