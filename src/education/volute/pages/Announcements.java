package education.volute.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import education.volute.common_lib.SuperTestScript;
//import utilities.CheckFound;

public class Announcements extends NavigatorTool {

	/*@FindBy(id = "Help")
	private WebElement anHelp;
	private By anHelpBy = By.id("Help");
	
	@FindBy(id = "Share")
	private WebElement anShare;
	private By anShareBy = By.id("Share");*/
	
	@FindBy(css = "paper-fab[title='Manage Announcements']")
	private WebElement manageAn;
	private By manageAnBy = By.cssSelector("paper-fab[title='Manage Announcements']");
	
	@FindBy(css = "paper-toolbar[id='paperToolbar'] > div > div[title='Announcements']")
	private WebElement anHeader;
	private By anHeaderBy = By.cssSelector("paper-toolbar[id='paperToolbar'] > div > div[title='Announcements']");
	
	@FindBy(id = "fullscreen")
	private WebElement anFullScreen;
	private By anFullScreenBy = By.id("fullscreen");
	
	@FindBy(css = "paper-input[label='Display Name'] > paper-input-container > div:nth-of-type(2) > div > input[title='Tool Title']")//paper-input[label='Display Name']")
	private WebElement anDisplayName;
	private By anDisplayNameBy = By.cssSelector("paper-input[label='Display Name'] > paper-input-container > div:nth-of-type(2) > div > input[title='Tool Title']");
	
	@FindBy(css = "paper-input[title='Announcement Title'] input")
	private WebElement anTitleName;
	private By anTitleNameBy = By.cssSelector("paper-input[title='Announcement Title'] input");
	
	@FindBy(css = "paper-textarea[label='Announcement Description'][title='Announcement Description:'] textarea")
	private WebElement anDescription;
	private By anDescriptionBy = By.cssSelector("paper-textarea[label='Announcement Description'][title='Announcement Description:'] textarea");
	
	@FindBy(css = "form[id='announcementForm'] paper-button[id='addFiles']")
	private WebElement anSelectFile;
	private By anSelectFileBy = By.cssSelector("form[id='announcementForm'] paper-button[id='addFiles']");
	
	@FindBy(css = "div[id='tabsContent'] > paper-tab:nth-of-type(1)")
	private WebElement anContentTab;
	private By anContentTabBy = By.cssSelector("div[id='tabsContent'] > paper-tab:nth-of-type(1)");
	
	@FindBy(css = "div[id='tabsContent'] > paper-tab:nth-of-type(2)")
	private WebElement anFeaturesTab;
	private By anFeaturesTabBy = By.cssSelector("div[id='tabsContent'] > paper-tab:nth-of-type(2)");
	
	@FindBy(css = "paper-dropdown-menu[label='Select Role']")
	private WebElement anSelectRoleMenu;
	private By anSelectRoleMenuBy = By.cssSelector("paper-dropdown-menu[label='Select Role']");
	
	@FindBy(css = "div[class='done-button style-scope app-management']")
	private WebElement anDone;
	private By anDoneBy = By.cssSelector("div[class='done-button style-scope app-management']");
	
	@FindBy(css = "paper-button[class='cancel-button style-scope announcement-manager x-scope paper-button-0']")
	private WebElement anClear;
	private By anClearBy = By.cssSelector("paper-button[class='cancel-button style-scope announcement-manager x-scope paper-button-0']");
	
	@FindBy(xpath = "//form[@id='announcementForm'] //paper-button[contains(text(),'Save')]")
	private WebElement anSave;
	private By anSaveBy = By.xpath("//form[@id='announcementForm'] //paper-button[contains(text(),'Save')]");
	
	@FindBy(css = "paper-toast[title='Announcement saved successfully']")
	private WebElement addedMsgAn;
	private By addedMsgAnBy = By.cssSelector("paper-toast[title='Announcement saved successfully']");
	
	@FindBy(css = "paper-toast[title='Please populate all required fields']")
	private WebElement missingInfoMsgAn;
	private By missingInfoMsgAnBy = By.cssSelector("paper-toast[title='Please populate all required fields']");
	
	@FindBy(css = "div[class='announcements-list-wrapper style-scope announcement-face3-module'] > paper-listbox > paper-item > iron-icon")
	private WebElement deleteUploadedFileAn;
	private By deleteUploadedFileAnBy = By.cssSelector("div[class='announcements-list-wrapper style-scope announcement-face3-module'] > paper-listbox > paper-item > iron-icon");
	
	@FindBy(css = "paper-toast[title='Announcement deleted successfully']")
	private WebElement deletedMsgAn;
	private By deletedMsgAnBy = By.cssSelector("paper-toast[title='Announcement deleted successfully']");
	
	@FindBy(css = "paper-button:nth-of-type(1)[class='style-scope confirmation-component x-scope paper-button-0']")
	private WebElement deleteYesAn;
	private By deleteYesAnBy = By.cssSelector("paper-button:nth-of-type(1)[class='style-scope confirmation-component x-scope paper-button-0']");
	
	@FindBy(css = "paper-button:nth-of-type(2)[class='style-scope confirmation-component x-scope paper-button-0']")
	private WebElement deleteNoAn;
	private By deleteNoAnBy = By.cssSelector("paper-button:nth-of-type(2)[class='style-scope confirmation-component x-scope paper-button-0']");
	
	public Announcements()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	/*public void clickOnAnHelp()
	{
		explicitWaitvisibilityOfElementLocated(anHelpBy);
		anHelp.click();
	}
	
	public void clickOnAnShare()
	{
		explicitWaitvisibilityOfElementLocated(anShareBy);
		anShare.click();
	}*/
	
	public void clickOnManageAn()
	{
		//explicitWaitvisibilityOfElementLocated(manageAnBy);
		//manageAn.click();
		javaScriptExecutorClick(manageAnBy);
	}
	
	public void clickOnAnFullScreen()
	{
		explicitWaitvisibilityOfElementLocated(anFullScreenBy);
		anFullScreen.click();
	}
	
	public void enterAnDisplayName(String anDName)
	{
		explicitWaitvisibilityOfElementLocated(anDisplayNameBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(anDisplayName).click().sendKeys(anDName).perform();
		
	}
	
	public void clearAnTitle()
	{
		explicitWaitvisibilityOfElementLocated(anTitleNameBy);
		anTitleName.clear();
	}
	
	public void enterAnTitleName(String anTName)
	{
		explicitWaitvisibilityOfElementLocated(anTitleNameBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(anTitleName).click().sendKeys(anTName).perform();
		
		//int sli_size = SuperTestScript.driver.findElements(slidesShowName).size();
		//SuperTestScript.driver.findElements(slidesShowNameBy).get(sli_size-1).sendKeys(sliSName);
		
	}
	
	public void clearAnDesc()
	{
		explicitWaitvisibilityOfElementLocated(anDescriptionBy);
		anDescription.clear();
	}
	
	public void enterAnDescription(String anDes)
	{
		explicitWaitvisibilityOfElementLocated(anDescriptionBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(anDescription).click().sendKeys(anDes).perform();
		
		//int desc_size = SuperTestScript.driver.findElements(anDescriptionBy).size();
		//SuperTestScript.driver.findElements(anDescriptionBy).get(desc_size-1).sendKeys(anDes);
	}
	
	public void clickOnAnSelectFile(String fileName) throws Exception 
	{
		javaScriptExecutorUpload(anSelectFileBy, fileName);
		//explicitWaitvisibilityOfElementLocated(anSelectFileBy);
		//anSelectFile.click();
	}
	
	public void clickOnAnnUploaded(String anName)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//paper-listbox/paper-item/div/announcement-face3-item/div/div[contains(text( ),'"+ anName +"')]"));
		WebElement selectAct = SuperTestScript.driver.findElement(By.xpath("//paper-listbox/paper-item/div/announcement-face3-item/div/div[contains(text( ),'"+ anName +"')]"));
    	selectAct.click();
	}
	
	public void clickOnAnEditButton(String anName)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//div[contains(text(),'"+anName+"')]/../.. //paper-icon-button[@icon='icons:create']"));
		WebElement selectAn = SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'"+anName+"')]/../.. //paper-icon-button[@icon='icons:create']"));
    	selectAn.click();
	}
	
	public void clickOnAnDeleteButton(String anName)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//div[contains(text(),'"+anName+"')]/../.. //paper-icon-button[@icon='icons:delete']"));
		WebElement selectAn = SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'"+anName+"')]/../.. //paper-icon-button[@icon='icons:delete']"));
    	selectAn.click();
	}
	
	public void clickOnAnContentTab()
	{
		explicitWaitvisibilityOfElementLocated(anContentTabBy);
		anContentTab.click();
	}
	
	public void clickOnAnFeaturesTab()
	{
		explicitWaitvisibilityOfElementLocated(anFeaturesTabBy);
		anFeaturesTab.click();
	}
	
	public void clickOnAnSelectRoleMenu()
	{
		explicitWaitvisibilityOfElementLocated(anSelectRoleMenuBy);
		anSelectRoleMenu.click();
	}
	
	public void clickOnAnDone()
	{
		explicitWaitvisibilityOfElementLocated(anDoneBy);
		anDone.click();
	}
	
	public void clickOnAnClear()
	{
		explicitWaitvisibilityOfElementLocated(anClearBy);
		anClear.click();
	}
	
	public void clickOnAnSave()
	{
		explicitWaitvisibilityOfElementLocated(anSaveBy);
		anSave.click();
	}
	
	public void clickOnDeleteYesAn()
	{
		explicitWaitvisibilityOfElementLocated(deleteYesAnBy);
		deleteYesAn.click();
	}
	
	public void clickOnDeleteNoAn()
	{
		explicitWaitvisibilityOfElementLocated(deleteNoAnBy);
		deleteNoAn.click();
	}
	
	public String fetchAddedMsgAn()
	{	
		explicitWaitvisibilityOfElementLocated(addedMsgAnBy);
		String msg = addedMsgAn.getText();
		return msg;
	}
	
	public String fetchMissingInfoMsgAn()
	{
		
		explicitWaitvisibilityOfElementLocated(missingInfoMsgAnBy);
		String msg = missingInfoMsgAn.getText();
		return msg;
	}
	
	public String fetchDeletedMsgAn()
	{	
		explicitWaitvisibilityOfElementLocated(deletedMsgAnBy);
		String msg = deletedMsgAn.getText();
		return msg;
	}
	
	public void ClickDeleteUploadedFileAn()
	{
		explicitWaitvisibilityOfElementLocated(deleteUploadedFileAnBy);
		deleteUploadedFileAn.click();
	}
	
	public String fetchAnHeader()
	{
		explicitWaitelementToBeClickable(anHeaderBy);
		String msg = anHeader.getText();
		return msg;
	}
	
	public String fetchManageFacesAnShows(String anName) // to void
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("announcement-manager div[title='"+anName+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchManageFacesImgAnShows(String imgName) // this was change to void
	{
		/*CheckFound check = new CheckFound();
		String str;
		str = check.determineIfFound("xpath", By.xpath("//div[@id='management'] //paper-item-body[text()[contains(.,'"+imgName+"')]]/.. //img[@src]"));
		return str;*/
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//div[@id='management'] //div[text()[contains(.,'"+imgName+"')]]/../.. //img[@src]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchListFacesNameAnShows(String anName) // to void
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("announcement-viewer div[title='"+anName+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchListFacesPostedAnShows(String anPost) // to void
	{
		try {
			SuperTestScript.driver.findElement(By.xpath("//section[@id='list'] //div[contains(text(),'"+anPost+"')]/.. //i[contains(text(),'posted by')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchListFacesUserAnShows(String anName, String anAuthor) // to void
	{
		try {
			SuperTestScript.driver.findElement(By.xpath("//section[@id='list'] //div[contains(text(),'"+anName+"')]/.. //b[contains(text(),'"+anAuthor+"')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchListFacesDescAnShows(String anDesc)
	{
		try {
			SuperTestScript.driver.findElement(By.xpath("//section[@id='list'] //div[contains(text(),'"+anDesc+"')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchListFacesImgAnShows(String imgName)
	{
		try {
			SuperTestScript.driver.findElement(By.xpath("//section[@id='list'] //div[contains(text(),'"+imgName+"')]/../.. //img[@src]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnListAn(String anName)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//section[@id='list'] //div[contains(text(),'"+anName+"')]"));
		WebElement selectAn = SuperTestScript.driver.findElement(By.xpath("//section[@id='list'] //div[contains(text(),'"+anName+"')]"));
    	selectAn.click();
	}
	
	public String fetchDetailFacesNameAnShows(String anName)
	{
		try {
			SuperTestScript.driver.findElement(By.xpath("//section[@id='detail'] //div[contains(text(),'"+anName+"')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchDetailFacesImgAnShows(String imgName)
	{
		try {
			SuperTestScript.driver.findElement(By.xpath("//section[@id='detail'] //div[contains(text(),'"+imgName+"')]/../.. //img[@src]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchDetailFacesPostedAnShows(String anPost)
	{
		try {
			SuperTestScript.driver.findElement(By.xpath("//section[@id='detail'] //div[contains(text(),'"+anPost+"')]/../.. //i[contains(text(),'posted by')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchDetailFacesUserAnShows(String anName, String anAuthor)
	{
		try {
			SuperTestScript.driver.findElement(By.xpath("//section[@id='detail'] //div[contains(text(),'"+anName+"')]/../.. //b[contains(text(),'"+anAuthor+"')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchDetailFacesFormattedTimeAnShows(String anName)
	{
		try {
			SuperTestScript.driver.findElement(By.xpath("//section[@id='detail'] //div[contains(text(),'"+anName+"')]/../.. //formatted-time"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchDetailFacesDescAnShows(String anDesc)
	{
		try {
			SuperTestScript.driver.findElement(By.xpath("//section[@id='detail'] //div[contains(text(),'"+anDesc+"')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnBackToListAn(String anName)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//section[@id='detail'] //div[contains(text(),'"+anName+"')]/.. /paper-fab[@icon='icons:arrow-back']"));
		WebElement selectAn = SuperTestScript.driver.findElement(By.xpath("//section[@id='detail'] //div[contains(text(),'"+anName+"')]/.. /paper-fab[@icon='icons:arrow-back']"));
    	selectAn.click();
	}
	
	/*public String fetchManageFacesUserInsight(String anUser)
	{
		try {
			SuperTestScript.driver.findElement(By.xpath("//section[@id='detail'] //div[contains(text(),'"+anDesc+"')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}*/
}
