package education.volute.pages;

//import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import education.volute.common_lib.SuperTestScript;

public class Portfolio extends NavigatorTool {

	/*@FindBy(css = "paper-fab[title='Manage Profile']")
	private WebElement ManageProfile;
	private By ManageProfileBy = By.cssSelector("Manage Profile");*/
    @FindBy(css = "div[class$='profile-module'] paper-icon-button[icon='icons:settings']")
	private WebElement ManageProfile;
	private By ManageProfileBy = By.cssSelector("div[class$='profile-module'] paper-icon-button[icon='icons:settings']");
	/*@FindBy(id = "manage")
	private WebElement ManageProfile;
	private By ManageProfileBy = By.id("manage");*/
	
	@FindBy(css = "paper-input[name='firstName'] input[name='firstName']")
	private WebElement ProfileFirstName;
	private By ProfileFirstNameBy = By.cssSelector("paper-input[name='firstName'] input[name='firstName']");
	
	@FindBy(css = "paper-input[label='Middle Name'] input")
	private WebElement ProfileMiddleName;
	private By ProfileMiddleNameBy = By.cssSelector("paper-input[name='firstName'] input[name='firstName']");
	
	@FindBy(css = "paper-input[name='lastName'] input[name='lastName']")
	private WebElement ProfileLastName;
	private By ProfileLastNameBy = By.cssSelector("paper-input[name='lastName'] input[name='lastName']");
	
	@FindBy(css = "paper-dropdown-menu[label='Gender']")
	private WebElement ProfileGender;
	private By ProfileGenderBy = By.cssSelector("paper-dropdown-menu[label='Gender']");
	
	@FindBy(css = "paper-input[label='Birth Date'] input")
	private WebElement ProfileBirthDate;
	private By ProfileBirthDateBy = By.cssSelector("paper-input[label='Birth Date'] input");
	
	@FindBy(css = "input[name='phone']")
	private WebElement ProfilePhone;
	private By ProfilePhoneBy = By.cssSelector("input[name='phone']");
	
	@FindBy(css = "paper-input[name='email'] input")
	private WebElement ProfileEmail;
	private By ProfileEmailBy = By.cssSelector("paper-input[name='email'] input");
	
	@FindBy(css = "profile-positions paper-button")
	private WebElement ProfilePosition;
	private By ProfilePositionBy = By.cssSelector("profile-positions paper-button");
	
	@FindBy(css = "profile-picture paper-fab[title='Remove Profile Picture']")
	private WebElement proClearImgBioButton;
	private By proClearImgBioButtonBy = By.cssSelector("profile-picture paper-fab[title='Remove Profile Picture']");
	
	@FindBy(css = "iron-selector[class*='style-scope volute-app-profile-face2-module'] > div:nth-of-type(2)")
	private WebElement ProfileAddressButton;
	private By ProfileAddressButtonBy = By.cssSelector("iron-selector[class*='style-scope volute-app-profile-face2-module'] > div:nth-of-type(2)");
	
	@FindBy(css = "paper-input[label='Address 1'] > paper-input-container > div:nth-of-type(2) > div[id='labelAndInputContainer'] > input")
	private WebElement ProfileAddress;
	private By ProfileAddressBy = By.cssSelector("paper-input[label='Address 1'] > paper-input-container > div:nth-of-type(2) > div[id='labelAndInputContainer'] > input");
	
	@FindBy(css = "paper-dropdown-menu[label='Country']")
	private WebElement ProfileCountry;
	private By ProfileCountryBy = By.cssSelector("paper-dropdown-menu[label='Country']");
	
	@FindBy(css = "paper-input[label='Zip Code'] input")
	private WebElement bioZip;
	private By bioZipBy = By.cssSelector("paper-input[label='Zip Code'] input");
	
	@FindBy(css = "iron-selector[class*='style-scope volute-app-profile-face2-module'] > div:nth-of-type(3)")
	private WebElement ProfileBiographyButton;
	private By ProfileBiographyButtonBy = By.cssSelector("iron-selector[class*='style-scope volute-app-profile-face2-module'] > div:nth-of-type(3)");
	
	//@FindBy(css = "profile-bio > div > paper-textarea > paper-input-container")
	//private WebElement ProfileBiography;
	//private By ProfileBiographyBy = By.cssSelector("/profile-bio > div > paper-textarea > paper-input-container");
	
	@FindBy(css = "paper-textarea[class$='profile-bio'] textarea")
	private WebElement ProfileBiography;
	private By ProfileBiographyBy = By.cssSelector("paper-textarea[class$='profile-bio'] textarea");
	//div[id='mirror']
	//textarea
	//profile-bio > div > paper-textarea > paper-input-container
	
	@FindBy(css = "iron-selector[class*='style-scope volute-app-profile-face2-module'] > div:nth-of-type(4)")
	private WebElement ProfileLinksButton;
	private By ProfileLinksButtonBy = By.cssSelector("iron-selector[class*='style-scope volute-app-profile-face2-module'] > div:nth-of-type(4)");
	
	@FindBy(css = "paper-input[name='linkedin'] > paper-input-container > div:nth-of-type(1) > div[id='labelAndInputContainer'] > input")
	private WebElement ProfileLinks;
	private By ProfileLinksBy = By.cssSelector("iron-selector[class*='style-scope volute-app-profile-face2-module'] > div:nth-of-type(3)");
	
	@FindBy(xpath = "//volute-app-profile-manager //paper-button[contains(text(),'Save')]")
	private WebElement ProfileSaveButton;
	private By ProfileSaveButtonBy = By.xpath("//volute-app-profile-manager //paper-button[contains(text(),'Save')]");
	
	@FindBy(css = "paper-toast[title='Profile saved successfully']>span[id='label'][class='style-scope paper-toast']")
	private WebElement profileSavedMsg;
	private By profileSavedMsgBy = By.cssSelector("paper-toast[title='Profile saved successfully']>span[id='label'][class='style-scope paper-toast']");
	
	@FindBy(css = "div:nth-of-type(2)[class*= 'done-button style-scope app-management']")
	private WebElement ProfileDoneButton;
	private By ProfileDoneButtonBy = By.cssSelector("div:nth-of-type(2)[class*= 'done-button style-scope app-management']");
	
	public Portfolio()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	/*-----------------------------------------PROFILE METHODS-----------------------------------------*/
	
	public void clickOnManageProfile()
	{
		explicitWaitvisibilityOfElementLocated(ManageProfileBy);
		ManageProfile.click();
		//javaScriptExecutor(ManageProfileBy);
	}
	
	
	/*public void clickOnHelpProfile()
	{
		explicitWaitvisibilityOfElementLocated(UserGuideBy);
		UserGuide.click();
	}*/
	
	public void clickOnClearImgProButton()
	{
		explicitWaitvisibilityOfElementLocated(proClearImgBioButtonBy);
		proClearImgBioButton.click();
	}
	
	public void clearProFirstName()
	{
		explicitWaitvisibilityOfElementLocated(ProfileFirstNameBy);
		ProfileFirstName.clear();
	}
	
	public void enterProFirstName(String proFName)
	{
		explicitWaitvisibilityOfElementLocated(ProfileFirstNameBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(ProfileFirstName).click().sendKeys(proFName).perform();
		
	}
	
	public void enterProMiddleName(String proMName)
	{
		explicitWaitvisibilityOfElementLocated(ProfileMiddleNameBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(ProfileMiddleName).click().sendKeys(proMName).perform();
		
	}
	
	public void enterProBirthDate(String proBDate)
	{
		explicitWaitvisibilityOfElementLocated(ProfileBirthDateBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(ProfileBirthDate).click().sendKeys(proBDate).perform();
		
	}
	
	public void clickOnProfileGender()
	{
		explicitWaitvisibilityOfElementLocated(ProfileGenderBy);
		ProfileGender.click();
	}
	
	public void selectGender(String gen)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//paper-item[text()='"+gen+"']"));
		WebElement selectGen = SuperTestScript.driver.findElement(By.xpath("//paper-item[text()='"+gen+"']"));
    	selectGen.click();
	}
	
	public void clearProLastName()
	{
		explicitWaitvisibilityOfElementLocated(ProfileLastNameBy);
		ProfileLastName.clear();
	}
	
	public void enterProLastName(String proLName)
	{
		explicitWaitvisibilityOfElementLocated(ProfileLastNameBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(ProfileLastName).click().sendKeys(proLName).perform();
		
	}
	
	public void enterProPhone(String proPhone)
	{
		explicitWaitvisibilityOfElementLocated(ProfilePhoneBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(ProfilePhone).click().sendKeys(proPhone).perform();
		
	}
	
	public void clearProEmail()
	{
		explicitWaitvisibilityOfElementLocated(ProfileEmailBy);
		ProfileEmail.clear();
	}
	
	public void enterProEmail(String proEmail)
	{
		explicitWaitvisibilityOfElementLocated(ProfileEmailBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(ProfileEmail).click().sendKeys(proEmail).perform();
		
	}
	
	public void clickOnAddPosition(){
		explicitWaitvisibilityOfElementLocated(ProfilePositionBy);
		ProfilePosition.click();
	}
	
	public void clickOnAddressProfile()
	{
		explicitWaitvisibilityOfElementLocated(ProfileAddressButtonBy);
		ProfileAddressButton.click();
	}
	
	public void enterProAddress(String proAddress)
	{
		explicitWaitvisibilityOfElementLocated(ProfileAddressBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(ProfileAddress).click().sendKeys(proAddress).perform();
		
	}
	
	public void clickOnCountryProfile()
	{
		explicitWaitvisibilityOfElementLocated(ProfileCountryBy);
		ProfileCountry.click();
	}
	
	public void enterZipCode(String proZipCode)
	{
		explicitWaitvisibilityOfElementLocated(bioZipBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(bioZip).click().sendKeys(proZipCode).perform();
		
	}
	
	public void clickOnBiographyProfile()
	{
		explicitWaitvisibilityOfElementLocated(ProfileBiographyButtonBy);
		ProfileBiographyButton.click();
	}
	
	public void enterProBiography(String proBiography)
	{
		int desc_size = SuperTestScript.driver.findElements(ProfileBiographyBy).size();
		SuperTestScript.driver.findElements(ProfileBiographyBy).get(desc_size-1).sendKeys(proBiography);
		//explicitWaitvisibilityOfElementLocated(ProfileBiographyBy);
		//Actions a1 = new Actions(SuperTestScript.driver);
		//a1.moveToElement(ProfileBiography).click().sendKeys(proBiography).perform();
		
	}
	
	public void clickOnLinksProfile()
	{
		explicitWaitvisibilityOfElementLocated(ProfileLinksButtonBy);
		ProfileLinksButton.click();
	}
	
	public void enterProLinks(String proLinks)
	{
		explicitWaitvisibilityOfElementLocated(ProfileLinksBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(ProfileLinks).click().sendKeys(proLinks).perform();
		
	}
	
	public void clickOnSaveProfile()
	{
		explicitWaitvisibilityOfElementLocated(ProfileSaveButtonBy);
		ProfileSaveButton.click();
	}
	
	public void clickOnDoneProfile()
	{
		explicitWaitvisibilityOfElementLocated(ProfileDoneButtonBy);
		ProfileDoneButton.click();
	}
	
	public String fetchProfileSuccessMsg()
	{
		
		explicitWaitvisibilityOfElementLocated(profileSavedMsgBy);
		String msg = profileSavedMsg.getText();
		return msg;
	}
	
}
