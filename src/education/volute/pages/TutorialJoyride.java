package education.volute.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import education.volute.common_lib.SuperTestScript;

public class TutorialJoyride extends NavigatorTool {

	@FindBy(css = "paper-icon-button[title='Launch Volute Tutorial']")//newDiscussion discussion-list > div:nth-of-type(1) > div:nth-of-type(1) > paper-button[id='newDiscussion'] > iron-icon
	private WebElement voluteTutorialButton;
	private By voluteTutorialButtonBy = By.cssSelector("paper-icon-button[title='Launch Volute Tutorial']");
	
	@FindBy(css = "div:nth-of-type(1)[class^='step'] paper-fab[id='tutorialClose']")//newDiscussion discussion-list > div:nth-of-type(1) > div:nth-of-type(1) > paper-button[id='newDiscussion'] > iron-icon
	private WebElement closeTutorialButton;
	private By closeTutorialButtonBy = By.cssSelector("social-hub div[id='lastLogin']");
	
	@FindBy(css = "volute-tutorial iron-icon[icon='image:navigate-before']")//newDiscussion discussion-list > div:nth-of-type(1) > div:nth-of-type(1) > paper-button[id='newDiscussion'] > iron-icon
	private WebElement previousTutorialButton;
	private By previousTutorialButtonBy = By.cssSelector("volute-tutorial iron-icon[icon='image:navigate-before']");
	
	@FindBy(css = "volute-tutorial iron-icon[icon='image:navigate-next']")//newDiscussion discussion-list > div:nth-of-type(1) > div:nth-of-type(1) > paper-button[id='newDiscussion'] > iron-icon
	private WebElement nextTutorialButton;
	private By nextTutorialButtonBy = By.cssSelector("volute-tutorial iron-icon[icon='image:navigate-next']");
	
	@FindBy(xpath = "//volute-tutorial //paper-button[contains(text(),'Get Started')]")//newDiscussion discussion-list > div:nth-of-type(1) > div:nth-of-type(1) > paper-button[id='newDiscussion'] > iron-icon
	private WebElement getStartedTutorialButton;
	private By getStartedTutorialButtonBy = By.xpath("//volute-tutorial //paper-button[contains(text(),'Get Started')]");
	
	@FindBy(css = "button[data-role='prev']")//newDiscussion discussion-list > div:nth-of-type(1) > div:nth-of-type(1) > paper-button[id='newDiscussion'] > iron-icon
	private WebElement previousJoyrideButton;
	private By previousJoyrideButtonBy = By.cssSelector("button[data-role='prev']");
	
	@FindBy(css = "button[data-role='next']")//newDiscussion discussion-list > div:nth-of-type(1) > div:nth-of-type(1) > paper-button[id='newDiscussion'] > iron-icon
	private WebElement nextJoyrideButton;
	private By nextJoyrideButtonBy = By.cssSelector("button[data-role='next']");
	
	@FindBy(css = "button[data-role='end']")//newDiscussion discussion-list > div:nth-of-type(1) > div:nth-of-type(1) > paper-button[id='newDiscussion'] > iron-icon
	private WebElement finishJoyrideButton;
	private By finishJoyrideButtonBy = By.cssSelector("button[data-role='end']");
	
	
	public TutorialJoyride()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	public void clickOnVoluteTutorialButton()
	{
		explicitWaitvisibilityOfElementLocated(voluteTutorialButtonBy);
		voluteTutorialButton.click();
	}
	
	public void clickOnCloseTutorialButtonBy()
	{
		explicitWaitvisibilityOfElementLocated(closeTutorialButtonBy);
		closeTutorialButton.click();
	}
	
	public void clickOnPreviousTutorialButton()
	{
		explicitWaitvisibilityOfElementLocated(previousTutorialButtonBy);
		previousTutorialButton.click();
	}
	
	public void clickOnNextTutorialButton()
	{
		explicitWaitvisibilityOfElementLocated(nextTutorialButtonBy);
		nextTutorialButton.click();
	}
	
	public void clickOnGetStartedTutorialButton()
	{
		explicitWaitvisibilityOfElementLocated(getStartedTutorialButtonBy);
		getStartedTutorialButton.click();
	}
	
	public void clickOnPreviousJoyrideButton()
	{
		explicitWaitvisibilityOfElementLocated(previousJoyrideButtonBy);
		previousJoyrideButton.click();
	}
	
	public void clickOnNextJoyrideButton()
	{
		explicitWaitvisibilityOfElementLocated(nextJoyrideButtonBy);
		nextJoyrideButton.click();
	}
	
	public void clickOnfinishJoyrideButton()
	{
		explicitWaitvisibilityOfElementLocated(finishJoyrideButtonBy);
		finishJoyrideButton.click();
	}
}
