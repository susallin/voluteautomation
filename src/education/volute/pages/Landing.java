package education.volute.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
//import org.openqa.selenium.interactions.Actions;
//import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import education.volute.common_lib.SuperTestScript;

public class Landing extends NavigatorTool {
	
	@FindBy(css = "landing-module > div:nth-of-type(2) > volute-app-feature-control:nth-of-type(1) > div:nth-of-type(1)")
	private WebElement landingManageProgs;
	private By landingManageProgsBy = By.cssSelector("landing-module > div:nth-of-type(2) > volute-app-feature-control:nth-of-type(1) > div:nth-of-type(1)");
	
	@FindBy(css = "landing-module > div:nth-of-type(2) > volute-app-feature-control:nth-of-type(1) > div:nth-of-type(2)")
	private WebElement landingManageLS;
	private By landingManageLSBy = By.cssSelector("landing-module > div:nth-of-type(2) > volute-app-feature-control:nth-of-type(1) > div:nth-of-type(2)");
	
	@FindBy(css = "landing-module > div:nth-of-type(2) > volute-app-feature-control:nth-of-type(1) > div:nth-of-type(3)")
	private WebElement landingManageUsers;
	private By landingManageUsersBy = By.cssSelector("landing-module > div:nth-of-type(2) > volute-app-feature-control:nth-of-type(1) > div:nth-of-type(3)");
	
	@FindBy(css = "landing-module > div:nth-of-type(2) > volute-app-feature-control:nth-of-type(1) > div:nth-of-type(4)")
	private WebElement landingIntructionalDesign;
	private By landingIntructionalDesignBy = By.cssSelector("landing-module > div:nth-of-type(2) > volute-app-feature-control:nth-of-type(1) > div:nth-of-type(4)");
	
	@FindBy(css = "landing-module > div:nth-of-type(6)")
	private WebElement landingAnalytics;
	private By landingAnalyticsBy = By.cssSelector("landing-module > div:nth-of-type(6)");
	
	@FindBy(css = "landing-module > div:nth-of-type(7)")
	private WebElement landingBillingRewards;
	private By landingBillingRewardsBy = By.cssSelector("landing-module > div:nth-of-type(7)");
	
	@FindBy(css = "landing-module > div:nth-of-type(2) > volute-app-feature-control:nth-of-type(1) > div:nth-of-type(5)")
	private WebElement landingPrivateMarketplace;
	private By landingPrivateMarketplaceBy = By.cssSelector("landing-module > div:nth-of-type(2) > volute-app-feature-control:nth-of-type(1) > div:nth-of-type(5)");
	
	@FindBy(css = "landing-module > div:nth-of-type(2) > volute-app-feature-control:nth-of-type(1) > div:nth-of-type(6)")
	private WebElement landingCommunityMarketplace;
	private By landingCommunityMarketplaceBy = By.cssSelector("landing-module > div:nth-of-type(2) > volute-app-feature-control:nth-of-type(1) > div:nth-of-type(6)");

	public Landing()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	public void clickOnLandingMP()
	{
		explicitWaitvisibilityOfElementLocated(landingManageProgsBy);
		landingManageProgs.click();
	}
	
	public void clickOnLandingLS()
	{
		explicitWaitvisibilityOfElementLocated(landingManageLSBy);
		landingManageLS.click();
	}
	
	public void clickOnLandingMU()
	{
		explicitWaitvisibilityOfElementLocated(landingManageUsersBy);
		landingManageUsers.click();
	}
	
	public void clickOnLandingID()
	{
		explicitWaitvisibilityOfElementLocated(landingIntructionalDesignBy);
		landingIntructionalDesign.click();
	}
	
	public void clickOnLandingAnalytics()
	{
		explicitWaitvisibilityOfElementLocated(landingAnalyticsBy);
		landingAnalytics.click();
	}
	
	public void clickOnLandingBR()
	{
		explicitWaitvisibilityOfElementLocated(landingBillingRewardsBy);
		landingBillingRewards.click();
	}
	
	public void clickOnLandingPM()
	{
		explicitWaitvisibilityOfElementLocated(landingPrivateMarketplaceBy);
		landingPrivateMarketplace.click();
	}
	
	public void clickOnLandingCM()
	{
		explicitWaitvisibilityOfElementLocated(landingCommunityMarketplaceBy);
		landingCommunityMarketplace.click();
	}
	
	/*
	public void selectaPlace(String landing)
	{
		// Trying to Launch to all the places, we need a locator for this.
		// Writ them separately.
		explicitWaitvisibilityOfElementLocated(By.xpath("//div[contains(text(),'" + landing + "')]"));
		WebElement land = SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'" + landing + "')]"));
    	land.click();
		/*
		WebElement landNum = SuperTestScript.driver.findElement(By.cssSelector("landing-module > "+landing+")"));
		explicitWaitvisibilityOfElementLocated(By.cssSelector("landing-module > "+landing+")"));
		landNum.click();
	}*/
}
