package education.volute.pages;

import java.util.concurrent.TimeUnit;

//import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import education.volute.common_lib.SuperTestScript;

public class Slides extends NavigatorTool {

	/*@FindBy(id = "Help")
	private WebElement slidesHelp;
	private By slidesHelpBy = By.id("Help");
	
	@FindBy(id = "Share")
	private WebElement slideShare;
	private By slideShareBy = By.id("Share");*/
	
	@FindBy(css = "paper-fab[id^='Slides'][title='Manage']") //paper-fab[title='Manage Slides']
	private WebElement manageSlides;
	private By manageSlidesBy = By.cssSelector("paper-fab[id^='Slides'][title='Manage']");
	
	@FindBy(id = "Full Screen")
	private WebElement slidesFullScreen;
	private By slidesFullScreenBy = By.id("Full Screen");
	
	@FindBy(css = "paper-input[label='Display Name'] > paper-input-container > div:nth-of-type(2) > div > input[title='Tool Title']")//paper-input[label='Display Name']")
	private WebElement slidesDisplayName;
	private By slidesDisplayNameBy = By.cssSelector("paper-input[label='Display Name'] > paper-input-container > div:nth-of-type(2) > div > input[title='Tool Title']");
	
	@FindBy(css = "paper-input[label='Slideshow Name:'] input")
	private WebElement slidesShowName;
	private By slidesShowNameBy = By.cssSelector("paper-input[label='Slideshow Name:'] input");
	
	//@FindBy(css = "paper-input[label='Video Title'] > paper-input-container > div:nth-of-type(2) > div > input")
	//private WebElement SlidesShowClear;
	//private By SlidesShowClearBy = By.cssSelector("paper-input[label='Video Title'] > paper-input-container > div:nth-of-type(2) > div > input");
	
	@FindBy(css = "paper-toolbar[id='paperToolbar'] > div > div[title='Slides']")
	private WebElement slidesHeader;
	private By slidesHeaderBy = By.cssSelector("paper-toolbar[id='paperToolbar'] > div > div[title='Slides']");
	
	//@FindBy(xpath = "//input[@type='file']")
	//private WebElement slideSelectFile;
	//private By slideSelectFileBy = By.xpath("//input[@type='file']");
	
	//vaadin-upload[title='PDF Uploader'] > div > div > 
	@FindBy(css = "vaadin-upload[title='Select Slideshow'] paper-button[id='addFiles']")
	private WebElement slideSelectFile;
	private By slideSelectFileBy = By.cssSelector("vaadin-upload[title='Select Slideshow'] paper-button[id='addFiles']");
	
	
	@FindBy(css = "div[id='tabsContent'] > paper-tab:nth-of-type(1)")
	private WebElement slidesContentTab;
	private By slidesContentTabBy = By.cssSelector("div[id='tabsContent'] > paper-tab:nth-of-type(1)");
	
	@FindBy(css = "div[id='tabsContent'] > paper-tab:nth-of-type(2)")
	private WebElement slidesFeaturesTab;
	private By slidesFeaturesTabBy = By.cssSelector("div[id='tabsContent'] > paper-tab:nth-of-type(2)");
	
	@FindBy(css = "paper-dropdown-menu[label='Select Role']")
	private WebElement slidesSelectRoleMenu;
	private By slidesSelectRoleMenuBy = By.cssSelector("paper-dropdown-menu[label='Select Role']");
	
	@FindBy(css = "Volute-app[id='UUID7934'] paper-fab[icon='icons:arrow-back']")
	private WebElement slidesBackArrow;
	private By slidesBackArrowBy = By.cssSelector("Volute-app[id='UUID7934'] paper-fab[icon='icons:arrow-back']");
	
	@FindBy(css = "Volute-app[id='UUID7934'] paper-fab[icon='icons:add']")
	private WebElement slidesAdd;
	private By slidesAddBy = By.cssSelector("Volute-app[id='UUID7934'] paper-fab[icon='icons:add']");
	
	@FindBy(css = "volute-app[id='UUID7934'] paper-button[title='Clear']") //paper-button[title='Clear Slideshow']
	private WebElement slidesShowClear;
	private By slidesShowClearBy = By.cssSelector("volute-app[id='UUID7934'] paper-button[title='Clear']");
	
	@FindBy(css = "volute-app[id='UUID7934'] paper-button[title='Save']") //paper-button[title='Save Slideshow']
	private WebElement slidesShowSave;
	private By slidesShowSaveBy = By.cssSelector("volute-app[id='UUID7934'] paper-button[title='Save']");
	
	@FindBy(css = "paper-toast[title='Slides saved successfully.']")
	private WebElement addedMsgSlides;
	private By addedMsgSlidesBy = By.cssSelector("paper-toast[title='Slides saved successfully.']");
	
	/*
	@FindBy(css = "paper-toast[title='Slides saved successfully.']")
	private WebElement slideSelectUploadedFile;
	private By slideSelectUploadedFileBy = By.cssSelector("paper-toast[title='Slides saved successfully.']");
	*/
	@FindBy(css = "paper-listbox[id='slideshowsListbox'] > paper-icon-item > paper-icon-button")
	private WebElement slideDeleteButton;
	private By slideDeleteButtonBy = By.cssSelector("paper-listbox[id='slideshowsListbox'] > paper-icon-item > paper-icon-button");
	
	
	@FindBy(css = "paper-button:nth-of-type(1)[class='style-scope confirmation-component x-scope paper-button-0']")
	private WebElement deleteYesSlides;
	private By deleteYesSlidesBy = By.cssSelector("paper-button:nth-of-type(1)[class='style-scope confirmation-component x-scope paper-button-0']");
	
	@FindBy(css = "paper-button:nth-of-type(2)[class='style-scope confirmation-component x-scope paper-button-0']")
	private WebElement deleteNoSlides;
	private By deleteNoSlidesBy = By.cssSelector("paper-button:nth-of-type(2)[class='style-scope confirmation-component x-scope paper-button-0']");
	
	@FindBy(css = "paper-toast[title='Slide deleted successfully.']")
	private WebElement deletedMsgSlides;
	private By deletedMsgSlidesBy = By.cssSelector("paper-toast[title='Slide deleted successfully.']");
	
	@FindBy(css = "paper-icon-button[title='Back to Slideshow List']")
	private WebElement slidesBackToList;
	private By slidesBackToListBy = By.cssSelector("paper-icon-button[title='Back to Slideshow List']");
	
	public Slides()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	/*public void clickOnSlidesHelp()
	{
		explicitWaitvisibilityOfElementLocated(slidesHelpBy);
		slidesHelp.click();
	}
	
	public void clickOnSlidesShare()
	{
		explicitWaitvisibilityOfElementLocated(slideShareBy);
		slideShare.click();
	}*/
	
	public void clickOnSlidesManage()
	{
		//explicitWaitvisibilityOfElementLocated(manageSlidesBy);
		//manageSlides.click();
		javaScriptExecutorClick(manageSlidesBy);
		//javaScriptExecutor(manageSlidesBy);
	}
	
	public void clickOnSlidesFullScreen()
	{
		explicitWaitvisibilityOfElementLocated(slidesFullScreenBy);
		slidesFullScreen.click();
	}
	
	public void enterSlideDisplayName(String sliDName)
	{
		explicitWaitvisibilityOfElementLocated(slidesDisplayNameBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(slidesDisplayName).click().sendKeys(sliDName).perform();
		
	}
	
	public void enterSlideShowName(String sliSName)
	{
		explicitWaitvisibilityOfElementLocated(slidesShowNameBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(slidesShowName).click().sendKeys(sliSName).perform();
		
		//int sli_size = SuperTestScript.driver.findElements(slidesShowName).size();
		//SuperTestScript.driver.findElements(slidesShowNameBy).get(sli_size-1).sendKeys(sliSName);
		
	}
	
	public void clearSlideShowName()
	{
		explicitWaitvisibilityOfElementLocated(slidesShowNameBy);
		slidesShowName.clear();
	}
	
	public void clickOnSlideSelectFile()//(String fileName) throws Exception 
	{
		//javaScriptExecutorUpload(slideSelectFileBy, fileName);
		//WebElement uploadElement = SuperTestScript.driver.findElement(slideSelectFileBy);
		//uploadElement.sendKeys(fileName);
		explicitWaitvisibilityOfElementLocated(slideSelectFileBy);
		slideSelectFile.click();
		//Runtime.getRuntime().exec("C:\\AutoIT\\FileUploadPDF.exe");
		//windowFileUpload(file);
		//explicitWaitvisibilityOfElementLocated(slideSelectFileBy);
		//slideSelectFile.sendKeys("/Users/Volute/Desktop/"+sliFilName);
		
		//getDriver().findElement(By.xpath("//input[@type='file']")).sendKeys("/tmp/test.xlsx");
	}
	
	public String fetchFacesliShows(String sliStr)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("span[title='"+sliStr+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchManageFacesliShows(String sliStr)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("paper-icon-item[title='Slideshow "+sliStr+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchSliShows(String sliStr)
	{
		try {
			SuperTestScript.driver.findElement(By.cssSelector("paper-icon-button[title='Delete "+sliStr+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchSliShowsFaceImg(String sliImg)
	{
		try {
			SuperTestScript.driver.findElement(By.cssSelector("div[aria-label='"+sliImg+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnSlidesBackArrow()
	{
		explicitWaitvisibilityOfElementLocated(slidesBackArrowBy);
		slidesBackArrow.click();
	}
	
	public void ClickslidesAddBy()
	{
		explicitWaitvisibilityOfElementLocated(slidesAddBy);
		slidesAdd.click();
	}
	
	public void clickOnSlidesShowCancel()
	{
		explicitWaitvisibilityOfElementLocated(slidesShowClearBy);
		slidesShowClear.click();
	}
	
	public void clickOnSlidesShowSave()
	{
		explicitWaitvisibilityOfElementLocated(slidesShowSaveBy);
		slidesShowSave.click();
	}
	
	public void clickOnSlideDeleteButton(String sliName)
	{
		javaScriptExecutor(By.cssSelector("paper-icon-button[title='Remove Slide "+sliName +"']"));
		//explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-icon-button[title='Delete "+sliName +"']"));
		//WebElement selectSli = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-button[title='Delete "+sliName +"']"));
    	//selectSli.click();
	}
	
	public void clickOnSlideEditButton(String sliName)
	{
		javaScriptExecutor(By.cssSelector("paper-icon-button[title='Edit Slide "+sliName +"']"));
	}
	
	public void clickOnDeleteYesSlides()
	{
		explicitWaitvisibilityOfElementLocated(deleteYesSlidesBy);
		deleteYesSlides.click();
	}
	
	public void clickOnDeleteNoSlides()
	{
		explicitWaitvisibilityOfElementLocated(deleteNoSlidesBy);
		deleteNoSlides.click();
	}
	
	public String fetchSlidesAddMsg()
	{	
		explicitWaitvisibilityOfElementLocated(addedMsgSlidesBy);
		String msg = addedMsgSlides.getText();
		return msg;
	}
	
	public String fetchSlidesDeleteMsg()
	{	
		explicitWaitvisibilityOfElementLocated(deletedMsgSlidesBy);
		String msg = deletedMsgSlides.getText();
		return msg;
	}
	
	public String fetchSlidesHeader()
	{
		explicitWaitelementToBeClickable(slidesHeaderBy);
		String msg = slidesHeader.getText();
		return msg;
	}
	
	public void clickOnSlidesContentTab()
	{
		explicitWaitvisibilityOfElementLocated(slidesContentTabBy);
		slidesContentTab.click();
	}
	
	public void clickOnSlidesFeaturesTab()
	{
		explicitWaitvisibilityOfElementLocated(slidesFeaturesTabBy);
		slidesFeaturesTab.click();
	}
	
	public void clickOnSlidesSelectRoleMenu()
	{
		explicitWaitvisibilityOfElementLocated(slidesSelectRoleMenuBy);
		slidesSelectRoleMenu.click();
	}
	
	public void selectSlidesRole(String sliRoleNum)
	{
		WebElement sli = SuperTestScript.driver.findElement(By.cssSelector("paper-listbox > paper-item:nth-of-type("+sliRoleNum+")"));
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-listbox > paper-item:nth-of-type("+sliRoleNum+")"));
		sli.click();
		
	}
	
	public void clickOnselectedSlide(String sliName)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-icon-item[title='Slideshow "+sliName+"']"));
		WebElement selectSli = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-item[title='Slideshow "+sliName+"']"));
    	selectSli.click();
    	
	}
	
	public String fetchFacesSlidShows(String sliStr)
	{
		try {
			SuperTestScript.driver.findElement(By.cssSelector("paper-icon-item[title='Slideshow "+sliStr+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnListSlide(String sliName)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//span[contains(text(),'"+sliName+"')]/../../div[contains(@class,'preview-img')]"));
		WebElement selectSli = SuperTestScript.driver.findElement(By.xpath("//span[contains(text(),'"+sliName+"')]/../../div[contains(@class,'preview-img')]"));
    	selectSli.click();
    	
	}
	
	public void clickOnSlidesBackToList(String sliName)
	{
		javaScriptExecutorClick(By.xpath("//div[contains(@title,'"+sliName+"')]/.. //paper-icon-button[@title='Back to Slideshow List']"));
		/*explicitWaitvisibilityOfElementLocated(By.xpath("//div[contains(@title,'"+sliName+"')]/.. //paper-icon-button[@title='Back to Slideshow List']"));
		WebElement selectSli = SuperTestScript.driver.findElement(By.xpath("//div[contains(@title,'"+sliName+"')]/.. //paper-icon-button[@title='Back to Slideshow List']"));
    	selectSli.click();*/
	}
}
