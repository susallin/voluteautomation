package education.volute.pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import education.volute.common_lib.SuperTestScript;

public class ProfileList extends NavigatorTool {
	/*@FindBy(id = "Help")
	private WebElement plHelp;
	private By plHelpBy = By.id("Help");
	
	@FindBy(id = "Share")
	private WebElement plShare;
	private By plShareBy = By.id("Share");*/
	
	@FindBy(css = "paper-fab[title='Manage Social']")
	private WebElement managePl;
	private By managePlBy = By.cssSelector("paper-fab[title='Manage Social']");
	
	@FindBy(id = "fullscreen")
	private WebElement plFullScreen;
	private By plFullScreenBy = By.id("fullscreen");
	
	@FindBy(css = "paper-input[label='Search']")
	private WebElement searchPl;
	private By searchPlBy = By.cssSelector("paper-input[label='Search']");
	
	@FindBy(css = "paper-dropdown-menu[label='Sort By']")
	private WebElement sortPl;
	private By sortPlBy = By.cssSelector("paper-dropdown-menu[label='Sort By']");
	
	@FindBy(css = "paper-toolbar[id='paperToolbar'] > div > div[title='Profile List']")
	private WebElement plHeader;
	private By plHeaderBy = By.cssSelector("paper-toolbar[id='paperToolbar'] > div > div[title='Profile List']");
	
	@FindBy(css = "paper-input[label='Search Users']")
	private WebElement plSearchIndividual;
	private By plSearchIndividualBy = By.cssSelector("paper-input[label='Search Users']");
	
	@FindBy(css = "paper-input[label='Search Groups']")
	private WebElement plSearchGroups;
	private By plSearchGroupsBy = By.cssSelector("paper-input[label='Search Groups']");
	
	@FindBy(css = "paper-button[class='style-scope volute-app-assignment-face1 x-scope paper-button-0']")
	private WebElement plAssignIndivGroups;
	private By plAssignIndivGroupsBy = By.cssSelector("paper-button[class='style-scope volute-app-assignment-face1 x-scope paper-button-0']");
	
	@FindBy(css = "div[id='tabsContent'] > paper-tab:nth-of-type(1)")
	private WebElement plContentTab;
	private By plContentTabBy = By.cssSelector("div[id='tabsContent'] > paper-tab:nth-of-type(1)");
	
	@FindBy(css = "div[id='tabsContent'] > paper-tab:nth-of-type(2)")
	private WebElement plFeaturesTab;
	private By plFeaturesTabBy = By.cssSelector("div[id='tabsContent'] > paper-tab:nth-of-type(2)");
	
	@FindBy(css = "paper-dropdown-menu[label='Select Role']")
	private WebElement plSelectRoleMenu;
	private By plSelectRoleMenuBy = By.cssSelector("paper-dropdown-menu[label='Select Role']");
	
	@FindBy(css = "paper-fab[title='Back to user list']")
	private WebElement plBackToUserList;
	private By plBackToUserListBy = By.cssSelector("paper-fab[title='Back to user list']");
	
	@FindBy(css = "paper-toast[title='Users added successfully']")
	private WebElement addMsgPro;
	private By addMsgProBy = By.cssSelector("paper-toast[title='Users added successfully']");
	
	public ProfileList()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	/*public void clickOnPlHelp()
	{
		explicitWaitvisibilityOfElementLocated(plHelpBy);
		plHelp.click();
	}
	
	public void clickOnPlShare()
	{
		explicitWaitvisibilityOfElementLocated(plShareBy);
		plShare.click();
	}*/
	
	public void clickOnPlManage()
	{
		//explicitWaitvisibilityOfElementLocated(managePlBy);
		//managePl.click();
		javaScriptExecutorClick(managePlBy);
	}
	
	public void clickOnPlFullScreen()
	{
		explicitWaitvisibilityOfElementLocated(plFullScreenBy);
		plFullScreen.click();
	}
	
	public void enterSearchBoxPl(String serDUser)
	{
		explicitWaitvisibilityOfElementLocated(searchPlBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(searchPl).click().sendKeys(serDUser).perform();
		
	}
	
	public void clickOnSortPl()
	{
		explicitWaitvisibilityOfElementLocated(sortPlBy);
		sortPl.click();
	}
	
	public void selectSort(String sortDirNumber)
	{
		WebElement ls = SuperTestScript.driver.findElement(By.cssSelector("paper-item[id='"+sortDirNumber+"']"));
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-item[id='"+sortDirNumber+"']"));
		ls.click();
	}
	
	public String fetchPlHeader()
	{
		explicitWaitelementToBeClickable(plHeaderBy);
		String msg = plHeader.getText();
		return msg;
	}
	
	/*public void enterPlDisplayName(String dirDName)
	{
		explicitWaitvisibilityOfElementLocated(plDisplayNameBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(plDisplayName).click().sendKeys(dirDName).perform();
		
	}*/
	
	public void enterPlSearchIndividual(String serIndiv)
	{
		explicitWaitvisibilityOfElementLocated(plSearchIndividualBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(plSearchIndividual).click().sendKeys(serIndiv).perform();
		
	}
	
	public void enterPlSearchGroups(String serGroup)
	{
		explicitWaitvisibilityOfElementLocated(plSearchGroupsBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(plSearchGroups).click().sendKeys(serGroup).perform();
		
	}
	
	public void clickOnPlAssignIndivGroups()
	{
		explicitWaitvisibilityOfElementLocated(plAssignIndivGroupsBy);
		plAssignIndivGroups.click();
	}
	
	public void clickOnPlContentTab()
	{
		explicitWaitvisibilityOfElementLocated(plContentTabBy);
		plContentTab.click();
	}
	
	public void clickOnPlFeaturesTab()
	{
		explicitWaitvisibilityOfElementLocated(plFeaturesTabBy);
		plFeaturesTab.click();
	}
	
	public void clickOnPlSelectRoleMenu()
	{
		explicitWaitvisibilityOfElementLocated(plSelectRoleMenuBy);
		plSelectRoleMenu.click();
	}
	
	public void selectPlRole(String dirRoleNum)
	{
		WebElement dir = SuperTestScript.driver.findElement(By.cssSelector("paper-listbox > paper-item:nth-of-type("+dirRoleNum+")"));
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-listbox > paper-item:nth-of-type("+dirRoleNum+")"));
		dir.click();
		
	}
	
	public void selectAllIndividuals()
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("assign-module[id='assignment'] > neon-animated-pages > div:nth-of-type(1) > paper-listbox > paper-icon-item"));//paper-icon-item[class='style-scope assign-module x-scope paper-icon-item-0']
		List<WebElement> allIndivs = SuperTestScript.driver.findElements(By.cssSelector("assign-module[id='assignment'] > neon-animated-pages > div:nth-of-type(1) > paper-listbox > paper-icon-item"));//paper-listbox[id='userListbox'] > paper-icon-item
		
		for(int i=0; i<=allIndivs.size()-1; i++)
		{
			allIndivs.get(i).click();
		}
		
	}
	
	public void selectAllGroups()
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-listbox[id='groupListbox'] > paper-icon-item"));
		List<WebElement> allGroups = SuperTestScript.driver.findElements(By.cssSelector("paper-listbox[id='groupListbox'] > paper-icon-item"));
		
		for(int i=0; i<=allGroups.size()-1; i++)
		{
			allGroups.get(i).click();
		}
		
	}
	
	public void clickOnPLDeleteButton(String plName)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//a[@href='mailto:"+plName+"']/../../../../paper-icon-button"));
		WebElement selectPl = SuperTestScript.driver.findElement(By.xpath("//a[@href='mailto:"+plName+"']/../../../../paper-icon-button"));
    	selectPl.click();
	}
	
	public String fetchManageFaceIndShows(String plStr)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//volute-social-manager //a[contains(text(),'"+plStr+"')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchFaceIndShows(String plStr)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("div[title='"+plStr+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnViewUserButton(String plName)
	{
		/*explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-icon-button[title='view "+plName+" profile']"));
		WebElement selectPl = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-button[title='view "+plName+" profile']"));
    	selectPl.click();*/
    	javaScriptExecutorClick(By.cssSelector("paper-icon-button[title='view "+plName+" profile']"));
	}
	
	public void clickOnBackToUserList()
	{
		/*explicitWaitvisibilityOfElementLocated(plBackToUserListBy);
		plBackToUserList.click();*/
		javaScriptExecutorClick(plBackToUserListBy);
	}
	
	public String fetchAddMsgPro()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(addMsgProBy);
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
}
