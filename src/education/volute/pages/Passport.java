package education.volute.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import education.volute.common_lib.SuperTestScript;

public class Passport extends NavigatorTool {
	
	// This hasn't been added yet.
	@FindBy(css = "paper-fab[title='Manage Passport']")
	private WebElement managePassport;
	private By managePassportBy = By.cssSelector("paper-fab[title='Manage Passport']");
	
	@FindBy(css = "paper-dropdown-menu[aria-label='Select an icon for this topic']")
	private WebElement selectIconPassport;
	private By selectIconPassportBy = By.cssSelector("paper-dropdown-menu[aria-label='Select an icon for this topic']");
	
	@FindBy(css = "paper-input[aria-label='New section title'] input")
	private WebElement titlePassport;
	private By titlePassportBy = By.cssSelector("paper-input[aria-label='New section title'] input");
	
	@FindBy(css = "form[id='addTopicForm'] vaadin-upload[label='Select an image'][title='Select Image'] paper-button[id='addFiles']")
	private WebElement selectFilePassport;
	private By selectFilePassportBy = By.cssSelector("form[id='addTopicForm'] vaadin-upload[label='Select an image'][title='Select Image'] paper-button[id='addFiles']");
	
	@FindBy(css = "volute-app[id='UUID1001'] div[class^='note-editable']")
	private WebElement contentPassport;
	private By contentPassportBy = By.cssSelector("volute-app[id='UUID1001'] div[class^='note-editable']");
	
	@FindBy(css = "polymer-quill[id='topicContent'] > div[id='editor'] > div[class$='editor']")
	private WebElement clearContentPassport;
	private By clearContentPassportBy = By.cssSelector("polymer-quill[id='topicContent'] > div[id='editor'] > div[class$='editor']");
	
	@FindBy(css = "passport-manager paper-fab[title='Remove Image']")
	private WebElement removeImgPassport;
	private By removeImgPassportBy = By.cssSelector("passport-manager paper-fab[title='Remove Image']");
	
	@FindBy(css = "volute-app[id='UUID1001'] paper-button[title='Clear']")
	private WebElement cancelButtonPassport;
	private By cancelButtonPassportBy = By.cssSelector("volute-app[id='UUID1001'] paper-button[title='Clear']");
	
	@FindBy(css = "volute-app[id='UUID1001'] paper-button[title^='Save']")
	private WebElement saveButtonPassport;
	private By saveButtonPassportBy = By.cssSelector("volute-app[id='UUID1001'] paper-button[title^='Save']");
	
	@FindBy(css = "paper-toast[title='Topic saved successfully']")
	private WebElement saveMsgPassport;
	private By saveMsgPassportBy = By.cssSelector("paper-toast[title='Topic saved successfully']");
	
	@FindBy(css = "paper-toast[title='Topic deleted successfully']")
	private WebElement deletedMsgPassport;
	private By deletedMsgPassportBy = By.cssSelector("paper-toast[title='Topic deleted successfully']");
	
	public Passport()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	public void clickOnPassportManage()
	{
		//explicitWaitvisibilityOfElementLocated(manageVideoBy);
		//manageVideo.click();
		javaScriptExecutor(managePassportBy);
	}
	
	public void clickOnSelectIconPassport()
	{
		explicitWaitvisibilityOfElementLocated(selectIconPassportBy);
		selectIconPassport.click();
	}
	
	public void selectIconPassport(String iconName)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-menu-button[id='menuButton'] iron-icon[title='"+iconName+"']"));
		WebElement selectIcon = SuperTestScript.driver.findElement(By.cssSelector("paper-menu-button[id='menuButton'] iron-icon[title='"+iconName+"']"));
		selectIcon.click();	
	}
	
	public void clearPassportTitle()
	{
		explicitWaitvisibilityOfElementLocated(titlePassportBy);
		titlePassport.clear();
	}
	
	public void enterOnTitlePassport(String pasTitle)
	{
		explicitWaitvisibilityOfElementLocated(titlePassportBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(titlePassport).click().sendKeys(pasTitle).perform();
		
	}
	
	public void clickOnSelectFilePassport(String fileName) throws Exception
	{
		javaScriptExecutorUpload(selectFilePassportBy, fileName);
		//explicitWaitvisibilityOfElementLocated(selectFilePassportBy);
		//selectFilePassport.click();
	}
	
	public void clickOnRemoveImgPassportButton()
	{
		explicitWaitvisibilityOfElementLocated(removeImgPassportBy);
		removeImgPassport.click();
	}
	
	public String fetchPassportImgShows()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("passport-manager-add-module iron-image[alt='Image Added']>img[src]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clearContentPassport()
	{
		explicitWaitvisibilityOfElementLocated(contentPassportBy);
		contentPassport.clear();
	}
	
	public void enterOnContentPassport(String pasContent)
	{
		explicitWaitvisibilityOfElementLocated(contentPassportBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(contentPassport).click().sendKeys(pasContent).perform();
		
	}
	
	public void clickOnCancelButtonPassport()
	{
		explicitWaitvisibilityOfElementLocated(cancelButtonPassportBy);
		cancelButtonPassport.click();
	}
	
	public void clickOnSaveButtonPassport()
	{
		explicitWaitvisibilityOfElementLocated(saveButtonPassportBy);
		saveButtonPassport.click();
	}
	
	public void clickOnPassportEditButton(String pasName)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//div[contains(text(),'"+pasName+"')]/../../paper-icon-button[@icon='icons:create']"));
		WebElement selectPas = SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'"+pasName+"')]/../../paper-icon-button[@icon='icons:create']"));
    	selectPas.click();
	}
	
	public void clickOnPassportDeleteButton(String pasName)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//div[contains(text(),'"+pasName+"')]/../../paper-icon-button[@icon='icons:delete']"));
		WebElement selectPas = SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'"+pasName+"')]/../../paper-icon-button[@icon='icons:delete']"));
    	selectPas.click();
	}
	
	public String fetchPassportAddMsg()
	{	
		explicitWaitvisibilityOfElementLocated(saveMsgPassportBy);
		String msg = saveMsgPassport.getText();
		return msg;
	}
	
	public String fetchPassportDeleteMsg()
	{	
		explicitWaitvisibilityOfElementLocated(deletedMsgPassportBy);
		String msg = deletedMsgPassport.getText();
		return msg;
	}
	
	public String fetchManageFacesPasShows(String pasStr)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//app-management //div[contains(text(),'"+pasStr+"')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchFacesPasShows(String pasStr)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//passport-view //div[contains(text(),'"+pasStr+"')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnPassportTab(String pasName)
	{
		javaScriptExecutorClick(By.xpath("//div[contains(text(),'"+pasName+"')]"));
		/*explicitWaitvisibilityOfElementLocated(By.xpath("//div[contains(text(),'"+pasName+"')]"));
		WebElement selectPas = SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'"+pasName+"')]"));
    	selectPas.click();*/
	}
	
	public String fetchPasisHighlighted(String pasStr)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//paper-item[@aria-selected]/div[contains(text(),'"+pasStr+"')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchPasisNotHighlighted(String pasStr)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//paper-item[@aria-disabled]/div[contains(text(),'"+pasStr+"')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchManageFacesImgPasShows(String imgName)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//div[@id='management'] //div[text()[contains(.,'"+imgName+"')]]/../.. //img[@src]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchFacesImgPasShows(String imgName)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//h1[contains(text(),'"+imgName+"')]/.. //img[@src]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
}
