package education.volute.pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import education.volute.common_lib.SuperTestScript;

public class Discussion extends NavigatorTool
{
	// Floatable
	@FindBy(css = "paper-fab[id='addDiscussion-undefined']")//newDiscussion discussion-list > div:nth-of-type(1) > div:nth-of-type(1) > paper-button[id='newDiscussion'] > iron-icon
	private WebElement addNewDiscButton;
	private By addNewDiscButtonBy = By.cssSelector("paper-fab[id='addDiscussion-undefined']");
	
	// Floatable
	@FindBy(css = "paper-fab[id='searchDiscussion-undefined']")//newDiscussion discussion-list > div:nth-of-type(1) > div:nth-of-type(1) > paper-button[id='newDiscussion'] > iron-icon
	private WebElement searchDiscButton;
	private By searchDiscButtonBy = By.cssSelector("paper-fab[id='searchDiscussion-undefined']");
	
	// Floatable
	@FindBy(xpath = "//paper-fab[@id='searchDiscussion-undefined']/.. //input[@title='Search Discussion Input']")//newDiscussion discussion-list > div:nth-of-type(1) > div:nth-of-type(1) > paper-button[id='newDiscussion'] > iron-icon
	private WebElement searchDiscTextBox;
	private By searchDiscTextBoxBy = By.xpath("//paper-fab[@id='searchDiscussion-undefined']/.. //input[@title='Search Discussion Input']");
	
	@FindBy(css = "paper-input[id='discussionName']")
	private WebElement discNameTextBox;
	private By discNameTextBoxBy = By.cssSelector("paper-input[id='discussionName']");
	
	@FindBy(id = "addDiscussionUsers")
	private WebElement assignGroupsOrIndividualsButton;
	private By assignGroupsOrIndividualsButtonBy = By.id("addDiscussionUsers");
	
//	@FindBy(css = "paper-tab[class='style-scope volute-app-assignment-face1 x-scope paper-tab-0 iron-selected']")
//	private WebElement individualsTab;
//	private By individualsTabBy = By.cssSelector("paper-tab[class='style-scope volute-app-assignment-face1 x-scope paper-tab-0 iron-selected']");
//	
//	@FindBy(css = "paper-tab[class='style-scope volute-app-assignment-face1 x-scope paper-tab-0 iron-selected']")
//	private WebElement groupsTab;
//	private By groupsTabBy = By.cssSelector("paper-tab[class='style-scope volute-app-assignment-face1 x-scope paper-tab-0 iron-selected']");
//	
	
	@FindBy(css = "volute-app[id='UUID6891'] paper-fab[id='closeModals']")
	private WebElement disCloseFl;
	private By disCloseFlBy = By.cssSelector("volute-app[id='UUID6891'] paper-fab[id='closeModals']");
			
			
	@FindBy(css = "paper-icon-item[class='volute-app-rooms-list-item style-scope volute-app-assignment-face1 x-scope paper-icon-item-3']")
	private WebElement groupSelection;
	private By groupsSelectionBy = By.cssSelector("paper-icon-item[class='volute-app-rooms-list-item style-scope volute-app-assignment-face1 x-scope paper-icon-item-3']");
	
	@FindBy(css = "paper-button[class='save-button style-scope assign-module x-scope paper-button-0']")
	private WebElement assignButton;
	private By assignButtonBy = By.cssSelector("paper-button[class='save-button style-scope assign-module x-scope paper-button-0']");
	
	@FindBy(id = "saveDiscussion")
	private WebElement saveDiscButton;
	private By saveDiscButtonBy = By.id("saveDiscussion");
	
	@FindBy(id = "cancelDiscussion")
	private WebElement cancelDiscButton;
	private By cancelDiscButtonBy = By.id("cancelDiscussion");
	
	@FindBy(css = "paper-toast[title='Discussion created successfully']")
	private WebElement addedMsgDiscussion;
	private By addedMsgDiscussionBy = By.cssSelector("paper-toast[title='Discussion created successfully']");
	
	@FindBy(id = "editDiscussion")
	private WebElement editDiscButton;
	private By editDiscButtonBy = By.id("editDiscussion");
	
	@FindBy(id = "deleteDiscussion")
	private WebElement deleteDiscButton;
	private By deleteDiscButtonBy = By.id("deleteDiscussion");
	
	@FindBy(css = "paper-button:nth-of-type(1)[class='style-scope confirmation-component x-scope paper-button-0']")
	private WebElement deleteYesDiscussion;
	private By deleteYesDiscussionBy = By.cssSelector("paper-button:nth-of-type(1)[class='style-scope confirmation-component x-scope paper-button-0']");
	
	@FindBy(css = "paper-button:nth-of-type(2)[class='style-scope confirmation-component x-scope paper-button-0']")
	private WebElement deleteNoDiscussion;
	private By deleteNoDiscussionBy = By.cssSelector("paper-button:nth-of-type(2)[class='style-scope confirmation-component x-scope paper-button-0']");
	
	@FindBy(css = "paper-toast[title='Discussion deleted successfully']")
	private WebElement deletedMsgDiscussion;
	private By deletedMsgDiscussionBy = By.cssSelector("paper-toast[title='Discussion deleted successfully']");
	
	@FindBy(css = "paper-toast[title='Discussion updated successfully']")
	private WebElement updatedMsgDiscussion;
	private By updatedMsgDiscussionBy = By.cssSelector("paper-toast[title='Discussion updated successfully']");
	
	//Docked
	@FindBy(css = "volute-app-grid[id='mashupGrid'] paper-fab[id^='addDiscussion']")//discussion-list > div:nth-of-type(1) > div:nth-of-type(1) > paper-button[id='newDiscussion'] > iron-icon
	private WebElement addNewDockDiscButton;
	private By addNewDockDiscButtonBy = By.cssSelector("volute-app-grid[id='mashupGrid'] paper-fab[id^='addDiscussion']");
	
	// Docked
	@FindBy(css = "volute-app-grid[id='mashupGrid'] paper-fab[id^='searchDiscussion']")//discussion-list > div:nth-of-type(1) > div:nth-of-type(1) > paper-button[id='newDiscussion'] > iron-icon
	private WebElement searchDockDiscButton;
	private By searchDockDiscButtonBy = By.cssSelector("volute-app-grid[id='mashupGrid'] paper-fab[id^='searchDiscussion']");
	
	// Docked
	@FindBy(xpath = "//volute-app-grid[@id='mashupGrid'] //input[@title='Search Discussion Input']")//newDiscussion discussion-list > div:nth-of-type(1) > div:nth-of-type(1) > paper-button[id='newDiscussion'] > iron-icon
	private WebElement searchDiscDockTextBox;
	private By searchDiscDockTextBoxBy = By.xpath("//volute-app-grid[@id='mashupGrid'] //input[@title='Search Discussion Input']");
	
	@FindBy(css = "volute-app-grid[id='mashupGrid'] paper-input[id='discussionName']")
	private WebElement discDockNameTextBox;
	private By discDockNameTextBoxBy = By.cssSelector("volute-app-grid[id='mashupGrid'] paper-input[id='discussionName']");
	
	@FindBy(css = "volute-app-grid[id='mashupGrid'] paper-button[id='saveDiscussion']")
	private WebElement saveDockDiscButton;
	private By saveDockDiscButtonBy = By.cssSelector("volute-app-grid[id='mashupGrid'] paper-button[id='saveDiscussion']");
	
	@FindBy(css = "volute-app-grid[id='mashupGrid'] paper-button[id='cancelDiscussion']")
	private WebElement cancelDockDiscButton;
	private By cancelDockDiscButtonBy = By.cssSelector("volute-app-grid[id='mashupGrid'] paper-button[id='cancelDiscussion']");
	
	@FindBy(css = "textarea[title='Discussion Message Input']")
	private WebElement typeMsgDisc;
	private By typeMsgDiscBy = By.cssSelector("textarea[title='Discussion Message Input']");
	
	@FindBy(css = "iron-icon[icon='icons:send']")
	private WebElement sendDiscButton;
	private By sendDiscButtonBy = By.cssSelector("iron-icon[icon='icons:send']");
	
	public Discussion()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	public void clickOnSearchDiscButton(String typeDisc)
	{
		if(typeDisc.equalsIgnoreCase("Float")){
			explicitWaitvisibilityOfElementLocated(searchDiscButtonBy);
			searchDiscButton.click();
		}
		else{
			//explicitWaitvisibilityOfElementLocated(addNewDockDiscButtonBy);
			//addNewDockDiscButton.click();
			javaScriptExecutorClick(searchDockDiscButtonBy);
		}
		
	}
	
	public void searchDisc(String typeDisc, String discName)
	{
		if(typeDisc.equalsIgnoreCase("Float")){
			javaScriptExecutorNoClick(searchDiscTextBoxBy);
			javaScriptExecutorWrite(searchDiscTextBoxBy, discName);
		}
		else {
			javaScriptExecutorNoClick(searchDiscDockTextBoxBy);
			javaScriptExecutorWrite(searchDiscDockTextBoxBy, discName);
		}
	}
	
	public void clickOnAddNewDiscButton(String typeDisc)
	{
		if(typeDisc.equalsIgnoreCase("Float")){
			explicitWaitvisibilityOfElementLocated(addNewDiscButtonBy);
			addNewDiscButton.click();
		}
		else{
			//explicitWaitvisibilityOfElementLocated(addNewDockDiscButtonBy);
			//addNewDockDiscButton.click();
			javaScriptExecutorClick(addNewDockDiscButtonBy);
		}
		
	}
	
	public void enterDiscNameTextBox(String typeDisc, String discName)
	{
		if(typeDisc.equalsIgnoreCase("Float")){
			//explicitWaitvisibilityOfElementLocated(discNameTextBoxBy);
			//Actions a1 = new Actions(SuperTestScript.driver);
			//a1.moveToElement(discNameTextBox).click().sendKeys(discName).perform();
			javaScriptExecutorNoClick(discNameTextBoxBy);
			javaScriptExecutorWrite(discNameTextBoxBy, discName);
		}
		else {
			//explicitWaitvisibilityOfElementLocated(discDockNameTextBoxBy);
			//Actions a1 = new Actions(SuperTestScript.driver);
			//a1.moveToElement(discDockNameTextBox).click().sendKeys(discName).perform();
			javaScriptExecutorNoClick(discDockNameTextBoxBy);
			javaScriptExecutorWrite(discDockNameTextBoxBy, discName);
		}
	}
	
	public void clickOnAssignGroupsOrIndividualsButton(String typeDisc)
	{
		if(typeDisc.equalsIgnoreCase("Float")){
			explicitWaitvisibilityOfElementLocated(assignGroupsOrIndividualsButtonBy);
			assignGroupsOrIndividualsButton.click();
		}
		
	}
	
	public void clickOnSaveDiscButton(String typeDisc)
	{
		if(typeDisc.equalsIgnoreCase("Float")){
			explicitWaitvisibilityOfElementLocated(saveDiscButtonBy);
			saveDiscButton.click();
		}
		else {
			//explicitWaitvisibilityOfElementLocated(saveDockDiscButtonBy);
			//saveDockDiscButton.click();
			javaScriptExecutorClick(saveDockDiscButtonBy);
		}
	}
	
	public void clickOnCancelDiscButton(String typeDisc)
	{
		if(typeDisc.equalsIgnoreCase("Float")){
			explicitWaitvisibilityOfElementLocated(cancelDiscButtonBy);
			cancelDiscButton.click();
		}
		else {
			//explicitWaitvisibilityOfElementLocated(cancelDockDiscButtonBy);
			//cancelDockDiscButton.click();
			javaScriptExecutorClick(cancelDockDiscButtonBy);
		}
	}
	
	public void clickOnEditFloatDiscButton(String disc)
	{	
		WebElement dis = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-button[title='Edit Discussion "+ disc +"']"));
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-icon-button[title='Edit Discussion "+ disc +"']"));
		dis.click();
	}
	
	public void clickOnEditDockDiscButton(String disc)
	{	
		//WebElement dis = SuperTestScript.driver.findElement(By.cssSelector("volute-app-grid[id='mashupGrid'] paper-icon-button[title='Edit Discussion "+ disc +"']"));
		//explicitWaitvisibilityOfElementLocated(By.cssSelector("volute-app-grid[id='mashupGrid'] paper-icon-button[title='Edit Discussion "+ disc +"']"));
		//dis.click();
		javaScriptExecutorClick(By.cssSelector("volute-app-grid[id='mashupGrid'] paper-icon-button[title='Edit Discussion "+ disc +"']"));
	}
	
	public void clickOnEditDiscButton(String typeDisc, String disc)
	{	
		if(typeDisc.equalsIgnoreCase("Float")){
			clickOnEditFloatDiscButton(disc);
		}
		else{
			clickOnEditDockDiscButton(disc);
		}
	}
	
	public void clickOndeleteFloatDiscButton(String del)
	{
		WebElement dis = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-button[title='Delete Discussion "+ del +"']"));
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-icon-button[title='Delete Discussion "+ del +"']"));
		dis.click();	
		//javaScriptExecutorClick(By.cssSelector("paper-icon-button[title='Delete Discussion "+ del +"']"));
	}
	
	public void clickOnDockdeleteDiscButton(String del)
	{
		//WebElement dis = SuperTestScript.driver.findElement(By.cssSelector("volute-app-grid[id='mashupGrid'] paper-icon-button[title='Delete Discussion "+ del +"']"));
		//explicitWaitvisibilityOfElementLocated(By.cssSelector("volute-app-grid[id='mashupGrid'] paper-icon-button[title='Delete Discussion "+ del +"']"));
		//dis.click();	
		javaScriptExecutorClick(By.cssSelector("volute-app-grid[id='mashupGrid'] paper-icon-button[title='Delete Discussion "+ del +"']"));
	}
	
	public void clickOnDeleteDiscButton(String typeDisc, String disc)
	{	
		if(typeDisc.equalsIgnoreCase("Float")){
			clickOndeleteFloatDiscButton(disc);
		}
		else{
			clickOnDockdeleteDiscButton(disc);
		}
	}
	
	public void clickOnDeleteYesDiscussion()
	{
		explicitWaitvisibilityOfElementLocated(deleteYesDiscussionBy);
		deleteYesDiscussion.click();
	}
	
	public void clickOnDeleteNoDiscussion()
	{
		explicitWaitvisibilityOfElementLocated(deleteNoDiscussionBy);
		deleteNoDiscussion.click();
	}
	
	public String fetchAddedMsgDiscussion()
	{	
		explicitWaitvisibilityOfElementLocated(addedMsgDiscussionBy);
		String msg = addedMsgDiscussion.getText();
		return msg;
	}
	
	public void ClickDiscussion(String typeDisc, String discName)
	{	
		if(typeDisc.equalsIgnoreCase("Float")){
			javaScriptExecutorClick(By.cssSelector("paper-icon-item[title='Open Discussion "+discName+"']"));
		}else{
			javaScriptExecutorClick(By.cssSelector("volute-app-grid paper-icon-item[title='Open Discussion "+discName+"']"));
		}
	}
	
	/*public String fetchFloatDiscussionShows(String disShows)
	{	
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//div[text()[contains(.,'"+disShows+"')]]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchDiscussionShows(String typeDisc, String disShows)
	{	
		if(typeDisc.equalsIgnoreCase("Float")){
			return fetchDockDiscussionShows(disShows);
		}
		else{
			return fetchFloatDiscussionShows(disShows);
		}
	}*/
	
	public String fetchDiscussionDeleteMsg()
	{	
		explicitWaitvisibilityOfElementLocated(deletedMsgDiscussionBy);
		String msg = deletedMsgDiscussion.getText();
		return msg;
	}
	
	public String fetchUpdatedMsgDiscussion()
	{	
		explicitWaitvisibilityOfElementLocated(updatedMsgDiscussionBy);
		String msg = updatedMsgDiscussion.getText();
		return msg;
	}
	
	public void selectGroupsToAddToDiscussion()
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-icon-item[class='volute-app-rooms-list-item style-scope volute-app-assignment-face1 x-scope paper-icon-item-4']"));
		List<WebElement> groups = SuperTestScript.driver.findElements(By.cssSelector("paper-icon-item[class='volute-app-rooms-list-item style-scope volute-app-assignment-face1 x-scope paper-icon-item-4']"));
		for(int i=0; i<=groups.size()-1; i++)
		{
			groups.get(i).click();
		}

	}
	
	public void selectIndividualsToAddToDiscussion()
	{		
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-icon-item[class='style-scope assign-module x-scope paper-icon-item-0']"));
		List<WebElement> groups = SuperTestScript.driver.findElements(By.cssSelector("paper-icon-item[class='style-scope assign-module x-scope paper-icon-item-0']"));
		for(int i=0; i<=groups.size()-1; i++)
		{
			groups.get(i).click();
		}

	}
	
	public void clickOnAssignButton()
	{
		explicitWaitvisibilityOfElementLocated(assignButtonBy);
		assignButton.click();
		
	}
	
	public void clickOnDisCloseflButton(String typeDisc)
	{
		if(typeDisc.equalsIgnoreCase("Float")){
			explicitWaitvisibilityOfElementLocated(disCloseFlBy);
			disCloseFl.click();
		}
		
	}
	
	public String fetchDiscussionShows(String typeDisc, String disShows)
	{	
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		if(typeDisc.equalsIgnoreCase("Float")){
			try {
				SuperTestScript.driver.findElement(By.cssSelector("paper-icon-item[title='Open Discussion "+disShows+"']"));
				return "Found";
			}
			catch(NoSuchElementException e){
				return "Not Found";
			}
		}else{
			SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			try {
				SuperTestScript.driver.findElement(By.cssSelector("volute-app-grid paper-icon-item[title='Open Discussion "+disShows+"']"));
				return "Found"; //volute-app-grid[id='mashupGrid'] paper-icon-item[title='Discussion "+disShows+"']
			}
			catch(NoSuchElementException e){
				return "Not Found";
			}
		}
	}
	
	public void enterDiscMessage(String msg)
	{
		explicitWaitvisibilityOfElementLocated(typeMsgDiscBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(typeMsgDisc).click().sendKeys(msg).perform();
		
		//int desc_size = SuperTestScript.driver.findElements(anDescriptionBy).size();
		//SuperTestScript.driver.findElements(anDescriptionBy).get(desc_size-1).sendKeys(anDes);
	}

	public void clickSendDiscButton()
	{
		explicitWaitvisibilityOfElementLocated(sendDiscButtonBy);
		sendDiscButton.click();
		
	}
	
	public String fetchMsgDiscussion(String msg)
	{	
		WebElement dis = SuperTestScript.driver.findElement(By.cssSelector("//div[contains(text(),'"+msg+"')]"));
		explicitWaitvisibilityOfElementLocated(By.cssSelector("//div[contains(text(),'"+msg+"')]"));
		String answer = dis.getText();
		return answer;
	}
}

	

