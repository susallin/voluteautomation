package education.volute.pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import education.volute.common_lib.SuperTestScript;

public class ManageActivities extends NavigatorTool

{
	@FindBy(css = "paper-fab[title='Add Program Activity']")
	private WebElement addActButton;	
	private By addActButtonBy = By.cssSelector("paper-fab[title='Add Program Activity']");
	
	/*@FindBy(id = "activityName")
	private WebElement actNameTextBox;
	private By actNameTextBoxBy = By.id("activityName");*/
	
	@FindBy(css = "activity-upsert input[title='Learning Space Name']")
	private WebElement actNameTextBox;
	private By actNameTextBoxBy = By.cssSelector("activity-upsert input[title='Learning Space Name']");
	
	
	@FindBy(css = "activity-upsert paper-textarea[title='Learning Space Description']")
	private WebElement actDescriptionTextBox;
	private By actDescriptionTextBoxBy = By.cssSelector("activity-upsert paper-textarea[title='Learning Space Description']");
			
	@FindBy(css = "activity-upsert vaadin-date-picker[label='Start Date']")
	private WebElement actStartDateTextBox;
	private By actStartDateTextBoxBy = By.cssSelector("activity-upsert vaadin-date-picker[label='Start Date']");
			
	@FindBy(css = "activity-upsert vaadin-date-picker[label='End Date']")
	private WebElement actEndDateTextBox;
	private By actEndDateTextBoxBy = By.cssSelector("activity-upsert vaadin-date-picker[label='End Date']");
			
	@FindBy(xpath = "//paper-button[text()='Assign Mashup']")
	private WebElement assignLSButton;
	private By assignLSButtonBy = By.xpath("//paper-button[text()='Assign Mashup']");
	
	@FindBy(css = "div[class='mashup-summary-assign style-scope']>paper-button[role='button']")
	private WebElement assignLsButton;
	private By assignLsButtonBy = By.cssSelector("div[class='mashup-summary-assign style-scope']>paper-button[role='button']");
	
	@FindBy(css = "paper-radio-button[name='private']")
	private WebElement privateButton;
	private By privateButtonBy = By.cssSelector("paper-radio-button[name='private']");
	
	@FindBy(css = "paper-radio-button[name='social']")
	private WebElement socialButton;
	private By socialButtonBy = By.cssSelector("paper-radio-button[name='social']");
	
	@FindBy(css = "paper-radio-button[name='open']")
	private WebElement openButton;
	private By openButtonBy = By.cssSelector("paper-radio-button[name='open']");
	
	@FindBy(css = "paper-radio-button[name='exclusive']")
	private WebElement exclusiveButton;
	private By exclusiveButtonBy = By.cssSelector("paper-radio-button[name='exclusive']");
		
	@FindBy(css = "paper-radio-button[name='unpublished']")
	private WebElement unpublishButton;
	private By unpublishButtonBy = By.cssSelector("paper-radio-button[name='unpublished']");
	
	@FindBy(css = "paper-radio-button[name='published']")
	private WebElement publishButton;
	private By publishButtonBy = By.cssSelector("paper-radio-button[name='published']");
	
	/*
	@FindBy(xpath = "/html/body/volute-container/neon-animated-pages/volute-app-fullsize/div[2]/div/volute-app/div[1]/div/neon-animated-pages/neon-animatable[1]/volute-app-face/roster-face2/div[2]/activities-panel/div/div/div/iron-selector/paper-item/activity-summary/div[2]/activity-upsert/form/div[6]/div/div/paper-toggle-button/div[1]/div[1]")//"/html/body/volute-container/neon-animated-pages/volute-app-fullsize/div[2]/div/volute-app/div[1]/div/neon-animated-pages/neon-animatable[2]/volute-app-face/roster-face2/div[2]/activities-panel/div/div/div/activity-upsert/form/div[6]/div/div/paper-toggle-button/div[1]/div[2]")
	private WebElement publishToggleButton;
	private By publishToggleButtonBy = By.xpath("/html/body/volute-container/neon-animated-pages/volute-app-fullsize/div[2]/div/volute-app/div[1]/div/neon-animated-pages/neon-animatable[1]/volute-app-face/roster-face2/div[2]/activities-panel/div/div/div/iron-selector/paper-item/activity-summary/div[2]/activity-upsert/form/div[6]/div/div/paper-toggle-button/div[1]/div[1]");
	*/
	
	@FindBy(xpath = "/html/body/volute-container/neon-animated-pages/volute-app-fullsize/div[2]/div/volute-app/div[1]/div/neon-animated-pages/neon-animatable[2]/volute-app-face/roster-face2/div[2]/activities-panel/div/div/div/iron-selector/paper-item[1]/activity-summary/div[2]/activity-upsert/form/div[6]/div/div/paper-toggle-button/div[1]")
	private WebElement unpublishToggleButton;
	private By unpublishToggleButtonBy = By.xpath("/html/body/volute-container/neon-animated-pages/volute-app-fullsize/div[2]/div/volute-app/div[1]/div/neon-animated-pages/neon-animatable[2]/volute-app-face/roster-face2/div[2]/activities-panel/div/div/div/iron-selector/paper-item[1]/activity-summary/div[2]/activity-upsert/form/div[6]/div/div/paper-toggle-button/div[1]");
	
	@FindBy(css = "paper-button[title='Assign Users']")
	private WebElement assignUsersButton;
	private By assignUsersButtonBy = By.cssSelector("paper-button[title='Assign Users']");
	
	@FindBy(css = "assign-module > paper-button")
	private WebElement assignUsersDoneButton;
	private By assignUsersDoneButtonBy = By.cssSelector("assign-module > paper-button");
	
	/*
	@FindBy(xpath = "//*[@id='dialog']/neon-animated-pages/assign-module/paper-button")
	private WebElement assignUsersDoneButton;
	private By assignUsersDoneButtonBy = By.xpath("//*[@id='dialog']/neon-animated-pages/assign-module/paper-button");
	*/
	
	@FindBy(css = "section[id='overviewSection'] paper-button[title='Cancel Learning Space']")
	private WebElement cancelActButton;
	private By cancelActButtonBy = By.cssSelector("section[id='overviewSection'] paper-button[title='Cancel Learning Space']");
	
	@FindBy(css = "section[id='overviewSection'] paper-button[title='Save Learning Space']")
	private WebElement saveActButton;
	private By saveActButtonBy = By.cssSelector("section[id='overviewSection'] paper-button[title='Save Learning Space']");
	
	@FindBy(css = "paper-button[title='Cancel']")
	private WebElement cancelAssignUserButton;
	private By cancelAssignUserButtonBy = By.cssSelector("paper-button[title='Cancel']");
	
	@FindBy(css = "paper-button[title='Assign Selected Users and Groups']")
	private WebElement confirmAssignUserButton;
	private By confirmAssignUserButtonBy = By.cssSelector("paper-button[title='Assign Selected Users and Groups']");
	
	@FindBy(css = "paper-toast[title = 'Activity created successfully']>span[id='label'][class='style-scope paper-toast']")
	private WebElement actSavedMsg;
	private By actSavedMsgBy = By.cssSelector("paper-toast[title = 'Activity created successfully']>span[id='label'][class='style-scope paper-toast']");
	

	@FindBy(xpath = "//paper-button[text()='Yes']")
	private WebElement confirmPopupYesButton;
	private By confirmPopupYesButtonBy = By.xpath("//paper-button[text()='Yes']");
	
	@FindBy(xpath = "//paper-button[text()='No']")
	private WebElement confirmPopupNoButton;
	private By confirmPopupNoButtonBy = By.xpath("//paper-button[text()='No']");

	@FindBy(css = "paper-toast[title = 'Activity updated successfully']>span[id='label'][class='style-scope paper-toast']")
	private WebElement actEditsSavedMsg;
	private By actEditsSavedMsgBy = By.cssSelector("paper-toast[title = 'Activity updated successfully']>span[id='label'][class='style-scope paper-toast']");
		
	@FindBy(css = "paper-toast[title = 'Activity deleted successfully']>span[id='label'][class='style-scope paper-toast']")
	private WebElement actDeletedMsg;
	private By actDeleteddMsgBy = By.cssSelector("paper-toast[title = 'Activity deleted successfully']>span[id='label'][class='style-scope paper-toast']");
	
	@FindBy(css = "paper-fab[title='Back to Learning Spaces List']")
	private WebElement actListBackButton;
	private By actListBackButtonBy = By.cssSelector("paper-fab[title='Back to Learning Spaces List']");
	
	@FindBy(css = "paper-icon-button[id='openActivityInfo']")
	private WebElement actInfoButton;
	private By actInfoButtonBy = By.cssSelector("paper-icon-button[id='openActivityInfo']");
	
	@FindBy(css = "paper-icon-button[title='View More Info']")
	private WebElement actViewMoreButton;
	private By actViewMoreButtonBy = By.cssSelector("paper-icon-button[title='View More Info']");
	
	@FindBy(css = "paper-fab[aria-label='Exit Information']")
	private WebElement actExitInfoButton;
	private By actExitInfoButtonBy = By.cssSelector("paper-fab[aria-label='Exit Information']");
	
	@FindBy(css = "paper-button[id='prev']")
	private WebElement actPrevButton;
	private By actPrevButtonBy = By.cssSelector("paper-button[id='prev']");
	
	@FindBy(css = "paper-button[id='next']")
	private WebElement actNextButton;
	private By actNextButtonBy = By.cssSelector("paper-button[id='next']");
	
	@FindBy(css = "input[title='Number of copies (Max 10)']")
	private WebElement enterCopy;
	private By enterCopyBy = By.cssSelector("input[title='Number of copies (Max 10)']");
	
	@FindBy(css = "paper-radio-button[name='yes']")
	private WebElement copyWithContent;
	private By copyWithContentBy = By.cssSelector("paper-radio-button[name='yes']");
	
	@FindBy(css = "paper-radio-button[name='no']")
	private WebElement copyWithNoContent;
	private By copyWithNoContentBy = By.cssSelector("paper-radio-button[name='no']");
	
	@FindBy(css = "activity-cloning paper-button[class^='cancel-button']")
	private WebElement cancelCopy;
	private By cancelCopyBy = By.cssSelector("activity-cloning paper-button[class^='cancel-button']");
	
	@FindBy(css = "activity-cloning paper-button[class^='save-button']")
	private WebElement saveCopy;
	private By saveCopyBy = By.cssSelector("activity-cloning paper-button[class^='save-button']");
	
	public ManageActivities()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	public void SelProgram(String pgmName)
	{
		WebElement pgm = SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'" + pgmName + "')]"));
    	pgm.click();
	}
	
	public void clickOnActListBackButton()
	{
		explicitWaitvisibilityOfElementLocated(actListBackButtonBy);
		actListBackButton.click();
	}
	
	public String fetchActShows(String actShows)
	{
		try {
			SuperTestScript.driver.findElement(By.xpath("//div[text()='"+actShows+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnAddActButton()
	{
		explicitWaitvisibilityOfElementLocated(addActButtonBy);
		addActButton.click();
	}
	
	public void closeArrowSub(String actName){
		WebElement buttonArrowSub = SuperTestScript.driver.findElement(By.xpath("//div[text()='"+actName+"']/../../paper-icon-button[@icon='icons:expand-less']"));//@icon='icons:expand-less'
		buttonArrowSub.click();
	}
	
	public String fetchArrowSub(String actName)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//div[text()='"+actName+"']/../../paper-icon-button[@icon='icons:expand-more'][@style='display: none;']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnArrowSub(String actName){
		WebElement buttonArrowSub = SuperTestScript.driver.findElement(By.xpath("//div[text()='"+actName+"']/../../paper-icon-button[@icon='icons:expand-more']"));
		buttonArrowSub.click();
	}
	
	public String fetchActArrow(String actName){
		try {
			SuperTestScript.driver.findElement(By.xpath("//div[text()='"+actName+"']/../../paper-icon-button[@icon='icons:expand-more']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void dragAct(String actName, String actName2){
		dragAndDrop(By.xpath("//div[text()='"+actName+"']/../../paper-icon-button[@icon='icons:menu']"),
				By.xpath("//div[text()='"+actName2+"']/../../paper-icon-button[@icon='icons:menu']"));
	}
	
	public void clickOnCloneAct(String actName){
		WebElement buttonClone = SuperTestScript.driver.findElement(By.xpath("//div[text()='"+actName+"']/../../paper-icon-button[@id='activityClone']"));
		buttonClone.click();
	}
	
	public void enterCopy(String numCopy)
	{
		explicitWaitvisibilityOfElementLocated(enterCopyBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(enterCopy).click().sendKeys(numCopy).perform();
		
	}
	
	public void clickOnCopyWithContent()
	{
		explicitWaitvisibilityOfElementLocated(copyWithContentBy);
		copyWithContent.click();
	}
	
	public void clickOnCopyWithNoContent()
	{
		explicitWaitvisibilityOfElementLocated(copyWithNoContentBy);
		copyWithNoContent.click();
	}
	
	public void clickOnCancelCopy()
	{
		explicitWaitvisibilityOfElementLocated(cancelCopyBy);
		cancelCopy.click();
	}
	
	public void clickOnSaveCopy()
	{
		explicitWaitvisibilityOfElementLocated(saveCopyBy);
		saveCopy.click();
	}
	
	public void clickOnEditAct(String actName){
		explicitWaitvisibilityOfElementLocated(By.xpath("//div[text()='"+actName+"']/../../paper-icon-button[@id='activityEdit']"));
		javaScriptExecutor(By.xpath("//div[text()='"+actName+"']/../../paper-icon-button[@id='activityEdit']"));
		
		//WebElement buttonEdit = SuperTestScript.driver.findElement(By.xpath("//div[text()='"+actName+"']/../../paper-icon-button[@id='activityEdit']"));
		//buttonEdit.click();
		
	}
	
	public void clickOnDeleteAct(String actName){
		explicitWaitvisibilityOfElementLocated(By.xpath("//div[text()='"+actName+"']/../../paper-icon-button[@id='activityDelete']"));
		javaScriptExecutor(By.xpath("//div[text()='"+actName+"']/../../paper-icon-button[@id='activityDelete']"));
		
		//WebElement buttonDelete = SuperTestScript.driver.findElement(By.xpath("//div[text()='"+actName+"']/../../paper-icon-button[@id='activityDelete']"));
		//buttonDelete.click();
	}
	
	public void clickSelectedSubAct(String actName){
		explicitWaitvisibilityOfElementLocated(By.xpath("//div[text()='"+actName+"']"));
		javaScriptExecutor(By.xpath("//div[text()='"+actName+"']"));
		
		//WebElement buttonSelect = SuperTestScript.driver.findElement(By.xpath("//div[text()='"+actName+"']"));
		//buttonSelect.click();
	}
	
	public void clickOnAddSubAct(String actName){
		explicitWaitvisibilityOfElementLocated(By.xpath("//div[text()='"+actName+"']/../../paper-icon-button[@id='activityAddSub']"));
		//WebElement buttonAdd = SuperTestScript.driver.findElement(By.xpath("//div[text()='"+actName+"']/../../paper-icon-button[@id='activityAddSub']"));
		//buttonAdd.click();
		javaScriptExecutor(By.xpath("//div[text()='"+actName+"']/../../paper-icon-button[@id='activityAddSub']"));
	}
	
	public void enterEditActName(String actName, String actName2)
	{
		WebElement nameAc = SuperTestScript.driver.findElement(By.cssSelector("paper-item[title='"+ actName +"'] > activity-summary > div:nth-of-type(2) > activity-upsert > form[id='saveActivityForm'] > paper-input[label='Activity Name']"));
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(nameAc).click().sendKeys(actName2).perform();
	}
	
	public void enterActName(String actName)
	{
		explicitWaitvisibilityOfElementLocated(actNameTextBoxBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(actNameTextBox).click().sendKeys(actName).perform();
		
	}
	
	public void enterEditActDesc(String actName, String actDesc)
	{
		WebElement descAc = SuperTestScript.driver.findElement(By.cssSelector("paper-item[title='"+ actName +"'] > activity-summary > div:nth-of-type(2) > activity-upsert > form[id='saveActivityForm'] > paper-textarea[title='Activity Description']"));
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(descAc).click().sendKeys(actDesc).perform();
	}
	
	public void enterActDesc(String actDesc)
	{
		explicitWaitvisibilityOfElementLocated(actDescriptionTextBoxBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(actDescriptionTextBox).click().sendKeys(actDesc).perform();
		
	}
	
	public void clickActStartDateBox() 
	{
		explicitWaitvisibilityOfElementLocated(actStartDateTextBoxBy);
		actStartDateTextBox.click();
	}
	
	public void selectActStartDateNum(String dateNum)
	{
		/*WebElement pgm = SuperTestScript.driver.findElement(By.cssSelector("vaadin-date-picker[name='startDate'][label='Start Date'][title='Program Start Date'] > iron-dropdown > div > vaadin-date-picker-overlay > div[id='scrollers'] > vaadin-infinite-scroller[id='scroller'] > div > div:nth-of-type(2) > div:nth-of-type(2) > vaadin-month-calendar > div > div[aria-label='"+dateNum+"']"));
		explicitWaitvisibilityOfElementLocated(By.cssSelector("vaadin-date-picker[name='startDate'][label='Start Date'][title='Program Start Date'] > iron-dropdown > div > vaadin-date-picker-overlay > div[id='scrollers'] > vaadin-infinite-scroller[id='scroller'] > div > div:nth-of-type(2) > div:nth-of-type(2) > vaadin-month-calendar > div > div[aria-label='"+dateNum+"']"));//vaadin-date-picker[title='Program Start Date']"+" "+"paper-item[style='"+dateNum+"']"));
		pgm.click();*/
		javaScriptExecutorClick(By.cssSelector("activity-upsert vaadin-date-picker[label='Start Date'] div[aria-label='"+ dateNum +"']"));
	}
	
	public void clickActEndDateBox()
	{
		explicitWaitvisibilityOfElementLocated(actEndDateTextBoxBy);
		actEndDateTextBox.click();
	}
	
	public void selectActEndDateNum(String dateNum)
	{
		/*WebElement pgm = SuperTestScript.driver.findElement(By.cssSelector("vaadin-date-picker[name='startDate'][label='Start Date'][title='Program Start Date'] > iron-dropdown > div > vaadin-date-picker-overlay > div[id='scrollers'] > vaadin-infinite-scroller[id='scroller'] > div > div:nth-of-type(2) > div:nth-of-type(2) > vaadin-month-calendar > div > div[aria-label='"+dateNum+"']"));
		explicitWaitvisibilityOfElementLocated(By.cssSelector("vaadin-date-picker[name='startDate'][label='Start Date'][title='Program Start Date'] > iron-dropdown > div > vaadin-date-picker-overlay > div[id='scrollers'] > vaadin-infinite-scroller[id='scroller'] > div > div:nth-of-type(2) > div:nth-of-type(2) > vaadin-month-calendar > div > div[aria-label='"+dateNum+"']"));//vaadin-date-picker[title='Program Start Date']"+" "+"paper-item[style='"+dateNum+"']"));
		pgm.click();*/
		javaScriptExecutorClick(By.cssSelector("activity-upsert vaadin-date-picker[label='End Date'] div[aria-label='"+ dateNum +"']"));
	}
	
	public void clickOnEditAssignLSButton(String actLs)
	{
		WebElement buttonLs = SuperTestScript.driver.findElement(By.xpath("//paper-item[@title='"+ actLs +"']/activity-summary/div[2]/activity-upsert/form/div[2]/paper-button[text()='Assign Mashup']"));
		buttonLs.click();
	}
	
	public void clickOnEditUnAssignLSButton(String actLs)
	{
		WebElement buttonLs = SuperTestScript.driver.findElement(By.xpath("//paper-item[@title='"+ actLs +"']/activity-summary/div[2]/activity-upsert/form/div[2]/paper-button[text()='Unassign']"));
		buttonLs.click();
	}
	
	public void clickOnAssignLSButton()
	{
		explicitWaitvisibilityOfElementLocated(assignLSButtonBy);
		assignLSButton.click();
	}
	
	public void clickOnAssignUsersDoneButton()
	{
		explicitWaitvisibilityOfElementLocated(assignUsersDoneButtonBy);
		assignUsersDoneButton.click();
	}
	
	
	public void clickOnArrowToAssignLS(String lsName)
	{
		WebElement arrowLs = SuperTestScript.driver.findElement(By.cssSelector("paper-item-body[title='"+lsName+"']>div[class='mashup-summary-body-child style-scope mashup-summary-assign']>paper-icon-button[role='button']"));
		arrowLs.click();
	}
	
	public void clickOnAssign()
	{
		explicitWaitvisibilityOfElementLocated(assignLsButtonBy);
		assignLsButton.click();
	}
	
	/** name for toggle buttons - social, private, open, exclusive, unpublished, published */
	public void clickOnTogglePublish(String actTog)
	{
		WebElement buttonPublish = SuperTestScript.driver.findElement(By.cssSelector("paper-radio-button[name='"+actTog+"']"));
		buttonPublish.click();
	}
	
	public void clickAssignUserButon()
	{
		explicitWaitvisibilityOfElementLocated(assignUsersButtonBy);
		assignUsersButton.click();
	}
	
	public void clickCancelAssignUserButton()
	{
		explicitWaitvisibilityOfElementLocated(cancelAssignUserButtonBy);
		cancelAssignUserButton.click();
	}
	
	public void clickConfirmAssignUserButton()
	{
		explicitWaitvisibilityOfElementLocated(confirmAssignUserButtonBy);
		confirmAssignUserButton.click();
	}
	
	public void clickOnCheckboxButton(String actTog)
	{
		WebElement buttonCheckbox = SuperTestScript.driver.findElement(By.cssSelector("paper-checkbox[title='"+actTog+"']"));
		buttonCheckbox.click();
	}
	
	public void clickOnEditSocialToggleButton(String actSoc)
	{
		WebElement buttonSoc = SuperTestScript.driver.findElement(By.cssSelector("paper-item[title='"+ actSoc +"'] > activity-summary > div:nth-of-type(2) > activity-upsert > form[id='saveActivityForm'] > div:nth-of-type(4) > div:nth-of-type(3) > div:nth-of-type(1) > paper-toggle-button > div:nth-of-type(1) > div[id='toggleButton']"));
		buttonSoc.click();
	}
	
	public void clickOnPrivateButton()
	{
		explicitWaitvisibilityOfElementLocated(privateButtonBy);
		privateButton.click();
	}
	
	public void clickOnSocialButton()
	{
		explicitWaitvisibilityOfElementLocated(socialButtonBy);
		socialButton.click();
	}
	
	public void clickOnEditExclusiveToggleButton(String actExc)
	{
		WebElement buttonExc = SuperTestScript.driver.findElement(By.cssSelector("paper-item[title='"+ actExc +"'] > activity-summary > div:nth-of-type(2) > activity-upsert > form[id='saveActivityForm'] > div:nth-of-type(4) > div:nth-of-type(3) > div:nth-of-type(2) > paper-toggle-button > div:nth-of-type(1) > div[id='toggleButton']"));
		buttonExc.click();
	}
	
	public void clickOnOpenButton()
	{
		explicitWaitvisibilityOfElementLocated(openButtonBy);
		openButton.click();
	}
	
	public void clickOnExclusiveButton()
	{
		explicitWaitvisibilityOfElementLocated(exclusiveButtonBy);
		exclusiveButton.click();
	}
	
	public void clickOnEditPublishToggleButton(String actName)
	{
		WebElement arrowAc = SuperTestScript.driver.findElement(By.cssSelector("paper-item[title='"+ actName +"'] > activity-summary > div:nth-of-type(2) > activity-upsert > form[id='saveActivityForm'] > div:nth-of-type(8) > div > div > paper-toggle-button > div:nth-of-type(1) > div[id='toggleButton']"));
		arrowAc.click();
	}
	
	public void clickOnUnpublishButton()
	{
		explicitWaitvisibilityOfElementLocated(unpublishButtonBy);
		unpublishButton.click();
	}
	
	public void clickOnPublishButton()
	{
		explicitWaitvisibilityOfElementLocated(publishButtonBy);
		publishButton.click();
	}
	
	public void clickOnUnpublishToggleButton()
	{
		explicitWaitvisibilityOfElementLocated(unpublishToggleButtonBy);
		unpublishToggleButton.click();
	}
	
	public void clickOnEditSaveAct(String actSave)
	{
		WebElement buttonSave = SuperTestScript.driver.findElement(By.cssSelector("paper-item[title='"+ actSave +"'] > activity-summary > div:nth-of-type(2) > activity-upsert > form > div:nth-of-type(9) > paper-button[title='Save Activity']"));
		buttonSave.click();
	}
	
	public void clickOnSaveAct()
	{
		javaScriptExecutorNoClick(saveActButtonBy);
		javaScriptExecutorClick(saveActButtonBy);
		//explicitWaitvisibilityOfElementLocated(saveActButtonBy);
		//saveActButton.click();
	}
	
	public String fetchSuccessMsg()
	{
		
		explicitWaitvisibilityOfElementLocated(actSavedMsgBy);
		String msg = actSavedMsg.getText();
		return msg;
	}
	
	public String fetchActEditsSavedSuccessMsg()
	{
		
		explicitWaitvisibilityOfElementLocated(actEditsSavedMsgBy);
		String msg = actEditsSavedMsg.getText();
		return msg;
	}
	
	public String fetchActDeletedMsg()
	{
		
		explicitWaitvisibilityOfElementLocated(actDeleteddMsgBy);
		String msg = actDeletedMsg.getText();
		return msg;
	}
	
	public void clickOnAct(String actName)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//span[contains(text(),'" + actName + "')]"));
		WebElement selectAct = SuperTestScript.driver.findElement(By.xpath("//span[contains(text(),'" + actName + "')]"));
    	selectAct.click();
	}
		
//	public void clickOnAddSubAct()
//	{
//		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-icon-button[title='Add Sub Activity']"));
//		WebElement addSubActButton = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-button[title='Add Sub Activity']"));
//		addSubActButton.click();
//	}
	
	public void selectAllindividuals()
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-icon-item[class='style-scope assign-module x-scope paper-icon-item-0']"));
		List<WebElement> allIndividuals = SuperTestScript.driver.findElements(By.cssSelector("paper-icon-item[class='style-scope assign-module x-scope paper-icon-item-0']"));
		
		for(int i=0; i<=allIndividuals.size()-7; i++)
		{
			allIndividuals.get(i).click();
		}
	}
	
	public void clickOnselectedActivity(String actName)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//div/span[contains(text(),'"+actName+"')]"));
		WebElement selectAct = SuperTestScript.driver.findElement(By.xpath("//div/span[contains(text(),'"+actName+"')]"));
		selectAct.click();
	}
	
	public void clickOnselectedSubActivityTwo(String subActName)
	{
		WebElement buttonSelect = SuperTestScript.driver.findElement(By.cssSelector("paper-listbox[id='activityList'] > paper-item[title='"+ subActName +"']"));
		buttonSelect.click();
	}
	
	public void clickOnselectedSubActivity(String subActName)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//div/span[contains(text(),'"+subActName+"')]"));
		WebElement selectSubAct = SuperTestScript.driver.findElement(By.xpath("//div/span[contains(text(),'"+subActName+"')]"));
		selectSubAct.click();
	}
		
		
		public void clickOnEditActicon(String actName)
		{
			explicitWaitvisibilityOfElementLocated(By.xpath("//div/span[contains(text(),'"+actName+"')]/../../..//paper-icon-button[@title='Edit Activity']"));
			WebElement editActIcon = SuperTestScript.driver.findElement(By.xpath("//div/span[contains(text(),'"+actName+"')]/../../..//paper-icon-button[@title='Edit Activity']"));
	    	editActIcon.click();
		}
		public void clickOnEditSubActicon(String subActName)
		{
			explicitWaitvisibilityOfElementLocated(By.xpath("//div/span[contains(text(),'"+subActName+"')]/../../..//paper-icon-button[@title='Edit Activity']"));
			WebElement editSubActIcon = SuperTestScript.driver.findElement(By.xpath("//div/span[contains(text(),'"+subActName+"')]/../../..//paper-icon-button[@title='Edit Activity']"));
	    	editSubActIcon.click();
		}
		
		public void clickOnDeleteActicon(String actName)
		{
			explicitWaitvisibilityOfElementLocated(By.xpath("//div/span[contains(text(),'"+actName+"')]/../../..//paper-icon-button[@title='Delete Activity']"));
			WebElement deleteActIcon = SuperTestScript.driver.findElement(By.xpath("//div/span[contains(text(),'"+actName+"')]/../../..//paper-icon-button[@title='Delete Activity']"));
	    	deleteActIcon.click();
		}
		
		public void clickOnAddSubActicon(String actName)
		{
			explicitWaitvisibilityOfElementLocated(By.xpath("//div/span[contains(text(),'"+actName+"')]/../../..//paper-icon-button[@title='Add Sub Activity']"));
			WebElement addSubActIcon = SuperTestScript.driver.findElement(By.xpath("//div/span[contains(text(),'"+actName+"')]/../../..//paper-icon-button[@title='Add Sub Activity']"));
	    	addSubActIcon.click();
		}
		
		public void clickOnConfirmPopupYesButton()
		{
			explicitWaitvisibilityOfElementLocated(confirmPopupYesButtonBy);
			confirmPopupYesButton.click();
			
		}
		
		public void clickOnConfirmPopupNoButton()
		{
			explicitWaitvisibilityOfElementLocated(confirmPopupNoButtonBy);
			confirmPopupNoButton.click();
			
		}
		
		public void clearActName()
		{
			explicitWaitvisibilityOfElementLocated(actNameTextBoxBy);
			actNameTextBox.clear();
		}
		
		public void clickOnActInfoButton()
		{
			explicitWaitvisibilityOfElementLocated(actInfoButtonBy);
			actInfoButton.click();
		}
		
		public void clickOnActViewMoreButton()
		{
			explicitWaitvisibilityOfElementLocated(actViewMoreButtonBy);
			actViewMoreButton.click();
		}
		
		public String fetchActExitInfoButton()
		{
			SuperTestScript.driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
			try {
				SuperTestScript.driver.findElement(By.cssSelector("paper-dialog[aria-hidden='true'] paper-fab[aria-label='Exit Information']"));
				return "Found";
			}
			catch(NoSuchElementException e){
			    return "Not Found";
			}
		}
		
		public void clickOnActExitInfoButton()
		{
			explicitWaitvisibilityOfElementLocated(actExitInfoButtonBy);
			actExitInfoButton.click();
		}
		
		public void clickOnActExitInfoButton2(String info)
		{	
			if(info.equalsIgnoreCase("Not Found")){
				explicitWaitvisibilityOfElementLocated(actExitInfoButtonBy);
				actExitInfoButton.click();
			}
		}
		
		public String checkPrevButton()
		{
			SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			try {
				SuperTestScript.driver.findElement(By.cssSelector("paper-button[id='prev'][aria-disabled='false']"));
				return "Found";
			}
			catch(NoSuchElementException e){
				return "Not Found";
			}
		}
		 
		public void clickPrevActButton()
		{
			explicitWaitvisibilityOfElementLocated(actPrevButtonBy);
			actPrevButton.click();
		}
		
		public String checkNextButton()
		{
			SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			try {
				SuperTestScript.driver.findElement(By.cssSelector("paper-button[id='next'][aria-disabled='false']"));
				return "Found";
			}
			catch(NoSuchElementException e){
				return "Not Found";
			}
		}
		
		public String checkDragLs(String name, String num)
		{
			SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			try {
				SuperTestScript.driver.findElement(By.cssSelector("program-activity[title='"+name+"']:nth-of-type("+num+")"));
				return "Found";
			}
			catch(NoSuchElementException e){
			    return "Not Found";
			}
		}
		
		public void clickNextActButton()
		{
			explicitWaitvisibilityOfElementLocated(actNextButtonBy);
			actNextButton.click();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

