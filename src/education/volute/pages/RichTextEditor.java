package education.volute.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import education.volute.common_lib.SuperTestScript;

public class RichTextEditor extends NavigatorTool {

	/*@FindBy(id = "Help")
	private WebElement richTEHelp;
	private By richTEHelpBy = By.id("Help");
	
	@FindBy(id = "Share")
	private WebElement richTEShare;
	private By richTEShareBy = By.id("Share");*/
	
	@FindBy(css = "paper-fab[title='Manage Rich Text']")
	private WebElement manageRichTE;
	private By manageRichTEBy = By.cssSelector("paper-fab[title='Manage Rich Text']");
	
	@FindBy(id = "fullscreen")
	private WebElement richTEFullScreen;
	private By richTEFullScreenBy = By.id("fullscreen");
	
	@FindBy(css = "div[title='Rich Text Editor']")
	private WebElement richTEHeader;
	private By richTEHeaderBy = By.cssSelector("div[title='Rich Text Editor']");
	
	@FindBy(css = "volute-app[id='UUID1000'] div[class^='note-editable']")//volute-app[id='UUID1000'] div[class^='note-editable']
	private WebElement writeRichTE;
	private By writeRichTEBy = By.cssSelector("volute-app[id='UUID1000'] div[class^='note-editable']");
	
	@FindBy(id = "mceu_18")
	private WebElement insertRichTEButton;
	private By insertRichTEButtonBy = By.id("mceu_18");
	
	@FindBy(id = "mceu_37")
	private WebElement insertoreditVideoRichTEButton;
	private By insertoreditVideoRichTEButtonBy = By.id("mceu_37");
	
	@FindBy(css = "div[class='mce-tabs'] > div:nth-of-type(1)")
	private WebElement richTEGeneralVideoButton;
	private By richTEGeneralVideoButtonBy = By.cssSelector("div[class='mce-tabs'] > div:nth-of-type(1)");
	
	@FindBy(id = "div[class='mce-container-body mce-abs-layout'] > div[role='tabpanel'] > div > div:nth-of-type(2) > div > div:nth-of-type(2)")
	private WebElement sourceVideoRichTE;
	private By sourceVideoRichTEBy = By.id("div[class='mce-container-body mce-abs-layout'] > div[role='tabpanel'] > div > div:nth-of-type(2) > div > div:nth-of-type(2)");		
	
	@FindBy(id = "div[class='mce-container-body mce-abs-layout'] > div[role='tabpanel'] > div > div:nth-of-type(3) > div > div:nth-of-type(2)")
	private WebElement altSourceVideoRichTE;
	private By altSourceVideoRichTEBy = By.id("div[class='mce-container-body mce-abs-layout'] > div[role='tabpanel'] > div > div:nth-of-type(3) > div > div:nth-of-type(2)");
	
	@FindBy(id = "div[class='mce-container-body mce-abs-layout'] > div[role='tabpanel'] > div > div:nth-of-type(4) > div > div:nth-of-type(2)")
	private WebElement posterRichTE;
	private By posterRichTEBy = By.id("div[class='mce-container-body mce-abs-layout'] > div[role='tabpanel'] > div > div:nth-of-type(4) > div > div:nth-of-type(2)");
	
	@FindBy(id = "div[class='mce-container-body mce-abs-layout'] > div[role='tabpanel'] > div > div:nth-of-type(5) > div > div:nth-of-type(2) > div > input:nth-of-type(1)")
	private WebElement leftDimensionVideoRichTE;
	private By leftDimensionVideoRichTEBy = By.id("div[class='mce-container-body mce-abs-layout'] > div[role='tabpanel'] > div > div:nth-of-type(5) > div > div:nth-of-type(2) > div > input:nth-of-type(1)");
	
	@FindBy(id = "div[class='mce-container-body mce-abs-layout'] > div[role='tabpanel'] > div > div:nth-of-type(5) > div > div:nth-of-type(2) > div > input:nth-of-type(2)")
	private WebElement rightDimensionVideoRichTE;
	private By rightDimensionVideoRichTEBy = By.id("div[class='mce-container-body mce-abs-layout'] > div[role='tabpanel'] > div > div:nth-of-type(5) > div > div:nth-of-type(2) > div > input:nth-of-type(2)");
	
	@FindBy(css = "div[class='mce-tabs'] > div:nth-of-type(2)")
	private WebElement richTEEmbedVideoButton;
	private By richTEEmbedVideoButtonBy = By.cssSelector("div[class='mce-tabs'] > div:nth-of-type(2)");
	
	@FindBy(id = "mcemediasource")
	private WebElement embedcodeRichTEText;
	private By embedcodeRichTETextBy = By.id("mcemediasource");
	
	@FindBy(id = "mceu_38")
	private WebElement insertoreditImageRichTEButton;
	private By insertoreditImageRichTEButtonBy = By.id("mceu_38");
	
	@FindBy(id = "div[class='mce-container-body mce-abs-layout'] > div:nth-of-type(2) > div > div:nth-of-type(2) > input")
	private WebElement sourceImageRichTE;
	private By sourceImageRichTEBy = By.id("div[class='mce-container-body mce-abs-layout'] > div[role='tabpanel'] > div > div:nth-of-type(2) > div > div:nth-of-type(2)");		
	
	@FindBy(id = "div[class='mce-container-body mce-abs-layout'] > div:nth-of-type(3) > div > input")
	private WebElement imageDescriptionRichTE;
	private By imageDescriptionRichTEBy = By.id("div[class='mce-container-body mce-abs-layout'] > div:nth-of-type(3) > div > input");
	
	@FindBy(id = "div[class='mce-container-body mce-abs-layout'] > div:nth-of-type(4) > div > div:nth-of-type(2) > div > input:nth-of-type(1)")
	private WebElement leftDimensionImageRichTE;
	private By leftDimensionImageRichTEBy = By.id("div[class='mce-container-body mce-abs-layout'] > div:nth-of-type(4) > div > div:nth-of-type(2) > div > input:nth-of-type(1)");
	
	@FindBy(id = "div[class='mce-container-body mce-abs-layout'] > div:nth-of-type(4) > div > div:nth-of-type(2) > div > input:nth-of-type(2)")
	private WebElement rightDimensionImageRichTE;
	private By rightDimensionImageRichTEBy = By.id("div[class='mce-container-body mce-abs-layout'] > div:nth-of-type(4) > div > div:nth-of-type(2) > div > input:nth-of-type(2)");
	
	@FindBy(id = "div[role='group'] > div > div:nth-of-type(2)[class='mce-widget mce-btn mce-primary mce-abs-layout-item mce-first mce-btn-has-text']")
	private WebElement okVideoImageRichTEButton;
	private By okVideoImageRichTEButtonBy = By.id("div[role='group'] > div > div:nth-of-type(2)[class='mce-widget mce-btn mce-primary mce-abs-layout-item mce-first mce-btn-has-text']");
	
	@FindBy(id = "div[role='group'] > div > div:nth-of-type(3)[class='mce-widget mce-btn mce-abs-layout-item mce-last mce-btn-has-text']")
	private WebElement cancelVideoImageRichTEButton;
	private By cancelVideoImageRichTEButtonBy = By.id("div[role='group'] > div > div:nth-of-type(3)[class='mce-widget mce-btn mce-abs-layout-item mce-last mce-btn-has-text']");
	
	@FindBy(css = "paper-button:nth-of-type(1)[class='style-scope confirmation-component x-scope paper-button-0']")
	private WebElement deleteYesRichTE;
	private By deleteYesRichTEBy = By.cssSelector("paper-button:nth-of-type(1)[class='style-scope confirmation-component x-scope paper-button-0']");
	
	@FindBy(css = "paper-button:nth-of-type(2)[class='style-scope confirmation-component x-scope paper-button-0']")
	private WebElement deleteNoRichTE;
	private By deleteNoRichTEBy = By.cssSelector("paper-button:nth-of-type(2)[class='style-scope confirmation-component x-scope paper-button-0']");
	
	@FindBy(css = "paper-button[title='Save'][aria-title='Save Rich Text Content']")
	private WebElement richTESave;
	private By richTESaveBy = By.cssSelector("paper-button[title='Save'][aria-title='Save Rich Text Content']");
	
	@FindBy(css = "paper-button[title='Clear'][aria-title='Clear Rich Text Content']")
	private WebElement richTEClear;
	private By richTEClearBy = By.cssSelector("paper-button[title='Clear'][aria-title='Clear Rich Text Content']");
	/*@FindBy(xpath = "//paper-button[contains(text( ),'Save')]")
	private WebElement richTESave;
	private By richTESaveBy = By.xpath("//paper-button[contains(text( ),'Save')]");*/
	
	@FindBy(css = "paper-toast[title='Rich text saved successfully']")
	private WebElement addedMsgRichTE;
	private By addedMsgRichTEBy = By.cssSelector("paper-toast[title='Rich text saved successfully']");
	
	@FindBy(css = "div[class='done-button style-scope app-management']")
	private WebElement richTEDoneButton;
	private By richTEDoneButtonBy = By.cssSelector("div[class='done-button style-scope app-management']");
	
	public RichTextEditor()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	/*public void clickOnRichHelp()
	{
		explicitWaitvisibilityOfElementLocated(richTEHelpBy);
		richTEHelp.click();
	}
	
	public void clickOnRichShare()
	{
		explicitWaitvisibilityOfElementLocated(richTEShareBy);
		richTEShare.click();
	}*/
	
	public void clickOnRichManage()
	{
		//explicitWaitvisibilityOfElementLocated(manageRichTEBy);
		//manageRichTE.click();
		javaScriptExecutor(manageRichTEBy);
	}
	
	public void clickOnRichFullScreen()
	{
		explicitWaitvisibilityOfElementLocated(richTEFullScreenBy);
		richTEFullScreen.click();
	}
	
	public String fetchRichHeader()
	{
		explicitWaitelementToBeClickable(richTEHeaderBy);
		String msg = richTEHeader.getText();
		return msg;
	}
	
	public void enterWriteRich(String richWrite)
	{
		//explicitWaitvisibilityOfElementLocated(writeRichTEBy);
		//Actions a1 = new Actions(SuperTestScript.driver);
		//a1.moveToElement(writeRichTE).click().sendKeys(richWrite).perform();
		//javaScriptExecutorWrite(writeRichTEBy, richWrite);
		int desc_size = SuperTestScript.driver.findElements(writeRichTEBy).size();
		SuperTestScript.driver.findElements(writeRichTEBy).get(desc_size-1).sendKeys(richWrite);
	}
	
	public void clickOnInsertRichButton()
	{
		explicitWaitvisibilityOfElementLocated(insertRichTEButtonBy);
		insertRichTEButton.click();
	}
	
	public void clickOnInsertOrEditVideoRichButton()
	{
		explicitWaitvisibilityOfElementLocated(insertoreditVideoRichTEButtonBy);
		insertoreditVideoRichTEButton.click();
	}
	
	public void clickOnRichGeneralVideoButton()
	{
		explicitWaitvisibilityOfElementLocated(richTEGeneralVideoButtonBy);
		richTEGeneralVideoButton.click();
	}
	
	public void enterSourceVideoRich(String richVidSource)
	{
		explicitWaitvisibilityOfElementLocated(sourceVideoRichTEBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(sourceVideoRichTE).click().sendKeys(richVidSource).perform();
		
	}
	
	public void enterAltSourceVideoRich(String richAltVidSource)
	{
		explicitWaitvisibilityOfElementLocated(altSourceVideoRichTEBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(altSourceVideoRichTE).click().sendKeys(richAltVidSource).perform();
		
	}
	
	public void enterPosterRich(String richPoster)
	{
		explicitWaitvisibilityOfElementLocated(posterRichTEBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(posterRichTE).click().sendKeys(richPoster).perform();
		
	}
	
	public void enterLDVideoRich(String richLDV)
	{
		explicitWaitvisibilityOfElementLocated(leftDimensionVideoRichTEBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(leftDimensionVideoRichTE).click().sendKeys(richLDV).perform();
		
	}
	
	public void enterRDVideoRich(String richRDV)
	{
		explicitWaitvisibilityOfElementLocated(rightDimensionVideoRichTEBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(rightDimensionVideoRichTE).click().sendKeys(richRDV).perform();
		
	}
	
	public void clickOnRichEmbedVideoButton()
	{
		explicitWaitvisibilityOfElementLocated(richTEEmbedVideoButtonBy);
		richTEEmbedVideoButton.click();
	}
	
	public void enterEmbedRichText(String richEmbed)
	{
		explicitWaitvisibilityOfElementLocated(embedcodeRichTETextBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(embedcodeRichTEText).click().sendKeys(richEmbed).perform();
		
	}
	
	public void clickOnInsertEditImageRichButton()
	{
		explicitWaitvisibilityOfElementLocated(insertoreditImageRichTEButtonBy);
		insertoreditImageRichTEButton.click();
	}
	
	public void enterSourceImageRich(String richImgSource)
	{
		explicitWaitvisibilityOfElementLocated(sourceImageRichTEBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(sourceImageRichTE).click().sendKeys(richImgSource).perform();
		
	}
	
	public void enterRichImageDescription(String richDesc)
	{
		explicitWaitvisibilityOfElementLocated(imageDescriptionRichTEBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(imageDescriptionRichTE).click().sendKeys(richDesc).perform();
	}
	
	public void enterLDImageRich(String richLDI)
	{
		explicitWaitvisibilityOfElementLocated(leftDimensionImageRichTEBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(leftDimensionImageRichTE).click().sendKeys(richLDI).perform();
		
	}
	
	public void enterRDImageRich(String richRDI)
	{
		explicitWaitvisibilityOfElementLocated(rightDimensionImageRichTEBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(rightDimensionImageRichTE).click().sendKeys(richRDI).perform();
		
	}
	
	public void clickOnOkVideoImageRich()
	{
		explicitWaitvisibilityOfElementLocated(okVideoImageRichTEButtonBy);
		okVideoImageRichTEButton.click();
	}
	
	public void clickOnCancelVideoImageRich()
	{
		explicitWaitvisibilityOfElementLocated(cancelVideoImageRichTEButtonBy);
		cancelVideoImageRichTEButton.click();
	}
	
	public void clickOnClearRich()
	{
		explicitWaitvisibilityOfElementLocated(richTEClearBy);
		richTEClear.click();
	}
	
	public void clickOnDeleteYesRich()
	{
		explicitWaitvisibilityOfElementLocated(deleteYesRichTEBy);
		deleteYesRichTE.click();
	}
	
	public void clickOnDeleteNoRich()
	{
		explicitWaitvisibilityOfElementLocated(deleteNoRichTEBy);
		deleteNoRichTE.click();
	}
	
	public void clickOnSaveRich()
	{
		explicitWaitvisibilityOfElementLocated(richTESaveBy);
		richTESave.click();
	}
	
	public String fetchRichAddMsg()
	{	
		explicitWaitvisibilityOfElementLocated(addedMsgRichTEBy);
		String msg = addedMsgRichTE.getText();
		return msg;
	}
	
	public void clickOnRichDone()
	{
		explicitWaitvisibilityOfElementLocated(richTEDoneButtonBy);
		richTEDoneButton.click();
	}
	
	public String fetchManFaceShows(String richStr)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//div[@class='note-editable panel-body'] //p[contains(text(),'"+richStr+"')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchFaceShows()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("div[style^='display'] > div[title='There is no text.']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
}
