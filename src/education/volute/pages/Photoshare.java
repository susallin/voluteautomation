package education.volute.pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import education.volute.common_lib.SuperTestScript;

public class Photoshare extends NavigatorTool {

	    // This hasn't been added yet.
		@FindBy(css = "paper-fab[id^='Photoshare'][title='Manage']")
		private WebElement managePhotoshare;
		private By managePhotoshareBy = By.cssSelector("paper-fab[id^='Photoshare'][title='Manage']");
		
		@FindBy(css = "paper-toolbar[id='paperToolbar'] > div > div[title='Photoshare']")
		private WebElement photoshareHeader;
		private By photoshareHeaderBy = By.cssSelector("paper-toolbar[id='paperToolbar'] > div > div[title='Photoshare']");
		
		@FindBy(css = "paper-fab[id='addButton'][title='Add Content']")
		private WebElement addAlbumPhotoshare;
		private By addAlbumPhotoshareBy = By.cssSelector("paper-fab[id^='addButton'][title='Add Content']");
		
		@FindBy(css = "form[id='albumForm'] input")
		private WebElement albumTitlePhotoshare;
		private By albumTitlePhotoshareBy = By.cssSelector("form[id='albumForm'] input");
		
		@FindBy(css = "vaadin-upload[id='uploadPhotos'] paper-button[id='addFiles']")
		private WebElement selectFilePhotoshare;
		private By selectFilePhotoshareBy = By.cssSelector("vaadin-upload[id='uploadPhotos'] paper-button[id='addFiles']");
		
		@FindBy(css = "paper-input[label='Caption'] input")
		private WebElement photoCaptionPhotoshare;
		private By photoCaptionPhotoshareBy = By.cssSelector("paper-input[label='Caption'] input");
		
		@FindBy(css = "paper-input[label='Tags (comma separator)'] input")
		private WebElement photoTagsPhotoshare;
		private By photoTagsPhotoshareBy = By.cssSelector("paper-input[label='Tags (comma separator)'] input");
		
		@FindBy(css = "photoshare-manager-module paper-button[title='Back']")
		private WebElement backToListPhotoshare;
		private By backToListPhotoshareBy = By.cssSelector("photoshare-manager-module paper-button[title='Back']");
		
		@FindBy(css = "paper-toast[title='Album successfully saved']")
		private WebElement savedAlbumPhotoshareMsg;
		private By saveAlbumPhotoshareMsgBy = By.cssSelector("paper-toast[title='Album successfully saved']");
		
		@FindBy(css = "paper-toast[title='File is too large or file type is not supported.']")
		private WebElement UploadFilePhotoshareErrorMsg;
		private By UploadFilePhotoshareErrorMsgBy = By.cssSelector("paper-toast[title='File is too large or file type is not supported.']");
		
		@FindBy(css = "paper-toast[title='Album successfully deleted']")
		private WebElement deletedAlbumPhotoshareMsg;
		private By deletedAlbumPhotoshareMsgBy = By.cssSelector("paper-toast[title='Album successfully deleted']");
		
		@FindBy(css = "paper-toast[title='Photo successfully added']")
		private WebElement addPhotoMsg;
		private By addPhotoMsgBy = By.cssSelector("paper-toast[title='Photo successfully added']");
		
		@FindBy(css = "paper-toast[title='Photo successfully deleted']")
		private WebElement deletedImgPhotoshare;
		private By deletedImgPhotoshareBy = By.cssSelector("paper-toast[title='Photo successfully deleted']");
		
		@FindBy(css = "paper-fab[aria-label='back to album list']")
		private WebElement selectFrontFaceBackToAlbumList;
		private By selectFrontFaceBackToAlbumListBy = By.cssSelector("paper-fab[aria-label='back to album list']");
		
		@FindBy(css = "paper-fab[aria-label='show photo details']")
		private WebElement selectShowPhotoDetails;
		private By selectShowPhotoDetailsBy = By.cssSelector("paper-fab[aria-label='show photo details']");
		
		@FindBy(css = "volute-albums-list[id^='volute-albums-list'] fab-module[id='mainSearch'] paper-fab[title='search button']")
		private WebElement selectSearchPhotoshare;
		private By selectSearchPhotoshareBy = By.cssSelector("volute-albums-list[id^='volute-albums-list'] fab-module[id='mainSearch'] paper-fab[title='search button']");
		
		@FindBy(css = "volute-albums-list[id^='volute-albums-list'] fab-module[id='mainSearchFullscreen'] paper-fab[title='search button']")
		private WebElement selectFullScreenSearchPhotoshare;
		private By selectFullScreenSearchPhotoshareBy = By.cssSelector("volute-albums-list[id^='volute-albums-list'] fab-module[id='mainSearchFullscreen'] paper-fab[title='search button']");
		
		public Photoshare()
		{
			PageFactory.initElements(SuperTestScript.driver, this);
			
		}
		
		public void clickOnPhotoshareManage()
		{
			//explicitWaitvisibilityOfElementLocated(manageVideoBy);
			//manageVideo.click();
			javaScriptExecutor(managePhotoshareBy);
		}
		
		public void clickOnAddAlbumButton()
		{
			explicitWaitvisibilityOfElementLocated(addAlbumPhotoshareBy);
			addAlbumPhotoshare.click();
		}
		
		public void enterAlbumTitlePhotoshare(String title)
		{
			int desc_size = SuperTestScript.driver.findElements(albumTitlePhotoshareBy).size();
			SuperTestScript.driver.findElements(albumTitlePhotoshareBy).get(desc_size-1).sendKeys(title);
		}
		
		public void clearAlbumTitle()
		{
			explicitWaitvisibilityOfElementLocated(albumTitlePhotoshareBy);
			albumTitlePhotoshare.clear();
		}
		
		public void clickOnSelectFilePhotoshareButton(String fileName) throws Exception 
		{
			javaScriptExecutorUpload(selectFilePhotoshareBy, fileName);
			//explicitWaitvisibilityOfElementLocated(selectFilePhotoshareBy);
			//selectFilePhotoshare.click();
		}
		
		public void enterPhotoCaptionPhotoshare(String caption)
		{
			int desc_size = SuperTestScript.driver.findElements(photoCaptionPhotoshareBy).size();
			SuperTestScript.driver.findElements(photoCaptionPhotoshareBy).get(desc_size-1).sendKeys(caption);
		}
		
		public void clearPhotoCaption()
		{
			explicitWaitvisibilityOfElementLocated(photoCaptionPhotoshareBy);
			photoCaptionPhotoshare.clear();
		}
		
		public void enterPhotoTagsPhotoshare(String tag)
		{
			int desc_size = SuperTestScript.driver.findElements(photoTagsPhotoshareBy).size();
			SuperTestScript.driver.findElements(photoTagsPhotoshareBy).get(desc_size-1).sendKeys(tag);
		}
		
		public void clearPhotoTag()
		{
			explicitWaitvisibilityOfElementLocated(photoTagsPhotoshareBy);
			photoTagsPhotoshare.clear();
		}
		
		public void moveToPhotoTag()
		{
			javaScriptExecutorNoClick(photoTagsPhotoshareBy);
		}
		
		public void selectRemovePhoto()
		{
			javaScriptExecutorNoClick(By.cssSelector("div:nth-of-type(1)>paper-fab[title='Remove This Photo']"));
			ManagePrograms mp = new ManagePrograms();
			
			explicitWaitvisibilityOfElementLocated(By.cssSelector("div:nth-of-type(1)>paper-fab[title='Remove This Photo']"));
			List<WebElement> selectIcon = SuperTestScript.driver.findElements(By.cssSelector("div:nth-of-type(1)>paper-fab[title='Remove This Photo']"));
			
			for(int i=0; i<=0; i++)
			{
				selectIcon.get(i).click();
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				mp.clickOnDeletePgmYesButton();
			}
			//selectIcon.click();	
		}
		
		public void clickOnBackToListPhotoshare() //throws IOException 
		{
			javaScriptExecutorNoClick(backToListPhotoshareBy);
			explicitWaitvisibilityOfElementLocated(backToListPhotoshareBy);
			backToListPhotoshare.click();
		}
		
		public void ClickOnEditAlbum(String name)
		{
			explicitWaitvisibilityOfElementLocated(By.xpath("//div[contains(text(),'"+name+"')]/../.. //paper-icon-button[@title='Edit Album']"));
			WebElement selectIcon = SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'"+name+"')]/../.. //paper-icon-button[@title='Edit Album']"));
			selectIcon.click();	
		}
		
		public void ClickOnDeleteAlbum(String name)
		{
			explicitWaitvisibilityOfElementLocated(By.xpath("//div[contains(text(),'"+name+"')]/../.. //paper-icon-button[@title='Delete Album']"));
			WebElement selectIcon = SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'"+name+"')]/../.. //paper-icon-button[@title='Delete Album']"));
			selectIcon.click();	
		}
		
		public void ClickOnAlbumCover(String name)
		{
			explicitWaitvisibilityOfElementLocated(By.xpath("//paper-icon-item[@alt='Album Cover'] //div[contains(text(),'"+name+"')]"));
			WebElement selectIcon = SuperTestScript.driver.findElement(By.xpath("//paper-icon-item[@alt='Album Cover'] //div[contains(text(),'"+name+"')]"));
			selectIcon.click();	
		}
		
		public void clickOnSelectFrontFaceBackToAlbumList() //throws IOException 
		{
			explicitWaitvisibilityOfElementLocated(selectFrontFaceBackToAlbumListBy);
			selectFrontFaceBackToAlbumList.click();
		}
		
		public void clickOnSelectShowPhotoDetails() //throws IOException 
		{
			explicitWaitvisibilityOfElementLocated(selectShowPhotoDetailsBy);
			selectShowPhotoDetails.click();
		}
		
		public void clickOnSelectSearchPhotoshare() //throws IOException 
		{
			explicitWaitvisibilityOfElementLocated(selectSearchPhotoshareBy);
			selectSearchPhotoshare.click();
		}
		
		public void clickOnSelectFullScreenSearchPhotoshare() //throws IOException 
		{
			explicitWaitvisibilityOfElementLocated(selectFullScreenSearchPhotoshareBy);
			selectFullScreenSearchPhotoshare.click();
		}
		
		public void clickOnSelectFilePhotoshare()
		{
			explicitWaitvisibilityOfElementLocated(selectFilePhotoshareBy);
			selectFilePhotoshare.click();
		}
		
		public String fetchDUploadFilePhotoshareErrorMsg()
		{
			SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			try {
				SuperTestScript.driver.findElement(UploadFilePhotoshareErrorMsgBy);
				return "Found";
			}
			catch(NoSuchElementException e){
				return "Not Found";
			}
		}
		
		public String fetchAddPhotoConfirmationMsg()
		{
			SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			try {
				SuperTestScript.driver.findElement(addPhotoMsgBy);
				return "Found";
			}
			catch(NoSuchElementException e){
				return "Not Found";
			}
		}
		
		public String fetchDeletedImgPhotoshareMsg()
		{
			SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			try {
				SuperTestScript.driver.findElement(deletedImgPhotoshareBy);
				return "Found";
			}
			catch(NoSuchElementException e){
				return "Not Found";
			}
		}
		
		public String fetchSaveAlbumPhotoshareMsg()
		{
			SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			try {
				SuperTestScript.driver.findElement(saveAlbumPhotoshareMsgBy);
				return "Found";
			}
			catch(NoSuchElementException e){
				return "Not Found";
			}
		}
		
		public String fetchDeletedAlbumPhotoshareMsg()
		{
			SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			try {
				SuperTestScript.driver.findElement(deletedAlbumPhotoshareMsgBy);
				return "Found";
			}
			catch(NoSuchElementException e){
				return "Not Found";
			}
		}
		
		public String fetchManageDeletedAlbum(String alb)
		{
			SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			try {
				SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'"+alb+"')]"));
				return "Found";
			}
			catch(NoSuchElementException e){
				return "Not Found";
			}
		}
		
		public String fetchFaceDeletedAlbum(String alb)
		{
			SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			try {
				SuperTestScript.driver.findElement(By.xpath("//paper-icon-item[@alt='Album Cover'] //div[contains(text(),'"+alb+"')]"));
				return "Found";
			}
			catch(NoSuchElementException e){
				return "Not Found";
			}
		}
		
		public String fetchDeletedPhoto(int n)
		{
			SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			try {
				SuperTestScript.driver.findElement(By.cssSelector("div:nth-of-type("+n+")>paper-fab[title='Remove This Photo']"));
				return "Found";
			}
			catch(NoSuchElementException e){
				return "Not Found";
			}
		}
}
