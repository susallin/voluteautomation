package education.volute.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import education.volute.common_lib.SuperTestScript;



public class ManageUsers extends NavigatorTool

{
	@FindBy(css = "fab-module[title-id='Add New User'] paper-fab[title='add button']")
	private WebElement addNewUserButton;
	private By addNewUserButtonBy = By.cssSelector("fab-module[title-id='Add New User'] paper-fab[title='add button']");
	
	@FindBy(css = "input[title='First Name']")
	private WebElement firstNameTextBox;
	private By firstNameTextBoxBy = By.cssSelector("input[title='First Name']");
	
	@FindBy(css = "input[title='Last Name']")
	private WebElement lastNameTextBox;
	private By lastNameTextBoxBy = By.cssSelector("input[title='Last Name']");
	
	@FindBy(css = "input[title='Email']")
	private WebElement eMailTextBox;
	private By emailTextBoxBy = By.cssSelector("input[title='Email']");
	
	@FindBy(id = "subdomainMenu")
	private WebElement selectSubDomain;
	private By selectSubDomainBy = By.id("subdomainMenu");
	
	@FindBy(id = "roleMenu")
	private WebElement selectRoleDropDown;
	private By selectRoleDropDownBy = By.id("roleMenu");
	
	@FindBy(css = "input[title='Cohort Name']")
	private WebElement cohortTextBox;
	private By cohortTextBoxBy = By.cssSelector("input[title='Cohort Name']");
	
	@FindBy(xpath = "//paper-dialog[@id='upsert'] //paper-button[text()='Save']")
	private WebElement saveButton2;
	private By saveButton2By = By.cssSelector("//paper-dialog[@id='upsert'] //paper-button[text()='Save']");
	
	@FindBy(css = "paper-button[class^='save-button add-button']")
	private WebElement addProgramRoleButton;
	private By addProgramRoleButtonBy = By.cssSelector("paper-button[class^='save-button add-button']");
	
	@FindBy(css = "div[class^='add-program'] > program-module")
	private WebElement programListButton;
	private By programListButtonBy = By.cssSelector("div[class^='add-program'] > program-module");
		
	@FindBy(xpath = "//program-module/.. //paper-dropdown-menu[@label='Learning Space Role']")
	private WebElement programRoleListButton;
	private By programRoleListButtonBy = By.xpath("//program-module/.. //paper-dropdown-menu[@label='Learning Space Role']");
	
	@FindBy(css = "upsert-user-modal[title='Edit User'] iron-icon[icon='icons:create']")
	private WebElement editUserButton;
	private By editUserButtonBy = By.cssSelector("upsert-user-modal[title='Edit User'] iron-icon[icon='icons:create']");
	
	@FindBy(css = "paper-icon-button[class^='delete-icon']")
	private WebElement deselectProgramRoleButton;
	private By deselectProgramRoleButtonBy = By.cssSelector("paper-button[class^='save-button add-button']");
	
	@FindBy(css = "paper-button[title='Save New User']")
	private WebElement saveButton;
	private By saveButtonBy = By.cssSelector("paper-button[title='Save New User']");
	
	@FindBy(css = "paper-button[title='Cancel New User']")
	private WebElement cancelButton;
	private By cancelButtonBy = By.cssSelector("paper-button[title='Cancel New User']");
	
	@FindBy(css = "paper-button[title='Save Edit']")
	private WebElement saveEditButton;
	private By saveEditButtonBy = By.cssSelector("paper-button[title='Save Edit']");
	
	@FindBy(css = "paper-button[title='Cancel Edit']")
	private WebElement cancelEditButton;
	private By cancelEditButtonBy = By.cssSelector("paper-button[title='Cancel Edit']");
	
	@FindBy(css = "paper-toast[title='User created successfully']")
	private WebElement successMsg;
	private By successMsgBy = By.cssSelector("paper-toast[title='User created successfully']");
	
	@FindBy(css = "paper-toast[text='Change Success']")
	private WebElement userEditSuccessMsg;
	private By userEditSuccessMsgBy = By.cssSelector("paper-toast[text='Change Success']");
	
	@FindBy(css = "paper-toast[title='User deleted successfully']")
	private WebElement userDeleteSuccessMsg;
	private By userDeleteSuccessMsgBy = By.cssSelector("paper-toast[title='User deleted successfully']");
	
	@FindBy(css = "fab-module[title-id='Add New User'] input[title='Search Users Input']")
	private WebElement searchUserTextbox;
	private By searchUserTextboxBy = By.cssSelector("fab-module[title-id='Add New User'] input[title='Search Users Input']");
	
	@FindBy(css = "paper-button[title='Edit Selected']")
	private WebElement editSelectedButton;
	private By editSelectedButtonBy = By.cssSelector("paper-button[title='Edit Selected']");
	
	@FindBy(css = "paper-button[title='Delete Selected']")
	private WebElement deleteSelectedButton;
	private By deleteSelectedButtonBy = By.cssSelector("paper-button[title='Delete Selected']");
	
	@FindBy(xpath = "//paper-button[text()='Yes']")
	private WebElement deleteUserYesButton;
	private By deleteUserYesButtonBy = By.xpath("//paper-button[text()='Yes']");
	
	@FindBy(css = "paper-dropdown-menu[title='All Programs']")
	private WebElement selectAllPrograms;
	private By selectAllProgramsBy = By.cssSelector("paper-dropdown-menu[title='All Programs']");
	
	@FindBy(css = "fab-module[title-search='Search Users'] paper-fab[title='Search Users']")
	private WebElement selectSearchUsersButton;
	private By selectSearchUsersButtonBy = By.cssSelector("fab-module[title-search='Search Users'] paper-fab[title='Search Users']");
	
	public ManageUsers()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	public void clickOnAddNewUserButton()
	{
		explicitWaitvisibilityOfElementLocated(addNewUserButtonBy);
		addNewUserButton.click();
	}
	
	public void clickOnEditUserButton()
	{	
		explicitWaitvisibilityOfElementLocated(editUserButtonBy);
		editUserButton.click();
	}
	
	public void enterFirstName(String fn)
	{
		javaScriptExecutorNoClick(firstNameTextBoxBy);
		
		explicitWaitvisibilityOfElementLocated(firstNameTextBoxBy);
		firstNameTextBox.sendKeys(fn);
	}
	
	public void clearFirstName()
	{
		javaScriptExecutorNoClick(editUserButtonBy);
		
		explicitWaitvisibilityOfElementLocated(firstNameTextBoxBy);
		firstNameTextBox.clear();
	}
	
	public void enterLastNAme(String ln)
	{
		explicitWaitvisibilityOfElementLocated(lastNameTextBoxBy);
		lastNameTextBox.sendKeys(ln);
	}
	
	public void clearLastName()
	{
		explicitWaitvisibilityOfElementLocated(lastNameTextBoxBy);
		lastNameTextBox.clear();
	}
	
	public void enterEmailId(String email)
	{
		explicitWaitvisibilityOfElementLocated(emailTextBoxBy);
		eMailTextBox.sendKeys(email);
	}
	
	// This was change
	public void selectPgmCheckBox(String pgmName)
	{
		//WebElement selPro = SuperTestScript.driver.findElement(By.cssSelector("paper-item[title='"+pgmName+"']>paper-tristate-checkbox[role='checkbox']>div[id='checkboxContainer']"));
		WebElement selPro = SuperTestScript.driver.findElement(By.cssSelector("paper-dialog[id='upsert'] paper-item[title='"+pgmName+"'] div[id='checkboxContainer']"));
		selPro.click();
	}
	
	public void clickOnSubDomain()
	{
		explicitWaitvisibilityOfElementLocated(selectSubDomainBy);
		selectSubDomain.click();
	}
	
	public void clickOnRoleDropDown()
	{
		javaScriptExecutorNoClick(selectRoleDropDownBy);
		//javaScriptExecutor(selectRoleDropDownBy);
		explicitWaitvisibilityOfElementLocated(selectRoleDropDownBy);
		javaScriptExecutor(selectRoleDropDownBy);
		//selectRoleDropDown.click();
	}	
	
	public void selectARole(String role)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-item[title='"+role+"']"));
		javaScriptExecutor(By.cssSelector("paper-item[title='"+role+"']"));
		//WebElement selectRole = SuperTestScript.driver.findElement(By.cssSelector("paper-item[title='"+role+"']"));
		  //selectRole.click();
	}
	
	public void selectASubDomain(String domain)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-item[title='"+domain+"']"));
		WebElement selectDomain = SuperTestScript.driver.findElement(By.cssSelector("paper-item[title='"+domain+"']"));
		selectDomain.click();
	}
	
	public void clearCohort()
	{
		explicitWaitvisibilityOfElementLocated(cohortTextBoxBy);
		cohortTextBox.clear();
	}
	
	public void enterCohort(String cohort)
	{
		//javaScriptExecutor(By.cssSelector("cohortTextBoxBy"));
		explicitWaitvisibilityOfElementLocated(cohortTextBoxBy);
		cohortTextBox.sendKeys(cohort);
	}
	
	public void clikOnAddProgramRoleButton()
	{
		explicitWaitvisibilityOfElementLocated(addProgramRoleButtonBy);
		addProgramRoleButton.click();
	}
	
	public void clikOnProgramListButton()
	{
		explicitWaitvisibilityOfElementLocated(programListButtonBy);
		programListButton.click();
	}
	
	public void selectProgram(String pgm)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//program-module //option[contains(text(),'"+pgm+"')]"));
		javaScriptExecutor(By.xpath("//program-module //option[contains(text(),'"+pgm+"')]"));
		//WebElement selectRole = SuperTestScript.driver.findElement(By.cssSelector("paper-item[title='"+role+"']"));
		  //selectRole.click();
	}
	
	public void clikOnProgramRoleListButton()
	{
		explicitWaitvisibilityOfElementLocated(programRoleListButtonBy);
		programRoleListButton.click();
	}
	
	public void selectRole(String role)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//program-module/.. //paper-item[text()[contains(.,'"+role+"')]]"));
		javaScriptExecutorClick(By.xpath("//program-module/.. //paper-item[text()[contains(.,'"+role+"')]]"));
		//WebElement selectRole = SuperTestScript.driver.findElement(By.cssSelector("paper-item[title='"+role+"']"));
		  //selectRole.click();
	}
	
	/*public void clikOnDeselectProgramRoleButton()
	{
		explicitWaitvisibilityOfElementLocated(deselectProgramRoleButtonBy);
		deselectProgramRoleButton.click();
	}*/
	
	public void clikOnDeselectProgramRoleButton(String pgmName)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//div[contains(text(),'"+pgmName+"')]/.. //paper-icon-button[@icon='clear']"));
		WebElement selectPgm = SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'"+pgmName+"')]/.. //paper-icon-button[@icon='clear']"));
    	selectPgm.click();
	}
	
	public void clikOnSaveUserButton()
	{
		//javaScriptExecutor(By.cssSelector("saveButton2By"));
		//WebElement scrollArea = SuperTestScript.driver.findElement(saveButtonBy);
		//scroll_Page(scrollArea ,200);
		
		explicitWaitvisibilityOfElementLocated(saveButtonBy);
		//saveButton.click();
		//explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-checkbox[title='"+oldeMail+"']"));
		WebElement userSavebutton = SuperTestScript.driver.findElement(saveButtonBy);
	    userSavebutton.click();
	    //userSavebutton.click();
	}
	
	public void clikOnSaveEditUserButton()
	{
		//javaScriptExecutor(By.cssSelector("saveButton2By"));
		//WebElement scrollArea = SuperTestScript.driver.findElement(saveButtonBy);
		//scroll_Page(scrollArea ,200);
		
		explicitWaitvisibilityOfElementLocated(saveEditButtonBy);
		//saveButton.click();
		//explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-checkbox[title='"+oldeMail+"']"));
		WebElement userSavebutton = SuperTestScript.driver.findElement(saveEditButtonBy);
	    userSavebutton.click();
	    //userSavebutton.click();
	}
	
	/*public void clikOnSaveUserButton()
	{
		//javaScriptExecutor(By.cssSelector("saveButtonBy"));
		explicitWaitvisibilityOfElementLocated(saveButtonBy);
		saveButton.click();
	}*/
	
	public String fetchSuccessMsg()
	{
		
		explicitWaitvisibilityOfElementLocated(successMsgBy);
		String msg = successMsg.getText();
		return msg;
	}
	
	public String fetchUserEditSuccessMsg()//String userEmail)
	{
		//explicitWaitvisibilityOfElementLocated(By.xpath("//div[@title='"+userEmail+"']/.. /paper-toast[@text='Change Success']"));
		//WebElement updateToastShows = SuperTestScript.driver.findElement(By.xpath("//div[@title='"+userEmail+"']/.. /paper-toast[@text='Change Success']"));
		//String msg = updateToastShows.getText();
		//return msg;
		explicitWaitvisibilityOfElementLocated(userEditSuccessMsgBy);
		String msg = userEditSuccessMsg.getText();
		return msg;
	}
	
	public String fetchUserDeleteSuccessMsg()
	{
		
		explicitWaitvisibilityOfElementLocated(userDeleteSuccessMsgBy);
		String msg = userDeleteSuccessMsg.getText();
		return msg;
	}
	
	public void clikOnSearchUserButton()
	{
		explicitWaitvisibilityOfElementLocated(selectSearchUsersButtonBy);
		selectSearchUsersButton.click();
	}
	
	public void enterSearchUser(String oldUser)
	{
		explicitWaitvisibilityOfElementLocated(searchUserTextboxBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(searchUserTextbox).click().sendKeys(oldUser).perform();
		//a1.moveToElement(searchUserTextbox).sendKeys(Keys.ENTER);		
	}
	
	public void pressEnter(){
		pressEnter(searchUserTextboxBy);
	}
	
	public void clickOnUserCheckbox(String oldeMail)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-checkbox[title='"+oldeMail+"']"));
		WebElement userCheckbox = SuperTestScript.driver.findElement(By.cssSelector("paper-checkbox[title='"+oldeMail+"']"));
	    userCheckbox.click();
	}
	
	
	public void clickOnEditSelectedButton()
	{
		explicitWaitvisibilityOfElementLocated(editSelectedButtonBy);
		editSelectedButton.click();
	}
	
	public void clickOnDeleteSelectedButton()
	{
		explicitWaitvisibilityOfElementLocated(deleteSelectedButtonBy);
		deleteSelectedButton.click();
	}
	
	public void clearEmailId()
	{
		explicitWaitvisibilityOfElementLocated(emailTextBoxBy);
		eMailTextBox.clear();
	}
	
	public void clickOnDeleteUserYesButton()
	{
		explicitWaitvisibilityOfElementLocated(deleteUserYesButtonBy);
		deleteUserYesButton.click();
		
	}
	
	public void clickOnAllProgramsTab()
	{
		explicitWaitvisibilityOfElementLocated(selectAllProgramsBy);
		selectAllPrograms.click();
	}
	
	public void ClickOnSelectedProgMU(String progMU)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-item[title='"+progMU+"']"));
		WebElement selectprg = SuperTestScript.driver.findElement(By.cssSelector("paper-item[title='"+progMU+"']"));
	    selectprg.click();
	}
	
	public String fetchUserInfo(String userEmail, String userInfo)
	{
		try {
			SuperTestScript.driver.findElement(By.cssSelector("div[title='"+userEmail+"'] > div[title='"+userInfo+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchProgram(String userEmail, String userProgram)
	{
		try {
			SuperTestScript.driver.findElement(By.cssSelector("div[title='"+userEmail+"'] span[title='"+userProgram+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	/*public String fetchEmailUser(String userEmail)
	{
		try {
			SuperTestScript.driver.findElement(By.cssSelector("div[title='"+userEmail+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}*/
	
	
	
//	// 1. To Add a new user......
//	  // Add New Users
//	 WebElement addNew = driver.findElement(By.cssSelector("paper-button[title='Add New User']"));
//	  addNew.click();
//	  
//	  
//	  Thread.sleep(2000);
//	  
//	  // First
//	 WebElement fName = driver.findElement(By.name("firstName"));
//	  commonFunctionRepo.SendData(driver, fName, fNameTxt);
//	  
//	  // Last
//	  WebElement lName = driver.findElement(By.name("lastName"));
//	  commonFunctionRepo.SendData(driver, lName, lNameTxt);
//	  
//	  // Email
//	  WebElement eMail = driver.findElement(By.name("email"));
//	  commonFunctionRepo.SendData(driver, eMail, eMailTxt);
//	  
//	  // Select first Programs - need id
//	  WebElement selPro = driver.findElement(By.cssSelector("paper-item[title='"+pgmNameTxt+"']>paper-tristate-checkbox[role='checkbox']>div[id='checkboxContainer']"));
//	  selPro.click();
//	  
//	  // Choose a role dropdown - need id
//	  WebElement roledd = driver.findElement(By.id("roleMenu"));		  
//	  roledd.click();
//	  
//	   // Selecting Type of role
//	  
//	  switch(roleTxt)
//	  {
//	  
//	  case "Account Owner" :
//		  WebElement role = driver.findElement(By.cssSelector("paper-item[title='Account Owner']"));
//		  role.click();
//		  break;
//	
//	  case "Admin" :
//		  WebElement arole = driver.findElement(By.cssSelector("paper-item[title='Admin']"));
//		  arole.click();
//		  break;
//		  
//	  case "Facilitator" :
//		  WebElement frole = driver.findElement(By.cssSelector("paper-item[title='Facilitator']"));
//		  frole.click();
//		  break;
//		  
//	  case "Participant" :
//		  WebElement prole = driver.findElement(By.cssSelector("paper-item[title='Participant']"));
//		  prole.click();
//		  break;
//		  
//	  case "Observer" :
//		  WebElement orole = driver.findElement(By.cssSelector("paper-item[title='Observer']"));
//		  orole.click();
//		  break;
//	  }
//	  
//	  
//	 
//	  //Add cohort
//	  WebElement cohort = driver.findElement(By.cssSelector("input[title='Cohort Name']"));
//	  commonFunctionRepo.SendData(driver, cohort,cohortTxt);
//	  
//	  // Cancel
//	  //driver.findElement(By.cssSelector("paper-button[class*='cancel-button-inverted style-scope upsert-user-modal x-scope paper-button-0']")).click();
//	  
//	//Click on Save
//	  
//   // WebElement save = driver.findElement(By.xpath("@paper-button[title='Save User']"));
//    WebElement save = driver.findElement(By.xpath("/html/body/volute-container/neon-animated-pages/volute-app-fullsize/div[2]/div/volute-app/div[1]/div/neon-animated-pages/neon-animatable[1]/volute-app-face/roster-face1-module/upsert-user-modal/paper-dialog/div/div[2]/paper-button[2]"));
//    save.click();
//	Thread.sleep(1000);
//	
//	
//	
	
//	public static WebElement addNew = driver.findElement(By.cssSelector("paper-button[class*='add-mashup style-scope roster-face1-module x-scope paper-button-0']"));
//	
//	public static WebElement fName = driver.findElement(By.cssSelector("paper-input-container > div:nth-of-type(2) > div[id='labelAndInputContainer'] > input[name='firstName']"));
//
//	public static WebElement lName = driver.findElement(By.cssSelector("paper-input-container > div:nth-of-type(2) > div[id='labelAndInputContainer'] > input[name='lastName']"));
//	  
//	public static WebElement eMail = driver.findElement(By.cssSelector("paper-input-container > div:nth-of-type(2) > div[id='labelAndInputContainer'] > input[name='email']"));
//	  
//	  // Select Programs
//	public static WebElement firstPgm = driver.findElement(By.cssSelector("paper-item:nth-of-type(1) > paper-tristate-checkbox > div[id='checkboxContainer']"));
//	  
//	 // Choose a role
//	public static WebElement roleDropArrow = driver.findElement(By.cssSelector("paper-input > paper-input-container[class*='style-scope paper-input x-scope paper-input-container-7'] > div:nth-of-type(2)"));
//	  
//	// Selecting Type of role
//	public static WebElement roleType = driver.findElement(By.cssSelector("paper-listbox[class*='dropdown-content volute-app-roster-profile-edit-org-role style-scope upsert-user-modal x-scope paper-listbox-0'] > paper-item:nth-of-type(1)"));
//	
//	//cancel
//	public static WebElement cancel = driver.findElement(By.cssSelector("paper-button[class*='cancel-button-inverted style-scope upsert-user-modal x-scope paper-button-0']"));
//	   
//	//save
//	public static WebElement save = driver.findElement(By.xpath("//paper-button[contains(text(),'Save')]"));

}
