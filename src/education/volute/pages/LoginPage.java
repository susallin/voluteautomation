package education.volute.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import education.volute.common_lib.SuperTestScript;



public class LoginPage extends NavigatorTool
{	
	
	@FindBy(id = "username")//(css = "input[name='email']")
	private WebElement userNameTextBox;	
	private By userNameTextBoxBy = By.id("username");
	
	/*@FindBy(css = "volute-container[id='main-volute-content']")
	private WebElement contentPage;
	private By contentPageBy = By.cssSelector("volute-container[id='main-volute-content']");
	*/
	@FindBy(id = "password")//(css = "input[type='password']")
	private WebElement pwdTextBox;	
	private By pwdTextBoxBy = By.id("password");//("input[type='password']");
	
	@FindBy(id = "confirm-password")//(css = "input[type='password']")
	private WebElement confirmPwdTextBox;	
	private By confirmPwdTextBoxBy = By.id("confirm-password");//("input[type='password']");
	
	@FindBy(css = "#supportedBrowser > #login-button")//#supportedBrowser > form > div.loginPage > #login-button")//(id = "login-button")
	private WebElement loginButton;
	private By loginButtonBy = By.cssSelector("#supportedBrowser > #login-button");
	
	@FindBy(css = "#login-button")//#supportedBrowser > form > div.loginPage > #login-button")//(id = "login-button")
	private WebElement expireButton;
	private By expireButtonBy = By.cssSelector("#login-button");
	
	@FindBy(xpath = "//a[contains(text(),'Forgot Password?')]")//span[2]//a[contains(text(),'Forgot Password?')]")//(id = "login-button")
	private WebElement forgotPwButton;
	private By forgotPwButtonBy = By.xpath("//a[contains(text(),'Forgot Password?')]");

	@FindBy(id = "email")//(css = "input[name='email']")
	private WebElement emailTextBox;
	private By emailTextBoxBy = By.id("email");
	
	@FindBy(css = "input[value='Submit'][type='submit']")//(id = "login-button")
	private WebElement submitButton;
	private By submitButtonBy = By.cssSelector("input[value='Submit'][type='submit']");
	
	public LoginPage()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	public void login(String un, String pwd)
	{
		//explicitWaitvisibilityOfElementLocated(userNameTextBoxBy);
		int un_size = SuperTestScript.driver.findElements(userNameTextBoxBy).size();
		SuperTestScript.driver.findElements(userNameTextBoxBy).get(un_size-1).sendKeys(un);
		//userNameTextBox.sendKeys(un);
		
		//explicitWaitvisibilityOfElementLocated(pwdTextBoxBy);
		int pwd_size = SuperTestScript.driver.findElements(pwdTextBoxBy).size();
		SuperTestScript.driver.findElements(pwdTextBoxBy).get(pwd_size-1).sendKeys(pwd);
		//pwdTextBox.sendKeys(pwd);
		
	}
	
	public void pwExpire(String pw, String pw2)
	{
		//explicitWaitvisibilityOfElementLocated(userNameTextBoxBy);
		//userNameTextBox.sendKeys(un);
		
		//explicitWaitvisibilityOfElementLocated(pwdTextBoxBy);
		int pwd_size = SuperTestScript.driver.findElements(pwdTextBoxBy).size();
		SuperTestScript.driver.findElements(pwdTextBoxBy).get(pwd_size-1).sendKeys(pw2);
		//pwdTextBox.sendKeys(pwd);
		
		int pwd2_size = SuperTestScript.driver.findElements(confirmPwdTextBoxBy).size();
		SuperTestScript.driver.findElements(confirmPwdTextBoxBy).get(pwd2_size-1).sendKeys(pw2);
		
	}
	
	public void logibutton(){
		explicitWaitvisibilityOfElementLocated(loginButtonBy);
		loginButton.click();
	}
	
	public void expireButton(){
		explicitWaitvisibilityOfElementLocated(expireButtonBy);
		expireButton.click();
	}
	
	public void forgotPwButton(){
		explicitWaitvisibilityOfElementLocated(forgotPwButtonBy);
		forgotPwButton.click();
	}
	
	public void enterOnemailTextBox(String email){
		int em_size = SuperTestScript.driver.findElements(emailTextBoxBy).size();
		SuperTestScript.driver.findElements(emailTextBoxBy).get(em_size-1).sendKeys(email);
	}
	
	public void submitButton(){
		explicitWaitvisibilityOfElementLocated(submitButtonBy);
		submitButton.click();
	}
	
	public String fetchResetpwConfirm(){
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//p[contains(text(),'Reset password email has been sent.')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchLogOffPage()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//h2[contains(text(),'Welcome')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchLogPage()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("volute-container[id='main-volute-content']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchWrongEmail()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'Email invalid')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchWrongPassword()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'Password incorrect')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchWrongEmailPwReset()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'No user found with the specified email address')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchForgotPw()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//h2[contains(text(),'Forgot Password?')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
//	public static WebElement useName=driver.findElement(By.name("email"));
//	
//	public static WebElement pwd = driver.findElement(By.cssSelector("input[type='password']"));
//	
//	public static WebElement login = driver.findElement(By.id("login-button"));
//	
}
