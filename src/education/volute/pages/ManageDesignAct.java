package education.volute.pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import education.volute.common_lib.SuperTestScript;

public class ManageDesignAct extends NavigatorTool

{
	/*@FindBy(css = "paper-fab[class='style-scope app-express-face3-module x-scope paper-fab-0']")			 	
	private WebElement addLSButton;
	private By addLsButtonBy = By.cssSelector("paper-fab[class='style-scope app-express-face3-module x-scope paper-fab-0']");			 	
	
	@FindBy(css = "input[placeholder='type name here']")
	private WebElement lsNameTextBox;
	private By lsNameTextBoxBy = By.cssSelector("input[placeholder='type name here']");
			
	@FindBy(css = "textarea[placeholder='type description here']")
	private WebElement lsDescTextBox;
	private By lsDescTextBoxBy = By.cssSelector("textarea[placeholder='type description here']");
			
	@FindBy(css = "paper-button[title='Save Mashup']")
	private WebElement saveLsButton;
	private By saveLsButtonBy = By.cssSelector("paper-button[title='Save Mashup']");
	*/
	@FindBy(css = "paper-toast[title='Learning Space saved.']")
	private WebElement dASavedMsg;
	private By dASavedMsgBy = By.cssSelector("paper-toast[title='Learning Space saved.']");	

	@FindBy(css = "paper-toast[title='Tool saved to mashup']")
	private WebElement dAUpdatedMsg;
	private By dAUpdatedMsgBy = By.cssSelector("paper-toast[title='Tool saved to mashup']");	
	
	@FindBy(css = "paper-toast[title='Tool successfully deleted.']")
	private WebElement dADeletedMsg;
	private By dADeletedMsgBy = By.cssSelector("paper-toast[title='Tool successfully deleted.']");	
	
	@FindBy(css = "paper-toast[title='This arrangement looks great, I saved it for you.']")
	private WebElement dAArrangeMsg;
	private By dAArrangeMsgBy = By.cssSelector("paper-toast[title='This arrangement looks great, I saved it for you.']");
	/*@FindBy(css = "paper-button:nth-of-type(1)[class='style-scope confirmation-component x-scope paper-button-0']")
	private WebElement deleteYesLs;
	private By deleteYesLsBy = By.cssSelector("paper-button:nth-of-type(1)[class='style-scope confirmation-component x-scope paper-button-0']");
	
	@FindBy(css = "paper-button:nth-of-type(2)[class='style-scope confirmation-component x-scope paper-button-0']")
	private WebElement deleteNoLs;
	private By deleteNoLsBy = By.cssSelector("paper-button:nth-of-type(2)[class='style-scope confirmation-component x-scope paper-button-0']");
	*/
	
	@FindBy(css = "paper-button[title='Cancel Learning Space']")
	private WebElement returnToListLsButton;
	private By returnToListLsButtonBy = By.cssSelector("paper-button[title='Cancel Learning Space']");
	
	/*@FindBy(css = "div[class='mashup-summary-assign style-scope']>paper-button[role='button']")
	private WebElement assignLs;
	private By assignLsBy = By.cssSelector("div[class='mashup-summary-assign style-scope']>paper-button[role='button']");
	*/
	// Needs id.
	//@FindBy(css = "div[title='SliTest'],iron-icon[title='Edit Learning Space']")
	//private WebElement editLs;
	//private By editLsBy = By.cssSelector("div[title='SliTest'],iron-icon[title='Edit Learning Space']");
	
	//@FindBy(css = "iron-icon[title='Delete Learning Space']")
	//private WebElement deleteLs;
	//private By deleteLsBy = By.cssSelector("iron-icon[title='Delete Learning Space']");
	
	/*@FindBy(css = "paper-button[title='Configure Mashup']")
	private WebElement configLs;
	private By configLsBy = By.cssSelector("paper-button[title='Configure Mashup']");
	
	@FindBy(css = "paper-toast[title = 'Learning Space assigned successfully']>span[id='label'][class='style-scope paper-toast']")
	private WebElement lsAssignedMsg;
	private By lsAssignedMsgBy = By.cssSelector("paper-toast[title = 'Learning Space assigned successfully']>span[id='label'][class='style-scope paper-toast']");
	*/
	@FindBy(css = "paper-item[title='Directory'] > paper-icon-item > paper-icon-button")
	private WebElement delDirLs;
	private By delDirLsBy = By.cssSelector("paper-item[title='Directory'] > paper-icon-item > paper-icon-button");
	
	@FindBy(css = "paper-item[title='Slides'] > paper-icon-item > paper-icon-button")
	private WebElement delSliLs;
	private By delSliLsBy = By.cssSelector("paper-item[title='Slides'] > paper-icon-item > paper-icon-button");
	
	@FindBy(css = "paper-item[title='Tasks'] > paper-icon-item > paper-icon-button")
	private WebElement delTasLs;
	private By delTasLsBy = By.cssSelector("paper-item[title='Tasks'] > paper-icon-item > paper-icon-button");
	
	@FindBy(css = "paper-item[title='Video'] > paper-icon-item > paper-icon-button")
	private WebElement delVidLs;
	private By delVidLsBy = By.cssSelector("paper-item[title='Video'] > paper-icon-item > paper-icon-button");
	
	@FindBy(css = "paper-item[title='Notes'] > paper-icon-item > paper-icon-button")
	private WebElement delNotLs;
	private By delNotLsBy = By.cssSelector("paper-item[title='Notes'] > paper-icon-item > paper-icon-button");
	
	@FindBy(css = "paper-item[title='Content Integration'] > paper-icon-item > paper-icon-button")
	private WebElement delConLs;
	private By delConLsBy = By.cssSelector("paper-item[title='Content Integration'] > paper-icon-item > paper-icon-button");
	
	@FindBy(css = "paper-item[title='Rich Text Editor'] > paper-icon-item > paper-icon-button")
	private WebElement delRicLs;
	private By delRicLsBy = By.cssSelector("paper-item[title='Rich Text Editor'] > paper-icon-item > paper-icon-button");
	
	@FindBy(css = "paper-item[title='Portal'] > paper-icon-item > paper-icon-button")
	private WebElement delPorLs;
	private By delPorLsBy = By.cssSelector("paper-item[title='Portal'] > paper-icon-item > paper-icon-button");
	
	@FindBy(css = "paper-item[title='Reader'] > paper-icon-item > paper-icon-button")
	private WebElement delRedLs;
	private By delRedLsBy = By.cssSelector("paper-item[title='Reader'] > paper-icon-item > paper-icon-button");
	
	@FindBy(css = "paper-item[title='Profile List'] > paper-icon-item > paper-icon-button")
	private WebElement delProLs;
	private By delProLsBy = By.cssSelector("paper-item[title='Profile List'] > paper-icon-item > paper-icon-button");
	
	@FindBy(css = "paper-item[title='Biography'] > paper-icon-item > paper-icon-button")
	private WebElement delBioLs;
	private By delBioLsBy = By.cssSelector("paper-item[title='Biography'] > paper-icon-item > paper-icon-button");
	
	@FindBy(css = "paper-item[title='Discussion'] > paper-icon-item > paper-icon-button")
	private WebElement delDisLs;
	private By delDisLsBy = By.cssSelector("paper-item[title='Discussion'] > paper-icon-item > paper-icon-button");
	
	@FindBy(css = "paper-item[title='Announcements'] > paper-icon-item > paper-icon-button")
	private WebElement delAnnLs;
	private By delAnnLsBy = By.cssSelector("paper-item[title='Announcements'] > paper-icon-item > paper-icon-button");
	
	@FindBy(css = "paper-item[title='Calendar'] > paper-icon-item > paper-icon-button")
	private WebElement delCalLs;
	private By delCalLsBy = By.cssSelector("paper-item[title='Calendar'] > paper-icon-item > paper-icon-button");
	
	@FindBy(css = "div[class^='edit-panel-tab']")
	private WebElement closeOpenTab;
	private By closeOpenTabBy = By.cssSelector("div[class^='edit-panel-tab']");
	
	@FindBy(css = "paper-menu-button[class^='style-scope activities-list']")
	private WebElement moreMenuButton;
	private By moreMenuButtonBy = By.cssSelector("paper-menu-button[class^='style-scope activities-list']");
	
	@FindBy(css = "activity-users-list fab-module[title-search='Search Users'] paper-fab[title='search button']")
	private WebElement searchButton;
	private By searchButtonBy = By.cssSelector("activity-users-list fab-module[title-search='Search Users'] paper-fab[title='search button']");
	
	@FindBy(css = "activity-users-list fab-module[title-search='Search Users'] input[title='Search Users Input']")
	private WebElement searchInput;
	private By searchInputBy = By.cssSelector("activity-users-list fab-module[title-search='Search Users'] input[title='Search Users Input']");
	
	@FindBy(css = "activity-users-list[id='activityUserList'] paper-radio-button[item='master']")
	private WebElement masterButton;
	private By masterButtonBy = By.cssSelector("activity-users-list[id='activityUserList'] paper-radio-button[item='master']");
	
	@FindBy(css = "paper-button[title='Assign Selected Users']")
	private WebElement cancelSvButton;
	private By cancelSvButtonBy = By.cssSelector("paper-button[title='Assign Selected Users']");
	
	@FindBy(css = "paper-button[title^='Switch to Selected User']")
	private WebElement switchSvButton;
	private By switchSvButtonBy = By.cssSelector("paper-button[title^='Switch to Selected User']");
	
	public ManageDesignAct()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	/*public void clickOnAddLS()
	{	
		//explicitWaitelementToBeClickable(addLsButtonBy);
		explicitWaitvisibilityOfElementLocated(addLsButtonBy);
		addLSButton.click();
	}
	
	public void enterLSName(String lsName)
	{
		explicitWaitvisibilityOfElementLocated(lsNameTextBoxBy);
		lsNameTextBox.sendKeys(lsName);
	}
	
	public void clearLSName()
	{
		explicitWaitvisibilityOfElementLocated(lsNameTextBoxBy);
		lsNameTextBox.clear();
	}
	
	public void enterLsDesc(String lsDesc)
	{
		explicitWaitvisibilityOfElementLocated(lsDescTextBoxBy);
		lsDescTextBox.sendKeys(lsDesc);
	}
	
	public void clearLSDesc()
	{
		explicitWaitvisibilityOfElementLocated(lsDescTextBoxBy);
		lsDescTextBox.clear();
	}
	
	public void clickOnSaveButton()
	{
		explicitWaitvisibilityOfElementLocated(saveLsButtonBy);
		saveLsButton.click();
	}*/
	
	/*public void clickOnEditButton(String lsName)
	{
		//div[contains(text(),'SliTest')]/../..//div[8]/iron-icon[@title='Edit Learning Space']
		explicitWaitelementToBeClickable(By.xpath("//div[contains(text(),'"+ lsName +"')]/../..//div[8]/iron-icon[@title='Edit Learning Space']"));
		WebElement lsEdit = SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'"+ lsName +"')]/../..//div[8]/iron-icon[@title='Edit Learning Space']"));
    	lsEdit.click();
	}
	
	public void clickOndeleteButton(String lsName)
	{
		explicitWaitelementToBeClickable(By.xpath("//div[contains(text(),'"+ lsName +"')]/../..//div[8]/iron-icon[@title='Delete Learning Space']"));
		WebElement lsDel = SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'"+ lsName +"')]/../..//div[8]/iron-icon[@title='Delete Learning Space']"));
    	lsDel.click();
	}
	
	/*public void ClickDeleteYesLs()
	{
		explicitWaitvisibilityOfElementLocated(deleteYesLsBy);
		deleteYesLs.click();
	}
	
	public void ClickDeleteNoLs()
	{
		explicitWaitvisibilityOfElementLocated(deleteNoLsBy);
		deleteNoLs.click();
	}
	
	public void clickOnConfigButton()
	{
		explicitWaitvisibilityOfElementLocated(configLsBy);
		configLs.click();
	}*/
	
	/*public void selectaLearningSpace(String lsName)
	{
		WebElement ls = SuperTestScript.driver.findElement(By.cssSelector("div[title='"+lsName+"']"));
		explicitWaitvisibilityOfElementLocated(By.cssSelector("div[title='"+lsName+"']"));
		ls.click();
	}*/
	
	public void clickMoreMenuButton()
	{
		explicitWaitvisibilityOfElementLocated(moreMenuButtonBy);
		moreMenuButton.click();
	}
	
	// This will click toolbox or Switch view.
	public void clickTbSw(String dsMenuName)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//paper-toolbar //div[contains(text( ),'"+dsMenuName+"')]"));
		WebElement selectMenu = SuperTestScript.driver.findElement(By.xpath("//paper-toolbar //div[contains(text( ),'"+dsMenuName+"')]"));
    	selectMenu.click();
	}
	
	public void clickSearchButton()
	{
		explicitWaitvisibilityOfElementLocated(searchButtonBy);
		searchButton.click();
	}
	
	public void enterOnSearchDes(String search)
	{
		explicitWaitvisibilityOfElementLocated(searchInputBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(searchInput).click().sendKeys(search).perform();
		
	}
	
	public void clickUserCopy()
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("activity-users-list[id='activityUserList'] div[class^='user-item'] > div:nth-of-type(1)"));
		WebElement selectUser = SuperTestScript.driver.findElement(By.cssSelector("activity-users-list[id='activityUserList'] div[class^='user-item'] > div:nth-of-type(1)"));
    	selectUser.click();
	}
	
	//
	public void clickmasterButton()
	{
		explicitWaitvisibilityOfElementLocated(masterButtonBy);
		masterButton.click();
	}
	
	public void clickCancelSV()
	{
		explicitWaitvisibilityOfElementLocated(cancelSvButtonBy);
		cancelSvButton.click();
	}
	
	public void clickSwitchSv()
	{
		explicitWaitvisibilityOfElementLocated(switchSvButtonBy);
		switchSvButton.click();
	}
			
	public void selectaTool(String dsToolName)
	{
		WebElement ds = SuperTestScript.driver.findElement(By.cssSelector("paper-item[title='"+dsToolName+"']"));
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-item[title='"+dsToolName+"']"));
		ds.click();
	}
	
	public String fetchTool(String dsToolName)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("paper-item[title='"+dsToolName+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void selectAllTools()
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("div[class='app-item-wrapper style-scope app-express-app-item']"));
		List<WebElement> allTools = SuperTestScript.driver.findElements(By.cssSelector("div[class='app-item-wrapper style-scope app-express-app-item']"));
		for(int i=0; i<=allTools.size()-1; i++)
		{
			allTools.get(i).click();
		}
	}
	
	public String fetchSuccessMsg()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(dASavedMsgBy);
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchUpdatedMsg()
	{
		
		explicitWaitvisibilityOfElementLocated(dAUpdatedMsgBy);
		String msg = dAUpdatedMsg.getText();
		return msg;
	}
	
	public String fetchDeletedMsg()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(dADeletedMsgBy);
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchArrangeMsg()
	{
		
		explicitWaitvisibilityOfElementLocated(dAArrangeMsgBy);
		String msg = dAArrangeMsg.getText();
		return msg;
	}
	
	public void clickOnReturnToList()
	{
		explicitWaitvisibilityOfElementLocated(returnToListLsButtonBy);
		returnToListLsButton.click();
	}
	
	/*public void clickOnAssign()
	{
		explicitWaitvisibilityOfElementLocated(assignLsBy);
		assignLs.click();
	}
	
	public String fetchLsAssignedmsg()
	{
		explicitWaitvisibilityOfElementLocated(lsAssignedMsgBy);
		String msg = lsAssignedMsg.getText();
		return msg;
	}*/
	
	public void clickOnDelDirLs()
	{
		explicitWaitvisibilityOfElementLocated(delDirLsBy);
		delDirLs.click();
	}
	
	public void clickOnDelSliLs()
	{
		explicitWaitvisibilityOfElementLocated(delSliLsBy);
		delSliLs.click();
	}
	
	public void clickOnDelTasLs()
	{
		explicitWaitvisibilityOfElementLocated(delTasLsBy);
		delTasLs.click();
	}
	
	public void clickOnDelVidLs()
	{
		explicitWaitvisibilityOfElementLocated(delVidLsBy);
		delVidLs.click();
	}
	
	public void clickOnDelNotLs()
	{
		explicitWaitvisibilityOfElementLocated(delNotLsBy);
		delNotLs.click();
	}
	
	public void clickOnDelConLs()
	{
		explicitWaitvisibilityOfElementLocated(delConLsBy);
		delConLs.click();
	}
	
	public void clickOnDelRicLs()
	{
		explicitWaitvisibilityOfElementLocated(delRicLsBy);
		delRicLs.click();
	}
	
	public void clickOnDelPorLs()
	{
		explicitWaitvisibilityOfElementLocated(delPorLsBy);
		delPorLs.click();
	}
	
	public void clickOnDelRedLs()
	{
		explicitWaitvisibilityOfElementLocated(delRedLsBy);
		delRedLs.click();
	}
	
	public void clickOnDelProLs()
	{
		explicitWaitvisibilityOfElementLocated(delProLsBy);
		delProLs.click();
	}
	
	public void clickOnDelBioLs()
	{
		explicitWaitvisibilityOfElementLocated(delBioLsBy);
		delBioLs.click();
	}
	
	public void clickOnDelDisLs()
	{
		explicitWaitvisibilityOfElementLocated(delDisLsBy);
		delDisLs.click();
	}
	
	public void clickOnDelAnnLs()
	{
		explicitWaitvisibilityOfElementLocated(delAnnLsBy);
		delAnnLs.click();
	}
	
	public void clickOnDelCalLs()
	{
		explicitWaitvisibilityOfElementLocated(delCalLsBy);
		delCalLs.click();
	}
	
	public void clickTabBy()
	{
		explicitWaitvisibilityOfElementLocated(closeOpenTabBy);
		closeOpenTab.click();
	}
}
