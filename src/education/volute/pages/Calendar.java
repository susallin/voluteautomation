package education.volute.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import education.volute.common_lib.SuperTestScript;

public class Calendar extends NavigatorTool {

	/*@FindBy(id = "Help")
	private WebElement calHelp;
	private By calHelpBy = By.id("Help");
	
	@FindBy(id = "Share")
	private WebElement calShare;
	private By calShareBy = By.id("Share");*/
	
	@FindBy(css = "paper-fab[title='Manage Calendar']")
	private WebElement manageCal;
	private By manageCalBy = By.cssSelector("paper-fab[title='Manage Calendar']");
	
	@FindBy(id = "fullscreen")
	private WebElement calFullScreen;
	private By calFullScreenBy = By.id("fullscreen");
	
	@FindBy(css = "paper-toolbar[id='paperToolbar'] > div > div[title='Calendar']")
	private WebElement calHeader;
	private By calHeaderBy = By.cssSelector("paper-toolbar[id='paperToolbar'] > div > div[title='Calendar']");
	
	/*@FindBy(css = "paper-input[label='Display Name'] > paper-input-container > div:nth-of-type(2) > div > input[title='Tool Title']")//paper-input[label='Display Name']")
	private WebElement calDisplayName;
	private By calDisplayNameBy = By.cssSelector("paper-input[label='Display Name'] > paper-input-container > div:nth-of-type(2) > div > input[title='Tool Title']");
	*/
	@FindBy(css = "paper-input[id='title'][label='Event name'] input")//paper-input[label='Event name']") > paper-input-container > div:nth-of-type(2) > div > input")
	private WebElement calClearEventName;
	private By calClearEventNameBy = By.cssSelector("paper-input[id='title'][label='Event name'] input");
	
	@FindBy(css = "paper-input[id='title'][label='Event name']")//paper-input[label='Event name']") > paper-input-container > div:nth-of-type(2) > div > input")
	private WebElement calEventName;
	private By calEventNameBy = By.cssSelector("paper-input[id='title'][label='Event name']");
	
	@FindBy(css = "paper-input[id='description'][label='Event Description'] input")//paper-input[label='Event name']") > paper-input-container > div:nth-of-type(2) > div > input")
	private WebElement calClearDescription;
	private By calClearDescriptionBy = By.cssSelector("paper-input[id='description'][label='Event Description'] input");
	
	@FindBy(id = "description")
	private WebElement calDescription;
	private By calDescriptionBy = By.id("description");
	
	/*@FindBy(css = "paper-toggle-button[class='calendar-event-toggle style-scope calendar-manage-event x-scope paper-toggle-button-0'] > div:nth-of-type(1) > div[id='toggleButton']")
	private WebElement calToggleButton;
	private By calToggleButtonBy = By.cssSelector("paper-toggle-button[class='calendar-event-toggle style-scope calendar-manage-event x-scope paper-toggle-button-0'] > div:nth-of-type(1) > div[id='toggleButton']");
	*/
	@FindBy(css = "calendar-manage-event vaadin-date-picker[name='startDate']")
	private WebElement calStartDateTextBox;
	private By calStartDateTextBoxBy = By.cssSelector("calendar-manage-event vaadin-date-picker[name='startDate']");
			
	@FindBy(css = "calendar-manage-event vaadin-date-picker[name='endDate']")
	private WebElement calEndDateTextBox;
	private By calEndDateTextBoxBy = By.cssSelector("calendar-manage-event vaadin-date-picker[name='endDate']");
	
	@FindBy(css = "paper-time-input[label='Start Time'] paper-input[id='hour']")
	private WebElement calHourStartTimeTextBox;
	private By calHourStartTimeTextBoxBy = By.cssSelector("paper-time-input[label='Start Time'] paper-input[id='hour']");
	
	@FindBy(css = "paper-time-input[label='Start Time'] paper-input[id='min']")
	private WebElement calMinStartTimeTextBox;
	private By calMinStartTimeTextBoxBy = By.cssSelector("paper-time-input[label='Start Time'] paper-input[id='min']");
	
	@FindBy(css = "paper-time-input[label='Start Time'] paper-dropdown-menu")
	private WebElement calDropdownButtonStartTimeAmPm;
	private By calDropdownButtonStartTimeAmPmBy = By.cssSelector("paper-time-input[label='Start Time'] paper-dropdown-menu");
	
	/*@FindBy(css = "paper-time-input[label='Start Time'] paper-item[name='AM']")
	private WebElement calStartTimeAM;
	private By calStartTimeAMBy = By.cssSelector("paper-time-input[label='Start Time'] paper-item[name='AM']");
	
	@FindBy(css = "paper-time-input[label='Start Time'] paper-item[name='PM']")
	private WebElement calStartTimePM;
	private By calStartTimePMBy = By.cssSelector("paper-time-input[label='Start Time'] paper-item[name='PM']");
	*/
	@FindBy(css = "paper-time-input[label='End Time'] paper-input[id='hour']")
	private WebElement calHourEndTimeTextBox;
	private By calHourEndTimeTextBoxBy = By.cssSelector("paper-time-input[label='End Time'] paper-input[id='hour']");
	
	@FindBy(css = "paper-time-input[label='End Time'] paper-input[id='min']")
	private WebElement calMinEndTimeTextBox;
	private By calMinEndTimeTextBoxBy = By.cssSelector("paper-time-input[label='End Time'] paper-input[id='min']");
	
	@FindBy(css = "paper-time-input[label='End Time'] paper-dropdown-menu")
	private WebElement calDropdownButtonEndTimeAmPm;
	private By calDropdownButtonEndTimeAmPmBy = By.cssSelector("paper-time-input[label='End Time'] paper-dropdown-menu");
	
	/*@FindBy(css = "paper-time-input[label='End Time'] paper-item[name='AM']")
	private WebElement calEndTimeAM;
	private By calEndTimeAMBy = By.cssSelector("paper-time-input[label='End Time'] paper-item[name='AM']");
	
	@FindBy(css = "paper-time-input[label='End Time'] paper-item[name='PM']")
	private WebElement calEndTimePM;
	private By calEndTimePMBy = By.cssSelector("paper-time-input[label='End Time'] paper-item[name='PM']");
	*/
	@FindBy(css = "div[id='tabsContent'] > paper-tab:nth-of-type(1)")
	private WebElement calContentTab;
	private By calContentTabBy = By.cssSelector("div[id='tabsContent'] > paper-tab:nth-of-type(1)");
	
	@FindBy(css = "div[id='tabsContent'] > paper-tab:nth-of-type(2)")
	private WebElement calFeaturesTab;
	private By calFeaturesTabBy = By.cssSelector("div[id='tabsContent'] > paper-tab:nth-of-type(2)");
	
	@FindBy(css = "paper-dropdown-menu[label='Select Role']")
	private WebElement calSelectRoleMenu;
	private By calSelectRoleMenuBy = By.cssSelector("paper-dropdown-menu[label='Select Role']");
	
	@FindBy(css = "div[class='done-button style-scope app-management']")
	private WebElement calDone;
	private By calDoneBy = By.cssSelector("div[class='done-button style-scope app-management']");
	
	@FindBy(xpath = "//calendar-manage-event //paper-button[contains(text(),'Clear')]")
	private WebElement calClear;
	private By calClearBy = By.xpath("//calendar-manage-event //paper-button[contains(text(),'Clear')]");
	
	@FindBy(xpath = "//calendar-manage-event //paper-button[contains(text(),'Save')]")
	private WebElement calSave;
	private By calSaveBy = By.xpath("//calendar-manage-event //paper-button[contains(text(),'Save')]");
	
	@FindBy(css = "paper-toast[title='Event saved successfully']")
	private WebElement addedMsgCal;
	private By addedMsgCalBy = By.cssSelector("paper-toast[title='Event saved successfully']");
	
	/*
	@FindBy(css = "paper-toast[title='Please populate all required fields']")
	private WebElement missingInfoMsgAn;
	private By missingInfoMsgAnBy = By.cssSelector("paper-toast[title='Please populate all required fields']");
	*/
	
	
	
	@FindBy(css = "paper-toast[title='Event deleted successfully']")
	private WebElement deletedMsgCal;
	private By deletedMsgCalBy = By.cssSelector("paper-toast[title='Event deleted successfully']");
	
	/*@FindBy(css = "div[id='calendarEventListWrapper'] > paper-listbox > paper-icon-item > paper-icon-button")
	private WebElement calendarDeleteButton;
	private By calendarDeleteButtonBy = By.cssSelector("div[id='calendarEventListWrapper'] > paper-listbox > paper-icon-item > paper-icon-button");
	*/
	@FindBy(css = "paper-button:nth-of-type(1)[class='style-scope confirmation-component x-scope paper-button-0']")
	private WebElement deleteYesCal;
	private By deleteYesCalBy = By.cssSelector("paper-button:nth-of-type(1)[class='style-scope confirmation-component x-scope paper-button-0']");
	
	@FindBy(css = "paper-button:nth-of-type(2)[class='style-scope confirmation-component x-scope paper-button-0']")
	private WebElement deleteNoCal;
	private By deleteNoCalBy = By.cssSelector("paper-button:nth-of-type(2)[class='style-scope confirmation-component x-scope paper-button-0']");
	
	public Calendar()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	/*public void clickOnCalHelp()
	{
		explicitWaitvisibilityOfElementLocated(calHelpBy);
		calHelp.click();
	}
	
	public void clickOnCalShare()
	{
		explicitWaitvisibilityOfElementLocated(calShareBy);
		calShare.click();
	}*/
	
	public void clickOnManageCal()
	{
		//javaScriptExecutor(manageCalBy);
		//explicitWaitvisibilityOfElementLocated(manageCalBy);
		//manageCal.click();
		javaScriptExecutorClick(manageCalBy);
	}
	
	public void clickOnCalFullScreen()
	{
		explicitWaitvisibilityOfElementLocated(calFullScreenBy);
		calFullScreen.click();
	}
	
	public String fetchCalHeader()
	{
		explicitWaitelementToBeClickable(calHeaderBy);
		String msg = calHeader.getText();
		return msg;
	}
	
	/*public void enterCalDisplayName(String calDName)
	{
		explicitWaitvisibilityOfElementLocated(calDisplayNameBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(calDisplayName).click().sendKeys(calDName).perform();
		
	}*/
	
	public void clearCalEventName()
	{
		explicitWaitvisibilityOfElementLocated(calClearEventNameBy);
		calClearEventName.clear();
	}
	
	public void enterCalEventName(String calEName)
	{
		explicitWaitvisibilityOfElementLocated(calEventNameBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(calEventName).click().sendKeys(calEName).perform();
		
		//int cal_size = SuperTestScript.driver.findElements(calEventNameBy).size();
		//SuperTestScript.driver.findElements(calEventNameBy).get(cal_size-1).sendKeys(calEName);
		
	}
	
	public void clearCalDescription()
	{
		explicitWaitvisibilityOfElementLocated(calClearDescriptionBy);
		calClearDescription.clear();
	}
	
	public void enterCalDescription(String calDes)
	{
		explicitWaitvisibilityOfElementLocated(calDescriptionBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(calDescription).click().sendKeys(calDes).perform();
		
		//int desc_size = SuperTestScript.driver.findElements(calDescriptionBy).size();
		//SuperTestScript.driver.findElements(calDescriptionBy).get(desc_size-1).sendKeys(calDes);
	}
	
	/*public void clickOnCalToggleButton()
	{
		explicitWaitvisibilityOfElementLocated(calToggleButtonBy);
		calToggleButton.click();
	}*/
	
	public void clickOnCalContentTab()
	{
		explicitWaitvisibilityOfElementLocated(calContentTabBy);
		calContentTab.click();
	}
	
	public void clickOnCalFeaturesTab()
	{
		explicitWaitvisibilityOfElementLocated(calFeaturesTabBy);
		calFeaturesTab.click();
	}
	
	public void clickOnCalSelectRoleMenu()
	{
		explicitWaitvisibilityOfElementLocated(calSelectRoleMenuBy);
		calSelectRoleMenu.click();
	}
	
	public void clickOnCalDone()
	{
		explicitWaitvisibilityOfElementLocated(calDoneBy);
		calDone.click();
	}
	
	public void clickOnCalCancel()
	{
		explicitWaitvisibilityOfElementLocated(calClearBy);
		calClear.click();
	}
	
	public void clickOnCalSave()
	{
		explicitWaitvisibilityOfElementLocated(calSaveBy);
		calSave.click();
	}
	
	public void clickOnDeleteYesCal()
	{
		explicitWaitvisibilityOfElementLocated(deleteYesCalBy);
		deleteYesCal.click();
	}
	
	public void clickOnDeleteNoCal()
	{
		explicitWaitvisibilityOfElementLocated(deleteNoCalBy);
		deleteNoCal.click();
	}
	
	public String fetchAddedMsgCal()
	{	
		explicitWaitvisibilityOfElementLocated(addedMsgCalBy);
		String msg = addedMsgCal.getText();
		return msg;
	}
	
	public String fetchDeletedMsgCal()
	{	
		explicitWaitvisibilityOfElementLocated(deletedMsgCalBy);
		String msg = deletedMsgCal.getText();
		return msg;
	}
	
	public void selectCalStartDateBox()
	{
		explicitWaitvisibilityOfElementLocated(calStartDateTextBoxBy);
		calStartDateTextBox.click();
		
		//explicitWaitvisibilityOfElementLocated(calStartDateTextBoxBy);
		//calStartDateTextBox.sendKeys(calStartDate);
		
	}
	
	public void selectTodayCalStartDate(String calStartDate)
	{
		//javaScriptExecutor(By.cssSelector("vaadin-date-picker[name='startDate'] div[aria-label='"+ calStartDate +"']"));
		//explicitWaitvisibilityOfElementLocated(By.cssSelector("vaadin-date-picker[name='startDate'] div[aria-label='"+ calStartDate +"']"));
		//WebElement selectAct = SuperTestScript.driver.findElement(By.cssSelector("vaadin-date-picker[name='startDate'] div[aria-label='"+ calStartDate +"']"));
    	//selectAct.click();
		//explicitWaitvisibilityOfElementLocated(calStartDateTextBoxBy);
		//calStartDateTextBox.sendKeys(calStartDate);
    	javaScriptExecutorClick(By.cssSelector("vaadin-date-picker[name='startDate'] div[aria-label='"+ calStartDate +"']"));
	}
	
	public void selectCalEndDateBox()
	{
		explicitWaitvisibilityOfElementLocated(calEndDateTextBoxBy);
		calEndDateTextBox.click();
		
		//explicitWaitvisibilityOfElementLocated(calStartDateTextBoxBy);
		//calStartDateTextBox.sendKeys(calStartDate);
		
	}
	
	public void selectCalEndDate(String calEndDate)
	{
		//WebElement cal = SuperTestScript.driver.findElement(By.cssSelector("vaadin-date-picker[label='End Date'] iron-dropdown > div > vaadin-date-picker-overlay > div[id='scrollers'] > vaadin-infinite-scroller > div > div:nth-of-type(2) > div > vaadin-month-calendar > div[id='monthGrid'] > div[aria-label='"+ calEndDate +"']"));
		//explicitWaitvisibilityOfElementLocated(By.cssSelector("vaadin-date-picker[label='End Date'] iron-dropdown > div > vaadin-date-picker-overlay > div[id='scrollers'] > vaadin-infinite-scroller > div > div:nth-of-type(2) > div > vaadin-month-calendar > div[id='monthGrid'] > div[aria-label='"+ calEndDate +"']"));
		//javaScriptExecutor(By.cssSelector("vaadin-date-picker[name='endDate'] div[aria-label='"+ calEndDate +"']"));
		//explicitWaitvisibilityOfElementLocated(By.cssSelector("vaadin-date-picker[name='endDate'] div[aria-label='"+ calEndDate +"']"));
		//WebElement selectCal = SuperTestScript.driver.findElement(By.cssSelector("vaadin-date-picker[name='endDate'] div[aria-label='"+ calEndDate +"']"));
		//selectCal.click();
		//explicitWaitvisibilityOfElementLocated(calEndDateTextBoxBy);
		//calEndDateTextBox.sendKeys(calEndDate);
		javaScriptExecutorClick(By.cssSelector("vaadin-date-picker[name='endDate'] div[aria-label='"+ calEndDate +"']"));
	}
	
	public void selectCalClearEndDate()
	{
		//WebElement cal = SuperTestScript.driver.findElement(By.cssSelector("vaadin-date-picker[label='End Date'] iron-dropdown > div > vaadin-date-picker-overlay > div[id='scrollers'] > vaadin-infinite-scroller > div > div:nth-of-type(2) > div > vaadin-month-calendar > div[id='monthGrid'] > div[aria-label='"+ calEndDate +"']"));
		//explicitWaitvisibilityOfElementLocated(By.cssSelector("vaadin-date-picker[label='End Date'] iron-dropdown > div > vaadin-date-picker-overlay > div[id='scrollers'] > vaadin-infinite-scroller > div > div:nth-of-type(2) > div > vaadin-month-calendar > div[id='monthGrid'] > div[aria-label='"+ calEndDate +"']"));
		//javaScriptExecutor(By.cssSelector("vaadin-date-picker[name='endDate'] div[aria-label='"+ calEndDate +"']"));
		explicitWaitvisibilityOfElementLocated(By.cssSelector("volute-app-calendar-face2 vaadin-date-picker[name='endDate'] div[id='clear']"));
		WebElement selectCal = SuperTestScript.driver.findElement(By.cssSelector("volute-app-calendar-face2 vaadin-date-picker[name='endDate'] div[id='clear']"));
		selectCal.click();	
		//explicitWaitvisibilityOfElementLocated(calEndDateTextBoxBy);
		//calEndDateTextBox.sendKeys(calEndDate);
		
	}
	
	public void clickOnCalendarSaved(String calEvent)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//div[contains(text( ),'"+ calEvent +"')]/../../paper-icon-button[@icon='icons:create']"));
		WebElement selectEvent = SuperTestScript.driver.findElement(By.xpath("//div[contains(text( ),'"+ calEvent +"')]/../../paper-icon-button[@icon='icons:create']"));
    	selectEvent.click();
	}
	
	public void clickOnCalendarDeleteButton(String calEvent)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//div[contains(text( ),'"+ calEvent +"')]/../../paper-icon-button[@icon='icons:delete']"));
		WebElement selectEvent = SuperTestScript.driver.findElement(By.xpath("//div[contains(text( ),'"+ calEvent +"')]/../../paper-icon-button[@icon='icons:delete']"));
    	selectEvent.click();
	}
	
	public void enterCalHourStartTimeTextBox(String hr)
	{
		explicitWaitvisibilityOfElementLocated(calHourStartTimeTextBoxBy);
		calHourStartTimeTextBox.sendKeys(hr);
		
	}
	
	public void enterCalMinStartTimeTextBox(String min)
	{
		explicitWaitvisibilityOfElementLocated(calMinStartTimeTextBoxBy);
		calMinStartTimeTextBox.sendKeys(min);
		
	}
	
	public void clickCalDropdownButtonStartTimeAmPm()
	{
		explicitWaitvisibilityOfElementLocated(calDropdownButtonStartTimeAmPmBy);
		calDropdownButtonStartTimeAmPm.click();	
	}
	
	/*public void clickCalStartTimeAM()
	{
		explicitWaitvisibilityOfElementLocated(calStartTimeAMBy);
		calStartTimeAM.click();	
	}
	
	public void clickCalStartTimePM()
	{
		explicitWaitvisibilityOfElementLocated(calStartTimePMBy);
		calStartTimePM.click();	
	}*/
	
	public void clickCalStartTimeAMPM(String AmPm)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-time-input[label='Start Time'] paper-item[name='"+ AmPm +"']"));
		WebElement selectTime = SuperTestScript.driver.findElement(By.cssSelector("paper-time-input[label='Start Time'] paper-item[name='"+ AmPm +"']"));
    	selectTime.click();
	}
	
	public void enterCalHourEndTimeTextBox(String hr)
	{
		explicitWaitvisibilityOfElementLocated(calHourEndTimeTextBoxBy);
		calHourEndTimeTextBox.sendKeys(hr);
		
	}
	
	public void enterCalMinEndTimeTextBox(String min)
	{
		explicitWaitvisibilityOfElementLocated(calMinEndTimeTextBoxBy);
		calMinEndTimeTextBox.sendKeys(min);
		
	}
	
	public void clickCalDropdownButtonEndTimeAmPm()
	{
		explicitWaitvisibilityOfElementLocated(calDropdownButtonEndTimeAmPmBy);
		calDropdownButtonEndTimeAmPm.click();	
	}
	
	/*public void clickCalEndTimeAM()
	{
		explicitWaitvisibilityOfElementLocated(calEndTimeAMBy);
		calEndTimeAM.click();	
	}
	
	public void clickCalEndTimePM()
	{
		explicitWaitvisibilityOfElementLocated(calEndTimePMBy);
		calEndTimePM.click();	
	}*/
	
	public void clickCalEndTimeAMPM(String AmPm)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-time-input[label='End Time'] paper-item[name='"+ AmPm +"']"));
		WebElement selectTime = SuperTestScript.driver.findElement(By.cssSelector("paper-time-input[label='End Time'] paper-item[name='"+ AmPm +"']"));
    	selectTime.click();
	}
	
	public void clickCalFaceEvent(String event)
	{
		javaScriptExecutorClick(By.xpath("//section[@id='main'] //div[contains(text(),'"+event+"')]"));
		
	}
	
	public void clickCalBackButton(String event)
	{
		javaScriptExecutorClick(By.xpath("//h4[contains(text(),'"+event+"')]/../.. //paper-fab[@icon]"));
		
	}
	
	public String fetchFacesCalShows(String calStr)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//app-management //div[contains(text(),'"+calStr+"')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchListFacesCalShows(String calStr)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//section[@id='mobile-dates'] //div[contains(text(),'"+calStr+"')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
}
