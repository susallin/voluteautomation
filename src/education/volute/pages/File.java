package education.volute.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import education.volute.common_lib.SuperTestScript;

public class File extends NavigatorTool {

	/*@FindBy(id = "Help")
	private WebElement contentIntHelp;
	private By contentIntHelpBy = By.id("Help");
	
	@FindBy(id = "Share")
	private WebElement contentIntShare;
	private By contentIntShareBy = By.id("Share");*/
	@FindBy(css = "paper-fab[id^='Files'][title^='Manage']")
	private WebElement manageFiles;
	private By manageFilesBy = By.cssSelector("paper-fab[id^='Files'][title^='Manage']");

	@FindBy(id = "fullscreen")
	private WebElement contentIntFullScreen;
	private By contentIntFullScreenBy = By.id("fullscreen");
	
	@FindBy(css = "div[title='Content Integration']")
	private WebElement contentIntHeader;
	private By contentIntHeaderBy = By.cssSelector("div[title='Content Integration']");
	
	@FindBy(css = "paper-input[label='Display Name']")
	private WebElement contentIntDisplayName;
	private By contentIntDisplayNameBy = By.cssSelector("paper-input[label='Display Name']");
	
	@FindBy(css = "vaadin-upload[title='Uploader'] paper-button[id='addFiles']")
	private WebElement contentIntSelectFile;
	private By contentIntSelectFileBy = By.cssSelector("vaadin-upload[title='Uploader'] paper-button[id='addFiles']");
	
	@FindBy(css = "paper-button[title='Save File']")
	private WebElement contentIntFileSave;
	private By contentIntFileSaveBy = By.cssSelector("paper-button[title='Save File']");
	
	@FindBy(css = "paper-button[title='Cancel CI File']")
	private WebElement contentIntFileCancel;
	private By contentIntFileCancelBy = By.cssSelector("paper-button[title='Cancel CI File']");
	
	@FindBy(css = "div[id='tabsContent'] > paper-tab:nth-of-type(1)")
	private WebElement contentIntContentTab;
	private By contentIntContentTabBy = By.cssSelector("div[id='tabsContent'] > paper-tab:nth-of-type(1)");
	
	@FindBy(css = "div[id='tabsContent'] > paper-tab:nth-of-type(2)")
	private WebElement contentIntFeaturesTab;
	private By contentIntFeaturesTabBy = By.cssSelector("div[id='tabsContent'] > paper-tab:nth-of-type(2)");
	
	@FindBy(css = "paper-dropdown-menu[label='Select Role']")
	private WebElement contentIntSelectRoleMenu;
	private By contentIntSelectRoleMenuBy = By.cssSelector("paper-dropdown-menu[label='Select Role']");
	
	@FindBy(css = "paper-toast[title='File added successfully']")
	private WebElement addedMsgContentInt;
	private By addedMsgContentIntBy = By.cssSelector("paper-toast[title='File added successfully']");
	
	/*@FindBy(css = "div[class='file-list style-scope ci-manager'] > paper-listbox > paper-icon-item > paper-icon-button")
	private WebElement contentIntDeleteButton;
	private By contentIntDeleteButtonBy = By.cssSelector("div[class='file-list style-scope ci-manager'] > paper-listbox > paper-icon-item > paper-icon-button");
	*/	
	@FindBy(css = "paper-button:nth-of-type(1)[class='style-scope confirmation-component x-scope paper-button-0']")
	private WebElement deleteYesContentInt;
	private By deleteYesContentIntBy = By.cssSelector("paper-button:nth-of-type(1)[class='style-scope confirmation-component x-scope paper-button-0']");
	
	@FindBy(css = "paper-button:nth-of-type(1)[class='style-scope confirmation-component x-scope paper-button-0']")
	private WebElement deleteNoContentInt;
	private By deleteNoContentIntBy = By.cssSelector("paper-button:nth-of-type(2)[class='style-scope confirmation-component x-scope paper-button-0']");
	
	@FindBy(css = "volute-app[id='UUID0105'] paper-fab[icon='icons:arrow-back']")
	private WebElement contentIntDoneButton;
	private By contentIntDoneButtonBy = By.cssSelector("volute-app[id='UUID0105'] paper-fab[icon='icons:arrow-back']");
	
	public File()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	/*public void clickOnContentIntHelp()
	{
		explicitWaitvisibilityOfElementLocated(contentIntHelpBy);
		contentIntHelp.click();
	}
	
	public void clickOnContentIntShare()
	{
		explicitWaitvisibilityOfElementLocated(contentIntShareBy);
		contentIntShare.click();
	}*/
	
	public void clickOnContentIntManage()
	{
		//explicitWaitvisibilityOfElementLocated(manageFilesBy);
		//manageFiles.click();
		javaScriptExecutorClick(manageFilesBy);
	}
	
	public void clickOnContentIntFullScreen()
	{
		explicitWaitvisibilityOfElementLocated(contentIntFullScreenBy);
		contentIntFullScreen.click();
	}
	
	public String fetchContentIntHeader()
	{
		explicitWaitelementToBeClickable(contentIntHeaderBy);
		String msg = contentIntHeader.getText();
		return msg;
	}
	
	public void enterContentIntDisplayName(String contDName)
	{
		explicitWaitvisibilityOfElementLocated(contentIntDisplayNameBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(contentIntDisplayName).click().sendKeys(contDName).perform();
		
	}
	
	public void clickOnContentIntSelectFile(String fileName) throws Exception
	{
		javaScriptExecutorUpload(contentIntSelectFileBy, fileName);
		//explicitWaitvisibilityOfElementLocated(contentIntSelectFileBy);
		//contentIntSelectFile.click();
	}
	
	public void clickOnCancelContentFile()
	{
		explicitWaitvisibilityOfElementLocated(contentIntFileCancelBy);
		contentIntFileCancel.click();
	}
	
	public void clickOnSaveContentFile()
	{
		explicitWaitvisibilityOfElementLocated(contentIntFileSaveBy);
		contentIntFileSave.click();
	}
	
	public String fetchContentIntAddMsg()
	{	
		explicitWaitvisibilityOfElementLocated(addedMsgContentIntBy);
		String msg = addedMsgContentInt.getText();
		return msg;
	}
	
	/*public void fetchContentIntFile(String sliName)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//paper-item-body[text()='"+sliName+"']"));
		WebElement selectSli = SuperTestScript.driver.findElement(By.xpath("//paper-item-body[text()='"+sliName+"']"));
    	selectSli.click();
    	
	}*/
	
	public void clickOnContentIntDeleteButton(String contDel)
	{
		javaScriptExecutor(By.cssSelector("paper-icon-button[title='Delete "+contDel+"']"));
		/*explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-icon-button[title='Delete "+contDel+"']"));
		WebElement delCont = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-button[title='Delete "+contDel+"']"));
    	delCont.click();*/
	}
	
	public String fetchContShows(String contStr)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("paper-icon-button[title='Delete "+contStr+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchContIntFaceTool(String contStr)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("paper-icon-item[title='File "+contStr+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnDeleteYesContentInt()
	{
		explicitWaitvisibilityOfElementLocated(deleteYesContentIntBy);
		deleteYesContentInt.click();
	}
	
	public void clickOnDeleteNoContent()
	{
		explicitWaitvisibilityOfElementLocated(deleteNoContentIntBy);
		deleteNoContentInt.click();
	}
	
	public void clickOnContentIntContentTab()
	{
		explicitWaitvisibilityOfElementLocated(contentIntContentTabBy);
		contentIntContentTab.click();
	}
	
	public void clickOnContentIntFeaturesTab()
	{
		explicitWaitvisibilityOfElementLocated(contentIntFeaturesTabBy);
		contentIntFeaturesTab.click();
	}
	
	public void clickOnContentIntSelectRoleMenu()
	{
		explicitWaitvisibilityOfElementLocated(contentIntSelectRoleMenuBy);
		contentIntSelectRoleMenu.click();
	}
	
	public void selectContIntRole(String contRoleNum)
	{
		WebElement contInt = SuperTestScript.driver.findElement(By.cssSelector("paper-listbox > paper-item:nth-of-type("+contRoleNum+")"));
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-listbox > paper-item:nth-of-type("+contRoleNum+")"));
		contInt.click();
	}
	
	public void clickOnContentIntDoneButton()
	{
		explicitWaitvisibilityOfElementLocated(contentIntDoneButtonBy);
		contentIntDoneButton.click();
	}
}
