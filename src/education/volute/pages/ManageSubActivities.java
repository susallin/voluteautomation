package education.volute.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import education.volute.common_lib.SuperTestScript;

public class ManageSubActivities extends NavigatorTool
{
	
	@FindBy(css = "activity-upsert[class='subActivity style-scope activity-summary'] > form > paper-input[title='Activity Name']")
	private WebElement subActNameTextBox;
	private By subActNameTextBoxBy = By.cssSelector("activity-upsert[class='subActivity style-scope activity-summary'] > form > paper-input[title='Activity Name']");
	
	@FindBy(css = "activity-upsert[class='style-scope activity-summary'] > form > paper-input[title='Activity Name']")
	private WebElement subActNameTextBox1;
	private By subActNameTextBox1By = By.cssSelector("activity-upsert[class='style-scope activity-summary'] > form > paper-input[title='Activity Name']");
	
	@FindBy(css = "input[title='Activity Start Date']")
	private WebElement subActStartDateTextBox;
	private By subActStartDateTextBoxBy = By.cssSelector("input[title='Activity Start Date']");
			
	@FindBy(css = "input[title='Activity End Date']")
	private WebElement subActEndDateTextBox;
	private By subActEndDateTextBoxBy = By.cssSelector("input[title='Activity End Date']");
			
	
	
	private By subActDescTextBoxBy = By.id("textarea");
	
	@FindBy(css = "paper-item[class='activity-sub-panel-list-item style-scope activity-summary x-scope paper-item-5'] > activity-summary > div:nth-of-type(2) > activity-upsert > form[id='saveActivityForm'] > div:nth-of-type(6) > div > div > paper-toggle-button > div:nth-of-type(1) > div[id='toggleButton']")
	private WebElement publishToggleButton;
	private By publishToggleButtonBy = By.cssSelector("paper-item[class='activity-sub-panel-list-item style-scope activity-summary x-scope paper-item-5'] > activity-summary > div:nth-of-type(2) > activity-upsert > form[id='saveActivityForm'] > div:nth-of-type(6) > div > div > paper-toggle-button > div:nth-of-type(1) > div[id='toggleButton']");
	
	@FindBy(css = "activity-upsert[class='subActivity style-scope activity-summary'] > form > div:nth-of-type(7) > paper-button[title='Save Activity']")
	private WebElement saveSubActButton;
	private By saveSubActButtonBy = By.cssSelector("activity-upsert[class='subActivity style-scope activity-summary'] > form > div:nth-of-type(7) > paper-button[title='Save Activity']");
		
	@FindBy(css = "paper-toast[title = 'Activity created successfully']>span[id='label'][class='style-scope paper-toast']")
	private WebElement subActSavedMsg;
	private By subActSavedMsgBy = By.cssSelector("paper-toast[title = 'Activity created successfully']>span[id='label'][class='style-scope paper-toast']");
	
	@FindBy(xpath = "//paper-button[text()='Assign Learning Space']")
	private WebElement assignLSButton;
	private By assignLSButtonBy = By.xpath("//paper-button[text()='Assign Learning Space']");
	
	@FindBy(css = "activity-upsert[class='subActivity style-scope activity-summary'] > form[id='saveActivityForm'] > div:nth-of-type(4) > div:nth-of-type(2) > div:nth-of-type(1) > paper-toggle-button > div:nth-of-type(1) > div[id='toggleButton']")
	private WebElement socialToggleButton;
	private By socialToggleButtonBy = By.cssSelector("activity-upsert[class='subActivity style-scope activity-summary'] > form[id='saveActivityForm'] > div:nth-of-type(4) > div:nth-of-type(2) > div:nth-of-type(1) > paper-toggle-button > div:nth-of-type(1) > div[id='toggleButton']");
	
	@FindBy(css = "activity-upsert[class='subActivity style-scope activity-summary'] > form[id='saveActivityForm'] > div:nth-of-type(4) > div:nth-of-type(2) > div:nth-of-type(2) > paper-toggle-button > div:nth-of-type(1) > div[id='toggleButton']")
	private WebElement exclusiveToggleButton;
	private By exclusiveToggleButtonBy = By.cssSelector("activity-upsert[class='subActivity style-scope activity-summary'] > form[id='saveActivityForm'] > div:nth-of-type(4) > div:nth-of-type(2) > div:nth-of-type(2) > paper-toggle-button > div:nth-of-type(1) > div[id='toggleButton']");
	
	@FindBy(css = "assign-module[id='assignment'] > paper-button")
	private WebElement assignUsersDoneButton;
	private By assignUsersDoneButtonBy = By.cssSelector("assign-module[id='assignment'] > paper-button");
	
	@FindBy(xpath = "/html/body/volute-container/neon-animated-pages/volute-app-fullsize/div[2]/div/volute-app/div[1]/div/neon-animated-pages/neon-animatable[2]/volute-app-face/roster-face2/div[2]/activities-panel/div/div/div/iron-selector/paper-item[11]/activity-summary/div[3]/paper-listbox/paper-item[1]/activity-summary/div[2]/activity-upsert/form/div[6]/div/div/paper-toggle-button/div[1]/div[2]")
	private WebElement unpublishToggleButton;
	private By unpublishToggleButtonBy = By.xpath("/html/body/volute-container/neon-animated-pages/volute-app-fullsize/div[2]/div/volute-app/div[1]/div/neon-animated-pages/neon-animatable[2]/volute-app-face/roster-face2/div[2]/activities-panel/div/div/div/iron-selector/paper-item[11]/activity-summary/div[3]/paper-listbox/paper-item[1]/activity-summary/div[2]/activity-upsert/form/div[6]/div/div/paper-toggle-button/div[1]/div[2]");
	
	@FindBy(css = "paper-toast[title = 'Activity updated successfully']>span[id='label'][class='style-scope paper-toast']")
	private WebElement subActEditsSavedMsg;
	private By subActEditsSavedMsgBy = By.cssSelector("paper-toast[title = 'Activity updated successfully']>span[id='label'][class='style-scope paper-toast']");
		
	@FindBy(css = "paper-toast[title = 'Sub-Activity deleted successfully']>span[id='label'][class='style-scope paper-toast']")
	private WebElement subActDeletedMsg;
	private By subActDeleteddMsgBy = By.cssSelector("paper-toast[title = 'Sub-Activity deleted successfully']>span[id='label'][class='style-scope paper-toast']");
	
	public ManageSubActivities()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	public void enterEditSubActName(String subActName, String subActName2)
	{
		WebElement nameSubAc = SuperTestScript.driver.findElement(By.cssSelector("paper-item[title='"+ subActName +"'] > activity-summary > div:nth-of-type(2) > activity-upsert > form[id='saveActivityForm'] > paper-input[label='Activity Name']"));
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(nameSubAc).click().sendKeys(subActName2).perform();
	}
	
	public void enterSubActName(String subActName)
	{
		explicitWaitvisibilityOfElementLocated(subActNameTextBoxBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(subActNameTextBox).click().sendKeys(subActName).perform();

//		int subActName_size = SuperTestScript.driver.findElements(subActNameTextBoxBy).size();
//		SuperTestScript.driver.findElements(subActNameTextBoxBy).get(subActName_size-1).sendKeys(subActName);
	}
		
	public void editSubActName(String subActName)
	{
		explicitWaitvisibilityOfElementLocated(subActNameTextBox1By);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(subActNameTextBox1).click().sendKeys(subActName).perform();

//		int subActName_size = SuperTestScript.driver.findElements(subActNameTextBoxBy).size();
//		SuperTestScript.driver.findElements(subActNameTextBoxBy).get(subActName_size-1).sendKeys(subActName);
	}
	
	public void enterSubActstartDate(String subActStartDate)
	{
		explicitWaitvisibilityOfElementLocated(subActStartDateTextBoxBy);
		subActStartDateTextBox.sendKeys(subActStartDate);
		
	}
	
	public void enterSubActEndDate(String subActEndDate)
	{
		explicitWaitvisibilityOfElementLocated(subActEndDateTextBoxBy);
		subActEndDateTextBox.sendKeys(subActEndDate);
		
	}
	
	public void enterEditSubActDesc(String SubActName, String SubActDesc)
	{
		WebElement descSubAc = SuperTestScript.driver.findElement(By.cssSelector("paper-item[title='"+ SubActName +"'] > activity-summary > div:nth-of-type(2) > activity-upsert > form[id='saveActivityForm'] > paper-textarea[title='Activity Description']"));
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(descSubAc).click().sendKeys(SubActDesc).perform();
	}
	
	public void enterSubActDesc(String subActDesc)
	{
		//explicitWaitvisibilityOfElementLocated(pgmDescTextBoxBy);
		int desc_size = SuperTestScript.driver.findElements(subActDescTextBoxBy).size();
		SuperTestScript.driver.findElements(subActDescTextBoxBy).get(desc_size-1).sendKeys(subActDesc);
	}
	
	public void clickOnEditAssignLSButton(String SubActLs)
	{
		WebElement buttonLs = SuperTestScript.driver.findElement(By.xpath("//paper-item[@title='"+ SubActLs +"']/activity-summary/div[2]/activity-upsert/form/div[2]/paper-button[text()='Assign Mashup']"));
		buttonLs.click();
	}
	
	public void clickOnAssignLSButton()
	{
		explicitWaitvisibilityOfElementLocated(assignLSButtonBy);
		int assignLS_size = SuperTestScript.driver.findElements(assignLSButtonBy).size();
		SuperTestScript.driver.findElements(assignLSButtonBy).get(assignLS_size-1).click();
		
	}
	
	public void clickOnSaveSubAct()
	{
		explicitWaitvisibilityOfElementLocated(saveSubActButtonBy);
		saveSubActButton.click();
	}
	
	public void clickOnEditPublishToggleButton(String SubActName)
	{
		WebElement arrowAc = SuperTestScript.driver.findElement(By.cssSelector("paper-item[title='"+ SubActName +"'] > activity-summary > div:nth-of-type(2) > activity-upsert > form[id='saveActivityForm'] > div:nth-of-type(8) > div > div > paper-toggle-button > div:nth-of-type(1) > div[id='toggleButton']"));
		arrowAc.click();
	}
	
	public void clickOnPublishToggleButton(String subActName)
	{
		//explicitWaitvisibilityOfElementLocated(publishToggleButtonBy);
		//publishToggleButton.click();
		
		WebElement arrowSubAc = SuperTestScript.driver.findElement(By.cssSelector("paper-item[title='"+ subActName +"'] > activity-summary > div:nth-of-type(1) > activity-upsert > form[id='saveActivityForm'] > div:nth-of-type(6) > div > div > paper-toggle-button > div:nth-of-type(1) > div[id='toggleButton']"));
		arrowSubAc.click();
	}
	
	
	public String fetchSuccessMsg()
	{
		
		explicitWaitvisibilityOfElementLocated(subActSavedMsgBy);
		String msg = subActSavedMsg.getText();
		return msg;
	}
	
	public void clickOnEditSocialToggleButton(String SubActSoc)
	{
		WebElement buttonSoc = SuperTestScript.driver.findElement(By.cssSelector("paper-item[title='"+ SubActSoc +"'] > activity-summary > div:nth-of-type(2) > activity-upsert > form[id='saveActivityForm'] > div:nth-of-type(4) > div:nth-of-type(3) > div:nth-of-type(1) > paper-toggle-button > div:nth-of-type(1) > div[id='toggleButton']"));
		buttonSoc.click();
	}
	
	public void clickOnSocialToggleButton()
	{
		explicitWaitvisibilityOfElementLocated(socialToggleButtonBy);
		socialToggleButton.click();
	}
	
	public void clickOnEditExclusiveToggleButton(String actExc)
	{
		WebElement buttonExc = SuperTestScript.driver.findElement(By.cssSelector("paper-item[title='"+ actExc +"'] > activity-summary > div:nth-of-type(2) > activity-upsert > form[id='saveActivityForm'] > div:nth-of-type(4) > div:nth-of-type(3) > div:nth-of-type(2) > paper-toggle-button > div:nth-of-type(1) > div[id='toggleButton']"));
		buttonExc.click();
	}
	
	public void clickOnExclusiveToggleButton()
	{
		explicitWaitvisibilityOfElementLocated(exclusiveToggleButtonBy);
		exclusiveToggleButton.click();
	}
	
	public void selectAllindividuals()
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-listbox > paper-icon-item[class='style-scope assign-module x-scope paper-icon-item-0']"));
				List<WebElement> allIndividuals = SuperTestScript.driver.findElements(By.cssSelector("paper-listbox > paper-icon-item[class='style-scope assign-module x-scope paper-icon-item-0']"));
		for(int i=0; i<=allIndividuals.size()-9; i++)
		{
			allIndividuals.get(i).click();
		}
	}
	
	public void clickOnAssignUsersDoneButton()
	{
		explicitWaitvisibilityOfElementLocated(assignUsersDoneButtonBy);
		assignUsersDoneButton.click();
	}
	
	public void clickOnEditSaveSubAct(String SubActSave)
	{
		WebElement buttonSave = SuperTestScript.driver.findElement(By.cssSelector("paper-item[title='"+ SubActSave +"'] > activity-summary > div:nth-of-type(2) > activity-upsert > form > div:nth-of-type(9) > paper-button[title='Save Activity']"));
		buttonSave.click();
	}
	
	public void clickOnUnpublishToggleButton()
	{
		explicitWaitvisibilityOfElementLocated(unpublishToggleButtonBy);
		unpublishToggleButton.click();
	}
	
	public String fetchSubActEditsSavedSuccessMsg()
	{
		
		explicitWaitvisibilityOfElementLocated(subActEditsSavedMsgBy);
		String msg = subActEditsSavedMsg.getText();
		return msg;
	}
	
	public void clickOnDeleteSubActicon(String actName)
	{
		WebElement buttonDelete = SuperTestScript.driver.findElement(By.cssSelector("paper-item[title='"+ actName +"'] > activity-summary > paper-icon-item > span:nth-of-type(1) > paper-icon-button[title='Delete Activity']"));
		buttonDelete.click();
	}
	
	public String fetchSubActDeletedMsg()
	{
		
		explicitWaitvisibilityOfElementLocated(subActDeleteddMsgBy);
		String msg = subActDeletedMsg.getText();
		return msg;
	}
	

}

