package education.volute.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import education.volute.common_lib.SuperTestScript;

public class RoomsTool extends NavigatorTool
{
	@FindBy(css = "paper-input[label='Search Rooms']")
	private WebElement searchRoom;
	private By searchRoomBy = By.cssSelector("paper-input[label='Search Rooms']");
	
	@FindBy(css = "paper-listbox[id='roomListbox'] > paper-item")
	private WebElement selectRoom;
	private By selectRoomBy = By.cssSelector("paper-listbox[id='roomListbox'] > paper-item");
	
	@FindBy(css = "paper-icon-button[title='Go to Room']")
	private WebElement launchRoom;
	private By launchRoomBy = By.cssSelector("paper-icon-button[title='Go to Room']");
	
	@FindBy(id = "Close")
	private WebElement closeRoom;
	private By closeRoomBy = By.id("Close");
	
	@FindBy(css = "paper-button[class='assign-button style-scope volute-app-assignment-face2 x-scope paper-button-0']")
	private WebElement assignRoom;
	private By assignRoomBy = By.cssSelector("paper-button[class='assign-button style-scope volute-app-assignment-face2 x-scope paper-button-0']");
	
	@FindBy(css = "div[class='module-inner style-scope mashup-navigator']")
	private WebElement roomHeader;
	private By roomHeaderBy = By.cssSelector("div[class='module-inner style-scope mashup-navigator']");
	
	public RoomsTool()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	public void enterSearchRoom(String roomTxt)
	{
		explicitWaitvisibilityOfElementLocated(searchRoomBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(searchRoom).click().sendKeys(roomTxt).perform();
		
	}
	
	public void clickOnRoom()
	{
		explicitWaitvisibilityOfElementLocated(selectRoomBy);
		selectRoom.click();
	}
	
	public void launchOnRoom()
	{
		explicitWaitvisibilityOfElementLocated(launchRoomBy);
		launchRoom.click();
	}
	
	public void clickOnLaunchOfaRoom(String roomName)
	{
		SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'"+roomName+"')]/../..//paper-icon-button[@title='Go to Room']")).click();
	}
	
	public void clickOnCloseRoom()
	{
		explicitWaitvisibilityOfElementLocated(closeRoomBy);
		closeRoom.click();
	}
	
	public void clickOnAssignRoom()
	{
		explicitWaitvisibilityOfElementLocated(assignRoomBy);
		assignRoom.click();
	}
	/* Missing Toast message on when sharing an act/sub to a room.
	 * 
	public String fetchRoomSuccessMsg()
	{
		
		explicitWaitvisibilityOfElementLocated(profileSavedMsgBy);
		String msg = profileSavedMsg.getText();
		return msg;
	}*/
	
	public String fetchroomHeader()
	{
		explicitWaitelementToBeClickable(roomHeaderBy);
		String msg = roomHeader.getText();
		return msg;
	}

}
