package education.volute.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import education.volute.common_lib.SuperTestScript;

public class PrivateMarketplace extends NavigatorTool {

	@FindBy(css = "sub-menu paper-tab:nth-of-type(1)")
	private WebElement pmTab;
	private By pmTabBy = By.cssSelector("sub-menu paper-tab:nth-of-type(1)");
	
	@FindBy(css = "sub-menu paper-tab:nth-of-type(2)")
	private WebElement cmTab;
	private By cmTabBy = By.cssSelector("sub-menu paper-tab:nth-of-type(2)");
	
	@FindBy(css = "sub-menu paper-tab:nth-of-type(3)")
	private WebElement cartTab;
	private By cartTabBy = By.cssSelector("sub-menu paper-tab:nth-of-type(3)");
	
	@FindBy(css = "div[id='topBar'] > paper-input[label='Search']")
	private WebElement searchPM;
	private By searchPMBy = By.cssSelector("div[id='topBar'] > paper-input[label='Search']");
	
	@FindBy(css = "div[id='topBar'] > div:nth-of-type(1) > paper-checkbox:nth-of-type(1) > div[id='checkboxContainer']")
	private WebElement toolCheckBoxPM;
	private By toolCheckBoxPMBy = By.cssSelector("div[id='topBar'] > div:nth-of-type(1) > paper-checkbox:nth-of-type(1) > div[id='checkboxContainer']");
	
	@FindBy(css = "div[id='topBar'] > div:nth-of-type(1) > paper-checkbox:nth-of-type(2) > div[id='checkboxContainer']")
	private WebElement bundlesCheckBoxPM;
	private By bundlesCheckBoxPMBy = By.cssSelector("div[id='topBar'] > div:nth-of-type(1) > paper-checkbox:nth-of-type(2) > div[id='checkboxContainer']");
	
	@FindBy(css = "div[id='topBar'] > div:nth-of-type(3) > paper-dropdown-menu > paper-menu-button")
	private WebElement sortPM;
	private By sortPMBy = By.cssSelector("div[id='topBar'] > div:nth-of-type(3) > paper-dropdown-menu > paper-menu-button");
	
	@FindBy(css = "paper-button[class='view-guides style-scope tool-module-pvt x-scope paper-button-0']")
	private WebElement viewGuidesPM;
	private By viewGuidesPMBy = By.cssSelector("paper-button[class='view-guides style-scope tool-module-pvt x-scope paper-button-0']");
	
	@FindBy(css = "paper-icon-button[title='Back to Tool List']")
	private WebElement backToListButton;
	private By backToListButtonBy = By.cssSelector("paper-icon-button[title='Back to Tool List']");
	
	@FindBy(xpath = "//div[text()[contains(.,'Role Access')]]")
	private WebElement roleAcTab;
	private By roleAcTabBy = By.xpath("//div[text()[contains(.,'Role Access')]]");
	
	@FindBy(xpath = "//div[text()[contains(.,'Feature Access')]]")
	private WebElement fetAcTab;
	private By fetAcTabBy = By.xpath("//div[text()[contains(.,'Feature Access')]]");
	
	@FindBy(css = "paper-toast[text='Change Success']")
	private WebElement confirmMsg;
	private By confirmMsgBy = By.cssSelector("paper-toast[text='Change Success']");
	
	public PrivateMarketplace()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	public void enterSearchPM(String srcName)
	{
		explicitWaitvisibilityOfElementLocated(searchPMBy);
		searchPM.click();
		searchPM.sendKeys(srcName);
		
	}
	
	public void clickOnToolCheckBoxPM()
	{
		explicitWaitvisibilityOfElementLocated(toolCheckBoxPMBy);
		toolCheckBoxPM.click();
	}
	
	public void clickOnBundlesCheckBoxPM()
	{
		explicitWaitvisibilityOfElementLocated(bundlesCheckBoxPMBy);
		bundlesCheckBoxPM.click();
	}
	
	public void clickOnSortPM()
	{
		explicitWaitvisibilityOfElementLocated(sortPMBy);
		sortPM.click();
	}

	public void clickOnViewGuidesPM()
	{
		explicitWaitvisibilityOfElementLocated(viewGuidesPMBy);
		viewGuidesPM.click();
	}
	
	public void clickOnPmTab()
	{
		explicitWaitvisibilityOfElementLocated(pmTabBy);
		pmTab.click();
	}
	
	public void clickOnCmTab()
	{
		explicitWaitvisibilityOfElementLocated(cmTabBy);
		cmTab.click();
	}
	
	public void clickOnCartTab()
	{
		explicitWaitvisibilityOfElementLocated(cartTabBy);
		cartTab.click();
	}
	
	public void clickToolMarket(String tool)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("img[title^='"+tool+"']"));
		WebElement selectTool = SuperTestScript.driver.findElement(By.cssSelector("img[title^='"+tool+"']"));
    	selectTool.click();
	}
	
	public void clickOnBackToListButton()
	{
		explicitWaitvisibilityOfElementLocated(backToListButtonBy);
		backToListButton.click();
	}
	
	public void clickOnRoleAcTab()
	{
		explicitWaitvisibilityOfElementLocated(roleAcTabBy);
		roleAcTab.click();
	}
	
	public void clickOnFetAcTab()
	{
		explicitWaitvisibilityOfElementLocated(fetAcTabBy);
		fetAcTab.click();
	}
	
	// n determines the roles 2(facilitator), 3(participant), 4(observer)
	public void clickCheckRoleFetAccess(String fetName, String n)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//td[contains(text(),'"+fetName+"')]/.."+n+"/put-delete-check"));
		WebElement selectCheckbox = SuperTestScript.driver.findElement(By.xpath("//td[contains(text(),'"+fetName+"')]/.."+n+"/put-delete-check"));
    	selectCheckbox.click();
	}
	
	// n determines the roles 2(facilitator), 3(participant), 4(observer)
	public String fetchCheckbox(String fetName, String n)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//td[contains(text(),'"+fetName+"')]/.."+n+" //paper-checkbox[@aria-checked='true']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchComfirmationMsg()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(confirmMsgBy);
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
}
