package education.volute.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import education.volute.common_lib.SuperTestScript;

public class Notes extends NavigatorTool
{
	//@FindBy(css = "paper-button[class='save-button style-scope volute-notes-face1 x-scope paper-button-0']")
	//private WebElement addNotesButton;
	//private By addNotesButtonBy = By.cssSelector("paper-button[class='save-button style-scope volute-notes-face1 x-scope paper-button-0']");
	@FindBy(css = "volute-notes-face1 paper-fab[title='Add Note']")
	private WebElement addNotesButton;
	private By addNotesButtonBy = By.cssSelector("volute-notes-face1 paper-fab[title='Add Note']");
	
	//@FindBy(css = "paper-input[label='Title']")
	//private WebElement noteTitleTextBox;
	//private By noteTitleTextBoxBy = By.cssSelector("paper-input[label='Title']");
	@FindBy(id = "noteName")
	private WebElement noteTitleTextBox;
	private By noteTitleTextBoxBy = By.id("noteName");
	
	//@FindBy(css = "paper-input[label='Tags']")
	//private WebElement noteTagTextBox;
	//private By noteTagTextBoxBy = By.cssSelector("paper-input[label='Tags']");
	@FindBy(id = "noteTags")
	private WebElement noteTagTextBox;
	private By noteTagTextBoxBy = By.id("noteTags");
	
	//@FindBy(css = "textarea[label='Note']")
	//private WebElement writeNotesTextBox;
	//private By writeNotesTextBoxBy = By.cssSelector("textarea[label='Note']");
	@FindBy(id = "noteDescription")
	private WebElement writeNotesTextBox;
	private By writeNotesTextBoxBy = By.id("noteDescription");
	
	@FindBy(id = "volute-notes-face1 > fab-module > div > paper-fab[id='Open Search']")
	private WebElement searchNotesTextBox;
	private By searchNotesTextBoxBy = By.id("volute-notes-face1 > fab-module > div > paper-fab[id='Open Search']");
	
	@FindBy(css = "paper-listbox[id='notes-list'] > paper-item:nth-of-type(1) > paper-icon-item > paper-icon-button[id='deleteNote']")
	private WebElement deleteNotesTextBox;
	private By deleteNotesTextBoxBy = By.cssSelector("paper-listbox[id='notes-list'] > paper-item:nth-of-type(1) > paper-icon-item > paper-icon-button[id='deleteNote']");
	
	@FindBy(css = "paper-button:nth-of-type(1)[class='style-scope confirmation-component x-scope paper-button-0']")
	private WebElement deleteYesNotes;
	private By deleteYesNotesBy = By.cssSelector("paper-button:nth-of-type(1)[class='style-scope confirmation-component x-scope paper-button-0']");
	
	@FindBy(css = "paper-button:nth-of-type(1)[class='style-scope confirmation-component x-scope paper-button-0']")
	private WebElement deleteNoNotes;
	private By deleteNoNotesBy = By.cssSelector("paper-button:nth-of-type(2)[class='style-scope confirmation-component x-scope paper-button-0']");

	@FindBy(css = "paper-toast[title = 'Note deleted successfully.']>span[id='label'][class='style-scope paper-toast']")
	private WebElement noteDeletedMsg;
	private By noteDeletedMsgBy = By.cssSelector("paper-toast[title = 'Note deleted successfully.']>span[id='label'][class='style-scope paper-toast']");
	
	@FindBy(css = "paper-fab[title='Back to list of notes']")
	private WebElement notesHeader;
	private By notesHeaderBy = By.cssSelector("paper-fab[title='Back to list of notes']");
	
	@FindBy(css = "volute-app[id='UUID6453'] > div[id='AppRoot'] > volute-app-footer > paper-toolbar > div[id='topBar'] > div:nth-of-type(2) > paper-icon-button")
	private WebElement closeNotesButton;
	private By closeNotesButtonBy = By.cssSelector("volute-app[id='UUID6453'] > div[id='AppRoot'] > volute-app-footer > paper-toolbar > div[id='topBar'] > div:nth-of-type(2) > paper-icon-button");
			
	@FindBy(css = "paper-fab[title='Back to list of notes']")
	private WebElement backNotesButton;
	private By backNotesButtonBy = By.cssSelector("paper-fab[title='Back to list of notes']");	
			
	public Notes()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	public void clickOnAddNotesBtn()
	{
		explicitWaitvisibilityOfElementLocated(addNotesButtonBy);
		addNotesButton.click();		
	}
	
	public void enterNotesTitle(String noteTitle)
	{
		explicitWaitvisibilityOfElementLocated(noteTitleTextBoxBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(noteTitleTextBox).click().sendKeys(noteTitle).perform();
		
	}
	
	public void enterNotesTag(String noteTag)
	{
		explicitWaitvisibilityOfElementLocated(noteTagTextBoxBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(noteTagTextBox).click().sendKeys(noteTag).perform();
		
	}
	
	public void enterNoteTextHere(String noteText)
	{
		explicitWaitvisibilityOfElementLocated(writeNotesTextBoxBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(writeNotesTextBox).click().sendKeys(noteText).perform();
		
	}
	
	public String getNoteTile(String textTitle)
	{
		String title = SuperTestScript.driver.findElement(By.xpath("//div[text()='"+textTitle+"']")).getText();
        return title;
	}
	
	public void clickOnOldNote(String oldNoteTitle)
	{
		WebElement oldNotes = SuperTestScript.driver.findElement(By.xpath("//div[text()='"+oldNoteTitle+"']"));
	    oldNotes.click();
	}
	
	public void clickOnXofaNote(String nt)
	{
		explicitWaitelementToBeClickable(By.cssSelector("paper-icon-button[title='Remove Note "+ nt +"']"));
		WebElement oldNote = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-button[title='Remove Note "+ nt +"']"));
    	oldNote.click();
		//int delete_size = SuperTestScript.driver.findElements(By.id("deleteNote")).size();
		//SuperTestScript.driver.findElements(By.id("deleteNote")).get(delete_size-1).click();
	}
	
	public void clickOnDelteOnConfirmationPopup()
	{
		SuperTestScript.driver.findElement(By.cssSelector("paper-button:nth-of-type(1)[class='style-scope confirmation-component x-scope paper-button-0']")).click();
	}
	
	public String fetchSuccessMsg()
	{
		
		explicitWaitvisibilityOfElementLocated(noteDeletedMsgBy);
		String msg = noteDeletedMsg.getText();
		return msg;
	}
	
	public String fetchNotesHeader()
	{
		explicitWaitelementToBeClickable(notesHeaderBy);
		String msg = notesHeader.getText();
		return msg;
	}
	
	public void clickOnCloseNotes()
	{
		explicitWaitvisibilityOfElementLocated(closeNotesButtonBy);
		closeNotesButton.click();
	}
	
	public void clickOnBackNotesButton()
	{
		explicitWaitvisibilityOfElementLocated(backNotesButtonBy);
		backNotesButton.click();
	}
	
	public String fetchFacesNoteShows(String noteStr)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("div[title='"+noteStr+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
}