package education.volute.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import education.volute.common_lib.SuperTestScript;

public class Assessments extends NavigatorTool {
		
		@FindBy(xpath = "//p[contains(text( ),'Enable the Qualtrics integration by adding your credentials under the CONFIGURATIONS tab.')]")
		private WebElement noContentManageMessage;
		private By noContentManageMessageBy = By.xpath("//p[contains(text( ),'Enable the Qualtrics integration by adding your credentials under the CONFIGURATIONS tab.')]");
		
		@FindBy(css = "form[id='qualtricsIntegrationForm'] > paper-checkbox")
		private WebElement enableQualIntegration;
		private By enableQualIntegrationBy = By.cssSelector("form[id='qualtricsIntegrationForm'] > paper-checkbox");
		
		@FindBy(css = "paper-input[label='Username'] input")
		private WebElement aUsername;
		private By aUsernameBy = By.cssSelector("paper-input[label='Username'] input");
		
		@FindBy(css = "paper-input[label='API'] input")
		private WebElement aApi;
		private By aApiBy = By.cssSelector("paper-input[label='API'] input");
		
		@FindBy(css = "div[class$='assessment-integrations'] > paper-button")
		private WebElement saveAssessmentButton;
		private By saveAssessmentButtonBy = By.cssSelector("div[class$='assessment-integrations'] > paper-button");
		
		@FindBy(css = "div[class$='assessment-manager'] > paper-button")
		private WebElement saveSurveyButton;
		private By saveSurveyButtonBy = By.cssSelector("div[class$='assessment-manager'] > paper-button");
		
		@FindBy(css = "paper-toast[title='Configuration saved successfully']")
		private WebElement saveMsgAssessment;
		private By saveMsgAssessmentBy = By.cssSelector("paper-toast[title='Configuration saved successfully']");
		
		@FindBy(css = "paper-toast[title='Please populate all required fields']")
		private WebElement errorMsgA1;
		private By errorMsgA1By = By.cssSelector("paper-toast[title='Please populate all required fields']");
		
		@FindBy(css = "paper-toast[title='There was an error saving configuration.']")
		private WebElement errorMsgA2;
		private By errorMsgA2By = By.cssSelector("paper-toast[title='There was an error saving configuration.']");
		
		@FindBy(css = "paper-toast[title='Incorrect credentials, please enter the correct Qualtrics credentials.']")
		private WebElement errorMsgA3;
		private By errorMsgA3By = By.cssSelector("paper-toast[title='Incorrect credentials, please enter the correct Qualtrics credentials.']");
		
		@FindBy(css = "paper-toast[title='You must first activate this survey in Qualtrics.']")
		private WebElement errorMsgA4;
		private By errorMsgA4By = By.cssSelector("paper-toast[title='You must first activate this survey in Qualtrics.']");
		
		@FindBy(css = "paper-toast[title='Survey published successfully']")
		private WebElement errorMsgA5;
		private By errorMsgA5By = By.cssSelector("paper-toast[title='Survey published successfully']");
		
		@FindBy(css = "paper-toast[title='Survey published successfully']")
		private WebElement saveMsgSurvey;
		private By saveMsgSurveyBy = By.cssSelector("paper-toast[title='Survey published successfully']");
		
		@FindBy(css = "assessment-viewer paper-button[class^='save-button']")
		private WebElement startAssessmentButton;
		private By startAssessmentButtonBy = By.cssSelector("assessment-viewer paper-button[class^='save-button']");
		
		@FindBy(css = "form[class='Page ActivePage'] input[id='NextButton']")
		private WebElement nextSurveyButton;
		private By nextSurveyButtonBy = By.cssSelector("form[class='Page ActivePage'] input[id='NextButton']");
		
		@FindBy(css = "//div[contains(text( ),'We thank you for your time spent taking this survey.')]")
		private WebElement finSurvey;
		private By finSurveyBy = By.cssSelector("//div[contains(text( ),'We thank you for your time spent taking this survey.')]");
		
		public Assessments()
		{
			PageFactory.initElements(SuperTestScript.driver, this);
			
		}
		
		public String fetchNoContentManageMsg()
		{
			try {
				SuperTestScript.driver.findElement(noContentManageMessageBy);
				return "Found";
			}
			catch(NoSuchElementException e){
				return "Not Found";
			}
		}
		
		/*public String fetchNoContentManageMsg()
		{
			
			explicitWaitvisibilityOfElementLocated(noContentManageMessageBy);
			String msg = noContentManageMessage.getText();
			return msg;
		}*/
		
		public void clickEnableQualIntegrationButton()
		{
			explicitWaitvisibilityOfElementLocated(enableQualIntegrationBy);
			enableQualIntegration.click();
		}
		
		public void enterUsername(String amUserName)
		{
			explicitWaitvisibilityOfElementLocated(aUsernameBy);
			Actions a1 = new Actions(SuperTestScript.driver);
			a1.moveToElement(aUsername).click().sendKeys(amUserName).perform();
			
		}
		
		public void clearUsername()
		{
			explicitWaitvisibilityOfElementLocated(aUsernameBy);
			aUsername.clear();
		}
		
		public void enterApi(String amApi)
		{
			explicitWaitvisibilityOfElementLocated(aApiBy);
			Actions a1 = new Actions(SuperTestScript.driver);
			a1.moveToElement(aApi).click().sendKeys(amApi).perform();
			
		}
		
		public void clearApi()
		{
			explicitWaitvisibilityOfElementLocated(aApiBy);
			aApi.clear();
		}
		
		public void clickSaveAssessmentButton()
		{
			explicitWaitvisibilityOfElementLocated(saveAssessmentButtonBy);
			saveAssessmentButton.click();
		}
		
		public void clickSaveSurveyButton()
		{
			explicitWaitvisibilityOfElementLocated(saveSurveyButtonBy);
			saveSurveyButton.click();
		}
		
		public String fetchSaveMsgAssessment()
		{
			try {
				SuperTestScript.driver.findElement(saveMsgAssessmentBy);
				return "Found";
			}
			catch(NoSuchElementException e){
				return "Not Found";
			}
		}
		
		/*public String fetchSaveMsgAssessment()
		{
			
			explicitWaitvisibilityOfElementLocated(saveMsgAssessmentBy);
			String msg = saveMsgAssessment.getText();
			return msg;
		}*/
		public String fetchErrorMsgA1()
		{
			try {
				SuperTestScript.driver.findElement(errorMsgA1By);
				return "Found";
			}
			catch(NoSuchElementException e){
				return "Not Found";
			}
		}
		
		/*public String fetchErrorMsgA1()
		{
			
			explicitWaitvisibilityOfElementLocated(errorMsgA1By);
			String msg = errorMsgA1.getText();
			return msg;
		}*/
		public String fetchErrorMsgA2()
		{
			try {
				SuperTestScript.driver.findElement(errorMsgA2By);
				return "Found";
			}
			catch(NoSuchElementException e){
				return "Not Found";
			}
		}
		
		/*public String fetchErrorMsgA2()
		{
			
			explicitWaitvisibilityOfElementLocated(errorMsgA2By);
			String msg = errorMsgA2.getText();
			return msg;
		}*/
		public String fetchErrorMsgA3()
		{
			try {
				SuperTestScript.driver.findElement(errorMsgA3By);
				return "Found";
			}
			catch(NoSuchElementException e){
				return "Not Found";
			}
		}
		
		/*public String fetchErrorMsgA3()
		{
			
			explicitWaitvisibilityOfElementLocated(errorMsgA3By);
			String msg = errorMsgA3.getText();
			return msg;
		}*/
		
		public void clickOnSelectTab(String Tab)
		{
			explicitWaitvisibilityOfElementLocated(By.xpath("//div[text()[contains(.,'"+Tab+"')]]"));
			WebElement selectTab = SuperTestScript.driver.findElement(By.xpath("//div[text()[contains(.,'"+Tab+"')]]"));
	    	selectTab.click();
		}
		
		public void clickOnSelectSurvey(String surName)
		{
			explicitWaitvisibilityOfElementLocated(By.xpath("//div[contains(text( ),'"+surName+"')]/.."));
			WebElement selectSurvey = SuperTestScript.driver.findElement(By.xpath("//div[contains(text( ),'"+surName+"')]/.."));
	    	selectSurvey.click();
		}
		
		// Toggle button - One or Multiple
		public void clickOnToggleButton(String surName)
		{
			explicitWaitvisibilityOfElementLocated(By.xpath("//div[contains(text( ),'"+surName+"')]/../.. //paper-toggle-button"));
			WebElement selectToggle = SuperTestScript.driver.findElement(By.xpath("//div[contains(text( ),'"+surName+"')]/../.. //paper-toggle-button"));
	    	selectToggle.click();
		}
		
		public String fetchErrorMsgA4()
		{
			try {
				SuperTestScript.driver.findElement(errorMsgA4By);
				return "Found";
			}
			catch(NoSuchElementException e){
				return "Not Found";
			}
		}
		
		/*public String fetchErrorMsgA4()
		{
			
			explicitWaitvisibilityOfElementLocated(errorMsgA4By);
			String msg = errorMsgA4.getText();
			return msg;
		}*/
		public String fetchErrorMsgA5()
		{
			try {
				SuperTestScript.driver.findElement(errorMsgA5By);
				return "Found";
			}
			catch(NoSuchElementException e){
				return "Not Found";
			}
		}
		
		public String fetchSaveMsgSurvey()
		{
			try {
				SuperTestScript.driver.findElement(saveMsgSurveyBy);
				return "Found";
			}
			catch(NoSuchElementException e){
				return "Not Found";
			}
		}
		
		/*public String fetchErrorMsgA5()
		{
			
			explicitWaitvisibilityOfElementLocated(errorMsgA5By);
			String msg = errorMsgA5.getText();
			return msg;
		}*/
		
		public void clickStartAssessmentButton()
		{
			explicitWaitvisibilityOfElementLocated(startAssessmentButtonBy);
			startAssessmentButton.click();
		}
		
		public void clickNextSurveyButton()
		{
			/*SuperTestScript.driver.switchTo().frame(SuperTestScript.driver.findElement(By.cssSelector("form[class='Page ActivePage']")));
			SuperTestScript.driver.findElement(By.cssSelector("input[id='NextButton']")).click();*/
			
			JavascriptExecutor js = (JavascriptExecutor) SuperTestScript.driver;
			js.executeScript("document.querySelector(\"input[id='NextButton']\").click()");
			/*explicitWaitvisibilityOfElementLocated(nextSurveyButtonBy);
			nextSurveyButton.click();*/
			//javaScriptExecutor(nextSurveyButtonBy);
		}
		
}
