package education.volute.pages;

import java.io.IOException;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.awt.*;
import java.io.File;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import education.volute.common_lib.SuperTestScript;






public class NavigatorTool
{
		
	@FindBy(css = "paper-icon-button[title='Open Menu']")
	private WebElement mainMenuIcon;
	private By mainMenuuIconBy = By.cssSelector("paper-icon-button[title='Open Menu']");
	
	@FindBy(css = "paper-icon-button[title='Close Menu']")
	private WebElement closeMainMenuIcon;
	private By closeMainMenuIconBy = By.cssSelector("paper-icon-button[title='Close Menu']");
	
	@FindBy(css = "paper-dialog[id='tutorialDialog'] paper-fab")
	private WebElement tutorialCloseButton;
	private By tutorialCloseButtonBy = By.cssSelector("paper-dialog[id='tutorialDialog'] paper-fab");
	
	@FindBy(css = "paper-button[title='Home']")
	private WebElement homeIcon;
	private By homeIconBy = By.cssSelector("paper-button[title='Home']");
	
	@FindBy(css = "paper-button[title='Notes']")
	private WebElement notesIcon;
	private By notesIconBy = By.cssSelector("paper-button[title='Notes']");
	
	@FindBy(css = "paper-button[title='Groups']")
	private WebElement collaborateIcon;
	private By collaborateIconBy = By.cssSelector("paper-button[title='Groups']");
	
	@FindBy(css = "paper-button[title='Support']")
	private WebElement supportIcon;
	private By supportIconBy = By.cssSelector("paper-button[title='Support']");
	
	@FindBy(css = "paper-icon-button[title='Rooms']")
	private WebElement roomsIcon;
	private By roomsIconBy = By.cssSelector("paper-icon-button[title='Rooms']");
	
	@FindBy(css = "paper-button[title='Discussion']")
	private WebElement discussionIcon;
	private By discussionIconBy = By.cssSelector("paper-button[title='Discussion']");
	
	@FindBy(css = "paper-icon-button[title='Peer Review']")
	private WebElement peerReviewIcon;
	private By peerReviewIconBy = By.cssSelector("paper-icon-button[title='Peer Review']");
	
	@FindBy(css = "paper-tab[title='Menu Tab']")
	private WebElement menuTab;
	private By menuTabBy = By.cssSelector("paper-tab[title='Menu Tab']");
	
	@FindBy(css = "paper-item[id='designTab']")
	private WebElement designTab;
	private By designTabBy = By.cssSelector("paper-item[id='designTab']");
	
	@FindBy(css = "paper-item[id='overviewTab']")
	private WebElement overviewTab;
	private By overviewTabBy = By.cssSelector("paper-item[id='overviewTab']");
	
	@FindBy(css = "paper-item[id='statusTab']")
	private WebElement statusTab;
	private By statusTabBy = By.cssSelector("paper-item[id='statusTab']");
	//@FindBy(css = "paper-icon-button[title='Landing']")
	//private WebElement landingIcon;
	//private By landingIconBy = By.cssSelector("paper-icon-button[title='Landing']");
	
	@FindBy(xpath = "//paper-button[text()='Yes']")
	private WebElement deleteYesButton;
	private By deleteYesButtonBy = By.xpath("//paper-button[text()='Yes']");
	
	@FindBy(xpath = "//paper-button[text()='No']")
	private WebElement deleteNoButton;
	private By deleteNoButtonBy = By.xpath("//paper-button[text()='No']");
	
	@FindBy(css = "paper-button[title='Me']")
	private WebElement profileIcon;
	private By profileIconBy = By.cssSelector("paper-button[title='Me']");
	
	@FindBy(css = "paper-button[title='Journey']")
	private WebElement myJourneyIcon;
	private By myJourneyIconBy = By.cssSelector("paper-button[title='Journey']");
	
	@FindBy(css = "paper-button[title='Admin']")
	private WebElement adminIcon;
	private By adminIconBy = By.cssSelector("paper-button[title='Admin']");
	
	@FindBy(xpath = "//paper-button[text()='Show Me Later']")
	private WebElement LeavingTutorial;
	private By LeavingTutorialBy = By.xpath("//paper-button[text()='Show Me Later']");
	
	@FindBy(css = "a[title='End Tour']")
	private WebElement endTour;
	private By endTourBy = By.cssSelector("a[title='End Tour']");
	
	public NavigatorTool()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	public void clickCloseButtonTutorial()
	{
		explicitWaitvisibilityOfElementLocated(tutorialCloseButtonBy);
		tutorialCloseButton.click();
	}
	
	public void clickLeavingTutorial(){
		explicitWaitvisibilityOfElementLocated(LeavingTutorialBy);
		LeavingTutorial.click();
	}
	
	public void clickendTour(){
		explicitWaitvisibilityOfElementLocated(endTourBy);
		endTour.click();
	}
	
	public void clickOnMainMenu()
	{
		explicitWaitvisibilityOfElementLocated(mainMenuuIconBy);
		mainMenuIcon.click();
	}
	
	public void CloseMainMenu()
	{
		explicitWaitvisibilityOfElementLocated(closeMainMenuIconBy);
		closeMainMenuIcon.click();
	}
	
	public String homeShows()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//String click;
		try {
			SuperTestScript.driver.findElement(homeIconBy);
			return "Found";
			//clickendTour();
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnHomeIcon()
	{
		explicitWaitvisibilityOfElementLocated(homeIconBy);
		homeIcon.click();
	}
	
	public String noteShows()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//String click;
		try {
			SuperTestScript.driver.findElement(notesIconBy);
			return "Found";
			//clickendTour();
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnNotesIcon()
	{
		explicitWaitelementToBeClickable(notesIconBy);
		notesIcon.click();
	}
	
	public String collaborateShows()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//String click;
		try {
			SuperTestScript.driver.findElement(collaborateIconBy);
			return "Found";
			//clickendTour();
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnCollaborateIcon()
	{
		explicitWaitelementToBeClickable(collaborateIconBy);
		collaborateIcon.click();
	}
	
	public String supportShows()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//String click;
		try {
			SuperTestScript.driver.findElement(supportIconBy);
			return "Found";
			//clickendTour();
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnSupportIcon()
	{
		explicitWaitelementToBeClickable(supportIconBy);
		supportIcon.click();
	}
	
	public void clickOnRoomsIcon()
	{
		explicitWaitelementToBeClickable(roomsIconBy);
		roomsIcon.click();
	}
	
	public String discussionShows()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//String click;
		try {
			SuperTestScript.driver.findElement(discussionIconBy);
			return "Found";
			//clickendTour();
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnDiscussionIcon(String typeDisc)
	{
		if(typeDisc.equalsIgnoreCase("Float")){
			explicitWaitelementToBeClickable(discussionIconBy);
			discussionIcon.click();
		}
		
	}
	
	public void clickOnPeerReviewIcon()
	{
		explicitWaitelementToBeClickable(peerReviewIconBy);
		peerReviewIcon.click();
	}
	
	public String journeyShows()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//String click;
		try {
			SuperTestScript.driver.findElement(myJourneyIconBy);
			return "Found";
			//clickendTour();
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnJourney()
	{
		explicitWaitvisibilityOfElementLocated(myJourneyIconBy);
		myJourneyIcon.click();
	}
	
	public String adminShows()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//String click;
		try {
			SuperTestScript.driver.findElement(adminIconBy);
			return "Found";
			//clickendTour();
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnAdmin()
	{
		explicitWaitvisibilityOfElementLocated(adminIconBy);
		adminIcon.click();
	}
	/*public void clickOnMenuTab()
	{
		explicitWaitvisibilityOfElementLocated(menuTabBy);
		menuTab.click();
	}*/
	
	public void clickOnDesignTab()
	{
		explicitWaitvisibilityOfElementLocated(designTabBy);
		designTab.click();
	}
	
	public void clickOnOverviewTab()
	{
		explicitWaitvisibilityOfElementLocated(overviewTabBy);
		overviewTab.click();
	}
	
	public void clickOnStatusTab()
	{
		explicitWaitvisibilityOfElementLocated(statusTabBy);
		statusTab.click();
	}
	/*public void clickOnLandingIcon()
	{
		explicitWaitelementToBeClickable(landingIconBy);
		landingIcon.click();
	}*/
	
	public String portfolioShows()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//String click;
		try {
			SuperTestScript.driver.findElement(profileIconBy);
			return "Found";
			//clickendTour();
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnPortfolioIcon()
	{
		explicitWaitelementToBeClickable(profileIconBy);
		profileIcon.click();
	}
	
	public static void explicitWaitvisibilityOfElementLocated(By value)
	{
		WebDriverWait wait = new WebDriverWait(SuperTestScript.driver, 100);
		wait.until(ExpectedConditions.visibilityOfElementLocated(value));
	}
	
	public static void explicitWaitelementToBeClickable(By value)
	{
		WebDriverWait wait = new WebDriverWait(SuperTestScript.driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(value));
	}
	
	public static void javaScriptExecutor(By value) {
		// Create instance of JS Exec
		JavascriptExecutor je = (JavascriptExecutor)SuperTestScript.driver;
		
		// Identify the WebElement which appears after scrolling
		WebElement element = SuperTestScript.driver.findElement(value);
		
		// Now execute query that will scroll until element does not appear on page
		je.executeScript("arguments[0].scrollIntoView(true);", element);
		
		// Extract and click
		element.click();
	}
	
	public static void javaScriptExecutorNoClick(By value) {
		// Create instance of JS Exec
		JavascriptExecutor je = (JavascriptExecutor)SuperTestScript.driver;
		
		// Identify the WebElement which appears after scrolling
		WebElement element = SuperTestScript.driver.findElement(value);
		
		// Now execute query that will scroll until element does not appear on page
		je.executeScript("arguments[0].scrollIntoView(false);", element);
		
		//je.executeScript(getElement(element).scrollIntoView());
		//}, []);
		// Extract and click
		//element.click();
	}
	
	public static void javaScriptExecutorClick(By value) {
		// Create instance of JS Exec
		JavascriptExecutor je = (JavascriptExecutor)SuperTestScript.driver;
		
		// Identify the WebElement which appears after scrolling
		WebElement element = SuperTestScript.driver.findElement(value);
		
		// Now execute query that will scroll until element does not appear on page
		je.executeScript("arguments[0].click();", element);
		
		// Extract and click
	}
	
	public static void javaScriptExecutorWrite(By value, String name) {
		// Create instance of JS Exec
		JavascriptExecutor je = (JavascriptExecutor)SuperTestScript.driver;
		
		// Identify the WebElement which appears after scrolling
		WebElement element = SuperTestScript.driver.findElement(value);
		
		// Now execute query that will scroll until element does not appear on page
		je.executeScript("arguments[0].value='"+name+"';", element);
		//je.executeScript("document."+value+".value = 'testing';");
		
		// Extract and click
	}
	
	public static void javaScriptExecutorUpload(By value, String name) throws Exception {

		// Specify the file location with extension
		StringSelection sel = new StringSelection(name);
		
		// Copy to clipboard
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sel,null);
		
		// This will click on Browse button
		SuperTestScript.driver.findElement(value).click();
		
		// Create object of Robot class
		Robot robot = new Robot();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		// Press Enter
		robot.keyPress(KeyEvent.VK_ENTER);
		
		// Release Enter
		robot.keyRelease(KeyEvent.VK_ENTER);
		
		// Press CTRL+V
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		
		// Release CTRL+V
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		// Press Enter 
		robot.keyPress(KeyEvent.VK_ENTER);
		
		// Release Enter
		robot.keyRelease(KeyEvent.VK_ENTER);
		
		/*
		File file = new File(name);
		// disable the click event on an `<input>` file
//		((JavascriptExecutor)SuperTestScript.driver).executeScript(
//		    "HTMLInputElement.prototype.click = function() {                     " +
//		    "  if(this.type !== 'file') HTMLElement.prototype.click.call(this);  " +
//		    "};                                                                  " );
//		
		
		// assign the file to the `<input>`
		SuperTestScript.driver.findElement(By.cssSelector("input[type=file]")).sendKeys(file.getAbsolutePath());
		
		// trigger the upload
		SuperTestScript.driver.findElement(value).click();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
	}
	
	public static void pressEnter(By value) {
		SuperTestScript.driver.findElement(value).sendKeys(Keys.ENTER);
	}
	
	public void refreshPage(){
		SuperTestScript.driver.navigate().refresh();
	}
	
	public void dragAndDrop(By first, By second){
		Actions act=new Actions(SuperTestScript.driver);
		
		WebElement drag=SuperTestScript.driver.findElement(first);
		
		WebElement drop=SuperTestScript.driver.findElement(second);
		
		act.dragAndDrop(drag, drop).build().perform();
	}
	
	public void switchWindow(String parent){
		
		Set<String> s1 = SuperTestScript.driver.getWindowHandles(); //child
	    
	    //Process of switching window
	    for(String child : s1)
	    {
	    	if(!child.equals(parent))
	    	{
	    		//Switch to new window
	    		SuperTestScript.driver.switchTo().window(child);
	    	}
	    }
	    
	    try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	 
	public String tutorialShows()
	{
		//String click;
		try {
			SuperTestScript.driver.findElement(By.xpath("a[title='End Tour']"));
			return "Found";
			//clickendTour();
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	 
	public void endButton(){
		String click = tutorialShows();
		//tutorialShows();
		if(click.equalsIgnoreCase("Found")){
			clickendTour();
		}
		else{
			// Do Nothing
		}
	}
	
	public void clickOnDeletePgmYesButton()
	{
		explicitWaitvisibilityOfElementLocated(deleteYesButtonBy);
		deleteYesButton.click();
		
	}
	
	public void clickOnDeleteNoButton()
	{
		explicitWaitvisibilityOfElementLocated(deleteNoButtonBy);
		deleteNoButton.click();
		
	}
	
	public String fetchProgressFile()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("div[id='status'][hidden]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchProgressFile2()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("div[id='name']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
//	public static WebElement navigation = driver.findElement(By.cssSelector("paper-icon-item[title='Navigation']"));
//    
//	public static WebElement navMenuHome = driver.findElement(By.cssSelector("paper-icon-button[title='Home']"));
//    		
//	public static WebElement navMenuNotes = driver.findElement(By.cssSelector("paper-icon-button[title='Notes']"));
//    
//	public static WebElement navMenuCollaborate = driver.findElement(By.cssSelector("paper-icon-button[title='Collaborate']"));
//	
//	public static WebElement navMenuRooms = driver.findElement(By.cssSelector("paper-icon-button[title='Rooms']"));
//    
//	public static WebElement navMenuDiscussion = driver.findElement(By.cssSelector("paper-icon-button[title='Discussion']"));
//    
//	public static WebElement navMenuProfile = driver.findElement(By.cssSelector("paper-icon-button[title='Profile']"));

}
