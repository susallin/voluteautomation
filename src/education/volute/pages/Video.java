package education.volute.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import education.volute.common_lib.SuperTestScript;

public class Video extends NavigatorTool {

	/*@FindBy(id = "Help")
	private WebElement videoHelp;
	private By videoHelpBy = By.id("Help");
	
	@FindBy(id = "Share")
	private WebElement videoShare;
	private By videoShareBy = By.id("Share");*/
	
	@FindBy(css = "paper-fab[id^='Video'][title='Manage']")
	private WebElement manageVideo;
	private By manageVideoBy = By.cssSelector("paper-fab[id^='Video'][title='Manage']");
	
	@FindBy(css = "div[title='Video']")
	private WebElement videoHeader;
	private By videoHeaderBy = By.cssSelector("div[title='Video']");
	
	@FindBy(css = "paper-input[label='Display Name']")
	private WebElement videoDisplayName;
	private By videoDisplayNameBy = By.cssSelector("paper-input[label='Display Name']");
	
//	@FindBy(css = "form[id='videoForm'] > paper-checkbox > div[id='checkboxContainer']")
//	private WebElement videoCheckBoxUpload;
//	private By videoCheckBoxUploadBy = By.cssSelector("form[id='videoForm'] > paper-checkbox > div[id='checkboxContainer']");
	
	@FindBy(css = "paper-radio-button[name='urlUpload']")
	private WebElement videoUrlUpload;
	private By videoUrlUploadBy = By.cssSelector("paper-radio-button[name='urlUpload']");
	
	@FindBy(css = "paper-radio-button[name='manualUpload']")
	private WebElement videoManualUpload;
	private By videoManualUploadBy = By.cssSelector("paper-radio-button[name='manualUpload']");
	
	@FindBy(css = "vaadin-upload[id='videoUpload'] paper-button[id='addFiles']")
	private WebElement videoUploadFile;
	private By videoUploadFileBy = By.cssSelector("vaadin-upload[id='videoUpload'] paper-button[id='addFiles']");
	
	@FindBy(css = "paper-input[id='videoTitle']")//input[placeholder='Add video title here']
	private WebElement videoTitle;
	private By videoTitleBy = By.cssSelector("paper-input[id='videoTitle']");
	
	@FindBy(css = "paper-input[id='videoTitle'] input[placeholder='Add video title here']")
	private WebElement videoTitleClear;
	private By videoTitleClearBy = By.cssSelector("paper-input[id='videoTitle'] input[placeholder='Add video title here']");
	
	@FindBy(css = "paper-textarea[id='videoDesc'] textarea")
	private WebElement videoDescClear;
	private By videoDescClearBy = By.cssSelector("paper-textarea[id='videoDesc'] textarea");
	
	@FindBy(css = "paper-textarea[id='videoDesc'] textarea")
	private WebElement videoDescription;
	private By videoDescriptionBy = By.cssSelector("paper-textarea[id='videoDesc'] textarea");
	
	@FindBy(css = "paper-input[label^='Insert YouTube']")//input[placeholder='Add video URL here']
	private WebElement insertURLVideo;
	private By insertURLVideoBy = By.cssSelector("paper-input[label^='Insert YouTube']");
	
	@FindBy(css = "paper-button[class='cancel-button style-scope volute-app-video-face2-module x-scope paper-button-0']")
	private WebElement videoClear;
	private By videoClearBy = By.cssSelector("paper-button[class='cancel-button style-scope volute-app-video-face2-module x-scope paper-button-0']");
	
	@FindBy(css = "paper-button[class='save-button style-scope volute-app-video-face2-module x-scope paper-button-0']")
	private WebElement videoSave;
	private By videoSaveBy = By.cssSelector("paper-button[class='save-button style-scope volute-app-video-face2-module x-scope paper-button-0']");
	
	@FindBy(css = "div[id='tabsContent'] > paper-tab:nth-of-type(1)")
	private WebElement videoContentTab;
	private By videoContentTabBy = By.cssSelector("div[id='tabsContent'] > paper-tab:nth-of-type(1)");
	
	@FindBy(css = "div[id='tabsContent'] > paper-tab:nth-of-type(2)")
	private WebElement videoFeaturesTab;
	private By videoFeaturesTabBy = By.cssSelector("div[id='tabsContent'] > paper-tab:nth-of-type(2)");
	
	@FindBy(css = "paper-dropdown-menu[label='Select Role']")
	private WebElement videoSelectRoleMenu;
	private By videoSelectRoleMenuBy = By.cssSelector("paper-dropdown-menu[label='Select Role']");
	
	@FindBy(css = "paper-toast[title='Video uploaded successfully']")
	private WebElement addedMsgVideo;
	private By addedMsgVideoBy = By.cssSelector("paper-toast[title='Video uploaded successfully']");
	
	@FindBy(css = "paper-listbox > paper-icon-item:nth-of-type(2) > paper-icon-button")
	private WebElement videoDeleteButton;
	private By videoDeleteButtonBy = By.cssSelector("paper-listbox > paper-icon-item:nth-of-type(2) > paper-icon-button");
	
	@FindBy(css = "paper-button:nth-of-type(1)[class='style-scope confirmation-component x-scope paper-button-0']")
	private WebElement deleteYesVideo;
	private By deleteYesVideoBy = By.cssSelector("paper-button:nth-of-type(1)[class='style-scope confirmation-component x-scope paper-button-0']");
	
	@FindBy(css = "paper-button:nth-of-type(2)[class='style-scope confirmation-component x-scope paper-button-0']")
	private WebElement deleteNoVideo;
	private By deleteNoVideoBy = By.cssSelector("paper-button:nth-of-type(2)[class='style-scope confirmation-component x-scope paper-button-0']");
	
	@FindBy(css = "paper-toast[title='Video deleted successfully']")
	private WebElement deleteMsgVideo;
	private By deleteMsgVideoBy = By.cssSelector("paper-toast[title='Video deleted successfully']");
	
	@FindBy(css = "div[class='done-button style-scope app-management']")
	private WebElement videoDoneButton;
	private By videoDoneButtonBy = By.cssSelector("div[class='done-button style-scope app-management']");
	
	@FindBy(css = "paper-fab[id^='videosListToggle'][aria-label='back to video list']")
	private WebElement videoBackListButton;
	private By videoBackListButtonBy = By.cssSelector("paper-fab[id^='videosListToggle'][aria-label='back to video list']");
	
	public Video()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	/*public void clickOnVideoHelp()
	{
		explicitWaitvisibilityOfElementLocated(videoHelpBy);
		videoHelp.click();
	}
	
	public void clickOnVideoShare()
	{
		explicitWaitvisibilityOfElementLocated(videoShareBy);
		videoShare.click();
	}*/
	
	public void clickOnVideoManage()
	{
		//explicitWaitvisibilityOfElementLocated(manageVideoBy);
		//manageVideo.click();
		javaScriptExecutorClick(manageVideoBy);
		//javaScriptExecutor(manageVideoBy);
	}
	
	public String fetchvideoHeader()
	{
		explicitWaitelementToBeClickable(videoHeaderBy);
		String msg = videoHeader.getText();
		return msg;
	}
	
	public void enterVideoDisplayName(String vidDName)
	{
		explicitWaitvisibilityOfElementLocated(videoDisplayNameBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(videoDisplayName).click().sendKeys(vidDName).perform();
		
	}
	
//	public void clickOnVideoCheckBoxUpload()
//	{
//		explicitWaitvisibilityOfElementLocated(videoCheckBoxUploadBy);
//		videoCheckBoxUpload.click();
//	}
	
	public void clickOnVideoUrlUpload()
	{
		explicitWaitvisibilityOfElementLocated(videoUrlUploadBy);
		videoUrlUpload.click();
	}
	
	public void clickOnvideoManualUpload()
	{
		explicitWaitvisibilityOfElementLocated(videoManualUploadBy);
		videoManualUpload.click();
	}
	
	public void clickOnVideoAddFile(String fileName) throws Exception
	{
		javaScriptExecutorUpload(videoUploadFileBy, fileName);
		//explicitWaitvisibilityOfElementLocated(videoUploadFileBy);
		//videoUploadFile.click();
	}
	
	public void clearVideoTitle()
	{
		explicitWaitvisibilityOfElementLocated(videoTitleClearBy);
		videoTitleClear.clear();
	}
	
	public void enterOnVideoTitle(String vidTitle)
	{
		explicitWaitvisibilityOfElementLocated(videoTitleBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(videoTitle).click().sendKeys(vidTitle).perform();
		
	}
	
	public void clearVideoDesc()
	{
		explicitWaitvisibilityOfElementLocated(videoDescClearBy);
		videoDescClear.clear();
	}
	
	public void enterOnVideoDescription(String vidDescrip)
	{
		explicitWaitvisibilityOfElementLocated(videoDescriptionBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(videoDescription).click().sendKeys(vidDescrip).perform();
		
	}
	
	public void enterVideoInsertURL(String vidURL)
	{
		explicitWaitvisibilityOfElementLocated(insertURLVideoBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(insertURLVideo).click().sendKeys(vidURL).perform();
		
	}
	
	public void clickOnVideoUploaded(String vidName)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//paper-listbox/paper-icon-item/paper-item-body/div[contains(text( ),'"+ vidName +"')]"));
		WebElement selectVid = SuperTestScript.driver.findElement(By.xpath("//paper-listbox/paper-icon-item/paper-item-body/div[contains(text( ),'"+ vidName +"')]"));
    	selectVid.click();
	}
	
	public void clickOnVideoClear()
	{
		explicitWaitvisibilityOfElementLocated(videoClearBy);
		videoClear.click();
	}
	
	public void clickOnVideoSave()
	{
		explicitWaitvisibilityOfElementLocated(videoSaveBy);
		videoSave.click();
	}
	
	public void clickOnVideoContentTab()
	{
		explicitWaitvisibilityOfElementLocated(videoContentTabBy);
		videoContentTab.click();
	}
	
	public void clickOnVideoFeaturesTab()
	{
		explicitWaitvisibilityOfElementLocated(videoFeaturesTabBy);
		videoFeaturesTab.click();
	}
	
	public void clickOnVideoSelectRoleMenu()
	{
		explicitWaitvisibilityOfElementLocated(videoSelectRoleMenuBy);
		videoSelectRoleMenu.click();
	}
	
	//****
	public void selectVideoRole(String vidRoleNum)
	{
		WebElement vid = SuperTestScript.driver.findElement(By.cssSelector("paper-listbox > paper-item:nth-of-type("+vidRoleNum+")"));
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-listbox > paper-item:nth-of-type("+vidRoleNum+")"));
		vid.click();
		
	}
	
	public void clickOnVideoDoneButton()
	{
		explicitWaitvisibilityOfElementLocated(videoDoneButtonBy);
		videoDoneButton.click();
	}
	
	public String fetchVideoAddMsg()
	{	
		explicitWaitvisibilityOfElementLocated(addedMsgVideoBy);
		String msg = addedMsgVideo.getText();
		return msg;
	}
	
	/*public void clickOnVideoDeleteButton()
	{
		explicitWaitvisibilityOfElementLocated(videoDeleteButtonBy);
		videoDeleteButton.click();
	}*/
	
	public void clickOnVideoEditButton(String vidName)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//div[contains(text(),'"+vidName+"')]/../../paper-icon-button[@icon='icons:create']"));
		WebElement selectVid = SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'"+vidName+"')]/../../paper-icon-button[@icon='icons:create']"));
    	selectVid.click();
	}
	
	public void clickOnVideoDeleteButton(String vidName)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//div[contains(text(),'"+vidName+"')]/../../paper-icon-button[@icon='icons:delete']"));
		WebElement selectVid = SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'"+vidName+"')]/../../paper-icon-button[@icon='icons:delete']"));
    	selectVid.click();
	}
	
	public void clickOnDeleteYesVideo()
	{
		explicitWaitvisibilityOfElementLocated(deleteYesVideoBy);
		deleteYesVideo.click();
	}
	
	public void clickOnDeleteNoVideo()
	{
		explicitWaitvisibilityOfElementLocated(deleteNoVideoBy);
		deleteNoVideo.click();
	}
	
	public String fetchVideoDeleteMsg()
	{	
		explicitWaitvisibilityOfElementLocated(deleteMsgVideoBy);
		String msg = deleteMsgVideo.getText();
		return msg;
	}
	
	public String fetchManageFacesVidShows(String vidStr)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'"+vidStr+"')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchFacesVidShows(String vidStr)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("div[title=' "+vidStr+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnListVideo(String vidName)
	{
		/*explicitWaitvisibilityOfElementLocated(By.xpath("//div[contains(text(),'"+vidName+"')]"));
		WebElement selectVid = SuperTestScript.driver.findElement(By.xpath("//div[contains(text(),'"+vidName+"')]"));
    	selectVid.click();*/
    	javaScriptExecutorNoClick(By.xpath("//div[contains(text(),'"+vidName+"')]"));
    	try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	javaScriptExecutorClick(By.xpath("//div[contains(text(),'"+vidName+"')]"));
	}
	
	public void clickVideoBackListButton()
	{
		explicitWaitvisibilityOfElementLocated(videoBackListButtonBy);
		videoBackListButton.click();
	}
}
