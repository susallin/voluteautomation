package education.volute.pages;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import utilities.Constant;




public class MenuItems extends NavigatorTool

{
	/*@FindBy(css = "paper-icon-item[title='Navigation']")
	private WebElement navigation;
	private By navigationBy = By.cssSelector("paper-icon-item[title='Navigation']");*/
	
	@FindBy(css = "div[title='Launch Users']")
	private WebElement manageUsers;
	private By manageUsersBy = By.cssSelector("div[title='Launch Users']");
	
	@FindBy(css = "div[class^='user-wrapper']")//div[title='Launch Profile']
	private WebElement profileMenu;
	private By profileMenuBy = By.cssSelector("div[class^='user-wrapper']");
	
	@FindBy(css = "div[title='Manage Review Menu']")
	private WebElement manageReviewMenu;
	private By manageReviewMenuBy = By.cssSelector("div[title='Manage Review Menu']");
	
	@FindBy(css = "div[title='Launch Learning Design']")
	private WebElement learningDesignMenu;
	private By learningDesignMenuBy = By.cssSelector("div[title='Launch Learning Design']");
	
	@FindBy(css = "div[title='Launch Marketplace']")
	private WebElement marketplaceMenu;
	private By marketplaceMenuBy = By.cssSelector("div[title='Launch Marketplace']");
	
	@FindBy(css = "div[title='Launch Groups']")
	private WebElement clickGroup;
	private By clickGroupBy = By.cssSelector("div[title='Launch Groups']");
	/*
	@FindBy(id = "Manage Programs")
	private WebElement managePgms;
	private By managePgmsBy = By.id("Manage Programs");*/
	@FindBy(css = "paper-menu > div > paper-icon-item[id='Rooms']")
	private WebElement roomS;
	private By roomSBy = By.cssSelector("paper-menu > div > paper-icon-item[id='Rooms']");
	
	@FindBy(css = "div[title='Launch Learning Spaces']")
	private WebElement prog;
	private By progBy = By.cssSelector("div[title='Launch Learning Spaces']");
	
	@FindBy(css = "div[title='Sign Out']")
	private WebElement logout;
	private By logoutBy = By.cssSelector("div[title='Sign Out']");
	
	/*@FindBy(xpath = "//div[contains(text(),'Sign Out')]/..")
	private WebElement logout1;
	private By logout1By = By.xpath("//div[contains(text(),'Sign Out')]/..");*/
	//div[contains(text(),'Sign Out')]/..
	
	@FindBy(css = "input[title='Tool Title']")
	private WebElement toolDisplayName;
	private By toolDisplayNameBy = By.cssSelector("input[title='Tool Title']");
	
	@FindBy(css = "paper-tab[title='Individuals Tab']")
	private WebElement plIndividualTab;
	private By plIndividualTabBy = By.cssSelector("paper-tab[title='Individuals Tab']");
	
	@FindBy(css = "paper-tab[title='Groups Tab']")
	private WebElement plGroupTab;
	private By plGroupTabBy = By.cssSelector("paper-tab[title='Groups Tab']");
	
	@FindBy(css = "paper-tab[title='Selected Tab']")
	private WebElement plSelectedTab;
	private By plSelectedTabBy = By.cssSelector("paper-tab[title='Selected Tab']");
	
	@FindBy(css = "paper-button[title='Assign Users']")
	private WebElement plAssignUserButton;
	private By plAssignUserButtonBy = By.cssSelector("paper-button[title='Assign Users']");
	
	@FindBy(xpath = "//paper-button[contains(text( ),'Done')]")
	private WebElement plDoneButton;
	private By plDoneButtonBy = By.xpath("//paper-button[contains(text( ),'Done')]");
	
	@FindBy(css = "paper-fab[icon='icons:arrow-back']")
	private WebElement selectArrowBack;
	private By selectArrowBackBy = By.cssSelector("paper-fab[icon='icons:arrow-back']");
	
	@FindBy(css = "paper-fab[icon='icons:add']")
	private WebElement SelectAdd;
	private By SelectAddBy = By.cssSelector("paper-fab[icon='icons:add']");
	
	@FindBy(css = "paper-icon-button[icon='icons:create']")
	private WebElement displayButton;
	private By displayButtonBy = By.cssSelector("paper-icon-button[icon='icons:create']");
	
	@FindBy(css = "paper-toast[title='Display name saved sucessfully']")
	private WebElement updatedMsgDisplayName;
	private By updatedMsgDisplayNameBy = By.cssSelector("paper-toast[title='Display name saved sucessfully']");
	
	@FindBy(css = "paper-icon-item[title^='Selected']")
	private WebElement listOfIndGrpSelectTab;
	private By listOfIndGrpSelectTabBy = By.cssSelector("paper-icon-item[title^='Selected']");
	
	@FindBy(css = "paper-icon-item[title^='User'][class$='selected']")
	private WebElement listOfIndSelected;
	private By listOfIndSelectedBy = By.cssSelector("paper-icon-item[title^='User'][class$='selected']");
	
	public MenuItems()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	
	public void goToManageUsers()
	{
		/*explicitWaitvisibilityOfElementLocated(navigationBy);
		navigation.click();*/
		explicitWaitvisibilityOfElementLocated(manageUsersBy);
		manageUsers.click();
	}
	
	public String profileShows()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//String click;
		try {
			SuperTestScript.driver.findElement(profileMenuBy);
			return "Found";
			//clickendTour();
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void goToProfile()
	{
		explicitWaitvisibilityOfElementLocated(profileMenuBy);
		profileMenu.click();
	}
	
	/*
	public void clickOnManagePgms()
	{
		explicitWaitvisibilityOfElementLocated(managePgmsBy);
		managePgms.click();
	}*/
	public void clickOnRoomS()
	{
		explicitWaitvisibilityOfElementLocated(roomSBy);
		roomS.click();
	}
	
	public String programShows()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//String click;
		try {
			SuperTestScript.driver.findElement(progBy);
			return "Found";
			//clickendTour();
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnProgram()
	{
		explicitWaitvisibilityOfElementLocated(progBy);
		prog.click();
	}
	
	public String manageUsersShows()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//String click;
		try {
			SuperTestScript.driver.findElement(manageUsersBy);
			return "Found";
			//clickendTour();
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnUsers()
	{
		explicitWaitvisibilityOfElementLocated(manageUsersBy);
		manageUsers.click();
	}
	
	public String marketplaceShows()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//String click;
		try {
			SuperTestScript.driver.findElement(marketplaceMenuBy);
			return "Found";
			//clickendTour();
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnMarketplaceMenu()
	{
		explicitWaitvisibilityOfElementLocated(marketplaceMenuBy);
		marketplaceMenu.click();
	}
	
	public void clickOnGroup()
	{
		explicitWaitvisibilityOfElementLocated(clickGroupBy);
		clickGroup.click();
	}
	
	public String logoutShows()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//String click;
		try {
			SuperTestScript.driver.findElement(logoutBy);
			return "Found";
			//clickendTour();
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnLogout()
	{
		explicitWaitvisibilityOfElementLocated(logoutBy);
		logout.click();
	}
	
	public void clickDisplayName(String tool)
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-toolbar [title='"+tool+"']"));
		WebElement clickDName = SuperTestScript.driver.findElement(By.cssSelector("paper-toolbar [title='"+tool+"']"));
    	clickDName.clear();
	}
	
	public void clearDisplayName(String tool)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//div[contains(text( ),'"+ tool +"')]/.. //input[@title='Tool Display Name Input']"));
		WebElement clearDName = SuperTestScript.driver.findElement(By.xpath("//div[contains(text( ),'"+ tool +"')]/.. //input[@title='Tool Display Name Input']"));
    	clearDName.clear();
		//div[contains(text( ),'Announcements')]/.. //input[@title='Tool Title']
		//explicitWaitvisibilityOfElementLocated(toolDisplayNameBy);
		//toolDisplayName.clear();
	}
	
	public void enterDisplayName(String tool, String dname)
	{
		/*explicitWaitvisibilityOfElementLocated(richTEDisplayNameBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(richTEDisplayName).click().sendKeys(richDName).perform();*/
		WebElement enterDName = SuperTestScript.driver.findElement(By.xpath("//div[contains(text( ),'"+ tool +"')]/.. //input[@title='Tool Display Name Input']"));
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(enterDName).click().sendKeys(dname).perform();
		
		/*explicitWaitvisibilityOfElementLocated(By.xpath("//div[contains(text( ),'"+ tool +"')]/.. //input[@title='Tool Title']"));
		WebElement enterDName = SuperTestScript.driver.findElement(By.xpath("//div[contains(text( ),'"+ tool +"')]/.. //input[@title='Tool Title']"));
		
		
		int desc_size = SuperTestScript.driver.findElements((By) enterDName).size();
		SuperTestScript.driver.findElements((By) enterDName).get(desc_size-1).sendKeys(dname);*/
		
	}
	
	// n determines the roles 2(Participant), 3(Observer)
	public void clickCheckFetAccess(String fetName, String n)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//td[contains(text(),'"+fetName+"')]/.."+n+"/paper-checkbox"));
		WebElement selectCheckbox = SuperTestScript.driver.findElement(By.xpath("//td[contains(text(),'"+fetName+"')]/.."+n+"/paper-checkbox"));
	   	selectCheckbox.click();
	}
	
	public void clickOnAssignUserButton()
	{
		explicitWaitvisibilityOfElementLocated(plAssignUserButtonBy);
		plAssignUserButton.click();
	}
	
	public String fetchIndivTab()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(plIndividualTabBy);
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnIndividualTab()
	{
		explicitWaitvisibilityOfElementLocated(plIndividualTabBy);
		plIndividualTab.click();
	}
	
	public void clickOnIndividualTab(String typeDisc)
	{
		if(typeDisc.equalsIgnoreCase("Float")){
			explicitWaitvisibilityOfElementLocated(plIndividualTabBy);
			plIndividualTab.click();
		}
	}
	
	public String fetchUserListBox()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//paper-listbox[@id='userListbox']/paper-icon-item"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void selectIndividual(String indivName)
	{
		boolean flag = true;
		while(flag){
			String result = fetchUserListBox();
			
			if(result.equalsIgnoreCase("Found")){
				WebElement indN = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-item[title='User "+indivName+"']"));
				explicitWaitelementToBeClickable(By.cssSelector("paper-icon-item[title='User "+indivName+"']"));
				indN.click();
				flag = false;
			}
			else{
				flag = true;
			}
		}
	}
	
	public void selectIndividual(String typeDisc, String indivName)
	{
		boolean flag = true;
		while(flag){
			String result = fetchUserListBox();
			
			if(result.equalsIgnoreCase("Found")){
				if(typeDisc.equalsIgnoreCase("Float")){
					WebElement indN = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-item[title='User "+indivName+"']"));
					explicitWaitelementToBeClickable(By.cssSelector("paper-icon-item[title='User "+indivName+"']"));
					indN.click();
				}
				flag = false;
			}
			else{
				flag = true;
			}
		}
	}
	
	public void selectGroup(String grpName)
	{
		boolean flag = true;
		while(flag){
			String result = fetchGroupListBox();
			
			if(result.equalsIgnoreCase("Found")){
				WebElement grp = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-item[title='Group "+grpName+"']"));
				explicitWaitelementToBeClickable(By.cssSelector("paper-icon-item[title='Group "+grpName+"']"));
				grp.click();
				flag = false;
			}
			else{
				flag = true;
			}
		}
	}
	
	public String fetchGroupListBox()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//paper-listbox[@id='groupListbox']/paper-icon-item"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void selectGroup(String typeDisc, String grpName)
	{
		boolean flag = true;
		while(flag){
			String result = fetchGroupListBox();
			
			if(result.equalsIgnoreCase("Found")){
				if(typeDisc.equalsIgnoreCase("Float")){
					WebElement grp = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-item[title='Group "+grpName+"']"));
					explicitWaitelementToBeClickable(By.cssSelector("paper-icon-item[title='Group "+grpName+"']"));
					grp.click();
				}
				flag = false;
			}
			else{
				flag = true;
			}
		}
	}
	
	public void clickOnGroupTab()
	{
		explicitWaitvisibilityOfElementLocated(plGroupTabBy);
		plGroupTab.click();
	}
	
	public void clickOnGroupTab(String typeDisc)
	{
		if(typeDisc.equalsIgnoreCase("Float")){
			explicitWaitvisibilityOfElementLocated(plGroupTabBy);
			plGroupTab.click();
		}
	}
	
	public String fetchSelectedBox()
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("assign-module div[class$='iron-selected']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void clickOnSelectedTab()
	{
		explicitWaitvisibilityOfElementLocated(plSelectedTabBy);
		plSelectedTab.click();
	}
	
	public void clickOnSelectedTab(String typeDisc)
	{
		if(typeDisc.equalsIgnoreCase("Float")){
			explicitWaitvisibilityOfElementLocated(plSelectedTabBy);
			plSelectedTab.click();
		}
	}
	
	public void clickOnDoneButton()
	{
		boolean flag = true;
		while(flag){
			String result = fetchSelectedBox();
			
			if(result.equalsIgnoreCase("Found")){
				explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-button[title='Assign Selected Users and Groups']"));
				WebElement selectDone = SuperTestScript.driver.findElement(By.cssSelector("paper-button[title='Assign Selected Users and Groups']"));
				selectDone.click();
				flag = false;
			}
			else{
				flag = true;
			}
		}
	}
	
	public void clickOnCancelDoneButton()
	{
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-button[title='Cancel']"));
		WebElement selectDone = SuperTestScript.driver.findElement(By.cssSelector("paper-button[title='Cancel']"));
		selectDone.click();
	}
	
	public void clickOnDoneButton(String typeDisc)
	{
		if(typeDisc.equalsIgnoreCase("Float")){
			SuperTestScript.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
			explicitWaitvisibilityOfElementLocated(By.xpath("//paper-button[contains(text( ),'Assign')]"));
			WebElement selectDone = SuperTestScript.driver.findElement(By.xpath("//paper-button[contains(text( ),'Assign')]"));
			selectDone.click();
		}
	}
	
	public String fetchIndGrpShows(String plStr)
	{
		try {
			SuperTestScript.driver.findElement(By.cssSelector("paper-icon-item[title='Selected "+plStr+"'] > paper-item-body"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchIndivGroupSelected(String grpIndName)
	{	
		WebElement grpInd = SuperTestScript.driver.findElement(By.cssSelector("paper-icon-item[title='Selected "+grpIndName+"']"));
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-icon-item[title='Selected "+grpIndName+"']"));
		String msg = grpInd.getText();
		return msg;
	}
	
	public String fetchSubMenu(String actName, String actType)
	{
		if(actType.equalsIgnoreCase("Activity")){
			try {
				SuperTestScript.driver.findElement(By.xpath("//div[text()[contains(.,'"+actName+"')]]/../.. //paper-icon-button[contains(@class,'info-private')]"));
				return "Found";
			}
			catch(NoSuchElementException e){
				return "Not Found";
			}
		}else{
			try {
				SuperTestScript.driver.findElement(By.xpath("//div[text()[contains(.,'"+actName+"')]]/../.. //paper-icon-button[contains(@class,'info-button')]"));
				return "Found";
			}
			catch(NoSuchElementException e){
				return "Not Found";
			}
		}
	}	
	
//	public String fetchSubMenu2(String actName, String actType, String type, String str2)
//	{
//		if(actType.equalsIgnoreCase("Parent")){
//			String str;
//			try {
//				SuperTestScript.driver.findElement(By.xpath("//span[text()[contains(.,'"+actName+"')]]/../../.. //div[@id='activityTypeIcon']/span[@style]/../span[text()[contains(.,'"+type+"')]]"));
//				str= "Found";
//			}
//			catch(NoSuchElementException e){
//				str= "Not Found";
//			}
//			str2 = str;
//		} 
//		
//		if(actType.equalsIgnoreCase("Nested1")){
//			String str;
//			try {
//				SuperTestScript.driver.findElement(By.xpath("//span[text()[contains(.,'"+actName+"')]]/../../../.. //div[@id='activityTypeIcon']/span[@style]/../span[text()[contains(.,'"+type+"')]]"));
//				str= "Found";
//			}
//			catch(NoSuchElementException e){
//				str= "Not Found";
//			}
//			str2 = str;
//		}
//		
//		if(actType.equalsIgnoreCase("Nested2")){
//			String str;
//			try {
//				SuperTestScript.driver.findElement(By.xpath("//span[text()[contains(.,'"+actName+"')]]/../../../.. //div[@id='activityTypeIcon']/span[@style]/../span[text()[contains(.,'"+type+"')]]"));
//				str= "Found";
//			}
//			catch(NoSuchElementException e){
//				str= "Not Found";
//			}
//			str2 = str;
//		}
//		
//		return str2;
//	}
	
	public String fetchPubActSub(String actName)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//span[text()[contains(.,'"+actName+"')]]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void SelectSubMenuInfoButton(String actName){
		explicitWaitvisibilityOfElementLocated(By.xpath("//div[text()[contains(.,'"+actName+"')]]/../.. //paper-icon-button[contains(@class,'info-private')]"));
		WebElement select = SuperTestScript.driver.findElement(By.xpath("//div[text()[contains(.,'"+actName+"')]]/../.. //paper-icon-button[contains(@class,'info-private')]"));
    	select.click();
	}
	
	public void SelectSearchOfTool(String tool){
		/*explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-fab[id^='"+tool+"'][title^='Manage']"));
		WebElement select = SuperTestScript.driver.findElement(By.cssSelector("paper-fab[id^='"+tool+"'][title^='Manage']"));
    	select.click();*/
    	javaScriptExecutorClick(By.cssSelector("paper-fab[title='Search "+tool+"']"));
	}
	
	public void enterSearch(String tool, String text)
	{	
		explicitWaitvisibilityOfElementLocated(By.cssSelector("input[title='Search "+tool+" Input']"));
      	WebElement enterSearch = SuperTestScript.driver.findElement(By.cssSelector("input[title='Search "+tool+" Input']"));

		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(enterSearch).click().sendKeys(text).perform();
	}
	
	public void SelectManageFaceOfTool(String tool){
		/*explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-fab[id^='"+tool+"'][title^='Manage']"));
		WebElement select = SuperTestScript.driver.findElement(By.cssSelector("paper-fab[id^='"+tool+"'][title^='Manage']"));
    	select.click();*/
    	javaScriptExecutorClick(By.cssSelector("paper-fab[id^='"+tool+"'][title^='Manage']"));
	}
	
	public void SelectExitManageFaceOfTool(String tool){
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-fab[title='Exit "+tool+" Management Face']"));
		WebElement select = SuperTestScript.driver.findElement(By.cssSelector("paper-fab[title='Exit "+tool+" Management Face']"));
    	select.click();
	}
	
	public void SelectDeleteToolButton(String tool){
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-fab[id^='"+tool+"'][id$='Delete']"));
		WebElement select = SuperTestScript.driver.findElement(By.cssSelector("paper-fab[id^='"+tool+"'][id$='Delete']"));
    	select.click();
	}
	
	public void SelectFullScreenToolButton(String tool){
		explicitWaitvisibilityOfElementLocated(By.cssSelector("paper-fab[id^='"+tool+"'][id$='Fullscreen']"));
		WebElement select = SuperTestScript.driver.findElement(By.cssSelector("paper-fab[id^='"+tool+"'][id$='Fullscreen']"));
    	select.click();
	}
	
	public void selectBackArrow(String tool)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//paper-button[contains(text( ),'"+ tool +"')]/../paper-fab[@icon='icons:arrow-back']"));
		WebElement selectBackButton = SuperTestScript.driver.findElement(By.xpath("//paper-button[contains(text( ),'"+ tool +"')]/../paper-fab[@icon='icons:arrow-back']"));
    	selectBackButton.click();
		//paper-button[contains(text( ),'Announcements')]/../paper-fab[@icon='icons:arrow-back']
		//explicitWaitvisibilityOfElementLocated(selectArrowBackBy);
		//selectArrowBack.click();
	}
	
	public void SelectAdd(String tool)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//paper-button[contains(text( ),'"+ tool +"')]/.. //paper-fab[@icon='icons:add']"));
		WebElement selectAddButton = SuperTestScript.driver.findElement(By.xpath("//paper-button[contains(text( ),'"+ tool +"')]/.. //paper-fab[@icon='icons:add']"));
    	selectAddButton.click();
		//paper-button[contains(text( ),'Announcements')]/.. //paper-fab[@icon='icons:add']
		//explicitWaitvisibilityOfElementLocated(SelectAddBy);
		//SelectAdd.click();
	}
	
	public String ListofSelectedIndGrp(List<WebElement> indgrp)
	{
		explicitWaitvisibilityOfElementLocated(listOfIndGrpSelectTabBy);
		//Collection list = new LinkedList();
		
		List<WebElement> indgroups = SuperTestScript.driver.findElements(listOfIndGrpSelectTabBy);
		/*for(int i=0; i<=indgroups.size()-1; i++)
		{
			indgroups.add(i);
		}*/
		indgroups.equals(indgrp);
		
		return null;
	}
	
	public void SelectdisplayButton(String tool)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//div[contains(text( ),'"+ tool +"')]/.. //volute-app-feature-control[@id='showEdit']"));
		WebElement selectDisplayButton = SuperTestScript.driver.findElement(By.xpath("//div[contains(text( ),'"+ tool +"')]/.. //volute-app-feature-control[@id='showEdit']"));
    	selectDisplayButton.click();
	}
	
	public void SelectInfoButton(String tool)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//paper-button[@title='"+tool+"']/../../.. //div[@aria-label='clickable Change to Info view']"));
		WebElement selectInfButton = SuperTestScript.driver.findElement(By.xpath("//paper-button[@title='"+tool+"']/../../.. //div[@aria-label='clickable Change to Info view']"));
    	selectInfButton.click();
	}
	
	public void SelectContentButton(String tool)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//paper-button[@title='"+tool+"']/../../.. //div[@aria-label='clickable Change to Content view']"));
		WebElement selectContentButton = SuperTestScript.driver.findElement(By.xpath("//paper-button[@title='"+tool+"']/../../.. //div[@aria-label='clickable Change to Content view']"));
    	selectContentButton.click();
	}
	
	public void SelectConfigurationButton(String tool)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//paper-button[@title='"+tool+"']/../../.. //div[@aria-label='clickable Change to Configuration view']"));
		WebElement selectConfigButton = SuperTestScript.driver.findElement(By.xpath("//paper-button[@title='"+tool+"']/../../.. //div[@aria-label='clickable Change to Configuration view']"));
    	selectConfigButton.click();
	}
	
	public void SelectFeaturesButton(String tool)
	{
		explicitWaitvisibilityOfElementLocated(By.xpath("//paper-button[@title='"+tool+"']/../../.. //div[@aria-label='clickable Change to Features view']"));
		WebElement selectFeatButton = SuperTestScript.driver.findElement(By.xpath("//paper-button[@title='"+tool+"']/../../.. //div[@aria-label='clickable Change to Features view']"));
    	selectFeatButton.click();
	}
	
	public void Savedisplay(String tool){
		explicitWaitvisibilityOfElementLocated(By.xpath("//div[contains(text( ),'"+ tool +"')]/../paper-button[@title='Save Tool Display Name']"));
		WebElement selectSave = SuperTestScript.driver.findElement(By.xpath("//div[contains(text( ),'"+ tool +"')]/../paper-button[@title='Save Tool Display Name']"));
    	selectSave.click();
		//div[contains(text( ),'Slides')]/../paper-button
	}
	
	public String fetchMsgDisplayName()
	{	
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(updatedMsgDisplayNameBy);
		//javaScriptExecutorNoClick(By.cssSelector("div[title='"+title+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}		
//		explicitWaitvisibilityOfElementLocated(updatedMsgDisplayNameBy);
//		String msg = updatedMsgDisplayName.getText();
//		return msg;
	}
	
	public void clickBackSaveListButton(String name){
		explicitWaitvisibilityOfElementLocated(By.xpath("//paper-button[@title='Back To "+name+" List']"));
		WebElement selectSave = SuperTestScript.driver.findElement(By.xpath("//paper-button[@title='Back To "+name+" List']"));
    	selectSave.click();
		//div[contains(text( ),'Slides')]/../paper-button
	}
	
	public void fetchTitleTool(String title)
	{
		//try {
			//SuperTestScript.driver.findElement(By.cssSelector("div[title='"+title+"']"));
		javaScriptExecutorNoClick(By.cssSelector("div[title='"+title+"']"));
			//return "Found";
		//}
		//catch(NoSuchElementException e){
			//return "Not Found";
		//}
	}
	
	public String fetchTitleTool2(String title)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("div[title='"+title+"']"));
		//javaScriptExecutorNoClick(By.cssSelector("div[title='"+title+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchWaterMarkTool(String title)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//div[text()[contains(.,'"+title+"')]]"));
		//javaScriptExecutorNoClick(By.xpath("//div[text()[contains(.,'"+title+"')]]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchWaterMarkLS(String title)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//p[text()[contains(.,'"+title+"')]]"));
		//javaScriptExecutorNoClick(By.xpath("//div[text()[contains(.,'"+title+"')]]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchAppToolShows(String title)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector(title));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public void fetchAppTool(String title)
	{
		boolean flag = true;
		while(flag){
			String result = fetchAppToolShows(title);
			
			if(result.equalsIgnoreCase("Found")){
				javaScriptExecutorNoClick(By.cssSelector(title));
				flag = false;
			}
			else{
				flag = true;
			}
		}
		//try {
			//SuperTestScript.driver.findElement(By.cssSelector("div[title='"+title+"']"));
			//return "Found";
		//}
		//catch(NoSuchElementException e){
			//return "Not Found";
		//}
	}
	
	public String fetchTitleToolDesign(String title)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("volute-app-grid[id='mashupGridActDesign'] div[title='"+title+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
//	//navigation
//	public static WebElement navigation = driver.findElement(By.cssSelector("paper-icon-item[title='Navigation']"));
//	
//	//Home
//	public static WebElement home = driver.findElement(By.id("Home"));
//	
//	//Notes
//		public static WebElement notes = driver.findElement(By.id("Notes"));
//		
//	//Collaborate
//		public static WebElement collaborate = driver.findElement(By.id("Collaborate"));
//		
//	//Discussion
//		public static WebElement discussion = driver.findElement(By.id("Discussion"));
//		
//	//Profile
//		public static WebElement profile = driver.findElement(By.id("Profile"));
//			
//	//Manage Users
//		public static WebElement manageUsers = driver.findElement(By.id("Manage Users"));
//		
//	//Manage Programs
//		public static WebElement managePrograms = driver.findElement(By.id("Manage Programs"));
//					
//	//Private Marketplace
//		public static WebElement privateMarketplace = driver.findElement(By.id("Private Marketplace"));
//	
//	//logout	
//		public static WebElement logout = driver.findElement(By.xpath("//paper-item[@title='Logout']"));
//		
	

}
