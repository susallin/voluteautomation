package education.volute.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import education.volute.common_lib.SuperTestScript;

public class Biography extends NavigatorTool {

	/*@FindBy(id = "Help")
	private WebElement bioHelp;
	private By bioHelpBy = By.id("Help");
	
	@FindBy(id = "Share")
	private WebElement bioShare;
	private By bioShareBy = By.id("Share");*/
	
	@FindBy(css = "paper-fab[title='Manage Biography']")
	private WebElement manageBio;
	private By manageBioBy = By.cssSelector("paper-fab[title='Manage Biography']");
	
	@FindBy(id = "fullscreen")
	private WebElement bioFullScreen;
	private By bioFullScreenBy = By.id("fullscreen");
	
	@FindBy(css = "paper-toolbar[id='paperToolbar'] > div > div[title='Biography']")
	private WebElement bioHeader;
	private By bioHeaderBy = By.cssSelector("paper-toolbar[id='paperToolbar'] > div > div[title='Biography']");
	
	@FindBy(css = "paper-input[name='firstName'] input[name='firstName']")
	private WebElement bioFirstName;
	private By bioFirstNameBy = By.cssSelector("paper-input[name='firstName'] input[name='firstName']");
	
	@FindBy(css = "paper-input[label='Middle Name'] input")
	private WebElement bioMiddleName;
	private By bioMiddleNameBy = By.cssSelector("paper-input[label='Middle Name'] input");
	
	@FindBy(css = "paper-input[name='lastName'] input[name='lastName']")
	private WebElement bioLastName;
	private By bioLastNameBy = By.cssSelector("paper-input[name='lastName'] input[name='lastName']");
	
	@FindBy(css = "paper-input[label='City'] input")
	private WebElement bioCity;
	private By bioCityBy = By.cssSelector("paper-input[label='City'] input");
	
	@FindBy(css = "paper-dropdown-menu[label='Country'] paper-input-container")
	private WebElement bioCountry;
	private By bioCountryBy = By.cssSelector("paper-dropdown-menu[label='Country'] paper-input-container");
	
	// We selecting United States
	@FindBy(xpath = "//paper-dropdown-menu[@label='Country'] //paper-item[1]")
	private WebElement bioSelectCt;
	private By bioSelectCtBy = By.xpath("//paper-dropdown-menu[@label='Country'] //paper-item[1]");
	
	@FindBy(css = "paper-input[label='State'] input")
	private WebElement bioState;
	private By bioStateBy = By.cssSelector("paper-input[label='State'] input");
	
	@FindBy(css = "paper-input[name='email'] input")
	private WebElement bioEmail;
	private By bioEmailBy = By.cssSelector("paper-input[name='email'] input");
	
	@FindBy(css = "paper-input[label='Add organization here'] input")
	private WebElement bioOrganization;
	private By bioOrganizationBy = By.cssSelector("paper-input[label='Add organization here'] input");
	
	@FindBy(css = "paper-input[label='Add title here'] input")
	private WebElement bioTitle;
	private By bioTitleBy = By.cssSelector("paper-input[label='Add title here'] input");
	
	@FindBy(css = "bio-positions paper-button")
	private WebElement bioAddPosition;
	private By bioAddPositionBy = By.cssSelector("bio-positions paper-button");
	
	@FindBy(css = "vaadin-upload[title='Image Uploader'] paper-button[id='addFiles']")
	private WebElement bioSelectFile;
	private By bioSelectFileBy = By.cssSelector("vaadin-upload[title='Image Uploader'] paper-button[id='addFiles']");
	
	@FindBy(css = "bio-picture paper-fab[icon='icons:clear']")
	private WebElement bioClearImgBioButton;
	private By bioClearImgBioButtonBy = By.cssSelector("bio-picture paper-fab[icon='icons:clear']");
	
	@FindBy(css = "iron-selector[class*='style-scope volute-app-biography-face2-module'] > div:nth-of-type(2)")
	private WebElement bioBiographyButton;
	private By bioBiographyButtonBy = By.cssSelector("iron-selector[class*='style-scope volute-app-biography-face2-module'] > div:nth-of-type(2)");
	
	//@FindBy(css = "profile-bio > div > paper-textarea > paper-input-container")
	//private WebElement ProfileBiography;
	//private By ProfileBiographyBy = By.cssSelector("/profile-bio > div > paper-textarea > paper-input-container");
	
	@FindBy(css = "paper-textarea[class^='bio-text'] textarea")
	private WebElement bioBiography;
	private By bioBiographyBy = By.cssSelector("paper-textarea[class^='bio-text'] textarea");
	//div[id='mirror']
	//textarea
	//profile-bio > div > paper-textarea > paper-input-container
	
	@FindBy(css = "iron-selector[class*='style-scope volute-app-biography-face2-module'] > div:nth-of-type(3)")
	private WebElement bioLinksButton;
	private By bioLinksButtonBy = By.cssSelector("iron-selector[class*='style-scope volute-app-biography-face2-module'] > div:nth-of-type(3)");
	
	@FindBy(css = "input[placeholder='Add linkedin URL here']")
	private WebElement bioLinkedin;
	private By bioLinkedinBy = By.cssSelector("input[placeholder='Add linkedin URL here']");
	
	@FindBy(css = "input[placeholder='Add facebook URL here']")
	private WebElement bioFacebook;
	private By bioFacebookBy = By.cssSelector("input[placeholder='Add facebook URL here']");
	
	@FindBy(css = "input[placeholder='Add twitter URL here']")
	private WebElement bioTwitter;
	private By bioTwitterBy = By.cssSelector("input[placeholder='Add twitter URL here']");
	
	@FindBy(css = "input[placeholder='Add website URL here']")
	private WebElement bioWebsite;
	private By bioWebsiteBy = By.cssSelector("input[placeholder='Add website URL here']");
	
	@FindBy(xpath = "//volute-app-biography-face2-module //paper-button[contains(text(),'Save')]")
	private WebElement bioSaveButton;
	private By bioSaveButtonBy = By.xpath("//volute-app-biography-face2-module //paper-button[contains(text(),'Save')]");
	
	@FindBy(css = "paper-toast[title='Profile picture saved successfully']")
	private WebElement bioImgSavedMsg;
	private By bioImgSavedMsgBy = By.cssSelector("paper-toast[title='Profile picture saved successfully']");
	
	@FindBy(css = "paper-toast[title='Profile picture deleted successfully']")
	private WebElement bioImgDeletedMsg;
	private By bioImgDeletedMsgBy = By.cssSelector("paper-toast[title='Profile picture deleted successfully']");
	
	@FindBy(css = "paper-toast[title='Biography saved successfully']")
	private WebElement bioSavedMsg;
	private By bioSavedMsgBy = By.cssSelector("paper-toast[title='Biography saved successfully']");
	
	@FindBy(css = "paper-toast[title='Please enter a valid last name.']")
	private WebElement missingInfoMsgBio;
	private By missingInfoMsgBioBy = By.cssSelector("paper-toast[title='Please enter a valid last name.']");
	
	@FindBy(css = "div[id='tabsContent'] > paper-tab:nth-of-type(1)")
	private WebElement bioContentTab;
	private By bioContentTabBy = By.cssSelector("div[id='tabsContent'] > paper-tab:nth-of-type(1)");
	
	@FindBy(css = "div[id='tabsContent'] > paper-tab:nth-of-type(2)")
	private WebElement bioFeaturesTab;
	private By bioFeaturesTabBy = By.cssSelector("div[id='tabsContent'] > paper-tab:nth-of-type(2)");
	
	@FindBy(css = "paper-dropdown-menu[label='Select Role']")
	private WebElement bioSelectRoleMenu;
	private By bioSelectRoleMenuBy = By.cssSelector("paper-dropdown-menu[label='Select Role']");
	
	@FindBy(css = "div:nth-of-type(2)[class*= 'done-button style-scope app-management']")
	private WebElement bioDoneButton;
	private By bioDoneButtonBy = By.cssSelector("div:nth-of-type(2)[class*= 'done-button style-scope app-management']");
	
	public Biography()
	{
		PageFactory.initElements(SuperTestScript.driver, this);
		
	}
	
	/*-----------------------------------------PROFILE METHODS-----------------------------------------*/
	
	/*public void clickOnBioHelp()
	{
		explicitWaitvisibilityOfElementLocated(bioHelpBy);
		bioHelp.click();
	}
	
	public void clickOnBioShare()
	{
		explicitWaitvisibilityOfElementLocated(bioShareBy);
		bioShare.click();
	}*/
	
	public void clickOnBioManage()
	{
		//explicitWaitvisibilityOfElementLocated(manageBioBy);
		//manageBio.click();
		javaScriptExecutorClick(manageBioBy);
	}
	
	public void clickOnBioFullScreen()
	{
		explicitWaitvisibilityOfElementLocated(bioFullScreenBy);
		bioFullScreen.click();
	}
	
	public String fetchBioHeader()
	{
		explicitWaitelementToBeClickable(bioHeaderBy);
		String msg = bioHeader.getText();
		return msg;
	}
	
	public void enterBioFirstName(String bioFName)
	{
		explicitWaitvisibilityOfElementLocated(bioFirstNameBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(bioFirstName).click().sendKeys(bioFName).perform();
		
	}
	
	public void enterBioMiddleName(String bioMName)
	{
		explicitWaitvisibilityOfElementLocated(bioMiddleNameBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(bioMiddleName).click().sendKeys(bioMName).perform();
		
	}
	
	public void enterBioLastName(String bioLName)
	{
		explicitWaitvisibilityOfElementLocated(bioLastNameBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(bioLastName).click().sendKeys(bioLName).perform();
		
	}
	
	public void clickOnBioCountry() //throws IOException 
	{
		explicitWaitvisibilityOfElementLocated(bioCountryBy);
		bioCountry.click();
	}
	
	public void clickOnBioSelectCt() //throws IOException 
	{
		explicitWaitvisibilityOfElementLocated(bioSelectCtBy);
		bioSelectCt.click();
	}
	
	public void enterBioEmail(String biogEmail)
	{
		explicitWaitvisibilityOfElementLocated(bioEmailBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(bioEmail).click().sendKeys(biogEmail).perform();
		
	}
	
	public void enterCity(String biogCity)
	{
		explicitWaitvisibilityOfElementLocated(bioCityBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(bioCity).click().sendKeys(biogCity).perform();
		
	}
	
	public void enterBioState(String biogState)
	{
		explicitWaitvisibilityOfElementLocated(bioStateBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(bioState).click().sendKeys(biogState).perform();
		
	}
	
	public void enterBioOrganization(String biogOrg)
	{
		explicitWaitvisibilityOfElementLocated(bioOrganizationBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(bioOrganization).click().sendKeys(biogOrg).perform();
		
	}
	
	public void enterBioTitle(String biogTitle)
	{
		explicitWaitvisibilityOfElementLocated(bioTitleBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(bioTitle).click().sendKeys(biogTitle).perform();
		
	}
	
	public void clickOnBioAddPosition() //throws IOException 
	{
		explicitWaitvisibilityOfElementLocated(bioAddPositionBy);
		bioAddPosition.click();
	}
	
	public void clickOnBioSelectFile(String fileName) throws Exception 
	{
		javaScriptExecutorUpload(bioSelectFileBy, fileName);
		//explicitWaitvisibilityOfElementLocated(bioSelectFileBy);
		//bioSelectFile.click();
	}
	
	public void clickOnClearImgBioButton()
	{
		explicitWaitvisibilityOfElementLocated(bioClearImgBioButtonBy);
		bioClearImgBioButton.click();
	}
	
	public void clickOnBioBiographyButton()
	{
		explicitWaitvisibilityOfElementLocated(bioBiographyButtonBy);
		bioBiographyButton.click();
	}
	
	public void enterBioBiography(String biogBiography)
	{
		int desc_size = SuperTestScript.driver.findElements(bioBiographyBy).size();
		SuperTestScript.driver.findElements(bioBiographyBy).get(desc_size-1).sendKeys(biogBiography);
		//explicitWaitvisibilityOfElementLocated(ProfileBiographyBy);
		//Actions a1 = new Actions(SuperTestScript.driver);
		//a1.moveToElement(ProfileBiography).click().sendKeys(proBiography).perform();
		
	}
	
	public void clickOnLinksBiography()
	{
		explicitWaitvisibilityOfElementLocated(bioLinksButtonBy);
		bioLinksButton.click();
	}
	
	public void enterBioLinks(String biogLink)
	{
		explicitWaitvisibilityOfElementLocated(bioLinkedinBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(bioLinkedin).click().sendKeys(biogLink).perform();
		
	}
	
	public void enterBioFacebook(String biogFb)
	{
		explicitWaitvisibilityOfElementLocated(bioFacebookBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(bioFacebook).click().sendKeys(biogFb).perform();
		
	}
	
	public void enterBioTwitter(String biogTw)
	{
		explicitWaitvisibilityOfElementLocated(bioTwitterBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(bioTwitter).click().sendKeys(biogTw).perform();
		
	}
	
	public void enterBioWebsite(String biogWe)
	{
		explicitWaitvisibilityOfElementLocated(bioWebsiteBy);
		Actions a1 = new Actions(SuperTestScript.driver);
		a1.moveToElement(bioWebsite).click().sendKeys(biogWe).perform();
		
	}
	
	public void clickOnBioSaveButton()
	{
		explicitWaitvisibilityOfElementLocated(bioSaveButtonBy);
		bioSaveButton.click();
	}
	
	public void clickOnBioContentTab()
	{
		explicitWaitvisibilityOfElementLocated(bioContentTabBy);
		bioContentTab.click();
	}
	
	public void clickOnBioFeaturesTab()
	{
		explicitWaitvisibilityOfElementLocated(bioFeaturesTabBy);
		bioFeaturesTab.click();
	}
	
	public void clickOnBioSelectRoleMenu()
	{
		explicitWaitvisibilityOfElementLocated(bioSelectRoleMenuBy);
		bioSelectRoleMenu.click();
	}
	
	public void clickOnBioDoneButton()
	{
		explicitWaitvisibilityOfElementLocated(bioDoneButtonBy);
		bioDoneButton.click();
	}
	
	public String fetchBioImgSavedMsg()
	{
		
		explicitWaitvisibilityOfElementLocated(bioImgSavedMsgBy);
		String msg = bioImgSavedMsg.getText();
		return msg;
	}
	
	public String fetchBioImgDeletedMsg()
	{
		
		explicitWaitvisibilityOfElementLocated(bioImgDeletedMsgBy);
		String msg = bioImgDeletedMsg.getText();
		return msg;
	}
	
	public String fetchBioSavedMsg()
	{
		
		explicitWaitvisibilityOfElementLocated(bioSavedMsgBy);
		String msg = bioSavedMsg.getText();
		return msg;
	}
	
	public String fetchMissingInfoMsgBio()
	{
		
		explicitWaitvisibilityOfElementLocated(missingInfoMsgBioBy);
		String msg = missingInfoMsgBio.getText();
		return msg;
	}
	
	public String fetchTopFacesBioShows(String bioStr)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//span[contains(text(),'"+bioStr+"')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	// Organization/Title
	public String fetchMidFacesBioShows(String bioStr)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//b[contains(text(),'"+bioStr+"')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchLinks(String bioStr)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.cssSelector("a[href='"+bioStr+"']"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchBiography(String bioStr)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(By.xpath("//volute-app-biography-face1-module //div[contains(text(),'"+bioStr+"')]"));
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
}
