package education.volute.manage_pgm_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.ManageActivities;
import education.volute.pages.Landing;
import education.volute.pages.ManagePrograms;
import education.volute.pages.ManageSubActivities;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.SoftAssertCheck;

public class DeleteActivity_TC_17 extends SuperTestScript
{
//@Test
public void testDeleteActivity_TC_17() 

{
	//LoginPage lp = new LoginPage();
	NavigatorTool nt = new NavigatorTool();
	//MenuItems mi	= new MenuItems();
	//Landing ld = new Landing();
	SoftAssertCheck soft = new SoftAssertCheck();
	ManagePrograms mp = new ManagePrograms();
	ManageSubActivities msa = new ManageSubActivities();
	ManageActivities ma = new ManageActivities();
	
	ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePrograms, "DeleteActSubs");
	
	for(int i=1; i<=ExcelUtils.getRowcount(); i++)
	{
	
	//String uName = ExcelUtils.getCellData(i, 1);
    //String pwd = ExcelUtils.getCellData(i, 2);
    String pgmNameTxt = ExcelUtils.getCellData(i, 1);
    String parentActNameTxt = ExcelUtils.getCellData(i, 2);
    String actNameTxt = ExcelUtils.getCellData(i, 3);
    String subSubActNameTxt = ExcelUtils.getCellData(i, 4);
    String actType = ExcelUtils.getCellData(i, 5);
    String expRes = ExcelUtils.getCellData(i, 6);
	
	/*lp.login(uName, pwd);
	nt.clickOnMainMenu();
	mi.clickOnNavigation();
	mi.clickOnProgram();
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	ld.clickOnLandingMP();
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	//mi.clickOnManagePgms();
	mp.clickOnArrowToAddAct(pgmNameTxt);
	ma.clickOnselectedActivity(actNameTxt);
	ma.clickOnDeleteActicon(actNameTxt);
	ma.clickOnConfirmPopupYesButton();*/
    mp.clickOnselectedprogram(pgmNameTxt);
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	mp.clickOnExpandProgram(pgmNameTxt);
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	/*mp.clickOnSelectProgramAct(pgmNameTxt);
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}*/
	String actArrow1 = ma.fetchActArrow(parentActNameTxt);
	System.out.println(actArrow1);
	if(actArrow1.equalsIgnoreCase("Found")){
		ma.clickOnArrowSub(parentActNameTxt);
		try {
			Thread.sleep(7000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	if(!actNameTxt.isEmpty()){
		ma.clickOnArrowSub(actNameTxt);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ma.clickSelectedSubAct(subSubActNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		// First Act
		ma.clickOnDeleteAct(subSubActNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	} else{
		ma.clickSelectedSubAct(subSubActNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		// First Act
		ma.clickOnDeleteAct(subSubActNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	/*ma.clickSelectedSubAct(actNameTxt);
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	ma.clickOnDeleteAct(actNameTxt);
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}*/
	
	ma.clickOnConfirmPopupYesButton();
	try {
		Thread.sleep(2000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	String actRes = mp.fetchPgmDeletedMsg();
	System.out.println(actRes);
	if(actRes.equalsIgnoreCase("Found")){
		soft.assertTrue(true, "Checking if toast (Learning Space deleted successfully) shows");
		System.out.println("The test case passed, the toast shows.");
	}else{
		soft.assertTrue(false, "The test case failed, the toast does not show.\n");
	}
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	/*if(actType.equalsIgnoreCase("Activity")){
		actRes = ma.fetchActDeletedMsg();
		System.out.println(actRes);
	} else{
		actRes = msa.fetchSubActDeletedMsg();
		System.out.println(actRes);
	}*/
	
	String actRes1 = ma.fetchActShows(subSubActNameTxt);
	System.out.println(actRes1);
	if(actRes1.equalsIgnoreCase("Not Found")){
		soft.assertTrue(true, "Checking if the content shows");
		System.out.println("The test case passed, the content is deleted.");
	}else{
		soft.assertTrue(false, "The test case failed, the content still shows.\n");
	}
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	nt.refreshPage();
	try {
		Thread.sleep(15000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	mp.clickOnselectedprogram(pgmNameTxt);
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
		
	mp.clickOnExpandProgram(pgmNameTxt);
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	String actArrow = ma.fetchActArrow(parentActNameTxt);
	System.out.println(actArrow);
	if(actArrow.equalsIgnoreCase("Found")){
		ma.clickOnArrowSub(parentActNameTxt);
		try {
			Thread.sleep(7000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	if(!actNameTxt.isEmpty()){
		String actArrow2 = ma.fetchActArrow(actNameTxt);
		System.out.println(actArrow2);
		if(actArrow2.equalsIgnoreCase("Found")){
			ma.clickOnArrowSub(actNameTxt);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	// This will determine if Value is still showing or not.
	String actShows = ma.fetchActShows(subSubActNameTxt);
	System.out.println(actShows);
	if(actRes1.equalsIgnoreCase("Not Found")){
		soft.assertTrue(true, "Checking if the content shows");
		System.out.println("The test case passed, the content is deleted.");
	}else{
		soft.assertTrue(false, "The test case failed, the content still shows.\n");
	}
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	ExcelUtils.setCellData(actShows, i, 8, Constant.File_TestData_ManagePrograms);
	try {
		Thread.sleep(2000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	if(!actNameTxt.isEmpty()){
		String actArrow4 = ma.fetchActArrow(actNameTxt);
		System.out.println(actArrow4);
		if(actArrow4.equalsIgnoreCase("Found")){
			ma.closeArrowSub(actNameTxt);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	String actArrow3 = ma.fetchActArrow(parentActNameTxt);
	System.out.println(actArrow3);
	if(actArrow3.equalsIgnoreCase("Found")){
		ma.closeArrowSub(parentActNameTxt);
		try {
			Thread.sleep(7000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	mp.clickOnHideProgram(pgmNameTxt);
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	// This will determine if Value is still showing or not.
	/*DelShows(i, actNameTxt);	
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}*/
	
	/*ma.closeArrowSub(parentActNameTxt);
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}*/		
	/*mp.clickOnBackButtonPgmList();
	try {
		Thread.sleep(7000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}*/
	//mi.clickOnMainMenu();
	//mi.clickOnLogout();
	
	ExcelUtils.setCellData(actRes, i, 7, Constant.File_TestData_ManagePrograms );
	
	Boolean status = ValidationLib.verifyMsg(expRes, actRes);
	
	if(status)
	{
		ExcelUtils.setCellData("pass", i, 9,Constant.File_TestData_ManagePrograms);
	}
	else 
    {
		ExcelUtils.setCellData("Fail", i, 9,Constant.File_TestData_ManagePrograms);
	}
	
	
	}
	soft.assertAll();
}

	/*public void DelShows(int i, String actName){
		ManageActivities ma = new ManageActivities();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePrograms, "DeleteActSubsShows");
		
		String expRes = ExcelUtils.getCellData(i, 1);
		
		String actRes = ma.fetchActShows(actName);
		System.out.println(actRes);
		
		ExcelUtils.setCellData(actRes, i, 2, Constant.File_TestData_ManagePrograms );
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 3,Constant.File_TestData_ManagePrograms);
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 3,Constant.File_TestData_ManagePrograms);
		}
	}*/
}


