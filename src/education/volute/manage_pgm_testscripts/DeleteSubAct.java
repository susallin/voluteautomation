package education.volute.manage_pgm_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Landing;
import education.volute.pages.LoginPage;
import education.volute.pages.ManageActivities;
import education.volute.pages.ManageSubActivities;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;

public class DeleteSubAct extends SuperTestScript {

	@Test
	public void testDeleteActivity_TC_17() 

	{
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePgms, "DeleteSubAct");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String uName = ExcelUtils.getCellData(i, 1);
	    String pwd = ExcelUtils.getCellData(i, 2);
	    String pgmNameTxt = ExcelUtils.getCellData(i, 3);
	    String subActNameTxt = ExcelUtils.getCellData(i, 4);
	    String expRes = ExcelUtils.getCellData(i, 5);
	    
	    LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		MenuItems mi	= new MenuItems();
		Landing ld = new Landing();
		ManagePrograms mp = new ManagePrograms();
		ManageActivities ma = new ManageActivities();
		ManageSubActivities msa = new ManageSubActivities();
		
		lp.login(uName, pwd);
		nt.clickOnMainMenu();
		mi.clickOnNavigation();
		mi.clickOnProgram();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ld.clickOnLandingMP();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//mi.clickOnManagePgms();
		mp.clickOnArrowToAddAct(pgmNameTxt);
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ma.clickOnselectedSubActivityTwo(subActNameTxt);
		msa.clickOnDeleteSubActicon(subActNameTxt);
		ma.clickOnConfirmPopupYesButton();
		
		String actRes = msa.fetchSubActDeletedMsg();
		System.out.println(actRes);
		
		mi.clickOnMainMenu();
		mi.clickOnLogout();
		
		ExcelUtils.setCellData(actRes, i, 6, Constant.File_TestData_ManagePgms );
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 7,Constant.File_TestData_ManagePgms);
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 7,Constant.File_TestData_ManagePgms);
		}
		
		
		}
		
		}
}
