package education.volute.manage_pgm_testscripts;

import java.util.Iterator;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Landing;
import education.volute.pages.LoginPage;
import education.volute.pages.ManageActivities;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.SoftAssertCheck;

public class AddActivityPrivateExclusivePublished_TC_05 extends SuperTestScript

	{
		//@Test
		public void testAddActivityPrivateExclusivePublished_TC_05() throws Exception 
		
		{
			LoginPage lp = new LoginPage();
			NavigatorTool nt = new NavigatorTool();
			MenuItems mi = new MenuItems();
			//Landing ld = new Landing();
			ManagePrograms mp = new ManagePrograms();
			ManageActivities ma = new ManageActivities();
			ReadCsvFiles csv = new ReadCsvFiles();
			SoftAssertCheck soft = new SoftAssertCheck();
					
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePrograms, "PrivExActivity");
			
			for(int i=1; i<=ExcelUtils.getRowcount(); i++)
			{
			
			//String uName = ExcelUtils.getCellData(i, 1);
	        //String pwd = ExcelUtils.getCellData(i, 2);
	        String pgmNameTxt = ExcelUtils.getCellData(i, 1);
	        String actNameTxt = ExcelUtils.getCellData(i, 2);
	        String actDescTxt = ExcelUtils.getCellData(i, 3);
	        String startDateTxt = ExcelUtils.getCellData(i, 4);
	        String endDateTxt = ExcelUtils.getCellData(i, 5);
	        String actToggle= ExcelUtils.getCellData(i, 6);
	        String indNames = ExcelUtils.getCellData(i, 7);
	        //String lsNameTxt = ExcelUtils.getCellData(i, 8);	        
	        String expRes = ExcelUtils.getCellData(i, 8);
	        boolean flag = true;
			/*lp.login(uName, pwd);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			nt.clickOnMainMenu();
			mi.clickOnNavigation();
			mi.clickOnProgram();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			ld.clickOnLandingMP();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			//mi.clickOnManagePgms();
			mp.clickOnArrowToAddAct(pgmNameTxt);
			ma.clickOnAddActButton();
			ma.enterActName(actNameTxt);
			ma.enterActDesc(actDescTxt);
		//	ma.enterActtartDate(startDateTxt);
		//	ma.enterActEndDate(endDateTxt);
			ma.clickOnAssignLSButton();
			ma.clickOnArrowToAssignLS(lsNameTxt);
			ma.clickOnAssign();*/
	        
			mp.clickOnselectedprogram(pgmNameTxt);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mp.clickOnSelectProgramAct(pgmNameTxt);
			
			mi.fetchAppTool("activity-upsert div[title='Switch View']");
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			/*ma.clickOnAddActButton();
			try {
				Thread.sleep(8000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
//			nt.clickOnOverviewTab();
//			try {
//				Thread.sleep(2000);
//			} catch (InterruptedException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
			ma.clearActName();
			ma.enterActName(actNameTxt);
			ma.enterActDesc(actDescTxt);
			
			ma.clickAssignUserButon();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			// exclusive
			//ma.clickOnCheckboxButton(actToggle);
			while(flag){
				String result = mi.fetchIndivTab();
				
				if(result.equalsIgnoreCase("Found")){
					
					//We start adding indivs/groups
					// Exclusive
					ma.clickOnCheckboxButton(actToggle);
					
					ma.clickOnConfirmPopupNoButton();
					
					Iterator<String[]> FileName = csv.ReadList(indNames);
					
					if(FileName.hasNext())
						FileName.next();
					
					while(FileName.hasNext()){
						int n = 0;
						String[] Name = FileName.next();
						
						String Type = Name[n];
						String indgrpNameF = Name[n+1];
						
						if(Type.equalsIgnoreCase("Individual")) {
							
							mi.clickOnIndividualTab();
							/*try {
								Thread.sleep(4000);
							} catch (InterruptedException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}*/
							
							mi.selectIndividual(indgrpNameF);
							/*try {
								Thread.sleep(2000);
							} catch (InterruptedException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}*/
							
						} else{
							
							mi.clickOnGroupTab();
							/*try {
								Thread.sleep(4000);
							} catch (InterruptedException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}*/
							
							mi.selectGroup(indgrpNameF);
							/*try {
								Thread.sleep(2000);
							} catch (InterruptedException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}*/
						}
					}
					flag = false;
				}
				else{
					flag = true;
				}
			}
			
			mi.clickOnSelectedTab();
			
			mi.clickOnDoneButton();			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			ma.clickOnSaveAct();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String actRes = mp.fetchPgmEditsSavedSuccessMsg();
			System.out.println(actRes);
			
			//mi.clickOnMainMenu();
			//mi.clickOnLogout();
			
			ExcelUtils.setCellData(actRes, i, 9, Constant.File_TestData_ManagePrograms );
			
			Boolean status = ValidationLib.verifyMsg(expRes, actRes);
			
			if(status)
			{
				ExcelUtils.setCellData("pass", i, 10,Constant.File_TestData_ManagePrograms);
				soft.assertTrue(true, "Toast");
				System.out.println("The test case passed on verifying Activity Toast.");
			}
			else 
		{
				ExcelUtils.setCellData("Fail", i, 10,Constant.File_TestData_ManagePrograms);
				soft.assertTrue(false, "The test case failed on verifying Activity Toast.\n");
			}
			
			
			}
			
			soft.assertAll();
			
			}

	}


