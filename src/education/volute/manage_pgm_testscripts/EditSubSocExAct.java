package education.volute.manage_pgm_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Landing;
import education.volute.pages.LoginPage;
import education.volute.pages.ManageActivities;
import education.volute.pages.ManagePrograms;
import education.volute.pages.ManageSubActivities;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;

public class EditSubSocExAct extends SuperTestScript {

	@Test
	public void testEditSubSocExAct() 

	{
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePgms, "EditSubSocExAct"+ "");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String uName = ExcelUtils.getCellData(i, 1);
	    String pwd = ExcelUtils.getCellData(i, 2);
	    String pgmNameTxt = ExcelUtils.getCellData(i, 3);
	    String actTypeTxt = ExcelUtils.getCellData(i, 4);
	    String oldSubActNameTxt = ExcelUtils.getCellData(i, 5);
	    String newSubActNameTxt = ExcelUtils.getCellData(i, 6);
	    String newSubActDescTxt = ExcelUtils.getCellData(i, 7);
	    String newStartDateTxt = ExcelUtils.getCellData(i,8);
	    String newEndDateTxt = ExcelUtils.getCellData(i, 9);
	    String newLsNameTxt = ExcelUtils.getCellData(i, 10);
	    
	    String expRes = ExcelUtils.getCellData(i, 11);
	    
	    LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		MenuItems mi	= new MenuItems();
		Landing ld = new Landing();
		ManagePrograms mp = new ManagePrograms();
		ManageActivities ma = new ManageActivities();
		ManageSubActivities msa = new ManageSubActivities();
		
		lp.login(uName, pwd);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnMainMenu();
		mi.clickOnNavigation();
		mi.clickOnProgram();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ld.clickOnLandingMP();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//mi.clickOnManagePgms();
		mp.clickOnArrowToAddAct(pgmNameTxt);
		ma.clickOnselectedActivity(oldSubActNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		ma.clickOnEditActicon(oldSubActNameTxt);
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// This will unpublish it.
		msa.clickOnEditPublishToggleButton(oldSubActNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ma.clickOnConfirmPopupYesButton();
		
		msa.clickOnEditSocialToggleButton(oldSubActNameTxt);
		ma.clickOnEditExclusiveToggleButton(oldSubActNameTxt);
		msa.enterEditSubActName(oldSubActNameTxt, " "+ newSubActNameTxt);
		msa.enterEditSubActDesc(oldSubActNameTxt, " "+ newSubActDescTxt);
		
		msa.clickOnEditAssignLSButton(oldSubActNameTxt);
		ma.clickOnArrowToAssignLS(newLsNameTxt);
		ma.clickOnAssign();	
		
		msa.clickOnEditSocialToggleButton(oldSubActNameTxt);
		
		msa.clickOnEditExclusiveToggleButton(oldSubActNameTxt);
		msa.selectAllindividuals();
		ma.clickOnAssignUsersDoneButton();
		
		msa.clickOnEditPublishToggleButton(oldSubActNameTxt);
		msa.clickOnEditSaveSubAct(oldSubActNameTxt);
		
		String actRes = msa.fetchSubActEditsSavedSuccessMsg();
		System.out.println(actRes);
		
		mi.clickOnMainMenu();
		mi.clickOnLogout();
		
		ExcelUtils.setCellData(actRes, i, 11, Constant.File_TestData_ManagePgms );
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 12,Constant.File_TestData_ManagePgms);
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 12,Constant.File_TestData_ManagePgms);
		}
		
		
		}
		
		}
}
