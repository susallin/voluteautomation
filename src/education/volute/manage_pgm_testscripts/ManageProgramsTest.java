package education.volute.manage_pgm_testscripts;

import utilities.DisplayName;

//import education.volute.manage_users_testscripts.AddUsers_TC_01;

public class ManageProgramsTest {

	// Program
	public void AddPgm(String txt) throws Exception{
		AddPgm_TC_01 mpg = new AddPgm_TC_01();
		mpg.testAddPgmTC_01(txt);
	}
	
	public void EditPgm() {
		EditPgm_TC_18 mpg = new EditPgm_TC_18();
		mpg.testEditPgm_TC_18();
	}
	
	public void DeletePgm(String txt) {
		DeletePgm_TC_19 mpg = new DeletePgm_TC_19();
		mpg.testDeletePgm_TC_19(txt);
	}
	//
	
	// Activities/Subs
	public void AddPrivateOpenAct(String str) {
		AddActivityPrivateOpen_TC_03 poa = new AddActivityPrivateOpen_TC_03();
		poa.testAddActivityPrivateOpen_TC_03(str);
	}
	
	public void AddSocialOpenAct() {
		AddActivitySocialOpenPublished_TC_04 soa = new AddActivitySocialOpenPublished_TC_04();
		soa.testAddActivitySocialOpenPublished_TC_04();
	}
	
	public void AddPrivateExAct() throws Exception {
		AddActivityPrivateExclusivePublished_TC_05 pea = new AddActivityPrivateExclusivePublished_TC_05();
		pea.testAddActivityPrivateExclusivePublished_TC_05();
	}
	
	public void AddSocialExAct() throws Exception {
		AddActivitySocialExclusive_TC_06 sea = new AddActivitySocialExclusive_TC_06();
		sea.testAddActivitySocialExclusive_TC_06();
	}
	
	public void AddPrivateOpenSubAct(String str) {
		AddSubActivityPrivateOpen_TC_07 posa = new AddSubActivityPrivateOpen_TC_07();
		posa.testAddSubActivityPrivateOpen_TC_07(str);
	}
	
	public void AddSocialOpenSubAct(String str) {
		AddSubActivitySocialOpen_TC_08 sosa = new AddSubActivitySocialOpen_TC_08();
		sosa.testAddSubActivitySocialOpen_TC_08(str);
	}
	
	public void AddPrivateExSubAct() throws Exception {
		AddSubActivityPrivateExclusive_TC_09 sea = new AddSubActivityPrivateExclusive_TC_09();
		sea.testAddSubActivityPrivateExclusive_TC_09();
	}
	
	public void AddSocialExSubAct() throws Exception {
		AddSubActivitySocialExclusive_TC_10 sea = new AddSubActivitySocialExclusive_TC_10();
		sea.testAddSubActivitySocialExclusive_TC_10();
	}
	
	public void AddPrivateSubSubAct(String str) throws Exception {
		AddSubSubActivityPrivate_TC_11 sea = new AddSubSubActivityPrivate_TC_11();
		sea.testAddSubSubActivityPrivateOpen_TC_11(str);
	}
	
	public void AddSocialExclusiveSubSubAct(String str) throws Exception {
		AddSubSubActivitySocialExclusive_TC_12 sea = new AddSubSubActivitySocialExclusive_TC_12();
		sea.testAddSubSubActivitySocialExclusive_TC_12(str);
	}
	//
	
	//Edit or Delete Activity and Sub
	public void EditActivity(){
		EditActivity_TC_16 eda = new EditActivity_TC_16();
		eda.testEditActivity_TC_16();
	}
	
	public void DeleteActivity(){
		DeleteActivity_TC_17 dela = new DeleteActivity_TC_17();
		dela.testDeleteActivity_TC_17();
	}
	//
	
	//Go to Activity
	public void EditActivity2(){
		EditActivity_TC_16 eda = new EditActivity_TC_16();
		eda.testGoToActivitySub();
	}
	//
	
	// Design Activity to add tools
	public void AddToolDesignAct(String str) throws Exception {
		AddToolDesignAct_TC_02 dsa = new AddToolDesignAct_TC_02();
		dsa.testAddToolDesignAct_TC_02(str);
	}
	//
	
	// Design Activity to delete tools
	public void DeleteToolDesignAct(){ 
		DeleteToolDesignAct_TC_22 dsa = new DeleteToolDesignAct_TC_22();
		dsa.testDeleteToolDesignAct_TC_22();
	}
	
	// Publish an Activity/Sub
	public void PublishActSub() {
		PublishUnpublishActSub poa = new PublishUnpublishActSub();
		poa.testPublishUnpublishActSub();
	}
	//
	
	// Switch View to User
	public void SwitchView(String Role) {
		SwitchViewActSub sw = new SwitchViewActSub();
		sw.testSwitchViewActSub(Role);
	}
	
	// Sequencing with User
	public void Sequencing(String Role) {
		SequencingActSub sw = new SequencingActSub();
		sw.testSequencingActSub(Role);
	}
	
	// Display Name
	public void DisplayName(String Role) {
		DisplayName dn = new DisplayName();
		dn.testDisplayName(Role);
	}
	
	public void CheckToolDesign(String txt, String Role) {
		CheckToolDesignAct dn = new CheckToolDesignAct();
		dn.testCheckToolDesignAct(txt, Role);
	}
	
	public void CheckWaterMarks(String txt, String Role) throws Exception {
		WaterMarks w = new WaterMarks();
		w.testWaterMarks(txt, Role);
	}
	
	public void SearchProgram(String txt) {
		SearchPgm_TC_23 s = new SearchPgm_TC_23();
		s.testSearchPgm_TC_23(txt);
	}
}
