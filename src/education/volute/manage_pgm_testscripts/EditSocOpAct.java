package education.volute.manage_pgm_testscripts;

import org.testng.annotations.Test;

import education.volute.excel_utilities.ExcelUtils;
import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.pages.Landing;
import education.volute.pages.LoginPage;
import education.volute.pages.ManageActivities;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;

public class EditSocOpAct extends SuperTestScript {

	@Test
	public void testEditSocOpAct() {
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePgms, "EditSocOpAct"+ "");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
        
		String uName = ExcelUtils.getCellData(i, 1);
	    String pwd = ExcelUtils.getCellData(i, 2);
	    String pgmNameTxt = ExcelUtils.getCellData(i, 3);
	    //String actTypeTxt = ExcelUtils.getCellData(i, 4);
		String oldActNameTxt = ExcelUtils.getCellData(i, 5);
        String newActNameTxt = ExcelUtils.getCellData(i, 6);
        String newActDescTxt = ExcelUtils.getCellData(i, 7);
        String newStartDateTxt = ExcelUtils.getCellData(i, 8);
        String newEndDateTxt = ExcelUtils.getCellData(i, 9);
        
        String newLsNameTxt = ExcelUtils.getCellData(i, 10);
        
        String expRes = ExcelUtils.getCellData(i, 11);
        
        LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		MenuItems mi	= new MenuItems();
		Landing ld = new Landing();
		ManagePrograms mp = new ManagePrograms();
		ManageActivities ma = new ManageActivities();
        
		lp.login(uName, pwd);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnMainMenu();
		mi.clickOnNavigation();
		mi.clickOnProgram();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ld.clickOnLandingMP();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//mi.clickOnManagePgms();
		mp.clickOnArrowToAddAct(pgmNameTxt);
		
		ma.clickOnselectedActivity(oldActNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		// First Act
		ma.clickOnEditActicon(oldActNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// This will unpublish it.
		ma.clickOnEditPublishToggleButton(oldActNameTxt);
		ma.clickOnConfirmPopupYesButton();
		
		ma.clickOnEditSocialToggleButton(oldActNameTxt);
		ma.clickOnEditUnAssignLSButton(oldActNameTxt);
		ma.clickOnConfirmPopupYesButton();
		
		//ma.clearActName();
		ma.enterEditActName(oldActNameTxt, " "+ newActNameTxt);
		ma.enterEditActDesc(oldActNameTxt, " "+ newActDescTxt);
	//	ma.enterActtartDate(startDateTxt);
	//	ma.enterActEndDate(endDateTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ma.clickOnEditAssignLSButton(oldActNameTxt);
		ma.clickOnArrowToAssignLS(newLsNameTxt);
		ma.clickOnAssign();
		
		ma.clickOnEditSocialToggleButton(oldActNameTxt);
		ma.clickOnEditPublishToggleButton(oldActNameTxt);
		ma.clickOnEditSaveAct(oldActNameTxt);
		
		String actRes = ma.fetchActEditsSavedSuccessMsg();
		System.out.println(actRes);
		
		mi.clickOnMainMenu();
		mi.clickOnLogout();
		
		ExcelUtils.setCellData(actRes, i, 12, Constant.File_TestData_ManagePgms );
		
		Boolean status2 = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status2)
		{
			ExcelUtils.setCellData("pass", i, 13,Constant.File_TestData_ManagePgms);
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 13,Constant.File_TestData_ManagePgms);
	}
		}
	}
}
