package education.volute.manage_pgm_testscripts;

import java.util.Iterator;

import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.ManageActivities;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.SearchElements;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class WaterMarks {

	//@Test
		public void testWaterMarks(String txt, String uRole) throws Exception 
		{
				//LoginPage lp = new LoginPage();
				NavigatorTool nt = new NavigatorTool();
				//SocialHub mj = new SocialHub();
				ReadCsvFiles csv = new ReadCsvFiles();
				ManagePrograms mp = new ManagePrograms();
				ManageActivities ma = new ManageActivities();
				MenuItems mi = new MenuItems();
				SearchElements sr = new SearchElements();
				SoftAssertCheck soft = new SoftAssertCheck();
				ToolDestAndCollections toolD = new ToolDestAndCollections();
			
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePrograms, txt);
				
				for(int i=1; i<=ExcelUtils.getRowcount(); i++)
				{
				
				String toolDest = ExcelUtils.getCellData(i, 1);
		        String actName = ExcelUtils.getCellData(i, 2);
		        String subActName = ExcelUtils.getCellData(i, 3);
		        String pgmNameTxt = ExcelUtils.getCellData(i, 4);
		        String waterMarkFileName = ExcelUtils.getCellData(i, 5);
		        //String toolName = ExcelUtils.getCellData(i, 5);
		        //String appLocName = ExcelUtils.getCellData(i, 6);
		        //String waterMarkName = ExcelUtils.getCellData(i, 7);
				
		        toolD.Destination(toolDest, actName, subActName, pgmNameTxt, uRole);
		        
		        // Here Tool will execute 
//				try {
//					Thread.sleep(15000);
//				} catch (InterruptedException e1) {
//					// TODO Auto-generated catch block
//					e1.printStackTrace();
//				}
	
				
				Iterator<String[]> FileName = csv.ReadList(waterMarkFileName);
				
				if(FileName.hasNext())
					FileName.next();
				
				while(FileName.hasNext()){
					int n = 0;
					String[] Name = FileName.next();

					String appLocName = Name[n];
					String objName = Name[n+1];
					String toolName = Name[n+2];
					String waterMarkFace = Name[n + 3];
					String confirmManage = Name[n + 4];
					String WaterMarkManageFace = Name[n+5];
					
					if(objName.equalsIgnoreCase("Tool")){
						mi.fetchAppTool(appLocName);
						try {
							Thread.sleep(5000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						String actRes = mi.fetchWaterMarkTool(waterMarkFace);
						System.out.println(actRes);
						if(actRes.equalsIgnoreCase("Found")){
							soft.assertTrue(true, "Checking if watermark shows");
							System.out.println("The test case passed, "+toolName+" watermark shows.");
						}else{
							soft.assertTrue(false, "The test case failed, "+toolName+" watermark does not shows.\n");
						}
						try {
							Thread.sleep(3000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						//nt.refreshPage();
					}
					
				    if(confirmManage.equalsIgnoreCase("Not Hidden")){
				    	//read the watermark
				    	toolD.goToManagePrograms();
				    	
				    	String actRes = mi.fetchWaterMarkLS(waterMarkFace);
						System.out.println(actRes);
						if(actRes.equalsIgnoreCase("Found")){
							soft.assertTrue(true, "Checking if watermark shows");
							System.out.println("The test case passed, Learning Space watermark shows.");
						}else{
							soft.assertTrue(false, "The test case failed, Learning Space watermark does not shows.\n");
						}
						try {
							Thread.sleep(3000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						mp.clickOnAddPgmBtn();
						
						mi.fetchAppTool("activity-upsert div[title='Switch View']");
						try {
							Thread.sleep(2000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						ma.clickOnActListBackButton();
						try {
							Thread.sleep(7000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
				    }
				    
				    if(confirmManage.equalsIgnoreCase("Hidden")){
				    	mp.clickOnExpandProgram(toolName);
						try {
							Thread.sleep(3000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
				    	String actRes = mi.fetchWaterMarkLS(waterMarkFace);
						System.out.println(actRes);
						if(actRes.equalsIgnoreCase("Found")){
							soft.assertTrue(true, "Checking if watermark shows");
							System.out.println("The test case passed, Nested Learning Space watermark shows.");
						}else{
							soft.assertTrue(false, "The test case failed, Nested Learning Space watermark does not shows.\n");
						}
						try {
							Thread.sleep(3000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
				    	//click arrow from ls
				    	//read the watermark
				    }
				    
				    if(confirmManage.equalsIgnoreCase("Yes")){
				    	mi.SelectManageFaceOfTool(toolName);
				    	try {
				    		Thread.sleep(5000);
				    	} catch (InterruptedException e1) {
				    		// TODO Auto-generated catch block
				    		e1.printStackTrace();
				    	}
				    
				    	String actRes2 = sr.fetchImageShows(WaterMarkManageFace);
				    	System.out.println(actRes2);
					
				    	if(actRes2.equalsIgnoreCase("Found")){
				    		soft.assertTrue(true, "Checking watermark shows in Manage Face.");
				    		System.out.println("The test case passed, "+toolName+" watermark shows in Manage Face.");
				    	}else{
				    		soft.assertTrue(false, "The test case failed, "+toolName+" watermark Does not shows in Manage Face.\n");
				    	}
				    	try {
				    		Thread.sleep(3000);
				    	} catch (InterruptedException e1) {
				    		// TODO Auto-generated catch block
				    		e1.printStackTrace();
				    	}
				    
				    	mi.SelectExitManageFaceOfTool(toolName);
				    	try {
				    		Thread.sleep(3000);
				    	} catch (InterruptedException e1) {
				    		// TODO Auto-generated catch block
				    		e1.printStackTrace();
				    	}
				    }
				    
				    //nt.refreshPage();
//				    try {
//						Thread.sleep(15000);
//					} catch (InterruptedException e1) {
//						// TODO Auto-generated catch block
//						e1.printStackTrace();
//					}
		    	}
				
				
			}
			soft.assertAll();
		}
}
