package education.volute.manage_pgm_testscripts;

import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.ManagePrograms;
import utilities.Constant;
import utilities.SoftAssertCheck;

public class SearchPgm_TC_23 {

	public void testSearchPgm_TC_23(String txt)
	{
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePrograms, txt);
		
		ManagePrograms mp = new ManagePrograms();
		SoftAssertCheck soft = new SoftAssertCheck();
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String searchPgmNameTxt = ExcelUtils.getCellData(i, 1);
        String pgmNameTxt = ExcelUtils.getCellData(i, 2);
        String pgmNameTxt2 = ExcelUtils.getCellData(i, 3);
        String determine = ExcelUtils.getCellData(i, 4);
        String determine2 = ExcelUtils.getCellData(i, 5);
        
        mp.clickOnSearchPgmBtn();
        try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		    
		mp.enterSearchPgm(searchPgmNameTxt);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String pgmShows = mp.fetchProgShows(pgmNameTxt);
		System.out.println(pgmShows);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(pgmShows.equalsIgnoreCase(determine)){
			soft.assertTrue(true, "Checking Program");
			System.out.println("The test case passed, program "+determine+".");
		}else{
			soft.assertTrue(false, "The test case failed, program "+determine+".\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	    String pgmShows1 = mp.fetchProgShows(pgmNameTxt2);
		System.out.println(pgmShows1);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(pgmShows1.equalsIgnoreCase(determine2)){
			soft.assertTrue(true, "Checking Program");
			System.out.println("The test case passed, program "+determine2+".");
		}else{
			soft.assertTrue(false, "The test case failed, program "+determine2+".\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    
	    mp.clickOnSearchPgmBtn();
        try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		}
		
		//sr.Validate(list, list2, list3, list4);
		soft.assertAll();
	}

}
