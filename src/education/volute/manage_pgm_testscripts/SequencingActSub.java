package education.volute.manage_pgm_testscripts;

import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.ManageActivities;
//import education.volute.pages.ManageDesignAct;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import education.volute.pages.SocialHub;
import utilities.Constant;
//import utilities.ReadCsvFiles;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class SequencingActSub {
		MenuItems mi = new MenuItems();
		SoftAssertCheck soft = new SoftAssertCheck();
		NavigatorTool nt = new NavigatorTool();
		SocialHub hb = new SocialHub();
		
public void testSequencingActSub(String Role) {
		
		//ReadCsvFiles csv = new ReadCsvFiles();
		ManageActivities ma = new ManageActivities();
		//ManageDesignAct mda = new ManageDesignAct();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePrograms, "SequencingActSub");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
			String toolDest = ExcelUtils.getCellData(i, 1);
	        String actName = ExcelUtils.getCellData(i, 2);
	        String subActName = ExcelUtils.getCellData(i, 3);
	        String pgmNameTxt = ExcelUtils.getCellData(i, 4);
	        String subSubActName = ExcelUtils.getCellData(i, 5);
	        String toolName = ExcelUtils.getCellData(i, 6);
	        String actType = ExcelUtils.getCellData(i, 7);
	        String prevButtonTxt = ExcelUtils.getCellData(i, 8);
	        String nextButtonTxt = ExcelUtils.getCellData(i, 9);
	        String pubActName = ExcelUtils.getCellData(i, 10);
	        String prevnextName = ExcelUtils.getCellData(i, 11);
	        
	        toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
	        
//	        try {
//				Thread.sleep(15000);
//			} catch (InterruptedException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
	        
	        mi.fetchAppTool("span[class^='sequencing-section']");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	        
			String info = ma.fetchActExitInfoButton();
			ma.clickOnActExitInfoButton2(info);
	        
	        String prevRes = ma.checkPrevButton();
			System.out.println(prevRes);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			if(prevRes.equalsIgnoreCase(prevButtonTxt)){
				soft.assertTrue(true, "Checking button is expected");
				System.out.println("The test case passed, the button behavior is expected.");
			}else{
				soft.assertTrue(false, "The test case failed, the button behavior is not correct.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	        
		    String nextRes = ma.checkNextButton();
			System.out.println(nextRes);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			if(nextRes.equalsIgnoreCase(nextButtonTxt)){
				soft.assertTrue(true, "Checking button is expected");
				System.out.println("The test case passed, the button behavior is expected.");
			}else{
				soft.assertTrue(false, "The test case failed, the button behavior is not correct.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    identifyActSub(pubActName, actName, subActName, subSubActName, toolName, actType);
		    
	        if(prevnextName.equalsIgnoreCase("next")){
	        	ma.clickNextActButton();
	        	try {
	        		Thread.sleep(5000);
	        	} catch (InterruptedException e) {
	        		// TODO Auto-generated catch block
	        		e.printStackTrace();
	        	}
	        }else{ 
	        	ma.clickPrevActButton();
	        	try {
	        		Thread.sleep(5000);
	        	} catch (InterruptedException e) {
	        		// TODO Auto-generated catch block
	        		e.printStackTrace();
	        	}
	        }
		}
		soft.assertAll();
	}

	public void identifyActSub(String pubActName, String actName, String subActName, String subSubActName, String toolName, String actType){
		
		String actSubRes = mi.fetchPubActSub(pubActName);
		System.out.println(actSubRes);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(actSubRes.equalsIgnoreCase("Found")){
			soft.assertTrue(true, "Checking Activity name shows");
			System.out.println("The test case passed, correct Activity user is in.");
		}else{
			soft.assertTrue(false, "The test case failed, either Act/Sub not showing or User is in the wrong Act/Sub.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    
	    String toolRes = mi.fetchTitleTool2(toolName);
		System.out.println(toolRes);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(toolRes.equalsIgnoreCase("Found")){
			soft.assertTrue(true, "Checking Activity contains tools");
			System.out.println("The test case passed, activity has tools.");
		}else{
			soft.assertTrue(false, "The test case failed, activity does not contain the tool.\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    
	    if(actType.equalsIgnoreCase("Act")){
			nt.clickOnJourney();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String actRes = hb.fetchJourneyActSub(actName);
			System.out.println(actRes);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			if(actRes.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking Activity name shows");
				System.out.println("The test case passed, Activity shows highlighted.");
			}else{
				soft.assertTrue(false, "The test case failed, Activity does not show highlighted.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    nt.clickOnJourney();
		}
	    
	    if(actType.equalsIgnoreCase("Sub")){
			nt.clickOnJourney();
			/*hb.clickOnShowSubActs(actName);*/
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String subRes = hb.fetchJourneyActSub(subActName);
			System.out.println(subRes);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			if(subRes.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking Activity name shows");
				System.out.println("The test case passed, Activity shows highlighted.");
			}else{
				soft.assertTrue(false, "The test case failed, Activity does not show highlighted.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    nt.clickOnJourney();
		}
	    
	    if(actType.equalsIgnoreCase("SubSub")){
			nt.clickOnJourney();
//			hb.clickOnShowSubActs(actName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			//hb.clickOnShowSubActs(subActName);
			
			String subSubRes = hb.fetchJourneyActSub(subSubActName);
			System.out.println(subSubRes);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			if(subSubRes.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking Activity name shows");
				System.out.println("The test case passed, Activity shows highlighted.");
			}else{
				soft.assertTrue(false, "The test case failed, Activity does not show highlighted.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    nt.clickOnJourney();
		}
	}
}
