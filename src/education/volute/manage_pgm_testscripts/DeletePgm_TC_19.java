package education.volute.manage_pgm_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.Landing;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.SoftAssertCheck;

public class DeletePgm_TC_19 extends SuperTestScript

	{

		//@Test
		public void testDeletePgm_TC_19(String txt) 
		
		{
					
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePrograms, txt);
			
			//LoginPage lp = new LoginPage();
			NavigatorTool nt = new NavigatorTool();
			SoftAssertCheck soft = new SoftAssertCheck();
			MenuItems mi	= new MenuItems();
			//Landing ld = new Landing();
			ManagePrograms mp = new ManagePrograms();
			
			for(int i=1; i<=ExcelUtils.getRowcount(); i++)
			{
			
			//String uName = ExcelUtils.getCellData(i, 1);
	        //String pwd = ExcelUtils.getCellData(i, 2);
	        String pgmNameTxt = ExcelUtils.getCellData(i, 1);
	        String expRes = ExcelUtils.getCellData(i, 2);
	        String locTxt = ExcelUtils.getCellData(i, 6);
			
	        /*lp.login(uName, pwd);//Time added
			lp.logibutton();
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} 
			nt.clickOnMainMenu();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			nt.clickOnMenuTab();
			mi.clickOnProgram();
			try {
				Thread.sleep(15000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mp.clickOnBackButtonPgmList();
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			/*mp.clickOnselectedprogram(pgmNameTxt);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
	        /*mp.clickOnEditPgmicon(pgmNameTxt);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			mp.clickOnSavePgm();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
	        mp.clickOnselectedprogram(pgmNameTxt);
	    	try {
	    		Thread.sleep(5000);
	    	} catch (InterruptedException e1) {
	    		// TODO Auto-generated catch block
	    		e1.printStackTrace();
	    	}
			
			mp.clickOnDeletePgmicon(pgmNameTxt);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			mp.clickOnDeletePgmYesButton();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String actRes = mp.fetchPgmDeletedMsg();
			System.out.println(actRes);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String pgmShows2 = mp.fetchProgShows(pgmNameTxt);
			System.out.println(pgmShows2);
			if(pgmShows2.equalsIgnoreCase("Not Found")){
				soft.assertTrue(true, "Checking Program content is deleted.");
				System.out.println("The test case passed on verifying the program content is deleted.");
			}else{
				soft.assertTrue(false, "The test case failed on verifying the program content is deleted.\n");
			}
			
			nt.refreshPage();
			
			mi.fetchAppTool(locTxt);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String pgmShows = mp.fetchProgShows(pgmNameTxt);
			System.out.println(pgmShows);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			ExcelUtils.setCellData(pgmShows, i, 5, Constant.File_TestData_ManagePrograms);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if(pgmShows.equalsIgnoreCase("Not Found")){
				soft.assertTrue(true, "Checking Program content is deleted.");
				System.out.println("The test case passed on verifying the program content is deleted.");
			}else{
				soft.assertTrue(false, "The test case failed on verifying the program content is deleted.\n");
			}
			
			ExcelUtils.setCellData(actRes, i, 3, Constant.File_TestData_ManagePrograms );
			
			Boolean status = ValidationLib.verifyMsg(expRes, actRes);
			
			if(status)
			{
				ExcelUtils.setCellData("pass", i, 4,Constant.File_TestData_ManagePrograms);
				soft.assertTrue(true, "Toast");
				System.out.println("The test case passed on verifying Program Toast.");
			}
			else 
		{
				ExcelUtils.setCellData("Fail", i, 4,Constant.File_TestData_ManagePrograms);
				soft.assertTrue(false, "The test case failed on verifying Program Toast.\n");
			}
			
			
			}
			
			soft.assertAll();	
			}
	}
	


