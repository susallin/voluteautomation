package education.volute.manage_pgm_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.ManageActivities;
import education.volute.pages.ManageDesignAct;
import education.volute.pages.Landing;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.SoftAssertCheck;

public class EditActivity_TC_16 extends SuperTestScript
{
	//@Test
	public void testEditActivity_TC_16() {
				
		//LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		//MenuItems mi	= new MenuItems();
		//Landing ld = new Landing();
		SoftAssertCheck soft = new SoftAssertCheck();
		ManagePrograms mp = new ManagePrograms();
		ManageActivities ma = new ManageActivities();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePrograms, "EditActSubs");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		//String uName = ExcelUtils.getCellData(i, 1);
	    //String pwd = ExcelUtils.getCellData(i, 2);
	    String pgmNameTxt = ExcelUtils.getCellData(i, 1);
	    String parentActNameTxt = ExcelUtils.getCellData(i, 2);
	    String subActNameTxt = ExcelUtils.getCellData(i, 3);
		String oldActNameTxt = ExcelUtils.getCellData(i, 4);
        String newActNameTxt = ExcelUtils.getCellData(i, 5);
        String newActDescTxt = ExcelUtils.getCellData(i, 6);
        String newStartDateTxt = ExcelUtils.getCellData(i, 7);
        String newEndDateTxt = ExcelUtils.getCellData(i, 8);
        
        String newActNameRes = ExcelUtils.getCellData(i, 9);
        
        String expRes = ExcelUtils.getCellData(i, 10);
        
		/*lp.login(uName, pwd);
		nt.clickOnMainMenu();
		mi.clickOnNavigation();
		mi.clickOnProgram();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ld.clickOnLandingMP();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		//mi.clickOnManagePgms();
		mp.clickOnselectedprogram(pgmNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		mp.clickOnExpandProgram(pgmNameTxt);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		/*mp.clickOnSelectProgramAct(pgmNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		ma.clickOnArrowSub(parentActNameTxt);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//mp.clickOnArrowToAddAct(pgmNameTxt);
		
		if(!subActNameTxt.isEmpty()){
			ma.clickOnArrowSub(oldActNameTxt);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			ma.clickSelectedSubAct(subActNameTxt);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			// First Act
			ma.clickOnEditAct(subActNameTxt);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		} else{
			ma.clickSelectedSubAct(oldActNameTxt);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			// First Act
			ma.clickOnEditAct(oldActNameTxt);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		nt.clickOnOverviewTab();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		/*nt.endButton();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		/*// This will unpublish it.
		ma.clickOnPublishToggleButton();
		ma.clickOnConfirmPopupYesButton();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ma.clickOnEditUnAssignLSButton(oldActNameTxt);
		ma.clickOnConfirmPopupYesButton();*/
		//ma.clearActName();
		ma.enterActName(" "+ newActNameTxt);
		ma.enterActDesc(" "+ newActDescTxt);
	//	ma.enterActtartDate(startDateTxt);
	//	ma.enterActEndDate(endDateTxt);
		//ma.clickOnAssignLSButton();
		//ma.clickOnArrowToAssignLS(newLsNameTxt);
		//ma.clickOnAssign();
		//ma.clickOnPublishToggleButton();
		ma.clickOnSaveAct();
		try {
			Thread.sleep(7000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ma.clickOnActListBackButton();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String actRes = ma.fetchActShows(newActNameRes);
		System.out.println(actRes);
		if(actRes.equalsIgnoreCase("Found")){
			soft.assertTrue(true, "Checking if content updated");
			System.out.println("The test case passed, the content was updated.");
		}else{
			soft.assertTrue(false, "The test case failed, the content did not update.\n");
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		if(!subActNameTxt.isEmpty()){
			ma.closeArrowSub(oldActNameTxt);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		ma.closeArrowSub(parentActNameTxt);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		mp.clickOnHideProgram(pgmNameTxt);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		/*ma.closeArrowSub(parentActNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/		
		/*mp.clickOnBackButtonPgmList();
		try {
			Thread.sleep(7000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		//mi.clickOnMainMenu();
		//mi.clickOnLogout();
		
		ExcelUtils.setCellData(actRes, i, 11, Constant.File_TestData_ManagePrograms );
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 12,Constant.File_TestData_ManagePrograms);
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 12,Constant.File_TestData_ManagePrograms);
		}
		soft.assertAll();
	}
	}
	
	public void testGoToActivitySub() {
		
		//LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		MenuItems mi = new MenuItems();
		ManageDesignAct mda = new ManageDesignAct();
		//Landing ld = new Landing();
		ManagePrograms mp = new ManagePrograms();
		ManageActivities ma = new ManageActivities();
		SoftAssertCheck soft = new SoftAssertCheck();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePrograms, "EditActSubs2");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		//String uName = ExcelUtils.getCellData(i, 1);
	    //String pwd = ExcelUtils.getCellData(i, 2);
	    String pgmNameTxt = ExcelUtils.getCellData(i, 1);
	    String parentActNameTxt = ExcelUtils.getCellData(i, 2);
		String oldActNameTxt = ExcelUtils.getCellData(i, 3);
        String toolName = ExcelUtils.getCellData(i, 4);
        String expRes = ExcelUtils.getCellData(i, 5);
        //String newStartDateTxt = ExcelUtils.getCellData(i, 6);
        //String newEndDateTxt = ExcelUtils.getCellData(i, 7);
        
        //String newActNameRes = ExcelUtils.getCellData(i, 8);
        
        //String expRes = ExcelUtils.getCellData(i, 9);
        
		/*lp.login(uName, pwd);
		nt.clickOnMainMenu();
		mi.clickOnNavigation();
		mi.clickOnProgram();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ld.clickOnLandingMP();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		//mi.clickOnManagePgms();
		mp.clickOnselectedprogram(pgmNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		mp.clickOnExpandProgram(pgmNameTxt);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		/*mp.clickOnSelectProgramAct(pgmNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		ma.clickOnArrowSub(parentActNameTxt);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//mp.clickOnArrowToAddAct(pgmNameTxt);
		
		ma.clickSelectedSubAct(oldActNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		// First Act
		ma.clickOnEditAct(oldActNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnOverviewTab();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		/*nt.endButton();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		/*// This will unpublish it.
		ma.clickOnPublishToggleButton();
		ma.clickOnConfirmPopupYesButton();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ma.clickOnEditUnAssignLSButton(oldActNameTxt);
		ma.clickOnConfirmPopupYesButton();*/
		//ma.clearActName();
		//ma.enterActName(" "+ newActNameTxt);
		//ma.enterActDesc(" "+ newActDescTxt);
	//	ma.enterActtartDate(startDateTxt);
	//	ma.enterActEndDate(endDateTxt);
		//ma.clickOnAssignLSButton();
		//ma.clickOnArrowToAssignLS(newLsNameTxt);
		//ma.clickOnAssign();
		//ma.clickOnPublishToggleButton();
		ma.clickOnSaveAct();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		nt.clickOnDesignTab();
		//mp.clickOnManageMashupicon(pgmNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*mda.clickTabBy();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		mi.SelectDeleteToolButton(toolName);		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		mi.clickOnDeletePgmYesButton();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		String actRes = mda.fetchDeletedMsg();
		System.out.println(actRes);
		
		// This will determine if Value is still showing or not.
		String actRes2 = mi.fetchTitleToolDesign(toolName);
		System.out.println(actRes2);
		if(actRes2.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking if content still shows");
			System.out.println("The test case passed on verifying the tool was deleted.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the tool was deleted.\n");
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		nt.refreshPage();
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		mp.clickOnselectedprogram(pgmNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		mp.clickOnExpandProgram(pgmNameTxt);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ma.clickOnArrowSub(parentActNameTxt);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//mp.clickOnArrowToAddAct(pgmNameTxt);
		
		ma.clickSelectedSubAct(oldActNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ma.clickOnEditAct(oldActNameTxt);
		try {
			Thread.sleep(7000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String actRes3 = mi.fetchTitleToolDesign(toolName);
		System.out.println(actRes3);
		if(actRes3.equalsIgnoreCase("Not Found")){
			soft.assertTrue(true, "Checking if content still shows");
			System.out.println("The test case passed on verifying the tool was deleted.");
		}else{
			soft.assertTrue(false, "The test case failed on verifying the tool was deleted.\n");
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ma.clickOnActListBackButton();
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ma.closeArrowSub(parentActNameTxt);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		mp.clickOnHideProgram(pgmNameTxt);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ExcelUtils.setCellData(actRes, i, 6, Constant.File_TestData_ManagePrograms );
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 7,Constant.File_TestData_ManagePrograms);
			soft.assertTrue(true, "Toast");
			System.out.println("The test case passed on verifying Design Toast.");
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 7,Constant.File_TestData_ManagePrograms);
			soft.assertTrue(false, "The test case failed on verifying Design Toast.\n");
		}
		
		
		}
		soft.assertAll();
		}
	}


