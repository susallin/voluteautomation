package education.volute.manage_pgm_testscripts;

import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.ManageDesignAct;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import education.volute.pages.Video;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.ToolDestAndCollections;

public class SwitchViewActSub {

	public void testSwitchViewActSub(String Role) {
		
		NavigatorTool nt = new NavigatorTool();
		ReadCsvFiles csv = new ReadCsvFiles();
		MenuItems mi = new MenuItems();
		ManageDesignAct mda = new ManageDesignAct();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePrograms, "SwitchViewUser");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
			String toolDest = ExcelUtils.getCellData(i, 1);
	        String actName = ExcelUtils.getCellData(i, 2);
	        String subActName = ExcelUtils.getCellData(i, 3);
	        String pgmNameTxt = ExcelUtils.getCellData(i, 4);
	        String dsMenuName = ExcelUtils.getCellData(i, 5);
	        String switchOptionName = ExcelUtils.getCellData(i, 6);
	        String uRole = ExcelUtils.getCellData(i, 7);
	        
	        toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
	        
	        try {
				Thread.sleep(15000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	        
	        mda.clickTbSw(dsMenuName);
	        try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        if(switchOptionName.equalsIgnoreCase("user")){
	        	mda.clickUserCopy();
	        	try {
	        		Thread.sleep(2000);
	        	} catch (InterruptedException e) {
	        		// TODO Auto-generated catch block
	        		e.printStackTrace();
	        	}
	        }else{ 
	        	mda.clickmasterButton();
	        	try {
	        		Thread.sleep(2000);
	        	} catch (InterruptedException e) {
	        		// TODO Auto-generated catch block
	        		e.printStackTrace();
	        	}
	        }
	        
	        mda.clickSwitchSv();
	        try {
        		Thread.sleep(2000);
        	} catch (InterruptedException e) {
        		// TODO Auto-generated catch block
        		e.printStackTrace();
        	}
	        
	        nt.clickOnDeletePgmYesButton();
	        try {
        		Thread.sleep(5000);
        	} catch (InterruptedException e) {
        		// TODO Auto-generated catch block
        		e.printStackTrace();
        	}
		}
	}
}
