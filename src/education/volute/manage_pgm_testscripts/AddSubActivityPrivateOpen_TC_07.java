package education.volute.manage_pgm_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.ManageActivities;
import education.volute.pages.Landing;
import education.volute.pages.ManagePrograms;
import education.volute.pages.ManageSubActivities;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.SoftAssertCheck;

public class AddSubActivityPrivateOpen_TC_07 extends SuperTestScript
{
	//@Test
	public void testAddSubActivityPrivateOpen_TC_07(String str) 
	
	{
		//LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		MenuItems mi	= new MenuItems();
		//Landing ld = new Landing();
		ManagePrograms mp = new ManagePrograms();
		ManageActivities ma = new ManageActivities();
		//ManageSubActivities msa = new ManageSubActivities();
		SoftAssertCheck soft = new SoftAssertCheck();
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePrograms, str);
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		//String uName = ExcelUtils.getCellData(i, 1);
        //String pwd = ExcelUtils.getCellData(i, 2);
        String pgmNameTxt = ExcelUtils.getCellData(i, 1);
        String actNameTxt = ExcelUtils.getCellData(i, 2);
        String actDescTxt = ExcelUtils.getCellData(i, 3);
        String subActNameTxt = ExcelUtils.getCellData(i, 4);
        String subActDescTxt = ExcelUtils.getCellData(i, 5);
        String startDateTxt = ExcelUtils.getCellData(i, 6);
        String endDateTxt = ExcelUtils.getCellData(i, 7);
        //String lsNameTxt = ExcelUtils.getCellData(i, 10);
        
        String expRes = ExcelUtils.getCellData(i, 8);
        
		
		/*lp.login(uName, pwd);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnMainMenu();
		mi.clickOnNavigation();
		mi.clickOnProgram();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ld.clickOnLandingMP();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//mi.clickOnManagePgms();*/
       
		mp.clickOnselectedprogram(pgmNameTxt);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnSelectProgramAct(pgmNameTxt);
	
		mi.fetchAppTool("activity-upsert div[title='Switch View']");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		/*ma.clickOnAddActButton();
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
//		nt.clickOnOverviewTab();
//		try {
//			Thread.sleep(2000);
//		} catch (InterruptedException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		ma.clearActName();
		ma.enterActName(actNameTxt);
		ma.enterActDesc(actDescTxt);
		ma.clickOnSaveAct();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ma.clickOnActListBackButton();
		
		mi.fetchAppTool("fab-module[title-search='Search Learning Space'] paper-fab[title='Search Learning Space']");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String actRes1 = mp.fetchExpandIcon(pgmNameTxt);
		System.out.println(actRes1);
		
		if(actRes1.equalsIgnoreCase("Found")){
			mp.clickOnExpandProgram(pgmNameTxt);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		ma.clickSelectedSubAct(actNameTxt);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ma.clickOnAddSubAct(actNameTxt);
		
		mi.fetchAppTool("activity-upsert div[title='Switch View']");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		nt.clickOnOverviewTab();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ma.clearActName();
		ma.enterActName(subActNameTxt);
		ma.enterActDesc(subActDescTxt);
		/*msa.enterSubActName(subActNameTxt);
		msa.enterSubActDesc(subActDescTxt);
		msa.clickOnPublishToggleButton(actNameTxt);
		msa.clickOnSaveSubAct();*/
		ma.clickOnSaveAct();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String actRes = mp.fetchPgmEditsSavedSuccessMsg();
		System.out.println(actRes);
		
		//mi.clickOnMainMenu();
		//mi.clickOnLogout();
		
		ExcelUtils.setCellData(actRes, i, 9, Constant.File_TestData_ManagePrograms);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 10,Constant.File_TestData_ManagePrograms);
			soft.assertTrue(true, "Toast");
			System.out.println("The test case passed on verifying Activity Toast.");
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 10,Constant.File_TestData_ManagePrograms);
			soft.assertTrue(false, "The test case failed on verifying Activity Toast.\n");
		}
		
		
		}
		soft.assertAll();
		
		}
}




