package education.volute.manage_pgm_testscripts;

import java.io.IOException;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Landing;
import education.volute.pages.LoginPage;
import education.volute.pages.ManageLearningSpaces;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;

public class EditLs_TC_20 extends SuperTestScript {

	@Test
	public void testEditLs_TC_20() 
	
	{
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePgms, "TC_20");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String uName = ExcelUtils.getCellData(i, 1);
        String pwd = ExcelUtils.getCellData(i, 2);
        String pgmNameTxt = ExcelUtils.getCellData(i, 3);
        String oldLsNameTxt = ExcelUtils.getCellData(i, 4);
        String newLsNameTxt = ExcelUtils.getCellData(i, 5);
        String newLsDescTxt = ExcelUtils.getCellData(i, 6);
        //String lsNameTxt = ExcelUtils.getCellData(i, 5);
        //String lsDescTxt = ExcelUtils.getCellData(i, 6);
        //String sliDName = ExcelUtils.getCellData(i, 7);
        //String sliFileName = ExcelUtils.getCellData(i, 5);
        //String sliSName = ExcelUtils.getCellData(i, 6);
        //String sliFilName = ExcelUtils.getCellData(i, 9);
        String expRes = ExcelUtils.getCellData(i, 7);
        
        LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		MenuItems mi = new MenuItems();
		Landing ld = new Landing();
		ManagePrograms mp = new ManagePrograms();
		ManageLearningSpaces mls = new ManageLearningSpaces();
		
		lp.login(uName, pwd);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnMainMenu();
		mi.clickOnNavigation();
		mi.clickOnProgram();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ld.clickOnLandingMP();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnselectedprogram(pgmNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnManageMashupicon(pgmNameTxt);
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*mls.selectaLearningSpace(oldLsNameTxt);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mls.clickOnEditButton(oldLsNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mls.clickOnDelCalLs();
		mls.clickOnDelAnnLs();
		mls.clickOnDelDisLs();
		mls.clickOnDelBioLs();
		mls.clickOnDelProLs();
		
		mls.clearLSName();
		mls.enterLSName(newLsNameTxt);
		mls.clearLSDesc();
		mls.enterLsDesc(newLsDescTxt);
		mls.clickOnSaveButton();*/
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String actRes = mls.fetchUpdatedMsg();
		System.out.println(actRes);
		ExcelUtils.setCellData(actRes, i, 8, Constant.File_TestData_ManagePgms );
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 9,Constant.File_TestData_ManagePgms);
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 9,Constant.File_TestData_ManagePgms);
		}
		
		
		}
		
		}
}
