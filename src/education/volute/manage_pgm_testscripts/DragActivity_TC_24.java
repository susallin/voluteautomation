package education.volute.manage_pgm_testscripts;

import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.ManageActivities;
import education.volute.pages.ManagePrograms;
import utilities.Constant;
import utilities.SoftAssertCheck;

public class DragActivity_TC_24 {

	public void testSearchPgm_TC_24(String txt)
	{
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePrograms, txt);
		
		ManagePrograms mp = new ManagePrograms();
		ManageActivities ma = new ManageActivities();
		SoftAssertCheck soft = new SoftAssertCheck();
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String pgmNameTxt = ExcelUtils.getCellData(i, 1);
		String actType = ExcelUtils.getCellData(i, 2);
        String actNameTxt = ExcelUtils.getCellData(i, 3);
        String num = ExcelUtils.getCellData(i, 4);
        String actNameTxt2 = ExcelUtils.getCellData(i, 5);
        String num2 = ExcelUtils.getCellData(i, 6);
        
        mp.clickOnselectedprogram(pgmNameTxt);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		mp.clickOnExpandProgram(pgmNameTxt);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(actType.equalsIgnoreCase("Act")){
			ma.dragAct(actNameTxt, actNameTxt2);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}else{
			
		}
		
		String actRes = ma.checkDragLs(actNameTxt, num);
		System.out.println(actRes);
		if(actRes.equalsIgnoreCase("Found")){
			soft.assertTrue(true, "Checking if content was moved correctly");
			System.out.println("The test case passed, content "+actRes+".");
		}else{
			soft.assertTrue(false, "The test case failed, content "+actRes+".\n");
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String actRes2 = ma.checkDragLs(actNameTxt2, num2);
		System.out.println(actRes2);
		if(actRes2.equalsIgnoreCase("Found")){
			soft.assertTrue(true, "Checking if content was moved correctly");
			System.out.println("The test case passed, content "+actRes2+".");
		}else{
			soft.assertTrue(false, "The test case failed, content "+actRes2+".\n");
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
        mp.clickOnSearchPgmBtn();
        try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		    
		mp.enterSearchPgm(searchPgmNameTxt);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String pgmShows = mp.fetchProgShows(pgmNameTxt);
		System.out.println(pgmShows);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(pgmShows.equalsIgnoreCase(determine)){
			soft.assertTrue(true, "Checking Program");
			System.out.println("The test case passed, program "+determine+".");
		}else{
			soft.assertTrue(false, "The test case failed, program "+determine+".\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	    String pgmShows1 = mp.fetchProgShows(pgmNameTxt2);
		System.out.println(pgmShows1);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(pgmShows1.equalsIgnoreCase(determine2)){
			soft.assertTrue(true, "Checking Program");
			System.out.println("The test case passed, program "+determine2+".");
		}else{
			soft.assertTrue(false, "The test case failed, program "+determine2+".\n");
		}
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    
	    mp.clickOnSearchPgmBtn();
        try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		}
		
		//sr.Validate(list, list2, list3, list4);
		soft.assertAll();
	}
}
