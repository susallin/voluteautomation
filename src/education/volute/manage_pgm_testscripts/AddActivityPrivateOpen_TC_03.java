package education.volute.manage_pgm_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.Landing;
import education.volute.pages.ManageActivities;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.SoftAssertCheck;

public class AddActivityPrivateOpen_TC_03 extends SuperTestScript
{
	//@Test
	public void testAddActivityPrivateOpen_TC_03(String str) 
	
	{
		//LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		MenuItems mi	= new MenuItems();
		//Landing ld = new Landing();
		ManagePrograms mp = new ManagePrograms();
		ManageActivities ma = new ManageActivities();
		SoftAssertCheck soft = new SoftAssertCheck();
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePrograms, str);
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		//String uName = ExcelUtils.getCellData(i, 1);
        //String pwd = ExcelUtils.getCellData(i, 2);
        String pgmNameTxt = ExcelUtils.getCellData(i, 1);
        String actNameTxt = ExcelUtils.getCellData(i, 2);
        String actDescTxt = ExcelUtils.getCellData(i, 3);
        String startDateTxt = ExcelUtils.getCellData(i, 4);
        String endDateTxt = ExcelUtils.getCellData(i, 5);
        //String lsNameTxt = ExcelUtils.getCellData(i, 8);    
        String expRes = ExcelUtils.getCellData(i, 6);
        
		
		/*lp.login(uName, pwd);
		lp.logibutton();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnMainMenu();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnMenuTab();
		mi.clickOnProgram();*/
//		try {
//			Thread.sleep(8000);
//		} catch (InterruptedException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		mp.clickOnselectedprogram(pgmNameTxt);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnSelectProgramAct(pgmNameTxt);
		
		mi.fetchAppTool("activity-upsert div[title='Switch View']");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		/*ma.clickOnAddActButton();
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
//		nt.clickOnOverviewTab();
//		try {
//			Thread.sleep(2000);
//		} catch (InterruptedException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		ma.clearActName();
		ma.enterActName(actNameTxt);
		ma.enterActDesc(actDescTxt);
	//	ma.enterActtartDate(startDateTxt);
	//	ma.enterActEndDate(endDateTxt);
		//ma.clickOnAssignLSButton();
		//ma.clickOnArrowToAssignLS(lsNameTxt);
		//ma.clickOnAssign();
		/*try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		//ma.clickOnPublishToggleButton();
		ma.clickOnSaveAct();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String actRes = mp.fetchPgmEditsSavedSuccessMsg();
		System.out.println(actRes);
		
		//mi.clickOnMainMenu();
		//mi.clickOnLogout();
		
		ExcelUtils.setCellData(actRes, i, 7, Constant.File_TestData_ManagePrograms);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 8,Constant.File_TestData_ManagePrograms);
			soft.assertTrue(true, "Toast");
			System.out.println("The test case passed on verifying Activity Toast.");
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 8,Constant.File_TestData_ManagePrograms);
			soft.assertTrue(false, "The test case failed on verifying Activity Toast.\n");
		}
		
		
		}
		
		soft.assertAll();
		
		}

}


