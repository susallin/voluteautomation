package education.volute.manage_pgm_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.ManageActivities;
import education.volute.pages.ManagePrograms;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.SoftAssertCheck;

public class PublishUnpublishActSub extends SuperTestScript
{

	//@Test
	public void testPublishUnpublishActSub() 	
	{
		NavigatorTool nt = new NavigatorTool();
		ManagePrograms mp = new ManagePrograms();
		ManageActivities ma = new ManageActivities();
		SoftAssertCheck soft = new SoftAssertCheck();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePrograms, "ActStatus");
		
		String actPub = ExcelUtils.getCellData(1, 1);
		String expRes = ExcelUtils.getCellData(1, 2);
		
		nt.clickOnOverviewTab();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
//		nt.clickOnStatusTab();
//		try {
//			Thread.sleep(3000);
//		} catch (InterruptedException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		// published
		ma.clickOnTogglePublish(actPub);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnOverviewTab();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ma.clickOnSaveAct();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String actRes = mp.fetchPgmEditsSavedSuccessMsg();
		System.out.println(actRes);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ma.clickOnActListBackButton();
		try {
			Thread.sleep(7000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		/*mp.clickOnBackButtonPgmList();
		try {
			Thread.sleep(7000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		
		ExcelUtils.setCellData(actRes, 1, 3, Constant.File_TestData_ManagePrograms );
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", 1, 4,Constant.File_TestData_ManagePrograms);
			soft.assertTrue(true, "Toast");
			System.out.println("The test case passed on verifying Published Toast.");
		}
		else 
	{
			ExcelUtils.setCellData("Fail", 1, 4,Constant.File_TestData_ManagePrograms);
			soft.assertTrue(false, "The test case failed on verifying Published Toast.\n");
		}
		soft.assertAll();
	}
}
