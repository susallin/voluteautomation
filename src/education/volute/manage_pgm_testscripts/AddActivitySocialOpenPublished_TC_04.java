package education.volute.manage_pgm_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.ManageActivities;
import education.volute.pages.Landing;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.SoftAssertCheck;

public class AddActivitySocialOpenPublished_TC_04 extends SuperTestScript

{
	//@Test
	public void testAddActivitySocialOpenPublished_TC_04() 
	
	{
		LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		MenuItems mi	= new MenuItems();
		//Landing ld = new Landing();
		ManagePrograms mp = new ManagePrograms();
		ManageActivities ma = new ManageActivities();
		SoftAssertCheck soft = new SoftAssertCheck();
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePrograms, "OpenSocialAct");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		//String uName = ExcelUtils.getCellData(i, 1);
        //String pwd = ExcelUtils.getCellData(i, 2);
        String pgmNameTxt = ExcelUtils.getCellData(i, 1);
        String actNameTxt = ExcelUtils.getCellData(i, 2);
        String actDescTxt = ExcelUtils.getCellData(i, 3);
        String startDateTxt = ExcelUtils.getCellData(i, 4);
        String endDateTxt = ExcelUtils.getCellData(i, 5);
        String actToggle = ExcelUtils.getCellData(i, 6);
        //String lsNameTxt = ExcelUtils.getCellData(i, 8);
        String expRes = ExcelUtils.getCellData(i, 7);
        
		
		/*lp.login(uName, pwd);
		nt.clickOnMainMenu();
		mi.clickOnNavigation();
		mi.clickOnProgram();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ld.clickOnLandingMP();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//mi.clickOnManagePgms();
		mp.clickOnArrowToAddAct(pgmNameTxt);
		ma.clickOnAddActButton();
		ma.enterActName(actNameTxt);
		ma.enterActDesc(actDescTxt);
	//	ma.enterActtartDate(startDateTxt);
	//	ma.enterActEndDate(endDateTxt);
		ma.clickOnAssignLSButton();
		ma.clickOnArrowToAssignLS(lsNameTxt);
		ma.clickOnAssign();*/
		
		mp.clickOnselectedprogram(pgmNameTxt);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnSelectProgramAct(pgmNameTxt);
		
		mi.fetchAppTool("activity-upsert div[title='Switch View']");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		/*ma.clickOnAddActButton();
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
//		nt.clickOnOverviewTab();
//		try {
//			Thread.sleep(2000);
//		} catch (InterruptedException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		ma.clearActName();
		ma.enterActName(actNameTxt);
		ma.enterActDesc(actDescTxt);
		
		ma.clickAssignUserButon();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// social
		ma.clickOnCheckboxButton(actToggle);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ma.clickOnConfirmPopupYesButton();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ma.clickConfirmAssignUserButton();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ma.clickOnSaveAct();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String actRes = mp.fetchPgmEditsSavedSuccessMsg();
		System.out.println(actRes);
		
		//mi.clickOnMainMenu();
		//mi.clickOnLogout();
		
		ExcelUtils.setCellData(actRes, i, 8, Constant.File_TestData_ManagePrograms);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 9,Constant.File_TestData_ManagePrograms);
			soft.assertTrue(true, "Toast");
			System.out.println("The test case passed on verifying Activity Toast.");
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 9,Constant.File_TestData_ManagePrograms);
			soft.assertTrue(true, "Toast");
			System.out.println("The test case passed on verifying Activity Toast.");
		}
		
		
		}
		soft.assertAll();
		
		}

}





