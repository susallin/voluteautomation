package education.volute.manage_pgm_testscripts;

import java.util.Iterator;

import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Announcements;
import education.volute.pages.MenuItems;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class ActSubInfo {

public void testActSubInfo() throws Exception 
	
	{
		MenuItems mi = new MenuItems();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		SoftAssertCheck soft = new SoftAssertCheck();

		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePrograms, "ActInfo");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String toolDest = ExcelUtils.getCellData(i, 1);
	    String actName = ExcelUtils.getCellData(i, 2);
	    String subActName = ExcelUtils.getCellData(i, 3);
        String pgmNameTxt = ExcelUtils.getCellData(i, 4);
        
        String expRes = ExcelUtils.getCellData(i, 7);
		
		/*lp.login(uName, pwd);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnMainMenu();
		mi.clickOnNavigation();*/
        toolD.Destination(toolDest, actName, subActName, pgmNameTxt);
		/*mi.clickOnProgram();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ld.clickOnLandingMP();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnselectedprogram(pgmNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnManageMashupicon(pgmNameTxt);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*mls.clickOnAddLS();
		mls.selectaTool(lsToolName);
		mls.enterLSName(lsNameTxt);
		mls.enterLsDesc(lsDescTxt);
		mls.clickOnSaveButton();
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mls.clickOnConfigButton();*/
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		an.clickOnManageAn();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Iterator<String[]> FileName = csv.ReadList(AnFilNames);
		
		if(FileName.hasNext())
			FileName.next();
		
		while(FileName.hasNext()){
			int n = 0;
			String[] Name = FileName.next();
			
			String File = Name[n];
			String anName = Name[n+1];
			String anDesc = Name[n + 2];
			
			mi.SelectAdd(toolName);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			an.enterAnTitleName(anName);
			an.enterAnDescription(anDesc);
			
			an.clickOnAnSelectFile();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+File);
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			an.clickOnAnSave();
			
		}
		//sli.enterSlideDisplayName(sliDName);
		/*an.enterAnTitleName(anTitle);
		an.enterAnDescription(anDesc);//sliFilName);
		an.clickOnAnSelectFile();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Runtime.getRuntime().exec("./user_data/Autoit/FileUploadJPG.exe");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		an.clickOnAnSave();*/
		
		String actRes = an.fetchAddedMsgAn();
		System.out.println(actRes);
		ExcelUtils.setCellData(actRes, i, 8, Constant.File_TestData_Announcements );
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 9,Constant.File_TestData_Announcements);
			soft.assertTrue(true, "Toast");
			System.out.println("The test case passed on verifying Announcement Toast.");
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 9,Constant.File_TestData_Announcements);
			soft.assertTrue(false, "The test case failed on verifying Announcement Toast.\n");
		}
		
		
		}
		soft.assertAll();
		
		}
}
