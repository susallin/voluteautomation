package education.volute.manage_pgm_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.Landing;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.SoftAssertCheck;

public class EditPgm_TC_18 extends SuperTestScript

{

	//@Test
	public void testEditPgm_TC_18() 
	
	{
		LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		MenuItems mi	= new MenuItems();
		//Landing ld = new Landing();
		ManagePrograms mp = new ManagePrograms();
		SoftAssertCheck soft = new SoftAssertCheck();
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePrograms, "editPgm");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String uName = ExcelUtils.getCellData(i, 1);
        String pwd = ExcelUtils.getCellData(i, 2);
        String oldPgmNameTxt = ExcelUtils.getCellData(i, 3);
        String newPgmNameTxt = ExcelUtils.getCellData(i, 4);
        String newStartDateTxt = ExcelUtils.getCellData(i, 5);
        String newEndDateTxt = ExcelUtils.getCellData(i, 6);
        String newPgmDescTxt = ExcelUtils.getCellData(i, 7);
        String expRes = ExcelUtils.getCellData(i, 8);
        
        
        /*lp.login(uName, pwd);//Time added
		lp.logibutton();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnMainMenu();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnMenuTab();
		mi.clickOnProgram();
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnBackButtonPgmList();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//mi.clickOnManagePgms();
		/*mp.clickOnselectedprogram(oldPgmNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
        try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
        mp.clickOnselectedprogram(oldPgmNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		mp.clickOnEditPgmicon(oldPgmNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clearPgmName();
		mp.enterPgmName(newPgmNameTxt);
		mp.clearPgmDesc();
		mp.enterPgmDesc(newPgmDescTxt);
		mp.clickOnSavePgm();
		try {
			Thread.sleep(7000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String actRes = mp.fetchProgShows("Mr.Script(Do not delete) Edited");//mp.fetchPgmEditsSavedSuccessMsg();//Program updated successfully
		System.out.println(actRes);
		ExcelUtils.setCellData(actRes, i, 9, Constant.File_TestData_ManagePrograms );
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 10,Constant.File_TestData_ManagePrograms);
			soft.assertTrue(true, "Toast");
			System.out.println("The test case passed on verifying Program Toast.");
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 10,Constant.File_TestData_ManagePrograms);
			soft.assertTrue(false, "The test case failed on verifying Program Toast.\n");
		}
		
		
		}
		
		soft.assertAll();
		}

}
