package education.volute.manage_pgm_testscripts;

import java.util.Iterator;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.Landing;
import education.volute.pages.ManageDesignAct;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.SoftAssertCheck;

public class AddToolDesignAct_TC_02 extends SuperTestScript
{
	//@Test
	public void testAddToolDesignAct_TC_02(String str) throws Exception 
	
	{
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePrograms, str);
		
		//LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		MenuItems mi = new MenuItems();
		//Landing ld = new Landing();
		//ManagePrograms mp = new ManagePrograms();
		ManageDesignAct mda = new ManageDesignAct();
		ReadCsvFiles csv = new ReadCsvFiles();
		SoftAssertCheck soft = new SoftAssertCheck();
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String dsMenuName = ExcelUtils.getCellData(i, 1);
		String dsToolNames = ExcelUtils.getCellData(i, 2);
		String titleName = ExcelUtils.getCellData(i, 3);
		//String uName = ExcelUtils.getCellData(i, 1);
        //String pwd = ExcelUtils.getCellData(i, 2);
        //String pgmNameTxt = ExcelUtils.getCellData(i, 3);
        //String actName = ExcelUtils.getCellData(i, 4);
        //String lsDescTxt = ExcelUtils.getCellData(i, 5);
        String expRes = ExcelUtils.getCellData(i, 4);
		
		/*lp.login(uName, pwd);
		lp.logibutton();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
		nt.clickOnMainMenu();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnMenuTab();
		mi.clickOnProgram();
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnselectedprogram(pgmNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mp.clickOnSelectProgramAct(pgmNameTxt);*/
        mi.fetchAppTool("activity-upsert div[title='Switch View']");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		nt.clickOnDesignTab();
		//mp.clickOnManageMashupicon(pgmNameTxt);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*mda.clickMoreMenuButton();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		mda.clickTbSw(dsMenuName);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Iterator<String[]> FileName = csv.ReadList(dsToolNames);
		
		if(FileName.hasNext())
			FileName.next();
		
		while(FileName.hasNext()){
			int n = 0;
			String[] Name = FileName.next();
			
			String toolName = Name[n];
			
			/*mi.fetchAppTool("div[title='Announcements']");*/
//			try {
//				Thread.sleep(2000);
//			} catch (InterruptedException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
			
			mda.selectaTool(toolName);
			mi.fetchAppTool("div[title='Announcements']");
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String actRes = mda.fetchSuccessMsg();
			System.out.println(actRes);
			
			if(actRes.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking toast");
				System.out.println("The test case passed toast shows.");
			}else{
				soft.assertTrue(false, "The test case failed toast does not show.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		/*try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mda.selectaTool("Tasks");*/
//		try {
//			Thread.sleep(2000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		/*mls.clickOnAddLS();
		mls.enterLSName(lsNameTxt);
		mls.enterLsDesc(lsDescTxt);
		mls.selectAllTools();
		mls.clickOnSaveButton();*/
	
		
//		ExcelUtils.setCellData(actRes, i, 5, Constant.File_TestData_ManagePrograms );
//		try {
//			Thread.sleep(5000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		mi.SelectFullScreenToolButton(titleName);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		mi.SelectFullScreenToolButton(titleName);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
//		
//		if(status)
//		{
//			ExcelUtils.setCellData("pass", i, 6,Constant.File_TestData_ManagePrograms);
//			soft.assertTrue(true, "Toast");
//			System.out.println("The test case passed on verifying Design Toast.");
//		}
//		else 
//	{
//			ExcelUtils.setCellData("Fail", i, 6,Constant.File_TestData_ManagePrograms);
//			soft.assertTrue(false, "The test case failed on verifying Design Toast.\n");
//		}
		
		
		}
		soft.assertAll();
		
		}

}


		
		




		
		
		
		


