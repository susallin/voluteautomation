package education.volute.manage_pgm_testscripts;

import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.ManageDesignAct;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class CheckToolDesignAct {

	//@Test
		public void testCheckToolDesignAct(String str, String uRole)
		
		{
					
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePrograms, str);
			
			ToolDestAndCollections toolD = new ToolDestAndCollections();
			NavigatorTool nt = new NavigatorTool();
			ManageDesignAct mda = new ManageDesignAct();
			SoftAssertCheck soft = new SoftAssertCheck();
			
			for(int i=1; i<=ExcelUtils.getRowcount(); i++)
			{
			
			String actName = ExcelUtils.getCellData(i, 1);
			String subActName = ExcelUtils.getCellData(i, 2);
			String dsMenuName = ExcelUtils.getCellData(i, 3);
			String dsToolNames = ExcelUtils.getCellData(i, 4);
			String expRes = ExcelUtils.getCellData(i, 5);
			
			
	        toolD.ActType3(actName,subActName,uRole);
	        
			nt.clickOnDesignTab();
			//mp.clickOnManageMashupicon(pgmNameTxt);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			mda.clickTbSw(dsMenuName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			// This will determine if Value is still showing or not.
			String actRes = mda.fetchTool(dsToolNames);
			System.out.println(actRes);
			if(actRes.equalsIgnoreCase(expRes)){
				soft.assertTrue(true, "Checking if content still shows");
				System.out.println("The test case passed, content "+actRes+".");
			}else{
				soft.assertTrue(false, "The test case failed, content "+actRes+".\n");
			}
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			nt.clickOnHomeIcon();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
			soft.assertAll();
			
	}
}
