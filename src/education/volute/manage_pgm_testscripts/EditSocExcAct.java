package education.volute.manage_pgm_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Landing;
import education.volute.pages.LoginPage;
import education.volute.pages.ManageActivities;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;

public class EditSocExcAct extends SuperTestScript {

	@Test
	public void testEditSocExcAct() {
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePgms, "EditSocExcAct"+ "");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
			String uName = ExcelUtils.getCellData(i, 1);
		    String pwd = ExcelUtils.getCellData(i, 2);
		    String pgmNameTxt = ExcelUtils.getCellData(i, 3);
		    String actTypeTxt = ExcelUtils.getCellData(i, 4);
			String oldActNameTxt = ExcelUtils.getCellData(i, 5);
			String newActNameTxt = ExcelUtils.getCellData(i, 6);
			String newActDescTxt = ExcelUtils.getCellData(i, 7);
			String newStartDateTxt = ExcelUtils.getCellData(i, 8);
			String newEndDateTxt = ExcelUtils.getCellData(i, 9);
    
			String newLsNameTxt = ExcelUtils.getCellData(i, 10);
    
			String expRes = ExcelUtils.getCellData(i, 11);
    
			//ManageActivities ma = new ManageActivities();
    
			LoginPage lp = new LoginPage();
			NavigatorTool nt = new NavigatorTool();
			MenuItems mi	= new MenuItems();
			Landing ld = new Landing();
			ManagePrograms mp = new ManagePrograms();
			ManageActivities ma = new ManageActivities();
			
			lp.login(uName, pwd);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			nt.clickOnMainMenu();
			mi.clickOnNavigation();
			mi.clickOnProgram();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			ld.clickOnLandingMP();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			//mi.clickOnManagePgms();
			mp.clickOnArrowToAddAct(pgmNameTxt);
			
			ma.clickOnselectedActivity(oldActNameTxt);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	
			// First Act
			ma.clickOnEditActicon(oldActNameTxt);
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// This will unpublish it.
			ma.clickOnEditPublishToggleButton(oldActNameTxt);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			ma.clickOnConfirmPopupYesButton();
	
			ma.clickOnEditUnAssignLSButton(oldActNameTxt);
			ma.clickOnConfirmPopupYesButton();
			
			//ma.clearActName();
			ma.clickOnEditExclusiveToggleButton(oldActNameTxt);
			ma.clickOnEditSocialToggleButton(oldActNameTxt);
			ma.enterEditActName(oldActNameTxt, " "+ newActNameTxt);
			ma.enterEditActDesc(oldActNameTxt, " "+ newActDescTxt);
			//	ma.enterActtartDate(startDateTxt);
			//	ma.enterActEndDate(endDateTxt);
			ma.clickOnEditAssignLSButton(oldActNameTxt);
			ma.clickOnArrowToAssignLS(newLsNameTxt);
			ma.clickOnAssign();
			
			ma.clickOnEditSocialToggleButton(oldActNameTxt);
			
			ma.clickOnEditExclusiveToggleButton(oldActNameTxt);
			ma.selectAllindividuals();
			ma.clickOnAssignUsersDoneButton();
			
			ma.clickOnEditPublishToggleButton(oldActNameTxt);
			ma.clickOnEditSaveAct(oldActNameTxt);
	
			String actRes4 = ma.fetchActEditsSavedSuccessMsg();
			System.out.println(actRes4);
	
			mi.clickOnMainMenu();
			mi.clickOnLogout();
	
			ExcelUtils.setCellData(actRes4, i, 12, Constant.File_TestData_ManagePgms );
	
			Boolean status4 = ValidationLib.verifyMsg(expRes, actRes4);
	
			if(status4)
			{
				ExcelUtils.setCellData("pass", i, 13,Constant.File_TestData_ManagePgms);
			}
			else 
			{
				ExcelUtils.setCellData("Fail", i, 13,Constant.File_TestData_ManagePgms);
			}
		}
	}
}