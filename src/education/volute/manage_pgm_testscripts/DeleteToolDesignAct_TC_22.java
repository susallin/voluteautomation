package education.volute.manage_pgm_testscripts;

import java.util.Iterator;

import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.ManageDesignAct;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.SoftAssertCheck;

public class DeleteToolDesignAct_TC_22 {

		//@Test
		public void testDeleteToolDesignAct_TC_22() //throws Exception 
		
		{
					
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePrograms, "DeleteTool");
			
			//LoginPage lp = new LoginPage();
			NavigatorTool nt = new NavigatorTool();
			MenuItems mi = new MenuItems();
			//Landing ld = new Landing();
			//ManagePrograms mp = new ManagePrograms();
			ManageDesignAct mda = new ManageDesignAct();
			//ReadCsvFiles csv = new ReadCsvFiles();
			SoftAssertCheck soft = new SoftAssertCheck();
			
			for(int i=1; i<=ExcelUtils.getRowcount(); i++)
			{
			
			//String dsToolNames = ExcelUtils.getCellData(i, 1);
			//String uName = ExcelUtils.getCellData(i, 1);
	        //String pwd = ExcelUtils.getCellData(i, 2);
	        //String pgmNameTxt = ExcelUtils.getCellData(i, 3);
	        //String actName = ExcelUtils.getCellData(i, 4);
	        String toolName = ExcelUtils.getCellData(i, 1);
	        String expRes = ExcelUtils.getCellData(i, 2);
			
			/*lp.login(uName, pwd);
			lp.logibutton();
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} 
			nt.clickOnMainMenu();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			nt.clickOnMenuTab();
			mi.clickOnProgram();
			try {
				Thread.sleep(15000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mp.clickOnselectedprogram(pgmNameTxt);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mp.clickOnSelectProgramAct(pgmNameTxt);*/
	        try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			nt.clickOnDesignTab();
			//mp.clickOnManageMashupicon(pgmNameTxt);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			mda.clickTabBy();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			mi.SelectDeleteToolButton(toolName);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			nt.clickOnDeletePgmYesButton();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/*Iterator<String[]> FileName = csv.ReadList(dsToolNames);
			
			if(FileName.hasNext())
				FileName.next();
			
			while(FileName.hasNext()){
				int n = 0;
				String[] Name = FileName.next();
				
				String toolName = Name[n];
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				mda.selectaTool(toolName);
				
			}*/
			
			/*try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mda.selectaTool("Tasks");*/
			/*mls.clickOnAddLS();
			mls.enterLSName(lsNameTxt);
			mls.enterLsDesc(lsDescTxt);
			mls.selectAllTools();
			mls.clickOnSaveButton();*/
			
			String actRes = mda.fetchDeletedMsg();
			System.out.println(actRes);
			ExcelUtils.setCellData(actRes, i, 3, Constant.File_TestData_ManagePrograms );
			
			Boolean status = ValidationLib.verifyMsg(expRes, actRes);
			
			if(status)
			{
				ExcelUtils.setCellData("pass", i, 4,Constant.File_TestData_ManagePrograms);
				soft.assertTrue(true, "Toast");
				System.out.println("The test case passed on verifying Design Toast.");
			}
			else 
		{
				ExcelUtils.setCellData("Fail", i, 4,Constant.File_TestData_ManagePrograms);
				soft.assertTrue(false, "The test case failed on verifying Design Toast.\n");
			}
			
			
			}
			soft.assertAll();
			
			}
}
