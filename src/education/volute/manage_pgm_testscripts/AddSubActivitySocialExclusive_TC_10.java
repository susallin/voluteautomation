package education.volute.manage_pgm_testscripts;

import java.util.Iterator;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.ManageActivities;
import education.volute.pages.Landing;
import education.volute.pages.ManagePrograms;
import education.volute.pages.ManageSubActivities;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.SoftAssertCheck;

public class AddSubActivitySocialExclusive_TC_10 extends SuperTestScript
{
	//@Test
	public void testAddSubActivitySocialExclusive_TC_10() throws Exception 

	{
		//LoginPage lp = new LoginPage();
		//NavigatorTool nt = new NavigatorTool();
		MenuItems mi	= new MenuItems();
		//Landing ld = new Landing();
		ManagePrograms mp = new ManagePrograms();
		ManageActivities ma = new ManageActivities();
		//ManageSubActivities msa = new ManageSubActivities();
		ReadCsvFiles csv = new ReadCsvFiles();
		SoftAssertCheck soft = new SoftAssertCheck();
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePrograms, "SocialExSubActivity");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		//String uName = ExcelUtils.getCellData(i, 1);
	    //String pwd = ExcelUtils.getCellData(i, 2);
	    String pgmNameTxt = ExcelUtils.getCellData(i, 1);
	    String actNameTxt = ExcelUtils.getCellData(i, 2);
	    String subActNameTxt = ExcelUtils.getCellData(i, 3);
	    String subActDescTxt = ExcelUtils.getCellData(i, 4);
	    String startDateTxt = ExcelUtils.getCellData(i,5);
	    String endDateTxt = ExcelUtils.getCellData(i, 6);
	    String actToggleSoc = ExcelUtils.getCellData(i, 7);
        String actToggleEx = ExcelUtils.getCellData(i, 8);
        String indNames = ExcelUtils.getCellData(i, 9);
	    //String lsNameTxt = ExcelUtils.getCellData(i, 7);
	    
	    String expRes = ExcelUtils.getCellData(i, 10);
	    boolean flag = true;
		/*lp.login(uName, pwd);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnMainMenu();
		mi.clickOnNavigation();
		mi.clickOnProgram();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ld.clickOnLandingMP();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//mi.clickOnManagePgms();*/
		mp.clickOnselectedprogram(pgmNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		/*mp.clickOnSelectProgramAct(pgmNameTxt);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		String actRes1 = mp.fetchExpandIcon2(pgmNameTxt);
		System.out.println(actRes1);
		
		if(actRes1.equalsIgnoreCase("Not Found")){
			mp.clickOnExpandProgram(pgmNameTxt);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		ma.clickSelectedSubAct(actNameTxt);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ma.clickOnAddSubAct(actNameTxt);
		
		mi.fetchAppTool("activity-upsert div[title='Switch View']");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ma.clearActName();
		ma.enterActName(subActNameTxt);
		ma.enterActDesc(subActDescTxt);
		
		ma.clickAssignUserButon();
		
		while(flag){
			String result = mi.fetchIndivTab();
				
			if(result.equalsIgnoreCase("Found")){
						
				//We start adding indivs/groups
				// social
				ma.clickOnCheckboxButton(actToggleSoc);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				ma.clickOnConfirmPopupYesButton();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				// Exclusive
				ma.clickOnCheckboxButton(actToggleEx);
				
				ma.clickOnConfirmPopupNoButton();
							
				Iterator<String[]> FileName = csv.ReadList(indNames);
							
				if(FileName.hasNext())
					FileName.next();
							
				while(FileName.hasNext()){
					int n = 0;
					String[] Name = FileName.next();
								
					String Type = Name[n];
					String indgrpNameF = Name[n+1];
								
					if(Type.equalsIgnoreCase("Individual")) {
									
					mi.clickOnIndividualTab();
						/*try {
							Thread.sleep(4000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}*/
									
						mi.selectIndividual(indgrpNameF);
						/*try {
							Thread.sleep(2000);
						} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
							e1.printStackTrace();
						}*/
									
					} else{
								
						mi.clickOnGroupTab();
						/*try {
							Thread.sleep(4000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}*/
									
						mi.selectGroup(indgrpNameF);
						/*try {
							Thread.sleep(2000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}*/
						}
					}
					flag = false;
				}
				else{
					flag = true;
				}
		}		
		
		mi.clickOnSelectedTab();
		
		mi.clickOnDoneButton();			
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
				
			ma.clickOnSaveAct();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
		//mp.clickOnArrowToAddAct(pgmNameTxt);
		//ma.clickOnselectedActivity(actNameTxt);
		//ma.clickOnAddSubActicon(actNameTxt);
		
		
		/*msa.enterSubActName(subActNameTxt);
		msa.enterSubActDesc(subActDescTxt);
		msa.clickOnSocialToggleButton();
		msa.clickOnExclusiveToggleButton();
		
		msa.selectAllindividuals();
		msa.clickOnAssignUsersDoneButton();
		
		msa.clickOnPublishToggleButton(actNameTxt);
		msa.clickOnSaveSubAct();*/
		String actRes = mp.fetchPgmEditsSavedSuccessMsg();
		System.out.println(actRes);
		
		//mi.clickOnMainMenu();
		//mi.clickOnLogout();
		
		ExcelUtils.setCellData(actRes, i, 11, Constant.File_TestData_ManagePrograms );
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 12,Constant.File_TestData_ManagePrograms);
			soft.assertTrue(true, "Toast");
			System.out.println("The test case passed on verifying Activity Toast.");
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 12,Constant.File_TestData_ManagePrograms);
			soft.assertTrue(false, "The test case failed on verifying Activity Toast.\n");
		}
		
		
		}
		soft.assertAll();
		
		}
	}





