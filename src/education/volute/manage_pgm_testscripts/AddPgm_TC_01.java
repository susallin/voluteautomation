
//* Script works for GC and FF


package education.volute.manage_pgm_testscripts;

import java.io.IOException;
import java.util.ArrayList;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.ManageActivities;
import education.volute.pages.Landing;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.SearchElements;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class AddPgm_TC_01 extends SuperTestScript
{
		//@Test
		public void testAddPgmTC_01(String txt) throws Exception
		{
					
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePrograms, txt);
			
			LoginPage lp = new LoginPage();
			NavigatorTool nt = new NavigatorTool();
			MenuItems mi	= new MenuItems();
			//Landing ld = new Landing();
			ManagePrograms mp = new ManagePrograms();
			SoftAssertCheck soft = new SoftAssertCheck();
			ManageActivities ma = new ManageActivities();
			
			for(int i=1; i<=ExcelUtils.getRowcount(); i++)
			{
			
			String uName = ExcelUtils.getCellData(i, 1);
	        String pwd = ExcelUtils.getCellData(i, 2);
	        String pgmNameTxt = ExcelUtils.getCellData(i, 3);
	        String startDateTxt = ExcelUtils.getCellData(i, 4);
	        String EndDateTxt = ExcelUtils.getCellData(i, 5);
	        String pgmDescTxt = ExcelUtils.getCellData(i, 6);
	        String colorTheme = ExcelUtils.getCellData(i, 7);
	        String logoFile = ExcelUtils.getCellData(i, 8);
	        String expRes = ExcelUtils.getCellData(i, 9);
	        
			
			/*lp.login(uName, pwd);//Time added
			lp.logibutton();
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} 
			/*nt.clickCloseButtonTutorial();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			nt.clickLeavingTutorial();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			nt.clickOnMainMenu();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			nt.clickOnMenuTab();
			mi.clickOnProgram();
			try {
				Thread.sleep(15000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			/*mp.clickOnBackButtonPgmList();
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			/*nt.clickendTour();
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			mp.clickOnAddPgmBtn();
			
			mi.fetchAppTool("activity-upsert div[title='Switch View']");
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			nt.clickOnOverviewTab();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			ma.clearActName();
			ma.enterActName(pgmNameTxt);
			ma.enterActDesc(pgmDescTxt);
			
//			mp.enterPgmName(pgmNameTxt);
//			try {
//				Thread.sleep(2000);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			
			ma.clickActStartDateBox();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			ma.selectActStartDateNum(startDateTxt);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			ma.clickActEndDateBox();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			ma.selectActEndDateNum(EndDateTxt);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
//			mp.enterPgmstartDate(startDateTxt);
//			mp.enterPgmEndDate(EndDateTxt);
			//mp.enterPgmDesc(pgmDescTxt);
			
			// Does not work on GeckoDriver
			mp.clickPgmThemeSelect();
			mp.selectPgmColor(colorTheme);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			// This is not working on GeckoDriver for the time.
			
			mp.clickPgmSelectFile();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Runtime.getRuntime().exec("./user_data/Autoit/FileUploadJPG.exe"+" "+"./user_data/"+logoFile);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			ma.clickOnSaveAct();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			String actRes = mp.fetchPgmEditsSavedSuccessMsg();
			System.out.println(actRes);
			ExcelUtils.setCellData(actRes, i, 10, Constant.File_TestData_ManagePrograms);			
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//mp.clickOnselectedprogram(pgmNameTxt);
			
			Boolean status = ValidationLib.verifyMsg(expRes, actRes);
			
			if(status)
			{
				ExcelUtils.setCellData("pass", i, 11,Constant.File_TestData_ManagePrograms);
				soft.assertTrue(true, "Toast");
				System.out.println("The test case passed on verifying Program Toast.");
				//break;
			}
			else 
		{
				ExcelUtils.setCellData("Fail", i, 11,Constant.File_TestData_ManagePrograms);
				soft.assertTrue(false, "The test case failed on verifying Program Toast.\n");
			}
			
			
			}
			
			//sr.Validate(list, list2, list3, list4);
			soft.assertAll();
		}

}
