package education.volute.manage_users_testscripts;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.Landing;
import education.volute.pages.ManageUsers;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.SoftAssertCheck;

	public class AddUsers_TC_01 extends SuperTestScript
	{

			//@Test
			public void testAddUserTC_01() 
			{
				LoginPage lp = new LoginPage();
				NavigatorTool nt = new NavigatorTool();
				MenuItems mi	= new MenuItems();
				//Landing ld = new Landing();
				ManageUsers mu = new ManageUsers();
				SoftAssertCheck soft = new SoftAssertCheck();
						
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManageUsers, "Add1");
				
				for(int i=1; i<=ExcelUtils.getRowcount(); i++)
				{
				
				String uName = ExcelUtils.getCellData(i, 1);
		        String pwd = ExcelUtils.getCellData(i, 2);
		        String fNameTxt = ExcelUtils.getCellData(i, 3);
		        String lNameTxt = ExcelUtils.getCellData(i, 4);
		        String eMailTxt = ExcelUtils.getCellData(i, 5);
		        String pgmNameTxt = ExcelUtils.getCellData(i, 6);
		        //String domainTxt = ExcelUtils.getCellData(i, 7);//This needs to be added on excel
		        String roleTxt = ExcelUtils.getCellData(i, 7);
		        String programRoleTxt = ExcelUtils.getCellData(i, 8);
		        String cohortTxt = ExcelUtils.getCellData(i, 9);
		        String expRes = ExcelUtils.getCellData(i, 10);
			
			/*lp.login(uName, pwd);
			lp.logibutton();
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			
		    nt.clickOnAdmin();
			mi.clickOnUsers();
			try {
				Thread.sleep(15000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			//mi.goToManageUsers();
			mu.clikOnSearchUserButton();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			//mi.goToManageUsers();
			mu.enterSearchUser("raulj9945");
			mu.pressEnter();
			
			mu.clickOnAddNewUserButton();
			try {
				Thread.sleep(8000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mu.enterFirstName(fNameTxt);
			mu.enterLastNAme(lNameTxt);
			mu.enterEmailId(eMailTxt);
			
			//mu.clickOnSubDomain();
			//mu.selectASubDomain(domainTxt);
			mu.clickOnRoleDropDown();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mu.selectARole(roleTxt);
			mu.enterCohort(cohortTxt);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			// This changes
			//mu.selectPgmCheckBox(pgmNameTxt);
			
			mu.clikOnAddProgramRoleButton();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			mu.clikOnProgramListButton();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			mu.selectProgram(pgmNameTxt);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			mu.clikOnProgramRoleListButton();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			mu.selectRole(programRoleTxt);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			/*mu.clikOnSaveUserButton();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String actRes = mu.fetchSuccessMsg();
			System.out.println(actRes);		
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			//mi.clickOnMainMenu();
			//mi.clickOnLogout();

			 ExcelUtils.setCellData(actRes, i, 11, Constant.File_TestData_ManageUsers);
				
				Boolean status = ValidationLib.verifyMsg(expRes, actRes);
				
				if(status)
				{
					ExcelUtils.setCellData("pass", i, 12, Constant.File_TestData_ManageUsers);
					soft.assertTrue(true, "Toast");
					System.out.println("The test case passed on verifying Toast.");
				}
				else 
			{
					ExcelUtils.setCellData("Fail", i, 12, Constant.File_TestData_ManageUsers);
					soft.assertTrue(false, "The test case failed on verifying Toast.\n");
				}*/
				
				
				}
				//soft.assertAll();
				
				}

	}

