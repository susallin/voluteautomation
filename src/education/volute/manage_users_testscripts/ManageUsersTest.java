package education.volute.manage_users_testscripts;


public class ManageUsersTest {

	public void AddUsers(){
		AddUsers_TC_01 mu = new AddUsers_TC_01();
		mu.testAddUserTC_01();
	}
	
	public void EditUser(){
		EditUser_TC_02 mu = new EditUser_TC_02();
		mu.testEditUserTC_02();
	}
	
	public void EditUser2(String user){
		EditUser_TC_02 mu = new EditUser_TC_02();
		mu.testEditUser(user);
	}
	
	public void DeleteUser(){
		DeleteUser_TC_03 mu = new DeleteUser_TC_03();
		mu.testDeleteUserTC_03();
	}
	
	public void NameShows(String textShows) //throws Exception
	{
		ContentUser mu = new ContentUser();
		mu.verifyInfo(textShows);
	}
	
}
