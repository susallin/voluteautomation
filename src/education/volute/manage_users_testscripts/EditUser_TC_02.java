package education.volute.manage_users_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.Landing;
import education.volute.pages.ManageUsers;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;
import utilities.SoftAssertCheck;

public class EditUser_TC_02 extends SuperTestScript
{

	//@Test
	public void testEditUserTC_02() 
	{
		LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		MenuItems mi	= new MenuItems();
		//Landing ld = new Landing();
		ManageUsers mu = new ManageUsers();
		SoftAssertCheck soft = new SoftAssertCheck();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManageUsers, "EditUser");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String uName = ExcelUtils.getCellData(i, 1);
        String pwd = ExcelUtils.getCellData(i, 2);
        String oldEMailTxt = ExcelUtils.getCellData(i, 3);
        String newfNameTxt = ExcelUtils.getCellData(i, 4);
        String newlNameTxt = ExcelUtils.getCellData(i, 5);
        String newEMailTxt = ExcelUtils.getCellData(i, 6);
        String newPgmNameTxt = ExcelUtils.getCellData(i, 7);
        String newRoleTxt = ExcelUtils.getCellData(i, 8);
        String programRoleTxt = ExcelUtils.getCellData(i, 9);
        String newCohortTxt = ExcelUtils.getCellData(i, 10);
        String expRes = ExcelUtils.getCellData(i, 11);
                
	
	/*lp.login(uName, pwd);
	lp.logibutton();
	try {
		Thread.sleep(10000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	nt.clickOnMainMenu();
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	nt.clickOnMenuTab();*/
    nt.clickOnAdmin();
	mi.clickOnUsers();
//	try {
//		Thread.sleep(15000);
//	} catch (InterruptedException e1) {
//		// TODO Auto-generated catch block
//		e1.printStackTrace();
//	}
	
	mi.fetchAppTool("div[id$='view-manage-users']");
	try {
		Thread.sleep(4000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	mu.clikOnSearchUserButton();
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	//mi.goToManageUsers();
	mu.enterSearchUser(oldEMailTxt);
	mu.pressEnter();
	
	
	
	mu.clickOnUserCheckbox(oldEMailTxt);
	mu.clickOnEditSelectedButton();
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	/*mu.clearFirstName();
	mu.enterFirstName(newfNameTxt);
	mu.enterLastNAme(newlNameTxt);
	mu.clearEmailId();
	mu.enterEmailId(newEMailTxt);
	//mu.selectPgmCheckBox(pgmNameTxt);
	mu.clickOnRoleDropDown();
	try {
		Thread.sleep(4000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	mu.selectARole(newRoleTxt);
	mu.enterCohort(newCohortTxt);*/
//	mu.selectPgmCheckBox(newPgmNameTxt);
//	try {
//		Thread.sleep(4000);
//	} catch (InterruptedException e1) {
//		// TODO Auto-generated catch block
//		e1.printStackTrace();
//	}
	mu.clikOnAddProgramRoleButton();
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}

	mu.clikOnProgramListButton();
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}

	mu.selectProgram(newPgmNameTxt);
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}

	mu.clikOnProgramRoleListButton();
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}

	mu.selectRole(programRoleTxt);
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	mu.clikOnSaveEditUserButton();
	try {
		Thread.sleep(1000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	mu.clikOnSearchUserButton();
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	/*String actRes = mu.fetchUserEditSuccessMsg();//newEMailTxt);
	System.out.println(actRes);
	
	 ExcelUtils.setCellData(actRes, i, 11, Constant.File_TestData_ManageUsers);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 12, Constant.File_TestData_ManageUsers);
			soft.assertTrue(true, "Toast");
			System.out.println("The test case passed on verifying Toast.");
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 12, Constant.File_TestData_ManageUsers);
			soft.assertTrue(false, "The test case failed on verifying Toast.\n");
		}*/
		
		
		}
		//soft.assertAll();
		
		}
	
	
	public void testEditUser(String str) 
	{
		LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		MenuItems mi	= new MenuItems();
		//Landing ld = new Landing();
		ManageUsers mu = new ManageUsers();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManageUsers, str);
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String uName = ExcelUtils.getCellData(i, 1);
        String pwd = ExcelUtils.getCellData(i, 2);
        String oldEMailTxt = ExcelUtils.getCellData(i, 3);
        String newfNameTxt = ExcelUtils.getCellData(i, 4);
        String newlNameTxt = ExcelUtils.getCellData(i, 5);
        String newEMailTxt = ExcelUtils.getCellData(i, 6);
        String newPgmNameTxt = ExcelUtils.getCellData(i, 7);
        String newRoleTxt = ExcelUtils.getCellData(i, 8);
        String programRoleTxt = ExcelUtils.getCellData(i, 9);
        String newCohortTxt = ExcelUtils.getCellData(i, 10);
        String expRes = ExcelUtils.getCellData(i, 11);
        // determines whether we want to remove or add
        String action = ExcelUtils.getCellData(i, 14);
                
	
	/*lp.login(uName, pwd);
	lp.logibutton();
	try {
		Thread.sleep(10000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	nt.clickOnMainMenu();
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	nt.clickOnMenuTab();*/
    nt.clickOnAdmin();
	mi.clickOnUsers();
//	try {
//		Thread.sleep(15000);
//	} catch (InterruptedException e1) {
//		// TODO Auto-generated catch block
//		e1.printStackTrace();
//	}
	
	mi.fetchAppTool("div[id$='view-manage-users']");
	try {
		Thread.sleep(4000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	mu.clikOnSearchUserButton();
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	//mi.goToManageUsers();
	mu.enterSearchUser(oldEMailTxt);
	mu.pressEnter();
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	
	mu.clickOnUserCheckbox(oldEMailTxt);
	mu.clickOnEditSelectedButton();
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	mu.clickOnEditUserButton();
	
	mu.clearFirstName();
	mu.enterFirstName(newfNameTxt);
	mu.clearLastName();
	mu.enterLastNAme(newlNameTxt);
	mu.clearEmailId();
	mu.enterEmailId(newEMailTxt);
	mu.clickOnRoleDropDown();
	try {
		Thread.sleep(4000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	mu.selectARole(newRoleTxt);
	mu.clearCohort();
	mu.enterCohort(newCohortTxt);
	try {
		Thread.sleep(4000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	// This changes
	/*mu.selectPgmCheckBox(newPgmNameTxt);
	try {
		Thread.sleep(4000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}*/
	
	if(action.equalsIgnoreCase("Add")) {
		
		mu.clikOnAddProgramRoleButton();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
		mu.clikOnProgramListButton();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
		mu.selectProgram(newPgmNameTxt);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
		mu.clikOnProgramRoleListButton();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
		mu.selectRole(programRoleTxt);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	} else{
		mu.clikOnDeselectProgramRoleButton(newPgmNameTxt);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		nt.clickOnDeletePgmYesButton();
	}
	//////////
	mu.clikOnSaveEditUserButton();
	try {
		Thread.sleep(1000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	String actRes = "Good!";//newEMailTxt);
	System.out.println(actRes);
	try {
		Thread.sleep(8000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	/////////////
	mu.clikOnSearchUserButton();
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	////////
	 /*ExcelUtils.setCellData(actRes, i, 12, Constant.File_TestData_ManageUsers);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 13, Constant.File_TestData_ManageUsers);
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 13, Constant.File_TestData_ManageUsers);
		}*/
		
		
		}
		
		}

}