package education.volute.manage_users_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.ManageUsers;
import utilities.Constant;
import utilities.SoftAssertCheck;

public class ContentUser extends SuperTestScript
{
	   // @Test
		public void verifyInfo(String textShows) //throws Exception
		{
			ManageUsers mu = new ManageUsers();
			SoftAssertCheck soft = new SoftAssertCheck();
			
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManageUsers, textShows);
			for(int i=1; i<=ExcelUtils.getRowcount(); i++)
			{
				String text = ExcelUtils.getCellData(i, 0);
				String emailText = ExcelUtils.getCellData(i, 1);
				String nameText = ExcelUtils.getCellData(i, 2);
				String expRes = ExcelUtils.getCellData(i, 3);
				//String actRes;
				
				if(text.equalsIgnoreCase("Program")){
					String actRes;
					
					actRes = mu.fetchProgram(emailText, nameText);
					System.out.println(actRes);	
					
					ExcelUtils.setCellData(actRes, i, 4, Constant.File_TestData_ManageUsers);
					
					Boolean status = ValidationLib.verifyMsg(expRes, actRes);
					
					if(status)
					{
						ExcelUtils.setCellData("pass", i, 5, Constant.File_TestData_ManageUsers);
						soft.assertTrue(true, text);
						System.out.println("The test case passed on verifying "+text+".");
					}
					else 
				{
						ExcelUtils.setCellData("Fail", 1, 5, Constant.File_TestData_ManageUsers);
						soft.assertTrue(false, "The test case failed on verifying "+text+".\n");
					}
				}else{
					
					String actRes;
					
					actRes = mu.fetchUserInfo(emailText, nameText);
					System.out.println(actRes);	
					
					ExcelUtils.setCellData(actRes, i, 4, Constant.File_TestData_ManageUsers);
					
					Boolean status = ValidationLib.verifyMsg(expRes, actRes);
					
					if(status)
					{
						ExcelUtils.setCellData("pass", i, 5, Constant.File_TestData_ManageUsers);
						soft.assertTrue(true, text);
						System.out.println("The test case passed on verifying "+text+".");
					}
					else 
				{
						ExcelUtils.setCellData("Fail", 1, 5, Constant.File_TestData_ManageUsers);
						soft.assertTrue(false, "The test case failed on verifying "+text+".\n");
					}
				}	
				
			}
			soft.assertAll();
		}

}
