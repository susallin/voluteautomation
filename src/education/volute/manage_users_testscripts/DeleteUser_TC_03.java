package education.volute.manage_users_testscripts;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.Landing;
import education.volute.pages.ManageUsers;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import utilities.Constant;

public class DeleteUser_TC_03 extends SuperTestScript
{

	//@Test
	public void testDeleteUserTC_03() 
	{
		
		LoginPage lp = new LoginPage();
		NavigatorTool nt = new NavigatorTool();
		MenuItems mi	= new MenuItems();
		//Landing ld = new Landing();
		ManageUsers mu = new ManageUsers();
				
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManageUsers, "TC_03");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
		
		String uName = ExcelUtils.getCellData(i, 1);
        String pwd = ExcelUtils.getCellData(i, 2);
        String EMailTxt = ExcelUtils.getCellData(i, 3);
        String expRes = ExcelUtils.getCellData(i, 4);
                
	
	/*lp.login(uName, pwd);
	lp.logibutton();
	try {
		Thread.sleep(10000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	nt.clickOnMainMenu();
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	nt.clickOnMenuTab();
	mi.clickOnUsers();
	try {
		Thread.sleep(15000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	mu.clikOnSearchUserButton();
	try {
		Thread.sleep(1000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	//mi.goToManageUsers();*/
    nt.clickOnAdmin();
    mi.clickOnUsers();
    try {
    	Thread.sleep(15000);
    } catch (InterruptedException e1) {
    	// TODO Auto-generated catch block
    	e1.printStackTrace();
    }
    mu.clikOnSearchUserButton();
    try {
    	Thread.sleep(1000);
    } catch (InterruptedException e1) {
    	// TODO Auto-generated catch block
    	e1.printStackTrace();
    }
	mu.enterSearchUser(EMailTxt);
	mu.pressEnter();
	mu.clickOnUserCheckbox(EMailTxt);
	mu.clickOnDeleteSelectedButton();
	mu.clickOnDeleteUserYesButton();
	try {
		Thread.sleep(1000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	String actRes = mu.fetchUserDeleteSuccessMsg();
	System.out.println(actRes);
	try {
		Thread.sleep(15000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
		
	 ExcelUtils.setCellData(actRes, i, 5, Constant.File_TestData_ManageUsers);
		
		Boolean status = ValidationLib.verifyMsg(expRes, actRes);
		
		if(status)
		{
			ExcelUtils.setCellData("pass", i, 6, Constant.File_TestData_ManageUsers);
		}
		else 
	{
			ExcelUtils.setCellData("Fail", i, 6, Constant.File_TestData_ManageUsers);
		}
		
		
		}
		
		}

}