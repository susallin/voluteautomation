package education.volute.file_testscripts;


import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.File;
import education.volute.pages.Landing;
import education.volute.pages.LoginPage;
//import education.volute.pages.ManageLearningSpaces;
import education.volute.pages.ManagePrograms;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import education.volute.pages.SocialHub;
import utilities.Constant;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class File_TC_02 extends SuperTestScript {

	//@Test
	public void testFile_TC_02(String Role)  	
	{
			//LoginPage lp = new LoginPage();
			NavigatorTool nt = new NavigatorTool();
			SoftAssertCheck soft = new SoftAssertCheck();
			MenuItems mi = new MenuItems();
			//SocialHub mj = new SocialHub();
			//Landing ld = new Landing();
			//ManagePrograms mp = new ManagePrograms();
			//ManageLearningSpaces mls = new ManageLearningSpaces();
			ToolDestAndCollections toolD = new ToolDestAndCollections();
			File con = new File();
					
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_File, "DeleteFile3");
			
			for(int i=1; i<=ExcelUtils.getRowcount(); i++)
			{
			
			//String uName = ExcelUtils.getCellData(i, 1);
	        //String pwd = ExcelUtils.getCellData(i, 2);
			String toolDest = ExcelUtils.getCellData(i, 1);
			String actName = ExcelUtils.getCellData(i, 2);
			String subActName = ExcelUtils.getCellData(i, 3);
			String pgmNameTxt = ExcelUtils.getCellData(i, 4);
	        String toolName = ExcelUtils.getCellData(i, 5);
	        String contentFile = ExcelUtils.getCellData(i, 6);
	        //String pgmNameTxt = ExcelUtils.getCellData(i, 3);
	        //String lsNameTxt = ExcelUtils.getCellData(i, 4);
	        String uRole = ExcelUtils.getCellData(i, 10);
	        String appLocName = ExcelUtils.getCellData(i, 11);
	        String expRes = ExcelUtils.getCellData(i, 7);
			
			/*lp.login(uName, pwd);
			lp.logibutton();
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			nt.clickOnMainMenu();*/
	        toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
			/*mi.clickOnNavigation();
			mi.clickOnProgram();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			ld.clickOnLandingMP();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mp.clickOnselectedprogram(pgmNameTxt);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mp.clickOnManageMashupicon(pgmNameTxt);
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/*mls.selectaLearningSpace(lsNameTxt);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mls.clickOnEditButton(lsNameTxt);
			mls.clickOnConfigButton();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			/*try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			nt.clickOnJourney();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mj.clickOnLaunchAct(actName);*/
			/*try {
				Thread.sleep(15000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			mi.fetchAppTool(appLocName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mi.SelectManageFaceOfTool(toolName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			//mi.clearDisplayName();
			//mi.enterDisplayName(contentDName);
			
			con.clickOnContentIntDeleteButton(contentFile);
			con.clickOnDeleteYesContentInt();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String actRes = con.fetchContShows(contentFile);
			System.out.println(actRes);
			if(actRes.equalsIgnoreCase("Not Found")){
				soft.assertTrue(true, "Checking if content shows after it was deleted");
				System.out.println("The test case passed, content does not show on Manage.");
			}else{
				soft.assertTrue(false, "The test case failed, content shows on Manage.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    mi.SelectExitManageFaceOfTool(toolName);
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    String actRes1 = con.fetchContIntFaceTool(contentFile);
			System.out.println(actRes1);
			if(actRes1.equalsIgnoreCase("Not Found")){
				soft.assertTrue(true, "Checking if content shows after it was deleted");
				System.out.println("The test case passed, content does not show on Face.");
			}else{
				soft.assertTrue(false, "The test case failed, content shows on Face.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			nt.refreshPage();
			/*try {
				Thread.sleep(15000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			mi.fetchAppTool(appLocName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String actRes1a = con.fetchContIntFaceTool(contentFile);
			System.out.println(actRes1a);
			if(actRes1a.equalsIgnoreCase("Not Found")){
				soft.assertTrue(true, "Checking if content shows after it was deleted");
				System.out.println("The test case passed, content does not show on Face.");
			}else{
				soft.assertTrue(false, "The test case failed, content shows on Face.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		    mi.SelectManageFaceOfTool(toolName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String actResShows = con.fetchContShows(contentFile);
			System.out.println(actResShows);
			if(actResShows.equalsIgnoreCase("Not Found")){
				soft.assertTrue(true, "Checking if content shows after it was deleted");
				System.out.println("The test case passed, content does not show Manage.");
			}else{
				soft.assertTrue(false, "The test case failed, content shows Manage.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			ExcelUtils.setCellData(actResShows, i, 8, Constant.File_TestData_File);
			
			Boolean status = ValidationLib.verifyMsg(expRes, actResShows);
			
			if(status)
			{
				ExcelUtils.setCellData("pass", i, 9,Constant.File_TestData_File);
			}
			else 
		{
				ExcelUtils.setCellData("Fail", i, 9,Constant.File_TestData_File);
			}
			
			
			}
			soft.assertAll();
		}
}
