package education.volute.contentintegration_testscripts;

import java.io.IOException;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.ContentIntegration;
import education.volute.pages.LoginPage;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import education.volute.pages.SocialHub;
import utilities.Constant;

public class ContentInt_TC_04 extends SuperTestScript {

	@Test
	public void testContentInt_TC_04() throws IOException 	
	{
					
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ContentIntegration, "AddConInShowsFace");
			
			for(int i=1; i<=ExcelUtils.getRowcount(); i++)
			{
			
			String uName = ExcelUtils.getCellData(i, 1);
	        String pwd = ExcelUtils.getCellData(i, 2);
	        String actName = ExcelUtils.getCellData(i, 3);
	        //String pgmNameTxt = ExcelUtils.getCellData(i, 3);
	        //String lsToolName = ExcelUtils.getCellData(i, 4);
	        //String lsNameTxt = ExcelUtils.getCellData(i, 5);
	        //String lsDescTxt = ExcelUtils.getCellData(i, 6);
	        String contentDName = ExcelUtils.getCellData(i, 4);
	        String contentFile = ExcelUtils.getCellData(i, 5);
	        String contentResFile = ExcelUtils.getCellData(i, 6);
	        //String vidURL2 = ExcelUtils.getCellData(i, 9);
	        String expRes = ExcelUtils.getCellData(i, 7);
	        
	        LoginPage lp = new LoginPage();
			NavigatorTool nt = new NavigatorTool();
			MenuItems mi = new MenuItems();
			//Landing ld = new Landing();
			//ManagePrograms mp = new ManagePrograms();
			//ManageLearningSpaces mls = new ManageLearningSpaces();
			SocialHub mj = new SocialHub();
			ContentIntegration con = new ContentIntegration();
			
			lp.login(uName, pwd);
			lp.logibutton();
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			nt.clickOnMainMenu();
			/*mi.clickOnNavigation();
			mi.clickOnProgram();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			ld.clickOnLandingMP();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mp.clickOnselectedprogram(pgmNameTxt);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mp.clickOnManageMashupicon(pgmNameTxt);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/*mls.clickOnAddLS();
			mls.selectaTool(lsToolName);
			mls.enterLSName(lsNameTxt);
			mls.enterLsDesc(lsDescTxt);
			mls.clickOnSaveButton();
			mls.clickOnConfigButton();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			nt.clickOnJourney();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mj.clickOnLaunchAct(actName);
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			con.clickOnContentIntManage();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mi.clearDisplayName();
			mi.enterDisplayName(contentDName);
			con.clickOnContentIntSelectFile();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+contentFile);
			
			try {
				Thread.sleep(15000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			con.clickOnSaveContentFile();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			con.clickOnContentIntDoneButton();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String actRes = con.fetchContIntFaceTool(contentResFile);
			System.out.println(actRes);
			ExcelUtils.setCellData(actRes, i, 8, Constant.File_TestData_ContentIntegration);
			
			Boolean status = ValidationLib.verifyMsg(expRes, actRes);
			
			if(status)
			{
				ExcelUtils.setCellData("pass", i, 9,Constant.File_TestData_ContentIntegration);
			}
			else 
		{
				ExcelUtils.setCellData("Fail", i, 9,Constant.File_TestData_ContentIntegration);
			}
			
			
			}
			
		}
}
