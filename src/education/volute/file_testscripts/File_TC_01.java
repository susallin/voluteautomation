package education.volute.file_testscripts;

import java.io.IOException;
import java.util.Iterator;

import org.testng.annotations.Test;

import education.volute.common_lib.SuperTestScript;
import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.LoginPage;
import education.volute.pages.File;
//import education.volute.pages.ManageLearningSpaces;
import education.volute.pages.ManagePrograms;
import education.volute.pages.Landing;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import education.volute.pages.SocialHub;
import utilities.Constant;
import utilities.ReadCsvFiles;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

public class File_TC_01 extends SuperTestScript{
	
	//@Test
	public void testFile_TC_01(String Role) throws Exception 	
	{
			//LoginPage lp = new LoginPage();
			NavigatorTool nt = new NavigatorTool();
			MenuItems mi = new MenuItems();
			SoftAssertCheck soft = new SoftAssertCheck();
			//Landing ld = new Landing();
			//ManagePrograms mp = new ManagePrograms();
			//ManageLearningSpaces mls = new ManageLearningSpaces();
			//SocialHub mj = new SocialHub();
			ReadCsvFiles csv = new ReadCsvFiles();
			ToolDestAndCollections toolD = new ToolDestAndCollections();
			File con = new File();		
		
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_File, "AddFilePart3");
			
			for(int i=1; i<=ExcelUtils.getRowcount(); i++)
			{
			
			//String uName = ExcelUtils.getCellData(i, 1);
	        //String pwd = ExcelUtils.getCellData(i, 2);
			String toolDest = ExcelUtils.getCellData(i, 1);
		    String actName = ExcelUtils.getCellData(i, 2);
		    String subActName = ExcelUtils.getCellData(i, 3);
		    String pgmNameTxt = ExcelUtils.getCellData(i, 4);
		    String toolName = ExcelUtils.getCellData(i, 5);
	        //String pgmNameTxt = ExcelUtils.getCellData(i, 3);
	        //String lsToolName = ExcelUtils.getCellData(i, 4);
	        //String lsNameTxt = ExcelUtils.getCellData(i, 5);
	        //String lsDescTxt = ExcelUtils.getCellData(i, 6);
	        //String contentDName = ExcelUtils.getCellData(i, 4);
	        String contentFile = ExcelUtils.getCellData(i, 6);
	        //String vidURL2 = ExcelUtils.getCellData(i, 9);
	        String uRole = ExcelUtils.getCellData(i, 10);
	        String appLocName = ExcelUtils.getCellData(i, 11);
	        String expRes = ExcelUtils.getCellData(i, 7);
			
			/*lp.login(uName, pwd);
			lp.logibutton();
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			nt.clickOnMainMenu();*/
			toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);
			/*mi.clickOnNavigation();
			mi.clickOnProgram();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			ld.clickOnLandingMP();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mp.clickOnselectedprogram(pgmNameTxt);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mp.clickOnManageMashupicon(pgmNameTxt);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/*mls.clickOnAddLS();
			mls.selectaTool(lsToolName);
			mls.enterLSName(lsNameTxt);
			mls.enterLsDesc(lsDescTxt);
			mls.clickOnSaveButton();
			mls.clickOnConfigButton();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			/*try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			nt.clickOnJourney();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mj.clickOnLaunchAct(actName);*/
			/*try {
				Thread.sleep(15000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			mi.fetchAppTool(appLocName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mi.SelectManageFaceOfTool(toolName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			//mi.clearDisplayName();
			//mi.enterDisplayName(contentDName);
			Iterator<String[]> FileName = csv.ReadList(contentFile);
			
			if(FileName.hasNext())
				FileName.next();
			
			while(FileName.hasNext()){
				int n = 0;
				String[] Name = FileName.next();
				boolean flag = true;
				
				String File = Name[n];
				
				mi.SelectAdd(toolName);
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				con.clickOnContentIntSelectFile(File);
				/*try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+File);*/
				while(flag){
					String result = nt.fetchProgressFile();
					
					if(result.equalsIgnoreCase("Found")){
						con.clickOnSaveContentFile();
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						flag = false;
					}
					else{
						flag = true;
					}
				}
			}
			
			/*con.clickOnContentIntSelectFile();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Runtime.getRuntime().exec("./user_data/Autoit/FileUploadPDF.exe"+" "+contentFile);
			
			try {
				Thread.sleep(40000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			con.clickOnSaveContentFile();*/
			
			String actRes = con.fetchContentIntAddMsg();
			System.out.println(actRes);
			if(actRes.equalsIgnoreCase("File added successfully")){
				soft.assertTrue(true, "Checking if toast shows/appears");
				System.out.println("The test case passed, toast shows.");
			}else{
				soft.assertTrue(false, "The test case failed, toast does not show.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			ExcelUtils.setCellData(actRes, i, 8, Constant.File_TestData_File);
			
			Boolean status = ValidationLib.verifyMsg(expRes, actRes);
			
			if(status)
			{
				ExcelUtils.setCellData("pass", i, 9,Constant.File_TestData_File);
			}
			else 
		{
				ExcelUtils.setCellData("Fail", i, 9,Constant.File_TestData_File);
			}
			
			
			}
			
		}
}

