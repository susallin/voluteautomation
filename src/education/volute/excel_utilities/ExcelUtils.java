package education.volute.excel_utilities;

	import java.io.FileInputStream;
	 
    import java.io.FileOutputStream;

    import org.apache.poi.xssf.usermodel.XSSFCell;

	import org.apache.poi.xssf.usermodel.XSSFRow;

	import org.apache.poi.xssf.usermodel.XSSFSheet;

	import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.Reporter;

import utilities.Constant;
public class ExcelUtils {

		private static XSSFSheet ExcelWSheet;

		private static XSSFWorkbook ExcelWBook;

		private static XSSFCell Cell;

		private static XSSFRow Row;

	//This method is to set the File path and to open the Excel file, Pass Excel Path and Sheetname as Arguments to this method

	public static void setExcelFile(String Path,String SheetName) 
	{

			try {

   			// Open the Excel file

			FileInputStream ExcelFile = new FileInputStream(Path);

			// Access the required test data sheet

			ExcelWBook = new XSSFWorkbook(ExcelFile);

			ExcelWSheet = ExcelWBook.getSheet(SheetName);

			} catch (Exception e){

				Reporter.log("Excel path is not set successfully",true);

			}

	}

	//This method is to read the test data from the Excel cell, in this we are passing parameters as Row num and Col num

    public static String getCellData(int RowNum, int ColNum)

    {

			try{

  			Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);

  			String CellData = Cell.getStringCellValue();

  			return CellData;

  			}catch (Exception e){

				return"";

  			}

    }

	//This method is to write in the Excel cell, Row num and Col num are the parameters

	public static void setCellData(String Result,  int RowNum, int ColNum, String File_TestData) 
	{

			try{

  			Row  = ExcelWSheet.getRow(RowNum);

			Cell = Row.getCell(ColNum, Row.RETURN_BLANK_AS_NULL);

			if (Cell == null) {

				Cell = Row.createCell(ColNum);

				Cell.setCellValue(Result);

				} else {

					Cell.setCellValue(Result);

				}

  // Constant variables Test Data path and Test Data file name

  				FileOutputStream fileOut = new FileOutputStream(Constant.Path_TestData + File_TestData);

  				ExcelWBook.write(fileOut);

  				fileOut.flush();

					fileOut.close();

				}catch(Exception e){

					Reporter.log("Data is not written to excel successfully",true);

			}

		}
	
	//This method returns the row count

    public static int getRowcount() 
{

			try{

				int rowCount = ExcelWSheet.getLastRowNum();

  			return rowCount;

  			}catch (Exception e){

				return 0;

  			}

    }

  //This method returns the column count for a particular row

    public static int getColcount(int i) 
    {

			try{

				int colCount = ExcelWSheet.getRow(i).getLastCellNum();

  			return colCount;

  			}catch (Exception e){

				return 0;

  			}

    }
}