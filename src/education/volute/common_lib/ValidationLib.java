package education.volute.common_lib;

import org.testng.Assert;

public class ValidationLib {

	public static boolean verifyMsg(String expMsg, String actMsg)
	{
		try
		{
			Assert.assertEquals(actMsg, expMsg);
			return true;
		}
		
		catch(AssertionError rv)
		{
			return false;
		}
	}
}

