package education.volute.common_lib;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

//import java.io.File;

//import org.apache.commons.io.FileUtils;
//import org.openqa.selenium.OutputType;
//import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.IInvokedMethod;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.Reporter;

public class FailListener implements ITestListener
{
	
	
	/*public void onTestFailure(ITestResult res) 
	{
		String mn = res.getName();
		takeScreenshot(mn);
	}
	
	public void takeScreenshot(String methodName)
	{
		try
		{
			FirefoxDriver f1 = (FirefoxDriver)SuperTestScript.driver;
			File src = f1.getScreenshotAs(OutputType.FILE);
			File dest = new File("./screenshots/"+methodName+".png");
			FileUtils.moveFile(src, dest);
		}
		
		catch(Exception rv)
		{
			Reporter.log("Screenshot not captured for"+ methodName);
		}
	}*/
	
	// This belongs to ITestListener and will execute before starting of Test set/batch.
	//@Override
	public void onStart(ITestContext result) {
		// TODO Auto-generated method stub
		Reporter.log("About to begin executing " + result.getName(), true);
	}

	// This belongs to ITestListener and will execute, once the Test set/batch is finished.
	//@Override
	public void onFinish(ITestContext result) {
		// TODO Auto-generated method stub
		Reporter.log("Completed executing " + result.getName(), true);
	}
	
	// This belongs to ITestListener and will execute only when the test is pass.
	//@Override
	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub
		printTestResults(result);
	}
	
	// This belongs to ITestListener and will execute only on the event of fail test.
	//@Override
	public void onTestFailure(ITestResult result) { 
		// This is calling the printTestResults method
		printTestResults(result);
		Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formater = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");
        String methodName = result.getName();
        if(!result.isSuccess()){
            File scrFile = ((TakesScreenshot)SuperTestScript.driver).getScreenshotAs(OutputType.FILE);
            try {
                String reportDirectory = new File(System.getProperty("user.dir")).getAbsolutePath() + "/target/surefire-reports";
                File destFile = new File((String) reportDirectory+"/failure_screenshots/"+methodName+"_"+formater.format(calendar.getTime())+".png");
                FileUtils.copyFile(scrFile, destFile);
                Reporter.log("<a href='"+ destFile.getAbsolutePath() + "'> <img src='"+ destFile.getAbsolutePath() + "' height='100' width='100'/> </a>");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    
	}
	
	// This belongs to ITestListener and will execute before the main test start (@Test).
	//@Override
	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub
		System.out.println("The execution of the main test starts now");
	}
	
	// This belongs to ITestListener and will execute only if any of the main test(@Test) get skipped
	//@Override
	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub
		printTestResults(result);
	}

	// This is the method which will be executed in case of test pass or fail
	// This will provide the information for the given test we are running
	private void printTestResults(ITestResult result) {
		 
		Reporter.log("Test Method resides in " + result.getTestClass().getName(), true);
 
		if (result.getParameters().length != 0) {
 
			String params = null;
 
			for (Object parameter : result.getParameters()) {
 
				params += parameter.toString() + ",";
 
			}
 
			Reporter.log("Test Method had the following parameters : " + params, true);
 
		}
 
		String status = null;
 
		switch (result.getStatus()) {
 
		case ITestResult.SUCCESS:
 
			status = "Pass";
 
			break;
 
		case ITestResult.FAILURE:
 
			status = "Failed";
 
			break;
 
		case ITestResult.SKIP:
 
			status = "Skipped";
 
		}
 
		Reporter.log("Test Status: " + status, true);
 
	}
	
	// This belongs to IInvokedMethodListener and will execute before every method including @Before @After @Test
	public void beforeInvocation(IInvokedMethod arg0, ITestResult arg1) {
		 
		String textMsg = "About to begin executing following method : " + returnMethodName(arg0.getTestMethod());
 
		Reporter.log(textMsg, true);
 
	}
 
	// This belongs to IInvokedMethodListener and will execute after every method including @Before @After @Test
	public void afterInvocation(IInvokedMethod arg0, ITestResult arg1) {
 
		String textMsg = "Completed executing following method : " + returnMethodName(arg0.getTestMethod());
 
		Reporter.log(textMsg, true);
 
	}
	
	// This will return method names to the calling function
	private String returnMethodName(ITestNGMethod method) {	 
		return method.getRealClass().getSimpleName() + "." + method.getMethodName();
	}
	
	//@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
		// Here we do nothing for the time.
	}

}
