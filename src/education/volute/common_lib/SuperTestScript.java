package education.volute.common_lib;

import java.io.File;
//import org.apache.commons.io.FileUtils;
import java.io.IOException;
import java.net.URL;
//import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.io.Zip;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
//import org.openqa.selenium.OutputType;
//import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.Parameters;

import com.gargoylesoftware.htmlunit.BrowserVersion;

import education.volute.excel_utilities.ExcelUtils;
import utilities.Constant;

//import education.volute.excel_utilities.ExcelUtils;
//import utilities.Constant;
//import utilities.SoftAssertCheck;

public class SuperTestScript 
{
	//public static ThreadLocal<RemoteWebDriver> driver = new ThreadLocal<>();
	public static final String USERNAME = "voluteeducation1";
	public static final String AUTOMATE_KEY = "TBSKPNJiSmZXq1LDy4n3";
	public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
	public static WebDriver driver;
	@BeforeMethod
	@Parameters("browserType")
	public void preSteps(String browserType) throws Exception
	{
		//RemoteWebDriver driver = driver();
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Config, "info");
		String buildUrl = ExcelUtils.getCellData(1, 0);
		//String browserType = ExcelUtils.getCellData(1, 1);
		
		
		if(browserType.equalsIgnoreCase("chrome"))//.equals(GC"))
		{
			System.setProperty("webdriver.chrome.driver", "./exe_files/chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().window().maximize();
		}
		
		else if (browserType.equals("internet"))//.equals(IE))
		{
			System.setProperty("webdriver.ie.driver", "./exe_files/IEDriverServer.exe");
			driver = new InternetExplorerDriver();
			driver.manage().window().maximize();
		}
		
		else if(browserType.equals("Edge"))//.equals(ED))
		{
			System.setProperty("webdriver.edge.driver", "./exe_files/MicrosoftWebDriver.exe");
			driver = new EdgeDriver();
			driver.manage().window().maximize();
		}
		
		else if(browserType.equalsIgnoreCase("firefox"))
		{
			System.setProperty("webdriver.gecko.driver", "./exe_files/geckodriver.exe");
			driver = new FirefoxDriver();
		}
		else if(browserType.equalsIgnoreCase("headlesschrome"))
		{
			System.setProperty("webdriver.chrome.driver", "./exe_files/chromedriver.exe");
			ChromeOptions options = new ChromeOptions();
            options.addArguments("headless");
            options.addArguments("window-size=1200x600");
            driver = new ChromeDriver(options);
			/*File src = new File("./exe_files/phantomjs-2.1.1-windows/bin/phantomjs.exe");
			System.setProperty("phantomjs.binary.path", src.getAbsolutePath());
			driver = new PhantomJSDriver();*/
			//driver.manage().window().maximize();
		}
		else if(browserType.equalsIgnoreCase("headless"))
		{
			driver = new HtmlUnitDriver(true);
			HtmlUnitDriver unitDriver = new HtmlUnitDriver(BrowserVersion.CHROME);
			unitDriver.setJavascriptEnabled(true);
		}
		else if(browserType.equalsIgnoreCase("gridgc")){
			DesiredCapabilities cap = new DesiredCapabilities();
			cap.setCapability("browser", "Chrome");
		    cap.setCapability("browser_version", "67.0");
		    cap.setCapability("os", "Windows");
		    cap.setCapability("os_version", "10");
		    cap.setCapability("resolution", "1024x768");
		    cap.setCapability("browserstack.debug", "true");
		    cap.setCapability("browserstack.networkLogs", "true");
		    
		    driver = new RemoteWebDriver(new URL(URL),cap);
			/*cap.setBrowserName("chrome");
			
			cap.setPlatform(Platform.WINDOWS);
			
			URL url = new URL("http://192.168.163.1:4444/grid/console");
			
			driver = new RemoteWebDriver(url,cap);*/
		}
		else
		{
			// If no browser passes
			throw new Exception("Browser is not correct.");
		}
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		driver.get(buildUrl);
		//driver.manage().window().maximize();
		
	}
	
	/*@Test
	public void testSoftAssert(String expRes, String msg)
	{
		SoftAssert assertion = new SoftAssert();
		
		System.out.println("softAssert Method Was Started");
		assertion.assertEquals(expRes, "Found","The test case failed on verifying "+msg+".");
		System.out.println("softAssert Method Was Executed");
		
		if(expRes.equalsIgnoreCase("Found")){
			System.out.println("The test case passed on verifying "+msg+".");
		}
		
		assertion.assertAll();
	}*/

	/*public static void takeSnapShot(WebDriver webdriver,String fileWithPath) throws Exception{
		//Convert web driver object to TakeScreenshot
		TakesScreenshot scrShot =((TakesScreenshot)webdriver);
		
		//Call getScreenshotAs method to create image file
		File SrcFile= scrShot.getScreenshotAs(OutputType.FILE);
		
		//Move image file to new destination
		File DestFile=new File(fileWithPath);
		
		//Copy file at destination
		FileUtils.copyFile(SrcFile, DestFile);
	}*/
	
	@AfterMethod
	public void postSteps()//String str) 
	{
				driver.quit();
	}
	
	@SuppressWarnings("static-access")
	@AfterSuite
	public void backUpSteps()
	{
		Date d1 = new Date();
		String currentDateAndTime = d1.toString().replace(':', '_');
		//File src = new File("./test-output");
		File dest = new File("./test-output/execution_reports/"+currentDateAndTime+".zip");
		
		Zip z1 = new Zip();
		try
		{
			z1.zip(dest);
			
		}
		
		catch(IOException rv)
		{
			Reporter.log("Backup is unsuccess");
		}
		
		
	}
	
}



