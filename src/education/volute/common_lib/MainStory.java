package education.volute.common_lib;

public class MainStory {

	Story story = new Story();
	
	public void Login(){
		story.LoginStory();
	}
	
	public void Tutorial(){
		story.TutorialStory();
	}
	
	public void CreateProgram() throws Exception{
		story.ProgramStartStory();
	}
	
	public void CreateLearningSpaces() throws Exception{
		story.ProgramStory();
	}
	
	public void CreateSecondLearningSpaces() throws Exception{
		story.ProgramStory2();
	}
	
	public void CreateThirdLearningSpaces() throws Exception{
		story.ProgramStory2a();
	}
	
	public void SwitchLearningSpaceSocialHub() throws Exception{
		story.SocialHubStory3();
	}
	
	public void SearchLearningSpaces() throws Exception{
		story.ProgramSearchStory();
	}
	
	public void EditLearningSpace(){
		story.ProgramStory3();
	}
	
	public void SwitchViewLearningSpace(){
		story.ProgramStory4();
	}
	
	public void Login(){
		story.LoginStory();
	}
}
