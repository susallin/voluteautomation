package education.volute.common_lib;

import java.io.IOException;
import java.util.ArrayList;

import org.testng.annotations.Test;

import education.volute.announcement_testscripts.AnnouncementTest;
import education.volute.assessments_testscripts.AssessmentTest;
import education.volute.biography_testscripts.BioTest;
import education.volute.calendar_testscripts.CalendarTest;
//import org.junit.Test;
import education.volute.collaborate_testscripts.GroupTest;
import education.volute.file_testscripts.FileTest;
import education.volute.ideaboard_testscripts.IdeaBoardTest;
import education.volute.directory_testscripts.DirectoryTest;
import education.volute.discussion_testscripts.DiscussionTest;
import education.volute.excel_utilities.ExcelUtils;
//import education.volute.excel_utilities.ExcelUtils;
import education.volute.login_testscripts.LoginTest;
import education.volute.manage_pgm_testscripts.ManageProgramsTest;
import education.volute.manage_users_testscripts.ManageUsersTest;
import education.volute.my_socialhub_testscripts.SocialHubTest;
//import education.volute.my_socialhub_testscripts.SocialHub_TC_01;
import education.volute.notes_testscripts.NotesTest;
import education.volute.pages.Discussion;
import education.volute.pages.ManageDesignAct;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import education.volute.pages.Reader;
import education.volute.passport_testscripts.PassportTest;
import education.volute.photoshare_testscripts.PhotoshareTest;
import education.volute.portal_testscripts.PortalTest;
import education.volute.privatemarketplace_testscripts.PrivateMTest;
import education.volute.profile_testscripts.ProfileTest;
import education.volute.profilelists_testscripts.ProfileListTest;
import education.volute.reader_testscripts.ReaderTest;
import education.volute.richtexteditor_testscripts.RichTextTest;
import education.volute.slides_testscripts.SlidesTest;
import education.volute.tasks_testscripts.TasksTest;
import education.volute.tutorialjoyride_testscripts.TutorialTest;
import education.volute.video_testscripts.VideoTest;
import utilities.Constant;
import utilities.SearchElements;
//import education.volute.pages.MenuItems;
//import education.volute.pages.NavigatorTool;
//import education.volute.tasks_testscripts.TasksTest;
//import utilities.Constant;
import utilities.SoftAssertCheck;
import utilities.ToolDestAndCollections;

/** 
 * Here we define all the Scripts from each component and run each one separately or together.
 * Still work in Progress on setting their priorities. After defining them here, they are run on
 * an .xml call "TestinSide".
 * @author Raul J
 *
 */

public class Story extends SuperTestScript {
	ArrayList<String> listM = new ArrayList<String>();
	ArrayList<String> list = new ArrayList<String>();
	ArrayList<String> list2 = new ArrayList<String>();
	ArrayList<String> list3 = new ArrayList<String>();
	ArrayList<String> list4 = new ArrayList<String>();
	ArrayList<String> list5 = new ArrayList<String>();
	ArrayList<String> list6 = new ArrayList<String>();
	LoginTest lg = new LoginTest();
	SearchElements sr = new SearchElements();
	TutorialTest tj = new TutorialTest();
	NavigatorTool nt = new NavigatorTool();
	ToolDestAndCollections toolD = new ToolDestAndCollections();
	ManageProgramsTest pgm = new ManageProgramsTest();
	SocialHubTest sh = new SocialHubTest();
	ManageUsersTest mu = new ManageUsersTest();
	NotesTest not = new NotesTest();
	DiscussionTest dis = new DiscussionTest();
	GroupTest grp = new GroupTest();
	PrivateMTest pm = new PrivateMTest();
	SlidesTest slid = new SlidesTest();
	VideoTest vid = new VideoTest();
	TasksTest ts = new TasksTest();
	RichTextTest ric = new RichTextTest();
	ReaderTest red = new ReaderTest();
	ProfileListTest pl = new ProfileListTest ();
	DirectoryTest dir = new DirectoryTest();
	FileTest con = new FileTest();
	PortalTest por = new PortalTest();
	BioTest bio = new BioTest();
	ProfileTest pro = new ProfileTest();
	CalendarTest cal = new CalendarTest();
	AnnouncementTest an = new AnnouncementTest();
	PassportTest pas = new PassportTest();
	AssessmentTest am = new AssessmentTest();
	IdeaBoardTest id = new IdeaBoardTest();
	PhotoshareTest ph = new PhotoshareTest();
	MenuItems mi = new MenuItems();
	//private Integer Num;
	
	
	//@Test(priority = 0)
	public void LoginStory()
	{
		/*ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> list2 = new ArrayList<String>();
		ArrayList<String> list3 = new ArrayList<String>();
		ArrayList<String> list4 = new ArrayList<String>();*/
		
		lg.InvalidEmail();
		
		lg.InvalidPassword();
		
		lg.EmailConfirmation();//This works on GC only
		
		lg.EmailInvalid();//This works on GC only
		
		lg.Login("Login");
		
		/*list = sr.elementShows4("LoginLabel");
		list2 = sr.elementShows4("LoginLabel");
		list3 = sr.elementShows4("LoginLabel");
		list4 = sr.elementShows4("LoginLabel");
		sr.Validate(list,list2,list3,list4);*/
		lg.Logout();
	}
	
	//@Test(priority = 0)
		public void LoginStory2()
		{
			lg.ExpirePw();
		}
	
	//@Test(priority = 0)
	public void TutorialStory()
	{
		/*LoginTest lg = new LoginTest();
		TutorialTest tj = new TutorialTest();*/
		
		lg.Login("Login");
		
		tj.Tutorial();
	}
	
	
	
	//@Test(priority = 1)
	public void ProgramStartStory() throws Exception
	{
		lg.Login("Login");
		
		toolD.goToManagePrograms();
		
		pgm.AddPgm("addPgm");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*list = toolD.navSlides("Stay", "", "", "ProgramLabelAddForm2", "Owner");
//		try {
//			Thread.sleep(5000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		listM.addAll(list);
		
		list = toolD.navSlides("Face", "Act Desc", "", "ProgramDescription", "Owner");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		listM.addAll(list);
		
		//
		
		list = toolD.navSlides("Face", "Master", "", "ActivityMasterCopy", "Owner");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		listM.addAll(list);*/
		
		pgm.AddToolDesignAct("AddToolV1");
		
		/*list = toolD.navSlides("Face", "Activity", "", "StatusLabelForm", "Owner");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		listM.addAll(list);*/
		
		pgm.PublishActSub();
		
		// 3
		/*list = toolD.navSlides("Face", "Journey", "", "Journeysik.PNG", "Owner");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		listM.addAll(list);
		
		list = toolD.navSlides("Stay", "", "", "ProgramLabelContentAdded", "Owner");
		//list = toolD.navSlides("Face", "Manage Programs", "Mr.Script(Do not delete)", "ProgramLabelAddForm", "Owner");
		//list = toolD.navSlides("Face", "Act List", "Mr.Script(Do not delete)", "ActivityListLabel", "Owner");
//		try {
//			Thread.sleep(5000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		listM.addAll(list);*/
		
		sr.ValidateTools(listM, list2, list3, list4, list5, list6);
	}
	
	//@Test(priority = 2)
	public void ProgramStory() throws Exception
	{
		
		lg.Login("Login");
		
		toolD.goToManagePrograms();
		
		pgm.AddPrivateOpenAct("OpenPrivateAct");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// 2
		list2 = toolD.navSlides("Stay", "", "", "ActivityLabelForm", "Owner");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		listM.addAll(list2);
		
		list2 = toolD.navSlides("Face", "Act Desc", "", "ActivityDescription", "Owner");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		listM.addAll(list2);
		
		//		
		list2 = toolD.navSlides("Face", "Master", "", "ActivityMasterCopy", "Owner");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		listM.addAll(list2);//**//
		//AddToolV2
		pgm.AddToolDesignAct("AddToolV2");
		
		/*list2 = toolD.navSlides("Face", "Activity", "", "StatusLabelForm", "Owner");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		listM.addAll(list2);*/
		
		pgm.PublishActSub();
		
		// 3
		list2 = toolD.navSlides("Face", "Journey", "", "Journeyfirstactivitysik.PNG", "Owner"); //JourneyLabel
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		listM.addAll(list2);
		
		list2 = toolD.navSlides("Face", "Act List", "Mr.Script(Do not delete)", "ActivityListLabel", "Owner");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		listM.addAll(list2);
	
		pgm.AddSocialOpenAct();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		pgm.AddToolDesignAct("AddToolV1");
		
		pgm.PublishActSub();
		
		//
		pgm.AddPrivateOpenSubAct("OpenPrivateSubAct");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		pgm.AddToolDesignAct("AddToolV1");
		
		pgm.PublishActSub();
		
		pgm.AddSocialOpenSubAct("OpenSocialSubAct");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// 4
		list2 = toolD.navSlides("Stay", "", "", "ActivityLabelForm2", "Owner");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		listM.addAll(list2);
		
		list2 = toolD.navSlides("Face", "Act Desc", "", "ActivityDescription2", "Owner");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		listM.addAll(list2);
		
		list2 = toolD.navSlides("Face", "Master", "", "ActivityMasterCopy", "Owner");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		listM.addAll(list2);
		// AddToolV2
		pgm.AddToolDesignAct("AddToolV2");
		
		// 5
		/*list2 = toolD.navSlides("Face", "Activity", "", "StatusLabelForm", "Owner");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		listM.addAll(list2);*/
		
		pgm.PublishActSub();
		
		// 6
		//Changed Name JourneyLabel2
		list2 = toolD.navSlides("Face", "JourneySub", "", "Journeysecondactivitysik.PNG", "Owner");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		listM.addAll(list2);
		
		list2 = toolD.navSlides("Face", "Sub-Act List", "Mr.Script(Do not delete)", "ActivityListLabel2", "Activity for Sub activities");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		listM.addAll(list2);
		
		sr.ValidateTools(listM, list2, list3, list4, list5, list6);
	}
			
	//@Test(priority = 6)
	public void ProgramStory2a() throws Exception{
		/*LoginTest lg = new LoginTest();
		ToolDestAndCollections td = new ToolDestAndCollections();
		ManageProgramsTest pgm = new ManageProgramsTest();*/
		
		lg.Login("Login");
		
		toolD.goToManagePrograms();
		
		pgm.AddPrivateOpenAct("OpenPrivateAct2");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		pgm.AddToolDesignAct("AddToolV2");
		
		pgm.PublishActSub();
		
		pgm.AddPrivateOpenSubAct("OpenPrivateSubAct2");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		pgm.AddToolDesignAct("AddToolV1");
		
		pgm.PublishActSub();
		
		pgm.AddSocialOpenSubAct("OpenSocialSubAct2");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		pgm.AddToolDesignAct("AddToolV2");
		
		pgm.PublishActSub();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//@Test(priority = 7)
	public void SocialHubStory3() throws Exception{
		//SocialHubTest sh = new SocialHubTest();
		//LoginTest lg = new LoginTest();
		
		lg.Login("Login");
		
		sh.SocialHubChangeProgram("SocialHubProgChange");
	}
	
	//@Test(priority = 8)
		public void ProgramSearchStory() throws Exception
		{
			//SoftAssertCheck soft = new SoftAssertCheck();
			//LoginTest lg = new LoginTest();
			
			lg.Login("Login");
			
			toolD.goToManagePrograms();
			
			pgm.AddPgm("addPgm2");
			
			pgm.SearchProgram("SearchProg");
			
			pgm.DeletePgm("delPgm2");
		}
	
	//@Test(priority = 9)
	public void ProgramStory2b() throws Exception{
		
		lg.Login("Login");
		
		toolD.goToManagePrograms();
		
		pgm.AddPrivateExAct();
		
		pgm.AddToolDesignAct("AddToolV2");
		
		pgm.PublishActSub();
		
		
		pgm.AddSocialExAct();
		
		pgm.AddToolDesignAct("AddToolV2");
		
		pgm.PublishActSub();
	}
	
	//@Test(priority = 10)
	public void ProgramStory2c() throws Exception{
			
		lg.Login("Login");
			
		toolD.goToManagePrograms();
			
		pgm.AddPrivateExSubAct();
		
		pgm.AddToolDesignAct("AddToolV2");
		
		pgm.PublishActSub();
		
		
		pgm.AddSocialExSubAct();
		
		pgm.AddToolDesignAct("AddToolV2");
		
		pgm.PublishActSub();
	}
	
	//@Test(priority = 11)
	public void ProgramStory2() throws Exception{
		
		lg.Login("Login");
		
		toolD.goToManagePrograms();
		
		pgm.AddPrivateSubSubAct("OpenPrivateSubSubAct");
		
		pgm.AddToolDesignAct("AddToolV2");
		
		pgm.PublishActSub();
	}	
	
	//@Test(priority = 12)
	public void ProgramStory2d() throws Exception{
		
		lg.Login("Login");
		
		toolD.goToManagePrograms();
		
		pgm.AddSocialExclusiveSubSubAct("SocialExSubSubAct");
		
		pgm.AddToolDesignAct("AddToolV2");
		
		pgm.PublishActSub();
	}	
	
	//@Test(priority = 1)
	public void ProgramStory3(){
		/*LoginTest lg = new LoginTest();
		ToolDestAndCollections td = new ToolDestAndCollections();
		ManageProgramsTest pgm = new ManageProgramsTest();*/
		
		lg.Login("Login");
		
		toolD.goToManagePrograms();
		
		//pgm.EditPgm();
		
		pgm.EditActivity();
		
		/*try {
			Thread.sleep(500000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	//@Test(priority = 2)
	public void ProgramStory4(){
		/*LoginTest lg = new LoginTest();
		ToolDestAndCollections td = new ToolDestAndCollections();
		ManageProgramsTest pgm = new ManageProgramsTest();*/
				
		lg.Login("Login");
				
		pgm.SwitchView("Facilitator");
			
		/*try {
			Thread.sleep(500000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	//@Test(priority = 2)
	public void ProgramStory5(){
		/*LoginTest lg = new LoginTest();
		ToolDestAndCollections td = new ToolDestAndCollections();
		ManageProgramsTest pgm = new ManageProgramsTest();*/
					
		lg.Login("Login2");
					
		pgm.Sequencing("Participant");
				
		/*try {
			Thread.sleep(500000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	//@Test(priority = 2)
	public void ProgramStory6(){
		/*LoginTest lg = new LoginTest();
		ToolDestAndCollections td = new ToolDestAndCollections();
		ManageProgramsTest pgm = new ManageProgramsTest();*/
			
		lg.Login("Login");
			
		toolD.goToManagePrograms();
			
		pgm.EditActivity2();
		
		/*try {
			Thread.sleep(500000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	//@Test(priority = 10)
	public void ProgramStory7(){
		/*LoginTest lg = new LoginTest();
		ToolDestAndCollections td = new ToolDestAndCollections();
		ManageProgramsTest pgm = new ManageProgramsTest();*/
		
		lg.Login("Login");
		
		toolD.goToManagePrograms();
		
		//pgm.DeletePgm();
		
		pgm.DeleteActivity();
		
		/*try {
			Thread.sleep(500000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	//@Test(priority = 11)
	public void ProgramStory8(){
		/*LoginTest lg = new LoginTest();
		ToolDestAndCollections td = new ToolDestAndCollections();
		ManageProgramsTest pgm = new ManageProgramsTest();*/
		
		lg.Login("Login");
		
		toolD.goToManagePrograms();
		
		pgm.DeletePgm("delPgm");
		
		/*try {
			Thread.sleep(500000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	//@Test
	public void WaterMarkStory() throws Exception{
		/*LoginTest lg = new LoginTest();
		ManageProgramsTest pgm = new ManageProgramsTest();*/
			
		lg.Login("Login");
			
		pgm.CheckWaterMarks("WaterMarks", "Facilitator");
			
		/*try {
			Thread.sleep(500000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}

	//@Test
	public void WaterMarkStory2() throws Exception{
		/*LoginTest lg = new LoginTest();
		ManageProgramsTest pgm = new ManageProgramsTest();*/
				
		lg.Login("Login5");
		
		pgm.CheckWaterMarks("WaterMarks2", "Facilitator");
				
		/*try {
			Thread.sleep(500000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	//@Test
	public void DisplayNameStory(){
		/*LoginTest lg = new LoginTest();
		ManageProgramsTest pgm = new ManageProgramsTest();*/
		
		lg.Login("Login");
		
		pgm.DisplayName("Facilitator");
		
		/*try {
			Thread.sleep(500000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	//@Test
	public void UserStory()
	{
		/*ManageUsersTest mu = new ManageUsersTest();
		LoginTest lg = new LoginTest();*/
		
		lg.Login("Login");
		
		mu.AddUsers();
//		try {
//			Thread.sleep(500000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
			//mu.NameShows("ContentShowsAdd");
	}
	
	//2
	//@Test(priority = 3)
	public void UserStory2()
	{
		//ManageUsersTest mu = new ManageUsersTest();
//		ArrayList<String> listM = new ArrayList<String>();
//		ArrayList<String> list = new ArrayList<String>();
//		ArrayList<String> list2 = new ArrayList<String>();
//		ArrayList<String> list3 = new ArrayList<String>();
//		ArrayList<String> list4 = new ArrayList<String>();
//		ArrayList<String> list5 = new ArrayList<String>();
//		ArrayList<String> list6 = new ArrayList<String>();
		//SearchElements sr = new SearchElements();
		//ToolDestAndCollections toolD = new ToolDestAndCollections();
		//LoginTest lg = new LoginTest();
		
		lg.Login("Login");
		
		mu.EditUser2("TC_02New");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list = toolD.navSlides("Face", "Manage Users", "sandyscriptpro12@mailinator.com", "ManageUserLabel", "Owner");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		listM.addAll(list);
		
		mu.EditUser2("TC_02Previous");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list = toolD.navSlides("Face", "Manage Users", "sandyscriptpro19@mailinator.com", "ManageUserLabel2", "Owner");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		listM.addAll(list);
		
		mu.EditUser();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		sr.ValidateTools(listM, list2, list3, list4, list5, list6);
	}
	
	//@Test
	public void UserStory21() throws Exception
	{
		/*ManageUsersTest mu = new ManageUsersTest();
		LoginTest lg = new LoginTest();*/
		
		//ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManageUsers, "ContentShows");
		
		lg.Login("Login");
		
		mu.EditUser2("");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			mu.NameShows("ContentShowsEdit");
		
	}
	
	//@Test
	public void UserStory3()
	{
		/*ManageUsersTest mu = new ManageUsersTest();
		LoginTest lg = new LoginTest();*/
		
		lg.Login("Login");
		
		mu.DeleteUser();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//ExcelUtils.setCellData("Test Case Edit Failed.", CelNum(), 9, Constant.File_TestData_Notes);
	//System.out.println("Test Case Edit Failed.");
	
	// Missing Locator for Notes
	
	//@Test(priority = 0)
	public void NotesStory()
	{
		//NotesTest nt = new NotesTest();
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> list2 = new ArrayList<String>();
		ArrayList<String> list3 = new ArrayList<String>();
		ArrayList<String> list4 = new ArrayList<String>();
		/*SearchElements sr = new SearchElements();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		//SoftAssertCheck sa = new SoftAssertCheck();
		LoginTest lg = new LoginTest();*/
		
		lg.Login("Login"); //login2
		
		not.AddNotes();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			//sa.testSoftAssert(false);
			//e.printStackTrace();
		}
		
		list = toolD.navSearch("Stay", "NotesLabelFloat", "Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//nt.EditNotes();
		not.EditNotes();	
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			//sa.testSoftAssert(false);
			//e.printStackTrace();
		}
		
		list2 = toolD.navSearch("Stay", "NotesLabelFloatEdit", "Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		sr.Validate(list, list2, list3, list4);
		
		/*try {
			Thread.sleep(500000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	//@Test(priority = 1)
	public void NotesStory2(){
		/*NotesTest nt = new NotesTest();
		LoginTest lg = new LoginTest();*/
		
		lg.Login("Login"); //login2
		
		not.DeleteNote();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			//sa.testSoftAssert(false);
			//e.printStackTrace();
		}
		
		/*try {
			Thread.sleep(500000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	//@Test(priority = 1)
		public void DiscussionSearchFacil() throws Exception
		{
			//DiscussionTest dis = new DiscussionTest();
			/*SearchElements sr = new SearchElements();
			ToolDestAndCollections toolD = new ToolDestAndCollections();
			//SoftAssertCheck soft = new SoftAssertCheck();
			//Discussion dis2 = new Discussion();
			LoginTest lg = new LoginTest();*/
			
			lg.Login("Login");
			
			dis.AddDiscussion("AddDiscussion2","Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
	//4
	//@Test(priority = 1)
	public void DiscussionStoryFacil() throws Exception
	{
		//DiscussionTest dis = new DiscussionTest();
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> list2 = new ArrayList<String>();
		ArrayList<String> list3 = new ArrayList<String>();
		ArrayList<String> list4 = new ArrayList<String>();
		/*SearchElements sr = new SearchElements();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		//SoftAssertCheck soft = new SoftAssertCheck();
		//Discussion dis2 = new Discussion();
		LoginTest lg = new LoginTest();*/
		
		lg.Login("Login");
		
		dis.AddDiscussion("AddDiscussion","Facilitator");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list = toolD.navSearch("Stay", "DiscLabelDockedFacil", "Facilitator");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list2 = toolD.navSearch("Activity", "DiscLabelDockedFacil", "Facilitator");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list3 = toolD.navSearch("Sub", "DiscLabelDockedFacil", "Facilitator");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list4 = toolD.navSearchFloat("DiscLabelFloatFacil");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		sr.Validate(list, list2, list3, list4);
	}
	
	//@Test(priority = 1)
	public void DiscussionStoryFacil2()
		{
			//DiscussionTest dis = new DiscussionTest();
			ArrayList<String> list = new ArrayList<String>();
			ArrayList<String> list2 = new ArrayList<String>();
			ArrayList<String> list3 = new ArrayList<String>();
			ArrayList<String> list4 = new ArrayList<String>();
			/*SearchElements sr = new SearchElements();
			ToolDestAndCollections toolD = new ToolDestAndCollections();
			//Discussion dis2 = new Discussion();
			LoginTest lg = new LoginTest();*/
			
			lg.Login("Login");
			
			dis.EditDiscussion("Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list = toolD.navSearch("Stay", "DiscLabelDockedEditFacil", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list2 = toolD.navSearch("Activity", "DiscLabelDockedEditFacil", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list3 = toolD.navSearch("Sub", "DiscLabelDockedEditFacil", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list4 = toolD.navSearchFloat("DiscLabelFloatEditFacil");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			sr.Validate(list, list2, list3, list4);
		}
	
	//@Test(priority = 2)
	public void DiscussionStoryFacil3()
		{
			/*DiscussionTest dis = new DiscussionTest();
			//Discussion dis2 = new Discussion();
			LoginTest lg = new LoginTest();*/
			
			lg.Login("Login");
			
			dis.DeleteDiscussion("Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
	/*@Test(priority = 2)
	public void DiscussionStoryFacil4()
		{
				DiscussionTest dis = new DiscussionTest();
				//Discussion dis2 = new Discussion();
				LoginTest lg = new LoginTest();
				
				lg.Login("Login");
				
				dis.SendDiscussion("Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}*/
	
	//@Test(priority = 0)
	public void DiscussionStory() throws Exception
	{
		//DiscussionTest dis = new DiscussionTest();
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> list2 = new ArrayList<String>();
		ArrayList<String> list3 = new ArrayList<String>();
		ArrayList<String> list4 = new ArrayList<String>();
		/*SearchElements sr = new SearchElements();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		//SoftAssertCheck soft = new SoftAssertCheck();
		//Discussion dis2 = new Discussion();
		LoginTest lg = new LoginTest();*/
		
		lg.Login("Login2");
		
		dis.AddDiscussion("AddDiscussion","Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list = toolD.navSearch("Stay", "DiscLabelDocked", "Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list2 = toolD.navSearch("Activity", "DiscLabelDocked", "Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list3 = toolD.navSearch("Sub", "DiscLabelDocked", "Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list4 = toolD.navSearchFloat("DiscLabelFloat");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		sr.Validate(list, list2, list3, list4);
	}
	
	//@Test(priority = 1)
	public void DiscussionStory2()
	{
		//DiscussionTest dis = new DiscussionTest();
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> list2 = new ArrayList<String>();
		ArrayList<String> list3 = new ArrayList<String>();
		ArrayList<String> list4 = new ArrayList<String>();
		/*SearchElements sr = new SearchElements();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		//Discussion dis2 = new Discussion();
		LoginTest lg = new LoginTest();*/
		
		lg.Login("Login2");
		
		dis.EditDiscussion("Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list = toolD.navSearch("Stay", "DiscLabelDockedEdit", "Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list2 = toolD.navSearch("Activity", "DiscLabelDockedEdit", "Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list3 = toolD.navSearch("Sub", "DiscLabelDockedEdit", "Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list4 = toolD.navSearchFloat("DiscLabelFloatEdit");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		sr.Validate(list, list2, list3, list4);
	}
	
	//@Test(priority = 2)
	public void DiscussionStory3()
	{
		/*DiscussionTest dis = new DiscussionTest();
		//Discussion dis2 = new Discussion();
		LoginTest lg = new LoginTest();*/
		
		lg.Login("Login2");
		
		dis.DeleteDiscussion("Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//@Test(priority = 3)
	public void DiscussionStory4()
	{
		
		lg.Login("Login2");
		
		dis.AddDiscussionShows("Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//@Test(priority = 4)
	public void DiscussionStory5() {
			
		lg.Login("Login2");
			
		dis.EditDiscussionShows("Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Missing Locator for Discussion
	//@Test(priority = 5)
	public void DiscussionStory6() {
		
		lg.Login("Login2");
		
		dis.DeleteDiscussionShows("DeleteDiscussionShows2","Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//@Test
	public void CollaborateStory() throws Exception
	{
		
		lg.Login("Login2");
//		
		grp.CollaborateAddGroup();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		grp.CollaborateAddGroup2();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		grp.CollaborateEditGroupShows();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		grp.CollaborateEditGroup();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		grp.CollaborateDeleteGroup();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		grp.CollaborateDeleteGroupShows("DeleteGroupShows");
		
		/*try {
			Thread.sleep(500000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	//@Test(priority = 4)
	public void CollaborateStory2() throws Exception
	{
		ArrayList<String> listM = new ArrayList<String>();
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> list2 = new ArrayList<String>();
		ArrayList<String> list3 = new ArrayList<String>();
		ArrayList<String> list4 = new ArrayList<String>();
		
		lg.Login("Login");
		
		grp.CollaborateAddGroup();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list = toolD.navSearch("Stay", "CollaborateLabelAdd", "Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*list2 = toolD.navSearch("Groups", "SocialHubGroupLabel", "");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		nt.refreshPage();
		
		mi.fetchAppTool("volute-app-group-face1 paper-listbox[id='groupListbox']");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		list2 = toolD.navSearch("Stay", "CollaborateLabelAdd", "Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		grp.CollaborateEditGroup();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list3 = toolD.navSearch("Stay", "CollaborateLabelEdit", "Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//listM.addAll(list4);
		
		/*list4 = toolD.navSearch("Groups", "SocialHubEditGroupLabel", "");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		listM.addAll(list4);*/
		
		nt.refreshPage();
		
		mi.fetchAppTool("volute-app-group-face1 paper-listbox[id='groupListbox']");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		list4 = toolD.navSearch("Stay", "CollaborateLabelEdit", "Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//listM.addAll(list4);
		
		sr.Validate(list, list2, list3, list4);
		
	}
	
	//@Test(priority = 5)
	public void CollaborateStory3()
	{
		NavigatorTool nt = new NavigatorTool();
		
		lg.Login("Login");
		
		nt.clickOnCollaborateIcon();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		grp.CollaborateDeleteGroupShows("DeleteGroupShows2");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	//@Test
	public void SocialHubStory()
	{
		
		sh.SocialHubSelectProgramShows();
		
		sh.SocialUserNameShows();
		
		sh.SocialLabelShows();
		
		//lg.Login();
		
		sh.SocialVisitedActSub();
		
		lg.Logout();
		
		sh.SocialHubEmptyMsg();
		
	}
	
	//@Test
	public void SocialHubStory2()
	{
		//SocialHubTest sh = new SocialHubTest();
		
		sh.SocialHubVerifyPage();
	}
	
	//@Test
	public void PrivateMStory() throws Exception
	{
		
		lg.Login("Login");
		
		//sh.SocialHubChangeProgram("SocialHubProgChange2");
		
		pm.modifyToolPermission("checkToolMpOn");
		
		lg.Logout();
		
		lg.Login("Login3");
		
		//sh.SocialHubChangeProgram("SocialHubProgChange2");
		
		pgm.CheckToolDesign("CheckToolbox2","Facilitator");
		
		lg.Logout();
		
	}
	// Missing Locator for slides
	
	@Test(priority = 5)
	public void SlidesStoryFacil() throws Exception{
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> list2 = new ArrayList<String>();
		ArrayList<String> list3 = new ArrayList<String>();
		ArrayList<String> list4 = new ArrayList<String>();
		ArrayList<String> list5 = new ArrayList<String>();
		ArrayList<String> list6 = new ArrayList<String>();
		
		lg.Login("Login");
		
		slid.SlidesAdd("Facilitator");
		
		list = toolD.navSlides("Manage Face", "", "Slides", "SlidesLabelManageFaceFacil", "Facilitator");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list2 = toolD.navSlides("List Face", "Slides", "1. File1", "SlidesLabelFaceListFacil", "Facilitator");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list3 = toolD.navSlides("Face", "Slides", "File1", "SlidesLabelFaceFacil", "Facilitator");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/**First part Navigate to Act/Manage */		
		toolD.navSlides("Activity", "Slides", "", "", "Facilitator");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list4 = toolD.navSlides("Manage Face", "", "Slides", "SlidesLabelManageFaceFacil", "Facilitator");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list5 = toolD.navSlides("List Face", "Slides", "1. File1", "SlidesLabelFaceListFacil", "Facilitator");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list6 = toolD.navSlides("Face", "Slides", "File1", "SlidesLabelFaceFacil", "Facilitator");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		sr.ValidateTools(list, list2, list3, list4, list5, list6);
		
	}
	
	//@Test(priority = 6)
	public void SlidesStoryFacil2() throws Exception {
			ArrayList<String> list = new ArrayList<String>();
			ArrayList<String> list2 = new ArrayList<String>();
			ArrayList<String> list3 = new ArrayList<String>();
			ArrayList<String> list4 = new ArrayList<String>();
			ArrayList<String> list5 = new ArrayList<String>();
			ArrayList<String> list6 = new ArrayList<String>();
			
			lg.Login("Login");
			
			slid.SlidesEdit("Facilitator");
				
				list = toolD.navSlides("Manage Face", "", "Slides", "SlidesLabelManageFaceEditFacil", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list2 = toolD.navSlides("List Face", "Slides", "3. Test..Case�Software", "SlidesLabelFaceListEdit", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list3 = toolD.navSlides("Face", "Slides", "Test..Case�Software", "SlidesLabelFaceEditFacil", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				/**First part Navigate to Act/Manage */		
				toolD.navSlides("Activity", "Slides", "", "", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list4 = toolD.navSlides("Manage Face", "", "Slides", "SlidesLabelManageFaceEditFacil2", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list5 = toolD.navSlides("List Face", "Slides", "1. STORYT~ELLING_P!reethiNairS3y[lla]bus", "SlidesLabelFaceListEdit2", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list6 = toolD.navSlides("Face", "Slides", "STORYT~ELLING_P!reethiNairS3y[lla]bus", "SlidesLabelFaceEditFacil2", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				sr.ValidateTools(list, list2, list3, list4, list5, list6);
			
		}
	
	//@Test(priority = 7)
	public void SlidesStoryFacil3(){
			
			lg.Login("Login");
			
			slid.SlidesDelete("Facilitator");
			
		}
	
	//@Test(priority = 0)
	public void SlidesStory() throws Exception{
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> list2 = new ArrayList<String>();
		ArrayList<String> list3 = new ArrayList<String>();
		ArrayList<String> list4 = new ArrayList<String>();
		ArrayList<String> list5 = new ArrayList<String>();
		ArrayList<String> list6 = new ArrayList<String>();
		
		lg.Login("Login2");
		
		slid.SlidesAdd("Participant");
		
		list = toolD.navSlides("Manage Face", "", "Slides", "SlidesLabelManageFace", "Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list2 = toolD.navSlides("List Face", "Slides", "1. File1", "SlidesLabelFaceList", "Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list3 = toolD.navSlides("Face", "Slides", "File1", "SlidesLabelFace", "Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/**First part Navigate to Act/Manage */		
		toolD.navSlides("Activity", "Slides", "", "", "Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list4 = toolD.navSlides("Manage Face", "", "Slides", "SlidesLabelManageFace2", "Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list5 = toolD.navSlides("List Face", "Slides", "2. File2", "SlidesLabelFaceList2", "Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list6 = toolD.navSlides("Face", "Slides", "File2", "SlidesLabelFace2", "Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		sr.ValidateTools(list, list2, list3, list4, list5, list6);
		
	}
	
	//@Test(priority = 1)
	public void SlidesStory2() throws Exception {
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> list2 = new ArrayList<String>();
		ArrayList<String> list3 = new ArrayList<String>();
		ArrayList<String> list4 = new ArrayList<String>();
		ArrayList<String> list5 = new ArrayList<String>();
		ArrayList<String> list6 = new ArrayList<String>();
		
		lg.Login("Login2");
		
		slid.SlidesEdit("Participant");
		
			list = toolD.navSlides("Manage Face", "", "Slides", "SlidesLabelManageFaceEdit", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list2 = toolD.navSlides("List Face", "Slides", "3. Test..Case�Software", "SlidesLabelFaceListEdit", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list3 = toolD.navSlides("Face", "Slides", "Test..Case�Software", "SlidesLabelFaceEdit", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			/**First part Navigate to Act/Manage */		
			toolD.navSlides("Activity", "Slides", "", "", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list4 = toolD.navSlides("Manage Face", "", "Slides", "SlidesLabelManageFaceEdit2", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list5 = toolD.navSlides("List Face", "Slides", "1. STORYT~ELLING_P!reethiNairS3y[lla]bus", "SlidesLabelFaceListEdit2", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list6 = toolD.navSlides("Face", "Slides", "STORYT~ELLING_P!reethiNairS3y[lla]bus", "SlidesLabelFaceEdit2", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			sr.ValidateTools(list, list2, list3, list4, list5, list6);
		
	}
	
	//@Test(priority = 2)
	public void SlidesStory3(){
		
		lg.Login("Login2");
	
		slid.SlidesDelete("Participant");
		
	}

	// Missing Locator for videos
	//@Test(priority = 0)
	public void VideoStoryFacil() throws Exception{
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> list2 = new ArrayList<String>();
		ArrayList<String> list3 = new ArrayList<String>();
		ArrayList<String> list4 = new ArrayList<String>();
		ArrayList<String> list5 = new ArrayList<String>();
		ArrayList<String> list6 = new ArrayList<String>();
		
		lg.Login("Login");
		
		vid.VideoAdd("Facilitator");
		
			list = toolD.navSlides("Manage Face", "", "Video", "VideoLabelManageFaceFacil", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list2 = toolD.navSlides("List Face", "Video", "Higher Education League", "VideoLabelFaceListFacil", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list3 = toolD.navSlides("Face", "Video", "", "VideoLabelFaceFacil", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			/**First part Navigate to Act/Manage */		
			toolD.navSlides("Activity", "Video", "", "", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list4 = toolD.navSlides("Manage Face", "", "Video", "VideoLabelManageFaceFacil", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list5 = toolD.navSlides("List Face", "Video", "Higher Education League", "VideoLabelFaceListFacil", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list6 = toolD.navSlides("Face", "Video", "", "VideoLabelFaceFacil", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			sr.ValidateTools(list, list2, list3, list4, list5, list6);
		
	}
	
	//@Test(priority = 1)
	public void VideoStoryFacil2(){
				ArrayList<String> list = new ArrayList<String>();
				ArrayList<String> list2 = new ArrayList<String>();
				ArrayList<String> list3 = new ArrayList<String>();
				ArrayList<String> list4 = new ArrayList<String>();
				ArrayList<String> list5 = new ArrayList<String>();
				ArrayList<String> list6 = new ArrayList<String>();
				
				lg.Login("Login");
				
				try {
					vid.VideoEdit("Facilitator");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list = toolD.navSlides("Manage Face", "", "Video", "VideoLabelEditManageFaceFacil", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list2 = toolD.navSlides("List Face", "Video", "New Version", "VideoLabelEditFaceListFacil", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list3 = toolD.navSlides("Face", "Video", "", "VideoLabelEditFaceFacil", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				/**First part Navigate to Act/Manage */		
				toolD.navSlides("Activity", "Video", "", "", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list4 = toolD.navSlides("Manage Face", "", "Video", "VideoLabelEditManageFaceFacil2", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list5 = toolD.navSlides("List Face", "Video", "2.", "VideoLabelEditFaceListFacil2", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list6 = toolD.navSlides("Face", "Video", "", "VideoLabelEditFaceFacil2", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				sr.ValidateTools(list, list2, list3, list4, list5, list6);
				
			}
	
	//@Test(priority = 2)
	public void VideoStoryFacil3(){
						
				lg.Login("Login");
				
				vid.VideoDelete("Facilitator");
						
			}
	
	//@Test(priority = 0)
	public void VideoStory() throws Exception{
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> list2 = new ArrayList<String>();
		ArrayList<String> list3 = new ArrayList<String>();
		ArrayList<String> list4 = new ArrayList<String>();
		ArrayList<String> list5 = new ArrayList<String>();
		ArrayList<String> list6 = new ArrayList<String>();
		
		lg.Login("Login2");
		
		vid.VideoAdd("Participant");
		
			list = toolD.navSlides("Manage Face", "", "Video", "VideoLabelManageFace", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list2 = toolD.navSlides("List Face", "Video", "Higher Education League", "VideoLabelFaceList", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list3 = toolD.navSlides("Face", "Video", "", "VideoLabelFace", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			/**First part Navigate to Act/Manage */		
			toolD.navSlides("Activity", "Video", "", "", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list4 = toolD.navSlides("Manage Face", "", "Video", "VideoLabelManageFace2", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list5 = toolD.navSlides("List Face", "Video", "Github Tutorial", "VideoLabelFaceList2", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list6 = toolD.navSlides("Face", "Video", "", "VideoLabelFace2", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			sr.ValidateTools(list, list2, list3, list4, list5, list6);
		
	}
	
	//@Test(priority = 1)
	public void VideoStory2(){
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> list2 = new ArrayList<String>();
		ArrayList<String> list3 = new ArrayList<String>();
		ArrayList<String> list4 = new ArrayList<String>();
		ArrayList<String> list5 = new ArrayList<String>();
		ArrayList<String> list6 = new ArrayList<String>();
		
		lg.Login("Login2");
		
		try {
			vid.VideoEdit("Participant");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list = toolD.navSlides("Manage Face", "", "Video", "VideoLabelEditManageFace", "Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list2 = toolD.navSlides("List Face", "Video", "New Version", "VideoLabelEditFaceList", "Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list3 = toolD.navSlides("Face", "Video", "", "VideoLabelEditFace", "Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/**First part Navigate to Act/Manage */		
		toolD.navSlides("Activity", "Video", "", "", "Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list4 = toolD.navSlides("Manage Face", "", "Video", "VideoLabelEditManageFace2", "Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list5 = toolD.navSlides("List Face", "Video", "Updated", "VideoLabelEditFaceList2", "Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list6 = toolD.navSlides("Face", "Video", "", "VideoLabelEditFace2", "Participant");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		sr.ValidateTools(list, list2, list3, list4, list5, list6);
		
	}
	
	//@Test(priority = 2)
	public void VideoStory3(){
				
		lg.Login("Login2");
		
		vid.VideoDelete("Participant");
				
	}
	
	//@Test(priority = 0)
	public void TaskStoryFacil() throws Exception{
			ArrayList<String> list = new ArrayList<String>();
			ArrayList<String> list2 = new ArrayList<String>();
			ArrayList<String> list3 = new ArrayList<String>();
			ArrayList<String> list4 = new ArrayList<String>();
			ArrayList<String> list5 = new ArrayList<String>();
			ArrayList<String> list6 = new ArrayList<String>();
			
			lg.Login("Login");
			
			ts.TasksAdd("Facilitator");
			
				list = toolD.navSlides("Face", "Tasks", "", "TasksFaceLabel", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list2 = toolD.navSlides("Manage Face", "", "Tasks", "TasksManageFaceLabel", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				/**First part Navigate to Act/Manage */		
				toolD.navSlides("Activity", "Tasks", "", "", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list3 = toolD.navSlides("Manage Face", "", "Tasks", "TasksManageFaceLabel", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list4 = toolD.navSlides("Face", "Tasks", "", "TasksFaceLabel", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				sr.ValidateTools(list, list2, list3, list4, list5, list6);
		}
	
	//@Test(priority = 1)
	public void TaskStorFacil2() throws Exception{
			ArrayList<String> list = new ArrayList<String>();
			ArrayList<String> list2 = new ArrayList<String>();
			ArrayList<String> list3 = new ArrayList<String>();
			ArrayList<String> list4 = new ArrayList<String>();
			ArrayList<String> list5 = new ArrayList<String>();
			ArrayList<String> list6 = new ArrayList<String>();
			
			lg.Login("Login");
			
			ts.TasksEdit("Facilitator");
				
				list = toolD.navSlides("Face", "Tasks", "", "TasksFaceLabel2", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list2 = toolD.navSlides("Manage Face", "", "Tasks", "TasksManageFaceLabel", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				/**First part Navigate to Act/Manage */		
				toolD.navSlides("Activity", "Tasks", "", "", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list3 = toolD.navSlides("Manage Face", "", "Tasks", "TasksManageFaceLabel", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list4 = toolD.navSlides("Face", "Tasks", "", "TasksFaceLabel2", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				sr.ValidateTools(list, list2, list3, list4, list5, list6);
		}
	
	//@Test(priority = 2)
	public void TaskStoryFacil3(){
				
			lg.Login("Login");
			
			ts.TaskStrike("Facilitator");
		}
	
	//@Test(priority = 3)
	public void TaskStoryFacil4(){
				
			lg.Login("Login");
		
			ts.TasksDelete("Facilitator");
		}
	
	//@Test(priority = 0)
	public void TaskStory() throws Exception{
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> list2 = new ArrayList<String>();
		ArrayList<String> list3 = new ArrayList<String>();
		ArrayList<String> list4 = new ArrayList<String>();
		ArrayList<String> list5 = new ArrayList<String>();
		ArrayList<String> list6 = new ArrayList<String>();
		
		lg.Login("Login2");
			
		ts.TasksAdd("Participant");
	
			list = toolD.navSlides("Face", "Tasks", "", "TasksFaceLabel", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list2 = toolD.navSlides("Manage Face", "", "Tasks", "TasksManageFaceLabel", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			/**First part Navigate to Act/Manage */		
			toolD.navSlides("Activity", "Tasks", "", "", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list3 = toolD.navSlides("Manage Face", "", "Tasks", "TasksManageFaceLabel", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list4 = toolD.navSlides("Face", "Tasks", "", "TasksFaceLabel", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			sr.ValidateTools(list, list2, list3, list4, list5, list6);
	}
	
	//@Test(priority = 1)
	public void TaskStory2() throws Exception{
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> list2 = new ArrayList<String>();
		ArrayList<String> list3 = new ArrayList<String>();
		ArrayList<String> list4 = new ArrayList<String>();
		ArrayList<String> list5 = new ArrayList<String>();
		ArrayList<String> list6 = new ArrayList<String>();
		
		lg.Login("Login2");
		
		ts.TasksEdit("Participant");
		
			list = toolD.navSlides("Face", "Tasks", "", "TasksFaceLabel2", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list2 = toolD.navSlides("Manage Face", "", "Tasks", "TasksManageFaceLabel", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			/**First part Navigate to Act/Manage */		
			toolD.navSlides("Activity", "Tasks", "", "", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list3 = toolD.navSlides("Manage Face", "", "Tasks", "TasksManageFaceLabel", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list4 = toolD.navSlides("Face", "Tasks", "", "TasksFaceLabel2", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			sr.ValidateTools(list, list2, list3, list4, list5, list6);
	}
	
	//@Test(priority = 3)
	public void TaskStory3(){
			
		lg.Login("Login2");
			
		ts.TaskStrike("Participant");
	}
	
	//@Test(priority = 4)
	public void TaskStory4(){
			
		lg.Login("Login2");
		
		ts.TasksDelete("Participant");
	}
	
	//@Test(priority = 8)
	public void RTEStoryFacil(){
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> list2 = new ArrayList<String>();
		ArrayList<String> list3 = new ArrayList<String>();
		ArrayList<String> list4 = new ArrayList<String>();
		ArrayList<String> list5 = new ArrayList<String>();
		ArrayList<String> list6 = new ArrayList<String>();
				
		lg.Login("Login");
			
		ric.RichTextAdd("Facilitator");
		
		list = toolD.navSlides("Manage Face", "", "Rich Text Editor", "RichManageFaceLabel", "Facilitator");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list2 = toolD.navSlides("Stay", "", "", "RichFaceLabel", "Facilitator");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/**First part Navigate to Act/Manage */		
		toolD.navSlides("Activity", "Rich Text Editor", "", "", "Facilitator");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list3 = toolD.navSlides("Manage Face", "", "Rich Text Editor", "RichManageFaceLabel", "Facilitator");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list4 = toolD.navSlides("Stay", "", "", "RichFaceLabel", "Facilitator");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		sr.ValidateTools(list, list2, list3, list4, list5, list6);
	}
	
	//@Test
	public void RTEStoryFacil2(){
					
		lg.Login("Login");
				
		ric.RichTextClear("Facilitator");
	}
	
	//@Test
	public void RTEStory(){
				
		lg.Login("Login2");
			
		ric.RichTextAdd("Participant");
	}
	
	//@Test(priority = 0)
	public void ReaderStoryFacil() throws Exception{
			
			lg.Login("Login");
			
				red.ReaderAdd("Facilitator");
				
				red.ReaderContent("Facilitator", "CheckReaderFacil");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	
	//@Test(priority = 1)
	public void ReaderStoryFacil2() throws Exception{
				
			lg.Login("Login");
			
			red.ReaderEdit("Facilitator");
				
			red.ReaderContent("Facilitator", "CheckEditReaderFacil");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
	//@Test(priority = 2)
	public void ReaderStoryFacil3(){
					
		lg.Login("Login");
		
		red.ReaderDelete("Facilitator");
					
	}
	
	// Missing Locator for reader
	//@Test(priority = 0)
	public void ReaderStory() throws Exception{
		
		lg.Login("Login2");
		
			red.ReaderAdd("Participant");
			
			red.ReaderContent("Participant", "CheckReader");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	//@Test(priority = 1)
	public void ReaderStory2() throws Exception{
			
		lg.Login("Login2");
		
		red.ReaderEdit("Participant");
			
		red.ReaderContent("Participant", "CheckEditReader");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//@Test(priority = 2)
	public void ReaderStory3(){
				
		lg.Login("Login2");
		
		red.ReaderDelete("Participant");
				
	}
	
	//@Test(priority = 0)
	public void ProfileListStoryFacil() throws Exception{
			ArrayList<String> list = new ArrayList<String>();
			ArrayList<String> list2 = new ArrayList<String>();
			ArrayList<String> list3 = new ArrayList<String>();
			ArrayList<String> list4 = new ArrayList<String>();
			ArrayList<String> list5 = new ArrayList<String>();
			ArrayList<String> list6 = new ArrayList<String>();
						
			lg.Login("Login");
					
			pl.ProfileListAddIndiv("Facilitator");
				
				list = toolD.navSlides("Manage Face", "", "Profiles", "PLLabelManageFaceFacil", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list2 = toolD.navSlides("List Face", "Profiles", "Sammy Abreu", "PLLabelFaceListFacil", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list3 = toolD.navSlides("Face", "Profiles", "", "PLLabelFace2", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				/**First part Navigate to Act/Manage */		
				toolD.navSlides("Activity", "Profiles", "", "", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list4 = toolD.navSlides("Manage Face", "", "Profiles", "PLLabelManageFaceFacil", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list5 = toolD.navSlides("List Face", "Profiles", "Sammy Abreu", "PLLabelFaceListFacil", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list6 = toolD.navSlides("Face", "Profiles", "", "PLLabelFace2", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				sr.ValidateTools(list, list2, list3, list4, list5, list6);		
		}
	
	//@Test(priority = 1)
	public void ProfileListStoryFacil2() throws Exception{
						
			lg.Login("Login");
	
			pl.ProfileListDeletIndv("Facilitator");
						
		}
	
	//@Test(priority = 2)
	public void ProfileListStoryFacil3() throws Exception{
							
			lg.Login("Login");
			
			pl.ProfileListSearchIndv("Facilitator");
							
	}
		
	//@Test(priority = 0)
	public void ProfileListStory() throws Exception{
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> list2 = new ArrayList<String>();
		ArrayList<String> list3 = new ArrayList<String>();
		ArrayList<String> list4 = new ArrayList<String>();
		ArrayList<String> list5 = new ArrayList<String>();
		ArrayList<String> list6 = new ArrayList<String>();
					
		lg.Login("Login2");
		
		pl.ProfileListAddIndiv("Participant");
			
			list = toolD.navSlides("Manage Face", "", "Profile List", "PLLabelManageFace", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list2 = toolD.navSlides("List Face", "Profile List", "Raul Jimenez", "PLLabelFaceList", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list3 = toolD.navSlides("Face", "Profile List", "", "PLLabelFace", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			/**First part Navigate to Act/Manage */		
			toolD.navSlides("Activity", "Profile List", "", "", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list4 = toolD.navSlides("Manage Face", "", "Profile List", "PLLabelManageFace2", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list5 = toolD.navSlides("List Face", "Profile List", "User temps02", "PLLabelFaceList2", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list6 = toolD.navSlides("Face", "Profile List", "", "PLLabelFace2", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			sr.ValidateTools(list, list2, list3, list4, list5, list6);		
	}
	
	//@Test(priority = 1)
	public void ProfileListStory2() throws Exception{
					
		lg.Login("Login2");
	
		pl.ProfileListDeletIndv("Participant");
					
	}
	
	//@Test(priority = 2)
	public void DirectoryStoryFacil() throws Exception{
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> list2 = new ArrayList<String>();
		ArrayList<String> list3 = new ArrayList<String>();
		ArrayList<String> list4 = new ArrayList<String>();
		ArrayList<String> list5 = new ArrayList<String>();
		ArrayList<String> list6 = new ArrayList<String>();
						
		lg.Login("Login");
		
		dir.DirectoryAddUser("Facilitator");
			
			list = toolD.navSlides("Manage Face", "", "Directory", "DrLabelManageFace", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list2 = toolD.navSlides("List Face", "Directory", "", "DrLabelFace", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			/**First part Navigate to Act/Manage */	
			toolD.navSlides("Activity", "Directory", "", "", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list4 = toolD.navSlides("Manage Face", "", "Directory", "DrLabelManageFace", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list5 = toolD.navSlides("List Face", "Directory", "", "DrLabelFace", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			sr.ValidateTools(list, list2, list3, list4, list5, list6);	
	}
	
	//@Test(priority = 3)
	public void DirectoryStoryFacil2() throws Exception{
							
			lg.Login("Login");
					
			dir.DirectoryRemoveUser("Facilitator");
							
		}
	
	//@Test(priority = 4)
	public void DirectoryStoryFacil3() throws Exception{
							
			lg.Login("Login");
			
			dir.DirectorySearchIndiv("Facilitator");
							
	}
	
	//@Test(priority = 0)
	public void DirectoryStory() throws Exception{
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> list2 = new ArrayList<String>();
		ArrayList<String> list3 = new ArrayList<String>();
		ArrayList<String> list4 = new ArrayList<String>();
		ArrayList<String> list5 = new ArrayList<String>();
		ArrayList<String> list6 = new ArrayList<String>();
						
		lg.Login("Login2");
						
		dir.DirectoryAddUser("Participant");
						
	}
	
	//@Test(priority = 1)
	public void DirectoryStory2() throws Exception{
						
		lg.Login("Login2");
		
		dir.DirectoryRemoveUser("Participant");
						
	}

	//@Test(priority = 1)
	public void FileStoryFacil() throws Exception{
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> list2 = new ArrayList<String>();
		ArrayList<String> list3 = new ArrayList<String>();
		ArrayList<String> list4 = new ArrayList<String>();
		ArrayList<String> list5 = new ArrayList<String>();
		ArrayList<String> list6 = new ArrayList<String>();
		
		lg.Login("Login");
		
		con.FileAdd("Facilitator");
			
			list = toolD.navSlides("Manage Face", "", "Files", "FlLabelManageFace", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list2 = toolD.navSlides("List Face", "Files", "", "FlLabelFace", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			/**First part Navigate to Act/Manage */	
			toolD.navSlides("Activity", "Files", "", "", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list4 = toolD.navSlides("Manage Face", "", "Files", "FlLabelManageFace", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list5 = toolD.navSlides("List Face", "Files", "", "FlLabelFace", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			sr.ValidateTools(list, list2, list3, list4, list5, list6);
	}
	
	//@Test(priority = 2)
	public void FileStoryFacil2() throws Exception{
			
			lg.Login("Login");
			
			con.FileDelete("Facilitator");
			
		}
	
	//@Test(priority = 0)
	public void FileStory() throws Exception{
		
		lg.Login("Login2");
	
		con.FileAdd("Participant");
		
	}
	
	//@Test(priority = 1)
	public void FileStory2() throws Exception{
		
		lg.Login("Login2");
	
		con.FileDelete("Participant");
		
	}
	
	//@Test(priority = 13)
	public void PortalStory(){
						
		lg.Login("Login");
		
		por.PortalAdd("Facilitator");
						
	}
	
	//@Test(priority = 14)
	public void PortalStory1(){
							
		lg.Login("Login");
			
		por.PortalClear("Facilitator");
							
	}	
	
	//@Test(priority = 14)
	public void BiographyStoryFacil() throws Exception{
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> list2 = new ArrayList<String>();
		ArrayList<String> list3 = new ArrayList<String>();
		ArrayList<String> list4 = new ArrayList<String>();
		ArrayList<String> list5 = new ArrayList<String>();
		ArrayList<String> list6 = new ArrayList<String>();
						
		lg.Login("Login");
		
		bio.BioAdd("Facilitator");
		
			list = toolD.navSlides("Manage Face", "", "Bio", "BioLabelManageFace", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list2 = toolD.navSlides("List Face", "Bio", "", "BioLabelFace", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			/**First part Navigate to Act/Manage */	
			toolD.navSlides("Activity", "Bio", "", "", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list4 = toolD.navSlides("Manage Face", "", "Bio", "BioLabelManageFace", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list5 = toolD.navSlides("List Face", "Bio", "", "BioLabelFace", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			sr.ValidateTools(list, list2, list3, list4, list5, list6);
						
	}
	
	//@Test
	public void BiographyStory() throws Exception{
						
		lg.Login("Login2");
		
		bio.BioAdd("Participant");
						
	}
	
	//@Test(priority = 1)
	public void BiographyStory2(){
		
		lg.Login("Login");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		sh.SocialGoToActSub("POtestactivity1","","Facilitator");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		bio.BioContent();
			
		sh.SocialGoToActSub("Activity for Sub activities","SASOtest1","Facilitator");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
	}

	//@Test
	public void ProfileStory() throws Exception{
		
			pro.ProfileAdd();
							
	}
	
	//@Test(priority = 15)
	public void CalendarStoryFacil() throws Exception{
			ArrayList<String> list = new ArrayList<String>();
			ArrayList<String> list2 = new ArrayList<String>();
			ArrayList<String> list3 = new ArrayList<String>();
			ArrayList<String> list4 = new ArrayList<String>();
			ArrayList<String> list5 = new ArrayList<String>();
			ArrayList<String> list6 = new ArrayList<String>();
			SearchElements sr = new SearchElements();
			
			lg.Login("Login");
			
			cal.CalendarAdd("Facilitator");
				
				list = toolD.navSlides("Manage Face", "", "Calendar", "CalendarLabelManageFace", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list2 = toolD.navSlides("List Face", "Calendar", "First Event", "CalendarLabelListFace", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list3 = toolD.navSlides("Face", "Calendar", "First Event", "CalendarLabelFace", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				/**First part Navigate to Act/Manage */		
				toolD.navSlides("Activity", "Calendar", "", "", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list4 = toolD.navSlides("Manage Face", "", "Calendar", "CalendarLabelManageFace", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list5 = toolD.navSlides("List Face", "Calendar", "First Event", "CalendarLabelListFace", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list6 = toolD.navSlides("Face", "Calendar", "", "CalendarLabelFace", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				sr.ValidateTools(list, list2, list3, list4, list5, list6);			
		}
	
	//@Test(priority = 16)
	public void CalendarStoryFacil2() throws Exception{
			ArrayList<String> list = new ArrayList<String>();
			ArrayList<String> list2 = new ArrayList<String>();
			ArrayList<String> list3 = new ArrayList<String>();
			ArrayList<String> list4 = new ArrayList<String>();
			ArrayList<String> list5 = new ArrayList<String>();
			ArrayList<String> list6 = new ArrayList<String>();
				
			lg.Login("Login");
				
			cal.CalendarEdit("Facilitator");
			
				list = toolD.navSlides("Manage Face", "", "Calendar", "CalendarEditLabelManageFace", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list2 = toolD.navSlides("List Face", "Calendar", "Primer Enlace", "CalendarEditLabelListFace", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list3 = toolD.navSlides("Face", "Calendar", "Primer Enlace", "CalendarEditLabelFace", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				/**First part Navigate to Act/Manage */		
				toolD.navSlides("Activity", "Calendar", "", "", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list4 = toolD.navSlides("Manage Face", "", "Calendar", "CalendarEditLabelManageFace", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list5 = toolD.navSlides("List Face", "Calendar", "Primer Enlace", "CalendarEditLabelListFace", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list6 = toolD.navSlides("Face", "Calendar", "", "CalendarEditLabelFace", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				sr.ValidateTools(list, list2, list3, list4, list5, list6);				
		}
	
	//@Test(priority = 17)
	public void CalendarStoryFacil3() throws Exception{
				
			lg.Login("Login");
	
			cal.CalendarDelete("Facilitator");
		}
	
	//@Test(priority = 0)
	public void CalendarStory() throws Exception{
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> list2 = new ArrayList<String>();
		ArrayList<String> list3 = new ArrayList<String>();
		ArrayList<String> list4 = new ArrayList<String>();
		ArrayList<String> list5 = new ArrayList<String>();
		ArrayList<String> list6 = new ArrayList<String>();
		
		lg.Login("Login2");
		
		cal.CalendarAdd("Participant");
			
			list = toolD.navSlides("Manage Face", "", "Calendar", "CalendarLabelManageFace", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list2 = toolD.navSlides("List Face", "Calendar", "First Event", "CalendarLabelListFace", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list3 = toolD.navSlides("Face", "Calendar", "First Event", "CalendarLabelFace", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			/**First part Navigate to Act/Manage */		
			toolD.navSlides("Activity", "Calendar", "", "", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list4 = toolD.navSlides("Manage Face", "", "Calendar", "CalendarLabelManageFace", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list5 = toolD.navSlides("List Face", "Calendar", "First Event", "CalendarLabelListFace", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list6 = toolD.navSlides("Face", "Calendar", "", "CalendarLabelFace", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			sr.ValidateTools(list, list2, list3, list4, list5, list6);			
	}
	
	//@Test(priority = 1)
	public void CalendarStory2() throws Exception{
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> list2 = new ArrayList<String>();
		ArrayList<String> list3 = new ArrayList<String>();
		ArrayList<String> list4 = new ArrayList<String>();
		ArrayList<String> list5 = new ArrayList<String>();
		ArrayList<String> list6 = new ArrayList<String>();
			
		lg.Login("Login2");
			
		cal.CalendarEdit("Participant");
		
			list = toolD.navSlides("Manage Face", "", "Calendar", "CalendarEditLabelManageFace", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list2 = toolD.navSlides("List Face", "Calendar", "Primer Enlace", "CalendarEditLabelListFace", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list3 = toolD.navSlides("Face", "Calendar", "Primer Enlace", "CalendarEditLabelFace", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			/**First part Navigate to Act/Manage */		
			toolD.navSlides("Activity", "Calendar", "", "", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list4 = toolD.navSlides("Manage Face", "", "Calendar", "CalendarEditLabelManageFace", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list5 = toolD.navSlides("List Face", "Calendar", "Primer Enlace", "CalendarEditLabelListFace", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list6 = toolD.navSlides("Face", "Calendar", "", "CalendarEditLabelFace", "Participant");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			sr.ValidateTools(list, list2, list3, list4, list5, list6);				
	}
	
	//@Test(priority = 2)
	public void CalendarStory3() throws Exception{
			
		lg.Login("Login2");
		
		cal.CalendarDelete("Participant");
	}
	
	//@Test(priority = 16)
	public void AnnouncementsStoryFacil() throws Exception{
						
			lg.Login("Login");
				
			an.AnAdd("Facilitator");
				
			an.AnCheckContent("CheckAnnouncementFacil","Facilitator");
										
		}
	
	//@Test(priority = 17)
	public void AnnouncementsStoryFacil2() throws Exception{
							
			lg.Login("Login");
							
			an.AnEdit("Facilitator");
				
			an.AnCheckContent("CheckAnnouncementEditFacil","Facilitator");
											
		}
	
	//@Test(priority = 18)
	public void AnnouncementsStoryFacil3() {
								
			lg.Login("Login");
								
			an.AnDelete("Facilitator");
												
		}
	
	//@Test(priority = 0)
	public void AnnouncementsStory() throws Exception{
					
		lg.Login("Login2");
					
		an.AnAdd("Participant");
			
		an.AnCheckContent("CheckAnnouncement","Participant");
									
	}

	//@Test(priority = 1)
	public void AnnouncementsStory2() throws Exception{
						
		lg.Login("Login2");
						
		an.AnEdit("Participant");
			
		an.AnCheckContent("CheckAnnouncementEdit","Participant");
										
	}

	//@Test(priority = 2)
	public void AnnouncementsStory3() {
							
		lg.Login("Login2");
							
		an.AnDelete("Participant");
											
	}

	//@Test(priority = 1) //17
	public void PassportStoryFacil() throws Exception {
			ArrayList<String> listM = new ArrayList<String>();
			ArrayList<String> list = new ArrayList<String>();
			ArrayList<String> list2 = new ArrayList<String>();
			ArrayList<String> list3 = new ArrayList<String>();
			ArrayList<String> list4 = new ArrayList<String>();
			ArrayList<String> list5 = new ArrayList<String>();
			ArrayList<String> list6 = new ArrayList<String>();
									
			lg.Login("Login");
									
			
			pas.PassportAdd("Facilitator");
			
				list = toolD.navSlides("Manage Face", "", "Guidebook", "GuideManageFaceLabel", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list2 = toolD.navSlides("List Face", "Guidebook", "The Moon", "GuideListFaceLabel", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list3 = toolD.navSlides("List Face", "Guidebook", "Flames", "GuideFaceLabel", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list4 = toolD.navSlides("List Face", "Guidebook", "United Nation", "GuideFaceLabel1", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list5 = toolD.navSlides("List Face", "Guidebook", "Electric", "GuideFaceLabel2", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list6 = toolD.navSlides("List Face", "Guidebook", "The Moon", "GuideFaceLabel3", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				listM.addAll(list6);
				
				/**First part Navigate to Act/Manage */		
				toolD.navSlides("Activity", "Guidebook", "", "", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list6 = toolD.navSlides("Manage Face", "", "Guidebook", "GuideManageFaceLabel", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				listM.addAll(list6);
				
				list6 = toolD.navSlides("List Face", "Guidebook", "The Moon", "GuideListFaceLabel", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				listM.addAll(list6);
				
				list6 = toolD.navSlides("List Face", "Guidebook", "Flames", "GuideFaceLabel", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				listM.addAll(list6);
				
				list6 = toolD.navSlides("List Face", "Guidebook", "United Nation", "GuideFaceLabel1", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				listM.addAll(list6);
				
				list6 = toolD.navSlides("List Face", "Guidebook", "Electric", "GuideFaceLabel2", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				listM.addAll(list6);
				
				list6 = toolD.navSlides("List Face", "Guidebook", "The Moon", "GuideFaceLabel3", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				listM.addAll(list6);
				
				sr.ValidateTools(list, list2, list3, list4, list5, list6);	
				
		}
	
	//@Test(priority = 2)
	public void PassportStoryFacil2() throws Exception {
			ArrayList<String> listM = new ArrayList<String>();
			ArrayList<String> list = new ArrayList<String>();
			ArrayList<String> list2 = new ArrayList<String>();
			ArrayList<String> list3 = new ArrayList<String>();
			ArrayList<String> list4 = new ArrayList<String>();
			ArrayList<String> list5 = new ArrayList<String>();
			ArrayList<String> list6 = new ArrayList<String>();
									
			lg.Login("Login");
									
			//try {
				pas.PassportEdit("Facilitator");
			//} catch (Exception e) {
				// TODO Auto-generated catch block
			//	e.printStackTrace();
			//}
				list = toolD.navSlides("Manage Face", "", "Guidebook", "GuideManageFaceLabelEdit", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list2 = toolD.navSlides("List Face", "Guidebook", "Movie: The Train", "GuideListFaceLabelEdit", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list3 = toolD.navSlides("List Face", "Guidebook", "View: Sunrise", "GuideFaceLabelEdit", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list4 = toolD.navSlides("List Face", "Guidebook", "Excitement: The Beach", "GuideFaceLabelEdit1", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list5 = toolD.navSlides("List Face", "Guidebook", "Story: Memories", "GuideFaceLabelEdit2", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list6 = toolD.navSlides("List Face", "Guidebook", "Movie: The Train", "GuideFaceLabelEdit3", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				listM.addAll(list6);
				
				/**First part Navigate to Act/Manage */		
				toolD.navSlides("Activity", "Guidebook", "", "", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list6 = toolD.navSlides("Manage Face", "", "Guidebook", "GuideManageFaceLabelEdit", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				listM.addAll(list6);
				
				list6 = toolD.navSlides("List Face", "Guidebook", "Movie: The Train", "GuideListFaceLabelEdit", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				listM.addAll(list6);
				
				list6 = toolD.navSlides("List Face", "Guidebook", "View: Sunrise", "GuideFaceLabelEdit", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				listM.addAll(list6);
				
				list6 = toolD.navSlides("List Face", "Guidebook", "Excitement: The Beach", "GuideFaceLabelEdit1", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				listM.addAll(list6);
				
				list6 = toolD.navSlides("List Face", "Guidebook", "Story: Memories", "GuideFaceLabelEdit2", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				listM.addAll(list6);
				
				list6 = toolD.navSlides("List Face", "Guidebook", "Movie: The Train", "GuideFaceLabelEdit3", "Facilitator");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				listM.addAll(list6);
				
				sr.ValidateTools(list, list2, list3, list4, list5, list6);	
													
		}
	
	//@Test(priority = 2)
	public void PassportStoryFacil3() throws Exception{
										
			lg.Login("Login");
					
			pas.PassportTabs("Facilitator");
		}
	
	//@Test(priority = 3)
	public void PassportStoryFacil4(){
									
			lg.Login("Login");
				
			pas.PassportDelete("Facilitator");
		}
	
	//@Test
	public void PassportStory() throws Exception {
								
		lg.Login("Login2");
								
		pas.PassportAdd("Participant");								
	}
		
	//@Test(priority = 1)
	public void PassportStory2() throws Exception {
								
		lg.Login("Login2");
								
		//try {
			pas.PassportEdit("Participant");
		//} catch (Exception e) {
			// TODO Auto-generated catch block
		//	e.printStackTrace();
		//}
												
	}
		
	//@Test(priority = 2)
	public void PassportStory3() throws Exception{
									
		lg.Login("Login2");
				
		pas.PassportTabs("Participant");
	}
		
	//@Test(priority = 3)
	public void PassportStory4(){
								
		lg.Login("Login2");
			
		pas.PassportDelete("Participant");
	}
	
	//@Test(priority = 0) //18
	public void AssessmentStoryFacil(){
									
			lg.Login("Login");
				
			am.CheckToastError1and3("Facilitator");
		}
	
	//@Test(priority = 1)
	public void AssessmentStoryFacil2(){
									
			lg.Login("Login");
				
			am.CheckToastError1and2("Facilitator");
		}
	
	//@Test(priority = 2)
	public void AssessmentStoryFacil3(){
										
			lg.Login("Login");
					
			am.EnableCheckSurveyToastError("Facilitator");
		}
	
	//@Test(priority = 3)
	public void AssessmentStoryFacil4(){
										
			lg.Login("Login");
					
			am.EnableTakeSurvey("Facilitator");
		}
	
	//@Test(priority = 0)
	public void AssessmentStory1(){
								
		lg.Login("Login2");
			
		am.CheckToastError1and3("Participant");
	}
		
	//@Test(priority = 1)
	public void AssessmentStory2(){
								
		lg.Login("Login2");
			
		am.CheckToastError1and2("Participant");
	}
		
	//@Test(priority = 2)
	public void AssessmentStory3(){
									
		lg.Login("Login2");
				
		am.EnableCheckSurveyToastError("Participant");
	}
		
	//@Test(priority = 3)
	public void AssessmentStory4(){
									
		lg.Login("Login2");
				
		am.EnableTakeSurvey("Participant");
	}
	
	//@Test(priority = 0)
	public void IdeaBoardStoryFacil() throws Exception{
									
		lg.Login("Login");
				
		id.IdeaBoardErrorMessageCheck("Facilitator");
	}
	
	//@Test(priority = 1)
	public void IdeaBoardStoryFacil2() throws Exception{
			ArrayList<String> list = new ArrayList<String>();
			ArrayList<String> list2 = new ArrayList<String>();
			ArrayList<String> list3 = new ArrayList<String>();
			ArrayList<String> list4 = new ArrayList<String>();
			ArrayList<String> list5 = new ArrayList<String>();
			ArrayList<String> list6 = new ArrayList<String>();
										
			lg.Login("Login");
					
			id.AddIdea("Facilitator");
				
			// Here we comment
			list = toolD.navSlides("Face", "Idea Board", "Manage", "IBManageFaceLabel", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list2 = toolD.navSlides("List Face", "Idea Board", "My First Idea", "IBListFaceLabel", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list3 = toolD.navSlides("Face", "Idea Board2", "", "IBFaceLabel", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			/**First part Navigate to Act/Manage */	
			toolD.navSlides("Activity", "Idea Board", "", "", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list4 = toolD.navSlides("Manage Face", "", "Idea Board", "IBManageFaceLabel", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list5 = toolD.navSlides("List Face", "Idea Board", "My First Idea", "IBListFaceLabel", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			list6 = toolD.navSlides("Face", "Idea Board2", "", "IBFaceLabel", "Facilitator");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			sr.ValidateTools(list, list2, list3, list4, list5, list6);
		}
	
	//@Test(priority = 2)
	public void IdeaBoardStoryCommentFacil() throws Exception{
												
			lg.Login("Login");
							
			id.CommentReplyIdea("CommentReply","Facilitator");
		}
	
	//@Test(priority = 3)
	public void IdeaBoardStoryFacil3() throws Exception{
											
			lg.Login("Login");
						
			id.EditIdea("Facilitator");
				
			// Here we comment
			
		}
	
	//@Test(priority = 4)
	public void IdeaBoardStoryFacil4() throws Exception{
											
			lg.Login("Login");
						
			id.DeleteIdea("Facilitator");
		}
	
	//@Test(priority = 0)
	public void IdeaBoardStory1() throws Exception{
									
		lg.Login("Login2");
				
		id.IdeaBoardErrorMessageCheck("Participant");
	}
		
	//@Test(priority = 1)
	public void IdeaBoardStory2() throws Exception{
									
		lg.Login("Login2");
				
		id.AddIdea("Participant");
			
		// Here we comment
	}
		
	//@Test(priority = 2)
	public void IdeaBoardStoryComment1() throws Exception{
											
		lg.Login("Login2");
						
		id.CommentReplyIdea("CommentReply","Participant");
	}
		
	//@Test(priority = 3)
	public void IdeaBoardStory3() throws Exception{
										
		lg.Login("Login2");
					
		id.EditIdea("Participant");
			
		// Here we comment
	}
	
	//@Test(priority = 3)
	public void IdeaBoardStory4() throws Exception{
										
		lg.Login("Login2");
					
		id.DeleteIdea("Participant");
	}	//@Test(priority = 2)

	//@Test(priority = 0)
	public void PhotoshareStoryFacil() throws Exception{
										
			lg.Login("Login");
					
			ph.ErrorPhotoshareCheck("Facilitator");
	}
	
	//@Test(priority = 1)
	public void PhotoshareStoryFacil2() throws Exception{
											
			lg.Login("Login");
						
			ph.AddPhotoshare("Facilitator");
	}
	
	//@Test(priority = 2)
	public void PhotoshareStoryFacil3() throws Exception{
												
			lg.Login("Login");
							
			ph.EditPhotoshare("Facilitator");
	}
	
	//@Test(priority = 3)
	public void PhotoshareStoryFacil4() throws Exception{
												
			lg.Login("Login");
							
			ph.DeletePhoto("Facilitator");
	}
	
	
	//@Test(priority = 4)
	public void PhotoshareStoryFacil5() throws Exception{
												
			lg.Login("Login");
							
			ph.DeleteAlbum("Facilitator");
	}
	
	
	//--------------------------------------------------------
	// No Need to touch this tests, is still work in Progress.
	//--------------------------------------------------------
	//
	
	//@Test(priority = 0)
	public void ProgramStoryAdd()
	{
		try {
			ProgramStory();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
	}
	
	//@Test(priority = 1)
	public void ProgramStoryEdit()
	{
		//ProgramStory2();
	}
	
	//@Test(priority = 2)
	public void ProgramStoryDelete()
	{
		ProgramStory3();
	}
	
	//@Test(priority = 3)
	public void ProgramStoryDeleteAll()
	{
		ProgramStory4();
	}
	
	//@Test
	public void MainSlideStory1(){
		
		//SlidesStory();
		//DisplayNameStory();
	}
	
	//@Test
	public void MainSlideStory2(){
		
		//SlidesStory2();
		//DisplayNameStory();
	}
	
	//@Test
	public void MainSlideStory3(){
		
		SlidesStory3();
		//DisplayNameStory();
	}
}
