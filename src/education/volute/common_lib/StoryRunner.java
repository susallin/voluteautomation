package education.volute.common_lib;

import java.lang.reflect.*;
import java.util.Iterator;

import org.testng.annotations.Test;

import utilities.ReadCsvFiles;

public class StoryRunner extends Story {


	 @Test //public static void main(String args[])
	 public void StoryRunnerTest() throws Exception { // I would use a properties or collection or map

		 //System.out.println("hello");
    	 ReadCsvFiles csv = new ReadCsvFiles();
    	 //something here to read the file, and parse the list� (see props sample below)
    	 Iterator<String[]> FileName = csv.ReadList("Stories");
			
			if(FileName.hasNext())
				FileName.next();
			
			while(FileName.hasNext()){
				int n = 0;
				String[] Name = FileName.next();
				
				String methodName = Name[n];
				//String quit = Name[n+1];
				
				try {
			          Class<?> c = Class.forName("education.volute.common_lib.Story");
			          
			          Object str = c.newInstance(); 
			           //Class Storyx = Story.class;
			          
			          Method setNameMethod = str.getClass().getMethod(methodName);
			          setNameMethod.invoke(str);
			           // Method m[] = c.getDeclaredMethods();

			           /* for (int i = 0; i < m.length; i++) {

			                m[i].invoke(methodName);*/

			         }

			         catch (Throwable e) {

			            System.err.println(e);

			         }
			}

	      }
	 
	 //@Test(invocationCount = 2)
	 public void StoryRunnerTest(String className, String methodName) { // I would use a properties or collection or map
	
				try {
			          Class<?> c = Class.forName("education.volute.common_lib.Story");
			          
			          Object str = c.newInstance(); 
			           //Class Storyx = Story.class;
			          Method setNameMethod = str.getClass().getMethod(methodName);
			          setNameMethod.invoke(str);
			    }
			    catch (Throwable e) {
			    	System.err.println(e);
			    }
	}
	 
	 //@Test(invocationCount = 2)
	 public void methodName() throws Exception{
		 ReadCsvFiles csv = new ReadCsvFiles();
    	 //something here to read the file, and parse the list� (see props sample below)
    	 Iterator<String[]> FileName = csv.ReadList("Stories");
			
			if(FileName.hasNext())
				FileName.next();
			
			while(FileName.hasNext()){
				int n = 0;
				String[] Name = FileName.next();
				
				String className = Name[n];
				String methodName = Name[n+1];
				//String quit = Name[n+1];
				
				StoryRunnerTest(className,methodName);
				driver.quit();
			}
	 }
	      
}
