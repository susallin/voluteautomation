package utilities;

import org.testng.annotations.Test;

import education.volute.excel_utilities.ExcelUtils;

public class NavigateTo {

	@Test
	public void GoTo(String nav){
		
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_Biography, nav);
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
			
		String toolDest = ExcelUtils.getCellData(i, 1);
		String actName = ExcelUtils.getCellData(i, 2);
		String subActName = ExcelUtils.getCellData(i,3);
		String pgmNameTxt = ExcelUtils.getCellData(i, 4);
		String uRole = ExcelUtils.getCellData(i, 5);
		
		toolD.Destination(toolDest, actName, subActName, pgmNameTxt, uRole);
		}
	}
}
