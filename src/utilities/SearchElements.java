package utilities;

import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.Select;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Finder;
import org.sikuli.script.Image;
import org.sikuli.script.Match;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.testng.annotations.Test;
import java.util.*;
import java.util.concurrent.TimeUnit;

import education.volute.common_lib.SuperTestScript;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.login_testscripts.Login_TC_01;
//import education.volute.pages.LoginPage;
import education.volute.pages.NavigatorTool;
import education.volute.pages.Notes;
import education.volute.pages.Discussion;
import education.volute.pages.LoginPage;
import education.volute.pages.MenuItems;

public class SearchElements extends SuperTestScript {

	SoftAssertCheck soft = new SoftAssertCheck();
	Screen screen = new Screen();
	//SoftAssertCheck soft2 = new SoftAssertCheck();
	//SoftAssertCheck soft3 = new SoftAssertCheck();
	//SoftAssertCheck soft4 = new SoftAssertCheck();
	
	/*public void elementShows(String type) {
		List<WebElement> elements;
		Iterator<WebElement> el;
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_SearchLocators, type);
		
		// Here we get all elements for a particular component and list them in an arraylist<>
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
			String tscName = ExcelUtils.getCellData(i, 0);
			String searLoc = ExcelUtils.getCellData(i, 1);
	        String labelTxt = ExcelUtils.getCellData(i, 2);
	        // We search with some locator
	        elements = driver.findElements(By.xpath(searLoc));
	        el = elements.iterator();
		
			// We go through the loop for each WebElement.
			while(el.hasNext()){
				WebElement els = el.next();
			
				//System.out.println(els.getText());
				
				if(els.getText().contains(labelTxt)){
					soft.assertTrue(true, "Found");
					System.out.println("The test case passed on "+tscName+".");
					//break;
				}else{
					System.out.println("The test case failed on "+tscName+".");
					soft.assertTrue(false, "The test case failed on "+tscName+".\n");
				
			
				}
			}
		}
		//soft.assertAll();
	}
	
	
	public void elementShows2(String type) {
		List<WebElement> elements;
		Iterator<WebElement> el;
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_SearchLocators, type);
		
		// Here we get all elements for a particular component and list them in an arraylist<>
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
			String tscName = ExcelUtils.getCellData(i, 0);
			String searLoc = ExcelUtils.getCellData(i, 1);
	        String labelTxt = ExcelUtils.getCellData(i, 2);
	        // We search with some locator
	        elements = driver.findElements(By.xpath(searLoc));
	        el = elements.iterator();
		
			// We go through the loop for each WebElement.
			while(el.hasNext()){
				WebElement els = el.next();
			
				//System.out.println(els.getText());
				
				if(els.getText().contains(labelTxt)){
					soft2.assertTrue(true, "Found");
					System.out.println("The test case passed on "+tscName+".");
					//break;
				}else{
					System.out.println("The test case failed on "+tscName+".");
					soft2.assertTrue(false, "The test case failed on "+tscName+".\n");
				
			
				}
			}
		}
		//soft2.assertAll();
	}
	
	
	public void elementShows3(String type) {
		List<WebElement> elements;
		Iterator<WebElement> el;
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_SearchLocators, type);
		
		// Here we get all elements for a particular component and list them in an arraylist<>
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
			String tscName = ExcelUtils.getCellData(i, 0);
			String searLoc = ExcelUtils.getCellData(i, 1);
	        String labelTxt = ExcelUtils.getCellData(i, 2);
	        // We search with some locator
	        elements = driver.findElements(By.xpath(searLoc));
	        el = elements.iterator();
		
			// We go through the loop for each WebElement.
			while(el.hasNext()){
				WebElement els = el.next();
			
				//System.out.println(els.getText());
				
				if(els.getText().contains(labelTxt)){
					soft3.assertTrue(true, "Found");
					System.out.println("The test case passed on "+tscName+".");
					//break;
				}else{
					System.out.println("The test case failed on "+tscName+".");
					soft3.assertTrue(false, "The test case failed on "+tscName+".\n");
				
			
				}
			}
		}
		//soft3.assertAll();
	}*/
	
	
	public ArrayList<String> elementShows4(String type) {
		List<WebElement> elements;
		ArrayList<String> exp = new ArrayList<String>();
		Iterator<WebElement> el;
		String determine;
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_SearchLocators, type);
		
		
		// Here we get all elements for a particular component and list them in an arraylist<>
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
			String tscName = ExcelUtils.getCellData(i, 0);
			String searLoc = ExcelUtils.getCellData(i, 1);
	        String labelTxt = ExcelUtils.getCellData(i, 2);
	        // We search with some locator
	        String locShows = fetchValueShows(By.xpath(searLoc));
	        
	        if(locShows.equalsIgnoreCase("Found")){
	        	elements = driver.findElements(By.xpath(searLoc));
	        	el = elements.iterator();
	        
	        	// We go through the loop for each WebElement.
	        	while(el.hasNext()){
	        		WebElement els = el.next();
			
	        		//System.out.println(els.getText());
	        		if(els.getText().contains(labelTxt)){
	        			determine = "True";
	        			exp.add(determine);
	        			//soft4.assertTrue(true, "Found");
	        			System.out.println("The test case passed on "+tscName+".\n");
	        			//break;
	        		}else{
	        			determine = "False";
	        			exp.add(determine);
	        			System.out.println("The test case failed on "+tscName+".\n");
	        			//soft4.assertTrue(false, "The test case failed on "+tscName+".\n");
	        		}
				
	        	 }			
	           }else{
	        	   determine = "False";
       			   exp.add(determine);
       			   System.out.println("The test case failed on "+tscName+".\n");
	           }
	        }
		return exp;
		//soft4.assertAll();
	}
	
	public String fetchValueShows(By value)
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			SuperTestScript.driver.findElement(value);
			return "Found";
		}
		catch(NoSuchElementException e){
			return "Not Found";
		}
	}
	
	public String fetchImageShows(String imgName) throws FindFailed
	{
		SuperTestScript.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//String img = "./user_data/sikuli/"+imgName;
		Pattern imgexp = new Pattern("./user_data/sikuli/"+imgName);
		//Image.reset();
		//String imgexp = "./user_data/sikuli/"+imgName;
		//Match f = screen.exists(imgexp);
		if(screen.exists(imgexp) != null){ //screen.exists(new Pattern(imgexp).similar(0.8f)) //screen.exists(new Pattern(imgexp).similar(0.99f)) != null
			System.out.println("The test case passed on finding "+imgName+".\n");
			return "Found";
		}else{
			System.out.println("The test case failed on finding "+imgName+".\n");
			return "Not Found";
		}
	}
	
	public void Validate(ArrayList<String> list, ArrayList<String> list2, ArrayList<String> list3, ArrayList<String> list4){
		List<String> listFinal = new ArrayList<String>();
		Iterator<String> li;
		listFinal.addAll(list);
		listFinal.addAll(list2);
		listFinal.addAll(list3);
		listFinal.addAll(list4);
		li = listFinal.iterator();
		while(li.hasNext()){
			String lis = li.next();
			if(lis.contains("True")){
				soft.assertTrue(true, "Found");
				//System.out.println("The test case passed.");
			}else{
				soft.assertTrue(false, "The test case failed.\n");
				break;
			}
		}
		soft.assertAll();
	}

	public void ValidateTools(ArrayList<String> list, ArrayList<String> list2, ArrayList<String> list3, ArrayList<String> list4, ArrayList<String> list5, ArrayList<String> list6){
		List<String> listFinal = new ArrayList<String>();
		Iterator<String> li;
		listFinal.addAll(list);
		listFinal.addAll(list2);
		listFinal.addAll(list3);
		listFinal.addAll(list4);
		listFinal.addAll(list5);
		listFinal.addAll(list6);
		li = listFinal.iterator();
		while(li.hasNext()){
			String lis = li.next();
			if(lis.contains("True")){
				soft.assertTrue(true, "Found");
				//System.out.println("The test case passed.");
			}else{
				soft.assertTrue(false, "The test case failed.\n");
				break;
			}
		}
		soft.assertAll();
	}
	
	public void DoSearchElements(String sear){
		// Here we get all elements for a particular component and list them in an arraylist<>
		List<WebElement> elements = driver.findElements(By.xpath(sear));
		
		for(WebElement el:elements){
			if(!el.getAttribute("id").isEmpty()){
				// We print the attribute by ids when there is one.
				System.out.println( el.getTagName() + ": #" + el.getAttribute("id"));
				System.out.println();
			}
			// We print the attribute by names when there is one.
			if(el.getAttribute("name") != null && !el.getAttribute("name").isEmpty()){
				System.out.println( el.getTagName() + "[name='"+ el.getAttribute("name")+"']");
				System.out.println();
			}
			// We print the attribute by titles when there is one.
			if(el.getAttribute("title") != null && !el.getAttribute("title").isEmpty()){
				System.out.println( el.getTagName() + "[title='"+ el.getAttribute("title")+"']");
				System.out.println();
			}
			// We print the attribute by labels when there is one.
			if(el.getAttribute("label") != null && !el.getAttribute("label").isEmpty()){
				System.out.println( el.getTagName() + "[label='"+ el.getAttribute("label")+"']");
				System.out.println();
			}
		}
	}
	
	/*public void ToolComponent(String loctyp, String tname){
		NavigatorTool nt = new NavigatorTool();
		Notes not = new Notes();
		Discussion dis = new Discussion();
		MenuItems mi = new MenuItems();
		
		if(tname.equalsIgnoreCase("FlNotes")){
			nt.clickOnNotesIcon();
			DoSearchElements(loctyp);
			not.clickOnCloseNotes();
			mi.clickOnMainMenu();
			mi.clickOnLogout();
		} else if(tname.equalsIgnoreCase("FlDiscussion")){
			nt.clickOnDiscussionIcon();
			DoSearchElements(loctyp);
			dis.clickOnDisCloseflButton();
			mi.clickOnMainMenu();
			mi.clickOnLogout();
		//} else if(tname.equalsIgnoreCase("Discussion")){
			
		}
	}*/
	
	
	//@Test
	public void Component(){

		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_SearchLocators, "SearchLocator");
		//int i = 1;
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
			String uName = ExcelUtils.getCellData(i, 1);
			String pwd = ExcelUtils.getCellData(i, 2);
			String locatorType = ExcelUtils.getCellData(i, 3);
			String comName = ExcelUtils.getCellData(i, 4);
			
			LoginPage lp = new LoginPage();
			//Login_TC_01 lp = new Login_TC_01();
			
			// If there is no component, it will terminate program.
			if(comName.isEmpty()){
				break;
				
			// Checks to see if it needs to log in or not.
			} else if (comName.equalsIgnoreCase("Login Page")) {
					DoSearchElements(locatorType);
				}
				else { 
					//If locator is anything else
			        lp.login(uName, pwd); 
			        lp.logibutton();
			        try {
						Thread.sleep(16000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				 
					//ToolComponent(locatorType, comName);
				}
		 }
	}
}

	/*private static String getText() {
		// TODO Auto-generated method stub
		return null;
	}

	private static String getTagName() {
		// TODO Auto-generated method stub
		return null;
	}*/

