package utilities;

import java.util.Map;

import org.testng.annotations.Test;
import org.testng.asserts.Assertion;
import org.testng.asserts.IAssert;
import org.testng.asserts.SoftAssert;
import org.testng.collections.Maps;

import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;


public class SoftAssertCheck extends Assertion {

	private Map<AssertionError, IAssert> m_errors = Maps.newHashMap();

	/*@Test//(priority = 0)
	public void testSoftAssertWord()//String expRes)
	{
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManageUsers, "Sample");
		
		//for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		//{	
			//String words = ExcelUtils.getCellData(i, 1);
			
			SoftAssert assertion = new SoftAssert();
			System.out.println("softAssert Method Was Started");
			assertion.assertEquals(words, "Found","The test case failed.");
			System.out.println("softAssert Method Was Executed");
			System.out.println("The test case passed.");
			assertion.assertAll();
		//}
	}*/
	
	//@Test//(priority = 0)
	/*public void Word()//String expRes)
	{
		SoftAssert assertion = new SoftAssert();
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManageUsers, "Sample");
		
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
			String words = ExcelUtils.getCellData(i, 1);
			
			String actRes = "Found";//newEMailTxt);
			System.out.println(actRes);
			
			 ExcelUtils.setCellData(actRes, i, 2, Constant.File_TestData_ManageUsers);
				
				Boolean status = ValidationLib.verifyMsg(words, actRes);
				
				if(status)
				{
					ExcelUtils.setCellData("pass", i, 3, Constant.File_TestData_ManageUsers);
					//testSoftAssertWord("pass");
				}
				else 
			{
					ExcelUtils.setCellData("Fail", i, 3, Constant.File_TestData_ManageUsers);			
				    //testSoftAssertWord("Fail");
				}
				assertion.assertAll();
				
				
		}
	}*/
	/*
	@Test(priority = 1)
	public void testSoftAssert2()//String expRes)
	{
		SoftAssert assertion = new SoftAssert();
		System.out.println("softAssert Method Was Started");
		assertion.assertEquals("Not Found", "Found","The test case failed.");
		System.out.println("softAssert Method Was Executed");
		System.out.println("The test case passed.");
		assertion.assertAll();
	}
	
	@Test(priority = 2)
	public void testSoftAssert3()//String expRes)
	{
		SoftAssert assertion = new SoftAssert();
		System.out.println("softAssert Method Was Started");
		assertion.assertTrue(false);
		System.out.println("softAssert Method Was Executed");
		System.out.println("The test case passed.");
		assertion.assertAll();
	}*/
	
	  @Override
	  public void executeAssert(IAssert a) {
	    try {
	      a.doAssert();
	    } catch(AssertionError ex) {
	      m_errors.put(ex, a);
	      //System.out.println(a.getMessage()); 
	    }
	  }
	
	public void assertAll() {
	    if (! m_errors.isEmpty()) {
	      StringBuilder sb =
	            new StringBuilder("The following asserts failed:\n");
	      boolean first = true;
	      for (Map.Entry<AssertionError, IAssert> ae : m_errors.entrySet()) {
	        if (first) {
	          first = false;
	        } else {
	          sb.append("");
	        }
	        sb.append(ae.getValue().getMessage());
	      }
	      throw new AssertionError(sb.toString());
	    }
	  }
	}

	
	/*@Test
	public void testSoftAssert(String expRes, String msg)
	{
		SoftAssert assertion = new SoftAssert();
		
		System.out.println("softAssert Method Was Started");
		assertion.assertEquals(expRes, "Found","The test case failed on verifying "+msg+".");
		System.out.println("softAssert Method Was Executed");
		
		if(expRes.equalsIgnoreCase("Found")){
			System.out.println("The test case passed on verifying "+msg+".");
		}
		
		assertion.assertAll();
	}*/
//}
