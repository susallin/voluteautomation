package utilities;

import java.util.ArrayList;

import org.sikuli.script.FindFailed;

import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.Biography;
import education.volute.pages.Calendar;
import education.volute.pages.IdeaBoard;
import education.volute.pages.LoginPage;
import education.volute.pages.ManageActivities;
import education.volute.pages.ManagePrograms;
import education.volute.pages.ManageUsers;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;
import education.volute.pages.Passport;
import education.volute.pages.ProfileList;
import education.volute.pages.Slides;
import education.volute.pages.SocialHub;
import education.volute.pages.Video;

public class ToolDestAndCollections {
	
	// This will give the ability to transfer any content to a specific place for each tool.
	public void Destination(String toolContDes, String actName, String actSubName, String prgName, String uRole){
		NavigatorTool nt = new NavigatorTool();
		SocialHub hb = new SocialHub();
		MenuItems mi = new MenuItems();
		ManagePrograms mp = new ManagePrograms();
		ManageActivities ma = new ManageActivities();
		
		/*if(toolContDes.equalsIgnoreCase("My Journey Widget") && actSubName.isEmpty()){
			hb.clickOnLaunchActByWidget(actName);
		}*/
		if(toolContDes.isEmpty()){
			// Do nothing.
			System.out.println("Staying on Page.");
		}
		
		if(toolContDes.equalsIgnoreCase("My Journey Tool") && actSubName.isEmpty()){
			nt.clickOnJourney();
			hb.clickOnLaunchAct(actName, uRole);
		}
		
		/*if(toolContDes.equalsIgnoreCase("My Journey Widget") && !actSubName.isEmpty()){
			hb.clickOnShowSubActsByWidget(actName);
			hb.clickOnLaunchSubActByWidget(actSubName);
		}*/
		
		if(toolContDes.equalsIgnoreCase("My Journey Tool") && !actSubName.isEmpty()){
			//nt.clickOnMainMenu();
			nt.clickOnJourney();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			hb.clickOnShowSubActs(actName);
			hb.clickOnLaunchSubAct(actSubName, uRole);
		}
		
//		if(toolContDes.equalsIgnoreCase("Learning Space") && actName.isEmpty() && actSubName.isEmpty()){
//			nt.clickOnAdmin();
//			try {
//				Thread.sleep(5000);
//			} catch (InterruptedException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
//			
//			mi.clickOnProgram();
//		    try {
//				Thread.sleep(5000);
//			} catch (InterruptedException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
//		}
		
		if(toolContDes.equalsIgnoreCase("Learning Space") && actSubName.isEmpty()){
			nt.clickOnAdmin();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			//nt.clickOnMenuTab();
			mi.clickOnProgram();
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mp.clickOnselectedprogram(prgName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mp.clickOnSelectProgramAct(prgName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			ma.clickSelectedSubAct(actName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			// First Act
			ma.clickOnEditAct(actName);
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			nt.clickOnDesignTab();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//mp.clickOnManageMashupicon(pgmNameTxt);
		}
		
		if(toolContDes.equalsIgnoreCase("Learning Space") && !actSubName.isEmpty()){
			nt.clickOnAdmin();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			//nt.clickOnMenuTab();
			mi.clickOnProgram();
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mp.clickOnselectedprogram(prgName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mp.clickOnSelectProgramAct(prgName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			ma.clickOnArrowSub(actName);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			ma.clickSelectedSubAct(actSubName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			// First Act
			ma.clickOnEditAct(actSubName);
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			nt.clickOnDesignTab();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//mp.clickOnManageMashupicon(pgmNameTxt);
		}
	}
	
	public void login(String name, String pw){
		LoginPage lp = new LoginPage();
		
		lp.login(name, pw);
		lp.logibutton();
	}
	
	public void logout(){
		MenuItems mi = new MenuItems();
		NavigatorTool nt = new NavigatorTool();
		
		nt.clickOnPortfolioIcon();
		mi.clickOnLogout();
	}
	
	public void selectProgram(String pgmName){
		SocialHub hb = new SocialHub();
		
		hb.clickOnPgmDropDown();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		hb.selectPgmFromDropDown(pgmName);
	}
	
	public void goToManagePrograms(){
		NavigatorTool nt = new NavigatorTool();
		MenuItems mi	= new MenuItems();
		
		nt.clickOnAdmin();
//		try {
//			Thread.sleep(5000);
//		} catch (InterruptedException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		//nt.clickOnMenuTab();
		mi.clickOnProgram();
		
		mi.fetchAppTool("fab-module[title-search='Search Learning Space'] paper-fab[title='Search Learning Space']");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		/*mp.clickOnBackButtonPgmList();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		/*nt.clickendTour();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
	}
	
	// This will check Activity and Sub only
	public String ActType(String actName, String actSubName, String uRole){
		NavigatorTool nt = new NavigatorTool();
		SocialHub hb = new SocialHub();
		MenuItems mi = new MenuItems();
		String txt;
		
		if(actSubName.isEmpty()){
			nt.clickOnJourney();
			/*try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			hb.clickOnLaunchAct(actName, uRole);
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mi.clickOnHomeIcon();
		    try {
		    	Thread.sleep(5000);
		    } catch (InterruptedException e1) {
		    	// TODO Auto-generated catch block
		    	e1.printStackTrace();
			}
		    txt = hb.fetchVisitedActSub(actName);
		}else{
			nt.clickOnJourney();
			/*try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			hb.clickOnShowSubActs(actName);
			hb.clickOnLaunchSubAct(actSubName, uRole);
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mi.clickOnHomeIcon();
		    try {
		    	Thread.sleep(5000);
		    } catch (InterruptedException e1) {
		    	// TODO Auto-generated catch block
		    	e1.printStackTrace();
			}
		    txt = hb.fetchVisitedActSub(actSubName);
		}
		return txt;
	}
	
	// This will check Activity and Sub within
		public String ActType2(String actName, String actSubName, String uRole){
			NavigatorTool nt = new NavigatorTool();
			SocialHub hb = new SocialHub();
			MenuItems mi = new MenuItems();
			String txt;
			
			if(actSubName.isEmpty()){
				nt.clickOnJourney();
				/*try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}*/
				hb.clickOnLaunchAct(actName, uRole);
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			    txt = mi.fetchSubMenu(actName, "Activity");
			}else{
				nt.clickOnJourney();
				/*try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}*/
				hb.clickOnShowSubActs(actName);
				hb.clickOnLaunchSubAct(actSubName, uRole);
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			    txt = mi.fetchSubMenu(actName, "Sub");
			}
			return txt;
		}
		
		public void ActType3(String actName, String actSubName, String uRole){
			NavigatorTool nt = new NavigatorTool();
			SocialHub hb = new SocialHub();
			//MenuItems mi = new MenuItems();
			
			if(actSubName.isEmpty()){
				nt.clickOnJourney();
				/*try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}*/
				hb.clickOnLaunchAct(actName, uRole);
				/*try {
					Thread.sleep(15000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}*/
			}else{
				nt.clickOnJourney();
				/*try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}*/
				hb.clickOnShowSubActs(actName);
				hb.clickOnLaunchSubAct(actSubName, uRole);
				/*try {
					Thread.sleep(10000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}*/
			}
		}
		
		public ArrayList<String> navSearch(String str, String locator, String uRole){
			ArrayList<String> list = new ArrayList<String>();
			NavigatorTool mi = new MenuItems();
			SocialHub sh = new SocialHub();
			SearchElements sr = new SearchElements();
			
			if(str.equalsIgnoreCase("Stay")){
				list = sr.elementShows4(locator);
			}
			
			if(str.equalsIgnoreCase("Groups")){
				mi.clickOnHomeIcon();
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				sh.clickOnGroups();
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				list = sr.elementShows4(locator);
				
				sh.ClickOncloseGroupModal();
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				mi.clickOnCollaborateIcon();
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(str.equalsIgnoreCase("Activity")){
				ActType3("POtestactivity1", "", uRole);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				list = sr.elementShows4(locator);
			}
		
			if(str.equalsIgnoreCase("Sub")){
				ActType3("Activity for Sub activities", "SASOtest1", uRole);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				list = sr.elementShows4(locator);
			}
			return list;
		}
		
		public ArrayList<String> navSearchFloat(String locator){
			ArrayList<String> list = new ArrayList<String>();
			NavigatorTool nt = new NavigatorTool();
			MenuItems mi = new MenuItems();
			SearchElements sr = new SearchElements();
			
			mi.clickOnHomeIcon();
		    try {
		    	Thread.sleep(5000);
		    } catch (InterruptedException e1) {
		    	// TODO Auto-generated catch block
		    	e1.printStackTrace();
			}
		    nt.clickOnDiscussionIcon("Float");
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			list = sr.elementShows4(locator);
			
			return list;
		}
		
		public ArrayList<String> navSlides(String str, String toolName, String txtName, String locator, String uRole){
			ArrayList<String> list = new ArrayList<String>();
			SearchElements sr = new SearchElements();
			MenuItems mi = new MenuItems();
			NavigatorTool nt = new NavigatorTool();
			Biography bio = new Biography();
			SocialHub hb = new SocialHub();
			ManagePrograms mp = new ManagePrograms();
			ManageUsers mu = new ManageUsers();
			ManageActivities ma = new ManageActivities();
			Passport pas = new Passport();
			IdeaBoard id = new IdeaBoard();
			Calendar cal = new Calendar();
			Video vid = new Video();
			Slides sli = new Slides();
			ProfileList pl = new ProfileList();
			
			if(str.equalsIgnoreCase("Stay")){
				list = sr.elementShows4(locator);
			}
			
			if(str.equalsIgnoreCase("Manage Face")){
				list = sr.elementShows4(locator);
				mi.SelectExitManageFaceOfTool(txtName);
			}
			
			if(str.equalsIgnoreCase("List Face")){
				list = sr.elementShows4(locator);
				
				if(toolName.equalsIgnoreCase("Slides")){
					sli.clickOnListSlide(txtName);
				}
				
				if(toolName.equalsIgnoreCase("Video")){
					vid.clickOnListVideo(txtName);
				}
				
				if(toolName.equalsIgnoreCase("Profiles")){
					pl.clickOnViewUserButton(txtName);
				}
				
				if(toolName.equalsIgnoreCase("Calendar")){
					cal.clickCalFaceEvent(txtName);
				}
				
				if(toolName.equalsIgnoreCase("Guidebook")){
					pas.clickOnPassportTab(txtName);
				}
				
				if(toolName.equalsIgnoreCase("Idea Board")){
					id.ClickOnSelectedIdeaButton(txtName);
				}
			}
			
			if(str.equalsIgnoreCase("Face")){
				if(toolName.equalsIgnoreCase("Slides")){
					list = sr.elementShows4(locator);
					sli.clickOnSlidesBackToList(txtName);
				}
				
				if(toolName.equalsIgnoreCase("Video")){
					list = sr.elementShows4(locator);
					vid.clickVideoBackListButton();
				}
				
				if(toolName.equalsIgnoreCase("Profiles")){
					list = sr.elementShows4(locator);
					pl.clickOnBackToUserList();
				}
				
				if(toolName.equalsIgnoreCase("Tasks")){
					list = sr.elementShows4(locator);
					mi.SelectManageFaceOfTool(toolName);
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				
				if(toolName.equalsIgnoreCase("Idea Board2")){
					list = sr.elementShows4(locator);
					id.ClickBackIdeaTapButton();
				}
				
				if(toolName.equalsIgnoreCase("Idea Board")){
		
					if(txtName.equalsIgnoreCase("Manage")){
						mi.SelectManageFaceOfTool(toolName);
						try {
							Thread.sleep(5000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						list = sr.elementShows4(locator);
						mi.SelectExitManageFaceOfTool(toolName);
						try {
							Thread.sleep(5000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
					
					
				}
				
				if(toolName.equalsIgnoreCase("Calendar")){
					list = sr.elementShows4(locator);
					cal.clickCalBackButton(txtName);
				}
				
				if(toolName.equalsIgnoreCase("Manage Programs")){
					mp.clickOnEditPgmicon(txtName);
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					list = sr.elementShows4(locator);
					mp.clickOncancelPgmButton();
				}
				
				if(toolName.equalsIgnoreCase("Manage Users")){
					mu.clikOnSearchUserButton();
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					mu.enterSearchUser(txtName);
					mu.pressEnter();
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					list = sr.elementShows4(locator);
					mu.clikOnSearchUserButton();
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					mu.clikOnSearchUserButton();
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					nt.refreshPage();
					
					mi.fetchAppTool("div[id$='view-manage-users']");
					try {
						Thread.sleep(4000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				
				if(toolName.equalsIgnoreCase("Activity")){
					nt.clickOnStatusTab();
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					list = sr.elementShows4(locator);
					nt.clickOnDesignTab();
				}
				
				if(toolName.equalsIgnoreCase("Act List")){
					mp.clickOnselectedprogram(txtName);
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					mp.clickOnExpandProgram(txtName);
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					/*mp.clickOnSelectProgramAct(txtName);
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}*/
					list = sr.elementShows4(locator);
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					mp.clickOnHideProgram(txtName);
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					/*mp.clickOnBackButtonPgmList();
					try {
						Thread.sleep(7000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}*/
				}
				
				if(toolName.equalsIgnoreCase("Sub-Act List")){
					mp.clickOnselectedprogram(txtName);
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					/*mp.clickOnSelectProgramAct(txtName);
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}*/
					
					ma.clickOnArrowSub(uRole);
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					list = sr.elementShows4(locator);
					
					/*mp.clickOnBackButtonPgmList();
					try {
						Thread.sleep(7000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}*/
				}
				
				if(toolName.equalsIgnoreCase("Journey")){
					nt.clickOnJourney();
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					String actRes;
					try {
						actRes = sr.fetchImageShows(locator);
						list.add(actRes);
					} catch (FindFailed e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//list = sr.elementShows4(locator);
					nt.clickOnJourney();
				}
				
				if(toolName.equalsIgnoreCase("JourneySub")){
					nt.clickOnJourney();
					try {
						Thread.sleep(8000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					String actRes;
					try {
						actRes = sr.fetchImageShows(locator);
						list.add(actRes);
					} catch (FindFailed e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					/*hb.clickOnShowSubActs(txtName);
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}*/
					//list = sr.elementShows4(locator);
					nt.clickOnJourney();
				}
				
				if(toolName.equalsIgnoreCase("Act Desc")){
					ma.clickOnActInfoButton();
					try {
						Thread.sleep(8000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					list = sr.elementShows4(locator);
					ma.clickOnActExitInfoButton();
				}
				
				if(toolName.equalsIgnoreCase("Master")){
					ma.clickOnActViewMoreButton();
					try {
						Thread.sleep(8000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					list = sr.elementShows4(locator);
					ma.clickOnActExitInfoButton();
				}
				//sli.clickOnSlidesBackToList(sliName);
			}
			
			if(str.equalsIgnoreCase("Activity")){
				ActType3("POtestactivity1", "", uRole);
				try {
					Thread.sleep(7000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				mi.SelectManageFaceOfTool(toolName);
			}
		
			if(str.equalsIgnoreCase("Sub")){
				ActType3("Activity for Sub activities", "SASOtest1", uRole);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				mi.SelectManageFaceOfTool(toolName);
			}
			return list;
		}
}
