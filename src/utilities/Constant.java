package utilities;

public class Constant 

{
	
	public static final String Path_TestData = "./user_data/";
	
	public static final String File_TestData= "Volute.xlsx";
	
	public static final String File_TestData_ManagePrograms = "ManagePrograms.xlsx";
	
	public static final String File_TestData_ManageUsers = "ManageUsers.xlsx";
	
	public static final String File_TestData_SocialHub = "SocialHub.xlsx";
	
	public static final String File_TestData_Notes = "Notes.xlsx";
	
	public static final String File_TestData_Collaborate = "Collaborate.xlsx";
	
	public static final String File_TestData_Rooms = "Rooms.xlsx";
	
	public static final String File_TestData_Profile = "Profile.xlsx";
	
	public static final String File_TestData_ProfileList = "ProfileList.xlsx";
	
	public static final String File_TestData_Portal = "Portal.xlsx";
	
	public static final String File_TestData_Reader = "Reader.xlsx";
	
	public static final String File_TestData_RichTextEditor = "RichTextEditor.xlsx";
	
	public static final String File_TestData_Slides = "Slides.xlsx";
	
	public static final String File_TestData_Task = "Task.xlsx";
	
	public static final String File_TestData_Video = "Video.xlsx";
	
	public static final String File_TestData_Announcements = "Announcements.xlsx";
	
	public static final String File_TestData_Assessments = "Assessments.xlsx";
	
	public static final String File_TestData_Biography = "Biography.xlsx";
	
	public static final String File_TestData_Calendar = "Calendar.xlsx";
	
	public static final String File_TestData_File = "File.xlsx";
	
	public static final String File_TestData_Photoshare = "Photoshare.xlsx";
	
	public static final String File_TestData_Directory = "Directory.xlsx";
	
	public static final String File_TestData_Discussion = "Discussion.xlsx";
	
	public static final String File_TestData_Passport = "Passport.xlsx";
	
	public static final String File_TestData_SearchLocators = "SearchLocators.xlsx";
	
	public static final String File_TestData_ReadCsvF = "ReadCsvF.xlsx";
	
	public static final String File_TestData_SessionTO = "SessionTO.xlsx";
	
	public static final String File_Config = "Config.xlsx";

	public static final String File_TestData_LoginPage = "LoginPage.xlsx";
	
	public static final String File_TestData_IdeaBoard = "IdeaBoard.xlsx";
	
	public static final String File_TestData_PrivateMarketplace = "PrivateMarketplace.xlsx";
}

