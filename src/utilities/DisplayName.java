package utilities;

import java.util.Iterator;

import org.testng.annotations.Test;

import education.volute.common_lib.ValidationLib;
import education.volute.excel_utilities.ExcelUtils;
import education.volute.pages.MenuItems;
import education.volute.pages.NavigatorTool;

public class DisplayName {

	//@Test
	public void testDisplayName(String Role) 
	
	{
		NavigatorTool nt = new NavigatorTool();
		//ReadCsvFiles csv = new ReadCsvFiles();
		MenuItems mi = new MenuItems();
		SoftAssertCheck soft = new SoftAssertCheck();
		ToolDestAndCollections toolD = new ToolDestAndCollections();
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ManagePrograms, "DisplayNameTool");
		
		for(int i=1; i<=ExcelUtils.getRowcount(); i++)
		{
			String toolDest = ExcelUtils.getCellData(i, 1);
			String actName = ExcelUtils.getCellData(i, 2);
	        String subActName = ExcelUtils.getCellData(i, 3);
	        String FinActName = ExcelUtils.getCellData(i, 4);
	        String actTypeName = ExcelUtils.getCellData(i, 5);
	        //String subMenuAct = ExcelUtils.getCellData(i, 4);
	        String pgmNameTxt = ExcelUtils.getCellData(i, 7);
	        String toolName = ExcelUtils.getCellData(i, 8);
	        String displayName = ExcelUtils.getCellData(i, 9);
	        String uRole = ExcelUtils.getCellData(i, 13);
	        String appLocName = ExcelUtils.getCellData(i, 14);
	        String expRes = ExcelUtils.getCellData(i, 10);
	        
	        toolD.Destination(toolDest, actName, subActName, pgmNameTxt, Role);       
	        try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	        
	        mi.fetchAppTool(appLocName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	        
//	        String subMenuAct = mi.fetchSubMenu(FinActName, actTypeName);
//			System.out.println(subMenuAct);
//			try {
//				Thread.sleep(5000);
//			} catch (InterruptedException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
//			ExcelUtils.setCellData(subMenuAct, i, 6, Constant.File_TestData_ManagePrograms);
//			try {
//				Thread.sleep(5000);
//			} catch (InterruptedException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
			
//	        mi.SelectManageFaceOfTool(toolName);
//	        try {
//				Thread.sleep(2000);
//			} catch (InterruptedException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
	        
	        mi.SelectdisplayButton(toolName);
	        try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	        				
			mi.clearDisplayName(toolName);
			mi.enterDisplayName(toolName,displayName);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			mi.Savedisplay(toolName);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String actRes = mi.fetchMsgDisplayName();
			System.out.println(actRes);
			if(actRes.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking toast");
				System.out.println("The test case passed toast shows.");
			}else{
				soft.assertTrue(false, "The test case failed toast does not show.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    String actRes1 = mi.fetchTitleTool2(displayName);
			System.out.println(actRes1);
			if(actRes1.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking toast");
				System.out.println("The test case pass, the title shows on the tool.");
			}else{
				soft.assertTrue(false, "The test case failed, the title does not show.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    nt.refreshPage();
		    
		    mi.fetchAppTool(appLocName);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			String actRes2 = mi.fetchTitleTool2(displayName);
			System.out.println(actRes2);
			if(actRes2.equalsIgnoreCase("Found")){
				soft.assertTrue(true, "Checking toast");
				System.out.println("The test case pass, the title shows on the tool.");
			}else{
				soft.assertTrue(false, "The test case pass, the title shows on the tool.\n");
			}
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
//			mi.selectBackArrow(toolName);
//			try {
//				Thread.sleep(5000);
//			} catch (InterruptedException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
//			nt.clickOnHomeIcon();
//			try {
//				Thread.sleep(5000);
//			} catch (InterruptedException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
			
			ExcelUtils.setCellData(actRes, i, 11, Constant.File_TestData_ManagePrograms);
			
			Boolean status = ValidationLib.verifyMsg(expRes, actRes);
			
			if(status)
			{
				ExcelUtils.setCellData("pass", i, 12,Constant.File_TestData_ManagePrograms);
			}
			else 
		{
				ExcelUtils.setCellData("Fail", i, 12,Constant.File_TestData_ManagePrograms);
			}									
			
		}
	}
}
