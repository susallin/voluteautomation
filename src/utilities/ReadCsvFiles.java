package utilities;

//package blog;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
//import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;
//import education.volute.excel_utilities.ExcelUtils;
import au.com.bytecode.opencsv.CSVWriter;

public class ReadCsvFiles {

	//@SuppressWarnings({ "hiding", "unchecked" })
	public Iterator<String[]> ReadList(String csvFile) throws Exception {

		// This will load csv file
		@SuppressWarnings("resource")
		CSVReader csvF = new CSVReader(new FileReader("./user_data/"+csvFile+".csv"));

		// this will load content into list
		//List<String> li = new ArrayList<>();
		List<String[]> li = csvF.readAll();
		//System.out.println("Total rows which we have is "+li.size());
	
		// create Iterator reference
		Iterator<String[]> i1 = li.iterator();
		
		return i1;

		
		/*
		// Iterate all values 
		while(i1.hasNext()){

			java.lang.String[] str=i1.next();

			//System.out.println(" Values are ");

			for(int i=0;i<str.length;i++)
			{

				System.out.print(" "+str[i]);

			}
		
			System.out.println("   ");

		}*/

	}
	
	//@SuppressWarnings("unused")
	public void writeCsv(Iterator<String[]> fileName, String date, String addInfo) throws IOException {

		@SuppressWarnings("resource")
		CSVWriter writer = new CSVWriter(new FileWriter(fileName.toString()));
		StringBuilder sb = new StringBuilder();
		
		List<String[]> data = new ArrayList<String[]>();
		sb.append(addInfo + date);
		//data.addAll(addInfo,date);

		writer.writeAll(data);
            /*for (int i = 0; i < csvMatrix.length; i++) {
                csvWriter.write(csvMatrix[i]);
            }*/

    }
	
	/*public static void main(String args[]) throws Exception{
		
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData_ReadCsvF, "CsvNames");
		String csvF = ExcelUtils.getCellData(1, 0);
		
		Iterator<String[]> Title = ReadList(csvF);
	
		if(Title.hasNext())
			Title.next();
		
		while(Title.hasNext()){
			int i = 0;
			String[] Name = Title.next();
			//String[] Name1 = Title2.next();
			//java.lang.String[] str=Title.next();

			//System.out.println(" Values are ");

			String FileName = Name[i];
			String SliShowName = Name[i + 1];
			
			System.out.print(" "+FileName+" "+SliShowName+"\n");
			/*for(int i=0;i<Name.length;i++)
			{
				
				System.out.print(" "+Name[i]+" "+);

			}
		*/
}
